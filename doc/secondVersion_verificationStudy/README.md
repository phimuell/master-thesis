# Info
This folder contains a report that documents the tests that where done to ensure that the code works correct.
This document focuses on the two solvers, Stokes and Temperature.
The other components where tested with differently with small unit tests, they are located in the test  folder.
For testing this code also different solver where used to ensure the proper working.

## MATHEMATICA
The folder "Mathematica" contains the notebooks that where used to compute the source term.
Values for the parameters can be found in the used test programs.

