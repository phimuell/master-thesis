(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     17058,        541]
NotebookOptionsPosition[     14877,        495]
NotebookOutlinePosition[     15222,        510]
CellTagsIndexPosition[     15179,        507]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Testing of the Stokes-Solver", "Section",
 CellChangeTimes->{{3.782184685516058*^9, 
  3.782184711468541*^9}},ExpressionUUID->"82547425-718e-43cd-9658-\
2204f4aa9c20"],

Cell[CellGroupData[{

Cell["Assumed solutions", "Subsection",
 CellChangeTimes->{{3.7821847125985823`*^9, 
  3.782184728620923*^9}},ExpressionUUID->"34ee2492-ec2a-41b5-a149-\
3f892f10a5d4"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"VEL", " ", "X"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"VelX", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
     RowBox[{"Ax", "*", 
      RowBox[{"Sin", "[", 
       RowBox[{"gx", "*", "x"}], "]"}]}]}], ";"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.782184729701659*^9, 3.782184761165409*^9}, {
  3.7821848026611023`*^9, 
  3.782184880446393*^9}},ExpressionUUID->"e8bd6f95-bc6c-4bd6-9d01-\
8a039299be8b"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"VEL", " ", "Y"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"VelY", "[", 
     RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
    RowBox[{"Ay", "*", 
     RowBox[{"Sin", "[", 
      RowBox[{"gy", "*", "y"}], "]"}]}]}], ";"}]}]], "Input",
 CellChangeTimes->{{3.782184883671427*^9, 
  3.782184902438424*^9}},ExpressionUUID->"de14e93c-9601-4efe-af89-\
c76e7f5dbfc9"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", "PRESSURE", " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"P", "[", 
     RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
    RowBox[{
     RowBox[{"Ap", "*", 
      RowBox[{"Sin", "[", 
       RowBox[{
        RowBox[{"wx", "*", "x"}], "+", 
        RowBox[{"wy", "*", "y"}]}], "]"}]}], "+", "Bp"}]}], ";"}]}]], "Input",\

 CellChangeTimes->{{3.7821849058074503`*^9, 
  3.782184960550946*^9}},ExpressionUUID->"c23be529-bdd4-4124-9452-\
fa14be9424bd"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Physical Properties", "Subsection",
 CellChangeTimes->{{3.78218546251231*^9, 
  3.782185484137958*^9}},ExpressionUUID->"b1fe622e-dc3b-4a3d-bd0a-\
ac6cd172ad8d"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", "ETA", " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"Eta", "[", 
     RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
    RowBox[{"aEta", "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"ArcTan", "[", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"x", "-", "mEtaX"}], ")"}], "/", "cEtaX"}], "]"}], "+", 
       "bEtaX"}], ")"}], "*", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"ArcTan", "[", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"y", "-", "mEtaY"}], ")"}], "/", "cEtaY"}], "]"}], "+", 
       "bEtaY"}], ")"}]}]}], ";"}]}]], "Input",
 CellChangeTimes->{{3.782185033342243*^9, 
  3.7821851303752213`*^9}},ExpressionUUID->"ab78555d-10d0-4a84-91ae-\
9ad5a58c380a"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", "RHO", " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"Rho", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{"rho0", " ", "*", " ", 
       RowBox[{"Exp", "[", 
        RowBox[{"aRho", " ", "*", " ", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{
            RowBox[{"(", 
             RowBox[{
              RowBox[{"(", 
               RowBox[{"x", "-", "mRhoX"}], ")"}], "/", "Lx"}], ")"}], "^", 
            "2"}], "+", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{
              RowBox[{"(", 
               RowBox[{"y", " ", "-", "nRhoY"}], ")"}], "/", "Ly"}], ")"}], 
            "^", "2"}]}], ")"}]}], "]"}]}], "+", "bRho"}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
    "GRAVITY", "  ", "only", " ", "in", " ", "y", " ", "direction", " ", 
     "and", " ", "positive", " ", "due", " ", "to", " ", "orientation"}], " ",
     "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Gx", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], ":=", "0"}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Gy", "[", 
      RowBox[{"x_", ",", "y_"}], "]"}], ":=", "gY"}], ";"}]}]}]], "Input",
 CellChangeTimes->{{3.7821854858339243`*^9, 3.782185587283045*^9}, {
  3.7821943520114317`*^9, 
  3.782194445498843*^9}},ExpressionUUID->"df4a9fe8-350c-47e2-8aa5-\
2d98d049ecff"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Stresses", "Subsection",
 CellChangeTimes->{{3.782184977140634*^9, 
  3.78218500815071*^9}},ExpressionUUID->"898cc8cf-b743-4a29-bdeb-\
5614113ac27b"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"SIGMA", " ", "XX"}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"SigXX", "[", 
      RowBox[{"X_", ",", "Y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{"Eta", "[", 
        RowBox[{"x", ",", "y"}], "]"}], "*", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"VelX", "[", 
          RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}]}], " ", "/.", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x", "\[Rule]", "X"}], ",", 
        RowBox[{"y", "\[Rule]", "Y"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{"SIGMA", " ", "YY"}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"SigYY", "[", 
      RowBox[{"X_", ",", "Y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{"Eta", "[", 
        RowBox[{"x", ",", "y"}], "]"}], "*", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"VelY", "[", 
          RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}]}], " ", "/.", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x", "\[Rule]", "Y"}], ",", 
        RowBox[{"y", "\[Rule]", "Y"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{"SIG", " ", 
     RowBox[{"XY", "/", "YX"}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"SigXY", "[", 
     RowBox[{"X_", ",", "Y_"}], "]"}], ":=", 
    RowBox[{
     RowBox[{
      RowBox[{"Eta", "[", 
       RowBox[{"x", ",", "y"}], "]"}], "*", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"VelX", "[", 
            RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}], "+", 
         RowBox[{"D", "[", 
          RowBox[{
           RowBox[{"VelY", "[", 
            RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}]}], ")"}], "/", 
       "2"}]}], " ", "/.", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"x", "\[Rule]", "X"}], ",", 
       RowBox[{"y", "\[Rule]", "Y"}]}], "}"}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.782185009469367*^9, 3.782185026095274*^9}, {
  3.782185136231881*^9, 
  3.7821853648740797`*^9}},ExpressionUUID->"72e27dc1-8013-44cd-bb04-\
e18031001caa"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Source Terms", "Subsection",
 CellChangeTimes->{{3.782185407375395*^9, 
  3.7821854312014847`*^9}},ExpressionUUID->"9fcaa468-7881-4fc5-8f7f-\
e21f830f38a1"],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"SOURCE", " ", "VEL", " ", "X"}], " ", "*)"}], 
  "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"Sx", "[", 
      RowBox[{"X_", ",", "Y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"P", "[", 
           RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}]}], "+", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"SigXX", "[", 
          RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], "+", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"SigXY", "[", 
          RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}], "+", 
       RowBox[{
        RowBox[{"Rho", "[", 
         RowBox[{"x", ",", "y"}], "]"}], "*", 
        RowBox[{"Gx", "[", 
         RowBox[{"x", ",", "y"}], "]"}]}]}], "/.", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x", "\[Rule]", "X"}], ",", 
        RowBox[{"y", "\[Rule]", "Y"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Sy", "[", 
      RowBox[{"X_", ",", "Y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{"-", 
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"P", "[", 
           RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}]}], "+", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"SigXY", "[", 
          RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], "+", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"SigYY", "[", 
          RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}], "+", 
       RowBox[{
        RowBox[{"Rho", "[", 
         RowBox[{"x", ",", "y"}], "]"}], "*", 
        RowBox[{"Gx", "[", 
         RowBox[{"x", ",", "y"}], "]"}]}]}], "/.", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x", "\[Rule]", "X"}], ",", 
        RowBox[{"y", "\[Rule]", "Y"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{
     RowBox[{"Sp", "[", 
      RowBox[{"X_", ",", "Y_"}], "]"}], ":=", 
     RowBox[{
      RowBox[{
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"VelX", "[", 
          RowBox[{"x", ",", "y"}], "]"}], ",", "x"}], "]"}], "+", 
       RowBox[{"D", "[", 
        RowBox[{
         RowBox[{"VelY", "[", 
          RowBox[{"x", ",", "y"}], "]"}], ",", "y"}], "]"}]}], "/.", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"x", "\[Rule]", "X"}], ",", 
        RowBox[{"y", "\[Rule]", "Y"}]}], "}"}]}]}], ";"}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.782185432603451*^9, 3.782185441177981*^9}, {
  3.782185594562711*^9, 
  3.782185820485402*^9}},ExpressionUUID->"b7ffecae-1d47-4c94-94be-\
6f538eeb0dc5"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Sx", "[", 
  RowBox[{"x", ",", "y"}], "]"}]], "Input",
 CellChangeTimes->{{3.7821858889106283`*^9, 
  3.782185927669265*^9}},ExpressionUUID->"95978bd7-b982-473b-beb7-\
48ab4349e273"],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"aEta", " ", "Ax", " ", "gx", " ", 
    RowBox[{"(", 
     RowBox[{"bEtaY", "+", 
      RowBox[{"ArcTan", "[", 
       FractionBox[
        RowBox[{
         RowBox[{"-", "mEtaY"}], "+", "y"}], "cEtaY"], "]"}]}], ")"}], " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"gx", " ", "x"}], "]"}]}], 
   RowBox[{"cEtaX", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      FractionBox[
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "mEtaX"}], "+", "x"}], ")"}], "2"], 
       SuperscriptBox["cEtaX", "2"]]}], ")"}]}]], "-", 
  RowBox[{"Ap", " ", "wx", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{
     RowBox[{"wx", " ", "x"}], "+", 
     RowBox[{"wy", " ", "y"}]}], "]"}]}], "-", 
  RowBox[{"aEta", " ", "Ax", " ", 
   SuperscriptBox["gx", "2"], " ", 
   RowBox[{"(", 
    RowBox[{"bEtaX", "+", 
     RowBox[{"ArcTan", "[", 
      FractionBox[
       RowBox[{
        RowBox[{"-", "mEtaX"}], "+", "x"}], "cEtaX"], "]"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{"bEtaY", "+", 
     RowBox[{"ArcTan", "[", 
      FractionBox[
       RowBox[{
        RowBox[{"-", "mEtaY"}], "+", "y"}], "cEtaY"], "]"}]}], ")"}], " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"gx", " ", "x"}], "]"}]}]}]], "Output",
 CellChangeTimes->{{3.782185898883704*^9, 
  3.7821859283285437`*^9}},ExpressionUUID->"5d0cd278-4d3d-4ff6-bb0e-\
44c6a922bfc8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Sy", "[", 
  RowBox[{"x", ",", "y"}], "]"}]], "Input",
 CellChangeTimes->{{3.782185981016081*^9, 
  3.7821859861017427`*^9}},ExpressionUUID->"c9cc3042-f290-48d2-a6b3-\
214247ebbcc9"],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"aEta", " ", "Ay", " ", "gy", " ", 
    RowBox[{"(", 
     RowBox[{"bEtaX", "+", 
      RowBox[{"ArcTan", "[", 
       FractionBox[
        RowBox[{
         RowBox[{"-", "mEtaX"}], "+", "y"}], "cEtaX"], "]"}]}], ")"}], " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"gy", " ", "y"}], "]"}]}], 
   RowBox[{"cEtaY", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      FractionBox[
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "mEtaY"}], "+", "y"}], ")"}], "2"], 
       SuperscriptBox["cEtaY", "2"]]}], ")"}]}]], "+", 
  FractionBox[
   RowBox[{"aEta", " ", "Ay", " ", "gy", " ", 
    RowBox[{"(", 
     RowBox[{"bEtaY", "+", 
      RowBox[{"ArcTan", "[", 
       FractionBox[
        RowBox[{
         RowBox[{"-", "mEtaY"}], "+", "y"}], "cEtaY"], "]"}]}], ")"}], " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"gy", " ", "y"}], "]"}]}], 
   RowBox[{"cEtaX", " ", 
    RowBox[{"(", 
     RowBox[{"1", "+", 
      FractionBox[
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "mEtaX"}], "+", "y"}], ")"}], "2"], 
       SuperscriptBox["cEtaX", "2"]]}], ")"}]}]], "-", 
  RowBox[{"Ap", " ", "wy", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{
     RowBox[{"wx", " ", "x"}], "+", 
     RowBox[{"wy", " ", "y"}]}], "]"}]}], "-", 
  RowBox[{"aEta", " ", "Ay", " ", 
   SuperscriptBox["gy", "2"], " ", 
   RowBox[{"(", 
    RowBox[{"bEtaX", "+", 
     RowBox[{"ArcTan", "[", 
      FractionBox[
       RowBox[{
        RowBox[{"-", "mEtaX"}], "+", "y"}], "cEtaX"], "]"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{"bEtaY", "+", 
     RowBox[{"ArcTan", "[", 
      FractionBox[
       RowBox[{
        RowBox[{"-", "mEtaY"}], "+", "y"}], "cEtaY"], "]"}]}], ")"}], " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"gy", " ", "y"}], "]"}]}]}]], "Output",
 CellChangeTimes->{
  3.78218598705646*^9},ExpressionUUID->"ce1b9c2e-7071-4ee4-bbf6-d15658986ddf"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Sp", "[", 
  RowBox[{"x", ",", "y"}], "]"}]], "Input",
 CellChangeTimes->{{3.7821859885117617`*^9, 
  3.782185991301834*^9}},ExpressionUUID->"eca30874-2966-4583-99c3-\
5ca4d2df2ded"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Ax", " ", "gx", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"gx", " ", "x"}], "]"}]}], "+", 
  RowBox[{"Ay", " ", "gy", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"gy", " ", "y"}], "]"}]}]}]], "Output",
 CellChangeTimes->{
  3.782185991986341*^9},ExpressionUUID->"dfdc3f01-f364-49bd-a04d-\
16fea1f162f0"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1676, 1006},
WindowMargins->{{122, Automatic}, {Automatic, -1060}},
FrontEndVersion->"11.2 for Linux x86 (64-bit) (September 10, 2017)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 173, 3, 68, "Section",ExpressionUUID->"82547425-718e-43cd-9658-2204f4aa9c20"],
Cell[CellGroupData[{
Cell[778, 29, 167, 3, 55, "Subsection",ExpressionUUID->"34ee2492-ec2a-41b5-a149-3f892f10a5d4"],
Cell[948, 34, 543, 16, 78, "Input",ExpressionUUID->"e8bd6f95-bc6c-4bd6-9d01-8a039299be8b"],
Cell[1494, 52, 445, 13, 55, "Input",ExpressionUUID->"de14e93c-9601-4efe-af89-c76e7f5dbfc9"],
Cell[1942, 67, 515, 16, 55, "Input",ExpressionUUID->"c23be529-bdd4-4124-9452-fa14be9424bd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2494, 88, 166, 3, 55, "Subsection",ExpressionUUID->"b1fe622e-dc3b-4a3d-bd0a-ac6cd172ad8d"],
Cell[2663, 93, 775, 24, 55, "Input",ExpressionUUID->"ab78555d-10d0-4a84-91ae-9ad5a58c380a"],
Cell[3441, 119, 1517, 44, 180, "Input",ExpressionUUID->"df4a9fe8-350c-47e2-8aa5-2d98d049ecff"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4995, 168, 155, 3, 55, "Subsection",ExpressionUUID->"898cc8cf-b743-4a29-bdeb-5614113ac27b"],
Cell[5153, 173, 2317, 70, 193, "Input",ExpressionUUID->"72e27dc1-8013-44cd-bb04-e18031001caa"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7507, 248, 162, 3, 55, "Subsection",ExpressionUUID->"9fcaa468-7881-4fc5-8f7f-e21f830f38a1"],
Cell[7672, 253, 2738, 86, 128, "Input",ExpressionUUID->"b7ffecae-1d47-4c94-94be-6f538eeb0dc5"],
Cell[CellGroupData[{
Cell[10435, 343, 206, 5, 31, "Input",ExpressionUUID->"95978bd7-b982-473b-beb7-48ab4349e273"],
Cell[10644, 350, 1405, 44, 84, "Output",ExpressionUUID->"5d0cd278-4d3d-4ff6-bb0e-44c6a922bfc8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12086, 399, 206, 5, 31, "Input",ExpressionUUID->"c9cc3042-f290-48d2-a6b3-214247ebbcc9"],
Cell[12295, 406, 1960, 62, 84, "Output",ExpressionUUID->"ce1b9c2e-7071-4ee4-bbf6-d15658986ddf"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14292, 473, 206, 5, 31, "Input",ExpressionUUID->"eca30874-2966-4583-99c3-5ca4d2df2ded"],
Cell[14501, 480, 336, 10, 35, "Output",ExpressionUUID->"dfdc3f01-f364-49bd-a04d-16fea1f162f0"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

