# Info
This folder contains everything that is related to documentation.
This includes the manual (if one will be written), or reports.
However it also contains project management related documents.

# Folders
Here is a list of folders and a small summary of their content.
For folders that contains a lot of other material.
Some folder, especially if they contain a lot of material,
have their own README, that gives additional information
and also provides them in more detail.


## REPORT
This folder contains the report document.
See internal README for more.


## SECONDVERSION_OUTLINE
This folder contains the outline of the changes to the original version.
You can consider it as a patch that is applied.
I have written it to make sure, that Taras' and I are on the same page.
It is not a complete description, but will give a rough overview.


## SECONDVERSION_VERIFICATIONSTUDY
This folder contains a small report that outlines the effort for
a verification of the implementation of the code.


## PRESENTATION
This is the presentation I made for the thesis.



