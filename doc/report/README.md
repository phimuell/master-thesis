# Report
In this folder you will found the report I wrote. I used LaTeX for writing it.

The folder "thesis_template" contains the template that I used for it.

The folder "philip_report" contains the actual report. Inside it, you will found
an additional README that explains how to compile it.


## PDF
The final PDF is also located here.



