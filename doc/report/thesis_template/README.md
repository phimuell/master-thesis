# Info
This folder contains the template for my report.
I have used it several times already.

It is a slightly bit modified version, with better ignoring scheme and so on.

## Picture
This is the folder where are the pictures are located.
Note that this folder unignores PDFs.
If it is needed this could be changed.

## GITIGNORE
The folder contains a .gitignore file that should work.

# Source
I downloaded it from: "https://www.cadmo.ethz.ch/education/thesis/thesis-template.zip"
I wanted to thanks Stefano Weidmann, stefanow@ethz.ch, for pointing my attention to the template.
