%%%%%%%%%%%%%%%%%%%%%%
% in this file we will discuss the NS equations

\section{The Navier--Stokes Equations}~\label{sec:GoverningEq:TheNSEQ}
In this section we will present the governing equations, which are known as Navier--Stokes equations.
The derivation shown here is not very rigorous, however its main purpose is to give the reader an intuitive understanding of them.
%
\\
A rigorous derivation of the equations can be found in a broad range of text books, such as \cite{kundu_book} and \cite{batchelor_2000}, which focuses on fluid dynamics or \cite{gerya_2019}, which is more focused on geodynamics.


		\subsubsection{General Assumptions}~\label{sec:GoverningEq:TheNSEQ::genAssump}
		Before we begin, we need to discuss our basic assumptions.
		%
		\begin{itemize}
			
			\item 
			The problem is seen as a \emph{continuum}, so we no longer assumes that the medium is composed of particles, a.k.a.\@ atoms. 
			There is just a field.
			
			\item
			A Quantity is described by a so called \emph{field variable}.
			This is a function of space and time and its value corresponds to the property at that time.
			Unless specified otherwise they are defined in an \emph{Eulerian} sense.
			%
			\\
			A good example is density, let $\Fkt{\rho}{\Vek{x},\, t}$ be the field variable that represent density, then its value corresponds to the density that is observed at point $\Vek{x}$ at time $t$.
			
			
			\item
			We assume that the field variable are sufficient differentiable.
			We would like to point out, that the equations we are going to derive are actually integral equations, which impose much weaker conditions on the functions as differential equations do.
			However they are most often presented in their differential form and we will do the same.
			
			\item
			We assume an incompressible media\footnote{We will later explain this point further.}.
						
		\end{itemize}
		%
		We also assume that the reader is familiar with basic fluid dynamic conventions, differential equations and vector calculus.
				
		% END:	General Assumptions
		%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{A Small Note About the Notation}~\label{sec:GoverningEq:TheNSEQ::Notation}
		As a small remark, we will mostly adopt the notation that is also used in \cite{gerya_mic} and \cite{gerya_2019}.
		But we will diverge from it in some aspects.
		%
		As such we will not use ${}^{\prime}$ to denote deviatoric quantities, instead we will use $\hat{\cdot}$ for such a purpose.
		Further we will denote deviatoric stress as $\tau$, instead of $\hat{\sigma}$.
				
		% END:		Notation
		%%%%%%%%%%%%%%%%%%%%%%%%%


	\subsection{\emph{The} Continuity Equation}~\label{sec:GoverningEq:TheNSEQ:ContEquations}
	Before we start with deriving the governing equations, we will derive a general continuity equation, which is also known as \emph{Reynolds transport theorem}.
	Let $\oldphi = \Fkt{\oldphi}{\Vek{x}, t}$ be some quantity of interest\footnote{The textbook example, which we also recommend is density.}, further let $\Omega = \Fkt{\Omega}{t} \subset \R^{3}$ be a time dependent \emph{control volume}.
	
	Now we define the property $\Phi$ as the accumulated ``property'' of $\oldphi$ inside the control volume.
	This is expressed by the following integral
	\begin{align}
		\Phi = \Fkt{\Phi}{t} = \int_{\Fkt{\Omega}{t}} \Fkt{\oldphi}{\Vek{x}, t} \udvol{\Vek{x}} .	\label{eq:GoverningEq:TheNSEQ:ContEquations:conservedValue}
	\end{align}
	%
	Please note, that while $\oldphi$ depends on time \emph{and} space, $\Phi$ \emph{only} depends on time.
	
	Now we would like to know how $\Phi$ changes with time, hence what is $\dot{\Phi}$?
	After lengthy and tedious, but straight forward calculation\footnote
		{
			The best mathematical derivation of this, that we are aware of, can be found on 
			\derivReynoldsWiki{} (German only), accessed 27-01-2020.
			The main idea is that we transform the time dependent control volume $\Omega$ into a time \emph{independent} control volume $\hat{\Omega}$ and using multidimensional substitution.
			Then the differentiation and integration can be swapped.
			The reset is just tedious and a little bit scary, math.
		}
	we arrive at the following expression
	%
	\begin{align}
		\dot{\Phi} 
			&= \frac{\mathrm{d}}{\mathrm{d}t} \int_{\Fkt{\Omega}{t}} \Fkt{\oldphi}{\Vek{x}, t} \udvol{\Vek{x}}  \nonumber \\
			&= \int_{\Fkt{\Omega}{t}} \pdiffAt{t}{\oldphi}{\Vek{x}, t} \udvol{\Vek{x}} 
				+ \int_{\partial \Fkt{\Omega}{t}} \DotP{ \Fkt{\oldphi}{\Vek{x}, t} \cdot \Fkt{\Vek{v}}{\Vek{x}, t} }{\Fkt{\Vek{n}}{\Vek{x}, t}} \udvol{\Vek{x}}  \nonumber \\
			&= \int_{\Fkt{\Omega}{t}} \dot{\oldphi} \udvol{\Vek{x}} 
				+ \int_{\partial \Fkt{\Omega}{t}} \DotP{ \oldphi \cdot \Vek{v} }{\Vek{n}} \udvol{\Vek{x}} \label{eq:GoverningEq:TheNSEQ:ContEquations:changeOfConcervedValue} \\
			&= \Fkt{S}{\Vek{x}, t}  
			 = \int_{\Fkt{\Omega}{t}} \Fkt{s}{\Vek{x}, t} \udvol{\Vek{x}} . \nonumber 
	\end{align}
	%
	In the above equation $\Vek{n} = \Fkt{\Vek{n}}{\Vek{x}, t}$, is the normal vector that points \emph{outwards} of $\Omega$, it naturally depends on space, but also on time.
	$\Vek{v} = \Fkt{\Vek{v}}{\Vek{x}, t}$ is the velocity at which the boundary of the control volume move through space.
	%
	\\
	Equation \eqref{eq:GoverningEq:TheNSEQ:ContEquations:changeOfConcervedValue} is composed out of two parts.
	The first part is a volume integral, it describes how the quantity $\Phi$ changes, because $\oldphi$ changes inside the control volume.
	The second part is a surface integral, it describes the amount of quantity that flows through the boundary and leaves or enters the control volume by convection.
	
	Finally the term $\Fkt{S}{\Vek{x}, t}$ is the source term, that describes how $\oldphi$ is lost or produced inside the control volume.
	We would like to point out, that the source is imposed by the physic of the concrete problem under consideration.
	At this point however, it will remain a general term.
	
	Applying Gauss theorem, allows us to transform the surface integral into a volume integral.
	After rearrange terms we arrive at the following expression
	\begin{align}
		0 = \int_{\Fkt{\Omega}{t}}  \left( \dot{\oldphi} +  \DivWrt{\oldphi \cdot \Vek{v}}{\Vek{x}} - s \right)  \udvol{\Vek{x}} . \label{eq:GoverningEq:TheNSEQ:ContEquations:genContiEquationInt}
	\end{align}
	%
	Since the equation has to hold for \emph{any} control volume, we deduce that the integrand itself must be zero.
	This leads us to the general form of the a conservation law in \emph{differential form},
	\begin{align}
		\pdiffAt{t}{\oldphi}{\Vek{x}, t} +  \DivWrt{\Fkt{\oldphi}{\Vek{x}, t} \cdot \Fkt{\Vek{v}}{\Vek{x}, t} }{\Vek{x}} 
		= \pdiff{t}{\oldphi} +  \nabla_{\Vek{x}} \cdot \left(\oldphi \Vek{v} \right)
		= \Fkt{s}{\Vek{x}, t} . \label{eq:GoverningEq:TheNSEQ:ContEquations:genContiEquationDiff}
	\end{align}
	
	
		\subsubsection{Material Derivative}~\label{sec:GoverningEq:TheNSEQ:ContEquations:Dt}
		We can further manipulate \eqref{eq:GoverningEq:TheNSEQ:ContEquations:genContiEquationDiff}, by applying the product rule for multiple dimensions
		\begin{align}
			\pdiff{t}{\oldphi} +  \nabla_{\Vek{x}} \cdot \left(\oldphi \Vek{v} \right)
			&= \pdiff{t}{\oldphi} +  \Vek{v} \cdot \nabla_{\Vek{x}} \oldphi  + \oldphi \nabla_{\Vek{x}} \cdot \Vek{v} \nonumber \\
			&= \Dt{\oldphi} + \oldphi \nabla_{\Vek{x}} \cdot \Vek{v}  \label{eq:GoverningEq:TheNSEQ:ContEquations:Dt:genContiEquationDiffMatDiff} \\
			&= \Fkt{s}{\Vek{x}, t} . \nonumber
		\end{align}
		%
		In equation \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Dt:genContiEquationDiffMatDiff} the symbol $\Dt{\cdot}$ is the so called \emph{material derivative}.
		It can be seen as the change that is experienced by a \emph{fluid parcel}.
		
		A fluid parcel is a general concept, that is encountered in fluid dynamics.
		It can be seen as an infinite small particle that moves through space, hence the ``the flow'' is composed of these particles\footnote
			{
				In a sense they are similar to atoms, but at the same times they rather different.
				%
				\\
				The similarity between them is, that both compose the objects under consideration.
				However they differ in a critical point.
				In a very simple view, atoms are similar to \emph{a} solar system.
				Such a system may be very large, but most of the space, that is designated as \emph{a solar system} is actually empty.
				The same is true for atoms.
				Fluid parcels on the other hand are dense, they are completly filled.
				Also they fill up the entire space, at every point in space there is always a parcel, never a void.
			}.
		
		The ordinary derivative, with respect to time of \emph{Eulerian} field variables does not fully describes how properties changes as time moves on.
		To see this, a critical observation must be made.
		The value of a (Eulerian) field property at a certain location at a specific time, is determined by the fluid parcel at that location (at that time).
		
		So in order to accurately describe the change of a property, one has to consider the changes that are experienced by a fluid parcel.
		%
		\\
		There are two components to this, the first is temporal change.
		This changes occur simply because time is moving on.
		As an example, image a temperature that is the same everywhere, but depends on time.
		Regardless if an observer is moving or stationary, the change experienced is the same.
		%
		\\
		The second kind are convective changes.
		These changes are solely caused by the fact that a particle moves through space.
		The textbook example is a temperature field that only depends on space.
		A parcel that remains at the same location, will not experience any change.
		However a moving parcel will experience changes.
		
		The material derivative is now the combination of both kind of changes.
		% END: 		Material Derivative
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	% END: 		Continuity equation
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	\subsection{Mass Conservation}~\label{sec:GoverningEq:TheNSEQ:ContEquations:Mass}	% Was previously only a sub section 
	We will now use the previously derived general continuity equation, \eqref{eq:GoverningEq:TheNSEQ:ContEquations:genContiEquationDiff}, to find the equation that governs mass.
	However before we proceed some basic considerations are necessary.
	
	First we have to think about what $\oldphi$ is.
	The answer to that question is, quite easy $\oldphi = \rho$.
	
	The second point we have to consider is what the source term $s$ is.
	From our every day experience we know that mass is conserved, i.e. it does not vanish, thus $s \equiv 0$.
	
	The last point is what are the implications of incompressibility?
	From a mathematical point, it means $\Dt{\rho} \equiv 0$.
	It is very important that the \emph{Eulerian density} may change even in an incompressible flow, but not the density of a fluid parcel\footnote
		{
			It is important while $\Dt{\rho} = 0$, does not imply $\rho \equiv \text{const}$, a constant density does imply $\Dt{\rho} \equiv 0$.
		}.
				
	With these three consideration, we can now derive the conservation law, that describes the evolution of mass as
	\begin{align}
		\dot{\rho} + \nabla_{\Vek{x}} \cdot \left(\rho \Vek{v} \right) 
			&= \Dt{\rho} + \rho \, \nabla_{\Vek{x}} \cdot \Vek{v} \nonumber \\ 
			&= \rho \, \nabla_{\Vek{x}} \cdot \Vek{v}  \nonumber \\
			&=         \nabla_{\Vek{x}} \cdot \Vek{v} = \DivWrt{\Vek{v}}{\Vek{x} }
    = 0 .  \label{eq:GoverningEq:TheNSEQ:ContEquations:Mass:Konti}
	\end{align}
	%
	Equation \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Mass:Konti} is called the incompressible continuity equation or simply continuity equation.
	
	% END: Mass Conservation
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
	\subsection{Momentum Conservation}~\label{sec:GoverningEq:TheNSEQ:ContEquations:Momentum} 	% Was previously only a sub section 
	We will now derive the conservation law for momentum.
	As before, some basic physical considerations are need.
	%
	\\
	Density was a \emph{scalar}, but momentum is a \emph{vector} quantity.
	But the same machinery that was developed before, can be used again, however each component must be considered separately.
	In the following we will use Einstein's summation convention, \cite{Einstein:1916vd}.		
	
	The first and most basic consideration is, what $\Vek{\oldphi}$ is.
	We want to study or rather express, how momentum evolves with time.
	So the most logical choice is to define is as $\Vek{\oldphi} = \rho \, \Vek{u}$.
	We would like to note that this, is not momentum but momentum density.
	This is necessary, such that $\Phi$, the integral quantity, has the unit of a moment.
	
	The second more involved problem is, how to choose $s$.
	From our every day experience we know that momentum is \emph{not} preserved, and vanishes.
	However, according to Newton, momentum of a body is preserved, unless a force is acting on it, \cite{Newton:1687eqk}.
	To keep things easy, for now at least, we will postpone this question and proceed with a general $s$.
	
	Substituting our choice for $\Vek{\oldphi}$ into \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Dt:genContiEquationDiffMatDiff} leads to 
	\begin{align}
		\Dt{\rho \, v_{i} } + \left(v_{i} \rho \right) \nabla_{\Vek{x}} \cdot \Vek{v} 
			&= \rho \, \Dt{v_i} + v_{i} \, \Dt{\rho} + \left(v_{i} \rho \right) \nabla_{\Vek{x}} \cdot \Vek{v}   		\nonumber \\
			&= \rho \, \Dt{v_i} + v_{i} \, \left( \Dt{\rho} +  \rho \nabla_{\Vek{x}} \cdot \Vek{v} \right)				\nonumber \\
			&= \rho \, \Dt{v_i} + v_{i} \, \cancel{\left( \Dt{\rho} +  \rho \nabla_{\Vek{x}} \cdot \Vek{v} \right)} 	\nonumber \\
			&= \rho \, \Dt{v_i} 
		 = s, \label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:gen}	
	\end{align}
	% 
	for the evolution of component $i$ of momentum.
	
	We still have a general source term, we will now continue our considerations from before.
	The momentum of a fluid parcel will change if a force is applied to it, so we must now express that force somehow.
	%
	Further considerations, lead us finally to Cauchy's stress law.
	\begin{align}
		\rho \, \Dt{u_i} = \partial_{x_j} \sigma_{ij} + f_{i}	\label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:cauchy1}		
	\end{align}
	%
	In the equation above $\Vek{f}$ is the body force, expressed in force per unit volume.
	$\sigma$, known as Cauchy's stress tensor, is a second order tensor, \emph{i.e.} a matrix.
	A good derivation of the above process is presented in \cite{gerya_2019}.
	%
	\\
	$\sigma$ is no ordinary second order tensor, the conservation of \emph{angular} momentum, implies that it must be \emph{symmetric}, i.e. $\sigma_{ij} = \sigma_{ji}$.
	
	We can further decompose the stress tensor into two components.
	The first component is an isotropic tensor, defined as 
	\begin{align}
		p_{ij} = p \, \delta_{ij} 	\qquad \qquad	p = -\frac{1}{2} \sigma_{kk} \label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:pressure}
	\end{align}
	%
	$\hat{p}$ is \emph{a} pressure.
	It can be seen as the force that tries \emph{uniformly} compress or extend a fluid parcel.
	Its sign depends on convention, we will define it such that it is positive under extension.
	
	The second part is called \emph{deviatoric stress}, and is often denoted as $\tau$, note that \cite{gerya_2019} uses $\sigma^{\prime}$, it is a traceless tensor.
	Physically this means that it does not try to changing the volume of a parcel, but tries to deform it.
	It is defined as 
	\begin{align}
		\tau = \sigma + p \Mat{I}		\qquad \qquad 		\tau_{ij} = \sigma_{ij} + p \, \delta_{ij}		\label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:shearstress} 
	\end{align}
	%
	This allows us to write \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:cauchy1} as
	\begin{align}
		\rho \, \Dt{v_i} = -\partial_{x_i} p + \partial_{x_j} \tau_{ij} + f_{i}	\label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:cauchy2}
	\end{align}

	The problem with equation \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:cauchy2} is that, even with the continuity equation, is not closed.
	We have $9$ unknowns\footnote
		{
			We have $3$ velocities, one pressure and $6$ unknown stresses. $\tau$ may have $9$ entries, but the symmetry condition reduces them to $6$.
		}, 
	but only $4$ equations\footnote
		{
			One for mass conservation and $3$ equations for momentum conservation.
		}.
	%
	In order to solve this problem, we must use a constitutive relationship, that allows us to express some unknowns by a combination of the remaining ones, thus effectively reducing their number.
	%
	We assume that we have a Newtonian fluid, which means that there is a \emph{linear} relationship between the (deviatoric) stress $\tau$ and the (deviatoric) strain rate $\dot{\hat{\epsilon}}_{ij}$.
	$\eta$ is the factor of proportionality and is known as (dynamic) viscosity.
	\begin{align}
		\tau_{ij} 
			&= 2 \, \eta \, \dot{\hat{\epsilon}}_{ij}		\label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:cnstiStressStrain}	\\
		\dot{\hat{\epsilon}}_{ij}
			&= \dot{\epsilon}_{i, j} - \frac{1}{3} \dot{\epsilon}_{kk}	\label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:defDeviatoricStrainRatePure} \\
			&= \frac{1}{2} \, \left( \frac{\partial v_{i}}{\partial x_j} + \frac{\partial v_{j}}{\partial x_i} \right)  - \frac{1}{3} \frac{\partial v_{k}}{\partial x_{k}}
				 \label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:defDeviatoricStrainRate}
	\end{align}
	%
	It is important that $\dot{\hat{\epsilon}}_{ij}$ is the \emph{deviatoric} strain rate tensor.
	However since we are in an incompressible setting, the deviatoric and non--deviatoric strain rate tensor are the same, because $\frac{\partial v_{k}}{\partial x_{k}} = \DivWrt{\Vek{v}}{\Vek{x} } \equiv 0$.
	Another important remark is, that $\eta$ does not need to be constant and can vary both in space and time.
	
	If we now use \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:cnstiStressStrain} and \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:defDeviatoricStrainRate} and put them into \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:cauchy2} we get the following system of equations
	\begin{align}
		\rho \, \Dt{v_i} = -\partial_{x_i} p + \partial_{x_j} \left[ \eta  \left( \frac{\partial v_{i}}{\partial x_j} + \frac{\partial v_{j}}{\partial x_i} \right) \right] + f_{i}	. \label{eq:GoverningEq:TheNSEQ:ContEquations:Momentum:cauchy3}
	\end{align}
	
	% END: 	Momentum conservation
	%%%%%%%%%%%%%%%%%%%%%%
	
		
	\subsection{Energy Equation}~\label{sec:GoverningEq:TheNSEQ:ContEquations:Energy}		% Was previously only a subsubsection
	In the current study the energy equation was mostly ignored.
	However since \EGD 's predecessor considered it, we will discuss it here\footnote{Although not going into too much details.}.
	It is called energy equation, but it describes evolution of the temperature.
	
	The energy equation that is implemented reads,
	\begin{align}
		\rho C_p \, \Dt{T} = -\nabla_{\Vek{x}} \cdot \Vek{q} + \mathcal{H} .		\label{eq:GoverningEq:TheNSEQ:ContEquations:Energy:fullTempEq}
	\end{align} 
	%
	A derivation can be found in \cite{gerya_2019}.
	%
	\\
	$C_p$ is the \emph{isobaric heat capacity}, its units are \si{\joule\per\kilogram\per\kelvin}.
	Multiplying it with density gives us the \emph{volumetric}, isobaric heat capacity, its units are \si{\joule\per\kelvin\per\cubic\meter}.
	Note that the volumetric heat capacity is composed of two properties, but is mostly considered as \emph{one}.
	It is clear from the above that multiplying it with a temperature gives us energy per unit volume.
	And if multiplied with a change of temperature, the term becomes an \emph{energy change per unit volume}. 
	
	On the right hand of equation \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Energy:fullTempEq}, we have the divergence of the heat flux $\Vek{q}$.
	It describe how much energy flows through a unit area per unit time.
	As it was the case for the stress, a constitutive relationship must be used to relate it to temperature and close the system.
	%
	$\mathcal{H}$ is the source term it described how energy is generated or consumed.
	
			\paragraph{Heat Flux}~\label{sec:GoverningEq:TheNSEQ:ContEquations:Energy:heatFlux}
			As constitutive relationship for the heat flux, we will use Fourier's Law, that reads as
			%
			\begin{align}
				q_{i} = -k \, \partial_{x_i} T .		\label{eq:GoverningEq:TheNSEQ:ContEquations:Energy:heatFlux:fourier}
			\end{align}
			%
			$k$ is the thermal conductivity, its units are \si{\watt\per\meter\per\kelvin}.
			It is similar to electrical conductivity, high values means that heat can move ``without much resistant'' through a media.
			The lower the value the better the ability of the material to isolate heat from its surrounding.
			The minus sign is needed, such that heat flows from warmer to colder regions.
			%
			\\
			The idea behind equation \eqref{eq:GoverningEq:TheNSEQ:ContEquations:Energy:heatFlux:fourier} is, that the larger the temperature gradient is, meaning more ``thermal pressure'' and the larger thermal conductivity is, the stronger the temperature transport will be.
			
			% END: 		Heat Flux
			%%%%%%%%%%%%%%%%%%
			
			
			\paragraph{Heat Sources}~\label{sec:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm}
			There are several ways for defining $\mathcal{H}$.
			%
			\\
			We will decompose the heating term into four contributions.
			\begin{align}
				\mathcal{H} = H_{s} + H_{a} + H_{r} + H_{L}			\label{eq:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm:heatDecomp}
			\end{align}
			
			\begin{description}
					\item[$H_s$ -- Shear Heating]
					This kind of heating is well known from our every day life, for example if we rub our hands, they get warm.
					This is caused by shear heating.
					It can be computed as
					\begin{align}
						H_s = \tau_{ij} \cdot \dot{\hat{\epsilon}}_{ij} 	\label{eq:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm:shearHeat}
					\end{align}
					%
					Note that $H_s \geq 0$ and is thus never a sink.
					%
					\\
					It is also worth noting, that the shear heat depends on the solution of the momentum equation and thus coupling them together.
					
					\item[$H_a$ -- Adiabatic Heating]
					This kind of heating is caused by expansion and compression of materials.
					If, for example, gas is expanded, it cools down and if compressed it heats up.
					It is defined as 
					\begin{align}
						H_a = T \, \alpha \, \Dt{P}			\label{eq:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm:adiabaticHeat}
					\end{align}
					%
					$\alpha$ is the thermal expansion coefficient defined as $\alpha = \frac{1}{\rho} \pdiff{T}{\rho}$.
					Like the shear heating, this term also depends on the solution of the stokes equation which further couples the two.
					
					\item[$H_r$ -- Radioactive Heating]
					This kind of heating is not so common in fluid dynamic but in geodynamic.
					It is the heat that is produced by the decaying of radioactive substances.
					It depends on the materials that are studied and on the time scales that are considered.
					
					\item[$H_L$ -- Lathent Heating]
					This is the heating that is produced by phase transitions.				
			\end{description}
			
			% END:		Heat sources
			%%%%%%%%%%%%%%%%%%%%

	% END:		Energy equation
	%%%%%%%%%%%%%%%%%%%%%%%
	
