%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Usage of Corello


\section{Usage and Operations of \Corello }~\label{sec:appendix:Corello:UsageOps}
In this section we will discuss some of \Corello 's internals.
We mainly focuses on the order of operations \Corello{} performs and how the program can be used.
Most of the behaviour of \Corello{} is controlled by the configuration file, which is discussed in section \ref{sec:appendix:Corello:Ini}.


	\subsection{\Corello 's Control Flow}~\label{sec:appendix:Corello:UsageOps:Flow}
	In figure \ref{fig:GoverningEq:UsedScheme:TheScheme:TheSteps} on page \pageref{fig:GoverningEq:UsedScheme:TheScheme:TheSteps}, we have shown the seven \emph{idealized} steps of the algorithm, but much of the details were omitted.
	We will now discuss the internal control flow of \Corello .
	%
	\\
	\Corello 's operation is split into three phases.
	
	
		\subsubsection{Phase $1$: Initialization}~\label{sec:appendix:Corello:UsageOps:Flow:init}
		In the first phase everything is initialized.
		
		At first the command line arguments are processed.
		The main task here is to determine the location of the configuration file.
		%
		Then the configuration file is loaded and a configuration \emph{object}, see section \ref{sec:appendix:EGD:util:ConfObj}, is created.
		%
		\\
		The supported arguments are discussed in section \ref{sec:appendix:Corello:UsageOps:CLI} on page \pageref{sec:appendix:Corello:UsageOps:CLI}.
		%
		The configuration file is discussed in \ref{sec:appendix:Corello:Ini} on page \pageref{sec:appendix:Corello:Ini}.
		
		
		The factory methods, see section \ref{sec:appendix:EGD:core:Step6:Factory}, are then used to create the core objects, see section \ref{sec:appendix:EGD:core}, which represents the $7$ steps of the method.
		To determine \emph{which} implementation is requested, the configuration object is used.
		It will also be used to preliminary configure the core objects.
		%
		\\
		After the core objects have been created, the dumper object, see section \ref{sec:appendix:EGD:util:Dumper}, is created as well.
		
		
		After the generation of the core objects the inspection starts.
		This is important, such that the core objects can further configure themself, which also involves the dumper.


		The inertial time, denoted as $\delta t^{(-1)}$, is initialized to the smallest allowed time step increment.
		However this value will be bootstrapped anyway later, see step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_9} of the simulation loop.


		The last task of this phase, is the execution of the zeroth step.
		This will generate the grid container and marker collection.
		Further the marker properties are initialized to their initial values.
		%
		After the step has finished, the initialization function of the dumper is called.
		
		% END:		INIT
		%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{Phase $2$: Simulation Loop}~\label{sec:appendix:Corello:UsageOps:Flow:Loop}
		The second phase is the actual simulation loop.
		%
		The loop is roughly equivalent to the idealized picture shown in figure \ref{fig:GoverningEq:UsedScheme:TheScheme:TheSteps}.
		However the description here, will involve much more details.
		%
		\begin{enumerate}
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_1m}
			At the beginning of each iteration, except iteration $0$, the inertial time is set to the time step increment from the previous iteration.
						
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_1}
			The grid properties, stored in the grid container, are updated.
			For this step $1$, see section \ref{sec:GoverningEq:UsedScheme:TheScheme:Step0_setup} on page \pageref{sec:GoverningEq:UsedScheme:TheScheme:Step0_setup}, of the scheme is executed.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_2}
			We solve the the momentum equations, which will generate the new velocity and the pressure fields.
			If the Navier--Stokes equations are solved, this also involves the fixed inertial time.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_3}
			Now the time step increment is set to the maximal allowed value.
			It will be restricted later.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_4}
			Now the maximal displacement value, see section \ref{sec:discretization:step6:TimeSteppingControl} for more on this subject, is chosen.
			During normal operations the value specified by the configuration file is used.
			However in the beginning of the simulation, a different value is used, see section \ref{sec:appendix:Corello:UsageOps:StartTime} on page \pageref{sec:appendix:Corello:UsageOps:StartTime} for more.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_5}
			Then the time step increment is restricted based on the marker displacement, see section \ref{sec:discretization:step6:TimeSteppingControl}.
			For this the maximal values, in magnitude, of the $x$ and $y$ velocities are determined.
			The time increment is then computed, such that the marker movements comply with the values from step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_4}.
			%
			\\
			This is done separately first for $x$ and then for $y$.
			This was discussed in section \ref{sec:discretization:step6:TimeSteppingControl} on page \pageref{sec:discretization:step6:TimeSteppingControl}.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_6}
			Step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_5} will only \emph{reduce} the time step increment, we must now check if the value has become too small.
			If this is the case, the time step increment will be set to the minimal allowed value.
			%
			\\
			This means that the marker displacement will be \emph{larger} than allowed.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_7p}
			The heating term is computed.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_7}
			Now the temperature equation is solved.
			%
			The discretization will use the time step increment from step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_6}. 
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_8}
			We again check if the time step increment is too large, but this time we consider temperature, see last point in section \ref{sec:discretization:step6:TimeSteppingControl}.
			If the absolute temperature change exceed a certain value, the time step is reduced.
			We assume that there is a linear relationship between temperature change and time.
	
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_8p}
			If we had to restrict the time step increment in step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_8}, we have to resolve the temperature equation again.
			But this time we must discretize the equation with the new, restricted, time step.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_9}
			This step is only executed the \emph{very} first time it is reached.
			The inertial time value is set to the current time step increment value.
			Then we will jump back to step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_1m}.
			%
			\\
			It is important that time step counter is \emph{not} incremented.
			It is like the iteration thus far has never happened.
			%
			The main intention of this step is to bootstrap a good initial value for $\delta t^{(-1)}$.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_10}
			Now we will decide if we have \emph{reached} the final iteration of the simulation.
			This happens either if we have performed the requested number of steps or the simulation has evolved to the final time.
			%
			See also section \ref{sec:appendix:Corello:UsageOps:Flow:Signals} on page \pageref{sec:appendix:Corello:UsageOps:Flow:Signals} for other factors that will terminate the loop.
			%
			\\
			It is very important that we only \emph{decided} if the loop should be exited, however it is not exited \emph{now}.
			The actual exiting is postponed to step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_16}.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_11}
			Now we will check if the conditions for performing a dump are fulfilled and dump the state if this is the case.
			See section \ref{sec:appendix:EGD:util:Dumper:Managing} for more.
			%
			\\
			As we will later see, there are several parameters that controls when a dump is performed.
			We will discuss them in section \ref{sec:appendix:Corello:UsageOps:DumpControll} in more detail.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_12}
			The grid properties which were computed by the solvers are mapped back to the markers.
			This performs step $4$ pf the scheme.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_13}
			The material model is called, which will update marker properties based on the new state of the simulation.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_14}
			The markers are advected, this is step $6$.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_15}
			Bookkeeping is performed, such as updating the time or the step counter.
			
			\item~\label{itm:appendix:Corello:UsageOps:Flow:Loop:s_16}
			If the simulation ended, see step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_10}, the loop is now exited.
			Otherwise we will jump to \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_1m} and start the next iteration.
			
		\end{enumerate}
			
		% END:		Sim Loop
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{Phase $3$: Finalizing}~\label{sec:appendix:Corello:UsageOps:Flow:End}
		In the last phase the simulation is finalized.
		%
		\\
		This involves calling the finalize function fo the dumper object, which will save the last marker state.
		However it will also print a small summary to the screen.
		
		% END:		Finalizing
		%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{Signals}~\label{sec:appendix:Corello:UsageOps:Flow:Signals}
		\Corello{} is also able to handle several signals\footnote
			{
				The following signals are handled, \texttt{SIGTERM}, \texttt{SIGINT}, \texttt{SIGUSR1}, \texttt{SIGUSR2}, \texttt{SIGTSTP} and \texttt{SIGQUIT}.
			}.
		%
		A signal is always interpreted as an exit request.
		Such a request is handled in step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_10} of the simulation loop.
		This allows a graceful exit and ensures that the dump file is not damaged and can be read later.
		
		% END:		Signals
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	% END:		Corello Control Flow
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
		
	\subsection{Dumping Control}~\label{sec:appendix:Corello:UsageOps:DumpControll}
	We will now further elaborate on point \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_11} from the control loop, where the dump was performed.
	%
	We distinguish between two kinds of dumps, regular dumps, which can be influenced by the user and unusual dumps.
	
			\paragraph{Unusual Dump}~\label{sec:appendix:Corello:UsageOps:DumpControll:unusualDump}
			Such dumps can \emph{not} be influenced by the user and happens in special situations.
			For example a dump is initiated in the last iteration\footnote{If the iteration ends is determined in step \ref{itm:appendix:Corello:UsageOps:Flow:Loop:s_10} of the simulation loop.}.
			%
			\\
			Other such situations include the \emph{super restricted} or the \emph{ramp} phase, see section \ref{sec:appendix:Corello:UsageOps:StartTime} for more.	
			% END:		Unusual Dump
			%%%%%%%%%%%%%%%%%%%%%%%%%
			
			
			\paragraph{Regular Dump}~\label{sec:appendix:Corello:UsageOps:DumpControll:regDump}
			These kinds of dump can be controlled by the user.
			For some situations the time step, of the situation, has to be rather small, for example to guarantee stability.
			%
			However storing the simulation with such a resolution may not be desirable\footnote
				{
					For example, the process of interest happens on much larger time scales or storage capacities are limited.
				}.
			So it is important to limit the number of dumps, \Corello{} supports two mechanism for this.
			They work by influencing the conditions at which a dump is be performed.
			%
			\begin{description}
				
				\item[Time Based] 
				In this mode we use the time to determine if a dump is needed.
				The user specifies a dump interval, lets say $\SI{1}{\milli\second}$.
				In each step \Corello{} checks how much the simulation has advanced since the last dump.
				If this time is larger than the specified interval, a dump is performed.
				
				\item[Step Based]
				In this mode the dump frequency is specified in terms of steps that are performed and not simulated time pasage.
											
			\end{description}
			%
			It is also possible to use both at the same time.
			In this situation the dump is issued whenever one condition is meet.
			
			% END:		Regular Dump
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% END:		Dump Control
	%%%%%%%%%%%%%%%%%%%%%%%
	
	
	\subsection{Time Step Control II}~\label{sec:appendix:Corello:UsageOps:StartTime}
	In section \ref{sec:discretization:step6:TimeSteppingControl} on page \pageref{sec:discretization:step6:TimeSteppingControl} we have described how the time step is restricted.
	The main idea of that restriction was, that the displacement of markers should be limited to a certain value, expressed in terms of grid spacing.
	Which effectively will limit the CFL number.
	
	However in the beginning of the simulation additional rules are used to further restrict the simulation.
	There are two consecutive phases at the beginning for this.
	%
	\begin{description}
		
		\item[Super Restricted Phase] 
		In this phase the maximal value for the displacement is set to a very small number, in the current implementation this value is $0.0001$.
		%
		\\
		It will lead to a very small time step and is intended to equilibrate the system.
		
		\item[Ramp Phase]
		The second phase is the so called \emph{ramp} phase.
		Here the displacement value is successively increased until it reaches the specified value.
		The formula is given as
		\begin{align}
			\gamma_{i}^{\text{eff}} = \frac{\gamma_{i} - \gamma^{\text{SR}}}{n_\text{R}} \cdot \left( n - n_\text{SR} \right) + \gamma^{\text{SR}}. 		\label{eq:appendix:Corello:UsageOps:StartTime:Ramp:maxMove}
		\end{align}
		%
		Above $n_\text{R}$ is the number of steps in the ramp phase, $n_\text{SR}$ in number of steps in the super restricted phase and $n$ denotes the current time step.
		$\gamma^{\text{SR}}$ is the maximal displacement in the super restricted phase and $\gamma_{i}$ is the normal displacement, specified by the user.
		
	\end{description}
	%
	It is important to note, that the rules does not influences the time step increment directly.
	Instead the time step value is restricted indirectly, by limiting the movement of markers.
		
	% END:		Time step in beginning
	%%%%%%%%%%%%%%%%%%%%%%	



	\subsection{Command Line Options}~\label{sec:appendix:Corello:UsageOps:CLI}
	\Corello{} is primarily controlled by the configuration file.
	However some command line options are provided.
	%
	\begin{description}
		
		\item[\texttt{-{}-conf FILE}]
		This flag is used to specify the location of the configuration file that should be used.
		It \emph{must be given} otherwise an error will be generated.
		
		\item[\texttt{-{}-dir DIR}]
		By default \Corello{} stores the dump file in the current folder.
		With this option it is possible to change the folder\footnote
			{
				If the folder does not exist it will be created.
			}
		in which the dump file should be created.
		
		\item[\texttt{-{}-force}]
		In the case the dump file already exists an error will be generated.
		This behaviour ensures that simulation data is not accidentally overwritten.
		If this flag is specified, the file will be deleted, however \Corello{} will wait a few seconds before that.
		
	\end{description}

	% END:		CLI
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
