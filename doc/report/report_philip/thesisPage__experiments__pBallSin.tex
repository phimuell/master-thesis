%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This is the pulled ball sine test

\section{Pulling an Object, Second}~\label{sec:experiments:pBallSin}
In this section we are again studying the behaviour of an object, that is pulled trough a media.
These experiments are similar to the ones we have studied in section \ref{sec:experiments:constBall} on page \pageref{sec:experiments:constBall}, but there are a few differences.
%
Previously we were focused on, what is the best way of pulling an object through a media.
%
In these experiments we are focusing on if the system acts \emph{physically}.
Additionally, the object does not move at constant speed any more, instead its velocity varies with time.


	\subsection{General Setting}~\label{sec:experiments:pBallSin:genSetting}
	Before we begin, we will describe the general setting of these experiments.
	%
	\begin{figure}[h]
		\centering
		\begin{minipage}{\columnwidth}
			\input{./Tikz/thesisPage__experiments/pBallSin/generalSetting.tex}
			\caption
			{
				Illustration of the general setting in the \pBallSin{} family of experiments.
			}
			\label{sec:experiments:pBallSin:genSetting:sketch}
		\end{minipage}
	\end{figure}
	%
	The situation is illustrated in figure \ref{sec:experiments:pBallSin:genSetting:sketch}, and looks similar to the \constBall{} setting, but there are some differences.
	First of all, the box is larger, its height is $\SI{2}{\meter}$ instead of just one meter and it is also $\SI{50}{\centi\meter}$ longer.
	%
	\\
	But the main difference is the velocity we are imposing on the ball.
	Instead of a constant velocity, it now depends on time $t$ as 
	%
	\begin{align}
		\Fkt{v_{x}}{t} = -\alpha \cdot \left( 1 + \Sin{\frac{2 \pi}{\omega} \left( t - t_{0} \right) } \right)	,	\label{eq:experiments:pBallSin:genSetting:vx}
	\end{align}
	%
	where $t_0$ is the time at which the simulation starts, it is usually zero.
	$\alpha$ describes how fast the ball moves, and $\omega$ how strong it shakes back and forth, we require $\alpha > 0$ and $\omega > 0$.
	Because of the minus in front of \eqref{eq:experiments:pBallSin:genSetting:vx} and the ``$+1$'', the velocity will always be negative and the ball will move towards the left.
	%
	As we have done it before, we will consider the \emph{absolute speed} of the ball in the following discussion, ignoring the sign.
	
	$\alpha$ and $\omega$ are parameters that describe the system.
	As default value we have selected
	%
	\begin{align*}
		\alpha = 2.3 ,		\qquad\qquad
		\omega = 0.5 .		\label{sec:experiments:pBallSin:genSetting:param:omega}	
	\end{align*}
	
	The viscosities for the ball and the media are the same in all systems,
	%
	\begin{align*}
		\eta_{B} = \SI{1e+8}{\pascal\second} ,						\qquad\qquad
		\eta_{F} = \SI{1.0016e-2}{\pascal\second} .
	\end{align*}
	%
	However densities are not fix, but they are chosen such that the ball is $3.2$ times more dense than the fluid.
	Thus density is an additional system parameter.
	
	
	In this work, we have studied four different systems.
	%
	\begin{description}
		
		\item[\pBallSinSin ]
		This is the base case.
		Here the fluid has a density of $\rho_{F} = \SI{0.1}{\kilogram\per\cubic\meter}$, and for the ball we have $\rho_{B} = \SI{0.32}{\kilogram\per\cubic\meter}$.
		The Reynolds number of this configuration is $\approx 18$.
		
		\item[\pBallSinRet ]
		As its name indicates, the Reynolds number is $\approx 180$.
		The density of the fluid was increased to $\rho_{F} = \SI{1}{\kilogram\per\cubic\meter}$, and for the ball we have $\rho_{B} = \SI{3.2}{\kilogram\per\cubic\meter}$.
		
		\item[\pBallSinOrg ]
		This is the original setting, that uses the real densities.
		Which means $\rho_{F} = \SI{1000}{\kilogram\per\cubic\meter}$ and $\rho_{B} = \SI{3200}{\kilogram\per\cubic\meter}$.
		This increases the Reynolds number to $\approx 180'000$.
		
		\item[\pBallSinTwo ]
		The densities are the same as in \pBallSinSin , but we have selected a different value of $\omega$.
		Instead of the usual parameter we have used $\omega = 0.25$.
		The result is, that acceleration and deceleration phases become shorter, and the ball ``shakes'' stronger.
		
	\end{description}
	%
	The Reynolds numbers stated above are the \emph{maximum} values in these systems.
	
	% END:		General setting
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	\subsection{Objective}~\label{sec:experiments:pBallSin:Goal}
	As we have learned in section \ref{sec:experiments:linMom:linMod:slowDown} on page \pageref{sec:experiments:linMom:linMod:slowDown}, if we accelerate a body inside a media, we have to accelerate a portion of the media as well.
	%
	We have also seen, that this portion of the media, can be seen as an added or virtual mass $\hat{m}$, that depends on the geometry of the object and the density of the fluid.
	In case of a cylinder\footnote
		{
			As we have noted several times before, the ``balls'' and ``discs'' are actually cylinders.
			But for several reasons we call them balls.
		} 
	we can calculate it analytically and obtain 
	%
	\begin{align}
		\hat{m} = \rho_{F} \, V_{K}	.		\label{eq:experiments:pBallSin:Goal:addedMass}
	\end{align}
	%
	In the above equation $V_{K}$ is the volume of the cylinder, we assume a height of one unit, and $\rho_{F}$ the density of the media.
	
	However, here we are studying something different, than in section \ref{sec:experiments:linMom:linMod:slowDown}.
	There we only \emph{related} a change of the momentum of the ball to a force that is exerted on the fluid.
	We have not studied the \emph{cause} of this, which is what we are doing now.
	
	Since we are pulling the ball according to \eqref{eq:experiments:pBallSin:genSetting:vx} we know the additional force, that has to be used to move the ball with a certain velocity.
	This additional force is given as 
	%		
	\begin{align}
		F_{f} &= \hat{m} \cdot \diff{v_{x}}{t} =  \rho_{F} \, V_{K} \cdot \diff{ -\alpha \, \left( \Sin{\frac{2 \pi}{\omega} \left( t - t_{0} \right) } + 1 \right) }{t} \nonumber  \\
				&= -\frac{2 \alpha \pi}{\omega} \Cos{ \frac{2 \pi}{\omega} \left( t - t_{0} \right)  }	\label{eq:experiments:pBallSin:Goal:Ff}
	\end{align}
	
	
		\subsubsection{Measuring}~\label{sec:experiments:pBallSin:Goal:Measuringoal}
		The acceleration of the body will lead to a ``collision'' with the fluid and will generate a pressure on the ball's surface.
		This means that \eqref{eq:experiments:pBallSin:Goal:Ff} is actually a pressure.
		
		We will measure this force in two ways.
		%
		In the \emph{indirect} way, we will measure the change in the fluid's momentum.
		This method was also used in \ref{sec:experiments:linMom:linMod:slowDown}.
		%
		\\
		In the second method, we will use the convex hull, see section \ref{sec:experiments:methodology:calculation:ConvexHull} on page \pageref{sec:experiments:methodology:calculation:ConvexHull}, as an approximation of the surface and \emph{directly} integrate pressure over it.
		%
		As we have noted before, this technique gives very bad results.
		
		% END:		Measuring
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
	% END: 		Objhective
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	\subsection{Results}~\label{sec:experiments:pBallSin:Results}
	We will now study the results we have obtained for the different settings.
	
		\subsubsection{\pBallSinSin }~\label{sec:experiments:pBallSin:Results:Sin}
		Here we are studying the \pBallSinSin{} setting.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__pBallSin/pBallSin__pressForceSin.png} 
				\caption
				{
					Pressure forces measured in the \pBallSinSin{} setting.
					Solid lines are obtained indirectly by measuring the fluid momentum, dashed lines by integrating over the convex hull.
					The cyan line is the expected force signal.
				}
				\label{fig:experiments:pBallSin:Results:Sin:pressForce}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:pBallSin:Results:Sin:pressForce} we see the pressure forces in different system and measured with different methods.
		%
		\\
		The solid lines are obtained by differentiating the fluid momentum with respect to time (direct).
		The dashed lines are computed by integrating pressure over the convex hull (indirect).
		
			\paragraph{Indirect Way}~\label{sec:experiments:pBallSin:Results:Sin:Indirect}
			As we see in the plot, we have almost a perfect match, and we also see that the lines are smooth, and we only have small fluctuations.
			We would like to emphasize, that the lines were not smoothed.
						
			% END:		Indirect
			%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
			
			\paragraph{Convex Hull}~\label{sec:experiments:pBallSin:Results:Sin:directCH}
			The measurements obtained by integrating over the convex hull, do not match the expected values, as good as we have seen it for the indirect way.
			To be honest, they are terrible.
			%
			What is worth noting is, that the \NoBC{} method is ``approximately'' right.
			For example it is able to capture the periodic nature of the force, but it has some other problems.
			%
			Especially from the other systems, except \StokesBC , we see, that the mean value of the curves is not zero, instead they seam to be shifted downwards.
			%
			\\
			For the \NoBC{} method we also see something more.
			When we would move the curve upwards, such that its mean is zero, we would see that the amplitude of the force is too large.
			It would over and under shoot, the expected value.
			%
			Another thing, that becomes clear then is, that pressure acts too slowly.
			The onset of the pressure increase and decrease happens too late, as if the measured signal has a phase shift.
			
			We will now try to explain some of this behaviour.
			%
			\begin{figure}[h]
				\centering
				\begin{minipage}{\columnwidth}
					\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__pBallSin/pBallSin__pressDist_sinFull.png} 
					\caption
					{
						Pressure distribution in \pBallSinSin , velocity is imposed by \FullImp .
						Value range is clamped to \SIrange{-3}{3}{\pascal}.
					}
					\label{fig:experiments:pBallSin:Results:Sin:pressForce:pressDistFull}
				\end{minipage}
			\end{figure}
			%
			In figure \ref{fig:experiments:pBallSin:Results:Sin:pressForce:pressDistFull}, we see the pressure distribution in the \pBallSinSin{} model, when the \FullImp{} method is used.
			For visualizing purposes we have clamped the pressure values to the range \SIrange{-3}{3}{\pascal}.
			%
			We can see that the centre of the ball contains two large pressure spikes.
			We have seen this already for the \constBall{} experiments, in section \ref{sec:experiments:constBall:Similarities} on page \pageref{sec:experiments:constBall:Similarities}, but not in that detail.
			These spikes are an artefact that is produced by the manipulation of the grid.
			%
			\\
			\begin{figure}[h]
				\centering
				\begin{minipage}{\columnwidth}
					\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__pBallSin/pBallSin__pressDist_sinNoBC.png} 
					\caption
					{
						Pressure distribution in \pBallSinSin , but velocity is imposed by \NoBC .
						Value range is clamped to \SIrange{-3}{3}{\pascal}.
					}
					\label{fig:experiments:pBallSin:Results:Sin:pressForce:pressDistNoBC}
				\end{minipage}
			\end{figure}
			%
			In figure \ref{fig:experiments:pBallSin:Results:Sin:pressForce:pressDistNoBC} we again show the pressure distribution in a \pBallSinSin{} model, but this time the \NoBC{} method is used.
			We do not see the spikes from before.
			%
			If we compare the two figures, we also see, that we have additional pressure artefacts for the \FullImp{} method.
			They are located near the surface, but on the \emph{inside} of the ball.
			%
			\\
			However if we focuses on the pressure field \emph{outside} the ball, we conclude that they look very similar to each other.

			It is quite intuitive, that the surface approximation, given by the convex hull, is very tight and the extension of the ball\footnote{Or at least the \emph{influence} of its markers.} is probably larger.
			In the end this means, that the approximation we are using, can be considered to lie \emph{inside} the ball.
			%
			Thus if we are integrating over it, the value will be affected by the internal artefacts, near the surface.
			%
			\\
			When using the \NoBC{} method these artefacts are not present, however there we have a different problem.
			Since the surface approximation is slightly inside the object, the ball will absorbs and distorts the pressure signal on the real surface.
			Thus we will only measure a distorted signal on the convex hull.
			
			% END:		COnvex hull
			%%%%%%%%%%%%%%%%%%%%%%%%%%%
		\FloatBarrier
		% END:		SIN
		%%%%%%%%%%%%%%%%%%%%%%
	
	
		\subsubsection{\pBallSinRet }~\label{sec:experiments:pBallSin:Results:Re3}
		In this section we are studying systems with higher densities, thus the Reynolds number is $\approx 200$.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__pBallSin/pBallSin__pressForceRe3.png} 
				\caption
				{
					Pressure forces measured in the \pBallSinRet{} setting.
					Solid lines are obtained indirectly by measuring the fluid momentum, dashed lines by integrating over the convex hull.
					The cyan line is the expected force signal.
				}
				\label{fig:experiments:pBallSin:Results:Re3:pressForce}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:pBallSin:Results:Re3:pressForce}, we are showing the measuring results.
		Again solid lines are obtained indirectly and dashed lines directly.
		The cyan line is the expected value.
		%
		\\
		The results are very similar to the ones we have already seen for the \pBallSinSin{} case, so we are not discussing it further.
		%
		In this plot we can see even more clearly, that the direct measurement technique gives values which are too low.
		But we also see, that they seam to have a periodic behaviour too.
		
		We would also like to point out, that the \NoBC{} system has improved.
		Before its amplitude was too large, however now the amplitude is a bit too small, but still shifted down.
		%
		\\
		We think that the stronger forces, that now acts on the ball, are responsible for this.
		They penetrate the outer layer of the ball more efficiently and are able to reach deeper levels.
		This results in a less distorted pressure signal on the convex hull.
		
		% END:		RE3
		%%%%%%%%%%%%%%%%%%%%%%%%
	
		
		\subsubsection{\pBallSinOrg }~\label{sec:experiments:pBallSin:Results:Org}
		Here we have the original setting, this means we have a very large Reynolds number.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__pBallSin/pBallSin__pressForceOrg.png} 
				\caption
				{
					Pressure forces measured in the \pBallSinOrg{} setting.
					Solid lines are obtained indirectly by measuring the fluid momentum, dashed lines by integrating over the convex hull.
					The cyan line is the expected force signal.
				}
				\label{fig:experiments:pBallSin:Results:Org:pressForce}
			\end{minipage}
		\end{figure}
		%
		As it was before, solid lines in figure \ref{fig:experiments:pBallSin:Results:Org:pressForce} were obtained indirectly, by differentiating the fluid's momentum and the dashed lines were obtained by integrating pressure over the convex hull.
		%
		\\
		As it was the case before, the indirect method is able to capture the force accurately.
		
		The results obtained for the \FullImp{} and \NoR{} methods looks still very similar to what we have seen before, but the \NoBC{} method has improved.
		%
		The measured pressure is still more ``faithful'' than accurate, but more details are captured.
		As usual the method is not able to accurately capture the onset of of the increasing phase.
		However it is able to capture \emph{both} turning points, \emph{i.e.} it does not really under or over shoot and is no longer shifted down.
		
		As we have noted above, we think that the improvement, we saw in the \NoBC{} system, is related to the even larger forces, that are present in these systems.
		%
		They penetrate the outer layer of the ball deeper and can transmit the pressure signal with less distortion to the lower levels, such as the convex hull.
		Thus we are able to measure the signal more clearly there.
		
		
		% END:		Org
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


		\subsubsection{\pBallSinTwo }~\label{sec:experiments:pBallSin:Results:Sin2}
		Here we are presenting the results for the \pBallSinTwo{} version of the experiment.
		In this setting the physical parameters are the same as in \pBallSinSin , but we have used $0.25$ for $\omega$.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__pBallSin/pBallSin__pressForceSin2.png} 
				\caption
				{
					Pressure forces measured in the \pBallSinTwo{} setting.
					Solid lines are obtained indirectly by measuring the fluid momentum, dashed lines by integrating over the convex hull.
					The cyan line is the expected force signal.
				}
				\label{fig:experiments:pBallSin:Results:Sin2:pressForce}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:pBallSin:Results:Sin2:pressForce} solid lines are again obtained by differentiating the momentum of the fluid and the dashed lines by integrating pressure over the convex hull.
		%
		Since the physical parameters are the same as in \pBallSinSin\footnote
			{
				This is not entirely true.
				The physical parameters and thus the added mass, are the same.
				However the force is also influenced by the derivative of velocity.
				For our choice we see that the magnitude is proportional to $\frac{\alpha}{\omega}$.
				Since we decreased $\omega$ but kept $\alpha$ fix, we have increased the force.
				%
				\\
				As we have realized that also $\alpha$ should have been changed we were unable to do so due to time constrains.   
			}.
		It is most interesting to compare the two systems with each other.
		%
		\\
		The basic traits of the figure still resembles the one we have seen for the usual \pBallSinSin{} system.
		So we still see the undershoot after decaying.
		%
		We are not sure how to interpret these results.
		\FloatBarrier
		% END:		Sin2
		%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
		\subsubsection{\StokesBC }~\label{sec:experiments:pBallSin:Results:StokesBC}
		We have simulated systems in which we used the \StokesBC{} method.
		In these systems, we have only solved the Stokes equations, ignoring inertial effects.
		The movement of the ball, was imposed by manipulating the grid.
		%
		\\
		Above, we have shown the results for these systems, but we have not discussed them yet.
		
		The behaviour of \StokesBC{} systems was consistent for all four different configurations.
		%
		The force measured indirectly, indicates that even these systems behave physically correct.
		Which is probably due to continuity.
		%
		\\
		But measuring the force directly, by integrating over the convex hull, shows that the value is basically zero for all of them.
		
		
		Above we have learned, that the convex hull, is not so reliable.
		%
		Never the less, we have actually expected to measure a value of zero for \StokesBC{} systems.
		%
		In section \ref{sec:experiments:pBallSin:Goal}, we have explained, that the force is caused by a pressure.
		That is generated by the collision of ball and fluid particles, this is only possible because the particles (markers) have a velocity memory.
		%
		Since in \StokesBC{} systems markers do not have a velocity memory, there is no real collision that could generate the pressure.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__pBallSin/pBallSin__pressDist_sinOrgStokes.png} 
				\caption
				{
					Pressure distribution in \pBallSinOrg , velocity is imposed by \StokesBC , thus inertial effects are \emph{ignored}.
					Value range is clamped to \SIrange{-3}{3}{\pascal}.
				}
				\label{fig:experiments:pBallSin:Results:StokesBC:pressDistOrg}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:pBallSin:Results:StokesBC:pressDistOrg} we see the pressure distribution of the \pBallSinOrg{} model when the \StokesBC{} method is used.
		We have clamped the values to \SIrange{-3}{4}{\pascal}, we would like to say, that when other methods are used, we observe pressures in the order of $\SI{10000}{\pascal}$ for the \pBallSinOrg{} configuration.
		%
		Inside the ball we see again some anomalies.
		But \emph{outside} the ball, we do not observe any meaningful pattern, such as we have seen in figure \ref{fig:experiments:pBallSin:Results:Sin:pressForce:pressDistFull} and \ref{fig:experiments:pBallSin:Results:Sin:pressForce:pressDistNoBC}.
		
		This is a very important observation.
		Because it tells us, that inertial effects, are indeed able to generate a pressure distribution on the surface of an object.

		% END:		StokesBC
		%%%%%%%%%%%%%%%%%%%%%%%%%%

	\FloatBarrier
	% END:		Results
	%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	\subsection{Conclusion}~\label{sec:experiments:pBallSin:Conclusion}
	After we have discussed the results, we would like to summarize the most important points we learnt from these experiments.
	%
	The goal of this family of experiments was, to determine if a body that is moved through a fluid interacts in a physical correct way with it.

	
	From the indirect method we learned, that fluid--body coupling works correct.
	We have seen something similar before, when we have analysed the slow down of the \LinMom{} system.
	But here we had more control over it and can now be sure, that the added mass, at least for the case of cylinders, is captured correct.
	%
	\\
	However our results for the \StokesBC{} method suggest, that this coupling is mostly due to continuity, since it is also present there.
	
	
	Our data on the generation of pressure, due to inertial effects, is not extremely solid.
	But we think, that there is a high probability, that the method is able to capturing them correctly.
	%
	From the different plots of the pressure distribution, we saw before, we can conclude, that inertial effects have indeed an influence on the pressure.
	However due to our inability of measuring surface pressure, we are not able to confirm, that they have the correct values.
	%
	But we take the result of \pBallSinOrg{} as a strong indication, that they are indeed captured correctly.
	

	We have seen that the convex hull, is not an accurate reconstruction of the body's surface.
	However in order to compute some quantities, such as friction, it is mandatory to have a meaningful surface approximation.
	%
	\\
	We thus suggest more research to find a better surface approximation.
	
	We also suggest to test different geometries.
	As an example we could simulate a square box instead of a ball.
	
	% END:		Conclusion
	%%%%%%%%%%%%%%%%%%%%%%%%%
	