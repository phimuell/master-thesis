%%%%%%%%%%%%%%%%%%%%%%%%%%
% in this file we will present the step 1 in the scheme

\section[Step $1)$]{Step $1)$ -- Mapping Markers to the Grid}~\label{sec:discretization:step1}
In this step we will discuss how we can map properties from the markers to the grid nodes.
For the discussion we will assume that the grid nodes are given by $\mathcal{X}^{(g)} = \Set{x_{k}^{(g)} }_{k = 1}^{N_x}$ and $\mathcal{Y}^{(g)} = \Set{y_{k}^{(g)} }_{k = 1}^{N_y}$.

On some grids it can be possible to compute values for ghost nodes, which is not very useful, since ghost nodes are \emph{never} used.
Thus we will simply assign zero to each ghost node.
This is only a convention, which we decided to use here.

As it was said before, there are several ways off performing this step.
We will only discuss the one that was used here.
%
For this work, we adapted the methods outlined in \cite{gerya_mic} and \cite{gerya_2019}, however we will handle some quantities differently.
There a two step procedure is used for some quantities.
In a first step the quantity is mapped to a different grid.
We will then use these values, to compute the values on the final grid.
In the majority of cases this involves some kind of average\footnote
	{
		As an example, assume you need density on the cell centre nodes.
		You first interpolate them to the basic nodal points.
		In order to get the value at the centre point you average the values of the four surrounding points.
		%
		\\
		However for some quantities it is better to use a different average.
		For example if viscosity is interpolated, it is better to compute the \emph{harmonic} average instead of the arithmetic average.
	}.
%
\\
In the present work we will not do this.
Instead we will always directly interpolate the quantities to the requested grid.

In the previous chapter we have discussed, that the markers can be seen as a finite set of randomly selected fluid parcels.
So the mapping procedure should reflect this, which in turn suggests some kind of statistical average process.
%
\\
A first observation is, that markers which are \emph{closer} to a node, should have a higher influence on it than markers that are far away.
So the \emph{weight} or \emph{influence} marker $m$ should have on grid node $(i,j)$, should depend on \emph{some} kind of distance between them.


	\subsection[Area of Dependency]{The Area of Dependency or the Set of Influential Markers }~\label{sec:discretization:step1:areaOfDependency}
	%
	\begin{figure}[h]
		\centering
		\begin{minipage}{.5\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__discretisation__step1/Step1_mappingToGrid.png} 
			\caption
				{
					Schematic depiction of the domain of dependency (DoD).
					Dashed box shows ``local mode''.
					%
					Taken from \cite{gerya_2019}, Fig. 8.8.
				}
			\label{fig:discretization:step1:areaOfDependency:MarkerSetting}
		\end{minipage}
	\end{figure}
	%
	In figure \ref{fig:discretization:step1:areaOfDependency:MarkerSetting}, a generic situation is shown, red squares are markers and the blue ones are grid nodes.
	Before we continue, the \emph{area of dependency} of a node must be defined, which we will denote as $\Gamma_{(i, j)}$. 
	This domain indicates the spatial region, in which a marker can have an influence on the node, and is thus considered for the interpolation.
	%
	Usually this domain involves the four cells that surround a node, as it is shown in the figure above.
	%
	\\
	$\Gamma_{(i, j)}$ can be seen in two ways, the first one was used before, where we have emphasized its geometrical extension.
	However a much more useful view, is to see it as the set of markers, that contribute to the value of the node.
	In this case we can define it as 
	\begin{align}
		\Gamma_{(j, i)} := \Set{m \in \mathcal{M} \, \left\vert \, x_{j - 1}^{(g)} < x_m < x_{j + 1}^{(g)},\; y_{i - 1}^{(g)} < y_m < y_{i + 1}^{(g)} \right.} ,  \label{eq:discretization:step1:areaOfDependency:defIndex}
	\end{align}
	where $\mathcal{M}$ is the set of all marker indices.
	If $(i,j)$ refers to a ghost node the set is empty.
	%
	\\
	We would like to point out, that depending the \emph{spatial extension} of the domain, a marker can be in multiple sets.
	
	
		\paragraph{Different Areas of Dependencies}~\label{sec:discretization:step1:areaOfDependency:DifferentForms}
		In some situations, it can be advisable, to consider a smaller \emph{area of dependency}.
		%
		One popular choice is the so called \emph{local} mode.
		In that choice the area extends half a grid wide into each direction, as it is indicated by the dashed line in figure \ref{fig:discretization:step1:areaOfDependency:MarkerSetting}.
		%
		\\
		In this mode a marker will only contribute to at most one node, the one\footnote 
			{
				It is possible that a marker will not contribute to any (physical) node.
				This is the case if the nearest node is a ghost node.
			}
		that is nearest.
		
		However it is also possible to change the geometry of the domain completely.
		For example we have always used rectangular domains, but it is also possible to use circular domains.
		However we have not studied the influence of such modifications.
		  
		% END:		Different forms
		%%%%%%%%%%%%%%%%%%%%%%
		
	% END: 		Area of dependency
	%%%%%%%%%%%%%%%%%%%%%%%%%


	\subsection{Averaging the Markers}~\label{sec:discretization:step1:average}
	Now we will describe how the marker interpolation is done.
	However we will not yet describe how the weights are computed, instead we will just assume that we already have \emph{some} weights.
	We will discuss this issue in the next section, \ref{sec:discretization:step1:weights}.
	
	We again consider figure \ref{fig:discretization:step1:areaOfDependency:MarkerSetting}.
	Our goal is to compute $B_{i, j}$ which is the value of the $B$ property on grid node $(i, j)$.
	%
	For the value of the $B$ property that marker $m$ is carrying we write $B_m$.
	The weight marker $m$ has towards node $(i,j)$, is denoted as $\omega_{m}^{(i,j)}$.
	%
	This leads to the following formula for computing the value
	%	
	\begin{align}
		B_{i, j} = \frac{ \sum_{m \in \Gamma_{(i, j)}} \omega_{m}^{(i, j)} \, B_{m} }{ \sum_{m \in \Gamma_{(j, i)}} \omega_{m}^{(i, j)} }. \label{eq:discretization:step1:average:nodeVal}
	\end{align}
	%
	The above formula can be seen as a some kind of average process.
	In that view $B_{m}$ is a \emph{realization} of the random variable $B_{i,j}$, and its \emph{relative probability weight} is given by $\omega_{m}^{(i,j)}$.
	The denominator in \eqref{eq:discretization:step1:average:nodeVal} is needed to ensure that the weights are a proper probability that sums to $1$.
	
	% END: 		Averaging
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
	
	
	\subsection{Computing the Weights}~\label{sec:discretization:step1:weights}
	As we have discussed above, marker weights should decay with distance, meaning markers that are near towards the node, should have more influence on the node than markers that are far away.
	In the following, $\Gamma_{(i, j)}$ will denote the usual DoD, where the markers in the four surrounding cells are considered.
	However the discussion here can also be used in \emph{local mode}.
	
	A very simple choice is a weight that resembles bilinear interpolation.
	In this case the weights are given by 
	\begin{align}
		\omega_{m}^{(i,j)} = \left(1 - \frac{\Delta x_{m}}{\Delta x} \right) \times \left(1 - \frac{\Delta y_{m}}{\Delta y} \right) . \label{eq:discretization:step1:weights:biLin}
	\end{align}
	%
	The above equation has some very important particularities, that we will discuss next.
	The first point is, that $\Delta x_{m}$ is the \emph{absolute} distance between marker $m$ and node $(i,j)$, so 
	\begin{align*}
		\Delta x_m := \Abs{x_{j}^{(g)} - x_m} 
			\qquad \text{ and } \qquad 
		\Delta y_m := \Abs{y_{j}^{(g)} - y_m} ,
	\end{align*}
	where $x_m$ and $y_m$ are the coordinates of marker $m$ and $x_{j}^{(g)}$ and $y_{j}^{(g)}$ are the coordinates of the associated node.
	%
	\\
	$\Delta x$ is the grid spacing, however care must be taken in the case of an irregular spaced grid.
	In that case, $\Delta x$ refers to the width of the cell the marker is located in.
	Thus for $\Delta x$ we have
	\begin{align*}
		\Delta x := 
			\left\{
				\begin{array}{lr}
					x_{j+1}^{(g)} - x_{j}^{(g)}, & \text{if } x_{m} > x_{j}^{(g)} \\
					x_{j}^{(g)} - x_{j-1}^{(g)}, & \text{if } x_{m} < x_{j}^{(g)}
				\end{array}
			\right. .
	\end{align*}
	%
	For $\Delta y$ the same is true.
	
	
		\subsubsection{Modified Weights}~\label{sec:discretization:step1:weights:modified}
		Using bilinear weights works well for most properties, however sometimes this is not good enough.
		Especially quantities that are supposed to be conserved, are known to cause problems without further modification.
		
		However it turns out, that just modifying the weights will solve the problem.
		%
		In case of temperature the weights are multiplied by the volumetric, isobaric heat capacity, $\rho C_p$ of the marker.
		This means instead of the normal weight we use the weight that is given as $\hat{\omega}_{m}^{(i,j)} = \left(\rho C_p \right)_{m} \, \omega_{m}^{(i,j)}$.
		%
		Roughly speaking, instead of simply interpolate temperature, thermal energy per unit volume is interpolated.
		This is however, not entirely correct, because $\rho C_p$ will be removed by the normalization step, but it helps to understand the modification.
		
		For interpolating velocity a similar modification should be used.
		In this case the weights are multiplied by the marker's density, giving the modified weight $\hat{\omega}_{m}^{(i,j)} = \rho \, \omega_{m}^{(i,j)}$.
		Again it can roughly be seen as interpolating momentum instead of bare velocity.
				
		% END:		Modified Weights
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	% END:		Weight
	%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	\subsection{Boundary Conditions}~\label{sec:discretization:step1:BC}
	There is a different aspect that we have to consider.
	As we have pointed out before, we also will map properties from the markers to the grid, which are the result of the two solver steps\footnote
		{
			For this work it means velocity.
			But it would technically also includes temperature and pressure.
		}.
	%
	However these properties are subject to some boundary conditions.
	%
	\\
	As we will later see, \emph{grid properties} that are computed by the solver, will fulfil them naturally.
	But the \emph{grid properties} that are generated in this step will most likely not fulfil them, especially since they got advected in the $6^{th}$ step of the previous iteration.
	
	Since these conditions have to hold all the time, we have to fix the generated properties, such that the comply with the conditions.
	We can do this by applying the discrete boundary conditions backwards.
	
	% END: BC
	%%%%%%%%%%%%%%%%%%%%%%%%

	\FloatBarrier

