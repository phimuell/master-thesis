%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In this file we will discuss the implementation of the heat conservation.

\section[Step $3)$]{$3)$ -- Solving the Temperature Equation}~\label{sec:discretization:step3}
In this section we will discretize the temperature equation.
%
\\
We would like to point out that this section is only provided for completeness, because we do not consider temperature during our studies.

If the equation is solved in regular mode, then temperature nodes are located on the basic nodal points.
However if the equation in solved in extended mode, temperature nodes are located on the cell centre points\footnote
	{
		As a short reminder, the cell centre (CC) nodes are located at the centre of a cell, this should be clear from its name.
		However since we are in extended mode, some (valid) nodes will be outside the computational domain.
		Such nodes are used to formulate the boundary conditions.
	}.
%
\\
As we have done it for the momentum equations, we again assume uniform grid spacing.
Also this discussion will only consider the extended case.


	\subsection{Discretized Form of the Heat Equation}~\label{sec:discretization:step3:HeatStencil}
	The energy equation, that was derived in section \ref{sec:GoverningEq:TheNSEQ:ContEquations:Energy} on page \pageref{sec:GoverningEq:TheNSEQ:ContEquations:Energy}, reads as
	\begin{align}
		\rho C_p \, \Dt{T} 	&= -\partial_{x} q_{x} - \partial_{y} q_{y} + \mathcal{H}	,	\label{eq:discretization:step3:HeatStencil:tempEq}		\\
		\Vek{q} 			&= - k \, \GradWrt{\Vek{x}}{T}								. 	\label{eq:discretization:step3:HeatStencil:Fourier}
	\end{align} 
	%
	In the equation above, $T$ is the temperature and $\rho C_p$ is the volumetric, isobaric heat capacity.
	$\Vek{q}$ is the heat flux and $\mathcal{H}$ is the heating term.
	%
	\\
	Equation \eqref{eq:discretization:step3:HeatStencil:Fourier} is known as Fourier's law, and is a constitutive law.
	It relates a temperature difference to a flux of heat.
	$k$ is the thermal conductivity, that describes how \emph{good} temperature can move\footnote
		{
			It is not a resistance.
			Larger values means that heat will move through the material with less resistance.
			Smaller values on the other hand, indicates that the material imposes a large resistance on the heat travel, and the material is thus a good (thermal) isolator.
		}.

	As we have already mentioned before in section \ref{sec:GoverningEq:UsedScheme:TheScheme:Step3_solveTemp}, where we have first described this step, we will use an implicit scheme.
	Instead of relating the temperature distribution from the last time step to the one of the current step, we relate the temperature distribution of this time step to the one of the next step.
	The temperature of the current step, was obtained by interpolating\footnote
		{
			As it was described in section \ref{sec:discretization:step1:weights:modified} on page \pageref{sec:discretization:step1:weights:modified}, this was done with modified weights.
			The usual weights were multiplied by the volumetric, isobaric heat capacity.
		}
	the temperature of the marker to the grid.

	% TODO
	% \memo{Draw a picture of the situation and put it here.}
	%  \memo{Taras' book only treats the basic nodal grid case.}

	Discretizing equation \eqref{eq:discretization:step3:HeatStencil:tempEq} is not particular hard, but tedious.
	We will proceed as we have done it for the momentum equations, by first discretizing each term and then collecting them.
	%
	\\
	The heating term will be ignored for now, we will cover it in section \ref{sec:discretization:step3:HeatingTerm} on page \pageref{sec:discretization:step3:HeatingTerm}.
	%
	As a small remark, we will use $T_{i,j}$ to denote the temperature at node $(i,j)$ in the \emph{next} time step.
	To denote the temperature in the \emph{current} time step we will use $T_{i,j}^{0}$.
	Other grid quantities, for example density or thermal conductivity are expressed in the \emph{current} time step and not in the next one.
	
	
			\paragraph{Material Derivative}
			Finding the stencil of the material derivative of temperature, was already done in section \ref{sec:GoverningEq:UsedScheme:TheScheme:Step3_solveTemp} on page \pageref{sec:GoverningEq:UsedScheme:TheScheme:Step3_solveTemp}, and reads as 
			\begin{align}
				\rho C_p \, Dt{T} &\approx	\left(\rho C_p\right)_{i,j} \; \frac{T_{i,j} - T_{i,j}^{0}}{\delta t}	 \nonumber \\
						&= \frac{\left(\rho C_p \right)_{i,j}}{\delta t} \, T_{i,j} - \frac{\left(\rho C_p \right)_{i,j} \cdot T_{i,j}^{0}}{\delta t} .		\label{eq:discretization:step3:HeatStencil:DtT}
			\end{align}
			%
			In the equation $\delta t$ is the time step increment that will be used to evolve the system to the next step.
			$\left(\rho C_p \right)_{i,j}$ is the volumetric, isobaric heat capacity on the cell centre grid point $(i,j)$, in the \emph{current} time step.
			% END: 		Material Derivative
			%%%%%%%%%%%%%%%%%%
			
			
			\paragraph{Derivative of $q_x$ with respect to $x$}
			This term must be derived in three step, similar to the process that was used to find the derivatives of stresses, in the momentum equations.
			$q_x$ is defined on the staggered $x$ velocity nodes.
			\begin{align}
				-\partial_{x} q_{x} \approx		-\frac{q_{x(i,j)} - q_{x(i,j-1)}}{h_x}		\label{eq:discretization:step3:HeatStencil:dQXdx}
			\end{align}
			%
			In the equation above $q_{x(i,j)}$ is the heat flux on the $x$ velocity node, that is associated to the basic nodal node $(i,j)$.
			
			Using Fourier's law, we find an approximation of $q_{x(i,j)}$ as
			\begin{align}
				q_{x(i,j)} = -k_{x(i,j)} \, \frac{T_{i,j+1} - T_{i,j}}{h_x} .		\label{eq:discretization:step3:HeatStencil:QX_stencil}
			\end{align}
			%
			$k_{x(i,j)}$ is the thermal conductivity, that is defined on the staggered $x$ velocity node, that is associated to basic nodal point $(i,j)$.
			
			Now we can put the two steps together and arrive at 
			\begin{align}
				-\partial_{x} q_{x} 	&\approx	-\frac{q_{x(i,j)} - q_{x(i,j-1)}}{h_x} 		\nonumber \\
				&= \frac{k_{x(i, j-1)}}{{h_x}^2} \,T_{i,j-1}
				+ \left( -\frac{k_{x(i, j-1)}}{{h_x}^2} - \frac{k_{x(i,j)}}{{h_x}^{2}} \right) \, T_{i,j}
				+ \frac{k_{x(i,j)}}{{h_x}^{2}} \, T_{i,j+1}	.		\label{eq:discretization:step3:HeatStencil:dQXdxStencil}
			\end{align}
			% END:		Derivative of q_x
			%%%%%%%%%%%%%%%%%%%%
	
	
			\paragraph{Derivative of $q_y$ with respect to $y$}
			This step is very similar to the step before.
			But instead of using the staggered $x$ velocity grid, we are now using the $y$ gird nodes.
			\begin{align}
				-\partial_{y} q_{y} 	\approx 	-\frac{q_{y(i,j)} - q_{y(i-1, j)}}{h_y}			\label{eq:discretization:step3:HeatStencil:dQYdy}	
			\end{align}
			%
			$q_{y(i,j)}$ is the heat flux on the $y$ velocity grid node, that is associated to basic nodal point $(i,j)$.\\
			
			We will now now use Fourier's law to find an expression for the heat flux.
			\begin{align}
				q_{y(i,j)} = -k_{y(i,j)} \, \frac{T_{i+1,j} - T_{i,j}}{h_y}			\label{eq:discretization:step3:HeatStencil:QY_stencil}
			\end{align}
			%
			As before $k_{y(i,j)}$ is a thermal conductivity, but this time it is defined on the $y$ velocity grid node.
			
			Now we can combine the two previous steps to find the following expression.
			\begin{align}
				-\partial_{y} q_{y} 	&\approx 	-\frac{q_{y(i,j)} - q_{y(i-1, j)}}{h_y}			\nonumber \\
					&= \frac{k_{y(i-1,j)}}{{h_y}^2} \, T_{i-1,j}
					+ \left( -\frac{k_{y(i-1,j}}{{h_y}^2} - \frac{k_{y(i,j)}}{{h_y}^2} \right) \, T_{i,j}
					+ \frac{k_{y(i,j)}}{{h_y}^2} \, T_{i+1,j}		\label{eq:discretization:step3:HeatStencil:dQYdyStencil}
			\end{align}
			% END:		Derivative of q_x
			%%%%%%%%%%%%%%%%%%%%
	
	
			\paragraph{Heat Term}
			As we have said before, we will not yet considering the heat term in full, this will be done in section \ref{sec:discretization:step3:HeatingTerm}.
			At the current step we just assume that we have $\mathcal{H}_{i,j}$, that describes the source term at the cell centre node $(i,j)$.
			% END:		Heat Term
			%%%%%%%%%%%%%%%%%%%%%%
			
			
		\subsubsection{Full Temperature Stencil}~\label{sec:discretization:step3:HeatStencil:FullStencil}
		Now that we have discretized the individual terms, we can collect them and reorder them.
		Doing so results in the following equation for $T_{i,j}$.
		\begin{align}
			& \frac{k_{x(i, j-1)}}{{h_x}^2} \,T_{i,j-1}						% T_1
			 +\frac{k_{y(i-1,j)}}{{h_y}^2} \, T_{i-1,j}						% T_2
			 +\frac{k_{y(i,j)}}{{h_y}^2} \, T_{i+1,j}						% T_4
			 +\frac{k_{x(i,j)}}{{h_x}^{2}}					\nonumber \\ 	% T_5
			&+\left(
				 -\frac{k_{y(i-1,j}}{{h_y}^2} -\frac{k_{y(i,j)}}{{h_y}^2}  -\frac{k_{x(i, j-1)}}{{h_x}^2} -\frac{k_{x(i,j)}}{{h_x}^{2}} -\frac{\left(\rho C_p \right)_{i,j}}{\delta t}
			  \right) \, T_{i,j}							\nonumber \\	% T_3
			&= -\frac{\left(\rho C_p \right)_{i,j} \cdot T_{i,j}^{0}}{\delta t} -\mathcal{H}_{i,j}			\label{eq:discretization:step3:HeatStencil:FullStencil:heatEqDisc}		
		\end{align}
		
		For every inner node\footnote{In the case of the extended cell centre grid, which was used, this are all nodes that lies inside the computational domain.}  \eqref{eq:discretization:step3:HeatStencil:FullStencil:heatEqDisc} can be used to get an equation for the temperature at the \emph{next} time step on that node.
		A careful analysis reveals that the used stencil is of second order in space and is first order in time\footnote
			{
				As we have already pointed out before in the second step, that the time step of the simulation is not fix, but is chosen such that the displacement of the markers is limited.
				However when the temperature equation is enabled, we also impose a condition that limits the maximal \emph{change} of temperature, during one time step.
			}.
		The handling of the boundary nodes will be discussed in section \ref{sec:discretization:step3:BC} on page \pageref{sec:discretization:step3:BC}.
		
		% END:		Full Stencil
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
	% END:		Discretized temperature equation
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	\subsection{Boundary Conditions}~\label{sec:discretization:step3:BC}
	The way the boundary conditions are implemented on this grid are similar to the ones that are described in section \ref{seq:discretization:step2:boundary:tangential} on page \pageref{seq:discretization:step2:boundary:tangential}.
	None of the nodes lies directly on the boundary of the computational domain, so we have to perform some kind of interpolation.
	%
	\\
	Above we have stated, that we only consider grids that have a uniform spacing, so we will only consider such grids as well.
	
			\paragraph{Dirichlet Condition}
			We will now discuss how Dirichlet conditions are implemented.
			As we have discussed already in section \ref{seq:discretization:step2:boundary} on page \pageref{seq:discretization:step2:boundary}, such conditions are used to impose a \emph{value} \emph{at} the boundary.
			But as we have said before, none of the grid nodes lies on the boundary, we have to reconstruct the value there.
			
			For this we will use linear interpolation.
			It will involve a node inside the domain, one outside and the value on the boundary.
			The value of the inside node is determined by an equation, but the outer node is not.
			%
			\\
			However this is not the case for nodes at the corners, because they do not have an inner node as neighbour.
			We will pretend that the node bellow or above are actually inner nodes, respectively.
			This is not a problem, since these nodes will not appear in any other expression.
			Actually we could set them to any value we want.
			
				
			After some calculations, it turns out that the following expression can be used, to reconstruct the value on the boundary
			\begin{align}
				T_b = \frac{T_{o} + T_{i}}{2}     
					= \frac{1}{2}\, T_{o} + \frac{1}{2} \, T_{i} .		\label{eq:discretization:step3:BC::Dirichlet:stencil}
			\end{align}
			%
			Above equation $T_b$ is the value that is imposed on the boundary.
			$T_{o}$ is the outer node and $T_{i}$ is the inner node, that is adjacent to $T_{o}$.
			We would like to point out that equation \eqref{eq:discretization:step3:BC::Dirichlet:stencil} is only valid, if the boundary lies exactly between the two nodes\footnote
				{
					This condition is naturally fulfilled when using constant grid spacing.
					It is also possible to achieve this in the irregular case, because nodes at the outside can be placed freely.
				}.
			%
			An analysis of this condition reveals that it has second order in space\footnote
				{
					The regular version of the temperature solver, who uses the basic nodal grid, can impose the value directly on the node.
				}.		
			% END:		Dirichlet
			%%%%%%%%%%%%%%%%%%%%%%%
			
			
			\paragraph{Neumann Condition}
			Such conditions impose restrictions on the normal derivative of a quantity at the boundary.
			Again we will use two nodes, one inner and one outer node, that are direct neighbours. 
			%
			Assuming that the boundary lies exactly in the middle between them, we can apply a finite difference stencil and arrive at
			\begin{align}
				f_b = \frac{T_{o} - T_{i}}{h_x} .			\label{eq:discretization:step3:BC::Neumann:stencil}
			\end{align}
			%
			The stencil above computes the \emph{out flux}.
			Again $T_{o}$ is the outer, $T_{i}$ the inner node and $f_b$ is the flux that should be imposed.
			%
			\\
			If $f_b$ is zero, meaning perfect insulation, the condition decays to $T_o \overset{!}{=} T_i$.
			In cases where $f_b \not= 0$, care must be taken to choose the correct \emph{nodes}, such that the stencil agrees with the normal direction.
						
			% END:		Neumann
			%%%%%%%%%%%%%%%%%%%%%%%%%%%			
		
		\subsubsection{Standard Boundary Conditions}~\label{sec:discretization:step3:BC:std}
		The default set of boundary conditions that is used is referred to as \texttt{pipe} condition\footnote
			{
				This is the same name that is used in the momentum equations as well.
			}.
		At the top and bottom of the domain Dirichlet conditions are imposed.
		On the left and right boundary perfect insulation is imposed.
		
		% END:		Standard boundary conditons
		%%%%%%%%%%%%%%%%%%%%%%%
		
	% END:		Boundary Conditions
	%%%%%%%%%%%%%%%%%%%%
	
	
	\subsection{Heating Term}~\label{sec:discretization:step3:HeatingTerm}
	Now we will discuss the discretization of the heating term.
	This term was first introduced in section \ref{sec:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm} on page \pageref{sec:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm}.
	The term is usually split into four different contributions
	%
	\begin{align}
		\mathcal{H} = H_{s} + H_{a} + H_{r} + H_{L}	.	\label{eq:discretization:step3:HeatingTerm:splitting}
	\end{align}
	%
	We will now discuss how they are implemented individually.
	
	
		\subsubsection{$H_L$ -- Latent Heat}~\label{sec:discretization:step3:HeatingTerm:H_L}
		Latent heat is caused by phase transitions.
		Its value can either be positive or negative or zero.
		We do not allow phase transitions, so latent heat is always zero\footnote{Actually it is ignored and not even implemented.}.
		% END:		Lathent heat
		%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$H_r$ -- Radioactive Heat}~\label{sec:discretization:step3:HeatingTerm:H_r}
		Radioactive material decays over time and as a result produces heat, it is either positive or zero.
		If it should be considered, depends on the materials that are involved and the time scales considered.
		Since we are considering fluids and rather short time scales, we do not need to consider it.
		%
		\\
		For compatibility with an older prototype it is supported.
		
		Since the radioactive decay is a material property markers caring such an information.
		We determine it by simply interpolating that marker property in step $1$ to the cell centre grid.			
		% END:		Radio active heat
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$H_a$ -- Adiabatic Heat}~\label{sec:discretization:step3:HeatingTerm:H_a}
		This term models heat that is created by volumetric changes.
		The most common example for such heat can be found in gas.
		If gases are expanded they cools down, which leads to a negative adiabatic heat.
		If gases are compressed they heats up, which means the adiabatic heat is positive.
		
		Gas may be a prime example but it is true for all compressible media.
		Strangely we have assumed that the medium is incompressible, when we have derived and implemented the Navier--Stokes equation.
		However this is a common simplification, which is known as \emph{extended Boussinesq approximation}, see \cite{gerya_2019}, chapter 11.
		
		Strictly, adiabatic heat is defined as
		\begin{align}
			H_a = T \, \alpha \, \Dt{P} .			\label{eq:discretization:step3:HeatingTerm:H_a:def}
		\end{align}
		%
		$T$ is the current temperature and $\alpha$ is the thermal expansion.
		$\Dt{P}$ is the material derivative of pressure.
		
		One could approximate the pressure derivative similar than we have done it before.
		However we will use an approximation that is commonly used in numerical geodynamics.
		In the absence of phase transformations and small thermal expansion values, it can be assumed that the pressure is mostly caused by hydrostatic effects, \cite{gerya_2019}.
		This assumption leads to the following approximation
		\begin{align}
			H_a = T \, \alpha \cdot \left( \rho \, u_{x} \, g_{x} + \rho \, u_{y} \, g_{y} \right) 		\label{eq:discretization:step3:HeatingTerm:H_a:approx}
		\end{align}
		%
		In the above equation $g_i$ is the acceleration, so $ \rho  u_{i}  g_{i}$ is an approximation for the pressure change.
		
		In cases where dynamic pressure changes dominates, this approximation will fail.
		Since we mostly have ignored temperature in this study, the restriction does not affect us at all.
		The implemented heating term uses approximation \eqref{eq:discretization:step3:HeatingTerm:H_a:approx}\footnote{But only the $y$ direction is implemented, $g_x$ is assumed to be zero.} for compatibility with an older prototype.
	
		% END:		Adiabatic heat
		%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$H_s$ -- Shear Heat}~\label{sec:discretization:step3:HeatingTerm:H_s}
		Sear heat is caused by friction, a prime example is rubbing hands on each other.
		As the radioactive heating this term is either positive or zero, but never negative.
		
		In section \ref{sec:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm} on page \pageref{sec:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm}, it was defined as
		%
		\begin{align}
			H_s &= \tau_{ij} \cdot \dot{\hat{\epsilon}}_{ij} 	 	\label{eq:discretization:step3:HeatingTerm:H_s:def}		\\
				&=    \tau_{xx} \, \dot{\hat{\epsilon}}_{xx} 
					+ \tau_{xy} \, \dot{\hat{\epsilon}}_{xy}
					+ \tau_{yx} \, \dot{\hat{\epsilon}}_{yx}
					+ \tau_{yy} \, \dot{\hat{\epsilon}}_{yy}		\nonumber		\\
				&=    \tau_{xx} \, \dot{\hat{\epsilon}}_{xx} 
					+ 2 \, \tau_{xy} \, \dot{\hat{\epsilon}}_{xy}
					+ \tau_{yy} \, \dot{\hat{\epsilon}}_{yy} .		\label{eq:discretization:step3:HeatingTerm:H_s:used}
		\end{align}
		%
		It is possible to apply several degrees of simplification.
		For example the last step exploited, that angular momentum is conserved, and thus makes the two tensors symmetric.
		Another simplification would be to directly include Hook's law, which relates $\dot{\hat{\epsilon}}$ to $\sigma$.
		And since the flow is incompressible, we have $\partial_{x} u_{x} = -\partial_{y} u_{y}$.
		%
		\\
		However to maintain a certain degree of generality, we have decided to work with equation \eqref{eq:discretization:step3:HeatingTerm:H_s:used} and not to apply further simplifications to it.
		
		Stress and strain rate are defined by the following relation.
		\begin{align}
			\dot{\hat{\epsilon}}_{ij} &= \frac{1}{2} \, \left( \frac{\partial u_i}{\partial x_j} + \frac{\partial u_j}{\partial x_i} \right)	\label{eq:discretization:step3:HeatingTerm:H_s:strainRate}	\\
			\sigma_{ij}				  &= 2 \, \eta \, \dot{\hat{\epsilon}}_{ij}
		\end{align}
		% 
		We want to point out that equation \eqref{eq:discretization:step3:HeatingTerm:H_s:strainRate} assumes that the flow is incompressible.
		
		\begin{figure}[h]
			\centering
			\begin{minipage}{.6\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__discretisation__step3/shearHeat__9_3_c.png} 
				\caption
					{
						Arrangement of different quantities on a staggered grid.
						We want to compute the shear heat located in the cell centre.
						%
						Taken from \cite{gerya_2019}, Fig. 9.3. $c)$.
					}
				\label{fig:discretization:step3:HeatingTerm:H_s:situation}
			\end{minipage}
		\end{figure}
		%
		As we can see from figure \ref{fig:discretization:step3:HeatingTerm:H_s:situation}, $\tau_{xx} \, \dot{\hat{\epsilon}}_{xx}$ and $\tau_{yy} \, \dot{\hat{\epsilon}}_{yy}$ are already defined at the correct location.
		But $\tau_{xy} \, \dot{\hat{\epsilon}}_{xy}$ are not located at the cell centre points but at the basic nodal points.
		%
		\\
		We will determine the value at the centre by averaging the four surrounding values.
		This can be done in two ways, first we could average $\tau_{xy}$ and $\dot{\hat{\epsilon}}_{xy}$ separately.
		The second option is to compute the four surrounding values of $\tau_{xy} \, \dot{\hat{\epsilon}}_{xy}$ and averaging them.
		We will use the second option and get 
		\begin{align}
			\left( \tau_{xy} \, \dot{\hat{\epsilon}}_{xy} \right)_{cc(i,j)} = 					
			\frac{1}{4} \big(  &\tau_{xy(i,j)} \, \dot{\hat{\epsilon}}_{xy(i,j)}					
							    + \tau_{xy(i,j+1)} \, \dot{\hat{\epsilon}}_{xy(i,j+1)}			\nonumber 	\\
							    &+ \tau_{xy(i+1,j)} \, \dot{\hat{\epsilon}}_{xy(i+1,j)}			
							    + \tau_{xy(i+1,j+1)} \, \dot{\hat{\epsilon}}_{xy(i+1,j+1)}
					\big) .																\nonumber
		\end{align}

		% END:		Shear heat
		%%%%%%%%%%%%%%%%%%%%%%%

	% END:		HEATING TERM
	%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
% END:		Step3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
