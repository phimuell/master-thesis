%%%%%%%%%%%%%%%%%%%%%%%%%%
% in this file we will present some important basics.


\section{Conceptional Basics}~\label{sec:discretization:basics}
In this section we will present some important concepts, that are used in the rest of the present work.
Previously, we have introduced the main ideas of the scheme, as well as some terminology, however it became necessary to go into more details.
This will contain some parts, which are imposed by the implementation, but we will keep it to a minimum, see appendix \ref{chap:appendix:EGD} and \ref{chap:appendix:Corello}, starting at page \pageref{chap:appendix:EGD} for more about the implementation.

As we have said before, the method has two components, one component is performed on an Eulerian grid, the second one operates on Lagrangian particles (markers).
We will first discuss the different grid\emph{s} that are used and then the markers.


	\subsection{The Grid(\emph{s})}~\label{sec:discretization:basics:Grids}
	We will use a staggered grid \cite{ARAKAWA1977173} for the Eulerian computations.
	Using such a gird will complicating things a little bit, especially the indexing, but at the same time will give great advantages.
	The big gain of using a staggered grid is, that properties are located where they are needed.
	
	In figure \ref{fig:discretization:basics:Grids:staggeredGrid} on page \pageref{fig:discretization:basics:Grids:staggeredGrid} an \emph{extended} staggered grid is shown.
	It is used for formulating the momentum equations.
	For each \emph{basic nodal point}\footnote{This are the na\"ive grid points.} we have $3$ quantities, two velocity components and the pressure.
	However they are not defined at the location of the node, instead each of them is defined at a different location.
	The dashed line in the schematic drawing indicates which staggered grid node belongs to a certain basic nodal point.
	%
	\begin{figure}[h]
		\centering
		\begin{minipage}{.9\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__discretisation__common/staggeredGrid.png} 
			\caption
				{
					Locations and associations (dashed line) on an extended staggered grid.
					%
					Taken from \cite{gerya_2019}, Fig. 7.15. 
				}
			\label{fig:discretization:basics:Grids:staggeredGrid}
		\end{minipage}
	\end{figure}
	%
	The (associated) pressure point is defined in the centre of the cell.
	The (associated) $x$ velocity component is defined in the middle of the right cell face, and the $y$ component is defined in the middle of the bottom cell face.
	
	Using such a grid seams bizarre at first, but will provide us with a lot of advantages, that will come to light when we formulate the discretized versions of the equations.
	Also boundary conditions can be incorporated in a very natural way\footnote
		{
			At least the ones we are using.
		}.
	%
	\\
	An important remark is, that not all nodes have a physical meaning.
	A node without a physical meaning is called \emph{ghost node}, consequently, when the equations are formulated none of them will \emph{ever} be used.
	They are an artefact that is caused by the way we index the nodes.
	They could be removed, but this would complicating the indexing.
	%
	A node \emph{outside} the domain is not automatically a ghost node.
	In fact some of these nodes are used to formulate the boundary conditions.
	\FloatBarrier

	
		\subsubsection{The Different Grids}~\label{sec:discretization:basics:Grids:diffGrid}
		Using a staggered grid, is like using several grids at the same time.
		
		Formally a grid can be seen as a discretization of space.
		Assume that the \emph{computational domain} is described by a rectangular box, and can be written as
		\begin{align}
			\mathcal{D} = \left[L_{x}^{l},\, L_{x}^{u}\right] \times \left[L_{y}^{l},\, L_{y}^{u}\right] . \label{eq:discretization:basics:Grids:diffGrid:defCompDomain}
		\end{align}
		%
		Then the grid is composed of \emph{two} sets of points, which we denote by
		\begin{align}
			\mathcal{X}^{(g)} = \Set{x_{k}^{(g)} }_{k = 1}^{N_x} \qquad \text{ and } \qquad \label{eq:discretization:basics:Grids:diffGrid:GridPointSets}
			\mathcal{Y}^{(g)} = \Set{y_{k}^{(g)} }_{k = 1}^{N_y} .     
		\end{align}
		%
		The sets $\mathcal{X}^{(g)}$ and $\mathcal{Y}^{(g)}$ also depends on the specific grid that is under consideration\footnote
			{
				Which grid is used, is mostly clear from the context.
			}.
		%
		Finally the grid nodes\footnote{This are the $2D$ points.} are formed by a cross product of the two sets.
		
		In the following we will describe the \emph{basic nodal grid}, is the most obvious and na\"ive choice.
		It is worth noting that, all other grids are derived from this one, thus we will discuss it in more detail.
		%
		\\
		The grid nodes are formed, by imposing the following conditions
		\begin{align}
			L_{x}^{l} &= x_{1}^{(g)} < x_{2}^{(g)} < \ldots < x_{i}^{(g)} < x_{i+1}^{(g)} < \ldots < x_{N_x}^{(g)} = L_{x}^{u} , \label{eq:discretization:basics:Grids:diffGrid:basicConditionX} \\
			L_{y}^{l} &= y_{1}^{(g)} < y_{2}^{(g)} < \ldots < y_{i}^{(g)} < y_{i+1}^{(g)} < \ldots < y_{N_y}^{(g)} = L_{y}^{u} . \label{eq:discretization:basics:Grids:diffGrid:basicConditionY}
		\end{align}
		
		The separation between two grid nodes is called \emph{grid spacing} and is given as,
		\begin{align}
			h_{x; j}^{(g)} &= x_{j+1}^{(g)} - x_{j}^{(g)} , 	\label{eq:discretization:basics:Grids:diffGrid:gridSpacingX}		\\
			h_{y; i}^{(g)} &= y_{i+1}^{(g)} - y_{i}^{(g)} . 	\label{eq:discretization:basics:Grids:diffGrid:gridSpacingY}		
		\end{align} 
		%
		The grid spacing depends on the grid that is under consideration.
		We would like to point out, that also $\Delta x_{j}^{(g)}$ and $\Delta y_{i}^{(g)}$ are used to denote the \emph{grid spacing}, or alternatively \emph{grid with}.
		
		For several reasons we impose an additional condition on the grids.
		We require that the grid spacing is \emph{constant}, thus $h_{x; j}^{(g)}$ and $h_{y; i}^{(g)}$ is the same for all grid nodes.
		As a consequence the grid spacing also does not depend on the grid and is completely characterized by $h_x$ and $h_y$.
		We would like to remark that $h_x$ and $h_y$ does not need to be equal\footnote{Although in this work they always are.}.
		
		In total we have five\footnote
			{
				Actually they are just $4$ grids, the fifth one is obtained, by handling the ghost nodes of the pressure grid differently.
			}
		different kinds grids and each grid, with the exception of the basic nodal grid, can be either of \emph{regular} or \emph{extended} size, resulting in a total of $9$ grids.
		%
		\\
		We will now present and describe them.
		As we have said before, all of them are derived by moving the basic nodal grid around.
		%
		\begin{description}
			
			\item[Basic Nodal Grid] 
			This grid was described above already.
			Its main feature is that all of its nodes are inside the domain, although the outermost nodes are located directly on the boundary.
			There is also only a regular version\footnote{However the extended version is technically used, but not allowed.} and it does not have any ghost nodes.
			
			\item[$x$-Velocity Grid]
			This grid is derived from the basic nodal grid by sifting the grid nodes half a grid spacing to the top.
			The nodes do not change their $x$ coordinate, only their $y$ coordinate is modified.
			As the name implies it is used to compute the $x$ velocity component.
			
			\item[$y$-Velocity Grid]
			As its name indicates at these nodes the $y$ component of the velocity is located.
			It is obtained by shifting the basic nodal grid half a grid spacing to the left.
			
			\item[Pressure Grid]
			As its name indicates at this grid the pressure is located.
			It is obtained by shifting the basic nodal grid half a grid width to the top and the left.
			The nodes are located in the centre of a cell.
			
			\item[Cell Centre Grid]
			This grid is basically the same as the pressure grid.
			Its only difference is, that if the grid is \emph{extended}, it does \emph{not} have any ghost nodes.
			But if the grid has \emph{regular} size, it is fully equivalent to the pressure grid.
			
		\end{description}

		When a grid is moved, some of its nodes will end up outside the computational domain.
		Such a node is a \emph{candidate} for being a ghost node, but if it is actually one, depends on the specific grid.
		%
		\\
		A grid has an additional attribute, its \emph{size}\footnote{This is \emph{not} the number of grid nodes.}.
		This attribute also influences the ghost nodes, there are two different sizes.
		%
		\begin{description}
			
			\item[Regular Grid]
			This are ``original'' sized grids, all of them are derived by moving the normal basic grid, as it is described above.
			All of them have $N_y$ many points in $y$ direction and $N_x$ many in $x$ direction.
			It is worth noting that \emph{all nodes} on \emph{all grids}, that are outside the computational domain, are considered ghost nodes.
			These ghost node have \emph{no} physical meaning, they are used to enable an easy indexing.
			
			
			\item[Extended Grid]
			These kind of grids are obtained by moving the \emph{extended basic nodal grid} in the same way as it is described above.
			The extended basic nodal grid is obtained by adding an additional row and column to the basic nodal grid, the resulting grids have $N_x+1$ many nodes in $x$ direction and $N_y + 1$ in $y$ direction.
			%
			\\
			However there are some differences to their regular counterparts.
			These mostly affects which outside node is considered a ghost node and which is not.
			In the case of the pressure grid, all nodes that are outside the domain are considered ghost nodes.
			On the other hand, the \emph{extended} cell centred grid, does not have any ghost nodes, even the nodes outside the domain, have a physical interpretation.
			%
			\\
			On the $x$ velocity grid only nodes of the right most column are ghost nodes, for the $y$ velocity grid, only nodes from the lowest row are considered ghost nodes.
			The other outside nodes, have a meaning, especially in the case, when a boundary conditions are formulated.
			 
		\end{description}
		
		We would like to remark that the grid type, does not solely depends on the kind of grid\footnote{Basic nodal grid, pressure grid, $v_y$ grid, $v_x$ grid or cell centre grid}, but also involves if the grid is extended or not.
		% END:		Difference Grids
		%%%%%%%%%%%%%%%%%%%%%%%%%%
		
	
		\subsubsection{Orientation and Indexing of the Grid}~\label{sec:discretization:basics:Grids:orientationIndexing}
		Geophysics is a science that concerns itself with \emph{depth}, so it is advisable to orientate the coordinate system, such that depth is a \emph{positive} quantity.
		It is also favourable to have a right hand sided coordinate system.
		Thus in geophysics the $x$ axes points towards the right side and the $y$ axes, which is associated with depth, points \emph{downwards}\footnote{A similar convention is used in computer graphics.}.
		%
		To complicate matters the term \emph{up} still refers to ``nearer towards the surface'', which translates to ``smaller value of $y$''.
		%
		\\
		The $z$ axis would points away from the reader, but we do not use it.
		
		Before be have explained that grid properties are stored in a matrix, which is addressed by two indexes, \texttt{A(i, j)}, but where does this grid node lies? 
		And to which node is it associated?
		It could either be the $i^{th}$ grid point in $y$ direction and the $j^{th}$ grid point in $x$ or it could be the other way around.
		The answer is, that the association in $y$ comes first.
		So \texttt{A(i, j)} means that the coordinate of the corresponding grid nodes is located at $\left( x_{j}^{(\text{g})},\, y_{i}^{(\text{g})} \right)$.
		Where a \texttt{A} is defined on the grid $g$.
				
		Note that if we refer to a grid node $(i, j)$ then the \emph{first index}, always tells the $y$ association and the second index the association in $x$.
		However, if non--index quantities are considered, such as a position or a force, then the first value is always the $x$ component and the second one is the $y$ component.
		
			\paragraph{Base of Indexing}~\label{sec:discretization:basics:Grids:orientationIndexing:BaseOfIndex}
			In this document the reader can generally expect, unless told differently, that the indexing starts at $1$.
			However most of the formula, will not depend on the start of the indexing\footnote{This is not true for the boundary conditions.}.
			%
			\\
			But on the code level, meaning in our implementation, we uses a zero based indexing.
			The reason for this is, that \Cpp{} was used and there zero based indexing is common.			
			% END:		Start of indexing
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% END:		Orientation
		%%%%%%%%%%%%%%%%%%%%%%%%
	
	\FloatBarrier
	% END:		The grid
	%%%%%%%%%%%%%%%%%%%%%%%%%


	
	\subsection{The Markers}~\label{sec:discretization:basics:Markers}
	Now we discuss the markers, as we have already mentioned, they are conceptionally simpler than the grids.
	Markers are assumed to have a consecutive enumeration\footnote
		{
			As we do it for the grid, we assume that the indexing starts at $1$.
			However in the code the indexing starts at $0$.
		}.
	
	Each of the different marker properties is represented by an array.
	As we know an array can be indexed by one number alone.
	This means that if \texttt{A} is a marker property and \texttt{m} is a marker's index, then \texttt{A[m]} is the value of property \texttt{A} of marker \texttt{m}.
	%
	\\
	Before we have said that we can have multiple grid properties with the same (physical) property.
	The only difference between them is, that they are defined different grids.
	This will not happen for marker properties, there a property can only occur once.
	
	
		\subsubsection{Associated Node}~\label{sec:discretization:basics:Markers:AssocNode}
		An important concept that is used to connect markers and grid nodes is the \emph{associated node of a marker}.
		
		A grid can be seen in two different ways.
		The first view emphasizes that the grid is composed of nodes, that are spread over the computational domain.
		However a grid can also be seen as \emph{cells}, which partition the computational domain, the cells themself are formed by the nodes.
		%
		\\
		Which of the two views should be adopted, depends on the situation.
		Since we are using finite \emph{differences}, adopting the \emph{node centric} view is advantageous.
		However in certain situations, such as interpolation, step $1$ and $4$ of the scheme, a cell centred view can help.
		
		However in the present work a \emph{cell} centred view, has some \emph{theoretical} problems, at least on some of the regular grids.
		The definition of a cell as ``the area formed by the four enclosing nodes,'' is not applicable to all regular grids.
		%
		Since some grids do not fully cover the computational domain, it is not guaranteed, that a marker is always inside a cell, formed by four surrounding nodes.
			
		As a solution, the concept of the \emph{associated node of a maker} was conceived.
		The associated node is the nearest top left node to the marker.
		Such a node will always exists, but the node is not guaranteed to be inside the domain, and can be a ghost node too, which is not a problem.
		%
		\\
		Also some nodes will never have associated markers.
		This is the case for nodes on the left or bottom side of the domain in the extended case.		
		% END:		Assoc Node
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
	% END:		Markers
	%%%%%%%%%%%%%%%%%%%%%%%

% END:		Concepts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
