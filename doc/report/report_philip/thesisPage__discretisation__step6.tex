%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In this section we will discuss the marker Advection


\section[Step $6)$]{Step $6)$ -- Advection the Marker}~\label{sec:discretization:step6}
This is the last step of each iteration, it is concerned with advecting the markers.
%
In general we will use \emph{the} $4^{th}$ order Runge--Kutta method\footnote
	{
		We would like to remark here, that a Runge--Kutta method, is a general procedure to solve ODEs.
		However usually if somebody just say Runge--Kutta or forth order RK, the classical fourth order Runge--Kutta method is meant.
	}.

The forth order Runge--Kutta method, abbreviated as RK4, solves the initial value problem, $\Fkt{f}{\Vek{y}} = \dot{\Vek{y}}$ with $\Fkt{\Vek{y}}{t = t_0} = \Vek{y}_{0}$.
We only consider autonomous ODEs here, meaning that they are independent of time.
We can do this, because of our assumption, that while we move the markers to their new positions, the velocity field will not change, see section \ref{sec:GoverningEq:UsedScheme:TheScheme:Step6_advection}.
%
\\
This is a pragmatic decision, that will lower the computational cost of the method.
Otherwise we would have to redo the $5$ other steps,  for each intermediate state needed by RK4.

RK4 can be written as
\begin{align}
	\Vek{k}_1 &= \Fkt{f}{\Vek{y}_n                           }		\nonumber \\
	\Vek{k}_2 &= \Fkt{f}{\Vek{y}_n + \frac{h}{2} \, \Vek{k}_1} 		\nonumber \\
	\Vek{k}_3 &= \Fkt{f}{\Vek{y}_n + \frac{h}{2} \, \Vek{k}_2}		\nonumber \\
	\Vek{k}_4 &= \Fkt{f}{\Vek{y}_n +       h     \, \Vek{k}_3}		\nonumber \\
	\Vek{y}_{n+1} &= \Vek{y}_n + \frac{h}{6} \left( \Vek{k}_1 + 2 \, \Vek{k}_2 + 2 \, \Vek{k}_3 + \Vek{k}_4 \right) .	\label{eq:discretization:step6:RK4Scheme}
\end{align}
%
\\
In our context $\Vek{y}$ is the \emph{whole} marker configuration, i.e. their positions.
We would like to point out, that we may have written $\Vek{y}$, but we mean $x$ \emph{and} $y$ of \emph{all} markers, with it.
%
$\Vek{k}$ is an intermediate velocity, as the position $\Vek{y}$ it contains both $x$ \emph{and} $y$ velocities of \emph{all} markers.
%
The final or effective velocity is found by combining the four intermediate velocities together.
Thus $h$ is the \emph{total} amount of time the system will evolve during this step.


			\paragraph{What is $f$?}
			For brevity we will only consider one marker in this description, but in reality it has to be applied to all markers.
			As it can be seen from the description of the method, $f$ is a function that evaluates the velocity at a given point, \emph{i.e.} the marker's location.
			
			The problem is, that $f$ is not known analytical.
			Instead the velocity is only known at certain points, which were computed by the Stokes solver\footnote
				{
					As a small remark, the term \emph{Stokes solver} usually refers to \emph{all} momentum solvers, which includes the Navier--Stokes solver.
					However it can also just refer to the Stokes equations.
					Which one is meant will be clear from the context.
				}.
			So we must approximate or reconstruct it somehow.
			There are many ways of doing it, some are better than others.
			%
			\\
			Since the flow is incompressible, the reconstructed $f$ should also have zero divergence.
			Different methods where studied and benchmarked in \cite{Pusok2015}.
			
			For the present work, we have implemented four different methods.
			We will discuss them in the sections below.
			% END:		What is f
			%%%%%%%%%%%%%%%%%%%%%%%%%%


	\subsection{Cell Centred Velocity Points}~\label{sec:discretization:step6:CCSTD}
	An efficient method of \emph{continuity based interpolation} is to use velocities defined on the \emph{extended} cell centre grid, or CC for short.
	The velocities at the markers' locations are then computed by simple bilinear interpolation\footnote
		{
			This method is internally known as \texttt{std} method.
			This has historical reasons and does not bear any meaning.
		}.
	%
	\\
	However the Stokes solver, does not compute these velocities, instead the velocities are located on the staggered velocity nodes.
	The question now is, how we can compute these velocities?
	
	\begin{figure}[h]
		\centering
		\begin{minipage}{.5\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__discretisation__step2/step2__pressureEq__7_17_a.png} 
			\caption
			{
				Velocities $v_{x1}$, $v_{x2}$, $v_{y1}$ and $v_{y2}$ are known.
				However for \emph{continuity based interpolation} a velocity located at $P$ is needed.
				Taken from \cite{gerya_2019}, Fig 7.17a.
			}
			\label{fig:discretization:step6:CCSTD:sketch}
		\end{minipage}
	\end{figure}
	%
	The situation is shown in figure \ref{fig:discretization:step6:CCSTD:sketch}.
	We see that a cell centre point, which for inner nodes coincide with the pressure nodes, is surrounded by velocities nodes.
	A simple approach is to average\footnote
		{
			It is actually a linear interpolation. 
			Because of the way how the grids were constructed, the interpolation, will result in an average.
		}
	the surrounding velocities and get
	%
	\begin{align}
		v_{x}^{(P)} = \frac{v_{x1} + v_{x2}}{2}	,		\qquad\qquad 		v_{y}^{(P)} = \frac{v_{y1} + v_{y2}}{2} .		\nonumber
	\end{align}
	
	This method allows us to determine the velocities at the \emph{inner} nodes.
	But on the extended CC gird, even the nodes \emph{outside} the computational domain, have a meaning.
	Thus we have to determine them as well.
	
	The reconstructed velocities do not only need to be divergence free, but also to respect the boundary conditions.
	So we will use them to determine the values on the outer nodes.
	We will use the techniques that were used for modelling the boundary conditions for the \emph{temperature} equation, since they share the same grid.

	\FloatBarrier
	% END: 		STD Mover
	%%%%%%%%%%%%%%%%%%%%%%%


	\subsection{LinP}~\label{sec:discretization:step6:LinP}
	This is a different method for reconstructing $f$.
	In the discussion we will focus on one marker, to keep things simple, but we have to do it for all of them.
	It is an empirical method that was found by Taras Gerya, see \cite{Pusok2015} for more informations.
	%
	\\
	Its main idea is, that the velocity at a point is given by a combination of two different velocities.
	The method can thus be written as
	%
	\begin{align}
		V_{i} = \omega \, V_{i}^{\text{Lin}} + (1 - \omega) \, V_{i}^{\text{CC}}	.	\label{eq:discretization:step6:LinP:formula}
	\end{align}
	%
	$V_{i}^{\text{Lin}}$ and $V_{i}^{\text{CC}}$ are the two different velocities and $i$ indicates the component.
	It is important to note, that the two velocity fields are defined on separate grids.
	$\omega$ is an \emph{empirical} constant, that describes the influence of each field.
	Empirical investigations have found that $\omega = \frac{2}{3}$ leads to good results.
	%
	\\
	The advection velocity for the marker, is then computed by bilinear interpolation.
	
	
			\paragraph{$V_{i}^{\text{CC}}$}
			This velocity field is defined at the cell centre points.
			It is the same field that was used by the previous method, that was described in section \ref{sec:discretization:step6:CCSTD}, so we will refer to that section for more information.
			% END: 		VEL_P
			%%%%%%%%%%%%%%%%%%%%%%%
			
			
			\paragraph{$V_{i}^{\text{Lin}}$}
			This velocity field is obtained by bilinear interpolation from the staggered velocity grids to the position of the marker.
			So it is conceptually simple, because the velocities are already known at that point.
			%
			\\
			It is important to realize that actually two grids are involved here, one for the $x$ velocities and one for the $y$ velocities.
			So depending on the component the grid will differ.
			% END:		VEL_LIN
			%%%%%%%%%%%%%%%%%%%%%
			
	% END:		LinP
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	
	\subsection{Test64}~\label{sec:discretization:step6:Test64}
	An other method that is used to reconstruct $f$, it was also developed by Taras Gerya.
	It has quite a different approach than the other methods.
	
	The previous methods worked by interpolating a \emph{velocity} from the grids to the markers.
	Here we do something different.
	%
	\\
	The main advantages of staggered grids are, that the quantities are located exactly where they are needed.
	Because of its construction, the velocity field is divergence free at the pressure point.
	%
	But the equations were formulated for a velocity \emph{derivative} not a velocity.
	%
	\\
	So instead of interpolating velocities from nodal points to the markers, this scheme interpolates \emph{derivatives of velocities} to them.
	%
	\begin{align}
		\left.\left(\pdiff{x}{v_x}\right)\right\vert_{\hat{x}, \hat{y}} &=      \left(\pdiff{x}{v_x}\right)_{i,j} \cdot \left(1 - \hat{x}\right) \cdot \left(1 - \hat{y} \right)
															 + \left(\pdiff{x}{v_x}\right)_{i+1,j} \cdot \left(1 - \hat{x}\right) \cdot \left( \hat{y} \right) \nonumber \\
															&\;\;\;+ \left(\pdiff{x}{v_x}\right)_{i,j+1} \cdot \left( \hat{x}\right) \cdot \left(1 - \hat{y} \right)
															 + \left(\pdiff{x}{v_x}\right)_{i+1,j+1} \cdot \left(\hat{x}\right) \cdot \left(\hat{y} \right)  \label{eq:discretization:step6:Test64:dVXdx_interpol}		\\
		\left.\left(\pdiff{y}{v_y}\right)\right\vert_{\hat{x}, \hat{y}} &=      \left(\pdiff{y}{v_y}\right)_{i,j} \cdot \left(1 - \hat{x}\right) \cdot \left(1 - \hat{y} \right)
															+ \left(\pdiff{y}{v_y}\right)_{i+1,j} \cdot \left(1 - \hat{x}\right) \cdot \left( \hat{y} \right) \nonumber \\
															&\;\;\;+ \left(\pdiff{y}{v_y}\right)_{i,j+1} \cdot \left( \hat{x}\right) \cdot \left(1 - \hat{y} \right)
															+ \left(\pdiff{y}{v_y}\right)_{i+1,j+1} \cdot \left(\hat{x}\right) \cdot \left(\hat{y} \right)  \label{eq:discretization:step6:Test64:dVYdy_interpol}
	\end{align}
		
	In \eqref{eq:discretization:step6:Test64:dVXdx_interpol} and \eqref{eq:discretization:step6:Test64:dVYdy_interpol} $\left( \pdiff{x_i}{v_{y_i}} \right)_{i,j}$ are the derivative of velocity at the pressure node $(i,j)$, which is exact by construction.
	$\hat{x}$ and $\hat{y}$ are the normalized distance towards node $(i,j)$.
	
	The final scheme is obtained by integrating equation \eqref{eq:discretization:step6:Test64:dVXdx_interpol} with respect to $x$ and \eqref{eq:discretization:step6:Test64:dVYdy_interpol} with respect to $y$.
	Integration constants are determined by matching them to the staggered velocity nodes.	
	% END:		Test64
	%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	\subsection{Jenny-Meyer Method}~\label{sec:discretization:step6:JM}
	This is the last method we have implemented to reconstruct $f$.
	The scheme was presented in \cite{Jenny1999} and \cite{MeyerJenny}, an extension to 3D can be found in \cite{WangJenny3D}.
	%
	\\
	This method operates on cells that are formed by the basic nodal pints.
	What distinguish this schemes from the others is, that the velocities, that are defined in the corners of a cell, are not continuous across different cells.
		
	
		\subsubsection{Cell Velocities}~\label{sec:discretization:step6:JM:cellVel}
		In this paragraph we will discuss, how the velocities in each cell are constructed.
		At each corner four cells intersects, although the four velocities are defined at the same point, they are not required to be same, which results in discontinuous velocities.
		%
		\\
		To be precise at a corner, at least at an inner one, there are not just four different velocities but $8$.
		Because we have four $x$ and four $y$ velocities.
		To keep things simple, we will only consider one velocity component.
		The other component is handled in the same way.
		
		We again have the problem, that velocities computed by the second step, are not at the right positions.
		The computed velocities are located in the middle of the cell faces.
		%			
		\begin{figure}[h]
			\centering
			\begin{minipage}{.5\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__discretisation__step6/step6__jm__cell__fig11.png} 
				\caption
				{
					Schematic drawing of the velocities located on a basic nodal cell.
					$U_{1}^{a,c}$, $U_{1}^{b,c}$ and $U_{2}^{a,b}$, $U_{2}^{c,d}$ were computed by the Stokes solver.
					Taken from \cite{Jenny1999}, Fig. 11.
				}
				\label{fig:discretization:step6:JM:cell}
			\end{minipage}
		\end{figure}
		%
		The situation, for the second component, is shown in figure \ref{fig:discretization:step6:JM:cell}.
		%
		\\
		A first idea is to assume that the velocity inside the whole cell is constant, thus we have $U_{2}^{a} = U_{2}^{b} = U_{2}^{a,b}$.
		This results in first order accuracy.
		
		\begin{figure}[h]
			\centering
			\begin{minipage}{.7\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__discretisation__step6/step6__jm_minmod__fig12.png} 
				\caption
				{
					Reconstructed (discontinuous) velocities at an intersection of two cells.
					Taken from \cite{Jenny1999}, Fig. 12.
				}
				\label{fig:discretization:step6:JM:minMod}
			\end{minipage}
		\end{figure}
		%
		However we would like to have second order accuracy.
		For this we need to linearly reconstruction the velocity, but for this \emph{two} velocity nodes are needed.
		We can take either use the node from the left or right cell.
		Considering figure \ref{fig:discretization:step6:JM:minMod}, we can thus formulate the following \emph{two} slopes
		%
		\begin{align}
			s^{(l)} = \frac{U_{2_{i,j}}^{a,b} - U_{2_{i-1,j}}^{a,b}}{ x_{1_{i,j}} - x_{1_{i-1,j}} } ,
				\qquad\qquad
			s^{(r)} =  \frac{U_{2_{i+1,j}}^{a,b} - U_{2_{i,j}}^{a,b}}{ x_{1_{i+1,j}} - x_{1_{i,j}} } .
		\end{align}
		%
		While both slopes are located at the same point, they have different interpretations.
		$s^{(l)}$ assumes that the flow on the left side of the cell is more representative for the current cell than the flow on the right side.
		Thus it uses the velocity value from the left cell, to approximate the slope.
		The other slope, $s^{(r)}$, assumes that the flow on the right side is more representative and uses the right velocity.
		%
		\\
		This means that the velocity value $U_{2_{i,j}}^{b}$, can be approximated in two different ways
		\begin{align}
			U_{2_{i,j}}^{b(l)}	&= U_{2_{i,j}}^{a,b} + s^{(l)} \cdot \frac{x_{1_{i,j}} - x_{1_{i-1/2,j}}}{ x_{1_{i+1/2,j}} - x_{1_{i-1/2,j}} }  ,	\label{eq:discretization:step6:JM:leftApprox} \\
			U_{2_{i,j}}^{b(r)}	&= U_{2_{i,j}}^{a,b} + s^{(r)} \cdot \frac{x_{1_{i+1, j}} - x_{1_{i-1/2,j}}}{ x_{1_{i+1/2,j}} - x_{1_{i-1/2,j}} }  . \label{eq:discretization:step6:JM:rightApprox}
		\end{align}
		
		The question now is, which one should we select?
		The answer is quite interesting, we will select \emph{the most reasonable}.
		However what is the most reasonable slope to begin with?
		In order to select it, we will use a so called \emph{slope} or \emph{flux} \emph{limiter}\footnote
			{
				A detailed discussion of the working of a flux limiter, is outside the scope of this work.
				Instead we will refer to a wide range of literature on that subject.
				The $\mathrm{minmod}$ limiter, that is used here, for example is described in \cite{RoeMinMod}.
			}.
		
		A lot of different limiters were devised over the years.
		For the reconstruction we have decided to use the minmod limiter.
		This limiter is defined as following
		\begin{align}
			\MinMod{A}{B} = 
				\left\{
					\begin{array}{ll}
						A 	& 	\text{for } A \cdot B > 0  \text{ and } \Abs{A} < \Abs{B}	\\
						B 	& 	\text{for } A \cdot B > 0  \text{ and } \Abs{B} < \Abs{A}	\\
						0	&	\text{for } A \cdot B \leq 0	
					\end{array}
				\right. .
		\end{align}
		%
		Using the minmod function, we can combine the two approximations, \eqref{eq:discretization:step6:JM:leftApprox} and \eqref{eq:discretization:step6:JM:rightApprox}, to the following expression
		\begin{align}
			U_{2_{i,j}}^{b}	&= U_{2_{i,j}}^{a,b} + \MinMod{s^{(l)}}{s^{(r)}} \cdot \frac{x_1 - x_{1_{i-1/2,j}}}{ x_{1_{i+1/2,j}} - x_{1_{i-1/2,j}} }   .	\label{eq:discretization:step6:JM:minmodApprox}
		\end{align}
		%
		The minmod limiter will select the slope, that results in the \emph{smallest} change.
		The general idea\footnote{Again, this is only an heuristic description of the working of a limiter.} is, that it is saver to underestimate a variation, than overestimate it.
		The branch of the limiter that returns zero, guarantees that discrete maximums and minimums are preserved.
		%
		\\
		It is also clear that this method will lead to a discontinuous reconstruction of the velocities.
		% END:		Cell Velocities
		%%%%%%%%%%%%%%%%%%%%%%%%
	
	
		\subsubsection{Velocity Interpolation}~\label{sec:discretization:step6:JM:VelInterpol}
		In addition to the more complex computation of the corner velocities, the computation of the advection velocities is also more involved.
		
		The velocity is composed of two parts\footnote
			{
				We do not mean velocity component such as $x$ or $y$ velocity.
			}.
		%
		The first component is obtained by a simple bilinear interpolation from the corner velocities to the marker's position.
		%
		\\
		The second component is a correction term.
		That will ensure that the reconstructed field is conservative, \emph{i.e.} has zero divergence.
		%
		The derivation of that term can be found in \cite{Jenny1999}.
		
		In the following we will only discuss the correction term.
		They are given by
		%
		\begin{align}
			\Delta U_{1} &= \frac{\hat{x}_{1} \, \left(  1 - \hat{x}_{1} \right) }{2} \left( - \left(1 - \hat{x}_{2}\right) \, \widetilde{U}_{1}^{e} - \hat{x}_{2} \, \widetilde{U}_{1}^{f} \right) ,			\label{eq:discretization:step6:JM:corrU1}	\\
			\Delta U_{2} &= \frac{\hat{x}_{2} \, \left(  1 - \hat{x}_{2} \right) }{2} \left( - \left(1 - \hat{x}_{1}\right) \, \widetilde{U}_{2}^{g} - \hat{x}_{1} \, \widetilde{U}_{2}^{h} \right) .			\label{eq:discretization:step6:JM:corrU2}
		\end{align}
		
		In the above equations $\hat{x}_{1}$ and $\hat{x}_2$ are the normalized cell coordinates of the marker.
		The velocities $\widetilde{U}_{1,2}^{\cdot}$ are defined by
		\begin{align}
			\widetilde{U}_{1}^{e} &= \widetilde{U}_{1}^{f} = -\frac{h_{1}}{h_{2}} \left( U_{2}^{a} - U_{2}^{b} - U_{2}^{c} + U_{2}^{d} \right) , 			\label{eq:discretization:step6:JM:tildeU1EF}	\\
			\widetilde{U}_{2}^{g} &= \widetilde{U}_{2}^{h} = -\frac{h_{2}}{h_{1}} \left( U_{1}^{a} - U_{1}^{b} - U_{1}^{c} + U_{1}^{d} \right) 	.		\label{eq:discretization:step6:JM:tildeU2GH}
		\end{align}
		%
		$h_1$ and $h_2$ are the physical extension or grid width, of the cell.			
		% END:		Velocity interpolation
		%%%%%%%%%%%%%%%%%%
	
	
		\subsubsection{Handling Cell Crossings}~\label{sec:discretization:step6:JM:CellCrossing}
		For reaching the intermediate states in RK4 the markers are moved along a straight trajectory.
		While on this trajectory cell crossings are ignored.
				
		In this method however, cell crossings are handled.
		By default markers are moved with RK4, which is not different to the other methods.
		If all four intermediate steps and the final position lies inside the same cell, the move is accepted.
		The move is \emph{also} accepted, if the final position of the marker lies outside its original cell.
		Since the velocity in the new cell was never evaluated, it would have no effect on the marker's movement anyway, as far as RK4 is concerned.
		%
		\\
		However if one of the intermediate steps lies outside the cell, the move is rejected.
		The time step will be reduced and it is tried to move the marker again.
		To guarantee progress, we will switch the integration method, if the time step has become too small.
		
		Instead of RK4, we will use the much simpler, but less accurate forward Euler method.
		The forward Euler method moves the marker on a straight trajectory, so we can easily calculate the time at which the marker will enter the next cell, and move it just as far\footnote
			{
				In order to \emph{numerically} guarantee, that the marker crosses the boundary, the time step is a little bit larger, than necessary.
				When we switch to the Euler method, the time step length has already become quite small.
				So the larger error, of the Euler method, should be compensated, by the much smaller time step that is used.
			}
		as that.
		This is similar to a sub-time stepping method.
		
		% END:		Subtimeing
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% END:		JM
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


	\subsection{Time Step Length Control}~\label{sec:discretization:step6:TimeSteppingControl}
	In this section we would like to discuss how the time step length is controlled.
	This discussion applies for all four methods.
	%
	\\
	The length of the time increment depends on the state fo the simulation and there are several rules for it.
	%
	Three of them are concerned with velocity and one with temperature.
	How and when the rules are applied exactly, is outlined in section \ref{sec:appendix:Corello:UsageOps:Flow:Loop} on page \pageref{sec:appendix:Corello:UsageOps:Flow:Loop}.
	%
	\begin{itemize}
		
		\item 
		The first rule is used to guarantee progress.
		It requires that the time increment is larger than a certain threshold.
		This will bound the time increment from below.
		It is obvious that such a rule guarantee progress.
		However the minimum value should be small enough to resolve the smallest structures that appears.
		
		
		\item
		The second rule provides an upper bound for the time increment.
		It will prevent steps from becoming too large.
		
		
		\item
		The other two rules are static, but this one is dynamic, it limits the \emph{displacement} of markers.
		It will shrink the time step in the case of large\footnote{Always in magnitude.} velocities and but allows for larger time steps if the flow moves slowly.
		%
		\\
		Currently the displacement in $x$ and $y$ are treated separately.
		Let $\gamma_{x}$ be the largest allowed displacement in $x$ direction and $\gamma_{y}$ the one for $y$.
		Then we define the marker movement as
		\begin{align}
			\delta_{\alpha} = \frac{\delta t \cdot \widetilde{v}_{\alpha}}{\hat{h}_\alpha} .		\label{eq:discretization:step6:TimeSteppingControl:displacement}
		\end{align}
		%
		In the equation above, $\alpha$ is the direction, $\delta t$ is the time increment and $\hat{h}_{\alpha}$ is the largest grid spacing.
		We would like to comment on $\widetilde{v}_{\alpha}$, which is not the maximum of the computed (discrete) velocity field, but the maximum of the \emph{absolute} values of the computed velocities.
		%
		Because we divide by the maximum grid spacing $\delta_{\alpha}$ does not have a unit.
		It is expressed in grid width, this means that in case of a coarser resolution, larger \emph{absolute} movements are allowed.
		%
		\\
		The time increment, $\delta t$, is then chosen such that
		%
		\begin{align}
			\delta_{x} < \gamma_{x}	 \qquad\qquad \text{ and } \qquad\qquad \delta_{y} < \gamma_{y}		\label{eq:discretization:step6:TimeSteppingControl:restrict}
		\end{align}
		%
		holds.
		
		
		\item
		The last rule is concerned with temperature changes.
		Since we assume an isothermal setting, we are ignoring temperature anyway, so this rule is never used.
		However for the sake of completes we will discuss it.
		%
		\\
		This rule is applied \emph{after} the three rules above are used.
		The intension of the rule is, to keep the \emph{absolute} temperature change below a certain value.
		%
		If the temperature change is larger as the maximal allowed value, we reduce the time step increment.
		We assume that temperature changes linearly with time.
		%
		\\
		It is important, that if this rule changes the time step increment, we will have to resolve the temperature equation, because the time step enters its formulation.
		A second point is, that this rule is allowed to lower the time step value \emph{below} the minimum value.
		

	\end{itemize}

		
		\paragraph{CFL}
		The Courant-Friedrichs-Lewy-Number or CFL number for short, \cite{Lewy1928}, is used to express how fast information propagates.
		In 2D this number is defined as
		\begin{align}
			\text{CFL} = \frac{\delta t \cdot \widetilde{v}_{x}}{h_x} + \frac{\delta t \cdot \widetilde{v}_{y}}{h_y} .		\label{eq:discretization:step6:TimeSteppingControl:CFL_2d}
		\end{align}
		%
		We see that the third rule will actually limit the CFL number to \emph{at most} $\gamma_{x} + \gamma_{y}$.
		
		This number should be below $1$.
		% END:		CFL number
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	% END:		Time step length control
	%%%%%%%%%%%%%%%%%%%%%%%%%%%

