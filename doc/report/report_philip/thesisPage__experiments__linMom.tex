%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In this file the linear momentum is handled


\section{Conservation of Linear Momentum}~\label{sec:experiments:linMom}
The conservation of linear momentum is a fundamental principle in the universe.
So any method to simulate a physical process should also be able to meet this requirement.


	\subsection{General Situation}~\label{sec:experiments:linMom:setting}
	The general setting of these experiments is illustrated in figure \ref{fig:experiments:linMom:setting:setting}.
	%
	\begin{figure}[h]
		\centering
		%\begin{minipage}{.5\columnwidth}
			\input{./Tikz/thesisPage__experiments/linMomentum/generalSetting.tex}
			\caption
				{
					Illustration of the general setting in the \LinMom{} and \LinMomT{} family of experiments.
				}
			\label{fig:experiments:linMom:setting:setting}
		%\end{minipage}
	\end{figure}
	%
	A ball with a certain initial velocity\footnote
		{
			This is implemented by initially setting the intrinsic marker velocity to this value.
			This is done during the setup step.
		}, 
	in our case $\SI{-4}{\meter\per\second}$, starts at the right side of the domain.
	The ball has a diameter of $\SI{40}{\centi\meter}$.
	Since the ball is supposed to be a rigid object, we used a viscosity of $\SI{1e+8}{\pascal\second}$, which is comparable to pitch.
	%
	\\
	The viscosity of the fluid is very low, we used $\eta_{F} = \SI{18.5e-5}{\pascal\second}$ and $\rho_{F} = \SI{1.2}{\kilogram\per\cubic\meter}$ as density, these values are comparable to air.
	
	We have two concrete settings in this family, that differs only in the density of the ball.
	In the first case, known as \LinMom , the density is just $\SI{7.2}{\kilogram\per\cubic\meter}$.
	In the second case, known as \LinMomT , the density is $\SI{3200}{\kilogram\per\cubic\meter}$.
	%
	\\
	There is also a special version of \LinMomT , where the fluid has zero viscosity.
	
	
		\subsubsection{Discretization}~\label{sec:experiments:linMom:setting:Discretization}
		We will now discuss the other parameters of the simulation.
		%
		\\
		We have used $301$ nodes in $y$ and $101$ nodes in $x$ direction to discretize the computational domain.
		Thus each cell has a spatial extension of $\SI{1}{\centi\meter}$ in each direction.
		%
		\\
		Each cell contains $25$ markers, arranged in a $5 \times 5$ sub grid.
		Additionally we have randomly displaced them from their regular arrangement.
		%
		\\
		The time increment is at least $\SI{40}{\micro\second}$, but is not allowed to be larger than $\SI{1}{\milli\second}$.
		%
		The maximal marker displacement was set to $0.1$, which translates to an \emph{absolute} displacement of $\SI{1}{\milli\meter}$, in each direction.
		Thus the CFL number is at most $0.2$.
		
		
			\paragraph{\LinMomT{} Modifications}
			There are some modification of the \LinMomT{} model.
			%			
			\begin{description}
				
				\item[\LinMomTZEta ]
				This is very similar to the original \LinMomT , but here the fluid has zero viscosity.
				Technically this means that there is no friction between object and fluid.
				
				
				\item[\LinMomTZEtaC ] 
				In this model the viscosity of the fluid is again set to zero.
				The different towards \LinMomTZEta{} is, that we use a coarser grid.
				Instead of $301 \times 101$ nodes, we just use half of that, so only $151 \times 51$ nodes are used.
				This means that the cell width is twice as large as before, and one cell occupies four times as much space.
				%
				\\
				There are two other changes.
				First of all, the number of markers is the same as usual.
				So instead of placing $25$ markers in each cell, there are now $100$ per cell.
				%
				A second change is the time step restriction.
				The maximal displacement was set to $0.01$ in each direction.
				This leads to a \emph{five} times smaller marker movement, than allowed in \LinMomTZEta .
				
					
				\item[\LinMomTZEtaCT ]
				This setting is very similar to \LinMomTZEtaC .
				But the maximum displacement is yet again different, we have set it to $0.05$.
				%
				This means the maximal marker movement, measured in \emph{absolute} distances, is the same as in \LinMomTZEta .
				
			\end{description}
			%
			For all four modifications of \LinMomT , we just simulated one system, using the Jenny--Meyer integrator.	
				
			% END:		Modification
			%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
		% END:		Discretization
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
	% END:		General Situation
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	\subsection{\LinMomT }~\label{sec:experiments:linMom:linMod2}
	We will start our discussion with the second family member.
	In these experiments we want to study, how well linear momentum is preserved.
	However momentum is only conserved, if no external force acts on the body.
	Since we are simulating a full continuum, we have to consider physical effects, such as friction, that will slow down the ball.
	
	Here the ball has a very large density, which means that it has a large amount of kinetic energy.
	Thus there is much energy to sustain the movement and oppose effects that would slows it down otherwise.
	
	\begin{figure}[h]
		\centering
		\begin{minipage}{\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMom2__smothBallRelativeMomPure.png} 
			\caption
				{
					Development of the ball's relative momentum.
					Normalized with respect to the \emph{theoretical} initial momentum (conserved quantity).
					Lines are smoothed with a SavGol smoother.
				}
			\label{fig:experiments:linMom:linMod2:relBallMomentum}
		\end{minipage}
	\end{figure}
	%
	In figure \ref{fig:experiments:linMom:linMod2:relBallMomentum} the momentum of the ball, using different integration schemes, is shown.
	There are two important technical thing about the figure.
	%
	\\
	The values were normalized, such that $1.0$ corresponds to the \emph{theoretical} initial momentum.
	Second we have smoothed the momentum, to get a clearer picture.
	We used a SavGol smoother, \cite{SavGol}, with degree $3$ and a centralized window\footnote
		{
			At the edges we have skewed the window accordingly.
		}
	involving $51$ data points.
	
	Looking at the figure we see two interesting points, that needs further discussion.
	First of all we see that momentum is \emph{not} conserved.
	However, if we looking at the scales, we see that the decay is quite small.
	%
	Second even at the first step, we have less momentum than we should have.
	
	
		\subsubsection{The Initial Momentum}~\label{sec:experiments:linMom:linMod2:initMomentum}
		We have identified two effects, for the initial momentum defect.
		%
		The first effect is related to the way how we initialized the setting.
		The intrinsic velocity of the ball was set to desired value\footnote
			{
				When we wrote ``of the ball'' we actually mean the set of markers comprising the ball. 
				For brevity we will not mentioned it again.
			}.
		However the fluid was initially at rest.
		%
		\\
		So the ball had to accelerate the fluid first, which leads to a momentum transfer from the ball to the fluid, which has lowered the ball's own momentum.
		We have mentioned this in section \ref{sec:experiments:methodology:addedMass} on page \pageref{sec:experiments:methodology:addedMass} before and will discuss it later in more details.
		
		The second effect is that our normalization is not perfect, in fact it is very hard to to find a good one.
		%
		We always wrote \emph{theoretical} initial momentum defined as
		\begin{align}
			p_{B}^{(\text{init})} = v_{x}^{(\text{init})} \cdot \rho_{K} \, \pi \, {R_{K}}^2 .	 \label{eq:experiments:linMom:linMod2:initMomentum:analyticalInitMom}
		\end{align}
		%
		This definition assumes, that the mass or volume is accurately captured by the scheme.
		But the scheme does not really care about the mass or volume of an object, it is only concerned about the density of markers.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMom2__smoothMassError.png} 
				\caption
				{
					Relative deviation of the mass from its expected value of $\rho_{K} \, \pi \, {R_{K}}^2$.
					A negative value mean that the ball is too light.
					The values are normalized with respect to the expected mass of the ball, which is $\SI{402.12}{\kilogram}$. 
					Curves are smoothed with SavGol.
				}
				\label{fig:experiments:linMom:linMod2:initMomentum:massError}
			\end{minipage}
		\end{figure}
		%
		To investigate this further, we have computed the mass of the ball during the simulation.
		Figure \ref{fig:experiments:linMom:linMod2:initMomentum:massError}, shows the deviation from the expected mass from the computed mass, normalized to the expected weight of the ball.
		Further the lines were smoothed.
		A negative values mean that the ball is too light.
		%
		\\
		We see that the fluctuation in the masses, appearing in the momentum plot \ref{fig:experiments:linMom:linMod2:relBallMomentum} as well.
		What is also an important indicator for a mass artefact is, that the relative ordering of the different systems, we see in the mass error plot, is also present in the momentum plot.
				
		In order to verify that this is indeed an artefact of the mass computation and no error in the simulation, we have studied the volume of the ball, where we see a slightly different picture\footnote{Plot is omitted.}.
		%
		As for the mass, the estimated volume is too small, but we expected this.
		What is more importantly, we do not observe fluctuations as we see them in the mass, the volume is mostly constant, with changes in the order of $\SI{1e-10}{\cubic\meter}$.
		We also found, that only $10\%$ of the fluctuating mass could be explained by the changes in volume\footnote{Which indicates the deformation of the rigid ball.}.
		

		We also analysed the velocity of the ball\footnote
			{
				The ``velocity of the ball'' is defined as the mean feel velocity of all marker composing the ball.
			}.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMod2__velError.png} 
				\caption
				{
					Derivation of the mean \emph{feel} velocity from its expected value of $\SI{4}{\meter\per\second}$.
					In all cases the standard deviation of the mean marker velocities were in the order of $\SI{1e-6}{\meter\per\second}$.
					A \emph{positive} error indicates that the ball move too slow.
				}
				\label{fig:experiments:linMom:linMod2:initMomentum:velError}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:linMom:linMod2:initMomentum:velError}, we see the error between the ball velocity and its expected value, which is the initial velocity.
		We have not included any form of error bars, because the standard derivation, in all cases, was in the order of $\SI{1e-6}{\meter\per\second}$.
		%
		As we have pointed out before, in this work velocity is equivalent to momentum up to a scaling constant.
		%
		\\
		The figure is very similar to the momentum plot we presented in figure \ref{fig:experiments:linMom:linMod2:relBallMomentum}.
		%
		We see that the error in \LinMomTZEta{} (dashed green line) is smaller than in the normal version (solid green line).
		Which is somehow expected, but looking at the different integrators, it could probably be an anomaly as well.
		
		% NOTE:
		%  There is also a difference between the two. Meaning that the initial momentum loss is larger in the coarser systems than in the normal system.
		%  Frankly I never noticed that, and so it is missing in this analysis.
		%  However I think that it is also related to grid bluring effect, that made the water heavier.
				
		\FloatBarrier
		% END:		Initial Momentum Problem
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		
		\subsubsection{The Decay}~\label{sec:experiments:linMom:linMod2:momDecay}
		We will now discuss the decay we have seen for the momentum in figure \ref{fig:experiments:linMom:linMod2:relBallMomentum}, as well as in the velocity plot \ref{fig:experiments:linMom:linMod2:initMomentum:velError}.
		%
		\\
		As we have pointed out above, the ball is not moving in a vacuum, but inside a viscous media.
		Thus, there are several physical reasons, which can lead to a decay in momentum.
		%
		In this section we will focuses on the viscous effects, and we will consider them only in a qualitative way.
		
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMom2__velErrorZEta.png} 
				\caption
				{
					Influence of the spatial resolution on the ball's movement.
					All systems, except the solid green line (normal system), had zero fluid viscosity.
					%
					Plot shows the deviation of the mean \emph{feel} velocity from its expected value of $\SI{4}{\meter\per\second}$.
					In all cases the standard deviation of the mean marker velocities were in the order of $\SI{1e-6}{\meter\per\second}$.
					%
					A \emph{positive} error indicates that the ball move too slow.
				}
				\label{fig:experiments:linMom:linMod2:momDecay:velError}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:linMom:linMod2:momDecay:velError} we see the error between the mean velocity of the ball and its expected (initial) velocity.
		We have omitted error bars because the standard derivation of the velocity was very small.
		%
		For all systems the Jenny--Meyer integrator was used.
		
		
		We will now compare the normal system (solid green line), with the \LinMomTZEta{} system (dashed green line).
		In both systems the Jenny--Meyer integrator was used, but in the later, the fluid has zero viscosity.
		%
		\\
		The first thing we notice is, that they are basically the same.
		This indicates to us, that the main cause of the slow down is not friction between fluid an ball.
		%
		Instead we think that a different form of viscosity is present, that is an artefact of the mapping process, we call it \emph{ghost viscosity}.
		%
		\\
		In the first step of the scheme, properties from the markers are mapped to the grid.
		As we have explained in section \ref{sec:discretization:step1:areaOfDependency}, a node is influenced by all markers in its vicinity, we called this the domain of dependency (DoD).
		%
		If a node is only surrounded by markers of one type and all have the same value, then the node will have that value too.
		%
		Now assume a node, that is located outside the ball, however near its vicinity.
		Clearly the viscosity for that node, should be the same as the surrounding media (fluid).
		However since its DoD will contain ball markers as well, which have a very high viscosity, the viscosity at the node will be artificially increased.
		%
		It is also important that this effect does not only affect viscosity, all quantities are affected by it, with varying implications.
		However in the current work, we have focused the on viscosity.
		%
		\\
		Another interpretation of this effect is, that a coarser resolution of the grid, will blur the surface of the ball more than a finer resolution would do.
		 
		
		To asses this effect, we will use the \LinMomTZEtaC{} and \LinMomTZEtaCT{} systems.
		In these systems the grid spacing is twice as large as in the other systems.
		However the time stepping\footnote
			{
				Actually, we restrict the displacement of the markers and not time.
				However, from a theoretical point, the velocities are in both cases are the \emph{same}.
				Thus a smaller marker displacement, will lead to smaller time increments that are performed.
			}
		is either the same or finer, expressed in absolute terms, as in the normal configuration.
		%
		\\		
		Due to the larger grid spacing, the DoD will be larger and more markers will contribute to the value of a given node.
		Thus more high viscosity markers from the ball, will contribute to near ball nodes.
		%
		\\
		As we can see from figure \ref{fig:experiments:linMom:linMod2:momDecay:velError} the velocity error in \LinMomTZEtaCT{} has indeed increased, which means that more momentum is removed from the ball.
		%
		It is also interesting to compare it against \LinMomTZEtaC{}.
		We see that both behave the same, which indicates that most likely it is not an effect caused by the time discretization, but is due to the grid.
		
		\FloatBarrier
		% END:		Momentum decay
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	\FloatBarrier
	% END:		Lin Momentum 2
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	\subsection{\LinMom }~\label{sec:experiments:linMom:linMod}
	We will now discuss the results for the \LinMom{} family.
	What distinguish this family form the \LinMomT{} experiments, is that the ball is lighter, its density is just $\rho{F} = \SI{7.2}{\kilogram\per\cubic\meter}$.
	%
	\\
	Since the external conditions are the same as before, we expect that the same momentum decreasing effects are present in \LinMom{} as in \LinMomT .
	However since the ball is much lighter, there is much less energy to sustain its movement.
	Thus the effects should be much stronger than before.
	
		
	\begin{figure}[h]
		\centering
		\begin{minipage}{\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMom__relativeBallMom.png} 
			\caption
			{
				Development of the relative ball momentum in \LinMom .
				Normalized with respect to the theoretical
initial momentum (conserved quantity).
			}
			\label{fix:experiments:linMom:linMod:relBallMomentum}
		\end{minipage}
	\end{figure}
	%
	In figure \ref{fix:experiments:linMom:linMod:relBallMomentum} we see the relative ball momentum, normalized to the \emph{theoretical} initial momentum.
	%
	\\
	Compared it to the \LinMomT{} model, we see that the decay is much stronger than before and the defect in initial momentum is larger.
	We have expected both of them.
	However on a functional level the plot is the same as \ref{fig:experiments:linMom:linMod2:relBallMomentum}.
		
	
		\subsubsection{Initial Momentum}~\label{sec:experiments:linMom:linMod:initMom}
		When we look at figure \ref{fix:experiments:linMom:linMod:relBallMomentum}, we see an effect that we have observed before, but is much stronger this time.
		Even at the very beginning the ball lacks a substantial amount of its momentum.
		
		The effect is again mostly caused by the initial condition.
		The fluid, initially at rest, must be accelerated at the beginning, which deplete the momentum of the ball.
		%
		The larger discrepancy is also according to our expectations.
		Since the amount of fluid, that has to be accelerated, is the same in both families, but \LinMom{} has less energy, a larger share of it will be consumed.
				
		% END:		Initial momentum
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		
		\subsubsection{Slowing Down}~\label{sec:experiments:linMom:linMod:slowDown}
		We will now investigate the slow down of the ball.
		We want to emphasize, that here we only studying the \emph{process} and not really investigate its cause.
		%
		For this we will use the concept of the added mass, which was introduced in section \ref{sec:experiments:methodology:addedMass} on page \pageref{sec:experiments:methodology:addedMass}.
		This mass describes the portion of the media, that is influenced by the body.
		%
		\\
		This means that if, for \emph{whatever} reasons, the ball changes its momentum, the momentum of the fluid has to change as well.
		This allows us to relate \emph{any} change in the ball's velocity to a change in the fluids momentum.
		And thus enables us to see, if the change happens in a physical meaningful way.
		%
		However this method does not allow us to verify, if the slow down was \emph{initiated} correctly.
		
		
		To compute the forces, we have computed the momentum change of the ball and fluid.
		For this we have used the SavGol smoother, which is able to compute the smoothed derivative directly.
		We used a degree\footnote
			{
				Since we compute the derivative, the degree that was used to smooth the curve is actually one less.
				This is because of the differentiation process, that reduces the degree of the smoothing curves by one.
			}
		of $5$ and $70$ data point for the window.
		
		Below the results of these calculations are shown, we have decided to show the integrators separately.
		%
		The green line shows the force that \emph{should} be exerted on the fluid.
		Its is caused by the change in the velocity of the ball\footnote{For which we use the mean feel velocity.} and the added mass, it is given by
		\begin{align}
			F_{v} = \hat{m} \cdot \diff{U_{B}}{t} = f \, \rho_{F} \, V_{K} \cdot \diff{U_{B}}{t}.	\label{eq:experiments:linMom:linMod:pressForce:Fv}
		\end{align}
		%
		\\
		The red line is the force\footnote
			{
				It is actually the negative of that, this is a consequences of the pressure, that couples the two.
			}
		that the fluid experiences.
		It is calculated by numerical differentiation of the fluid's momentum.
				
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMom__VirtForce_vs_PressForce__jm.png} 
				\caption
				{
					Comparison between force acting on the ball ($F_{v}$) and force acting on the fluid ($F_{p}$).
					Jenny--Meyer integrator is used.
				}
				\label{fix:experiments:linMom:linMod:pressForce_jm}
			\end{minipage}
		\end{figure}
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMom__VirtForce_vs_PressForce__std.png} 
				\caption
				{
					Comparison between force acting on the ball ($F_{v}$) and force acting on the fluid ($F_{p}$).
					\emph{Cell centred velocity} (\texttt{std}) integrator is used.
				}
				\label{fix:experiments:linMom:linMod:pressForce_std}
			\end{minipage}
		\end{figure}
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMom__VirtForce_vs_PressForce__LinP.png} 
				\caption
				{
					Comparison between force acting on the ball ($F_{v}$) and force acting on the fluid ($F_{p}$).
					LinP integrator is used.
				}
				\label{fix:experiments:linMom:linMod:pressForce_LinP}
			\end{minipage}
		\end{figure}
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__linMom/linMom__VirtForce_vs_PressForce__test64.png} 
				\caption
				{
					Comparison between force acting on the ball ($F_{v}$) and force acting on the fluid ($F_{p}$).
					Test64 integrator is used.
				}
				\label{fix:experiments:linMom:linMod:pressForce_test64}
			\end{minipage}
		\end{figure}
		%
		As we can see from the figures \ref{fix:experiments:linMom:linMod:pressForce_jm} to \ref{fix:experiments:linMom:linMod:pressForce_test64}, the systems behave as they should.
		This indicates that the slow down is indeed physically and captured correctly by the simulation.
		%
		\\
		Looking at the figures, it is hard to tell, which of integrators is the best, however we see from figure \ref{fix:experiments:linMom:linMod:pressForce_std}, that the standard integrator is probably the worst.
		
			
			\paragraph{Possible Source for the Decay}~\label{sec:experiments:linMom:linMod:slowDown:sources}
			It is clear that friction between the ball and fluid is a source of momentum decay.
			However we have also looked at the pressure distribution during the simulation.
			%
			There we see centres of negative pressure behind the ball, which will oppose the movement of the ball, and we think that they one source of the slow down.
			
			We will not discuss them here further, but instead point the reader towards section \ref{sec:experiments:constBall:Similarities:turb} on page \pageref{sec:experiments:constBall:Similarities:turb}, where we will look at such systems again.
			
			% END:		Possible source
			%%%%%%%%%%%%%%%%%%%%%%%%%
		
		\FloatBarrier
		% END:		Slowing down
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
	\FloatBarrier
	% END:		LinMOm	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	\subsection{General Conclusion About the Linear Momentum}~\label{sec:experiments:linMom:genConclusion}
	Results for \LinMomT{} shows that the momentum is decaying.
	However we have to keep in mind, that figure \ref{fig:experiments:linMom:linMod2:relBallMomentum} is very suggestive.
	To see this we just have to look at the scales, this shows that momentum is decreased only by about $0.5\%$ of its initial value, including the initial acceleration of the fluid.
	%
	\\
	Since we are simulating a viscous continuum, we have to expect some slow down.
	The main question is, if this decay is physical or not\footnote
		{
			The decay by the ghost viscosity is an artefact and strictly speaking not physical.
			However it is an inherent problem of the method, so we consider it as a semi-physical effect.
		}?
	%
	The experiments we have conducted indicating that at least some part of the decaying is physical and can be explained by grid artefacts or viscosity itself.
	However we suggests a more quantitative investigations on this subject.
	%
	\\
	As an example we propose to perform a series of new experiments similar to \LinMomT , but with successively increased fluid viscosity.
	
	The \LinMom{} experiments showed a strong decay of the momentum.
	However as we have shown, this decay happens in a physical \emph{plausible} way.
	%
	The ball does not simply decelerate, in the process fluid is also decelerated.
	We have also seen that the slowing down of the ball and the forces on the fluid matches each other.
		
	% END:		General Conclusion
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	