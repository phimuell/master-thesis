%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% in this file we describe the idea of the used scheme

\section[Used Scheme]{\emph{Marker}--\emph{in}--Cell Scheme}~\label{sec:GoverningEq:UsedScheme}
In this section we will discuss the main ideas behind the scheme.
This introduction is very general and does not contains any specific details.
We have split the scheme into several sub steps, in this section we will describe the requirements for each of them.
%
\\
However to get a \emph{working} method, we must define a concrete strategy to perform each step.
Such a description is given in chapter \ref{chap:discretization} starting on page \pageref{chap:discretization}.
%
A discussion of the implementation, will be presented in chapter \ref{chap:appendix:EGD} and \ref{chap:appendix:Corello}.

This section is build on \cite{gerya_mic}, but there only the Stokes equations are considered.
Since we are concerned with solving the \emph{Navier}--Stokes equations, we need to extend the scheme.
The changes needed to do this can be found in section \ref{sec:GoverningEq:UsedScheme:extendingScheme}.



			\paragraph{Different Lagrangian Schemes}~\label{sec:GoverningEq:UsedScheme:diffSchemes}
			Over the past few decades, a lot of methods were invented to solve the Stokes and the Navier--Stokes equations.
			A particular flavour of them splits the problem in an Eulerian and an Lagrangian part.
			%
			Their names are very similar as well as their working, however there are very subtle differences between them.
			The different types can roughly be split in three groups.
			\begin{description}
				
				\item[Particles--\emph{In}--Cell (PIC)]
				In the PIC method, which was introduced in the mid fifties, \cite{EvansHarlow_1957}, markers takes on an active role.
				A mass is assigned to them, and the density within a cell is derived from it.
				The equations are then solved on the grid.
				%
				\\
				A similar method is the so called ``smooth particle hydrodynamics'' (SPH) method, which was introduced in the mid seventies for astrophysical simulations, \cite{SPH1} and \cite{SPH2}.
				There a particle has a kernel, usually a Gaussian, attached to it, that blur the particle.
				The kernel indicates the influence of the particle on its surrounding.
				It was later applied to different fields, such as fluid mechanics \cite{SPH3} or the cutting of metal \cite{mfreeIWF}.
				
				
				\item[Markers--\emph{And}--Cell (MAC)]
				The MAC methods were introduced in the sixties for incompressible flows, \cite{harlowWelch_1965}.
				A fundamental difference between MAC and PIC is the role of the markers.
				In MAC markers are \emph{passive} particles without any mass.
				They are primarily used to keep \emph{track} which cell contains fluid and which not.
				A cell is considered to contain fluid, if at least one marker is inside it.
				If a cell contains fluid, the physical properties, such as density, of the fluid are assigned to it.
				%
				One of the main goals of this method was to handle free surfaces, which are defined by a fluid cell located next to an empty cell.
				The equations are solved on the grid, but only in cells containing fluid.				
				
				
				\item[Markers--\emph{In}--Cell (MIC)]
				The MIC\footnote
					{
						The term MIC is often used interchangeable with MAC, however these two methods are different, see \cite{Pusok2015}.
					}
				method, can be seen as some kind of blend between PIC and MAC, \cite{gerya_mic}.
				%
				As in MAC, markers are passive particles that are advected through the domain.
				However they are not used to label cells as material $A$ or $B$.
				%
				As in PIC, markers are carrying properties, but have neither a mass nor extension.
				All properties defined in a grid cell, are influenced by the markers within that cell.
				What is also important here is, markers are supposed to be everywhere\footnote
					{
						Which can lead to the interesting task of creating ``empty space'' markers.
					},
				including regions with noting of interest, such as air.				
				
			\end{description}
			%
			In this work the MIC, as it is described in \cite{gerya_mic} and \cite{gerya_2019}, is used.			
			
			% END:		Diff lagrangian schemes
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	\FloatBarrier
	\subsection[The Scheme]{A High Level Description of the Scheme}~\label{sec:GoverningEq:UsedScheme:TheScheme}
	The scheme can be subdivided into several steps.
	First a setup step is performed before the evolution loop starts.
	%
	Each execution of the loop will advance the system a certain time into the future.
	The loop is repeated until a termination criteria, such as final time, is reached. 
	%
	\begin{figure}[h]
		\centering
		\begin{minipage}{.9\columnwidth}
			\input{./Tikz/thesisPage__govEq/flowCartOfMethod.tex}
			\caption{Schematic description of the different steps of the ``marker--in--cell'' method.}
			\label{fig:GoverningEq:UsedScheme:TheScheme:TheSteps}
		\end{minipage}
	\end{figure}
	%
	In the above figure, some aspects are omitted, for example persistence, \emph{i.e.} the writing of the simulation state to disc or the time step control are missing.
	At this point they are not important and we will discuss them in later sections.
	However a description that involves \emph{all} aspects of the loop is given in section \ref{sec:appendix:Corello:UsageOps:Flow:Loop} on page \pageref{sec:appendix:Corello:UsageOps:Flow:Loop}.
	

		\subsubsection{Important Concepts}~\label{sec:GoverningEq:UsedScheme:TheScheme:impConcepts}
		Before we begin, some terminology is needed.
		The concepts we will introduce here, are important and used throughout in this work.
		%
		First of all it is important to understand, that the method consists of two parts, that are interwoven.
		It will also contains some implementation details, but we try to keep them to a minimum.

		The first part happens on \emph{the grid}\footnote
			{
				Actually a more accurate description would be \emph{Eulerian part}.
				Technically a grid is not needed, we could handle it also with finite elements, which uses a mesh.
				However in this work we use finite differences, this means we have already made a particular choice.
			}.
		To be honest, since a staggered grid is used, we actually have several different grids.
		But at this point it is only important, that there is some kind of grid, that is used to solve the equations, we will discuss them in section \ref{sec:discretization:basics:Grids} on page \pageref{sec:discretization:basics:Grids}.
		%
		\\
		A property, such as density, that is defined on the grid, is only known at discrete points, known as grid nodes.
		The values of a property \emph{between} two grid nodes is not known, and must be \emph{reconstructed}.
		Such a (discrete) property, defined on \emph{a} grid is simply called \emph{grid property}.
		%
		Since we operates on two dimensions, we need \emph{two indexes} to access a property on a specific grid.
		Such an object is commonly known as matrix or second order tensor.
		%
		\\
		Let \texttt{A} be such an object then \texttt{A[i, j]} is the value of that property at grid node $(i, j)$\footnote{Which grid is meant exactly will be usually clear form the context.}.
		%
		Thus for each property on \emph{a} grid there is a separate \emph{grid property object}.
		As a consequence of using staggered grids it can happens, that different grid property objects represent the same \emph{physical} property, however \emph{object} represents the property on a different grid.
		Thus a grid property object is \emph{uniquely} identified by its  ``property'' \emph{and} ``grid type'', and not by its ``property'' alone.
		
		
		The second part of the scheme are the markers\footnote
			{
				More generally known as \emph{Lagrangian part}.
			}.
		They are used to transport information through the domain, it is important that they are \emph{passive particles}.
		Thus no force or pressure acts on them, however they are advected by the flow.
		%
		\\
		Conceptionally they are easier to understand than grid properties, because each physical property exists only once.
		It is important that we assume that markers have a \emph{fix}, consecutive indexing\footnote 
			{
				In this document we assume that the indexing starts at $1$ and runs to $N_m$, the number of markers.
				This will ease the notation of formulas involving markers.
				%
				\\
				But on code level, we use a zero based indexing, that runs from $0$ to $N_m - 1$.
			}.
		Each property, that is defined on the marker, is represented by a \emph{single} array.
		Thus let \texttt{A} be such an property array, then \texttt{A[m]} is the value of marker \texttt{m}.
		We would like to remark that also \emph{position} is a property.
		%
		\\
		The markers can be seen as a statistical sample set of the fluid parcels that compose the flow.
		% END:		Concepts
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%


		\subsubsection{$0)$ The Setup Step}~\label{sec:GoverningEq:UsedScheme:TheScheme:Step0_setup}
		In this step the simulation is configured.
		This is a very specific task, that depends on the exact situation that should be modelled.
		
		But it involves the distribution of markers inside the computational domain\footnote{It is advised to randomly displace them, which can be seen as applying stratified sampling of their position.}.
		In this step the marker properties are set to their initial values.
			
		% END:		Setup step
		%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$1)$ Mapping from Markers to the Grid}~\label{sec:GoverningEq:UsedScheme:TheScheme:Step1_mapToGrid}
		In this step the markers are connected to the grid.
		%
		\\
		The flow may be represented by markers, but the governing equations are solved on the grid.
		So we must first map the properties from the markers, where they are defined, to the grid, where they are needed, before we can do anything.
		
		So in order to determine the value at a grid node, the values on markers, in the vicinity of said node, will be processed in some way to get one value.
		%
		\\
		How large this ``vicinity'' is, depends on the setting and also on the property, so different strategies could be used. 
		However most of them use \emph{a} distance between the marker and the node to compute a weight, that describes the influence of a marker on a node.
		We will discuss some strategies in section \ref{sec:discretization:step1} on page \pageref{sec:discretization:step1}.
				
		% END:		Map to grid
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$2)$ Solving the Momentum Equations}~\label{sec:GoverningEq:UsedScheme:TheScheme:Step2_solveMomentum}
		In this step we are solving the momentum equation together with the continuity equation.
		%
		\\		
		For this we use a finite difference (FD) approach to discretization the equations.
		However it is possible to use \emph{any} method\footnote
			{
				It is quite clear, that there are certain compatibility constraints.
				If the, lets call it, output format of this step has been changed, then other steps have to be modified to handle the new format.
				But beside this technical considerations, no restriction on the method are applies. 
			}.
				
		Using FD to solve the Stokes equation, will result in a linear system of equations, one equation for each grid node.
		The resulting system matrix is sparse, how sparse exactly depends on the stencil, in tow dimensions it is still possible to use a direct matrix solver to find the solution.
				
		At this point we want to remark, that even if the Navier--Stokes equations are solved, the resulting system of equations remains linear.
		We will explain how this is done in section \ref{sec:GoverningEq:UsedScheme:extendingScheme} on page \pageref{sec:GoverningEq:UsedScheme:extendingScheme}.
		The intuitive explanation for this is, that the non--linearity is handled by the advection of markers.

		At the moment it is just important that this step will produce a velocity and pressure field, that is defined on the grid.
						
		% END:		Solve Stokes
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$3)$ Solving the Temperature Equation}~\label{sec:GoverningEq:UsedScheme:TheScheme:Step3_solveTemp}
		As we have seen in section \ref{sec:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm} on page \pageref{sec:GoverningEq:TheNSEQ:ContEquations:Energy:sourceTerm}, the temperature equation depends on the velocity field.
		So either, we must solve it simultaneously with the momentum equations or solve it afterwards.
		We have chosen to do the later one.
		
		An interesting observation, that distinguish the temperature equation from the Stokes equations above is, that the equation now contains the material derivative, which introduces a time dependency.
		%
		The material derivative is handled by the markers.
		%
		\\
		As the other properties, temperature was interpolated from the markers to the grid.
		Thus it captures the dynamics of the system.
		
		We discretize the derivative in time with implicit Euler,
		\begin{align}
			\Dt{T}_{i,j} \approx \frac{T_{i,j}^{(n+1)} - T_{i,j}^{(n)}}{\delta t}	.	\label{eq:GoverningEq:UsedScheme:TheScheme:Step3_solveTemp:disc}
		\end{align}
		
		The above approximations has two interesting features.
		First, we will not compute the temperature distribution of the current step based on the last one, because it is already known, instead the distribution of the next step is computed.
		%
		\\
		Second, we approximating the non-linear material derivative, by a simple finite differences quotient.
		%
		The main idea, why we can do this is, because the temperature at a node, is a statistical average, see first step, \ref{sec:GoverningEq:UsedScheme:TheScheme:Step1_mapToGrid} on page \pageref{sec:GoverningEq:UsedScheme:TheScheme:Step1_mapToGrid}, of the surrounding markers.
		So its interpretation is something like ``the mean temperature experienced by the markers in the vicinity of the node''.
		So the difference quotient, \eqref{eq:GoverningEq:UsedScheme:TheScheme:Step3_solveTemp:disc}, above, is nothing more than the ``mean rate of change in temperature experienced by the markers in the vicinity of the node.''
		
		It would also be possible to use an explicit scheme, but then we would need to have a other temperature property, one that represents the \emph{advected old} temperature.
		We will do this for the velocity, when we extend the scheme for the Navier--Stokes equations.
		
		% END:		Solve Temperature
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$4)$ Mapping from Grid to Markers}~\label{sec:GoverningEq:UsedScheme:TheScheme:Step4_gridToMarkers}
		In step $1$, we mapped the field, defined by the markers, to the grid.
		This was necessary to compute the (Eulerian) state of this time step.
		Now after we have computed new field properties, we have to figuring out, how the new marker fields look like.
		
		There are several critical points here.
		The first one is, that not every property needs to be mapped back, only the ones that were computed before.
		%
		\\
		Second this step does not involve moving the markers, the velocity that is mapped to the markers, is not used for moving them, instead it is advected \emph{on} the markers\footnote
			{
				This is already a preview to the extensions that we do for solving the \emph{Navier}--Stokes equations.
			}.
		The advection of the markers will take place in the $6^{th}$ step.
		It is also worth pointing out, that some marker properties are influenced by the material model, which is the $5^{th}$ step.
		
		
		The most obvious approach would be, to reconstruct the fields from the computed nodal value, for example by bilinear interpolation from the nodes to the markers.
		This will completely overwrite the old marker properties.
		However this is susceptible to numerical diffusion, \cite{gerya_2019}.
		%
		\\
		A better way is to just interpolate the \emph{change} back to the marker.
		This will ensure that sub-grid differences between markers are preserved.
				
		% END:		Grid to markers
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$5)$ New Marker Property}~\label{sec:GoverningEq:UsedScheme:TheScheme:Step5_rheology}
		In this step the material model is executed, which influences some of the marker properties.
		In the previous step only properties, that were computed by the two solver steps, were mapped back.
		
		Some property might be influenced by them, a good example for such a property is viscosity.
		From our every day experience, the viscosity of an objects depends on temperature\footnote
			{
				In reality it will depend on many different things too, but for the sake of explanation, we consider only temperature.
			}, 
		butter for example is hard when taken out of the fridge, but gets soft after it has warmed up.
		%
		Such materials can be handled by this step.
		
		% END:		Rehology
		%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		\subsubsection{$6)$ Advecting the Markers}~\label{sec:GoverningEq:UsedScheme:TheScheme:Step6_advection}
		The last step of an iteration advects the markers.
		%
		\\
		Since the velocity field itself depends on the marker configuration, moving them will change the velocity field as well.		
		A conceptional simple approach, is to assume, that the changes are very small and can be neglected.
		So the velocity remains fixed or frozen \emph{in time} during the advection step.
		
		However there are still several things that have to be considered.
		Most importantly, especially in incompressible flows, is marker divergence.
		The velocity field that was computed by the solver is by construction divergence free, so ideally the velocity that moves the markers is divergence free as well.
		There are a lot of methods for this, at this point we recommend \cite{Pusok2015} for a detailed discussion about this issue.
		
		It can also lead to instability at free surfaces, so called \emph{drunken sailor instabilities}.
		%
		They are usually addressed by a stabilization of the solution by an approximating of the density distribution of the next step, this subject is discussed in \cite{KAUS201012}.
		%
		\\
		In the present work we does not have free surfaces, so no stabilization is employed\footnote{However it is implemented, but only for the $y$ component. But it is not used.}.
				
		% END: 		Advection
		%%%%%%%%%%%%%%%%%%%%%%  

	% END:		The scheme
	%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	\subsection[Extending the Scheme]{Extending the Scheme for Solving the Navier--Stokes Equations}~\label{sec:GoverningEq:UsedScheme:extendingScheme}
	In this section we will explain how we can extend the above scheme, such that the Navier--Stokes equations can be solved.
	%
	First of all, the Stokes equations are a \emph{simplified} version of the Navier--Stokes equations.
	They are derived by assuming, that the process is very slow and is dominated by \emph{viscous} effects.
	Thus the material acceleration, $\Dt{\Vek{v}}$, is very small and can be neglected.
	%
	As a side effect, this simplification will remove the \emph{non--linear} from the equations.
	
	Since we are able to solve the Stokes equations, we have to think about, how we can reintroduce the material derivative into the formulation, and keeping the problem linear.
	As we have pointed out in section \ref{sec:GoverningEq:TheNSEQ:ContEquations:Dt}, the material derivative is used to express the change a fluid parcel is experiencing as it moves through the domain.
	%
	\\
	We have seen a similar problem before, when the temperature equation was treated, see section \ref{sec:GoverningEq:UsedScheme:TheScheme:Step3_solveTemp}.
	The idea we will use here is quite similar to that one used there.
	
	
	First of all, inertia is the resistance of an object to any change of its velocity.
	This makes it clear that the markers need to have a velocity, which represents their \emph{intrinsic velocity}.
	This \emph{intrinsic marker velocity} represents the memory or history of velocities that the marker has experienced in the past.
	It is \emph{not} the velocity at which the marker is advected, rather it is advected \emph{on} the markers itself.
	%
	\\
	The velocity that is used to move the markers, is still the one, that was computed by the momentum solver, step $2$, section \ref{sec:GoverningEq:UsedScheme:TheScheme:Step2_solveMomentum}.
	A second important point is, that the velocity field, that was computed by the solver, already accounts for the velocity history of the markers.
	
	When comparing the method, used for formulating the temperature equation, there are some similarities, but also differences.
	We approximate the material acceleration as
	\begin{align}
		\Dt{v}_{i,j} \approx \frac{v_{i,j}^{(n)} - v_{i,j}^{(n-1)}}{\delta t^{(n-1)}}	.	\label{eq:GoverningEq:UsedScheme:extendingScheme:approx}
	\end{align}
	%
	In the above equation $v$ is a general velocity component\footnote
		{
			The procedure is the same for all components, so we will only use $v$ here.
		}.
	The main difference between \eqref{eq:GoverningEq:UsedScheme:TheScheme:Step3_solveTemp:disc} and \eqref{eq:GoverningEq:UsedScheme:extendingScheme:approx} is, that we do not compute the velocity field in the next step, but in the current step.
	%
	\\
	Consequently $v_{ij}^{(n-1)}$ is the velocity from the last step at the node.
	But it is not just the velocity that was computed by the solver, instead it is the \emph{advected} previous velocity.
	%
	See, in the previous step, the velocity was computed at the nodes, then it was mapped back to the markers and they were advected, which was also the end of the previous iteration.
	At the beginning of the \emph{current} iteration, labelled as $n$, the marker velocities were mapped to the grid, and stored in $v_{i,j}^{(n-1)}$.
	This velocity can be seen as ``the mean velocity that the markers have experienced in the previous step, that are \emph{now} located in the vicinity of the node.''
	As we have said before, inertia is the resistance of objects to changes of their velocity, so $v_{i,j}^{(n-1)}$ is the velocity the markers would like to maintain.
	%
	\\
	A further point involves $\delta t^{(n-1)}$, this is a time step increment.
	However it is not the increment that is used to advance the system form step $n$ to $n+1$.
	Instead it is the time step increment that was used to advance the system from the previous to the current step.
	
	
		\subsection[Feel Velocity]{Velocity for Moving the Markers}~\label{sec:GoverningEq:UsedScheme:extendingScheme:feelVel}
		There is a crucial point that deserves some more explanation.
		The velocity that is located on the markers and is advected on them, is known as the \emph{intrinsic} velocity (of a marker).
		%
		\\
		However this is not necessarily the velocity that is used to actually move the markers.
		For simplicity we call this velocity \emph{feel velocity}.
		%
		It is an \emph{instantaneous} velocity that acts on the markers.
		The intrinsic velocity on the other hand, is more like a \emph{memory}, that stores the velocity that the marker would like to maintain.
		
		Usually the intrinsic velocities are updated by mapping velocity changes back to the markers, which is done in the $4^{th}$ step.
		The feel velocity is usually computed with \emph{the} Runge-Kutta $4^{th}$ order method\footnote 
			{
				This is a multi step method, that works with intermediate steps.
				We also use the frozen velocity assumption here, this means that the velocity field is not recomputed for the intermediate steps.
				Thus the integration is fourth order in space, but only first order in time.
				%
				\\
				Recomputing the velocities for intermediate steps, would require us to perform all steps again.
				It will increase the time accuracy to fourth order, but also increase the computational costs, which is why we do not do it.			
			}, 
		which is done in the $6^{th}$ and last step..
		
		% END:		Feel Vel
		%%%%%%%%%%%%%%%%%%%%%%
		
	% END:		Extending Scheme
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

