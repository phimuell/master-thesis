%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In this setting we test the angular momentum

\section{Conservation of Angular Momentum}~\label{sec:experiments:angMom}
In the last section, we have studied how \emph{linear} momentum is conserved, now we will discuss \emph{rotational} momentum.


	\subsection{General Setting}~\label{sec:experiments:angMom:setting}
	A disc\footnote
		{
			Which is actually a cylinder, with unit height, but we will refer to it as disc.
	}, 
	placed at the origin, rotates inside a box.
	
	\begin{figure}[h]
		\centering
		\input{./Tikz/thesisPage__experiments/rotDisc/generalSetting.tex}
		\caption
		{
			Illustration of the general setting in the \RotDisc{} family of experiments.
		}
		\label{fig:experiments:angMom:setting:setting}
	\end{figure}
	%
	The disc has a radius of $R_{D} = \SI{20}{\centi\meter}$, its density is set to $\rho_{D} = \SI{865.64}{\kilogram\per\cubic\meter}$ and viscosity to $\eta_{D} = \SI{1e+9}{\pascal\second}$.
	%
	The parameters of the fluid are similar to air, density is $\rho_{F} = \SI{1.2}{\kilogram\per\cubic\meter}$ and viscosity is $\eta_{F} = \SI{18.5e-5}{\pascal\second}$.
	%
	\\
	The square box has a side length of $\SI{1.2}{\meter}$, and $121 \times 121$ nodes are used for the discretization.
	Thus the cell width is $\SI{1}{\centi\meter}$ in each direction.
		
	The initial angular velocity of the disc is the system's main parameter.
	In the current work we studied two different speeds.
	%
	\begin{description}
		
		\item[\RotDiscFast ] 
		Here we have $\omega = \SI{30}{\radian\per\second}$.
		For one rotation the disc needs $\SI{0.2}{\second}$.
		
		\item[\RotDiscSlow ]
		Here we have $\omega= \SI{10}{\radian\per\second}$.
		For one rotation the disc needs $\SI{0.6}{\second}$.
		
	\end{description}

	The maximal marker displacement is set to $0.02$ in each direction, thus the CFL number is at most $0.04$.
	%
	\\
	The \RotDiscSlow{} simulation is run for $\SI{2.4}{\second}$ and the \RotDiscFast{} simulation is run for $\SI{1}{\second}$.
	
	
			\paragraph{Initial Cartesian Velocity of the Disc}~\label{sec:experiments:angMom:setting:initDisc}
			The experiments are parametrized by the initial \emph{angular} velocity of the disc, denoted as $\omega^{(i)}$.
			However markers just have Cartesian velocity components.
			%
			In order to convert them we use the following expressions
			\begin{align}
				v_{x} &= \Fkt{v_{x}}{x, y} = -r_{(x, y)} \, \omega^{(i)} \, \Sin{\phi_{(x, y)}} ,		\label{eq:experiments:angMom:setting:initDisc:vx}  	\\
				v_{y} &= \Fkt{v_{y}}{x, y} = r_{(x, y)} \, \omega^{(i)} \, \Cos{\phi_{(x,y)}}   ,		\label{eq:experiments:angMom:setting:initDisc:vy}	\\
				\phi_{(x,y)} &= \ATanTwo{y}{x} 													, 		\label{eq:experiments:angMom:setting:initDisc:phi}  \\
				r_{(x,y)}    &= \sqrt{x^2 + y^2}                                                .    	\label{eq:experiments:angMom:setting:initDisc:distOrigon}  
			\end{align}
						
			% END:		Initial Disc
			%%%%%%%%%%%%%%%%%%%%%%%
	
	
			\paragraph{Initial Condition of the Fluid}~\label{sec:experiments:angMom:setting:initFluid}
			For this experiment we have decided, that some part of the fluid should rotate.
			Its velocity is given by 
			%
			\begin{align}
				\Fkt{v_{\phi}}{r} = 
					 \left\{
						\begin{array}{ll}
							1 						& \text{if } r \leq R_{D} 			\\
							A \, r + \frac{B}{r}  	& \text{if } R_{D} < r \leq R_{q} 	\\
							0 						& \text{else}
						\end{array}
					\right.  .	\label{eq:experiments:angMom:setting:initFluid:angVel}
			\end{align}
			%
			The equation above describes the azimuthal velocity of a Taylor--Couette flow, equations \eqref{eq:experiments:angMom:setting:initFluid:vx} and \eqref{eq:experiments:angMom:setting:initFluid:vy} can be used to transform it into Cartesian velocities.
			%
			\\
			$r$ is the distance from a point to the origin and is calculated according to \eqref{eq:experiments:angMom:setting:initDisc:distOrigon}.
			In the classical Taylor--Couette flow $R_{q}$ is the radius of the outer cylinder.
			However in our case, we do not have a second cylinder, instead it is the distance at which the fluid stops rotating, we define it as $R_{q} := \SI{54}{\centi\meter}$.
			%			
			\begin{align}
				v_{x} &= \Fkt{v_{x}}{x, y} = -\Fkt{v_{\phi}}{r_{(x, y)}} \, \Sin{\phi_{(x, y)}}		\label{eq:experiments:angMom:setting:initFluid:vx} 	\\
				v_{y} &= \Fkt{v_{y}}{x, y} = \Fkt{v_{\phi}}{r_{(x, y)}} \, \Cos{\phi_{(x,y)}}		\label{eq:experiments:angMom:setting:initFluid:vy}
			\end{align}
			%
			The parameter $A$ and $B$ in equation \eqref{eq:experiments:angMom:setting:initFluid:angVel} are defined as
			%
			\begin{align}
				A     &= \omega^{(i)} \,                \frac{-\zeta^{2}}{1 - \zeta^2}	,		\label{eq:experiments:angMom:setting:initFluid:CT_A}	\\
				B     &= \omega^{(i)} \, {R_{D}}^{2} \, \frac{1         }{1 - \zeta^2}	,		\label{eq:experiments:angMom:setting:initFluid:CT_B}	\\
				\zeta &=                                \frac{R_{D}     }{R_{q}      }	.		\label{eq:experiments:angMom:setting:initFluid:CT_zeta}
			\end{align}
			
			% END:		Fluid Initial
			%%%%%%%%%%%%%%%%%%%%%%
			
			
			\paragraph{Coarse Systems}~\label{sec:experiments:angMom:setting:CoarseSystem}
			As we have done it for the linear momentum, we also looked at systems with a coarser grid resolution.
			Instead of $121 \times 121$ nodes, we just used $61 \times 61$ nodes to discretize the box.
			Thus each grid cell has a side length of $\SI{2}{\centi\meter}$, which makes each cell four times larger, than before.
			%
			\\
			The marker density was also increased such that the total number of markers stayed the same.
			
			The maximal allowed marker displacement was also reduced to $0.01$ for each direction.
			This means, that the \emph{absolute} marker displacement is the same as before.
			
			% END:		Coarse Sytem
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
			
			\paragraph{Long Time Step Systems}~\label{sec:experiments:angMom:setting:longTimeStep}
			In the coarse systems we are studying the influence of the spatial discretization.
			However since we also decrease the maximum allowed marker displacement, the time step increment will stay roughly the same.
			%
			\\
			However in the \emph{long time step systems} we will study the influence of the temporal discretization.
			The spatial grid still involves $121 \times 121$ nodes, but a different marker displacement was used.
			We have set it to $0.06$, which increases the \emph{absolute} marker displacement by the factor three\footnote
				{
					There is an important historical fact, that we would like to discuss here.
					At the beginning \emph{these} systems, were the default systems.
					However we had trouble in understand the results.
					As a solution, we decided to increase the temporal resolution, by shrinking the maximal marker displacement to $0.02$.
					This has indeed solved the issue and allowed us to understand the results much better.
				}.
			
			% END:		Long time step 
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
	
			\paragraph{Boundary Condition}~\label{sec:experiments:angMom:setting:BC}
			On the boundary of the computational domain we imposed free slip conditions.
			
			% END:		Boundary condition
			%%%%%%%%%%%%%%%%%%%%%%%%
	
	% END:		General Setting
	%%%%%%%%%%%%%%%%%%%%%%%%

	
	\subsection[Brief Theoretical Overview]{Important Equations Regarding Angular Momentum}~\label{sec:experiments:angMom:theory}
	In this section we will discuss the theoretical background of the angular momentum.
	We assume that the centre of rotation is located at the origin.
	
	The most natural way to work with a rotational problem is using $3$ dimension.
	Since we have only $2$ dimensions we assume that the $z$ coordinate is zero as well as the velocity in that direction.
	
		\subsubsection{Discrete}~\label{sec:experiments:angMom:theory:particle}
		We will first discuss the discrete case, in which we consider one or more points rotating around the origin.
	
			\paragraph{Angular Momentum of a Single Particle}~\label{sec:experiments:angMom:theory:particle:singlePart}
			Consider a single particle with mass $m$, Cartesian velocity $\Vek{v}$ and (Cartesian) position $\Vek{r}$.
			Thus its angular momentum is given as,
			\begin{align}
				\Vek{L} &= \Vek{r} \times \Vek{p} = \Vek{r} \times \left(m \, \Vek{v} \right) = m \, \Vek{r} \times \Vek{v} , \label{eq:experiments:angMom:theory:particle:singlePart:VekL}  \\
				L_{z}   &= \left(\Vek{L}\right)_{z} = m \cdot \left( x \, v^{(y)} - y \, v^{(x)} \right) . \label{eq:experiments:angMom:theory:particle:singlePart:Lz}
			\end{align}
			%
			Because of our assumptions all components of $\Vek{L}$ except $L_{z}$ are zero.
			% END:		Single particle
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
			\paragraph{Angular Momentum of Particle Could }~\label{sec:experiments:angMom:theory:particle:manyPart}
			Now consider $N$ particles, rotating around the origin.
			In order to calculate the angular momentum of them, we can simply sum up the individual contributions.
			This gives us
			\begin{align}
				\Vek{L} &= 
					  \sum_{k = 1}^{N} \Vek{L}^{k} 
					= \sum_{k = 1}^{N} \Vek{r}_{k} \times \Vek{p}_{k} 
					= \sum_{k = 1}^{N} \Vek{r}_{k} \times \left(m_k \, \Vek{v}_{k} \right) 
					= \sum_{k = 1}^{N} m_{k} \, \Vek{r}_{k} \times \Vek{v}_{k} , \label{eq:experiments:angMom:theory:particle:manyPart:VekL}  \\
				L_{z}  &= 
				      \sum_{k = 1}^{N} \left(\Vek{L}^{i}\right)_{z} 
				    = \sum_{k = 1}^{N} m_{k} \cdot \left( x_k \, v_{k}^{(y)} - y_k \, v_{k}^{(x)} \right) . \label{eq:experiments:angMom:theory:particle:manyPart:Lz}
			\end{align}

			% END:		Many particle
			%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
			
			\paragraph{Angular Velocity}~\label{sec:experiments:angMom:theory:particle:angVel}
			In section \ref{sec:experiments:angMom:setting:initDisc} we have described how an angular velocity can be transformed into a Cartesian velocity, we will now discuss how we can do the reverse.
			Assume that the a particle is located at position $\Vek{r}$ with velocity $\Vek{v}$ (both Cartesian), then its angular velocity is given as 
			\begin{align}
				\Vek{\omega} &= \frac{\Vek{r} \times \Vek{v}}{r^2}	,	\label{eq:experiments:angMom:theory:particle:angVel:angVelVek}		\\
				     \omega  &= \left( \Vek{\omega} \right)_{z} 
									= \frac{x \, v_{y} - y \, v_{x}}{r^2} .		\label{eq:experiments:angMom:theory:particle:angVel:angVel}
			\end{align}
			
			% END:		Angular velocity
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
		% END: 		Particle
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


		\subsubsection{Continuum}~\label{sec:experiments:angMom:theory:body}
		Now we will discuss the case were we have a continuum instead of individual particles.
		Generally the same formulas as in section \ref{sec:experiments:angMom:theory:particle} can be used, however summation is replaced by integration.
		Assume that the body is described by $\Omega_{B} \subset \R^{d}$, then the angular momentum of it is given as 
		\begin{align}
			\Vek{L} &= \int_{\Omega_{B}} \Vek{r} \times \Vek{p} \udvol{\Vek{r}}
					 = \int_{\Omega_{B}} \Vek{r} \times \left(\rho \, \Vek{v}\right) \udvol{\Vek{r}} \nonumber \\
					&= \int_{\Omega_{B}} \rho \, \Vek{r} \times \Vek{v} \udvol{\Vek{r}} 	\label{eq:experiments:angMom:theory:body:VelLGen} \\
			L_{z}   &= \int_{\Omega_{B}} \rho \, \left( x \, v_{y} - y \, v_{x} \right) \udvol{\Vek{r}} .	\label{eq:experiments:angMom:theory:body:VelLxGen}	
		\end{align}
		%
		All quantities in the above equations depend on the location $\Vek{r}$.
		
		Since we consider only discs, with an uniform mass distribution, in this experiment, we can further simplify \ref{eq:experiments:angMom:theory:body:VelLxGen} and arrive at 
		%
		\begin{align}
			L_{z} = \frac{1}{2} M_{D} {R_{D}}^{2} \cdot \omega^{(i)} 
				  = \frac{\pi}{2} \rho_{D} {R_{D}}^4 \cdot \omega^{(i)} ,			\label{eq:experiments:angMom:theory:body:DiscLx}
		\end{align}
		%
		where $R_{D}$ is the disc' radius and $M_{D}$ its mass\footnote{As height we use $\SI{1}{\meter}$.}, given as $\rho \, \pi {R_{D}}^{2}$.
		
		% END:		Continuum
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	% END:		Theory
	%%%%%%%%%%%%%%%%%%%%%%%%%



	\subsection{Angular Moment}~\label{sec:experiments:angMom:angMom}
	We will now discuss the results about the angular momentum.
	As long as no torque is applied to the rotating body, it should be conserved.
	However since we have a continuum, we \emph{must} expect some decay.
	%
	\\
	For this kind of experiment, we expect that most of the slowing down is generated by friction.
	Further no\footnote
		{
			It is still possible because of some numerical artefacts.
		} 
	energy has to be used to initially accelerate fluid, since it was initialized to its steady state.
	%
	However currently we are not able to measure viscous forces\footnote 
		{
			Calculating the viscous forces is not trivial, but possible.
			However its calculation involves integrating the wall shear stress over the \emph{surface}, which obviously require a useful surface approximation to begin with.
			As we will later see, such a thing is not very easy to get and we were not able to find a good one.
			So we are unable to calculate friction.
		}, 
	and have to relay on the qualitative behaviour of the system.
	
	
		\subsubsection{Slow}~\label{sec:experiments:angMom:angMom:slow}
		In this section we will discuss results obtained for \RotDiscSlow{} systems.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__rotDisc/rotDisc__slowRelAngMomInfluence.png} 
				\caption
				{
					Relative angular momentum in the \RotDiscSlow{} setting.
					Values are normalized with respect to the \emph{theoretical} value.
					%
					Solid lines using the normal time stepping.
					The magenta line shows the coarse grid and the dashed line uses longer time steps.
				}
				\label{fig:experiments:angMom:angMom:slow:relL}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:angMom:angMom:slow:relL} we see the development of the angular momentum\footnote
			{
				This involves the computation of the mass which is a bit unstable. 
				Despite this, we have decided to plot the momentum, since the fluctuations are quite small.
				%
				\\
				We think that the rotational nature of the system, is responsible for limiting the fluctuation.
				But we did not investigate it further.
			}.
		The values are normalized with respect to the \emph{theoretical} angular momentum, given by \eqref{eq:experiments:angMom:theory:body:DiscLx}.
		The dashed line shows the system in which we have used a larger time step\footnote
			{
				As a short reminder, in these systems the maximal marker displacement is not $0.02$ but $0.06$ grid widths in each direction.
				We show only the results using the Jenny--Meyer integrator, since the other three are quite similar.
			}.
		%
		\\
		The magenta line shows the coarse system\footnote 
			{
				There cells are four times as large as usual, but the maximum displacement is also smaller, so the maximal marker displacement in absolute terms, is still the same.
			}.
		%
		The other solid lines show the normal system configuration.
		
		
		The above plot is quite interesting.
		%
		First of all, we notice that the different integrators perform similar, something we also observed for the linear momentum.
		The angular velocity would show a similar picture, however we would also see, that the standard integrator\footnote{Velocity interpolation from the extended cell centre point; red line.} shows a faster decay than the others.
		
		What is also interesting in the above plot is the behaviour of the coarse system (solid magenta line).
		%
		We see two things, first its initial value is lower and and it slows down faster\footnote 
			{
				We performed a linear fit on the normalized angular velocity.
				We found that the slope of the normal setting is $-0.004$, but for the coarse system it is $-0.006$.
			},
		when compared with systems using the normal spatial discretization.
		%
		\\
		The faster slow down is in our view again caused by the larger ghost viscosity in the vicinity of the disc.
		More viscosity leads to larger friction and thus more angular momentum is lost to the fluid.
		%
		\\
		A second effect, that probably also contribute to the slow down is, the ``surface'' of the disc, seen by the grid solver.
		It is not a smooth curve, instead it has steps, that are created by the grid, which also increases the resistance.
		
		Explaining the lower initial value of the coarser system is a bit more complicated.
		%
		We have initialized the fluid, such that it match the rotation of the disc and is already in its steady state\footnote
			{
				Given by a Taylor--Couette flow, albeit without the outer cylinder.
			}.
		So technically there is no need for the disc to accelerate fluid.
		%
		\\
		This is correct in a continuum, but we have a discrete approximation.
		The explanation is also connected to the same effect responsible for the ghost viscosity.
		As we have described before, some inherent problem of the method is, that sharp surfaces are blurt.
		%
		Here this means, that the disc will appear slightly \emph{larger} and has \emph{steps} in it.
		Thus the continuous initialization we have used, does not fully agree with what the method sees.
		%
		In the end, energy has to be used for reaching a discrete initial equilibrium.
				
		As we have mentioned it before, we are simulating a viscous continuum, losses have to be expected.
		However if only a very small time span is considered, these losses should be small.
		%
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__rotDisc/rotDisc__slowRelAngMomInfluenceBeginning.png} 
				\caption
				{
					Relative angular momentum in the \RotDiscSlow{} setting.
					Values are normalized with respect to the \emph{theoretical} value.
					%
					Plot is the same as \ref{fig:experiments:angMom:angMom:slow:relL}, but restricted to the very beginning of the simulation.
				}
				\label{fig:experiments:angMom:angMom:slow:relLBeg}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:angMom:angMom:slow:relLBeg} we see the behaviour of various \RotDiscSlow{} systems at the very beginning.
		It just shows the first sixth of the first revolution.
		%
		As we can see, the angular momentum stays nearly constant, just the coarse system (magenta line) shows some decay.
		This means that the momentum is mostly conserves by the method.
				
		% END:		Slow
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
		\subsubsection{\RotDiscFast }~\label{sec:experiments:angMom:angMom:fast}
		In this section we would like to discuss the results of the \RotDiscFast{} configuration.
		Discs in these systems are rotating at a higher speed, which increases their energy budget.
		However since they rotate faster, we also expect larger velocity gradients, which in turn implies stronger friction.
			% Since the regions in which the fluid has an initial rotating, is the same for both systems.
			% And thus the distance from the centre at which the fluid has to halt.
			% Since here the disc rotates faster, the velocity decay must be faster as well, to meet the boundary condition.
		
		
		\begin{figure}[h]
			\centering
			\begin{minipage}{\columnwidth}
				\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__rotDisc/rotDisc__fastRelAngMomInfluence.png} 
				\caption
				{
					Relative angular momentum of the \RotDiscFast{} setting.
					Values are normalized with respect to the \emph{theoretical} value.
					%
					Solid lines using the normal time stepping.
					The magenta line shows the coarse grid and the dashed line uses longer time steps.
				}
				\label{fig:experiments:angMom:angMom:fast:relL}
			\end{minipage}
		\end{figure}
		%
		In figure \ref{fig:experiments:angMom:angMom:fast:relL}, we see the development of the angular momentum in the \RotDiscFast{} systems.
		We have normalized them according to the \emph{theoretical} expected value.
		%
		The dashed green line, shows the system, where the long time step was used, the magenta line represents the system with the coarser spatial resolution.
		Both of them are using the Jenny--Meyer integrator.
		The other solid lines show the normal systems.
		%
		\\
		The behaviour is pretty similar, to what we have seen before for \RotDiscSlow .
		%
		Thus we will not discuss them in the same details has before.
		
		
			\paragraph{Rate of Decay}~\label{secexperiments:angMom:angMom:fast:rateDecay}
			We expect the slow down to be faster in \RotDiscFast{} than in \RotDiscSlow .
			%
			In a Newtonian fluid, friction linearly depends on strain rate.
			Since in both systems, the same geometries was used, but in \RotDiscFast{} the disc was rotating faster, it had to decay faster as well.
			This implies larger gradients and thus more friction\footnote
				{
					There is an important catch here, the process is no one way road, because the process influences itself.
					It is true that the stronger velocity gradients will lead to a stronger fiction and thus larger slow down.
					But due to the slow down, the velocity gradients will become smaller and thus friction as well.
				}.
			
			We have performed a linear fit on the \emph{relative} angular velocity of several \RotDiscSlow{} \emph{and} \RotDiscFast{} systems.
			The results are showed in table \ref{tab:experiments:angMom:angMom:fast:rateDecay:JM}.
			%			
			\begin{table}[h]
				\centering
				\begin{tabular}{l|c|c}
					 	                &   \RotDiscSlow        & \RotDiscFast       \\ \hline
					\textsc{Norm}       &	$\num{-0.004381}$	& $\num{-0.02169}$   \\ \hline
					\textsc{Long Step}  &   $\num{-0.009775}$   & $\num{-0.03828}$   \\ \hline
					\textsc{Coarse}	    &   $\num{-0.006083}$   & $\num{-0.02665}$
				\end{tabular}
				\caption{%
						Decay rate of several selected systems.
						In all systems the Jenny--Meyer integrator was used.
				}
				\label{tab:experiments:angMom:angMom:fast:rateDecay:JM}
			\end{table}
			% 
			As we can see, the decay is stronger in the \RotDiscFast{} systems, as we have expected.

			% END:		Rate of decay
			%%%%%%%%%%%%%%%%%%%%
			
		% END:		Fast
		%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	\FloatBarrier
	% END:		Angular momentum
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	\subsection{Centre Of Mass}~\label{sec:experiments:angMom:CoM}
	In this section we would like to discuss how the centre of mass (CoM) of the discs behave.
	Technically the CoM is a mass weighted average, see section \ref{sec:experiments:methodology:CoM} on page \pageref{sec:experiments:methodology:CoM} for more, but for this analysis, we will use the arithmetic average instead.
	Let $\Vek{\mathcal{X}}$ be all $x$ coordinates of the markers and $\Vek{\mathcal{Y}}$ all $y$ coordinates.
	%
	\\
	Thus we can define
	%
	\begin{align}
		x_{com} &= \mathrm{mean}\!\left( \Vek{\mathcal{X}} \right)  ,	\label{eq:experiments:angMom:CoM:meanXCoM} 		\\
		y_{com} &= \mathrm{mean}\!\left( \Vek{\mathcal{Y}} \right)  .   \label{eq:experiments:angMom:CoM:meanYCoM}
	\end{align}
	%
	Since two values are cumbersome to handle, we will simplify them further and define the \emph{CoM distance} as
	%
	\begin{align}
		r_{com} = \sqrt{ {x_{com}}^{2} + {y_{com}}^{2} }  .		\label{eq:experiments:angMom:CoM:CoMDist}
	\end{align}
	
	The CoM should be located at the origin.
	But since the disc is represented by a \emph{finite} sample set (markers), this is most likely not the case, thus we expect $r_{com} > 0$.
	However a good method should keep the value constant.
	
	\begin{figure}[h]
		\centering
		\begin{minipage}{\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__rotDisc/rotDisc__slowCoMDist.png} 
			\caption
			{
				CoM distance in the \RotDiscSlow{} systems, values are expressed in centimetres.
				%
				Solid lines using the normal time stepping.
				The magenta line shows the coarse grid and the dashed line uses longer time steps.
			}
			\label{fig:experiments:angMom:CoM:CoMDist:slow}
		\end{minipage}
	\end{figure}
	%
	\begin{figure}[h]
		\centering
		\begin{minipage}{\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__experiments__rotDisc/rotDisc__fastCoMDist.png} 
			\caption
			{
				CoM distance in the \RotDiscFast{} systems, values are expressed in centimetres.
				%
				Solid lines using the normal time stepping.
				The magenta line shows the coarse grid and the dashed line uses longer time steps.
			}
			\label{fig:experiments:angMom:CoM:CoMDist:fast}
		\end{minipage}
	\end{figure}
	%
	In figures \ref{fig:experiments:angMom:CoM:CoMDist:slow} and \ref{fig:experiments:angMom:CoM:CoMDist:fast} the CoM distance for \RotDiscSlow{} and \RotDiscFast{} are shown, respectively.
	Dashed lines represent systems with long time step and magenta lines indicate systems with coarse spatial discretization.
	The other solid lines show normal systems.
	
	From the two figures, it appears that the choice of the integration scheme, does not have a big influence on the CoM distance.
	%
	\\
	Instead, looking at the plots indicates, that the \emph{initial} CoM distance is the most decisive parameter, that determines how strong the CoM distances grows.
	We would like to remark, that the configuration and thus the initial CoM value are both random values.
	%
	However we have to keep in mind that the values are still quite small.
	Further we have not investigated this topic in more details.
		
	\FloatBarrier
	% END:		Centre of Mass
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	\subsection{Conclusion}~\label{sec:experiments:angMom:Conclusion}
	The results we have presented indicating that the method is able to preserve angular momentum.
	However we believe that the topic of rotational movements needs more investigation.
	%
	\\
	When we created the configurations for these experiments, we used the time step restrictions from the linear momentum and reduced them a bit, these systems are now known as the ``long step'' systems.
	It took us some time to realize that we have to lower the time step even more, which leads to the current situation.
		
	Based on our experience we gained during the analysis, we think that further research should include at least the following points.
	%
	\begin{itemize}
		
		\item 
		In section \ref{sec:experiments:angMom:CoM}, we have seen that simulation depends on its initial configuration.
		We have encountered such effects at other occasions as well.
		Most often it involved experiments that were repeated several times.
		The results looked similar for each run, but different enough to catch our eyes.
		%
		\\
		So the dependences on the initial condition should be studied in more detail, to better asses its influence.
		
		\item 
		We have only simulated the rotation over a small time duration.
		However it is also interesting to study how the system behaves for longer times.
		This does not necessarily concerns angular momentum, since we know that it decay eventually, but for example the behaviour of the CoM could be interesting.
		
		\item  
		Due to the larger gradients in the \RotDiscFast{} systems, we expected a stronger decay, as for the \RotDiscSlow{} systems, which was the case.
		%
		But we should also study how the system behaves, if the mass of the disc is increased.
		This will increase the angular momentum of the disc, but not friction, since it depends on the velocity gradients.
		Such discs will have more energy to sustain the rotation, thus the decay should be less strong\footnote
			{
				They are not directly comparable with each other.
				Since friction depends on the velocity gradient, thus the slowing down does influences itself.
				However we should observe a slower decay.
			} 
		than with a lighter disc.
		
		\item
		We have only studied an uniform mass distribution.
		However we should also study cases when density across the disc vary.
		
		\item
		Further experiments should also focuses on the fluid.
		For example fluids with different viscosities should be considered, since they influence friction.
		%
		\\
		We have performed such an experiment, but omitted the result.
		We observed that the angular momentum indeed decays faster.
		However the experiment that we conducted, was more of an extreme case and we just studied one.
		Instead we propose many experiments with increasing viscosities.
			
	\end{itemize}
	
	
	% END:		Conclusion
	%%%%%%%%%%%%%%%%%%%%%%%%%
	