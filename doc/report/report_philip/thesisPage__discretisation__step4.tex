%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% In this file we will discuss the mapping from the grid to the markers

\section[Step $4)$]{Step $4)$ -- Mapping the Grid to Markers}~\label{sec:discretization:step4}
In this section we will discuss how \emph{some} marker properties are updated.
We also use the term \emph{pull back} to refer to this operation.
%
\\
As we have said it before in section \ref{sec:GoverningEq:UsedScheme:TheScheme:Step4_gridToMarkers} on page \pageref{sec:GoverningEq:UsedScheme:TheScheme:Step4_gridToMarkers}, in this step only properties that are computed by solvers are updated, thus only $x$ and $y$ velocity, pressure and temperature are considered by this step.
Other variable marker properties are handled by the material model, which is step $5$.
%
\\
Regarding the velocities we would like to remember the reader, that this only concerns the \emph{intrinsic} velocities and not the one that are used to advect the markers.
These velocities are handled in step $6$ and are known as \emph{feel} velocities.

We will proceed in two steps.
First we will discuss, \emph{what} we pull back and then \emph{how} the pull back works.


	\subsection{What to Pull Back}~\label{sec:discretization:step4:whatPB}
	In principle we can pull back two different things the \emph{absolute} value or the \emph{change}.
	%
	\begin{figure}[h]
		\centering
		\begin{minipage}{.7\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__discretisation__step4/Step4__diffChangeAbs.png} 
			\caption
				{
					Comparison between the two different pull back techniques \emph{change} and \emph{absolute}.
					%
					Taken from \cite{gerya_2019}, Fig. 8.11.
				}
			\label{fig:discretization:step4:diffChangeAbs}
		\end{minipage}
	\end{figure}
	
	The idea of pulling back the \emph{absolute} field is, that we simply pull back the new field, this will completely \emph{overwrite} the \emph{old} field.
	We can see this in the left part of figure \ref{fig:discretization:step4:diffChangeAbs}.
	The marker in the middle had a larger value than its two neighbours.
	But when we pull back the absolute or total value to the markers, this information is lost.
	
	The main idea of pulling back \emph{changes} is to fix this.
	It works by realizing, that we had an \emph{old} filed, which was generated in the first step, where we mapped the marker values to the grid, and a \emph{new} field, that was computed by a solver.
	%
	\\
	The two fields are defined on the same points, so we can compute the differences between them, which is in turn a new field.
	Instead of the new field the change field is pulled back.
	However it is important, that this field, will not override the values on the markers, but \emph{update} them.
	The main effect is, that subgrid information is not lost, as it can be seen in the right part of figure \ref{fig:discretization:step4:diffChangeAbs}.
		
	\FloatBarrier
	% END:		what pull back
	%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	\subsection{How to Pull Back}~\label{sec:discretization:step4:howPB}
	Now we will discuss \emph{how} the pull back operation is done exactly.
	This is fairly similar to the discussion we had in section \ref{sec:discretization:step1} on page \pageref{sec:discretization:step1}, where we discussed step $1$.
	
	The situation is shown in figure \ref{fig:discretization:step4:pullBack}.	
	%
	\begin{figure}[h]
		\centering
		\begin{minipage}{.4\columnwidth}
			\includegraphics[width=\columnwidth]{pictures/thesisPage__discretisation__step4/Step4__pullBack__8_9.png} 
			\caption
				{
					Location of a marker within a cell.
					Distances towards its associated node.
					%
					Taken from \cite{gerya_2019}, Fig. 8.9.
				}
			\label{fig:discretization:step4:pullBack}
		\end{minipage}
	\end{figure}
	%
	We have \emph{some} field $B_{\cdot, \cdot}$, that we would like to pull back to the markers.
	
	Back in the first step, when we have computed the weight or influence, that a marker has on a node, we used bilinear weights.
	Thus we will use bilinear interpolation to computing the new marker value.
	%
	\begin{align}
		B_{m} =\; &B_{m}^{\text{old}}																								\nonumber \\
			&+ B_{i,j} \, \left(1 - \frac{\Delta x_{m}}{\Delta x} \right) \, \left(1 - \frac{\Delta y_{m}}{\Delta y} \right)  	\nonumber \\
			&+ B_{i, j+1} \, \frac{\Delta x_{m}}{\Delta x} \, \left(1 - \frac{\Delta y_{m}}{\Delta y} \right)				 	\nonumber \\
			&+ B_{i+1,j} \, \left(1 - \frac{\Delta x_{m}}{\Delta x} \right) \, \frac{\Delta y_{m}}{\Delta y} 				 	\nonumber \\
			&+ B_{i+1,j} \, \frac{\Delta x_{m}}{\Delta x}  \, \frac{\Delta y_{m}}{\Delta y} 	\label{eq:discretization:step4:pullBack}
	\end{align}
	%
	Above $\Delta x_{m}$ is the distance in $x$ between the marker $m$ and the node $(i,j)$ and $\Delta y_{m}$ is the distance in $y$.
	The values $\Delta x$ and $\Delta y$ are the grid spacings for the cell, where marker $m$ is located in.
	%
	\\
	The value of $B_{m}^{\text{old}}$ depends on the pull back mode.
	If we perform an \emph{absolute} pull back, it is set to zero.
	This will override the marker's value.
	%
	However if we pull back \emph{changes} then $B_{m}^{\text{old}}$ is set to the \emph{current} marker value, thus it will \emph{update} it.
	
	\FloatBarrier

			\paragraph{Subgrid Diffusion}~\label{sec:discretization:step4::subgridDiff}		
			The main intension of pulling back changes is to preserve subgrid features.
			However they can exists for a long time and even lead to undesirable effects.
			To tackle this subgrid diffusion can be used, see chapter $10$ in \cite{gerya_2019}.
			
			Its idea is to split change into two parts, the subgrid and remaining part.
			%
			First an analytical expression is used, to compute the diffusion of a marker's value.
			This will slowly smooth out peaks in the distribution of marker values.
			The remaining part of the change is then computed and mapped back as it is explained above.
			
			It is important that this method can handle some unphysical behaviour of the markers, but can also be dangerous.
			Thus it should only be used in the case of strong mixing.
			
			% END: 		Subgrid diffusion
			%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
	% END:		how to pull back
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
			
