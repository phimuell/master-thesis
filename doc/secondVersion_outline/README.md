# Info
This file contains a description of the changes that are applied,
or will be applied, to the original code.
It is written to make sure that the correct behavior and functionality is implemented.

It is not a complete description of what will be done, but an overview.
How it is implemented is not specified by this document.

Everything is located in the TeX folder.


