\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}

% https://tex.stackexchange.com/questions/33519/vertical-line-in-matrix-using-latexit/33523#33523
\makeatletter
\renewcommand*\env@matrix[1][*\c@MaxMatrixCols c]{%
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{#1}}
\makeatother

%%%%%%%%%%
% Some useful defines
\def\EGD{\texttt{egd}}
\def\Eigen{\texttt{Eigen}}
\def\NAN{\texttt{nan}}
\def\Class{\texttt{class}}
\def\CPP{\texttt{C++}}
\def\RS{\textsc{R}}



\usepackage{url}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{braket}
\usepackage{array}
\usepackage{xcolor}

\usepackage{makecell}

\usepackage{enumitem}

% You need the following package in order to include figures in your report:
\usepackage{graphicx}
\usepackage{float}
\usepackage{wrapfig}	% Braucht extra package

\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\author{ Philip Müller, \texttt{13-928-304}}
\title{Second version of \EGD{} -- outline}
\date{\today}


\newcommand{\imgof}[1]{\mathrm{img}\left( #1 \right) }
\newcommand{\urimgof}[2]{\mathrm{img}_{#1}^{-1}\left( #2 \right) }
\newcommand{\Abs}[1]{\left\vert #1 \right\vert }
\newcommand{\Norm}[1]{\left\Vert { #1 } \right\Vert }
\newcommand{\ud}[1]{\; \mathrm{d}#1 }
\newcommand{\udvol}[1]{\; \mathrm{vol}\left({#1}\right) }
\newcommand{\Exp}[1]{ \exp \left[ #1 \right] }
\newcommand{\Log}[1]{\log\left({#1}\right)}
\newcommand{\LogB}[2]{\log_{#2}\left({#1}\right)}
\newcommand{\Rot}[1]{ \mathrm{rot} \left[ {#1} \right] }
\newcommand{\Div}[1]{ \mathrm{div} \left[ {#1} \right] }
\newcommand{\Vek}[1]{ \MakeLowercase{\underline{#1}} }
\newcommand{\Mat}[1]{ \MakeUppercase{\mathbf{#1}} }
\newcommand{\Math}[1]{ \MakeUppercase{\mathbf{#1}} }
\newcommand{\Rank}[1]{\mathrm{Rg}\left(\Mat{#1} \right)}
\newcommand{\Sub}[3]{\MakeLowercase{#1}_{#2, #3}}
\newcommand{\Subb}[3]{\left(\Mat{#1}\right)_{#2, #3}}
\newcommand{\Det}[1]{\mathrm{det}\left( {#1} \right)}
\newcommand{\Tr}[1]{\mathrm{Tr}\left( #1 \right)}
\newcommand{\pdiff}[2]{\partial_{#1}\!\left[ {#2} \right] }
\newcommand{\pdiffAt}[3]{\partial_{#1}\left[ {#2} \right]\left({#3}\right) }
\newcommand{\pdiffF}[2]{\frac{\partial}{\partial {#1}} {#2} }
\newcommand{\diff}[2]{\frac{\mathrm{d}}{\mathrm{d}{#2}}\left[{#1}\right]   }
\newcommand{\diffN}[3]{\frac{\mathrm{d}^{#3}}{\mathrm{d}{#2}^{#3}}\left[{#1}\right]   }
\newcommand{\Jacobi}[1]{\mathcal{J}\left[{#1}\right]}
\newcommand{\JacobiAt}[2]{\mathcal{J}\left[{#1}\right]\left({#2}\right)}
\newcommand{\Dt}[1]{\mathrm{D}_{t} \! \left[ {#1} \right]}

\newcommand{\Fkt}[2]{{#1}\left({#2}\right)}


\newtheorem{satz}{Satz} %[section]

\newcommand{\difff}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2} }


\def\R{\mathbb{R}}
\def\Z{\mathbb{Z}}
\def\N{\mathbb{N}}
\def\C{\mathbb{C}}
\def\K{\mathbb{K}}
\def\L{\mathcal{L}}

\def\double{\texttt{double}}


%%%%%%%%%%%%%%%%%%%%%%%%
% Inverse Hyberbolische Funktionen, die ams nicht kennt
\DeclareMathOperator{\sech}{sech}
\DeclareMathOperator{\csch}{csch}
\DeclareMathOperator{\arcsec}{arcsec}
\DeclareMathOperator{\arccot}{arccot}
\DeclareMathOperator{\arccsc}{arccsc}
\DeclareMathOperator{\arcosh}{arcosh}
\DeclareMathOperator{\arsinh}{arsinh}
\DeclareMathOperator{\artanh}{artanh}
\DeclareMathOperator{\arsech}{arsech}
\DeclareMathOperator{\arcsch}{arcsch}
\DeclareMathOperator{\arcoth}{arcoth}
\DeclareMathOperator{\cis}{cis}
\DeclareMathOperator{\ddet}{det}
\DeclareMathOperator{\chp}{chp}


\newcommand{\Sin}[1]{\sin\left( {#1} \right)}
\newcommand{\Cos}[1]{\cos\left( {#1} \right)}
\newcommand{\Tan}[1]{\tan\left( {#1} \right)}

% Dieser Befehl erlaubt es eine Kleine Box in der Mitte der Seite zu erzeugen
\newcommand{\bx}[1]{
	\begin{center}
	\fbox{
		\sloppy
		\begin{minipage}{0.9\columnwidth}
			#1
		\end{minipage}
	}
	\end{center}
}

\newcommand{\memo}[1]{{\color{red} {#1}} }

\begin{document}

\maketitle
	
	%%%%%%%%%%%%%
	% BEGIN:	Small Intro
	\subsection{Info}
	This document will give an explanation of the changes that are applied to the original version.
	This is like a patch expressed on a very high level.

	% END:		Small Intro
	%%%%%%%%%%%%%%%%
	
	
%%%%%%%%%%%%%%%%%%%
% BEGIN:	Changes
\section{Changes}
We will now list several components and describe how they are affected by the modifications and how we will handle that.

The modifications involves using an extended grid.
This grid has more points than before, see figure \ref{fig:changes:newGrid} for more.
Also an inertia term will be added to the Stokes equations, see section \ref{seq:changes:Stokes:inertia} on page \pageref{seq:changes:Stokes:inertia}.


	%%%%%%%%%%%%%%%%%%%%%%%%
	% BEGIN:	Marker modifications
	\subsection{New Marker Properties}~\label{seq:changes:NewMarkProp}
	The markers are still treated the same.
	However we will add two new properties to them.
	These are \emph{old} $x$-velocity and $y$-velocities of the markers.
	These are the velocities the markers had in the previous time step \emph{before} they were advected\footnote{This property is updated before the markers are moved.}.	
	% END:		Marker modifications
	%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	%%%%%%%%%%%%%%%%%%%%%%%%
	% BEGIN:	New Grid
	\subsection{New Grid}~\label{seq:changes:newGrid}
	We will also operate on a new staggered grid.
	This is an extended version of the previous one.
	It can be seen in figure \ref{fig:changes:newGrid}.
	%
	\begin{figure}[H]
		\centering
		\begin{minipage}{0.6\columnwidth}
			\includegraphics[width=\columnwidth]{StaggeredGrid.png}
		\end{minipage}
		\label{fig:changes:newGrid}
		\caption{Indexing and association in the \emph{extended} staggered grid. Node $98$ is designated as a boundary node, but this is an error.}
	\end{figure}
	%
	The staggered grid is a superposition of four different grids.
	\begin{description}
		
		\item[Basic-Nodal Grid] 
		This is the grid that is \emph{completely} inside the computational domain.
		This means it has still $N_y \times N_y$ many grid points, this is done for convenience.
		Note in the picture above it is a bigger grid, but this is not used here.
		
		
		\item[Cell-Center Grid]
		This is a grid that has its grid points in the cell centres.
		Note that it also involves the points that surround the computational domain, it has thus $(N_x + 1) \times (N_y + 1)$ many grid points.
		This is technically not needed, for the Stokes equation, but is done for compatibility with the temperature equation.
		The temperature equation does not have ghost nodes, all nodes are either boundary nodes\footnote{Nodes that are outside the computational domain.} or inner nodes.
		
		
		\item[$x$-Velocity Grid]
		This grid contains the $x$ component of the velocity.
		At these points other intermediate values are located, also some material properties are located there.
				
		The last column of that grid consists of ghost nodes.
		Other nodes have a physical interpretation either as boundary or inner nodes.
		
		
		\item[$y$-Velocity Grid]
		This grid contains the $y$ component of the velocity.
		As the $x$ velocity nodal points some other intermediate quantities and properties are located there.
		
		The last row of this grid contains ghost nodes, that have no meaning.
		Other nodes have a meaning either as boundary or inner nodes.		
		
	\end{description}
	%
	Note that for the uniformity of numbering and compatibility with each other all grid, with exception\footnote{Subject to change.} of the basic nodal grids are $(N_x + 1) \times (N_y + 1)$ matrices.



		%%%%%%%%%%%%%
		% BEGIN:	Grid Spacing
		\subsubsection{Grid Spacing}~\label{seq:changes:newGrid:Spacing}
		We allow non-constants grids, but focuses on equidistant grids.
		We also assume that the grid points can be formed by a cross operation.
		Let $X$ be the $x$ coordinates of all \emph{basic} nodal points, this means there are $N_x$ many.
		$Y$ is defined analogue, meaning that it has $N_y$ many entries.
		So the coordinate of node $(i,j)$ denoted as $r_{(i, j)}$ is given as:
		\begin{align}
			r_{(i, j)} = \begin{pmatrix} X_{j} \\ Y_{i}	\end{pmatrix}   \label{eq:changes:newGrid:Spacing:Coordinates}
		\end{align}
		%
		Note that equation \eqref{eq:changes:newGrid:Spacing:Coordinates} is a bit strange, in the way that $i$ refers to the $y$ coordinate and $j$ to the $x$ coordinate, which means that they are swapped.
		
		When operating on the extended grid, or need some positions of the boundary/ghost nodes, we assume that the row and columns have the same width and height as the first and last real one.
		% END:		Grid Spacing
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	% END:		New Grid
	%%%%%%%%%%%%%%%%%%%%%%%%%%
	


	%%%%%%%%%%%%%%%%%%%%%%%%
	% BEGIN:	Interpolator --  Marker -> Grid
	\subsection{Interpolator}~\label{seq:changes:MtoG}
	Here we will discuses how the interpolator, the part of the code that maps marker properties to the different grids, has to be modified.
	However \emph{some} properties will now be imterpolated to several different grids.
	This means that other parts such as the solver did not have to make an interpolation on their own and have the right values at the right positions at their disposal.
	
	%
	\begin{description}
		
		\item[Density]
		In addition to the basic nodal points, density will now also be interpolated to the $x$ and $y$ velocity points and to the cell centre points too.
		Note that for the ghost velocity nodes, no value will be computed.
		For the cell centre point, although only inner nodes are used/needed, all nodes will be considered for interpolation.
		%
		\\
		The density in the basic nodal point is needed for the stabilization term.
		Since there we have to compute the density derivative in the velocity node points.
		
		\item[$\rho C_p$]
		Was formally defined on the basic nodal points, but now is defined in the pressure points.
		Although only the inner nodes are needed, all nodes will be considered.
		%
		\\
		Note that since the temperature equation is not solved, we do not need it.
		It is here for the sake of completeness.		
		
		\item[Thermal Conductivity]
		Was formally defined on the basic nodal points, but now is defined in the pressure points.
		Although only the inner nodes are needed, all nodes will be considered.
		%
		\\
		Note that since the temperature equation is not solved, we do not need it.
		It is here for the sake of completeness.
		
		\item[Viscosity]
		At the basic nodal point, this property is needed for calculating the shear stress.
		This is already done.
		Additionally the viscosity is also needed in the pressure points, for computing the normal stresses.
		Note that for all pressure points a value is computed, also for the ones that are outside the computational domain.
		
	\end{description} 

	Not only we have to redefine the location of some properties, we also need to add entirely new properties.
	%
	\begin{description}
		
		\item[Old $x$ Velocity]
		This is a quantity that is defined in the $x$ velocity points.
		It contains the value of the velocity in the previous step.
		It is important that the marker velocity is updated and then the marker is moved.
		%
		\\
		They are interpolated by a conservative scheme.
		
		\item[Old $y$ Velocity]
		This is the same as for the $x$ velocity.
		
		\item[Old Pressure]
		The old pressure is also needed on the pressure points.
		They are needed for calculating the adiabatic heating.
		Previously the pressure derivative was approximated by the velocity.
		However since the temperature equation is currently not considered, this is not needed yet.
		
	\end{description}

	Since the values of some properties\footnote{Velocities and temperature (not needed any longer).} at the boundary have to satisfy the boundary conditions and the interpolation may not preserve them, they have to be reapplied.
	
	This readjusting is done after the interpolation has been completed.
	It involves setting the nodes that are designated as boundary nodes in figure \ref{fig:changes:newGrid} are modified such that the boundary conditions are respected.
	This is essentially done the same way as they are encoded in the matrix, see section \ref{seq:changes:Stokes:BC} on page \pageref{seq:changes:Stokes:BC} and section \ref{seq:changes:Temp:BC} on page \pageref{seq:changes:Temp:BC}, for more details.
	% END:		Interpolator --  Marker -> Grid
	%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	
	%%%%%%%%%%%%%%%%%%%%%%
	% BEGIN:	Momentum Equation
	\subsection{Momentum/Stokes Equation}~\label{seq:changes:Stokes}
	Here we will discuss the changes to the Stokes equations.
	Generally the Stokes equations will be transformed to the Navier-Stokes equations.
	So the new form reads:
	\begin{align}
		\rho \Dt{v_i} = -\pdiff{x_{i}}{p} + \pdiff{x_j}{\sigma_{ij}^{\prime}} + g_i \rho 	\label{eq:changes:Stokes:NS}
	\end{align}
	%
	The right part of equation \eqref{eq:changes:Stokes:NS} is the same as before and does not change at least in the inner points.
	The density stabilisation term will look a bit differently, because now we have the densities more or less there where we need them.
	We still have to make an average for the velocity component, but it will roughly stay the same.
	Gravitation is only present in the $y$ equation, this will not be changed.
	
	The left side of the equation is new and was previously set to zero.
	This new part is the inertia part that will be discussed next.
	
		%%%%%%%%%%%%%%%%%%%%%%%
		% BEGIN:	Intertia
		\subsubsection{Inertia}~\label{seq:changes:Stokes:inertia}
		Now we will explain how we deal with the inertia term, left side of equation \eqref{eq:changes:Stokes:NS}.
		We will approximate it in the following way:
		\begin{align}
		\rho \Dt{v_\alpha}_{(i, j)} \approx \rho_{(i,j)}^{(\alpha)} \cdot \frac{v_{(i, j)}^{(\alpha)} - \hat{v}_{(i, j)}^{(\alpha)}}{\Delta t}    	\label{eq:changes:Stokes:inertia:disk}
		\end{align}
		%
		In equation \eqref{eq:changes:Stokes:inertia:disk} the symbols have the following meaning.
		\begin{description}
			
			\item[$\alpha$]
			Is the component under consideration and also the velocity grid point.
			If $\alpha = x$ then we consider the $x$ velocity component at a $x$ velocity grid point.
			Analogue for $\alpha = y$.
			
			\item[$\rho_{(i,j)}^{(\alpha)}$]
			Is the density at the $\alpha$ velocity grid point $(i, j)$.
			
			\item[$v_{(i, j)}^{(\alpha)}$]
			This is the $\alpha$ velocity in the current time step, it is the unknown we are solving for.
			
			\item[$\hat{v}_{(i, j)}^{(\alpha)}$]
			This is the old marker $\alpha$ velocity we have interpolated from the markers to the respective grid points, boundary conditions where applied.
			One can see this as the old velocity that was in that point in the previous time step.
			
			\item[$\Delta t$]
			This is the \emph{last} time step, not the current time step, or the time step used for stabilization\footnote{Currently they are the same, but this does not need to be true.}.
			
		\end{description}
		%
		This scheme is used to approximate the inertial term.
		The $\hat{v}^{(\alpha)}$ part will modify the right hand side value.
		The $v^{(\alpha)}$ part will influence the central coefficients of the system matrix.	
		% END:		Intertia
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
		%%%%%%%%%%%%%%%%%%%%%%%% 
		% BEGIN:	Boundary conditions
		\subsubsection{Boundary Conditions}~\label{seq:changes:Stokes:BC}
		One of the greatest changes, that are described here, is the new grid.
		Technically it is still the same, but it is organized differently and a bit larger.
		
		First of all, the ghost nodes are at different locations.
		However they are still treated the same way, by just setting them to zero.
		But for the sake of an uniform numbering they are still present.
		
		The velocities can have two kinds of boundary conditions.
		Boundaries that are perpendicular to the normal component are governed by a no slip condition, which ensures that no penetration\footnote{In this case the velocity is zero, however it is implemented, such that any velocity can be prescribed.} happens.
		The other boundaries, where the velocity component is tangential to the boundary normal, a free slip condition is applied, which means that no momentum flux over that boundary occur.
		
		Since the velocity nodes at the Dirichlet boundaries are already in the right position\footnote{With the exception of the boundary nodes.}, the implementation is straight forward.
		Technically the corner points could be governed by either of the two types.
		But by convention, the corner points are always considered as part of the no slip condition, this means we force the velocity to a certain value.
		
		The Neumann condition boundaries are a bit trickier then they are technically all outside the domain.
		For the $y$ velocities the condition reads as $\pdiff{x}{v_y} = 0$.
		If we discretize this, we immediately see that it is equivalent to force the two values, the boundary node and its inner neighbour to the same value.
		
		The boundary condition for the pressure have changed as well.
		In the old grid we have synchronized the horizontal neighbours of the four corner points, such that they have the same value and prescribed the value at the bottom, left node.
		This was needed because technically, we can only solve for the pressure changes, so we have to have an offset, or more formally an integration constant, to bring the pressure to the right value.
		Now this is not needed.
		Instead we just force the top left corner node to a certain, prescribed value.
		All ghost nodes, which are in this case all nodes that are outside the computational domain, are set to zero.
		
		% END:		Bopundary condition
		%%%%%%%%%%%%%%%%%%%%%%%%%%
	% END:		Momentum Equation
	%%%%%%%%%%%%%%%%%%%%%%
	
	
	%%%%%%%%%%%%%%%%%%%%%%%
	% BEGIN:	Temperature Equation
	\subsection{Temperature Equation}~\label{seq:changes:Temp}
	The temperature equation is not solved any longer, but it is still described here.
	The reason for that is, the original problem, slap that breaks off and ``falls'' down, is still an interesting test case.
	
	The equation is still discretized in the same way as before.
	And the equation generally looks the same.
	By introducing new interpolation points we have already accounted for the changed grid in the equations and now have all properties right where we need them.
	
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% BEGIN:	Grid
		\subsubsection{Grid}~\label{seq:changes:Temp:Grid}
		The old grid was identical with the basic nodal grid.
		The new grid coincides with the extended pressure grid.
		In contrast to the pressure grid, where the nodes outside the computational domains are ghost nodes, here they are boundary nodes.
		Which have a physical meaning and will enter the equations.		
		
		A major change that the new grid has, is that the heat fluxes $\vec{q} = \left(q_x,\, q_y\right)^{T}$ are now at a different locations as before.
		Now they coincide with the velocity component.
		Meaning $q_x$ is located at the $v_x$ nodes\footnote{Including the boundary nodes, but not the ghost nodes.} and $q_y$ at the $v_y$ nodes.
		Before it was swapped.		
		% END:		Grid
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
		
		
		%%%%%%%%%%%%%%%%%%%%%%%%%%%
		% BEGIN:	Boundary conditions
		\subsubsection{Boundary Conditions}~\label{seq:changes:Temp:BC}
		As with the velocity we have two kinds of boundary conditions here.
		At the top and bottom Dirichlet conditions are in place, where we force the temperature to have a certain value.
		At the left and right side we force thermal insulation, meaning no heat flux.
		
		All nodes that are outside the computational domain are treated as boundary nodes.
		The corner points are associated with the top and bottom conditions respectively.
		
		On the old grid, the Dirichlet condition fitted in naturally and the insulation was kind of strange, now it is different.
		
		The Dirichlet condition are implemented by forcing that, if we linearly interpolate the temperature between a boundary node and its inner neighbour\footnote{The \emph{inner} node that is right below or above the boundary node, a bit strange for the corner points.} we get the prescribed value at the domain boundary.
		
		On the boundary nodes that are governed by a Neumann condition we force that the temperature on the boundary node and its inner neighbour are the same.
		This will lead to a zero temperature derivative at the border and thus no flux will occur.		
		% END:		Boundary conditions
		%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% END:		Temperature Equation
	%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	%%%%%%%%%%%%%%%%%%%%%%%%
	% BEGIN:	Grid To Marker
	\subsection{Grid To Marker}~\label{seq:changes:GtoM}
	In this section we will discuss how the remapping works.
	
	The temperature, although not solved any more, would still be redistributed by a subgrid diffusion scheme.
	There are no changes, but the grid has changed, so new routines would be needed.
	
	But now we have to map the nodal velocities back to the markers, such that they can be used in the next time step.
	For that also a subgrid diffusion scheme, similar to the one for the temperature, is used.
	This is described in the book, so it will be rather short here.
	Please note that the $x$ and $y$ components must be handled separately, because they have different grids.	
	% END:		Grid To Marker
	%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% BEGIN:	Advection
	\subsection{Advection of Markers}~\label{seq:changes:Advection}
	Is still the same, it will use the continuum based interpolator and move the markers with RK4.
	The change here is, that the routines now have to be adapted such that they can handle the new format of the velocities.
	% END:		Advection
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	% BEGIN:	Steps
	\subsection{Steps}~\label{seq:changes:Steps}
	In this section we will outline the new procedure.
	
	\begin{enumerate}
		
		\item 
		Interpolate marker properties to the grid.
		
		\item
		Solve the Stoke equation to get the velocities and the pressure on the staggered grid.
		For the time derivative in the inertia term use the time step size form the \emph{previous} step.
		
		\item
		Apply a first restriction to the time step size.
		First start with the default value, if needed, reduce it such that no marker travels more than the allowed distance.
		\label{itm:changes:Steps:FirstTimeRestr}
		
		\item
		Compute the source term for the heat equation.
		(Not needed any more.)
		
		\item 
		Solve heat equation to get the new temperature distribution.
		As time step use the one that was estimated in \ref{itm:changes:Steps:FirstTimeRestr}.
		(Not needed any more.)
		
		\item
		Check if the temperature has changed to much.
		If so restrict the time step further, by assuming that the change is linear with respect to time.
		%
		\\
		Recompute the temperatures again, be solving the temperature equation a second time, but now with the even more restricted time step.
		(Not needed any more.)
		\label{itm:changes:Steps:SecTimeResolveHeat}
		
		\item
		Interpolate new new properties back to the markers.
		Do this by not mapping the absolute values back, but the changes.
		Also use subgrid diffusion, but not in the first step, there interpolate absolute values back.
		
		\item
		Recompute analytic values of markers.
		If there are marker properties that are given by an analytical formula, recompute them.
		
		\item
		Move the markers to their new positions.
		As time step use the final value from step \ref{itm:changes:Steps:SecTimeResolveHeat} or if the temperature equation was not solved, the one from step \ref{itm:changes:Steps:FirstTimeRestr}.
	\end{enumerate}
	% END:		Steps
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% END:		Changes
%%%%%%%%%%%%%%%%%%%%%%
	
	

\end{document}

