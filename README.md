# Master Thesis
This Repository contains the master thesis of Philip Müller.
It is tracked with git, which also helps to organize it.

However some parts of the project is not managed by git.
Because they would pollute the history.
For example not managed are:
- The generated data.
- Some Jupyter notebooks, however the main ones are checked in.


Most of the folders will contain a README.md file such as this one.
The file will describe the content of the folder, this also includes
a summary of the contents of sub folders.
As well as instructions to use certain programs.


## GIT
The project is tracked with git. It was used for _everything_.
Thus the LOG is a good source for information.

### IGNORE
Git can be configured to ignore certain files and whole directory.
For that the .gitignore files are used. The behaviour can be configured
on a per directory basis.
The project contains several ignore files, that should work well.

## Most Important Places
Here is a small list of the most important parts of the project and where they are located in.

- Report ~ "doc/report"
	In this folder you will found the PDF of the report (declaration of originality is the
	last page). You will also found a folder that contains the LaTeX source.
- Code ~ "code/scorpia_cpp"
	In this folder you will found the root of the C++ codebase, meaning a CMake file.


## SCORPIA
This is the code name of the project.
Scorpia was a legendary game reviewer in the 80s and 90s.
She was famous/feared for her harsh criticism of games she did not like.

### Folder Structure
These are the main folders of the project.
In each of them you will find a README.md file, that further describes
its content.

#### PAPERS
This folder contains all papers that have a connection with the project.

#### CODE
This folder contains everything that is connected to the code that
has been written during this thesis.

#### DOC
This folder contains everything that is related to documentation and related
documents. For example the final report and the diary is located there.

#### ANALYZE
This folder contains everything that deals with the analyzing and
visualization of the data.


