# INFO
This folder contains the notebooks that I used for analysing the result of the simulation.
However I did not include the raw data because they are too large.


## Using the Notebooks
In order to use the notebooks you must first unpack them and copy them into a location that
Jupyter can found. A path suitable for this is "../sibyl/libSibyl/" which points to the
working directory of Jupyter, if DOMINATOR.sh was used to start Sibyl.


## Structure of the Folders
In the report we discuss 4 experiments, each of the subfolders will contain the result for
one of them. Which experiment is located in which folder should be clear. In addition we
also have included the notebook for the rotTC case, which mainly shows how the tools can
be used.
In each of the folder you will found two files.


### ZIP-File
This file contains a print out of the notebook, it is stored as LaTeX source file and must
first be compiled. It is important that the command "pdflatex" must be used for it, using
plain latex will not work.
It will generate a PDF file that shows the notebook.


### XZ-File
This is the actual notebook that was used to analyse the date. It is stored with the pictures
in it. To view it you must open it with Jupyter, instructions are given above.
It is important that the notebooks on their own are useless, because for further analysis
the raw data, which _can_ not be added to this repro must be available.



