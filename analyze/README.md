# Info
This folder contains everything that is related to analyzing and visualization of the experiment results.


# Folders
Here is a list of folders that are located here.
They are created to separate the analyzing into several pieces.

## SIBYL
SIBYL is the main analyze framework.
It is named after a ancient Greek prophet. She wished for immortality, but continued to age unable to die,
she now seeks to die. However Sibyl is also a very powerful computer in the anime Psycho-Pass, where she
controls Japan with an iron fist. For historical reason I name _all_ my analyze frameworks Sibyl.
And actually this is Sibyl 2.0.

You will find instruction for starting SIBYL within this folder. However the Notebooks for the thesis are
not located in it, also the data are not located there, actually none of them are inside this repro.


## SIBYL_REPORT
In this folder you will found the notebooks that were written made for the thesis. I used to analyse the
data. However the raw data is not there.


