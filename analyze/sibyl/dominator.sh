#!/bin/bash
#
# This script is used for starting the Sibyl engine.
# The name 'Dominator' comes from the series 'Psycho Pass' where also Sibyl comes from.
# The dominator is a scanner and a weapon.
#
##################

# Where sybil is stored
SIBYL_FOLDER="libSibyl"

# This is the notebook folder.
#  This is a subfolder in the sibyl folder, this is only the name
NOTEBOOK_FNAME="yNoteBooks"

# The name of the Sybil notebook
#  This is in the sibyl folder.
SIBYL_FILE="Sibyl.ipynb"

# This are the parts for the listening
IP_TO_LISTEN="localhost"
PORT_TO_LISTEN="8888"
NO_BROWSER_COM=""

# This variable can be used to determine, if we should pull/push to the notebook repro
DO_PULLING="1"


## Make some test to screen the dependency
if hash gcc &>/dev/null
then
	NOTHING=1
else
	echo "Could not detect GCC on your system."
	echo " GCC is needed for CSIBYL."
	echo " ABORT."
	exit 70
fi

if hash jupyter &>/dev/null
then
	NOTHING=1
else
	echo "Could not detect JUPYTER on your system."
	echo " JUPYTER is needed for SIBYL."
	echo " ABORT."
	exit 71
fi

if hash git &>/dev/null
then
	NOTHING=1
else
	echo "Could not detect GIT on your system."
	echo " GIT is needed for SIBYL."
	echo " ABORT."
	exit 71
fi

#####################


if [ $# -eq 1 ] && [ "${1}" == "--help" -o "$1" == "-h" ]
then
	echo    "The DOMINATOR is used to ease the use of jupyter."
	echo    "It will set up all the external work and create set up a new notebook that will be used."
	echo    "It supports some options, they must apear in the order they yre presented here."
	echo    " "
	echo -e "--no-pull        This option will skipt the pulling from the NB repro (not recomended)."
	echo -e "REMOTE [ip: IP]  Will create a server that listen to [IP]:${PORT_TO_LISTEN}."
	echo -e "                  This must be the ip address of this PC, defaults to 192.168.1.120."
	echo    " "
	echo    "In REMOTE mode there are some glitches I was not able to fix. For example it will,"
	echo    "open a browser, LYNX on console, which is annoying. It will also not start the newest"
	echo    "notebook. In order to get the token to log in use \"jupyter-notebook list\" to get"
	echo    "a list of all running servers, with the token."
	echo    " "
	echo    "The script uses a primitve way to determine if an the NB folder \"${NOTEBOOK_FNAME}\","
	echo    "contains a git repro or not. All git commands will only be performed if it is considered one."
	echo    " "

	exit 0
fi


# Test if sibyl is present
if [ ! -e "${SIBYL_FOLDER}/${SIBYL_FILE}" ]
then
	echo "Sibyl template was not found."
	echo "This is an error."
	echo "Sibyl Folder: \"${SIBYL_FOLDER}\""
	echo "Sibyl File:   \"${SIBYL_FILE}\""
	echo "Abort."

	exit 2
fi


# Test if the hdf5 files are present.
# Aka if the folder exists
if [ ! -d "${SIBYL_FOLDER}/hdf5" ]
then
	echo "The HDF5 folder is not pressent, creating it."
	mkdir -p "${SIBYL_FOLDER}/hdf5"
fi

# Test if the Notebook folder is present
if [ ! -d "${SIBYL_FOLDER}/${NOTEBOOK_FNAME}" ]
then
	echo "No notebook folder is present, creating it."
	echo "It is recomended to create a git folder in it."
	mkdir -p "${SIBYL_FOLDER}/${NOTEBOOK_FNAME}"

	sleep 30
fi

#
# Arguments
if [ $# -ge 1 ]
then


	# no pulling
	if [ "$1" == "--no-pull" ]
	then
		echo "No pulling is requested. This will also not push your changes."
		sleep 3
		shift
		DO_PULLING="0"
	fi # end: no pulling

	# remote
	if  [ "${1^^}" == "REMOTE" ]
	then
		echo "On REMOTE MACHINE"

		echo ""
		echo "Did you start SCREEN?"
		echo ""
		sleep 3

		# Remove the remote from the argument list
		shift

		# We will now set the IP address to the default one.
		#  This must be changed to the machine you are running the server.
		#  But on local instances must be loaclhost.
		IP_TO_LISTEN="192.168.1.120"
		PORT_TO_LISTEN="8888"

		# ",," makes to all lower case
		if [ $# -ge 1 ] && [ "${1,,}" == "ip:" ]
		then
			if [ $# -lt 1 ]
			then
				echo "Given the IP keyword, but not enough arguments."
				exit 123
			fi

			IP_TO_LISTEN="$(echo "$2" | cut -f1 -d':')"
			# TODO: extract port
			shift
			shift
		fi

		echo "IP: ${IP_TO_LISTEN}"

		# Try to disable the browser, for some reason it does not realy work
		NO_BROWSER_COM="--no-browser --NotebookApp.open_browser=False"
	fi   # end if: remote
fi # end: arguments given

#
# Starting

cd "${SIBYL_FOLDER}"

echo "==================================="
echo "   Enforcement mode: Lethal Eliminator."
echo "   Please aim carefully and eliminate the target."
echo " "

sleep 3

# Compose a special name of the script
SIBYL_FILE_EXTENSION="${SIBYL_FILE##*.}"
SIBYL_FILE_FILENAME="${SIBYL_FILE%.*}"




# Compose a new name, withot the subfolder extension; they are just the file name
DATE="$(date +%Y_%m_%d__%H_%M)"
SIBYL_FILE_TO_USE_PURE="${SIBYL_FILE_FILENAME}_${DATE}.${SIBYL_FILE_EXTENSION}"
SIBYL_FILE_TO_LOG_PURE="${SIBYL_FILE_FILENAME}_${DATE}.log"

# Now we append the name to them
SIBYL_FILE_TO_USE="${NOTEBOOK_FNAME}/${SIBYL_FILE_TO_USE_PURE}"
SIBYL_FILE_TO_LOG="${NOTEBOOK_FNAME}/${SIBYL_FILE_TO_LOG_PURE}"

if [ -e "${SIBYL_FILE_TO_USE}" ]
then
	echo "The file \"${SIBYL_FILE_TO_USE}\" already exists"
	echo "Abort."

	exit 1
fi


# Pull the repro of the Notebooks
if [ ${DO_PULLING} -ne 0 ]
then

	# Primitive test
	if [ ! -e "${NOTEBOOK_FNAME}/.git" ]
	then
		echo "Pulling was selected but \"${NOTEBOOK_FNAME}\" does not seam to contain a git repro."
		echo " Since pulling is activated by default, try with --no-pull again."
		sleep 30
	else
		echo "Start pulling from the Notebook-Repro"
		git -C "${NOTEBOOK_FNAME}" pull --rebase --ff-only

		if [ $? -ne 0 ]
		then
			echo "Failed to pull the changes from the notebook repro."
			echo "Abort"

			exit 1
		fi
	fi
fi # end: pulling from repro


# Now we coping the tenmplate file, which is not in the subfolder
#  into the subfolder under the computed name.
cp "${SIBYL_FILE}" "${SIBYL_FILE_TO_USE}"

if [ $? -ne 0 ]
then
	echo "Error while coping the template file to \"${SIBYL_FILE_TO_USE}\""
	echo "Abort."

	exit 1
fi


# Now we create a symlink to the file, this way it still apears
#  to be inside the folder
ln -s "${SIBYL_FILE_TO_USE}" "${SIBYL_FILE_TO_USE_PURE}"

if [ $? -ne 0 ]
then
	echo "Error while creating the symlink for \"${SIBYL_FILE_TO_USE}\""
	echo "Abort."

	exit 1
fi


# Execute the make file to be sure, that everything works
make all --always-make -C "libSibyl"
if [ $? -ne 0 ]
then
	echo "Problem while bilding the c parts of sibyl."
	exit 1
fi

# We reapend because now start the server.
#  As argument we give it the newly created file.
#  We use the symlink, such that everything is here.
jupyter notebook ${NO_BROWSER_COM} --ip=${IP_TO_LISTEN} --port=${PORT_TO_LISTEN} "${SIBYL_FILE_TO_USE_PURE}" 2>&1  | tee --append "${SIBYL_FILE_TO_LOG}"

JUP_EX=$?


echo " "
echo " "
echo " "
echo "The target's thread judgement has been reappraised."
echo "Actions are no longer requiered."
echo "============================="
echo " "

#  Test if a git folder is present if so, add the changes to the folder
if [ -e "${NOTEBOOK_FNAME}/.git" ]
then
	echo "Add and commit the changes to the repro."
	DATE2="$(date +%Y-%m-%d\ %R)"
	git -C "${NOTEBOOK_FNAME}" add .
	if [ $? -ne 0 ]
	then
		echo "Git add failed."
		echo "Abort."
		exit 1
	fi

	# Commit it, we assume that this commit will work.
	git -C "${NOTEBOOK_FNAME}" commit --allow-empty -m "Automatical commit of the \"${DATE}\" series. Commited at ${DATE2}"
fi


# Now we add all the change of the notebooks to the repro
#  We will add all changes, because other notebooks where changed.
#  We use the -C option to virtually change the folder
if [ "${DO_PULLING}" -ne 0 ]
then
	# Primitive test
	if [ -e "${NOTEBOOK_FNAME}/.git" ]
	then
		echo "Pushing the changes to the remote"

		git -C "${NOTEBOOK_FNAME}" push
		if [ $? -ne 0 ]
		then
			echo "Git push failed."
			echo "Abort."
			exit 1
		fi
	else
		echo "Pushing was enabled, but no repro detected."
		sleep 5
	fi
fi # end: push


# Exit the script, return the return state of jupyter
exit ${JUP_EX}


