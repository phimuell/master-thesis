# Info
This folder contains SIBYL. SIBYL is the main analyze framework used in this thesis.
It is build on Jupyter notebooks, that allows to analyze the data in an interactive manner.

You will not find an introduction to Python or Jupyter inside this project. Please use this
as an experimental ground or consult a tutorial on the internet.
This guide assumes that you have at least some experience with Jupyter.

Please also read the README in the LIBSIBYL folder.


## Dependencies
In order to be used, several external components are needed.
- Jupyter:
	For running the notebooks.
- Python:
	As runtime.
- NumPy & SciPy:
	Numerical calculations.
- Matplotlib:
	For plotting.
- h5py:
	Accessing HDF5 files from withing Python.
- GCC:
 	Is needed for compiling some auxiliary code structures.
- git:
	Needed for the internal version control.

## Design
SIBYL is actually only a collection of functions that was written to automatize common tasks
that are needed frequently.
SIBYL is written around the data base. As it was described elsewhere the simulation states are
stored using HDF5 files. The data base allows to access the data in a easy way.Its main job
is basically to compose the path at which the dataset was stored and load it from disc.

### CSIBYL
Some computationally intense parts are written in C. Python is able to dynamically load library
and use the functions inside them. We exploit this to provide the functionality. The DOMINATOR
script will take care of everything. However it will deactivate asserts.


## DOMINATOR.sh
The script "dominator.sh" is provided to ease the use of SIBYL. It will perform all needed
initialization tasks. You should only use SIBYL through this script, otherwise it may not
work correctly.

DOMINATOR.sh also accepts a "--help" falg which will print out some basic informations.


### Working Directory
The script will enter the subfolder LIBSIBYL. Jupyter is also called from this folder.
Thus all paths are relative to that folder and _not_ to this one.

### Usage
There are two ways of usage. As a novice it is recommended to use the first one and call
the script without any arguments.

#### Calling Without Arguments
The script will enter the LIBSIBYL folder and perform the setup steps. It will then create
a new notebook for the current session. For this a template file is used.
On most machines it should open your browser and load the notebook.
If not, see below how to get the URL.

#### REMOTE Mode
This mode is activated by supplying the argument "REMOTE" to the script. It is intended to
run the notebook on a different PC, this is especially useful if your machine is not that
powerful. You may have to modify the script and change the IP address and the port as well.
You must set it to the IP address of the machine you are running on.

It is highly recommended to run the script in a screen session, and the script will remind
you about this. However at least on my machine, I was not able to suppress the opening of
a browser, so it may be possible that LYNX is opened, or an error happens.
In order to get the URL to access the notebook you have to enter the following command:
	jupyter-notebook list
which will output the list of all servers together with the tokens.


## Storage of Notebooks and HDF5
See the subfolder for an explanation how the storage works.

## Folders

### LIBSIBYL
This folder contains everything that is related to SIBYL.
It contains the Python code and the analyzing notebooks.
Before doing anything else the dominator.sh script will enter this folder.



