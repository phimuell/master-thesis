#!/bin/bash
#
# This file will create symlinks to all hdf5 files that are given in its argument
#
#

# This is the variable for the source folder
SRC=""

if [ $# -ne 1 ]
then
	echo "No arguments given, use default."
	SRC="./hdf5_scorpia/"
else
	# Use the one that is supplied as argument
	SRC="${1}"
fi


# Test if it is realy a folder
if [ ! -d "${SRC}" ]
then
	echo "\"${SRC}\" does not appear to be a folder."
	exit 2
fi


# Scan the folder
ls -1 "${SRC}" | grep -iP '\.hdf5$' | while read R
do
	linkName="${R}"			# This is the name of the link we will create
	linkTarget="${SRC}/${R}"	# This is the target of the link

	if [ ! -e "${linkName}" ] || [ -L "${linkName}" ]
	then
		# Link name does not exist so create it
		ln -sf "${linkTarget}" "${linkName}"

		if [ $? -ne 0 ]
		then
			echo "Creating the symlink for target \"${linkTarget}\" failed".
			exit 3
		fi
	else
		echo "The file \"${linkTarget}\" does not exists and is no link, it will be ignored."
	fi
done

exit 0

