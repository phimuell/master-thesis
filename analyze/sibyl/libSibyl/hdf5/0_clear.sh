#!/bin/bash
#
# This script will remove all hdf5 simlinks in the given folder.
#



# This is the source folder
SRC="${1:-${PWD}}"

if [ ! -d "${SRC}" ]
then
	echo "\"${SRC}\" does not appear to be a folder."
	exit 2
fi

# Scan the folder
ls -1 "${SRC}" | grep -iP '\.hdf5$' | while read R
do
	linkName="${R}"			# This is the name of the link we will create

	if [ -L "${linkName}" ]
	then
		rm "${linkName}"
	fi
done

exit 0

