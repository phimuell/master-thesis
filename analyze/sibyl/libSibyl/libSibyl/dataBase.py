# This file is a database wrapper for Sibyl.
# This file is a database wrapper for Sibyl.
# It wrapps a hdf5 file and gives easy access to it.
# It also allows to uniformaly handle all different finds
# of experiments.


import h5py
import numpy as np
import copy

from .PropIdx import PropIdx
from libSibyl.simSeries import SimSeries
from libSibyl.gTypeEnum import eGridType
import libSibyl.sibyl_traits         as sTraits
import libSibyl.sibyl_util           as util


# This class is a wrapper around an HDF5 file.
#  It gives uniformal access to the stored information.
#  Note that due to the ordering of data the matricies must
#  be transposed before returning to the user.
class DataBase:

    #
    # CONSTRUCTORS

    # This is the building constructor, it takes a file name to
    # A database and bind itself to it.
    # The data base is opened for verification that it is one
    # and then colsed.
    def __init__(self, pathToDB):

        # It did not generate an error, so we will safe the path
        self.m_dbPath = pathToDB

        # Try to open the databse and fail if the file does not exist
        f = self.getDBHandler()

        try:
            # This variable will store the number of states thata re recorded in the file
            self.m_nStates = -1

            # This is a dict that will be used to store the time point and the time step
            # we can not be sure to get the listing in alphabetical ordering
            timeMap = {}

            # We now find the number of simulation states that are stored
            # and also the time information, they are stored as attributes
            for state in f['simulation_states'].items():

                nameOfState = state[0]          # Extracting the name of the group, this will be a numeric name
                idOfState   = int(nameOfState)  # This is the actuall ID of the state, it is basically all leading zeros removed

                if(idOfState in timeMap):
                    raise ValueError("The state {} was found twice.".format(idOfState))

                # Update the state counter, we must do it that way, because
                # the order in which they are recived is unspecific
                self.m_nStates = max(idOfState, self.m_nStates)

                # Load the fifferent attributes
                DT       = state[1].attrs['dt'      ]
                thatTime = state[1].attrs['currtime']

                # Inserting the time times into  the dict
                timeMap[idOfState] = (DT, thatTime)
            # end for(a):

            # The counting of the states starts at 0, since m_nStates records the found name of the state
            # It is behind one, so we have to increment it by one
            self.m_nStates += 1

            if(self.m_nStates != len(timeMap)):
                raise ValueError("Inconsistency in simulation states, found {} many states, but the (past the end) index is {}".format(len(timeMap), self.m_nStates))
            # end if

            # Now we store them in handy numpy indexes
            self.m_dt    = np.zeros(self.m_nStates)
            self.m_times = np.zeros(self.m_nStates)

            for sID, times in timeMap.items():
                self.m_dt[sID]    = times[0]
                self.m_times[sID] = times[1]
            # end for(sID, times):

            if(util.isSorted(self.m_times, True) == False):
                raise ValueError("The time series that were extracted from the file are not sorted.")
            #end if: isSorted

            # We will now iterate through the state and load the number of
            #  markers that are stored. This is needed since the number of
            #  markers can change during the simulation, at least in the dump.
            self.m_nMarkers = np.zeros(self.m_nStates, dtype = int)
            if(True ):
                # GROSS HACK
                xPos = self.getSimState(stateIdx = 0, prop = PropIdx.PosX, onMarker = True, comp = None, F = f)
                self.m_nMarkers[:] = len(xPos)
            else:
                for s in range(self.m_nStates):
                    xPos = self.getSimState(stateIdx = s, prop = PropIdx.PosX, onMarker = True, comp = None, F = f)
                    self.m_nMarkers[s] = len(xPos)
                #end for(s): detecting the different markers
            #end if:


        finally:
            self.closeDBHandler(f)
    # End init: Building


    # This function is depricated and generates an exception upon using it
    def getInitialState(self, prop, onMarker = False, comp = None, F = None, Weak = False):
        raise ValueError("The initial status has been removed. And the version status has not been implemented yet.")
    # end: getInitialState


    # This function allows to access the state during the simulation.
    # Severall properties are handled special, at least on the grid.
    # For example it is possible that staggered grids are returned.
    # StateIdx is the index of the simulation state.
    # Comp is needed when stress and strain are involved and used to
    # determine which entry {xx, yy, xy, yx} is used.
    # mIdx is a set of index that is requested, not a marker type.
    # it is not possible to load a single marker value
    def getSimState(self, stateIdx, prop, onMarker = False, comp = None, mIdx = None, F = None):

        if(sTraits.isInt(stateIdx) == False):
            raise ValueError("stateIdx is not an integer.")
        if((stateIdx < 0) or (stateIdx >= self.nStates())):
            raise ValueError("No simulation state with id {} is known.".format(stateIdx))
        if(sTraits.isInt(mIdx) ):
            raise ValueError("Not possible to load a single marker value.")
        # end check:


        if(prop is None):
            raise ValueError("Passed none as property.")
        if(prop.isSpecial() ):
            raise ValueError("The passed property is the special propery.")
        # end check: prop

        f = F
        try:
            if(F is None):
                f = self.getDBHandler()

            # Compose the path to load, this function will also verify the existence of it
            PATH = self.composePATH(stateIdx = stateIdx, prop = prop, onMarker = onMarker, comp = comp, F = f)

            # Load the data set; It is already transposed
            ds = self.loadDataSet(PATH, onMarker, F = f, isType = prop.isType())

            if(onMarker == True):
                # Test if only a subset is requiered
                if(sTraits.isIndexSeries(mIdx) ):
                    ds = ds[mIdx]
            # end if: handling marker

            return ds

        finally:
            if(F is None):
                self.closeDBHandler(f)
    # end: getSimState


    # This function is able to test if the simulation state exists or not
    #  Note that this test is only able to detect the states and not the initial state.
    def hasSimSatete(self, stateIdx, prop, onMarker = False, comp = None, F = None):
        if(sTraits.isInt(stateIdx) == False):
            raise ValueError("stateIdx is not an integer.")
        if((stateIdx < 0) or (stateIdx >= self.nStates())):
            raise ValueError("No simulation state with id {} is known.".format(stateIdx))
        # end check: state idx

        if(prop is None):
            raise ValueError("Passed none as property.")
        if(prop.isSpecial() ):
            raise ValueError("The passed property is the special propery.")
        # end check: prop

        # Compose the path to load, this function will also verify the existence, if not pressent
        # an empty path is returned.
        PATH = self.composePATH(stateIdx = stateIdx, prop = prop, onMarker = onMarker, comp = comp, F = F, noExcept = True)

        if(len(PATH) == 0):
            return False
        else:
            return True
    #end def: hasSimState


    # This function is designed to load the full series of the given quantity
    #  This function is a bit more efficient and cleaner than doing it by hand.
    #  This function returns a sim series object.
    #  This is a nicely wrapper.
    #  It is also possible to select a subset of the time, note that an endtime of -1
    #  means the time the simulation ended.
    #  In case of markers the positions of the basic nodal grid are stored in teh series.
    #
    #
    def getSimSeries(
                self, prop,
                onMarker = False,
                startTime = 0, endTime = -1, inYr = False,
                withInitialState = False,
                comp = None,
                F = None
            ):
        f = F
        try:
            if(F is None):
                f = self.getDBHandler()

            # Load the state series
            recStates, recTime = self.getStateSeries(prop = prop, onMarker = onMarker, comp = comp,
                                        startTime = startTime, endTime = endTime, inYr = inYr,
                                        withInitialState = withInitialState,
                                        withTime = True, F = f)

            # Get the indexes (We can not load it from here)
            recIdx = util.getTimeIndicesRANGE(times = self.m_times, start = startTime, ende = endTime, inYr = inYr)

            # This is for accounting of markers
            useProp = prop if (onMarker == False) else None

            # Load the positions
            xNodes = list()
            yNodes = list()

            for i in recIdx:
                xNodes.append(self.getXPos(prop = useProp, idx = i, comp = comp, F = f) )
                yNodes.append(self.getYPos(prop = useProp, idx = i, comp = comp, F = f) )
            # end for(i): extracting the positions

        finally:
            if(F is None):
                self.closeDBHandler(f)

            # Return now the list of state results and the times
            return SimSeries(Times = recTime, Values = recStates,
                        onMarker = onMarker, Prop = prop, Comp = comp,
                        XNodal = xNodes, YNodal = yNodes)
    # end: getSimSeries



    #
    # ============================
    # Small status functions
    #

    # This function returns the number of different
    # simulation states that are stored in *self.
    def nStates(self):
        return self.m_nStates
    # end: nStates



    #
    # =============================
    # Grid Nodes and Position functions
    #   This functions deals with the accessing
    #   of the positions of the grid nodes
    #


    # This function returns the x positions that are atarted.
    #  This function only operates on grids, so no markers are
    #  currently supported. if no property is given, then the
    #  basic grid is returned. Depending on the property
    #  A different grid meight be returned.
    #
    #   prop        The property that is requested.
    #   comp        Component.
    #   gType       The grid type that should be used.
    #   onMarker    If on marker.
    #   idx         On marker which time frame
    #
    def getXPos(self,
            prop = None,
            comp = None,
            gType = eGridType.RegBasic,
            onMarker = None,
            idx = None,
            F = None):

        if(onMarker == True):
            raise ValueError("Handling of marker positions is not implemented by this function. Use the state function.")
        # end checks:

        # For compability we map the idx property to zero if not given
        sIdx = 0 if idx is None else idx

        # This is the handler that we will use here
        f = F
        try:
            # If the NULL handler was passed, open it.
            if(F is None):
                f = self.getDBHandler()
            # end if: open handler

            # This is the variable we will return
            posArray = None

            # Explicit grid type is requested
            if(prop is None):
                if(sTraits.isGridTypeEnum(gType) == False):
                    raise ValueError("No bvalid grid type passed.")
                PATH = self.composePATH(stateIdx = sIdx, prop = None, comp = None, onMarker = False, F = f)

                PATH = PATH + "gridGeo" + "/" + gType.toStr() + "/" + "X"       # Update the path

                # Now we load the dataset, it is a markerproperty, because it is an array
                posArray = self.loadDataSet(PATH = PATH, onMarker = True, F = f)
            # end if: simple handling

            else:
                #
                # We must load the grid value firectly from the file


                if(sTraits.isInt(sIdx) == False):
                    raise ValueError("idx is not an integer or None.")
                if(sIdx < 0 or sIdx >= self.nStates()):
                    raise ValueError("idx is out of range it was {}".format(sIdx))
                # end check: index

                if(prop is None):
                    raise ValueError("Passed none as property.")
                if(prop.isSpecial() ):
                    raise ValueError("The passed property is the special propery.")
                # end check: prop

                # Load the Path to the selected
                X, Y = self.getGeoPath(prop = prop, stateIdx = sIdx, comp = comp, F = f)

                # Now load the positions
                posArray = self.loadDataSet(PATH = X, onMarker = True, F = f)

        finally:
            if F is None:
                self.closeDBHandler(f)
            # end if: close file if needed
        # end finally:

        # We must flatten it to make a real array out of it.
        # Currently it is an (1, N) matrix. flatten will make a copy
        # So not needed to copy it later
        posArray = posArray.flatten()

        if(sTraits.isArray(posArray) == False):
            raise ValueError("The extracted position array is not an array, but an {}, shape {}".format(type(posArray), posArray.shape))
        if(len(posArray) <= 1):
            raise ValueError("The array is too short, it is only {}".format(len(posArray)))
        # end if: final test

        return posArray
    # end: getXPos


    # This function returns a vector containing the y coordinates.
    #  This function only operates on grids, so no markers are
    #  currently supported. if no property is given, then the
    #  basic grid is returned. Depending on the property
    #  A different grid meight be returned.
    #
    #   prop        The property that is requested.
    #   comp        Component.
    #   gType       The grid type that should be used.
    #   onMarker    If on marker.
    #   idx         On marker which time frame
    #
    def getYPos(self,
            prop = None,
            comp = None,
            gType = eGridType.RegBasic,
            onMarker = None,
            idx = None,
            F = None):

        if(onMarker == True):
            raise ValueError("Handling of marker positions is not implemented by this function. Use the state function.")
        # end checks:

        # For compability we map the idx property to zero if not given
        sIdx = 0 if idx is None else idx

        # This is the handler that we will use here
        f = F

        try:
            # Open the file
            if F is None:
                f = self.getDBHandler()
            # end if: F is not given so we open it

            # This is the variable we will return
            posArray = None

            # Explicit grid type is requested
            if(prop is None):
                if(sTraits.isGridTypeEnum(gType) == False):
                    raise ValueError("No bvalid grid type passed.")
                PATH = self.composePATH(stateIdx = sIdx, prop = None, comp = None, onMarker = False, F = f)

                PATH = PATH + "gridGeo" + "/" + gType.toStr() + "/" + "Y"       # Update the path

                # Now we load the dataset, it is a markerproperty, because it is an array
                posArray = self.loadDataSet(PATH = PATH, onMarker = True, F = f)

                # We must flatten it to make a real array out of it.
                # Currently it is an (1, N) matrix. flatten will make a copy
                # So not needed to copy it later
                posArray = posArray.flatten()

                return posArray
            # end if: simple handling

            else:
                if(sTraits.isInt(sIdx) == False):
                    raise ValueError("idx is not an integer or None.")
                if(sIdx < 0 or sIdx >= self.nStates()):
                    raise ValueError("idx is out of range it was {}".format(sIdx))
                # end check: index

                if(prop is None):
                    raise ValueError("Passed none as property.")
                if(prop.isSpecial() ):
                    raise ValueError("The passed property is the special propery.")
                # end check: prop

                # Load the Path to the selected
                X, Y = self.getGeoPath(prop = prop, stateIdx = sIdx, comp = comp, F = f)

                # Now load the positions
                posArray = self.loadDataSet(PATH = Y, onMarker = True)

        finally:
            if F is None:
                self.closeDBHandler(f)
            # end if: close file if needed
        # end finally:


        # We must flatten it to make a real array out of it.
        # Currently it is an (1, N) matrix. flatten will make a copy
        # So not needed to copy it later
        posArray = posArray.flatten()

        if(sTraits.isArray(posArray) == False):
            raise ValueError("The extracted position array is not an array, but an {}, shape {}".format(type(posArray), posArray.shape))
        if(len(posArray) <= 1):
            raise ValueError("The array is too short, it is only {}".format(len(posArray)))
        # end if: final test

        return posArray
    # end: getYPos


    # This function returns a suitable pair, first X and then Y coordinates
    #  Such that they can be used for plotting. The property argument is
    #  currently ignored, but may have affects in the furture.
    #  This functiona also takes the component, which is ste to None.
    #  With the optional flag "noMeshing" the positions are simply
    #  returned, first X and then Y.
    def getPlotPos(self,
            prop, comp = None,
            gType = eGridType.RegBasic,
            idx = None,
            noMeshing = False,
            F = None):

        f = F
        try:
            if(F is None):
                f = self.getDBHandler()

            X = self.getXPos(prop = prop, gType = gType, comp = comp, idx = idx, F = f)
            Y = self.getYPos(prop = prop, gType = gType, comp = comp, idx = idx, F = f)

            if(noMeshing == True):
                return (X, Y)

            # Apply mesh grid to it
            Xg, Yg = np.meshgrid(X, Y)

            return (Xg, Yg)

        finally:
            if(F is None):
                self.closeDBHandler(f)
    # end: getPlotPos



    #
    # =============================
    # Time Stepping Functions
    #   These functions gives access to the time values.
    #


    # returns a specific time
    def getTime(self, n):

        if(n is None):
            return self.getTimes()

        if(n < 0 or n >= self.nStates()):
            raise ValueError("Illegal step requested, requested time {}, but only {} are known.".format(n, self.nStates()-1))
        # end if

        return self.m_times[n]
    #end: getTime


    # Returns the times where we have recorded the information
    def getTimes(self):
        return self.m_times.copy()
    # end: getTimes


    # Returns the time step sizes
    def getDT(self, n = None):

        if(n is None):
            return self.m_dt.copy()

        if(n < 0 or n >= self.nStates()):
            raise ValueError("Illegal step requested, requested time {}, but only {} are known.".format(n, self.nStates()-1))
        # end if

        return self.m_dt[n]
    # end: getDT



    #
    # ==============================
    # simSeries Compability function.
    #   This function allows a database to fake
    #   a simulation series, at least in some cases.
    #

    # This function returns true if *self is on markers.
    #  Thus this function always returns true.
    def onMarker(self):
        return True
    # end def: onMarkers


    # This function returns the number of markers that are stored.
    #  If no state index is given the first one is returned.
    def nMarkers(self, sIdx = None):

        N = -1
        if(sIdx is None):
            N = self.m_nMarkers[0]
        else:
            N = self.m_nMarkers[sIdx]

        return N
    # end def: nMarkers


    #
    # =============================
    # Special queries
    #   These are function that are supposed to
    #   make life easier.
    #

    # This function returns the marker type of
    #   a given marker. Note also a list is supported.
    #   A state index can be given. if set to None, the initial
    #   state is queries
    #
    #  idx          Indexes we want to test
    #  stateIdx     At which timepoint we should look
    #
    def getTypeofMarker(self, idx, stateIdx = None, F = None):

        # If state index is not provided use the first one.
        sIdx = stateIdx if not sTraits.isNone(stateIdx) else 0

        if(sTraits.isInt(sIdx) == False):
            raise ValueError("The state index must be an integer.")
        # end if:

        f = F

        try:
            if(F is None):
                f = self.getDBHandler()

            # Load the initial state, already ints
            iniType = self.getSimState(stateIdx = sIdx,
                    prop = PropIdx.Type, onMarker = True, comp = None, F = f)

            return iniType[idx]

        finally:
            if(F is None):
                self.closeDBHandler(f)
    # end def: getMarkerType


    # This function returns an index list of all markers that have a certain type.
    #  The function accepts an optional state index, that can be used to select
    #  the time point which should be used.
    #
    #  Note that both mType and stateIdx can be a list. mTypes are then unied
    #  together and if multiple stateIdx are passed, a list is returned, same
    #  order, that contains the index associations at the requested times.
    #
    #
    #   mType       The type that should be looked for
    #   stateIdx    State index.
    #   allowEmpty  If set to true, then no empty set of index is allowed.
    #
    def getIdxOfMarkersWithType(self, mType,
            stateIdx = None, allowEmpty = False, F = None):

        # If state index is not provided use the first one.
        sIdx = stateIdx if not sTraits.isNone(stateIdx) else 0

        sIdxList  = list()       # This is the list of state index that has to be loaded
        mTypeList = list()       # This is the list of all marker types that has to be loaded.


        # This is the list of all marker types we have to look for
        if(sTraits.isInt(mType) ):
            mTypeList.append(mType)
        else:
            mTypeList.extend(mType)
        # end if: fill list of markers
        if(len(mTypeList) == 0):
            raise ValueError("No marker types are specified.")
        # end if:

        # handle the state index
        if(sTraits.isInt(sIdx) ):
            sIdxList.append(sIdx)
        else:
            sIdxList.extend(sIdx)
        if(len(sIdxList) == 0):
            raise ValueError("No state index where detected")

        # This is the list of indexes of the different state
        selIdx = list()

        f = F

        try:
            if(F is None):
                f = self.getDBHandler()

            # This are the index values that are used.
            iIdx = np.arange(start=0, stop=self.nMarkers(), step=1, dtype=np.int32)

            # Going through the markers and test which one belongs to which
            for i in sIdxList:
                # Load the markers
                mTypeV = self.getSimState(stateIdx = i, prop = PropIdx.Type, onMarker = True, F = f)

                if(len(mTypeV) != len(iIdx)):
                    iIdx = np.arange(start=0, stop=len(mTypeV), step=1, dtype=np.int32)

                # Figuring out which indexes we need, and store them
                selIdx.append(iIdx[np.isin(mTypeV, mTypeList)])

                if(allowEmpty == False):
                    if(len(selIdx[-1]) == 0):
                        raise ValueError("Did not found any marker in state index {} with the type {}.".format(i, mType))
                # end check:
            # end for(i):
        finally:
            if(F is None):
                self.closeDBHandler(f)
        # end finally

        # Test if only one state was requested.
        if(len(selIdx) == 1):
            return selIdx[0]
        # end if: only one index

        return selIdx
    # end def: getIdxOfMarkersWithType


    #
    # ===============================
    # INTERNAL FUNCTIONS
    #

    # This function loads the grid geometry path of a data set.
    #  In the end it will read out the "X" and "Y" attributes of the
    #  dpecified dataset and return them, in that order, which is
    #  different to EGD. Note that they will be casted to strings.
    #
    #   prop        The property to load.
    #   comp        optional component.
    #   stateIdx    The state index to load.
    #   F           Optional file handler
    #
    def getGeoPath(self,
        prop, stateIdx = None,
        comp = None, F = None):

        # If state index is None, map it to 0
        stateIdx = 0 if stateIdx is None else stateIdx

        if(prop is None):
            raise ValueError("Passed None as property index.");
        if(sTraits.isInt(stateIdx) == False or (stateIdx < 0) or (stateIdx >= self.nStates())):
            raise ValueError("Passed an invalid index {}".format(stateIdx))
        # end if: checks

        # This si teh variable that we will be using
        f = F

        #
        # START
        try:
            if F is None:
                f = self.getDBHandler()

            # This will store the two arguments
            geoPath = ["X", "Y"]

            # Composing the path, will also verify if it exists or not
            PATH = self.composePATH(prop = prop, stateIdx = stateIdx, comp = comp, onMarker = False, F = f)

            # We now have to load the attributes
            gPropAttrs = f[PATH].attrs

            for q in range(len(geoPath)):

                A = geoPath[q]
                if(A not in  gPropAttrs):
                    for n, v in gPropAttrs.items():
                        print("{} -> {}".format(n, v))
                    raise ValueError("no '{}' attribute under the PATH '{}'. Maybe data base is too old.".format(A, PATH))
                # end if

                # pyHDF5 will not returns a python string, but a numpy byte type.
                # This has to be casted into a UTF-8 string.
                # https://stackoverflow.com/questions/23618218/numpy-bytes-to-plain-string/23618235#23618235
                geoPath[q] = gPropAttrs[A].decode('UTF-8')
            # end for(q):

            for w in geoPath:
                if(self.hasDataSet(w, F = f) == False):
                    raise ValueError("Could not find '{}'.".format(w))
                # end if: check
            # end for(w):
        # END
        #
        finally:
            # Now close the file if needed
            if F is None:
                self.closeDBHandler(f)
            # end if: close file
        # end: finally

        # Make a tuple out of it
        return tuple(geoPath)
    # end def: getGeoPath


    # This function returns true if *this has a certain data set
    # If the file is open, then you have to pass the file to the function.
    # If the file is not passed it is tried to open it
    def hasDataSet(self, PATH, F = None):

        # This si teh variable that we will be using
        f = F

        try:
            if F is None:
                f = self.getDBHandler()
            # end if: F is not given so we open it

            # This is teh result
            retVal = False

            if PATH in f:
                return True
            else:
                return False

        finally:
            # Now close the file if needed
            if F is None:
                self.closeDBHandler(f)
    # end: hasDataSet


    # This function loads a data set. By passing the already open file
    #  by parameter F the file did non need to be opened.
    #  if the file is not passed it will be open and closed afterwards.
    #  Note that in the past there were errors when the file was opened twice.
    #
    #  This function will transform the output data into a C continuus array,
    #  with a data type that is equivalent to float64. However if isType and
    #  onMarker are both True, then the type that is returned is int32. this
    #  is the same type that is also used inside csibyl.
    #
    def loadDataSet(self, PATH, onMarker, isType = False, F = None):

        if(util.isValidStr(PATH, noEmpty = True) == False):
            raise ValueError("Did not passed a valid string, instead passed '{}'.".format(PATH))
        # end if: check if pressent

        f = F

        try:
            if F is None:
                f = self.getDBHandler()

            if self.hasDataSet(PATH, F = f) == False:
                raise ValueError("The Dataset '{}' is unknown".format(PATH))
            #end if:

            # Load the data set and ensure that it is unique
            dataSet = copy.deepcopy(f[PATH][:])

            # If we are not on markers then we must perform some actions
            if(onMarker == False):

                # We must have two dimensions
                if(len(dataSet.shape) != 2):
                    raise ValueError("Loaded a grid property, but the property, did not have two dimensios, instead its shape was ~ {}".format(dataSet.shape))
                # end if: check

                # We have to transpose it first, this accounts for the fact
                # that Eigen uses colmajor order, and NumPy rowmajor order.
                # This also works with marker property, since they are 1D
                # and this operation will have no effect on them.
                dataSet = dataSet.transpose()

                # Note that the returned struct has for some odd reason fortran continuues
                #  memory, so we have to account for it as well, we also want to ensure that
                #  we have float64
                if((dataSet.flags['C_CONTIGUOUS'] == False     ) or
                   (dataSet.dtype                 != np.float64)   ):
                    # It is not C continuus, so we will now transform it
                    dataSet = np.ascontiguousarray(a = dataSet, dtype=np.float64)
                # end if: convert order
            # End: have to load a merker

            else:

                # If we are here, then we have loaded a marker, it must be a one dimensional array.
                #  so memory order is irelevant
                # Because of the way it is stored in C, it is a 2D array, with one of its shapes set to
                # 1, so we have to flatten it first, according to the documentation, the following will
                # work, without doing a copy, at least I hope that
                dataSet.shape = (dataSet.size,)

                if(len(dataSet.shape) != 1):
                    raise ValueError("The shape of the loaded marker property is wrong, it is ~ {}".format(dataSet.shape))
                # end: check if correct

                # Load the target datatype
                tarDType = np.int32 if isType else np.float64

                # Now test if it is correct, using order is good practice
                if((dataSet.flags['C_CONTIGUOUS'] == False   ) or
                   (dataSet.dtype                 != tarDType)   ):
                    dataSet = np.ascontiguousarray(a = dataSet, dtype = tarDType)
                # end: check if correct type
            # end: onMarker

            # Return the datase
            return dataSet

        finally:
            if F is None:
                self.closeDBHandler(f)
    # end: loadDataSet


    # This function is used to compose the PATH that should be loaded.
    #  It it performs a check if the path is valid, but does nothing furtehr
    #  on it. It is possible to bypass the test, in that case an empty path
    #  is returned, if the path does not exist.
    #  If prop is empty the base path with be returned.
    #
    #   prop        Property to load
    #   stateIdx    The state index
    #   onMarker    Is it a marker property
    #   comp        The component, in tensor cases.
    #   F           Optional file handler.
    #   noExcept    Do not generate an error, instead return empty path if DS is not present.
    #
    def composePATH(self,
            stateIdx, prop,
            onMarker = False, comp = None,
            F = None,
            noExcept = False):

        if(sTraits.isInt(stateIdx) == False):
            raise ValueError("stateIdx is not an integer.")
        if((stateIdx < 0) or (stateIdx >= self.nStates())):
            raise ValueError("No simulation state with id {} is known.".format(stateIdx))
        # end check: state idx

        # This is the path that will be sucesively be build
        idxStr = str(stateIdx)
        idxStr = idxStr.rjust(5, '0')   # Make it four digit long, pad it with '0'
        PATH = '/simulation_states/' + idxStr + '/'

        # Previously we made a test here if the folder exists.
        #  But I have removed it, because it would lead to two look ups.

        # If prop is None and on marker is false, then we will return the
        #  Path to the complete state dump, which is now done
        if(sTraits.isNone(prop) and (onMarker == False)):
            return PATH
        # end if: only base dump was requested.


        # Test if prop is valid
        if(prop is None):
            raise ValueError("Passed none as property.")
        if(prop.isSpecial() ):
            raise ValueError("The passed property is the special propery.")
        # end check: prop

        # Now we will find the subfolder where it belongs to
        # for that we will first use the default, which means that the
        # index dataset name is given by the property
        dsName = prop.toStr()

        # This bool indicates if special handling was needed. This is needed since we have
        #  to know f we have to add "grid"/"marker" token to the path.
        #  You can see this variable as a bool to indicate if the token must be added.
        #  This is not the property SPECIAL
        wasSpecialCase = False

        # tets if we have strain or stress
        if((prop == PropIdx.StrainRate) or (prop == PropIdx.Stress)):

            if comp is None:
                raise ValueError("The requested a tensor property, but no component is given.")
            # end if:

            ccomp = comp    # This si a copy of comp, we will manipulate this

            if(ccomp == 'yx'):      # Rotate
                ccomp = 'xy'

            # This means we have to incooperate the component in the name
            dsName = dsName + '/' + ccomp

            # On the grid this is a special case
            if(onMarker == False):
                wasSpecialCase = True
            # end if:
        # end if: it is stress/strainrate


        # In the case of velocity and pressure, we have to take the mechanical solution
        if((onMarker == False) and ((prop == PropIdx.VelX) or (prop == PropIdx.VelY) or (prop == PropIdx.Pressure))):

            # The names of the data sets are still the same, they are just located in a different group
            dsName = 'mechSol' + '/' + dsName

            wasSpecialCase = True
        # end if: we have to use the mechanical solution

        # In the case of the temp diff we have to use
        if((onMarker == False) and (prop == PropIdx.TempDiff)):

            # The names are still fine, but we have to change the folder.
            dsName = 'tempSol' + '/' + dsName

            wasSpecialCase = True
        # end if: property was requested


        # It is very important that gVel{X,Y} is a specal case, but is not
        #  considered to be one, at least as the assembly of the path is concerned
        if(prop.isGridVelocity() ):
            # We have to modify the data set name, since tzhe name does not match
            # We have to remove the first part of the name
            dsName = dsName[1:]
        # end if: modify name for grid velocity


        # Add the sub folder to the path if we are on the grid or the markers
        #  but only if needed
        if(wasSpecialCase == False):
            if(onMarker == True):
                PATH = PATH + 'marker' + '/'
            else:
                PATH = PATH + 'grid'  + '/'
            # end if: Where is the property
        # end if: was special case

        # Now we have reshaped the dsName such that it should be valid.
        # We will now combine it with the path to get the full path that
        # we have to load. No '/' is needed, was done above
        PATH = PATH + dsName

        f = F
        try:
            if(F is None):
                f = self.getDBHandler()

            if(self.hasDataSet(PATH, F = F) == False):
                if(noExcept == False):
                    raise ValueError("Could not find propery {} on {} in timestep {}; Path was '{}'".format(prop.toStr(), "markers" if onMarker else "grid", stateIdx, PATH))
                else:
                    PATH = ''
            #end if:

            return PATH

        finally:
            if(F is None):
                self.closeDBHandler(f)
    # end: composePATH


    # This functtion returns a state series, and not a simulation series.
    #  A state series is just a list o states.
    #  This function is provided for quick extracting some states.
    #  It is also possible to request that the simulation times are returned.
    #  if so they are second in a pair.
    def getStateSeries(
                self, prop,
                onMarker = False,
                startTime = 0, endTime = -1, inYr = False,
                withInitialState = False,
                comp = None,
                withTime = True,
                F = None
            ):

        # Get the list of indexes that we want to load
        toLoadIndexes = util.getTimeIndicesRANGE(times = self.m_times, start = startTime, ende = endTime, inYr = inYr)

        f = F
        try:
            if(F is None):
                f = self.getDBHandler()

            # In this values we will store the data
            recStates = []              # This is the output of the state
            recTime   = []              # This is teh output of the time

            # Going through all the state and load them
            for i in toLoadIndexes:

                # Load the data set; pass the opened file
                time_i = self.m_times[i]
                dataSet_i = self.getSimState(stateIdx = i, prop = prop, onMarker = onMarker, comp = comp, F = f)

                recStates.append(dataSet_i)
                recTime.append(time_i)
            # end for(i):

            # Decide if times should be returned or not
            if(withTime == True):
                return (recStates, recTime)
            else:
                return recStates

        finally:
            if(F is None):
                self.closeDBHandler(f)
    # end: getStateSeries


    # This function returns a handler to the databse.
    #  Note that this function is not managed in any sense
    #  and the user is responsible for closing it.
    #  This is an internal function.
    #
    def getDBHandler(self):
            f = h5py.File(self.m_dbPath, 'r+')
            return f
    # end def: getDBHandler


    # This function closes the db handler.
    #  This function is not for uniformity that anything else
    #
    #   F   The handler that was previously opened.
    #
    def closeDBHandler(self, F):
        if(F is None):
            raise ValueError("Passed None as handler")
        F.close()
        return
    # end def: closeDBHandler

# End class(DataBase)

