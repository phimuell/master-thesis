# This file is part of Sibly.
# This file allows ploting operations that operates on a database.


# Normal
import h5py
import numpy as np
import math

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
from .PropIdx import PropIdx
from .simSeries import SimSeries
from .dataBase import DataBase
import libSibyl.sibyl_util as util
import libSibyl.sibyl_fmtUtil as fmtUtil
import libSibyl.sibyl_genPlot as genPlot
import libSibyl.sibyl_traits  as sTraits
import libSibyl.sibyl_extractor as sExtractor


# This function plots the evolution of time.
#  This function operates on the getTimes() function
#  that is provided by the database. For certain reasons,
#  This function supports two DBs.
#
#   db      The main db to plot.
#   db2     The second db To compare to.
#   titel   The title, if not given a suitable default will be created.
#
def plotTimeEvol(db, db2 = None,

        xLab = 'Steps',
        yLab = None,    # Depending if two DBs are given a default will be selected.
        title = None,

        fmt = '-r', fmt2 = '--k',
        label = 'db', label2 = 'db2',

        inYr = True,

        loglog = False,         # Plotting in log log scale

        xLim = None, yLim = None
    ):

    # This variable is used to test if a second db was given
    hasSecondDB = False

    # Make some checks
    if(sTraits.isDataBase(db) == False):
        raise ValueError("db does not apear to be a database.")
    # end if: check

    if(db2 is not None):
        if(sTraits.isDataBase(db2) == False):
            raise ValueError("db2 does not appear to be a database.")
        # end if: check

        hasSecondDB = True
    # end if: db2 was given

    #
    # Set all defaults
    if(title is None):
        if(hasSecondDB == False):
            title = "Evolution of the simulation time"
        else:
            title = "Comparisson of the simulation times"
    # end if: set title

    if(yLab is None):
        yLab = "Time [{}]".format("yr" if inYr else "s")
    # end if: set ylabel

    # Load times from the databse
    times1, idx1 = sExtractor.extractTimePoints(src = db, inYr = inYr, start = 0, ende = -1, alsoIdx = True)

    if(inYr ):
        times1 = util.secToYear(times1)

    # Determine which function to use for plotting
    p = plt.semilogy if loglog else plt.plot

    #
    # make the plotting

    p(idx1, times1, fmt, label = label)

    if(hasSecondDB == True):
        # Load times from the databse
        times2, idx2 = sExtractor.extractTimePoints(src = db2, inYr = inYr, start = 0, ende = -1, alsoIdx = True)
        if(inYr ):
            times2 = util.secToYear(times2)
        p(idx2, times2, fmt2, label = label2)
    # end if: plot DB2

    # Limits
    if(xLim is not None):
        plt.xlim(xLim)
    if(yLim is not None):
        plt.ylim(yLim)

    # Title
    if(len(title) > 0):
        plt.title(title)
    # end if: titile

    # Axis labels
    plt.xlabel(xLab)
    plt.ylabel(yLab)

    # test if legent is needed
    if(hasSecondDB == True):
        plt.legend()

    # Show it
    plt.show()
# end def: plotTimeEvolution


# Plot Time step evolution
#  This function is used to plot the evolution of the time step.
#  It can plot the timestep versus the time or the state index.
#  It supports also two databases.
#
#  vsIdx    If this is true indexes are used for the x axis.
def plotDtEvol(db,
        db2 = None,

        vsIdx = True,
        inYr  = True,

        xLab = None,
        yLab = None,    # Depending if two DBs are given a default will be selected.
        title = None,

        fmt = '-r', fmt2 = '--k',
        label = 'db', label2 = 'db2',

        loglog = False,         # Plotting in log log scale

        xLim = None, yLim = None
    ):

    # This variable is used to test if a second db was given
    hasSecondDB = False

    # Make some checks
    if(sTraits.isDataBase(db) == False):
        raise ValueError("db does not apear to be a database.")
    # end if: check

    if(db2 is not None):
        if(sTraits.isDataBase(db2) == False):
            raise ValueError("db2 does not appear to be a database.")
        # end if: check

        hasSecondDB = True
    # end if: db2 was given

    #
    # Set all defaults
    if(title is None):
        if(hasSecondDB == False):
            title = "Evolution of the time step, vs. {}".format('idx' if vsIdx else 'time')
        else:
            title = "Comparisson of the time step, vs. {}".format('idx' if vsIdx else 'time')
    # end if: set title

    if(yLab is None):
        yLab = "Time Step [{}]".format("yr" if inYr else "s")
    # end if: set ylabel

    if(xLab is None):
        xLab = 'Sim State' if vsIdx else 'Time [{}]'.format('yr' if inYr else 's')
    # end if: no x label given

    # Determine which function to use for plotting
    p = plt.semilogy if loglog else plt.plot


    # Loading the timesteps of the first data base
    db1_DT = db.getDT(n = None)
    if(inYr ):
        db1_DT = util.secToYear(db1_DT)

    db1_pp = None
    if(vsIdx ):
        db1_pp = np.arange(len(db1_DT), dtype = int)
    else:
        db1_pp = db.getTimes()
        if(inYr ):
            db1_pp = util.secToYear(db1_pp)
    # end if: load the x value

    #
    # make the plotting
    p(db1_pp, db1_DT, fmt, label = label)

    if(hasSecondDB == True):
        # Load the plot points
        db2_DT = db2.getDT(n = None)
        if(inYr ):
            db2_DT = util.secToYear(db2_DT)

        db2_pp = None
        if(vsIdx ):
            db2_pp = np.arange(len(db2_DT), dtype = int)
        else:
            db2_pp = db2.getTimes()
            if(inYr ):
                db2_pp = util.secToYear(db2_pp)
        # end selecting: x axis for plot 2

        p(db2_pp, db2_DT, fmt2, label = label2)
    # end if: plot DB2

    # Limits
    if(xLim is not None):
        plt.xlim(xLim)
    if(yLim is not None):
        plt.ylim(yLim)

    # Title
    if(len(title) > 0):
        plt.title(title)
    # end if: titile

    # Axis labels
    plt.xlabel(xLab)
    plt.ylabel(yLab)

    # test if legent is needed
    if(hasSecondDB == True):
        plt.legend()

    # Show it
    plt.show()
# end def: plotDtEvol





# This function perform plot operations on databases.
#  Not this function is only provided for historical reasons.
#  Use the plotField function in the general plotting file.
#  This function only wraps the mentioned function.
#
#  db       The data base to plot
#  prop     The property to plot.
#  idx      The state index that should be plotted.
#  comp     Component of tensor valued functoions.
#  cMap     The color map that should be used, defaults to jet.
#  cRange   The range that is used to limit the color range; must be a pair.
#  leg      The legend string; omitted if not given.
#  title    The title string; if not given it is formed.
#  xLab     The x label; defaults to [x in m]
#  yLab     The y label; defaults to [y in m]
#
#  velArrow Acivate the Plotting of velocity arrows.
#  xStep    Slicing step for plotting the velocity arrows in x direction.
#  yStep    Slicing step for plotting the velocity arrows in y direction.
def plot_db(db, prop, idx,
        comp  = None,
        cMap = 'jet',
        cRange = None,

        leg = None,

        title = None,
        xLab = 'x in m',
        yLab = 'y in m',

        velArrow = False,
        xStep = 5,
        yStep = 5
    ):

    genPlot.plotField(
            src = db, prop = prop, idx = idx, comp = comp,
            title = None, xLab = xLab, yLab = yLab,
            velArrow = velArrow, xStep = xStep, yStep = yStep,
            xPos = None, yPos = None,
            mIdx = None,
            cMap = cMap, cRange = cRange,
            noBar = False)
# end: plot_db


