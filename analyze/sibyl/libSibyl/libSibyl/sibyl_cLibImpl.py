# This file is part of Sibyl.
#
# This file contians the interface functions of the C part of SIBYL.
# The functions should only be called through this managed interfaces.
#

# Normal
import h5py
import numpy as np
import math
import copy

import libSibyl.sibyl_traits as sTraits
import libSibyl.sibyl_extractor as sExtr

####
# Needed for the c extension

import numpy.ctypeslib as npct
import ctypes
from ctypes import c_int, c_double, c_char

#       C L I B
cLibSibyl = npct.load_library("libcLibSibyl", "./libSibyl/")


#       A R R A Y
array_1d_double = npct.ndpointer(dtype = c_double, ndim = 1, flags = 'C_CONTIGUOUS')
array_1d_int    = npct.ndpointer(dtype = c_int   , ndim = 1, flags = 'C_CONTIGUOUS')
array_2d_double = npct.ndpointer(dtype = c_double, ndim = 2, flags = 'C_CONTIGUOUS')
array_2d_int    = npct.ndpointer(dtype = c_int   , ndim = 2, flags = 'C_CONTIGUOUS')


#       F U N C T I O N S

# cLibSibyl_countAssocMarkers
cLibSibyl.cLibSibyl_countAssocMarkers.restype  = c_int
cLibSibyl.cLibSibyl_countAssocMarkers.argtypes = [array_1d_int, array_1d_int, c_int, c_int, c_int, array_2d_int]

# cLibSibyl_countAssocMarkersK
cLibSibyl.cLibSibyl_countAssocMarkersK.restype  = c_int
cLibSibyl.cLibSibyl_countAssocMarkersK.argtypes = [array_1d_int, c_int, c_int, c_int, array_2d_int]

# cLibSibyl_findAssociation
cLibSibyl.cLibSibyl_findAssociation.restype  = c_int
cLibSibyl.cLibSibyl_findAssociation.argtypes = [array_1d_double, array_1d_double, c_int, array_1d_double, array_1d_double, c_int, c_int, c_int, array_1d_int, array_1d_int]

# cLibSibyl_updateAssociationK
cLibSibyl.cLibSibyl_updateAssociationK.restype  = c_int
cLibSibyl.cLibSibyl_updateAssociationK.argtypes = [array_1d_double, array_1d_double, c_int, array_1d_double, array_1d_double, c_int, c_int, c_int, array_1d_int]

# cLibSibyl_computeTheoreticalExtension
cLibSibyl.cLibSibyl_computeTheoreticalExtension.restype  = c_int
cLibSibyl.cLibSibyl_computeTheoreticalExtension.argtypes = [array_1d_double, array_1d_double, c_int, array_1d_double, array_1d_double, c_int, c_int, array_1d_double]

# cLibSibyl_updateTheoreticalExtension
cLibSibyl.cLibSibyl_updateTheoreticalExtension.restype  = c_int
cLibSibyl.cLibSibyl_updateTheoreticalExtension.argtypes = [array_1d_double, array_1d_double, c_int, array_1d_double, array_1d_double, c_int, c_int, array_1d_int, array_1d_double]

# cLibSibyl_computeTheoreticalExtensionOnCA
cLibSibyl.cLibSibyl_computeTheoreticalExtensionOnCA.restype  = c_int
cLibSibyl.cLibSibyl_computeTheoreticalExtensionOnCA.argtypes = [c_int, array_1d_double, array_1d_double, c_int, c_int, array_1d_int, array_2d_int, array_1d_double]

# cLibSibyl_firstDerivative
cLibSibyl.cLibSibyl_firstDerivative.restype  = c_int
cLibSibyl.cLibSibyl_firstDerivative.argtypes = [array_1d_double, array_1d_double, array_1d_double, c_int, c_int, c_int]

# cLibSibyl_SavGolSmoother
cLibSibyl.cLibSibyl_SavGolSmooth.restype  = c_int
cLibSibyl.cLibSibyl_SavGolSmooth.argtypes = [array_1d_double, array_1d_double, array_1d_double, c_int, c_int, c_int, c_double, c_int, c_int, c_int, c_int, c_int]

# cLibSibyl_computePressureForce
cLibSibyl.cLibSibyl_computePressureForce.restype  = c_int
cLibSibyl.cLibSibyl_computePressureForce.argtypes = [array_1d_double, array_1d_double, c_int, array_1d_int, c_int, array_2d_double, array_1d_double, array_1d_double, c_int, c_int, array_1d_double, ctypes.POINTER(c_double)]

# cLibSibyl_binAzimuthVel
cLibSibyl.cLibSibyl_binAzimuthVel.restype  = c_int
cLibSibyl.cLibSibyl_binAzimuthVel.argtypes = [array_1d_double, array_1d_double, array_1d_double, array_1d_double, c_int, array_1d_double, c_int, array_1d_double, array_1d_double, array_1d_double, ctypes.POINTER(c_int), ctypes.POINTER(c_int), ctypes.POINTER(c_int)]


#
# Runtime controll

# Get löast exit status
cLibSibyl.cLibSibylRT_lastExitStatus.restype = c_int

# Line number with error
cLibSibyl.cLibSibylRT_errorLineNumber.restype = c_int

# Func NameN
cLibSibyl.cLibSibylRT_errorFuncNameN.restype = c_int

# Func NameC
cLibSibyl.cLibSibylRT_errorFuncNameChar.restype  =  c_char
cLibSibyl.cLibSibylRT_errorFuncNameChar.argtypes = [c_int ]

# Error Cond N
cLibSibyl.cLibSibylRT_errorCondN.restype = c_int

# Error Cond Char
cLibSibyl.cLibSibylRT_errorCondChar.restype  =  c_char
cLibSibyl.cLibSibylRT_errorCondChar.argtypes = [c_int ]

# Clear error
cLibSibyl.cLibSibylRT_errorClear.restype       = c_int

# Aquiere the state
cLibSibyl.cLibSibylRT_aquireErrorState.restype = c_int

# get the PID
cLibSibyl.cLibSibylRT_getPID.restype = c_int


# This is a general function that will check the error state
#  of CSIBYL generate an exception and clear the state
#
#   retCode      The return code of the CSIBYL function
#   sName        The name of the sibyl function that failed.
#
def cSibylIntern_checkCode(retCode, sName = ''):

    if(retCode == 0):
        return
    # end

    if(retCode == -123456789):
        raise ValueError("The runtime system of CSIBYL was not initialized.")

    # Get the last error code, note that this will happen atomic
    lastErrorRec = cLibSibyl.cLibSibylRT_lastExitStatus()

    # Several things can happen.
    #  The first one, is that the last error code is zero.
    #  this means a thread has already cleared it.
    #  this means we have an error, that was not installed.
    if(lastErrorRec == 0):
        raise ValueError("{}: An error occured, but the runtime was already cleared, there are different things that meight have caused it. The recorded error was {}".format(sName, retCode))

    # A zero means that it was either not installed, or already handled.
    #  But a non zero means, that it is still in the system. so we must check if this error
    #  is the same as we have detected. If not two python threads have an error, but the other was abel to install the eror, but we not.
    #  So we putput the problem
    if(lastErrorRec != retCode):
        raise ValueError("{}: Two errors have occured, but our was not installed. We have an error {}, but installed was {}".format(sName, retCode, lastErrorRec));

    # Now we try to aquiere the state
    stateReturn = cLibSibyl.cLibSibylRT_aquireErrorState()
    if(stateReturn != 0):
        raise ValueError("{}: Could not aquiere the error state, aquiere returned {}, stored error code is {}, passed error code {}.".format(sName, stateReturn, lastErrorRec, retCode))
    # end

    # extract function name
    nFn = cLibSibyl.cLibSibylRT_errorFuncNameN()
    fn = ""
    for i in range(nFn):
        c = cLibSibyl.cLibSibylRT_errorFuncNameChar(i)
        cc = bytes(c)  # https://www.journaldev.com/23500/python-string-to-bytes-to-string
        fn = "{}{}".format(fn, cc.decode())
    # end for(i)

    # extract condition
    nCond = cLibSibyl.cLibSibylRT_errorCondN()
    cond = ""
    for i in range(nCond):
        c = cLibSibyl.cLibSibylRT_errorCondChar(i)
        cc = bytes(c)
        cond = "{}{}".format(cond, cc.decode() )
    # end for(i):

    # Line number
    lNum = cLibSibyl.cLibSibylRT_errorLineNumber()


    # In the mean time an error could be cleared an an other installed.
    # This must be checked, by again read the installed error. however we have to take responsibility for this one.
    lastErrorRec2 = cLibSibyl.cLibSibylRT_lastExitStatus()


    # Compose the string
    s = "cSibyl ERROR({})".format(lastErrorRec2)

    # Write down if the error has changed during action.
    if(lastErrorRec2 != retCode):
        s = "{}; There was some inconsietncy, the error we wanted to handle was already handled, but we got a new one, we will do it insteat, but our error was {}".format(s, retCode)

    if(len(sName) != 0):
        s = "{} in SIBYL function '{}'".format(s, sName)

    if(nFn > 0):
        s = "{}. Error location was '{}".format(s, fn)
        if(lNum > 0):
            s = "{}:{}".format(s, lNum)
        s = "{}'".format(s)

    if(nCond > 0):
        s = "{}, error message was '{}'".format(s, cond)

    r = cLibSibyl.cLibSibylRT_errorClear()
    if(r != 0):
        raise ValueError("Error wile clearing the error: {}".format(r))

    raise ValueError(s)
# end def: checkCode


#
##########


# This is the implementation for counting the marker association.
#  The number of grid points must be passed to this function,
#  and the associations. This function returns an $nGridY \times nGridX$
#  matrix, each entry contains the number of markers that are
#  associated to it.
#
#   yAssoc      To which associated in y direction.
#   xAssoc      To which associated in x direction.
#   nGridY      Numbers of grid points in x direction.
#   nGridX      Numbers of grid points in x direction.
#
def cSibyl_countAssocMarker(yAssoc, xAssoc, nGridY, nGridX):

    if(sTraits.isIndexSeries(yAssoc) == False):
        raise ValueError("yAssoc is not an index series, it is {}".format(type(yAssoc)))
    if(sTraits.isIndexSeries(xAssoc) == False):
        raise ValueError("xAssoc is not an index series, it is {}".format(type(xAssoc)))
    if(len(xAssoc) != len(yAssoc)):
        raise ValueError("The two index series have different lengeth. len(xAssoc) = {}; len(yAssoc) = {}".format(len(xAssoc), len(yAssoc)))
    # end check: assoc

    nGridX_ = nGridX if (sTraits.isArray(nGridX) == False) else len(nGridX)
    nGridY_ = nGridY if (sTraits.isArray(nGridY) == False) else len(nGridY)

    if(sTraits.isInt(nGridX_) == False or
                     nGridX_  <  1       ):
        raise ValueError("Number of x grid points is invalid.")
    if(sTraits.isInt(nGridY_) == False or
                     nGridY_  <  1       ):
        raise ValueError("Number of y grid points is invalid.")
    # end check: number of grid poinst


    # Generate the output array
    nCountAssoc = np.zeros((nGridY_, nGridX_), dtype = c_int)

    # call the function
    ret = cLibSibyl.cLibSibyl_countAssocMarkers(yAssoc, xAssoc, len(yAssoc), nGridY_, nGridX_, nCountAssoc)

    cSibylIntern_checkCode(ret, "countAssocMarkers")

    return nCountAssoc
# end def: cSibyl_countAssocMarters


# Similar to the normal count function, but this function has the association stored
#  in one vector. The format is conmpatible to the findK association function, as well
#  as the updating function. This function returns an $nGridY \times nGridX$  matrix,
#  each entry contains the number of markers that are associated to it.
#
#   kAssoc      To which associated, in compressed format.
#   nGridY      Numbers of grid points in x direction.
#   nGridX      Numbers of grid points in x direction.
#
def cSibyl_countAssocMarkerK(kAssoc, nGridY, nGridX):

    if(sTraits.isIndexSeries(kAssoc) == False):
        raise ValueError("xAssoc is not an index series, it is {}".format(type(xAssoc)))
    # end: checks

    nGridX_ = nGridX if (sTraits.isArray(nGridX) == False) else len(nGridX)
    nGridY_ = nGridY if (sTraits.isArray(nGridY) == False) else len(nGridY)

    if(sTraits.isInt(nGridX_) == False or
                     nGridX_  <  1       ):
        raise ValueError("Number of x grid points is invalid.")
    if(sTraits.isInt(nGridY_) == False or
                     nGridY_  <  1       ):
        raise ValueError("Number of y grid points is invalid.")
    # end check: number of grid poinst


    # Generate the output array
    nCountAssoc = np.zeros((nGridY_, nGridX_), dtype = c_int)

    # call the function
    ret = cLibSibyl.cLibSibyl_countAssocMarkersK(kAssoc, len(kAssoc), nGridY_, nGridX_, nCountAssoc)

    cSibylIntern_checkCode(ret, "countAssocMarkersK")

    return nCountAssoc
# end def: cSibyl_countAssocMartersK


# This function finds the associated node index.
#  If the passed grid sizes are tight you can use pass the tight option.
#  Note that this function returns first the association in X
#  and then in Y, this is different from EGD.
#
#   yPosMarker      y positions of the markers.
#   xPosMarker      x positions of the markers.
#   gPosY           y positions of the grid nodes.
#   gPosX           x positions of the grid nodes.
#   isTight         If the grid nodes complitly bound the grid.
#
def cSibyl_findAssociation(
        xPosMarker, yPosMarker,
        gPosX, gPosY = None,
        isTight = False):

    gPosX_ = None
    gPosY_ = None

    if(sTraits.isArray(xPosMarker) == False or
       sTraits.isArray(yPosMarker) == False   ):
        raise ValueError("The passed positions are not arraies.")
    if(len(xPosMarker) != len(yPosMarker)):
        raise ValueError("The length of the two marker positions arrays differ, x ~ {}; y ~ {}.".format(len(xPosMarker), len(yPosMarker)))
    if(sTraits.isTuple(gPosX, 2) == True):
        gPosX_ = gPosX[1]
        gPosY_ = gPosX[0]
    else:
        gPosX_ = gPosX
        gPosY_ = gPosY
    if(sTraits.isArray(gPosX_) == False or
       sTraits.isArray(gPosY_) == False   ):
        raise ValueError("The positions array are wrong.")
    # end checks:

    # Generate return values
    J = np.zeros(len(xPosMarker), dtype = c_int)
    I = np.zeros(len(yPosMarker), dtype = c_int)

    # Generate input value
    isTightInt = int(0) if isTight == False else int(1)
    nMarkers = int(len(xPosMarker))
    nGridX   = int(len(gPosX_))
    nGridY   = int(len(gPosY_))

    # Call the function
    ret = cLibSibyl.cLibSibyl_findAssociation(
            yPosMarker, xPosMarker, nMarkers,
            gPosY_, gPosX_, nGridY, nGridX,
            isTightInt,
            I, J)

    cSibylIntern_checkCode(ret, "findAssociation")

    # Different ordering than in EGD
    return (J, I)
# end def: find acssociation


# This function updates a previously knwon associon to the new state.
#  A default association can be created with cSibyl_defAssociation.
#  This function only creates one vector for the association.
#
#   yPosMarker      y positions of the markers.
#   xPosMarker      x positions of the markers.
#   K               The association to update.
#   gPosY           y positions of the grid nodes.
#   gPosX           x positions of the grid nodes.
#   isTight         If the grid nodes complitly bound the grid.
#   retK            If this flarg is set to true K will be returned (no copy is explicitly made).
#
def cSibyl_updateAssociationK(
        xPosMarker, yPosMarker,
        K,
        gPosX, gPosY = None,
        isTight = False,
        retK = False):

    gPosX_ = None
    gPosY_ = None

    if(sTraits.isArray(xPosMarker) == False or
       sTraits.isArray(yPosMarker) == False   ):
        raise ValueError("The passed positions are not arraies.")
    if(len(xPosMarker) != len(yPosMarker)):
        raise ValueError("The length of the two marker positions arrays differ, x ~ {}; y ~ {}.".format(len(xPosMarker), len(yPosMarker)))
    if(len(K)          != len(yPosMarker)):
        raise ValueError("The association vector has the wrong length len(K) = {}, but len(yPosMarker) = {}.".format(len(K), len(yPosMarker)))
    if(sTraits.isTuple(gPosX, 2) == True):
        gPosX_ = gPosX[1]
        gPosY_ = gPosX[0]
    else:
        gPosX_ = gPosX
        gPosY_ = gPosY
    if(sTraits.isArray(gPosX_) == False or
       sTraits.isArray(gPosY_) == False   ):
        raise ValueError("The positions array are wrong.")
    # end checks:


    # Generate input value
    isTightInt = int(0) if isTight == False else int(1)
    nMarkers = int(len(xPosMarker))
    nGridX   = int(len(gPosX_))
    nGridY   = int(len(gPosY_))

    # Call the function
    ret = cLibSibyl.cLibSibyl_updateAssociationK(
            yPosMarker, xPosMarker, nMarkers,
            gPosY_, gPosX_, nGridY, nGridX,
            isTightInt,
            K)

    cSibylIntern_checkCode(ret, "updateAssociationK")

    if(retK ):
        return K
# end def: update acssociation


# This function allows to create a default association vector.
#  This vector is made such, that it is obviously wrong.
#
#   n       Number of markers or an array.
#
def cSibyl_defAssociation(n):

    N = -1
    if(sTraits.isArray(n) ):
        N = len(n)
    elif(sTraits.isInt(n) ):
        N = n
    else:
        raise ValueError("Unknown type {}.".format(type(n)))

    K = -np.ones(N, dtype = c_int)

    return K
# end def: defAssocation


# This function updates the theoretical extension of markers.
#  This is done by an additional argument. This function also
#  accepts the last known associated state of the markers and
#  will update it instead of fully recompute it.
#  The function is able to make sense of a lot of things.
#  But best is to
#
#   yPosMarker      y positions of the markers.
#   xPosMarker      x positions of the markers.
#   gPosY           y positions of the basic grid nodes.
#   gPosX           x positions of the basic grid nodes.
#   K               last known association.
#
def cSibyl_updateTheoreticalExtension(
        xPosMarker, yPosMarker, K,
        gPosX, gPosY = None):

    gPosX_ = None
    gPosY_ = None

    if(sTraits.isArray(xPosMarker) == False or
       sTraits.isArray(yPosMarker) == False   ):
        raise ValueError("The passed positions are not arraies.")
    if(len(xPosMarker) != len(yPosMarker)):
        raise ValueError("The length of the two marker positions arrays differ, x ~ {}; y ~ {}.".format(len(xPosMarker), len(yPosMarker)))
    if(sTraits.isIndexSeries(K) == False):
        raise ValueError("K is not an index series, it is {}".format(type(K)))
    if(len(K) != len(xPosMarker)):
        raise ValueError("The length of K is not correct, it is {}, but should be {}".format(len(K), len(xPosMarker)))
    if(sTraits.isTuple(gPosX, 2) == True):
        gPosX_ = gPosX[1]
        gPosY_ = gPosX[0]
    else:
        gPosX_ = gPosX
        gPosY_ = gPosY
    if(sTraits.isArray(gPosX_) == False or
       sTraits.isArray(gPosY_) == False   ):
        raise ValueError("The positions array are wrong.")
    # end checks:

    # Generate input value
    isTightInt = int(1)
    nMarkers = int(len(xPosMarker))
    nGridX   = int(len(gPosX_))
    nGridY   = int(len(gPosY_))

    # Generate output value
    theoExtension = np.zeros(nMarkers, dtype = c_double)

    # Call the function
    ret = cLibSibyl.cLibSibyl_updateTheoreticalExtension(
            yPosMarker, xPosMarker, nMarkers,
            gPosY_, gPosX_, nGridY, nGridX,
            K,
            theoExtension)

    cSibylIntern_checkCode(ret, "updateTheoreticalExpansion")

    return theoExtension
# end def: update theoretical extension


# This function computes the theoretical extension of a set of markers.
#  The theoretical extension is an area. It is the mean area a marker
#  covers in a cell. Put differently, it is the spatial extension of a
#  cell divided by the number of markers in it. The passed grid positions
#  are interpreted as basic nodal points.
#
#   yPosMarker      y positions of the markers.
#   xPosMarker      x positions of the markers.
#   gPosY           y positions of the basic grid nodes.
#   gPosX           x positions of the basic grid nodes.
#
def cSibyl_computeTheoreticalExtension(
        xPosMarker, yPosMarker,
        gPosX, gPosY = None):

    gPosX_ = None
    gPosY_ = None

    if(sTraits.isArray(xPosMarker) == False or
       sTraits.isArray(yPosMarker) == False   ):
        raise ValueError("The passed positions are not arraies.")
    if(len(xPosMarker) != len(yPosMarker)):
        raise ValueError("The length of the two marker positions arrays differ, x ~ {}; y ~ {}.".format(len(xPosMarker), len(yPosMarker)))
    if(sTraits.isTuple(gPosX, 2) == True):
        gPosX_ = gPosX[1]
        gPosY_ = gPosX[0]
    else:
        gPosX_ = gPosX
        gPosY_ = gPosY
    if(sTraits.isArray(gPosX_) == False or
       sTraits.isArray(gPosY_) == False   ):
        raise ValueError("The positions array are wrong.")
    # end checks:

    # Generate input value
    isTightInt = int(1)
    nMarkers = int(len(xPosMarker))
    nGridX   = int(len(gPosX_))
    nGridY   = int(len(gPosY_))

    # Generate output value
    theoExtension = np.zeros(nMarkers, dtype = c_double)

    # Call the function
    ret = cLibSibyl.cLibSibyl_computeTheoreticalExtension(
            yPosMarker, xPosMarker, nMarkers,
            gPosY_, gPosX_, nGridY, nGridX,
            theoExtension)

    cSibylIntern_checkCode(ret, "theoreticalExpansion")

    return theoExtension
# end def: find theoretical extension


# This function assignes the theoretical extensions to markers.
#  In contrast to other version of this function family, it does not
#  compute the assocaition nor counts the amrkers. It only assignes
#  the markers. The assocaition and count that are used, are assumed to be
#  updated already, thus no positions are taken.
#
#   K               The association of a marker to the grid points.
#   nCellCount      Matrix that stores the number of markers in each cell.
#   gPosY           y positions of the basic grid nodes.
#   gPosX           x positions of the basic grid nodes.
#
def cSibyl_assignTheoreticalExtension(
        K, nCellCount,
        gPosX, gPosY = None):

    gPosX_ = None
    gPosY_ = None

    if(sTraits.isTuple(gPosX, 2) == True):
        gPosX_ = gPosX[1]
        gPosY_ = gPosX[0]
    else:
        gPosX_ = gPosX
        gPosY_ = gPosY
    if(sTraits.isArray(gPosX_) == False or
       sTraits.isArray(gPosY_) == False   ):
        raise ValueError("The positions array are wrong.")
    # end checks:

    # Generate input value
    nMarkers = int(len(K))
    nGridX   = int(len(gPosX_))
    nGridY   = int(len(gPosY_))

    # Generate output value
    theoExtension = np.zeros(nMarkers, dtype = c_double)

    # Call the function
    ret = cLibSibyl.cLibSibyl_computeTheoreticalExtensionOnCA(
            nMarkers,
            gPosY_, gPosX_, nGridY, nGridX,
            K, nCellCount,
            theoExtension)

    cSibylIntern_checkCode(ret, "assigneTheoreticalExpansion")

    return theoExtension
# end def: assigne theoretical extension


# This function is used to compute the derivative of a series of numbers using cLibSibyl.
#  The derivatives can be computed using a backwards or forward scheme, default is forward.
#  This function only computes the first derivative.
#  Note that the returned series has the same length as the input series, the point, where
#  the stencil could not be used, a different one is used.
#  The schem that is used can be selected by two different ways, first explicit by specifing
#  the order and scheme ID or by using the selecting switches.
#
#   y           Function values.
#   x           x psoition where the function is defined
#   useForward  Use forward differenciation.
#   order       The order that should be used, currently only 1 is supported.
#
def cSibyl_firstDerivative(
        y, x,
        useForward = False, useBackward = False, useCentral = True,
        order = None, scheme = None):

    if(len(y) != len(x)):
        raise ValueError("The lengths are different. len(x) = {}, len(y) = {}.".format(len(x), len(y)))
    if(len(y) <= 0):
        raise ValueError("The length of y (and x) are zero.")
    # end checks:

    if(useForward == True):
        if(sTraits.isNone(scheme) ):
            scheme = int(1)
        if(sTraits.isNone(order) ):
            order  = 1
    # end if: forward
    elif(useBackward == True):
        if(sTraits.isNone(scheme) ):
            scheme = int(-1)
        if(sTraits.isNone(order) ):
            order  = 1
    # end if: backwards
    elif(useCentral == True):
        if(sTraits.isNone(scheme) ):
            scheme = int(0)
        if(sTraits.isNone(order) ):
            order  = 2
    # end if: central

    if(sTraits.isInt(scheme) == False):
        raise ValueError("The scheme was not valid, it was {}.".format(scheme))
    if(sTraits.isInt(order ) == False):
        raise ValueError("The order was not valid, it was {}.".format(order))
    # end check:

    # prepare input
    orderPass  = int(order)
    N          = len(y)

    # Ouptput
    dy = np.zeros(N, float)

    # Call the function
    ret = cLibSibyl.cLibSibyl_firstDerivative(y, x, dy, N, scheme, orderPass)

    cSibylIntern_checkCode(ret, "firstDerivative")

    return dy
# end def: cSibyl_firstDerivative


# This function is able to smooth a signal, that is non equidistance.
#  For this savgol is used, which is a moving window, inside that a
#  polynomial is fitted. This function allows to do screwed windows.
#  It also allows to compute the derivation of the signal directly.
#  The data set can be restricted, moving the start and end point.
#
#   x       x coordinates of the observations
#   y       Observed values
#   wSize   The size of the moving window.
#   mR      Left fraction of the moving window, defaults to 0.5, centred.
#   deg     The degree of the polynomial that is fitted, default 3.
#   dy      The derivative that should be performed.
#   start   The first point to start.
#   ende    where the domain stop.
#   doCopy  copy the value in yHat, only possible dy == 0.
#   noSkew  Do not skew the window at the edges, shrink domain.
#
def cSibyl_smooth(
        x, y, wSize,
        mR = 0.5,
        deg = 3,
        dy = 0,
        start = 0,
        ende = -1,
        inYr = False,
        doCopy = True,
        noSkew = False):

    if(len(x) != len(y)):
        raise ValueError("x and y have not the same length. x ~ {}; y ~ {}".format(len(x), len(y)))

    # Get the time points we need
    Idx = sExtr.getTimeIndices(times = x, start = start, ende = ende, inYr = inYr)
    sIdx = c_int(Idx[ 0])
    eIdx = c_int(Idx[-1])

    # Convert the values
    doCopyArg = c_int(1 if doCopy else 0)
    noSkewArg = c_int(1 if noSkew else 0)

    # Allocate the return value
    yHat = np.zeros_like(y)
    n    = len(yHat)

    # Call the C function
    retVal = cLibSibyl.cLibSibyl_SavGolSmooth(x, y, yHat, dy, deg, wSize, mR, n, sIdx, eIdx, doCopyArg, noSkewArg)

    cSibylIntern_checkCode(retVal, "SavGolSmoother")

    return yHat
# end def: smooth


# This function is able to compute the force that is exerted by pressure on a set of markers.
#  The force is calculated by integrating the pressure over the convex hull. As a byproduct
#  the volume of that object is computed too, it is possible to ignore that.
#  If no index vector is passed to the function, then all selected points are used.
#  The pressure field that is passed is expected to be on the extended pressure grid. And
#  all nodes are used.
#
#   xPos    x coordinates of the markers
#   yPos    y coordinates of the markers
#   P       The pressure field.
#   xP      x coordinates of the pressure nodes.
#   yP      y coordinates of the pressure nodes.
#   selIdx  List of indexes of the selected markers, if empty, then all are used.
#   noVol   If given the volume is not returned as second argument.
#
def cSibyl_computePressForce(
        xPos, yPos,
        P, xP, yP,
        selIdx = None,
        noVol = False):

    # Load some parameters
    nMarkers = len(xPos)

    # Make some small tests
    if(len(xPos.shape) != 1):
        raise ValueError("The shape of xPos is not correct, it is {}.".format(xPos.shape()))
    if(len(yPos) != nMarkers):
        raise ValueError("The length of yPos ({}) is not the same as the one for xPos ~ {}".format(len(yPos), nMarkers))
    if(len(xP) != P.shape[1]):
        raise ValueError("The passed presser field is wrong, it has {} many columns, but expected {}.".format(len(xP), P.shape[1]))
    if(len(yP) != P.shape[0]):
        raise ValueError("The passed presser field is wrong, it has {} many rows, but expected {}.".format(len(yP), P.shape[0]))
    selIdx_ = selIdx if selIdx is not None else np.zeros(0, c_int)
    nSels   = 0 if selIdx is None else len(selIdx_)

    vol = c_double(0.0)        # This is the variable for vol
    pForce = np.zeros(2, dtype=np.float64)    # This is the variable for the pressure force

    # Now call the C code
    ret = cLibSibyl.cLibSibyl_computePressureForce(
            xPos, yPos, nMarkers,
            selIdx_, nSels,
            P, xP, yP, len(xP), len(yP),
            pForce, ctypes.byref(vol) )

    # Check the return value
    cSibylIntern_checkCode(ret, "computePressForce")

    # Make a python float out of the valume
    vol = vol.value

    if(noVol == True):
        return pForce
    else:
        return (pForce, vol)
# end def: compute pressure force



# This function finds the azimuth velocities.
#  The markers are binned and for each bin the mean and std azimuth
#  velocity is computed. It is also possible to obtain a statistic.
#  (meanBinVel, stdBinVel, (nProcessed, nTooLow, nTooLarge))
#  The last pair is optional and the statistic.
#  The bin edges are such that they include the right most point.
#  Thus nBins = len(binEdges) - 1.
#
#   x       x position of the markers.
#   y       y position of the markers.
#   vx      x velocity of the markers.
#   vy      y velcoity of the markers.
#   binEgde Coordinates of the bin edges.
#   orig    The origin of the point, defaults to zero (first x and then y)
#   stat    Should statistic be returned.
#
def cSibyl_binAzimuthVel(x, y, vx, vy, binEdges,
            orig = None,
            stat = False):

    # Load some variables
    nMarkers = len(x)
    nBins    = len(binEdges) - 1

    # Make some checks
    if(nMarkers <= 0):
        raise ValueError("The number of x coordinates is invalid.")
    if(nMarkers != len(y)):
        raise ValueError("Expected {} many markers, but only got {} many for y.".format(nMarkers, len(y)))
    if(nMarkers != len(vx)):
        raise ValueError("Expected {} many markers, but only got {} mayn for vx.".format(nMarkers, len(vx)))
    if(nMarkers != len(vy)):
        raise ValueError("Expected {} many markers, but only got {} mayn for vy.".format(nMarkers, len(vy)))
    if(nBins < 1):
        raise ValueError("The number of bins was {}, this is too low.".format(nBins))
    # end check

    ## Create the the needed arguments

    # origes
    argOrig = np.zeros(2, dtype=np.float64)
    if(not sTraits.isNone(orig)):
        if(len(orig) != 2):
            raise ValueError("Passed {} many origens instead of 2.".format(len(orig)))
        argOrig[0] = orig[0]
        argOrig[1] = orig[1]
    # end: handle orig

    # statistic
    nProcessed = c_int(0)
    nTooLow    = c_int(0)
    nTooLarge  = c_int(0)

    # vel stat
    binMean = np.zeros(nBins, dtype=np.float64)
    binStd  = np.zeros(nBins, dtype=np.float64)

    # end: arguments


    # calling the real function
    ret = cLibSibyl.cLibSibyl_binAzimuthVel(x, y, vx, vy, nMarkers,
                                    binEdges, nBins,
                                    binMean, binStd,
                                    argOrig,
                                    ctypes.byref(nProcessed), ctypes.byref(nTooLow), ctypes.byref(nTooLarge))

    # Check the return value
    cSibylIntern_checkCode(ret, "binAzimuthVel")

    T = (binMean, binStd)

    if(stat == True):
        T = (*T, (nProcessed.value, nTooLow.value, nTooLarge.value))
    # end: stats

    return T
# end def: binAzimuth


# This function returns the PID that is used by CSIBYL to denote the log.
def cSibyl_getPID():
    return cLibSibyl.cLibSibylRT_getPID()
# end def:


