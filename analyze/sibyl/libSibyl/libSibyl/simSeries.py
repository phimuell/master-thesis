# This file is a simulation series.
# It is returned by the database.

import numpy as np
import copy

from .PropIdx import PropIdx
import libSibyl.sibyl_util as util
import libSibyl.simSeries_helper as sHelper


# A sim series is a simple wrapper around
# two list one of the fimes and one of
# The state informations.
class SimSeries:

    # Construct it from the data that is given
    #  Times    The recorded time points
    #  Values   The List of the values
    #  onMarker Bool if it is the parker
    #  Prop     The property
    #  Comp     Component when handling with tensor valued functions
    #  {X,Y}Nodal   Are the nodal point of the grids, these are currently vectors.
    def __init__(self, Times, Values, onMarker, Prop, Comp, XNodal, YNodal):

        if(len(Times) != len(Values)):
            raise ValueError("The length of the time series is {}, but {} different values were provided.".format(len(Times), len(Values)))
        #end if:
        if(util.isSorted(Times, True) == False):
            raise ValueError("The passed times are not sorted.")
        #end if

        # Load everything
        self.m_comp = copy.deepcopy(Comp)
        self.m_prop = copy.deepcopy(Prop)
        self.m_times = np.array(copy.deepcopy(Times))
        self.m_values = copy.deepcopy(Values)
        self.m_onMarker = copy.deepcopy(onMarker)

        if(sHelper.isArray(XNodal)):
            N = len(self.m_values)
            self.m_xNode = list()
            self.m_yNode = list()
            for i in range(N):
                self.m_xNode.append(copy.deepcopy(XNodal))
                self.m_yNode.append(copy.deepcopy(YNodal))
            # end for(i):
        else:
            if(not (sHelper.isList(XNodal) and sHelper.isList(YNodal))):
                raise ValueError("Not the correct type.")
            if(not (sHelper.isArray(XNodal[0]) and sHelper.isArray(YNodal[0])) ):
                raise ValueError("Not a list of arrays.")
            # end if:

            self.m_xNode = copy.deepcopy(XNodal)
            self.m_yNode = copy.deepcopy(YNodal)
        # end if:


        # We will now find some maximal bound it is not very nice to do that so but
        self.m_max   = np.zeros(self.nStates() )
        self.m_min   = np.zeros(self.nStates() )
        self.m_aMax  = np.zeros(self.nStates() ) # maxima off absolute
        self.m_aMin  = np.zeros(self.nStates() )
        self.m_mean  = np.zeros(self.nStates() )
        self.m_std   = np.zeros(self.nStates() ) # Std of a state
        self.m_aMean = np.zeros(self.nStates() ) # Mean of the absolute value

        for i in range(len(self.m_values)):
            # Regular minima and maxima
            self.m_max[i] = np.amax(self.m_values[i], axis = None)
            self.m_min[i] = np.amin(self.m_values[i], axis = None)

            # Max/Min of absolute values
            aValue = np.abs(self.m_values[i])   # Cache the absolute value
            self.m_aMax[i] = np.amax(aValue, axis = None)
            self.m_aMin[i] = np.amin(aValue, axis = None)

            # The mean of the series
            self.m_mean[i]  = np.mean(self.m_values[i], axis = None)
            self.m_aMean[i] = np.mean(aValue          , axis = None)

            # The standard deviation
            self.m_std[i]   = np.std(self.m_values[i] , axis = None)
        # end for(i):

        if(len(self.m_times) != len(self.m_values)):
            raise ValueError("Numner of times and passed values are not the same. Got {} many times but {} many values.".format(len(self.m_times), len(self.m_values)))
        # end if: check


        # Get the global maximas
        self.m_totMax    = np.max(self.m_max )
        self.m_totMin    = np.min(self.m_min )
        self.m_totAbsMax = np.max(self.m_aMax)
        self.m_totAbsMin = np.min(self.m_aMin)

        # Get meta mean information
        self.m_meanMean  = np.mean(self.m_mean)     # The mean over all mean
        self.m_meanStd   = np.std(self.m_mean)      # The std of the means
        self.m_meanMax   = np.max(self.m_mean)
        self.m_meanMin   = np.min(self.m_mean)
    # end __init__


    #
    # =============================
    # Status Functions
    #   These are small functions that returns the
    #   state of *self.


    # This function returns the property that was stored
    def getProp(self):
        return self.m_prop
    # end: getProp


    # This function returns True if *self is sepcial.
    def isSpecial(self):
        return self.m_prop.isSpecial()
    # end def: isSpecial


    # Returns true if *this has a component
    def hasComp(self):
        if(self.m_comp is None):
            return False

        return True
    # end: hasComp


    # This function returns the component of *this
    def getComp(self):
        if(self.hasComp() == False):
            raise ValueError("This has None as component.")
        # end if

        ret = self.m_comp

        return ret
    # end: getComp


    # This function returns the number of states that are stored
    def nStates(self):
        return len(self.m_times)
    # end: nStates


    # Return true if this is are marker properties
    def onMarker(self):
        return self.m_onMarker
    # end: onMarker


    # Return the number of grid points in y direction.
    # if this is a marker state, then this is a synonym of nMarkers
    # Note that this is the first dimension
    def Ny(self):

        if(self.onMarker() == True):
            return self.nMarkers()
        # end if: is marker

        return self.m_values[0].shape[0]
    # end: Ny


    # This returns the number of gird points in x direction.
    # Note that this is the second dimension
    def Nx(self):

        if(self.onMarker() == True):
            return 1
        # end if: is marker

        return self.m_values[0].shape[1]
    # end: Nx


    # This returns the number of markers of *self.
    # Only works if *self is defined on markers.
    def nMarkers(self):

        if(self.onMarker() == False):
            raise ValueError("The simulation series does not contain marker properties.")
        # end if:

        return self.m_values[0].shape[0]
    # end: nMarkers


    #
    # ==============================
    # Max/Min Queries
    #   For several reasons *self stores maxima and
    #   minima of the stored states directly.
    #   Here you can access them.
    #


    # This function returns the maximum number that is stored in state n.
    #  If n is omitted or None, the maximum of all states is returned.
    def getMax(self, n = None):
        if(n is None):
            return self.m_totMax
        # end if:

        # A special entry is requested
        return self.m_max[n]
    # end: getMax


    # This function returns the vector of maximas back
    def getMaxSeries(self):
        return copy.deepcopy(self.m_max)
    # end def: getMaxSeries


    # This function returns the minum number in state n.
    #  If n is omitted or None, the maximum of all states is returned.
    def getMin(self, n = None):
        if(n is None):
            return self.m_totMin

        return self.m_min[n]
    # end: getMin


    # This function returns a vector that contains teh minimum of each
    #  recorded state.
    def getMinSeries(self):
        return copy.deepcopy(self.m_min)
    # end def: getMinSeries


    # This function returns the range of values that is stored in state n.
    #  If n is omitted or None, the whole range is returned.
    def getRange(self, n = None):
        return (self.getMin(n), self.getMax(n))
    # end: get range




    #
    # ==============================
    # Absolute Max/Min Queries
    #   These functions allows accessing the Max/Min
    #   ob the absolute values of the series
    #

    # This function returns the maximum number that is stored in state n.
    #  If n is omitted or None, the maximum of all states is returned.
    def getAbsMax(self, n = None):
        if(n is None):
            return self.m_totAbsMax
        # end if:

        # A special entry is requested
        return self.m_aMax[n]
    # end: getMax


    # Thsi function returns the vector of the maximal value of
    #   the absolute value of each state
    def getAbsMaxSeries(self):
        return copy.deepcopy(self.m_aMax)
    # end def: getAbsMaxSeries


    # This function returns the minum number in state n.
    #  If n is omitted or None, the maximum of all states is returned.
    def getAbsMin(self, n = None):
        if(n is None):
            return self.m_totAbsMin

        return self.m_aMin[n]
    # end: getMin


    # Thsi function returns the vector of the minimal value of
    #   the absolute value of each state
    def getAbsMinSeries(self):
        return copy.deepcopy(self.m_aMin)
    # end def: getAbsMaxSeries


    # This function returns the range of values that is stored in state n.
    #  If n is omitted or None, the whole range is returned.
    def getAbsRange(self, n = None):
        return (self.getAbsMin(n), self.getAbsMax(n))
    # end: get range




    #
    # ===============================
    # Mean
    #   This function gives access to the mean.
    #   The mean is the mean of each recoded state.
    #

    # This function returns the mean of state n.
    #  If n is None the mean of all mean is returned.
    def getMean(self, n = None):

        if(n is None):
            return self.m_meanMean

        return self.m_mean[n]
    # end def: getMean()


    # This function returns the mean of each state collected in a vector
    def getMeanSeries(self):
        return copy.deepcopy(self.m_mean)
    # end def: getMeanSeries


    # This function returns the std of teh recorded means
    def getMeanStd(self):
        return self.m_meanStd
    # end def: getMeanStd


    # This function returns teh range of the means as a tuple.
    #  First the smalles mean is contained as second the
    #  largest mean is contained.
    def getMeanRange(self):
        return (self.m_meanMin, self.m_meanMax)
    # end def: getMeanRange


    # This function returns the mean of the absolute value of the
    #  nth state. Note that this function does not accepts None.
    def getAbsMean(self, n):
        if(n is None):
            raise ValueError("This function does not accepts non as argument.")
        # end if:

        return self.m_aMean[n]
    # end def: getAbsMean


    # Thsi function returns a series of all absolute means
    def getAbsMeanSeries(self):
        return copy.deepcopy(self.m_aMean)
    # end def: getAbsMeanSeries

    # This function returns the range of the values that
    #  are stored in the absolute mean. It is a tuple,
    #  first is the smalles recoreded value and second
    #  the larges
    def getAbsMeanRange(self):
        return (np.amin(self.m_aMean, axis = None), np.amax(self.m_aMean, axis = None))
    # end def: getAbsMeanSeries


    #
    # =================================
    # Std
    #   This functions allows to access the std that where recorded for each setp

    # This function returns the std that was recorded for the ith state
    def getStd(self, i):
        return self.m_std[i]
    # end def: getStd


    # Thsi function returns the std for each recodned state
    def getStdSeries(self):
        return copy.deepcopy(self.m_std)
    # end def: std series


    # This function returns the range of std that was reconded.
    #  Pair with first the smallest, and second the largest
    def getStdRange(self):
        return ( np.amin(self.m_std, axis = None),
                 np.amax(self.m_std, axis = none) )
    # end def: getStdRange



    #
    # =================================
    # Time
    #   This functions allows you to access the simulation times.
    #


    # This function returns the nth time.
    # If no index (n == None) is passed the entire list is returned
    def getTime(self, n):

        if(n is None):
            # Make a full copy of the list
            return copy.deepcopy(self.m_times)
        # end if: entiere list

        if(n < 0 or n >= self.nStates()):
            raise ValueError("Illegal timestep requested, requested time {}, but only {} are known.".format(n, self.nStates()-1))
        # end if

        ret = self.m_times[n]

        return ret
    # end: getTime


    # Returns all times.
    # Is a short hand for getTime(n = None)
    def getTimes(self):
        return self.getTime(n = None)
    # end: getTimes


    #
    # ===============================
    # Accessing Functions
    #   This functions allows to access the stored
    #   states of *self.
    #

    # This function returns the nth state.
    # The state will not be copied, so try not to modify it
    # If n is None, then the entiere list of states is returned.
    def getState(self, n):

        if(n is None):

            # We make a swallow copy of the values.
            # This means that only the lists are copied and the
            # Matrices are still the same, this is not ideal but the best
            return copy.copy(self.m_values)
        # end if: entier list

        if(n < 0 or n >= self.nStates()):
            raise ValueError("Illegal step requested, requested time {}, but only {} are known.".format(n, self.nStates()-1))
        # end if

        #ret = copy.deepcopy(self.m_values[n])
        #return ret

        return self.m_values[n]
    # end: getState


    # Get a liist of all simulation states
    def getStates(self):
        return self.getState(n = None)
    # end: getStates


    # This function returns the nth state of *this.
    #  Note that this is a synonym for self.getState(n).
    #  But None is not allowed.
    def __getitem__(self, n):
        if(n is None):
            raise ValueError("Can not access None, must pass index.")
        # end if:

        # Slicing operations are forwarded to the underlying array
        if(isinstance(n, slice) == True):
            return copy.copy(self.m_values[n])
        # end if: slice object was passed

        return self.getState(n = n)
    # end: __getitem__


    # This function is for implementing the len() function.
    # Since the [] operator returns the stored states, this
    # function will return nState.
    def __len__(self):
        return self.nStates()
    # end: __len__



    #
    # ==========================
    # Database Compatibility Functions
    #   This functions allows compatibility with
    #   The database, they basically fake the main
    #   interface provided by the database.
    #

    # This function allows compatibility with the database.
    #   Note that this function allows that prop is None.
    def getSimState(self, stateIdx, prop, onMarker = False, comp = None):

        if(stateIdx is None):
            raise ValueError("The state index shall not be negative.");
        # end if

        if(self.isSpecial() == False):
            # We allow None, this is needed for interoperability between the series
            #   and the data base.
            if(prop is not None):
                if(prop != self.getProp()):
                    raise ValueError("The property is not the same. This is a {}, but requested was a {}".format(self.getProp().toStr(), prop.toStr()))
                # end if: raise
            # end if: allow none

            if(self.onMarker() != onMarker):
                raise ValueError("Marker property is not the same.")
            # end if:

            if(comp is None):
                if(self.hasComp() == True):
                    raise ValueError("No component was passed, but *this has a component.")
                # end if: throw
            else:
                if(self.getComp() != comp):
                    raise ValueError("The components are not the same, *this is a {}, but passed was a {}".format(self.getComp(), comp))
                # end if: throw
            #end else:
        #end if: isSpecial

        return self.getState(stateIdx)
    # end: getSimState



    # This function is used to create a simulation series from *self.
    #  This function is implemented such that it makes life easier.
    #  Note that this function technically allows to create
    #  views to small subsections of the series.
    def getSimSeries(
                self, prop,
                onMarker = False,
                startTime = 0, endTime = -1, inYr = False,
                withInitialState = False,
                comp = None):

        if(onMarker != self.onMarker()):
            raise ValueError("An error with the markers.")
        # end if: no correct markers

        if(prop is not None):
            if(prop != self.getProp()):
                raise ValueError("Not the same property, *self is a {}, but requested a {}.".format(self.getProp(), prop))
            # end if: property
        # end if: only force the same if prop is given.

        if((comp is None) != (self.hasComp() == False)):
            raise ValueError("Problems with the components.")
        elif(comp is not None):
            if(comp != self.getComp()):
                raise ValueError("Components are not the same. *self is {}, but passed {}.".format(self.getComp(), comp))
            # end if: check
        # end elif:

        if(withInitialState == True):
            raise ValueError("In this case no initial state is supported.")
        #end if

        # We here handle end time.
        if(endTime == -1):
            endTime = self.m_times[-1] + 10;
        # end if: find endtime

        # Get the index that we want to extract
        toLoadIndexes = util.getTimeIndicesRANGE(times = self.m_times, start = startTime, ende = endTime, inYr = inYr)

        # This are the variables that holds the states that we are gone
        # writing int the simulation series.
        recStates = []              # This is the output of the state
        recTime   = []              # This is teh output of the time

        # Going through all the state and load them
        for i in toLoadIndexes:

            # Load the data we need for creating the new series
            time_i = self.m_times[i]
            dataSet_i = self.getSimState(stateIdx = i, prop = prop, onMarker = onMarker, comp = comp)

            # Append the file
            recStates.append(dataSet_i)
            recTime.append(time_i)
        # end for(i):

        # Return now the list of state results and the times
        return SimSeries(Times = recTime, Values = recStates,
                    onMarker = onMarker, Prop = self.m_prop, Comp = comp,
                    XNodal = self.m_xNode, YNodal = self.m_yNode)
    # end: getSimSeries


    #
    # =============================
    # Position Functions
    #   This functions allows to access the positions
    #   of the nodes.
    #


    # This function returns the x coordinate of the supplied nodal grids
    #  This function is needed for compability with the dabatabse
    #  It checks for compability and
    def getXPos(self,
            gType = None,
            prop = None, idx = None, comp = None):

        # if idx is None, set it to zero
        sIdx = 0 if (idx is None) else idx

        # Now we make some basic checkings
        if((prop is not None) and (prop != self.m_prop) ):
            raise ValueError("Property missmatch. self is a {}, but passed {}".format(self.m_prop, prop))
        if(util.isSameComp(comp, self.m_comp) == False):
            raise ValueError("COmponents are different, comp = {}, self.m_comp = {}".format(comp, self.m_comp))
        if((idx < 0) or (idx >= self.nStates())):
            raise ValueError("idx is out of range, it is {}, nStates = {}.".format(idx, self.nStates() ))
        # end checks:

        return copy.deepcopy(self.m_xNode[sIdx])
    # end: getXPos


    # This function returns the y coordinate of the supplied nodal grids
    #  This function is needed for compability with the dabatabse
    def getYPos(self,
            gType = None,
            prop = None, idx = None, comp = None):

        # if idx is None, set it to zero
        sIdx = 0 if (idx is None) else idx

        # Now we make some basic checkings
        if((prop is not None) and (prop != self.m_prop) ):
            raise ValueError("Property missmatch. self is a {}, but passed {}".format(self.m_prop, prop))
        if(util.isSameComp(comp, self.m_comp) == False):
            raise ValueError("COmponents are different, comp = {}, self.m_comp = {}".format(comp, self.m_comp))
        if((idx < 0) or (idx >= self.nStates())):
            raise ValueError("idx is out of range, it is {}, nStates = {}.".format(idx, self.nStates() ))
        # end checks:

        return copy.deepcopy(self.m_yNode[sIdx])
    # end: getYPos


    # This function returns suitable points for ploting.
    #  X comes before Y. Property is important,s icne it must agree.
    #  This function is needed for compatibility with the database.
    #  Note it accepts None as argument.
    #  It is also possible to deactivate the meshing.
    def getPlotPos(self, prop = None, gType = None, comp = None, idx = None, noMeshing = False):

        if(self.isSpecial() == False):
            if(prop is not None):
                if(prop != self.getProp()):
                    raise ValueError("Properties are not the same. This is {}, but requested was {}".format(self.getProp().toStr(), prop.toStr()))
                # end if: same property
            # end if: prop is not None

            if(util.isSameComp(comp, self.m_comp) == False):
                raise ValueError("Passed a non None component, that thisagree with the one of *self. Passed {}, but expected {}".format(comp, self.m_comp))
            #end if: comp was given
        # end if: special

        X = self.getXPos(prop = prop, comp = comp, idx = idx, gType = gType)
        Y = self.getYPos(prop = prop, comp = comp, idx = idx, gType = gType)

        if(noMeshing == True):
            return (X, Y)

        # Apply mesh grid to it
        Xg, Yg = np.meshgrid(X, Y)

        return (Xg, Yg)
    # end: getPlotPos


    #
    # ===========================
    # Magic Methods
    #   Here are some magic methods of *self.
    #   Note that not all of them are here, some are
    #   implemented in sections where they fit more.
    #   Here is like the rest that can not be sorted
    #   to any of the specific sections

    # This function converts this into a string of the property
    def __str__(self):
        return self.m_prop.toStr()
    # end: __str__


    # This function will return an iterator that iterates over the series.
    #  The iterator is a pair, the first element is the time where the recording
    #  Happended and the second element is the state.
    def __iter__(self):
        return zip(self.getTime(n = None), self.getState(n = None))
    # end: __iter__

# end class(SimSeries)


