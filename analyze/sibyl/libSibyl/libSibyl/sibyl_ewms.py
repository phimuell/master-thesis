# This file is part of Sibyl.
#
#  This file contains exactly one function of sibyl.
#  This function combines most of all other marker functions
#  in one function. This allows to process the data just once and ery field has to be read
#  just one single time.

# Normal
import h5py
import numpy as np
import math
import copy
import time

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
from .PropIdx   import PropIdx
from .simSeries import SimSeries
from .dataBase  import DataBase
from libSibyl.gTypeEnum import eGridType
import libSibyl.sibyl_util         as util
import libSibyl.sibyl_traits       as sTraits
import libSibyl.sibyl_extractor    as sExtr
import libSibyl.sibyl_cLibImpl     as cSibyl
import libSibyl.sibyl_fmtUtil      as fmtUtil


# This function combines all marker functionality in on.
#  It will return a pairs, which elements are pairs themself.
#  The order is as follows:
#       ([MarkerOccupation], [AngularMomentum], [CoM], [Pressure], [Force], [Momentum], [FLUIDI])
#  The order within the pairs is described in the respective function.
#  It is possible to disable the creation of individsual parirs.
#  FLUIDI describes the fluid that, it can also be specified by an number.
#  It has the structure of (F_x, F_y, p_x, p_y, angularMom, m_{fluid}, t).
#  As its name indicates, it was obtained by the fluid, by default it is disabled.
#
#   db      The database that is used.
#   mType   The marker type or the index list that should be handled.
#   fType   This is the type of the fluid.
#   occQ    The quantiles for the marker counts
#   occFull Output the full data for the occumpation.
#
def examineMarkers(db, mType,
        mOcc = True,
        doAngMom = True,
        CoM = True,
        Press = True,
        Force = True,
        Momentum = True,

        fType = None,       # Fluid type if given, it will be generated.

        occQ = (0.01, 0.05, 0.25, 0.5, 0.75, 0.95, 0.99), # quantiles for occupation
        occFull = True,

        angVel = False, rotCentre = None,      # Angular velocity and rotation centre

        meanVel = True,     # Compute mean and std of the marker velocity

        useFeelVel = True,  # Which velocity should be used (used everywhere)

        startFrame = 0, endFrame = -1, inYr = False,
        F = None):

    if(not sTraits.isNone(rotCentre) ):
        if(len(rotCentre) != 2):
            raise ValueError("The length of rotCentre is {}, isneat of 2".format(len(rotCentre)))
    if(sTraits.isDataBase(db) == False):
        raise ValueError("Not passed a database, instead passed a {}.".format(type(db)))
    # end if: check

    # get the time where we start
    execStartTP = time.time()

    # Get the time points we need, timeIdx is an array that contains the
    #  selected state indexes
    times, Idx = sExtr.extractTimePoints(src = db,
                    start = startFrame, ende = endFrame, inYr = inYr,
                    alsoIdx = True)

    # If the mType is a number then we interpete it as a marker type
    #  This measn we will extract it by ourself.
    #  We only do this once, wo we are not able to handle rehandlings
    if(sTraits.isInt(mType) ):
        mIdx = db.getIdxOfMarkersWithType(mType = mType)
    else:
        mIdx = mType
    # end if: creating marker indexes

    # generate the index list of fluid markers
    if(not sTraits.isNone(fType) ):
        if(sTraits.isInt(fType) ):
            fIdx = db.getIdxOfMarkersWithType(mType = fType)
        else:
            fIdx = fType
    # end if: find fluid markers


    # Allocating some structures
    nTSteps  = len(Idx )          # This is the number of selected time steps
    nMarkers = len(mIdx)          # This is the number of selected markers


    #
    # This are all the quantities that we have to compute at least in some way

    # Occupation
    mRhoMax     = np.zeros(len(Idx))                # min marker count observed
    mRhoMin     = np.zeros(len(Idx))                # max marker count
    mRhoMean    = np.zeros(len(Idx))                # mean marker count
    mRhoStd     = np.zeros(len(Idx))                # std of marker count
    rhoFullData = list() if occFull else None       # Full data (reduced)
    qRhoMat     = None                              # quantiles

    # Angular momentum
    angMom      = np.zeros(len(Idx))                # Angular momentum
    angularVel  = np.zeros(len(Idx))                # angular velocity
    angVelStd   = np.zeros(len(Idx))                # std of the angular velocity

    # COM
    xCoM        = np.zeros(len(Idx))                # x coordinate of the centre of mass
    yCoM        = np.zeros(len(Idx))                # y coordinate of the centre of mass

    meanX       = np.zeros(len(Idx))                # mean x position
    stdX        = np.zeros(len(Idx))                # std of x position
    medianX     = np.zeros(len(Idx))                # median of the x position
    meanY       = np.zeros(len(Idx))                # mean y position
    stdY        = np.zeros(len(Idx))                # std of y position
    medianY     = np.zeros(len(Idx))                # median of the y position

    # Pressure
    pFx         = np.zeros(len(Idx))                # x componente of pressure force
    pFy         = np.zeros(len(Idx))                # y componente of pressure force
    vol         = np.zeros(len(Idx))                # volume enclosed by marker surface

    # Force
    tFx         = np.zeros(len(Idx))                # x componente of total force
    tFy         = np.zeros(len(Idx))                # y components of total force

    # Momentum  (including velocities)
    momPx       = np.zeros(len(Idx))                # momentum in x direction
    momPy       = np.zeros(len(Idx))                # momentum in y direction
    meanVX      = np.zeros(len(Idx))                # mean marker velocity in x direction
    stdVX       = np.zeros(len(Idx))                # stdanard derivaion of x velocity
    meanVY      = np.zeros(len(Idx))                # mean marker velocity in y direction
    stdVY       = np.zeros(len(Idx))                # stdanard derivaion of y velocity

    # generall used in many return types
    totMass     = np.zeros(len(Idx))                # Mass of the markers
    timeRet     = copy.deepcopy(times)              # Times for returning

    # Fluid
    fludi_tFx     = np.zeros(len(Idx))                # Fluid: total force in x
    fludi_tFy     = np.zeros(len(Idx))                # Fluid: total force in y
    fludi_px      = np.zeros(len(Idx))                # Fluid: momentum in x
    fludi_py      = np.zeros(len(Idx))                # Fluid: momentum in y
    fludi_aMom    = np.zeros(len(Idx))                # Fluid: angular momentum
    fludi_mass    = np.zeros(len(Idx))                # Fluid: mass of the fluid
    fludi_aVel    = np.zeros(len(Idx))                # Fluid: angular velocity
    fludi_aVelStd = np.zeros(len(Idx))                # Fluid: angular velocity

    # End:      Variables
    ###################################


    if(not sTraits.isNone(occQ) ):
        if(sTraits.isFloat(occQ) ):
            raise ValueError("q is not a list like object, but of type {}".format(type(q)))
        # end check:

        nRhoQ = len(occQ)         # Number of quantiles
        qRhoMat = np.zeros((len(Idx), nRhoQ))
    # end if: q is given

    # This is the velocity that is used
    if(useFeelVel ):
        usedVelX = PropIdx.FeelVelX
        usedVelY = PropIdx.FeelVelY
    else:
        usedVelX = PropIdx.VelX
        usedVelY = PropIdx.VelY
    # end: selecting velocity


    # This is the association, that we will use through out this code
    K = cSibyl.cSibyl_defAssociation(db.nMarkers() )

    f = F
    try:
        if(F is None):
            f = db.getDBHandler()

        # Iterate through the states
        for i in range(len(Idx)):
            idx = Idx[i]        # Current index we handle

            # Load the marker propertiey we need
            xf  = db.getSimState(prop = PropIdx.PosX,    onMarker = True, stateIdx = idx)
            yf  = db.getSimState(prop = PropIdx.PosY,    onMarker = True, stateIdx = idx)
            vxf = db.getSimState(prop = usedVelX,        onMarker = True, stateIdx = idx)
            vyf = db.getSimState(prop = usedVelY,        onMarker = True, stateIdx = idx)
            rf  = db.getSimState(prop = PropIdx.Density, onMarker = True, stateIdx = idx)

            # Load the grid positions of the basic nodal grid
            gX, gY = db.getPlotPos(gType = eGridType.RegBasic, idx = idx, prop = None, noMeshing = True, F = f)

            # The marker occupation is almost always needed
            if(mOcc or doAngMom or CoM or Force or Momentum):
                # Update the association
                cSibyl.cSibyl_updateAssociationK(
                        xPosMarker = xf, yPosMarker = yf,
                        K = K,
                        gPosX = gX, gPosY = gY,
                        isTight = True, retK = False)

                # Count the accociation
                nCellCount = cSibyl.cSibyl_countAssocMarkerK(kAssoc = K, nGridY = gY, nGridX = gX)

                # Compute the theoretical extension
                theoExtensionf = cSibyl.cSibyl_assignTheoreticalExtension(
                        K = K,
                        nCellCount = nCellCount,
                        gPosX = gX, gPosY = gY)
            # end if: compute the association and count

            # Now we shrinking the marker arrays, such that only the selected ones are used.
            #  But we will keep the full arraies
            x  =  xf[mIdx]
            y  =  yf[mIdx]
            vx = vxf[mIdx]
            vy = vyf[mIdx]
            r  =  rf[mIdx]

            # Shrinking derived quantities
            theoExtension = theoExtensionf[mIdx]

            # Mass of a single marker
            m  = r * theoExtension

            # Now compute the total mass
            totMass[i] = np.sum(m)


            if(mOcc ):
                # We will now reduce the cell count matrix, since it contains the last row and column.
                nCC = nCellCount[0:-1, 0:-1]

                mRhoMax [i] = np.amax(nCC)
                mRhoMin [i] = np.amin(nCC)
                mRhoMean[i] = np.mean(nCC)
                mRhoStd [i] = np.std(nCC)

                # Quantiles
                if(not sTraits.isNone(occQ) ):
                    qRhoMat[i, :] = np.quantile(a = nCC, q = occQ, interpolation = 'linear')
                # end if: quantile

                # Full data
                if(occFull ):
                    rhoFullData.append(nCC)
                # end append

                nCC = None
            # end if: marker Occupation

            if(doAngMom ):

                # Angular momentum is defined by:
                #   \sum_{i = 1}^{N_{m}} m_{i} * (x * vy - y * vx)
                # Angular velocity of a single particle is
                #   \frac{x * vy - y * vx}{r^2}
                # where r is the distance to the observer. To get the angular velocity
                # the mean is computed.
                # Note that x & y is relative to the centre of rotation.

                # This is the relative distance from the centre of rotation
                cx = x
                cy = y
                if(not sTraits.isNone(rotCentre) ):
                    cx = x - rotCentre[0]
                    cy = y - rotCentre[1]
                # end if:

                # This is the quantity that is needed
                temp = cx * vy - cy * vx

                # Test if we must compute the angular velocity
                if(angVel ):
                    dist = ((cx)**2) + ((cy)**2)
                    aVelInd = np.where(dist == 0.0, 0.0, temp / dist)

                    # Using the nan version that ignores nan
                    angularVel[i] = np.nanmean(aVelInd)
                    angVelStd[i]  = np.nanstd(aVelInd)

                    dist = None
                    aVelInd = None
                # end if: angular vel

                # Conmpute the angular momentum
                angMom[i] = np.sum(temp * m)

                temp = None
            # end if: angular momentum


            if(CoM ):
                totMarkerMass = totMass[i]

                # Calculating the CoM
                xCoM[i] = np.sum(x * m) / totMarkerMass
                yCoM[i] = np.sum(y * m) / totMarkerMass

                # Calculate the mean and std of the position,
                #  This is an alternative to the classical CoM, since it ignores the fluctuation in mass
                meanX[i] = np.mean(x)
                stdX [i] = np.std (x)
                meanY[i] = np.mean(y)
                stdY [i] = np.std (y)

                medianX[i] = np.median(x)
                medianY[i] = np.median(y)
            # end: do CoM

            if(Momentum or Force):

                # Calculate the momentum
                momPx[i] = np.sum(m * vx)
                momPy[i] = np.sum(m * vy)

                if(Momentum ):
                    # Only needed in case of full momentum

                    if(meanVel):
                        meanVX[i] = np.mean(vx)
                        stdVX [i] = np.std (vx)
                        meanVY[i] = np.mean(vy)
                        stdVY [i] = np.std (vy)
                    # end if: optional marker
                # end if: true momentum calculatioons
            # end momentum


            if(Press ):
                P = db.getSimState(prop=PropIdx.Pressure, stateIdx = idx               )
                xP, yP = db.getPlotPos(prop=PropIdx.Pressure, idx = idx, noMeshing=True)

                # Call the function to optain the value
                fPressI, volI = cSibyl.cSibyl_computePressForce(xPos=x, yPos=y, P=P, xP=xP, yP=yP)

                # Unpack the values
                pFx[i] = fPressI[0]
                pFy[i] = fPressI[1]
                vol[i] = volI

                fPressI = None
                volI    = None
                xP      = None
                yP      = None
                P       = None
            # End if: pressure needed

            # FLUID
            if(not sTraits.isNone(fType) ):

                # Reducing the markers
                xFluid  = xf [fIdx]
                yFluid  = yf [fIdx]
                vxFluid = vxf[fIdx]
                vyFluid = vyf[fIdx]
                rFluid  = rf [fIdx]
                teFluid = theoExtensionf[fIdx]

                # Computing the mass once
                mFluid = teFluid * rFluid

                # Compute the total mass
                fludi_mass[i] = np.sum(mFluid)

                # Computing the impuls
                fludi_px[i] = np.sum(mFluid * vxFluid)
                fludi_py[i] = np.sum(mFluid * vyFluid)

                # Compute the angular momentum
                if(sTraits.isNone(rotCentre) ):
                    fludi_aMom[i] = np.sum(mFluid * (xFluid * vyFluid - yFluid * vxFluid))
                else:
                    fludi_aMom[i] = np.sum(mFluid * ((xFluid - rotCentre[0]) * vyFluid - (yFluid - rotCentre[1]) * vxFluid))
                # end: ang momentum

                # Compute the angular velocity
                if(sTraits.isNone(rotCentre) ):
                    RX = xFluid
                    RY = yFluid
                else:
                    RX = xFluid - rotCentre[0]
                    RY = yFluid - rotCentre[1]

                # Auxilery computation
                tmp   = RX * vyFluid - RY * vxFluid
                dist2 = (RX**2) + (RY**2)

                # Compute it, avoid division by zero
                aVelFluidInd = np.where(dist2 == 0.0, 0.0, tmp / dist2)

                # Compute the mean and std
                fludi_aVel[i]    = np.nanmean(aVelFluidInd)
                fludi_aVelStd[i] = np.nanstd(aVelFluidInd)
            # end if: fluid
        # end for(i):
    finally:
        if(F is None):
            db.closeDBHandler(f)
    # end


    # Now we compute the force, for this only the derivation is needed
    if(Force ):
        tFx = cSibyl.cSibyl_firstDerivative(y = momPx, x = times,
                useCentral = True)
        tFy = cSibyl.cSibyl_firstDerivative(y = momPy, x = times,
                useCentral = True)
    # End: force

    # If fluid was given, we must nbow compute teh force
    if(not sTraits.isNone(fType) ):
        fludi_tFx = cSibyl.cSibyl_firstDerivative(y = fludi_px, x = times,
                useCentral = True)
        fludi_tFy = cSibyl.cSibyl_firstDerivative(y = fludi_py, x = times,
                useCentral = True)
    # end if: fluid force

    # This is the return tuple it is empty
    RET = tuple()

    if(mOcc ):
        T = (mRhoMin, mRhoMax, timeRet, mRhoMean, mRhoStd)

        if(not sTraits.isNone(occQ) ):
            T = (*T, qRhoMat)
        if(occFull ):
            T = (*T, rhoFullData)
        # End optional components of occupation pair

        RET = (*RET, T)
        T = None
    # marker Occupation

    if(doAngMom ):
        T = (angMom, totMass, timeRet)

        if(angVel ):
            T = (*T, (angularVel, angVelStd))
        # end optional

        RET = (*RET, T)
        T = None
    # end: angular momentum

    if(CoM ):
        T = (xCoM, yCoM, totMass, timeRet, (meanX, stdX, medianX), (meanY, stdY, medianY))

        RET = (*RET, T)
        T = None
    # End: COM

    if(Press ):
        T = (pFx, pFy, vol, timeRet)

        RET = (*RET, T)
        T = None
    # end Press:

    if(Force ):
        T = (tFx, tFy, totMass, timeRet)

        RET = (*RET, T)
        T = None
    # end: Force

    if(Momentum ):
        T = (momPx, momPy, totMass, timeRet)

        if(meanVel ):
            T = (*T, (meanVX, stdVX), (meanVY, stdVY))
        # end: optional

        RET = (*RET, T)
        T = None
    # end: momentum

    if(not sTraits.isNone(fType) ):
        T = (fludi_tFx, fludi_tFy, fludi_px, fludi_py, fludi_aMom, fludi_mass, timeRet, (fludi_aVel, fludi_aVelStd) )

        RET = (*RET, T)
        T = None
    # end: fluid


    # get the time when we finished
    execStopTP = time.time()
    elapsedTime = execStopTP - execStartTP

    # Prittyprint
    timeDurationNice = fmtUtil.secToYear(elapsedTime)
    print("Needed {}".format(timeDurationNice))

    return RET
# end def: examine markers


