# This file is part of Sibyl.
#
# It contains some helper functions that eases the use
# of markers in Sibyl.

# Normal
import h5py
import numpy as np
import math
import copy

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
from .PropIdx   import PropIdx
from .simSeries import SimSeries
from .dataBase  import DataBase
from libSibyl.gTypeEnum import eGridType
import libSibyl.sibyl_util         as util
import libSibyl.sibyl_traits       as sTraits
import libSibyl.sibyl_extractor    as sExtr
import libSibyl.sibyl_cLibImpl     as cSibyl



# GENERAL NOTE:
#   For some quantities, it is necessary to know the mass of a marker.
#   This mass is calculated by estimating the extension of a marker.
#   This assumtion is calculated by assuming that all marker, inside
#   a cell, "govern" the same domain. Thus the volume of a marker is
#   The volume of the cell it is located, divided by the number of
#   markers in that cell.
#   This behaviour can be disabled by setting the "specific" argument
#   to True. In this case the volume of a marker is estimated by
#   dividing the total volume by the number of markers. This diregards
#   Individual displacements. It is assumed that the domain size
#   never changes.
#



# This function extracts markers form the series. The indices of
#   the markers are given as an argument. The markers are returned
#   in a $nStates x nDiffMarkers$ matrix, meaning that each column
#   contains one marker and the rows the timesteps.
#
#  sSeries       The simulation series that should be used.
#  whichMarkers  The index that should be extracted.
#
#  propIdx       If sSeries is a database, a property must be supplied.
#  comp          Which component is used
def extractMarkers(
        sSeries,
        whichMarkers,

        propIdx = None,
        comp = None,
        F = None
    ):

    comp_ = comp    # This is an alias that will be used

    # chck the input
    if(sTraits.isDataBase(src) ):

        if propIdx is None:
            raise ValueError("Passed a database to the function but no property index.")
        # end if:
    # end if: is Database

    elif(sTraits.isSimSeries(src) ):
        if(sSerie.hasComp() == True):

            if comp is None:
                comp_ = sSeries.getComp()
            else:
                if(comp != sSeries.getComp()):
                    raise ValueError("A component was passed, but it is not the same as the one stored in the series. Passed {}, but expected {}.".format(comp, sSeries.getComp()))
                #end if
            #end if
        # end if: has comp
    # end elif: isSimSeries

    else:
        raise ValueError("Does not know how to handle {}".format(type(src)))
    # end else: not handle



    if(sSerie.onMarker() == False):
        raise ValueError("Not on markers.");
    # end if:

    # Load some variables
    nStates = sSeries.nStates()
    nDiffMarkers = len(whichMarkers)

    # Thsi is the output value
    resVal = np.zeros((nStates, nDiffMarkers))

    F = f
    try:
        if(F is None):
            f = db.getDBHandler()


        # Go through the series and load the markers
        for i in range(nStates):

            # Load the instance
            iData = sSerie.getSimState(
                    stateIdx = i, prop = propIdx,
                    onMarker = True, comp = comp_, F = f)

            for k in range(nDiffMarkers):
                resVal[i, k] = iData[whichMarkers[k] ]
            # end for(k): extract markers
        # end for(i):

    finally:
        if(F is None):
            db.closeDBHandler(f)
    # end:

    return retVal
# end def: wxtractMarkers


# This function is able to calculate the centre of mass of of the
#  markers. It is possible to only process a certain type of marker.
#  The density is loaded by testing if the markers have a stored density
#  in the first step, if not present, the initial densityies are loaded.
#  The function returns a tuple, first is x position and second is y position.
#  The optional third argument is the time tpoint if selected.
#  The full output tuple is (xCoM, yCoM, [totMass,] [times,])
#
#   db          The database that is used.
#   types       The marker type that shopuld be loaded.
#   noTimes     Flag to select the returning of the time points.
#   startFrame  Start frame
#   endFrame    End frame that should be extracted.
#
def calculateCoM(db, mType,
        noTimes = False,
        withTotMass = False,
        startFrame = 0, endFrame = -1, inYr = False,
        specific = False,
        F = None):

    if(sTraits.isDataBase(db) == False):
        raise ValueError("Not passed a database, instead passed a {}.".format(type(db)))
    # end if: check

    # Get the time points we need
    times, Idx = sExtr.extractTimePoints(src = db,
                    start = startFrame, ende = endFrame, inYr = inYr,
                    alsoIdx = True)

    # Allocate the return values
    xCoM    = np.zeros(len(Idx))
    yCoM    = np.zeros(len(Idx))
    totMass = None
    if(withTotMass ):
        totMass = np.zeros(len(Idx))
    #

    # This is the helper vector for the theoretical extension
    K = cSibyl.cSibyl_defAssociation(db.nMarkers() )

    f = F
    try:
        if(F is None):
            f = db.getDBHandler()

        # Test if the specific flag is given, so we can calculate the extension
        #  once. Note that in the following we assume that the domain size never
        #  changes.
        if(specific ):
            gPosX, gPosY = db.getPlotPos(prop = None, idx = 0, gType = eGridType.RegBasic, F = f, noMeshing = True)
            lengthX = gPosX[-1] - gPosX[0]
            lengthY = gPosY[-1] - gPosY[0]
            domVol  = lengthX * lengthY
            lastNMarkers = -1
        # end if: specific

        # Iterate through the states
        for i in range(len(Idx)):
            idx = Idx[i]        # Current index we handle

            # Load the index of the markers that we need to extract.
            mIdx = db.getIdxOfMarkersWithType(stateIdx = idx, mType = mType, allowEmpty = False, F = f)

            # Load the marker propertiey we need
            x = db.getSimState(prop = PropIdx.PosX,    onMarker = True, stateIdx = idx, F = f)
            y = db.getSimState(prop = PropIdx.PosY,    onMarker = True, stateIdx = idx, F = f)
            r = db.getSimState(prop = PropIdx.Density, onMarker = True, stateIdx = idx, F = f)

            if(specific ):
                if(lastNMarkers != len(x)):
                    lastNMarkers = len(x)                                        # Update the number of the markers we have recorded
                    theoExtension = (domVol / lastNMarkers) * np.ones(len(mIdx)) # Calculating the averige space a marker occupies
                # end if: only update if needed
            else:
                # Load the grid positions of the basic nodal grid
                gX, gY = db.getPlotPos(gType = eGridType.RegBasic, idx = idx, prop = None, noMeshing = True, F = f)

                # Get the theoretical extension, needs all markers for this
                theoExtension = cSibyl.cSibyl_updateTheoreticalExtension(
                        xPosMarker = x, yPosMarker = y, K = K,
                        gPosX = gX, gPosY = gY)
                theoExtension = theoExtension[mIdx]
            # end if: handle specific case

            # Filter the marker propertyies, such the only teh relevants are here
            x = x[mIdx]
            y = y[mIdx]
            r = r[mIdx]

            mMasses       = r * theoExtension        # This is the mass of the selected marker
            totMarkerMass = np.sum(mMasses)          # This is the mass of all markers

            # Calculating the CoM
            xCoM[i] = np.sum(x * mMasses ) / totMarkerMass
            yCoM[i] = np.sum(y * mMasses ) / totMarkerMass
        # end for(i):

    finally:
        if(F is None):
            db.closeDBHandler(f)
    # end

    RET = (xCoM, yCoM)

    if(withTotMass ):
        RET = (*RET, totMarkerMass)
    if(noTimes == False):
        RET = (*RET, times)

    return RET
# end: calculateCoM


# This function calculates the momentum of the markers. It has two modes
#  of operations, that are selected by the total argument. This argument
#  determines how te return type is comprised of. If total is set to True,
#  a tuple consisting of arrays will be returned. One element per timestep.
#  Each element consist the total momentum, sum of all indivitual momentum
#  of the markers. First X and then Y is returned.
#  The second mode, total set to False, returns a marker centristic result.
#  In this case the first and the second member of the tuple, same ordering,
#  are matrices. The number of columns is given by the number of selected time
#  steps and the number of rows is given by the number of markers.
#  Each row contains the force a marker experiences and the column is indicating
#  the time step. If requested the matrix will be converted into a dict, where
#  the key is the marker index, this can be enabled by setting noMatrix to True.
#  If the argument noTimes is set to False, which is the default, the function
#  will return a tuple of length three, the third argument is the value of time
#  during the time step.
#  The full format of the output is (Px, Py, [mMass], times)
#
#  Note that for the velocity the feel velocity is used, but it can be requested
#  to use the intrinsic marker velocity. Note that this my not be possible because
#  the data is not aviable.
#
#   db          The database that is used.
#   mIdx        The list of markers that should be tracked, assumed to be sorted
#                and unique.
#   total       Not per marker but sum
#   noTimes     Flag to select the returning of the time points.
#
#   withMarkerMass With this flag you can request the dumping of the total mass.
#                depending on the output format will differ.
#
#   startFrame  Start frame
#   endFrame    End frame that should be extracted.
#   inYr        If times are in years.
#   useFeelVel  If true, the default, the feel velocity is used.
#
def calculateMarkerMomentum(db, mIdx,
        total = True,
        noTimes = False,
        noMatrix = False, withMarkerMass = False,
        startFrame = 0, endFrame = -1, inYr = False,
        specific = False, useFeelVel = True,
        F = None):

    if(sTraits.isDataBase(db) == False):
        raise ValueError("Not passed a database, instead passed a {}.".format(type(db)))
    # end if: check

    # Get the time points we need, timeIdx is an array that contains the
    #  selected state indexes
    times, timeIdx = sExtr.extractTimePoints(src = db,
                    start = startFrame, ende = endFrame, inYr = inYr,
                    alsoIdx = True)

    # If the mType is a number then we interpete it as a marker type
    #  This measn we will extract it by ourself.
    #  We only do this once, wo we are not able to handle rehandlings
    if(sTraits.isInt(mIdx) ):
        mIdx = db.getIdxOfMarkersWithType(mType = mIdx)
    # end if: creating marker indexes

    # Allocating some structures
    nTSteps  = len(timeIdx)          # This is the number of selected time steps
    nMarkers = len(mIdx   )          # This is the number of selected markers

    # This is the helper vector for the association
    K = cSibyl.cSibyl_defAssociation(db.nMarkers())

    # This is the return type, depending
    momtum = None
    if(total):
        momtum = ( np.zeros(nTSteps), np.zeros(nTSteps) )
    else:
        momtum = ( dict()           , dict()            )
        for i in mIdx:
            momtum[0][i] = np.zeros((nMarkers, nTSteps))
            momtum[1][i] = np.zeros((nMarkers, nTSteps))
        # end for(i):
    # end: generating return type

    Mass = None
    if(withMarkerMass ):
        if(total ):
            Mass = np.zeros(nTSteps)
        else:
            Mass = np.zeros((nMarkers, nTSteps))
    # end if:

    # We will now detrmine which velocity we are using
    usedVelX = PropIdx.FeelVelX if useFeelVel else PropIdx.VelX
    usedVelY = PropIdx.FeelVelY if useFeelVel else PropIdx.VelY


    f = F
    try:
        if(F is None):
            f = db.getDBHandler()

        # Test if the specific flag is given, so we can calculate the extension
        #  once. Note that in the following we assume that the domain size never
        #  changes.
        if(specific ):
            gPosX, gPosY = db.getPlotPos(prop = None, idx = 0, gType = eGridType.RegBasic, F = f, noMeshing = True)
            lengthX = gPosX[-1] - gPosX[0]
            lengthY = gPosY[-1] - gPosY[0]
            domVol  = lengthX * lengthY
            lastNMarkers = -1;      # Stores the number of markers that are recorded
        # end if: specific

        # Iterate through the states
        for i in range(len(timeIdx)):
            currStateIdx = timeIdx[i]        # Current state index we handle

            # Load the grid positions of the basic nodal grid
            gX, gY = db.getPlotPos(gType = eGridType.RegBasic, idx = currStateIdx, prop = None, noMeshing = True, F = f)

            # Load the marker propertiey we need
            x  = db.getSimState(prop = PropIdx.PosX,    onMarker = True, stateIdx = currStateIdx)
            y  = db.getSimState(prop = PropIdx.PosY,    onMarker = True, stateIdx = currStateIdx)
            vx = db.getSimState(prop = usedVelX,        onMarker = True, stateIdx = currStateIdx)
            vy = db.getSimState(prop = usedVelY,        onMarker = True, stateIdx = currStateIdx)
            r  = db.getSimState(prop = PropIdx.Density, onMarker = True, stateIdx = currStateIdx)

            # Calculating the extension only if needed.
            if(not specific):
                # Get the theoretical extension, needs all markers for this
                theoExtension = cSibyl.cSibyl_updateTheoreticalExtension(
                        xPosMarker = x, yPosMarker = y, K = K,
                        gPosX = gX, gPosY = gY)
                theoExtension = theoExtension[mIdx]     # Remove all non selected markers
            else:
                if(lastNMarkers != len(x)):
                    lastNMarkers = len(x)                                        # Total numbers markers
                    theoExtension = (domVol / lastNMarkers) * np.ones(len(mIdx)) # Calculating the averige space a marker occupies
                # end if: maybe update theoretical extension
            # end: handling of momentum

            # Filter the marker propertyies, such the only teh relevants are here
            x  =  x[mIdx]
            y  =  y[mIdx]
            vx = vx[mIdx]
            vy = vy[mIdx]
            r  =  r[mIdx]

            # This is the mass, depending on how its extension is considered.
            mMasses = r * theoExtension

            # Calculate the impulses
            xImp = mMasses * vx
            yImp = mMasses * vy

            # Determine how to store it
            if(total):
                momtum[0][i] = np.sum(xImp)
                momtum[1][i] = np.sum(yImp)
            else:
                momtum[0][:, i] = xImp
                momtum[1][:, i] = yImp
            # end else: handle non total format

            if(withMarkerMass ):
                if(total ):
                    Mass[i] = np.sum(mMasses)
                else:
                    Mass[:, i] = mMasses
            # end if: tot mass
        # end for(i):

    finally:
        if(F is None):
            db.closeDBHandler(f)
    # end:

    if(noMatrix == True):
        Px = dict()
        Py = dict()
        for i in range(len(mIdx)):
            idx = mIdx[i]

            if(idx in Px):
                raise ValueError("It apears that the index {} was passed twice.".format(idx))
            # end if: check

            # Unpack the momentum from the matrix
            Px[idx] = momtum[0][i, :]
            Py[idx] = momtum[1][i, :]
        # end for(i)
        momtum = (Px, Py)       # Exchange the format

        if(withMarkerMass ):
            q = dict()
            for i in range(len(mIdx)):
                idx = mIdx[i]

                q[idx] = Mass[i, :]
            # end for(i)

            Mass = q    # exchange the masses
        # end if: handle marker masses
    # end if:

    # extend output if needed
    if(withMarkerMass ):
        momtum = (*momtum, Mass)
    if(noTimes == False):
        momtum = (*momtum, times)

    return momtum
# end: calculateMomentum



# This function calculates the angular momentum of a set of markers.
#  For that it uses:
#       \sum_{i} \left( m_{i} \Vec{x}_{i} \cross \Vec{v}_{i} \right)
#       = \sum_{i} m_{i} * \left(x_{i} * v_{i}^{(y)} - y_{i} * v_{i}^{(x)} \right)
#
#  This function only supports the total format and not an individual format as the
#  momentum function does. For that it uses the feel velocity by default. The intrinsic
#  marker velocity can also be used. It assumes that it ritates arround the centre,
#  but it can be specified by the rotCentre keyword.
#
#   db          The database that is used.
#   mIdx        The list of markers that should be tracked, assumed to be sorted
#                and unique.
#   total       Not per marker but sum
#   noTimes     Flag to select the returning of the time points.
#
#   withMarkerMass With this flag you can request the dumping of the total mass.
#                depending on the output format will differ.
#
#   startFrame  Start frame
#   endFrame    End frame that should be extracted.
#   inYr        If times are in years.
#
def calculateMarkerAngularMomentum(db, mIdx,
        total = True,
        noTimes = False,
        rotCentre = None,
        noMatrix = False, withMarkerMass = False,
        startFrame = 0, endFrame = -1, inYr = False,
        specific = False, useFeelVel = True,
        F = None):

    if(sTraits.isDataBase(db) == False):
        raise ValueError("Not passed a database, instead passed a {}.".format(type(db)))
    if(total == False):
        raise ValueError("Only support total.")
    if(not sTraits.isNone(rotCentre) ):
        if(len(rotCentre) != 2):
            raise ValueError("The length of rotCentre is {}, isneat of 2".format(len(rotCentre)))
    # end if: check



    # Get the time points we need, timeIdx is an array that contains the
    #  selected state indexes
    times, timeIdx = sExtr.extractTimePoints(src = db,
                    start = startFrame, ende = endFrame, inYr = inYr,
                    alsoIdx = True)

    nTSteps  = len(timeIdx)          # This is the number of selected time steps

    # If mInt is an inte, then we assume that it is a marker type and we will load the value
    if(sTraits.isInt(mIdx) ):
        mIdx = db.getIdxOfMarkersWithType(mType = mIdx)
    # end if: construct the index list on our own

    # This is the helper vector for the association
    K = cSibyl.cSibyl_defAssociation(db.nMarkers())

    # This variable will contain the angular momentum.
    #  Is just a scalar
    momtum = np.zeros(nTSteps)

    Mass = None
    if(withMarkerMass ):
        Mass = np.zeros(nTSteps)
    # end if:

    # We will now detrmine which velocity we are using
    usedVelX = PropIdx.FeelVelX if useFeelVel else PropIdx.VelX
    usedVelY = PropIdx.FeelVelY if useFeelVel else PropIdx.VelY


    f = F
    try:
        if(F is None):
            f = db.getDBHandler()

        # Test if the specific flag is given, so we can calculate the extension
        #  once. Note that in the following we assume that the domain size never
        #  changes.
        if(specific ):
            gPosX, gPosY = db.getPlotPos(prop = None, idx = 0, gType = eGridType.RegBasic, F = f, noMeshing = True)
            lengthX = gPosX[-1] - gPosX[0]
            lengthY = gPosY[-1] - gPosY[0]
            domVol  = lengthX * lengthY
            lastNMarkers    = -1                # Total markers that are here
        # end if: specific

        # Iterate through the states
        for i in range(len(timeIdx)):
            currStateIdx = timeIdx[i]        # Current state index we handle

            # Load the grid positions of the basic nodal grid
            gX, gY = db.getPlotPos(gType = eGridType.RegBasic, idx = currStateIdx, prop = None, noMeshing = True, F = f)

            # Load the marker propertiey we need
            x  = db.getSimState(prop = PropIdx.PosX,    onMarker = True, stateIdx = currStateIdx)
            y  = db.getSimState(prop = PropIdx.PosY,    onMarker = True, stateIdx = currStateIdx)
            vx = db.getSimState(prop = usedVelX,        onMarker = True, stateIdx = currStateIdx)
            vy = db.getSimState(prop = usedVelY,        onMarker = True, stateIdx = currStateIdx)
            r  = db.getSimState(prop = PropIdx.Density, onMarker = True, stateIdx = currStateIdx)

            # Calculating the extension only if needed.
            if(not specific):
                # Get the theoretical extension, needs all markers for this
                theoExtension = cSibyl.cSibyl_updateTheoreticalExtension(
                        xPosMarker = x, yPosMarker = y, K = K,
                        gPosX = gX, gPosY = gY)
                theoExtension = theoExtension[mIdx]     # Remove all non selected markers
            else:
                if(lastNMarkers != len(x)):
                    lastNMarkers = len(x)                                        # Total numbers markers
                    theoExtension = (domVol / lastNMarkers) * np.ones(len(mIdx)) # Calculating the averige space a marker occupies
                # end if: maybe update theoretical extension
            # end: handling of momentum

            # Filter the marker propertyies, such the only teh relevants are here
            x  =  x[mIdx]
            y  =  y[mIdx]
            vx = vx[mIdx]
            vy = vy[mIdx]
            r  =  r[mIdx]

            # Compute the mass of each selected markers
            mMasses = r * theoExtension     # This is the real mass


            # Calculate the impulses, the component is relative to the velocity
            xComp = None
            yComp = None
            if(sTraits.isNone(rotCentre) ):
                xComp = mMasses * vx * y
                yComp = mMasses * vy * x
            else:
                xComp = mMasses * vx * (y - rotCentre[1])
                yComp = mMasses * vy * (x - rotCentre[0])
            # end: calculating or comp


            # This is the angular momentum, the minus comes from the fact that we have the
            #  third component of a cross product
            aMomentum = yComp - xComp

            momtum[i] = np.sum(aMomentum)

            if(withMarkerMass ):
                Mass[i] = np.sum(mMasses)
        # end for(i):

    finally:
        if(F is None):
            db.closeDBHandler(f)
    # end:

    # extend output if needed
    if(withMarkerMass ):
        momtum = util.catTuple(momtum, Mass)
    if(noTimes == False):
        momtum = util.catTuple(momtum, times)

    return momtum
# end: calculateAngularMomentum



# This function is able to calculate the resulting force that acts on a set
#  of markers. This function uses the momentum function to compute the force.
#  The force is optained by calculating the derivative of the momentum.
#  Note that this my not be what you want, also consider the pressure force
#  function that calculates the force resulting from pressure.
#  The return tuple consists of the following:
#       (Fx, Fy, [Px, Py,] [markerMass], [times])
#
#   db          The database that is used.
#   mIdx        The list of markers that should be tracked, assumed to be sorted
#                and unique.
#   total       Not per marker but sum
#   noTimes     Flag to select the returning of the time points.
#   withMarkerMass Also calculate the total marker mass.
#
#   startFrame  Start frame
#   endFrame    End frame that should be extracted.
#   inYr        If times are in years.
#
def calculateMarkerForce(db, mIdx,
        total = True,
        noTimes = False,
        noMatrix = False,
        withMomentum = False, withMarkerMass = False,
        useForward = False, useBackward = False, useCentral = True,
        order = None, scheme = None,
        startFrame = 0, endFrame = -1, inYr = False,
        specific = False, useFeelVel = True,
        F = None):

    if(sTraits.isDataBase(db) == False):
        raise ValueError("Not passed a database, instead passed a {}.".format(type(db)))
    # end if: check

    # Calculate the momentum with times
    PP = calculateMarkerMomentum(db = db, mIdx = mIdx,
            total = total, noMatrix = noMatrix,
            withMarkerMass = withMarkerMass,
            noTimes = False,    # Explicitly request times for derivative
            startFrame = startFrame, endFrame = endFrame, inYr = inYr,
            specific = specific, useFeelVel = useFeelVel,
            F = F)

    # Unpack PP
    px = PP[ 0]
    py = PP[ 1]
    t  = PP[-1]
    mM = None
    if(withMarkerMass ):
        mM = PP[2]
    # end if

    # If the mType is a number then we interpete it as a marker type
    #  This measn we will extract it by ourself.
    #  We only do this once, wo we are not able to handle rehandlings
    if(sTraits.isInt(mIdx) ):
        mIdx = db.getIdxOfMarkersWithType(mType = mIdx)
    # end if: creating marker indexes

    # Calculate the derivative depending on the requested format
    Fx = None
    Fy = None
    if(total):
        Fx = cSibyl.cSibyl_firstDerivative(y = px, x = t,
                useForward = useForward, useBackward = useBackward, useCentral = useCentral,
                scheme = scheme, order = order)
        Fy = cSibyl.cSibyl_firstDerivative(y = py, x = t,
                useForward = useForward, useBackward = useBackward, useCentral = useCentral,
                scheme = scheme, order = order)
    else:
        if(noMatrix):
            Fx = dict()
            Fy = dict()
            for idx in mIdx:
                if(idx not in px):
                    raise ValueError("Marker index {} is not in the returned x momentum dict.".format(idx))
                if(idx not in py):
                    raise ValueError("Marker index {} is not in the returned y momentum dict.".format(idx))
                # end check:

                px_m = px[idx]      # Load the momentum value
                py_m = py[idx]

                # Calculate force and store them directly
                Fx[idx] = cSibyl.cSibyl_firstDerivative(y = px_m, x = t,
                            useForward = useForward, useBackward = useBackward, useCentral = useCentral,
                            scheme = scheme, order = order)
                Fy[idx] = cSibyl.cSibyl_firstDerivative(y = py_m, x = t,
                            useForward = useForward, useBackward = useBackward, useCentral = useCentral,
                            scheme = scheme, order = order)
            # end for(idx):

        else:
            Fx = np.zeros_like(px)
            Fy = np.zeros_like(py)

            nMarkers = len(mIdx)

            for i in range(nMarkers):
                px_m = px[i, :]
                py_m = py[i, :]

                Fx[i] = cSibyl.cSibyl_firstDerivative(y = px_m, x = t,
                            useForward = useForward, useBackward = useBackward, useCentral = useCentral,
                            scheme = scheme, order = order)
                Fy[i] = cSibyl.cSibyl_firstDerivative(y = py_m, x = t,
                            useForward = useForward, useBackward = useBackward, useCentral = useCentral,
                            scheme = scheme, order = order)
            # end for(i):
        # end if: handle non total case, depending on format
    # end else: non total case

    # Compose return value
    RET = (Fx, Fy)
    if(withMomentum ):
        RET = (*RET, px, py)
    if(withMarkerMass ):
        RET = (*RET, mM)
    if(noTimes == False):
        RET = (*RET, t)

    return RET
# end: calculateForce


# This function is able to calculate the force that is exerted by pressure
#  on the object. For that a convex hull is claculated and the pressure is
#  integrated over it. This means it is a resulting force and not an acting
#  force as it is computed by the other force function.
#  The return tuple consists of the following:
#       (Fx, Fy, [markerVol], [times])
#
#   db          The database that is used.
#   mIdx        The list of markers that should be tracked, assumed to be sorted
#                and unique.
#   noTimes     Flag to select the returning of the time points.
#   noVol       Flag to disable the computation of volume.
#   startFrame  Start frame
#   endFrame    End frame that should be extracted.
#   inYr        If times are in years.
#
def calculatePressureForce(db, mIdx,
        noTimes = False, noVol = True,
        startFrame = 0,
        endFrame = -1,
        inYr = False,
        F = None):

    if(not sTraits.isDataBase(db) ):
        raise ValueError("Passed db is not a database, instead it is {}".format(type(db)))

    # Get the time points we need, timeIdx is an array that contains the
    #  selected state indexes
    times, timeIdx = sExtr.extractTimePoints(src = db,
                    start = startFrame, ende = endFrame, inYr = inYr,
                    alsoIdx = True)

    nTSteps  = len(timeIdx)          # This is the number of selected time steps

    # This are the result variables
    pForces_x = np.zeros(nTSteps)
    pForces_y = np.zeros(nTSteps)
    vols      = np.zeros(nTSteps)

    # Iterate through the indexes to compute them
    for i in range(nTSteps):
        sIdx = timeIdx[i]       # Get the state index

        # This is the variable that is used identify the markers that we need
        mIdx_ = None
        if(sTraits.isInt(mIdx) ):
            mIdx_ = db.getIdxOfMarkersWithType(mType = mIdx)
        elif(sTraits.isIndexSeries(mIdx) ):
            mIdx_ = mIdx
        else:
            raise ValueError("Unkown type for indexing encountered: {}".format(type(mIdx)))
        # end:

        # Load everything
        x = db.getSimState(prop=PropIdx.PosX    , stateIdx = sIdx, onMarker=True)
        y = db.getSimState(prop=PropIdx.PosY    , stateIdx = sIdx, onMarker=True)
        P = db.getSimState(prop=PropIdx.Pressure, stateIdx = sIdx               )
        xP, yP = db.getPlotPos(prop=PropIdx.Pressure, idx = sIdx, noMeshing=True)

        # Call the function to optain the value
        fPress, vol = cSibyl.cSibyl_computePressForce(xPos=x, yPos=y, P=P, xP=xP, yP=yP, selIdx=mIdx_)

        # Store the computed values in the resuls
        pForces_x[i] = fPress[0]
        pForces_y[i] = fPress[1]
        vols     [i] = vol
    # end for(i):

    # Compose the return valriable
    RET = (pForces_x, pForces_y)
    if(noVol == False):
        RET = (*RET, vols)
    if(noTimes == False):
        RET = (*RET, times)

    return RET
# end def: pressure force


# This function computes the total enery, that is defined
#  by the set of markers. The total enery is defined as
#       E_{i}^{n} := \frac{\rho_{i} * A_i}{2} \norm{\vec{v}^{(n)}_{i}}^{2}
#                   + \rho_i * A_i \vec{g} \cdot \vec{x_{i}}^{(n)}
#  And A_i is the theoretical extension of a marker.
#  Potential energy is assumed to be a conservative field. The origin (0, 0),
#  is defined to set potential energy to zero, this can be changed, by
#  specifing Epot0.
#  It is also important that a positive value of gY means a force pointing downwards.
#  It is also important that the energy is defined for a single marker.
#  By default the kinetic energy is calculated by using the feel velocity, but this
#  could be changed.
#
#   db          The database that is used.
#
#   mIdx        The list of markers that should be tracked.
#   gY          The gravity in y direction.
#   gX          Gravity in x direction, set to zero, by default.
#
#   Epot0       Initial potential enery, scalr or array.
#   total       Not per marker but sum.
#
#   noTimes     Flag to select the returning of the time points.
#   startFrame  Start frame
#   endFrame    End frame that should be extracted.
#   inYr        If times are in years.
#
def calculateMarkerEnergy(db, mIdx, gY,
        gX = 0, Epot0 = 0.0,
        total = False,
        noTimes = False,
        onlyKin = False,
        startFrame = 0, endFrame = -1, inYr = False,
        specific = False, useFeelVel = True,
        F = None):

    if(sTraits.isDataBase(db) == False):
        raise ValueError("Not passed a database, instead passed a {}.".format(type(db)))
    # end if: check

    if(sTraits.isInt(mIdx) == True):
        mIdx = list([mIdx])
    # end if:

    nM = len(mIdx)
    if(nM <= 0):
        raise ValueError("No marker specified.")
    # end check

    # Get the time points we need
    times, Idx = sExtr.extractTimePoints(src = db,
                    start = startFrame, ende = endFrame, inYr = inYr,
                    alsoIdx = True)

    # This is the variable which will hold the energy
    energy = list()

    EpotInit = None
    if(sTraits.isArray(Epot0) ):
        EpotInit = Epot0
    else:
        EpotInit = Epot0 * np.ones(len(mIdx))

    if(sTraits.isArray(EpotInit) == False):
        raise ValueError("No array provided as potential energy.")
    if(len(EpotInit) != nM):
        raise ValueError("The initial value for potential energy is wrong, expexted an array of length {}, but got length {}".format(len(mIdx), len(EpotInit)))
    # end check:

    # This is the temporary for the association
    K = cSibyl.cSibyl_defAssociation(db.nMarkers())

    # We will now detrmine which velocity we are using
    usedVelX = PropIdx.FeelVelX if useFeelVel else PropIdx.VelX
    usedVelY = PropIdx.FeelVelY if useFeelVel else PropIdx.VelY

    f = F
    try:
        if(F is None):
            f = db.getDBHandler()
        # end if: handler


        # Test if the specific flag is given, so we can calculate the extension
        #  once. Note that in the following we assume that the domain size never
        #  changes.
        if(specific ):
            gPosX, gPosY = db.getPlotPos(prop = None, idx = 0, gType = eGridType.RegBasic, F = f, noMeshing = True)
            lengthX = gPosX[-1] - gPosX[0]
            lengthY = gPosY[-1] - gPosY[0]
            domVol  = lengthX * lengthY
            lastNMarkers    = -1                # Total markers that are here
        # end if: specific

        # Iterate through the states
        for i in range(len(Idx)):
            idx = Idx[i]        # Current index we handle

            # This variable is used to hold the indexes that we iterate in this iteration
            mIdx_i = None
            if(sTraits.isIndexSeries(mIdx) ):
                mIdx_i = mIdx
            else:
                raise ValueError("An unkown type for the index series was passed, it was a {}".format(type(mIdx)))
            # end check:

            # Load the marker propertiey we need
            x  = db.getSimState(prop = PropIdx.PosX   , onMarker = True, stateIdx = idx, F = f)
            y  = db.getSimState(prop = PropIdx.PosY   , onMarker = True, stateIdx = idx, F = f)
            vx = db.getSimState(prop = usedVelX       , onMarker = True, stateIdx = idx, F = f)
            vy = db.getSimState(prop = usedVelY       , onMarker = True, stateIdx = idx, F = f)
            r  = db.getSimState(prop = PropIdx.Density, onMarker = True, stateIdx = idx, F = f)

            if(not specific):
                # This is the theoretical extension
                gridX, gridY = db.getPlotPos(gType = eGridType.RegBasic, idx = idx, prop = None, F = f, noMeshing = True)
                theoExtension = cSibyl.cSibyl_updateTheoreticalExtension(
                        xPosMarker = x, yPosMarker = y, K = K,
                        gPosX = gridX, gPosY = gridY)
                theoExtension = theoExtension[mIdx_i]   # Select only the one we need
            else:
                if(lastNMarkers != len(x)):
                    lastNMarkers = len(x)                                        # Total numbers markers
                    theoExtension = (domVol / lastNMarkers) * np.ones(len(mIdx)) # Calculating the averige space a marker occupies
                # end if: maybe update theoretical extension
            # End else:

            # Filtering out the markers that are not interesting
            x  =  x[mIdx_i]
            y  =  y[mIdx_i]
            vx = vx[mIdx_i]
            vy = vy[mIdx_i]
            r  =  r[mIdx_i]

            # Calculate the kinetic energy
            Ekin = 0.5 * r * theoExtension * (vx ** 2 + vy ** 2)

            # This is the potential enery
            Epot = None

            if(onlyKin):
                if(total):
                    Epot = 0
                else:
                    Epot = np.zeros(len(mIdx_i))
            else:

                # Calculate the potential energy change
                if(i == 0):
                    # In first step, we have to add the potential energy from the initial
                    Epot = EpotInit
                else:
                    # Signes are complicated. It is more important that they
                    # are different, if they are wrong then it is just not the meaning
                    # but the lagrangien.
                    Epot = r * theoExtension * ( +gX * (x) - gY * (y) ) + EpotInit
                # end: handle potential energy
            #end if: calculate Epot

            if(total == True):
                energy.append((Epot + Ekin).sum())
            else:
                energy.append(Epot + Ekin)
        # end for(i):

    finally:
        if(F is None):
            db.closeDBHandler(f)
    # end finally


    # Return
    if(noTimes ):
        return energy
    else:
        return (energy, times)
# end: calculateMarkerEnergy





# This function is able to determine the associated node index.
#  The node indexes are the ones that are top left of the marker.
#  This function needs the position of the grid nodes and the
#  marker positions. Note that this function is grid agnostic.
#  Negative index will triger an error.
#  Note that this function returns first the association in X
#  and then in Y, this is different from EGD.
#
#   X       The x position of the markers, if Y is NONE, then X is expected to be a tuple of arrays.
#   Y       The y position of the markers, if NONE, X is a tuple.
#   gX      The x position of the the grid points, if yG is NONE, this must be a tuple of arrays
#   gY      The y position of the grid nodes.
#   isTight If set to TRUE, the bounding is tight.
#
def findAssociatedNodes(
         X,  Y,         # Maker Pos
        gX, gY,         # Grid Pos
        isTight = False # Is the bounding tight
    ):

    # Unpack the arguments
    xMarker = X
    yMarker = Y
    xGrid   = gX
    yGrid   = gY

    if(sTraits.isNone(gY) ):
        if(sTraits.isTuple(gX, 2) == False):
            raise ValueError("gY is not given and gX is not a tuiple of 2, instead it is a {}".format(type(gX)))
        # end checks:
        xGrid = gX[0]
        yGrid = gX[1]
    # end: unpack grid

    if(sTraits.isNone(Y) ):
        if(sTraits.isTuple(X, 2) == False):
            raise ValueError("Y is not given and X is not a tuple of length two, instead it is a {}".format(type(X)))
        # end checks:
        xMarker = X[0]
        yMarker = X[1]
    # end: unpack grid

    if((sTraits.isArray(xMarker) == False) or
       (sTraits.isArray(yMarker) == False)   ):
        raise ValueError("The passed marker arrays were not arrays. X ~ {}; Y ~ {}".format(type(xMarker), type(yMarker)))
    if(len(xMarker) != len(yMarker) ):
        raise ValueError("The passed marker arraies had different length, X ~ {}; Y ~ {}".format(len(xMarker), len(yMarker)))
    # end checks:

    if((sTraits.isArray(xGrid) == False) or
       (sTraits.isArray(yGrid) == False)   ):
        raise ValueError("The passed marker arrays were not arrays. X ~ {}; Y ~ {}".format(type(xGrid), type(yGrid)))
    if((util.isSorted(xGrid, True) == False) or
       (util.isSorted(yGrid, True) == False)   ):
        raise ValueError("The passed grid arries are not sorted.")
    # end checks:

    xAssos, yAssos = cSibyl.cSibyl_findAssociation(
                        xPosMarker = xMarker, yPosMarker = yMarker,
                        gPosX = xGrid, gPosY = yGrid,
                        isTight = isTight)

    return (xAssos, yAssos)
# end def: findAssociatedNodes


# This function determines the marker densities in the basic cells.
#  There is a trick here, there is one grid point more than we have
#  cells, this function will respect this. There are several output
#  formats that are suported. By default only two vectors, min and
#  max are returned, in that order. But also time can be returned.
#  It is also possible to return mean and std of the count.
#  And a full list with the association. It is also possible to
#  compute quantiles, the quantiles are stored in a (nS, nQ) matrix
#  where qN is the number of passed quantiles, and nS the number
#  of states that whewre examined. q must contains values in the range
#  [0, 1] and of list type.
#  The full outoout format is:
#       (MIN, MAX, [TIME], [MEAN, STD], [QMAT], [FULLDATA])
#
#
#   db          The database that is used.
#   noTimes     Flag to select the returning of the time points.
#   startFrame  Start frame
#   endFrame    End frame that should be extracted.
#
def calculateMarkerCount(db,
        noTimes = False,
        startFrame = 0, endFrame = -1, inYr = False,
        fullCount = False,
        meanStd = True,
        q = None,
        F = None):

    if(sTraits.isDataBase(db) == False):
        raise ValueError("Not passed a database, instead passed a {}.".format(type(db)))
    # end if: check

    # Get the time points we need
    times, Idx = sExtr.extractTimePoints(src = db,
                    start = startFrame, ende = endFrame, inYr = inYr,
                    alsoIdx = True)

    # Allocate the return values.
    mMax     = np.zeros(len(Idx))    # min and max values
    mMin     = np.zeros(len(Idx))
    mMean    = np.zeros(len(Idx))
    mStd     = np.zeros(len(Idx))
    fullData = list() if fullCount else None
    qMat     = None

    if(not sTraits.isNone(q) ):
        if(sTraits.isFloat(q) ):
            raise ValueError("q is not a list like object, but of type {}".format(type(q)))
        # end check:

        nQ = len(q)         # Number of quantiles
        qMat = np.zeros((len(Idx), nQ))
    # end if: q is given

    f = F
    try:
        if(F is None):
            f = db.getDBHandler()

        # Iterate through the states
        for i in range(len(Idx)):
            idx = Idx[i]        # Current index we handle

            # Load the marker propertiey we need
            x = db.getSimState(prop = PropIdx.PosX,    onMarker = True, stateIdx = idx, F = f)
            y = db.getSimState(prop = PropIdx.PosY,    onMarker = True, stateIdx = idx, F = f)

            # Load the grid positions of the basic nodal grid
            gX, gY = db.getPlotPos(gType = eGridType.RegBasic, idx = idx, prop = None, noMeshing = True, F = f)

            # Determine the association of the markers
            I, J = cSibyl.cSibyl_findAssociation(xPosMarker=x, yPosMarker=y, gPosX=gX, gPosY=gY, isTight=True)

            # Get the theoretical extension, needs all markers for this
            cellCount = cSibyl.cSibyl_countAssocMarker(yAssoc=I, xAssoc=J, nGridY=gY, nGridX=gX)

            # Now reduce the matrix such that only cells are remaining
            cellCount = cellCount[0:-1, 0:-1]

            # Now determine the max and the minimum
            mMax[i] = np.amax(cellCount)
            mMin[i] = np.amin(cellCount)

            # Mean and std
            if(meanStd ):
                mMean[i] = np.mean(cellCount)
                mStd [i] = np.std (cellCount)
            # end if: mean

            # Quantiles
            if(not sTraits.isNone(q) ):
                qMat[i, :] = np.quantile(a = cellCount, q = q, interpolation = 'linear')
            # end if: quantile

            # Test if we have to store the full data
            if(fullCount == True):
                fullData.append(cellCount)
            # end full count

        # end for(i):

    finally:
        if(F is None):
            db.closeDBHandler(f)
    # end

    # This the the return value that is returned anyway
    RET = (mMin, mMax)

    if(noTimes == False):
        RET = (*RET, times)
    if(meanStd ):
        RET = (*RET, mMean, mStd)
    if(not sTraits.isNone(q) ):
        RET = (*RET, qMat)
    if(fullCount ):
        RET = (*RET, fullData)

    return RET
# end: calculate association


