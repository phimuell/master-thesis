# This file is part of Sibyl.
#
# This file contains the operation functions.
# This functions allows easaly to create derived quantities
# from series and such.
# It builds on the idea of operations.


# Normal
import h5py
import numpy as np
import math
import copy

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
import libSibyl.sibyl_traits         as sTraits


# This is the null operation.
#  Its argument is just returned without modifications.
#   s   The series.
#
def nullOps(s):
    return s
# end def: nullOps


# This is a general axpy like meta operation.
#  It will execute the following operation:
#       a * op(s) + y
#  Where op is a passed operation. Note that
#  the interpretion of None of the argument is such
#  that they will not do any action.
#
def genOps(s,
        op = nullOps,
        a = None,       # Will be interpreted as one.
        y = None        # Will be interpreted as zero.
    ):

    if((a is None) and (y is None)):
        return op(s)

    if(a is None):  # y is not None
        return op(s) + y

    if(y is None):  # a is notz None
        return a * op(s)

    return a * op(s) + y
# end def: general operation


# This function operates on series, in order to work
#  s1 and s2 must be simulation series or state series.
#  By default they must have the same length, as given
#  by len(), but this can be disabled, then the returned
#  series has the smallest length of the two.
#
#  This function will merge the two corresponding states
#  of the two given series, according to
#   s_i = gamma * op3( alpha * op1(s1_i) + beta * op2(s2_i) )
#
#  Note that the result of the operation, s_i above, is
#  deepcopied into the resulting list.
#
def generalSeriesMerger(
        s1, s2,         # The series to be merged

        alpha = 1.0,    # Scalling factors
        beta  = 1.0,
        gamma = 1.0,

        op1 = nullOps,  # Operations
        op2 = nullOps,
        op3 = nullOps,

        sameLength = True   # Force that s1 & s2 have the same length
    ):

    if(s1 is None or s2 is None):
        raise ValueError("Passed None as series.")
    # end if: check for None series

    if(sTraits.isSeries(s1) == False  or
       sTraits.isSeries(s2) == False    ):
        raise ValueError("You have not passed an abject that does not apear to be a series. s1 = {}; s2 = {}.".format(type(s1), type(s2)))
    # end if check if series

    if(alpha is None  or
       beta  is None  or
       gamma is None    ):
        raise ValueError("Passed illegal scaling constants. Passed {}, {}, {}.".format(alpha, beta, gamma))
    # end if: check scaling constant

    if(op1 is None  or
       op2 is None  or
       op3 is None    ):
        raise ValueError("Passed None as operation.")
    # end if: check if ops are valid

    # end checks


    # Get the length of the two and check
    nS1 = len(s1)
    nS2 = len(s2)

    nStates = min(nS1, nS2)     # Thsi is the number of states

    if(sameLength == True):
        if(nS1 != nS2):
            raise ValueError("The length of the series differs, s1 has length {} and s2 has length {}.".format(nS1, nS2))
        # end if: check
    # end if: check if length is strange

    # Make the resulting list
    resList = list()


    #
    # Perform the merge operation
    for i in range(nStates):
        resList.append(copy.deepcopy(
                            gamma * op3( alpha * op1(s1[i]) + beta * op2(s2[i]) )
                                    )
                      )
    # end for(i): merge


    #
    # Return the list
    return resList
# end generalSeriesMerger



# This function computes the differences between two series.
#  The difference between two series is formed by operation
#  state wise on them. The new series will be a series
#  each state of it, is the differences of the states. To
#  each state an operation is applied, such that the
#  difference is computed as:
#   diff_i := op(s1_i) - op(s2_i)
#
#   s1      The first series.
#   s2      The second series.
#   op{1,2} Operations, defaults to nullOps
#
def differenceSeries(
        s1, s2,

        op1 = nullOps,
        op2 = nullOps,

        sameLength = True,
    ):

    return generalSeriesMerger(s1 = s1, s2 = s2,
            op1 = op1, op2 = op2,
            alpha = 1.0, beta = -1.0, gamma = 1.0,
            sameLength = sameLength)
# end def: copute the difference


# This fucntion is used to generate the squared
#  differences, of the series. The operationreads
#  as:
#   sqDiff_i := ( op1(s1_i) - op2(s2_i) ) .^ 2
#  Note that the .^ is read as coefficient whise
#  square.
#
#   s1      The first series.
#   s2      The second series.
#   op{1,2} Operations, defaults to nullOps
#
def squareDifferenceSeries(
        s1, s2,

        op1 = nullOps,
        op2 = nullOps,

        sameLength = True,
    ):

    def pow2(s):
        return np.power(s, 2.0)
    # end def: pow2

    return generalSeriesMerger(s1 = s1, s2 = s2,
            op1 = op1, op2 = op2,
            op3 = pow2,
            alpha = 1.0, beta = -1.0, gamma = 1.0,
            sameLength = sameLength)
# end def: compte the squate difference


