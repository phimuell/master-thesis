/**
 * \brief	This file contains the implentation of the test functions for matrix matrix multiplication.
 */

#include "./cLibSibyl_linAlg.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"
#include "./cSibylTests.h"

#include <stdio.h>


Int_t
cSibylTest_matProd(void)
{
	{
		Numeric_t A[] = {-1.0, -2.0,  1.0,  1.0};
		Numeric_t B[] = { 1.0,  2.0, -1.0, -1.0};
		Numeric_t C[] = { 0.0,  0.0,  0.0,  0.0};

		const Int_t e = cLibSibyl_dGEMM(20.0, A, B, C, 2, 2, 2);

		if(e != 0)
		{
			printf("There was an error %d\n", e);
		};

		printf("The matrix is:\n");

		cLibSibyl_printMatrix(C, 2, 2);
	};
	{
		Numeric_t A[] = {1, 2, 0, 2, 4, 1, 2, 1, 0};
		Numeric_t B[] = { -1./3, 0, 2./3, 2./3, 0, -1./3, -2, 1, 0};
		Numeric_t C[9];

		const Int_t e = cLibSibyl_dGEMM(0.0, A, B, C, 3, 3, 3);

		if(e != 0)
		{
			printf("There was an error %d\n", e);
		};

		printf("\n\nThe matrix is:\n");

		cLibSibyl_printMatrix(C, 3, 3);
	};

	{
		Numeric_t A[] = {5.0, 3.0, -2.0};
		Numeric_t B[] = {2.0, 4.0, -3.0};
		Numeric_t C[9];
		const Int_t e = cLibSibyl_dGEMM(0.0, A, B, C, 3, 1, 3);

		if(e != 0)
		{
			printf("There was an error %d\n", e);
		};

		printf("\n\nThe matrix is:\n");

		cLibSibyl_printMatrix(C, 3, 3);
	};

	{
		Numeric_t A[] = {2.0, 4.0, -3.0};
		Numeric_t B[] = {-1.0, 0.0, 4.0, 3.0, 2.0, -3.0};
		Numeric_t C[2];
		const Int_t e = cLibSibyl_dGEMM(0.0, A, B, C, 1, 3, 2);

		if(e != 0)
		{
			printf("There was an error %d\n", e);
		};

		printf("\n\nThe matrix is:\n");

		cLibSibyl_printMatrix(C, 1, 2);
	};

	{
		Numeric_t A[] = {-1.0, 0.0, 4.0, 3.0, 2.0, -3.0};
		Numeric_t B[6];
		Numeric_t x[] = { 1.0, 1.0};
		Numeric_t y[] = {0.1, 0.1, 0.1};
		const Int_t e = cLibSibyl_dGEMV(10.0, A, x, y, 3, 2);

		if(e != 0)
		{
			printf("There was an error %d\n", e);
		};

		printf("\n\nThe matrix is:\n");

		cLibSibyl_printMatrix(y, 3, 1);
	};
	{
		Numeric_t A[] = {-1.0, 0.0, 4.0, 3.0, 2.0, -3.0};
		Numeric_t B[6];
		Numeric_t x[] = { 1.0, 1.0};
		Numeric_t y[] = {0.1, 0.1, 0.1};

		if(cLibSibyl_transpose(A, B, 3, 2) != 0)
		{
			printf("Error wihile transposing.\n");
			return 1;
		};

		const Int_t e = cLibSibyl_dGEMtV(10.0, B, x, y, 2, 3);

		if(e != 0)
		{
			printf("There was an error %d\n", e);
		};

		printf("\n\nThe matrix is:\n");

		cLibSibyl_printMatrix(y, 3, 1);
	};

	{
		Numeric_t A[] = {-1.0, 0.0, 4.0, 3.0, 2.0, -3.0};
		Numeric_t B[6];
		Numeric_t C[4];

		if(cLibSibyl_transpose(A, B, 3, 2) != 0)
		{
			printf("Error wihile transposing.\n");
			return 1;
		};

		const Int_t e = cLibSibyl_dMtSquare(0.0, B, C, 2, 3);

		if(e != 0)
		{
			printf("There was an error %d\n", e);
		};

		printf("\n\nThe matrix is:\n");

		cLibSibyl_printMatrix(C, 2, 2);
	};

	return 0;
};





