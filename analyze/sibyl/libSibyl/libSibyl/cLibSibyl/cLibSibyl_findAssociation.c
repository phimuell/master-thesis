/**
 * \brief	This file computes the assocaition.
 *
 */

//We define the no check macro
#define CSIBYL_NO_CHECKS_IN_FINDIDX 1

#include "./cLibSibyl.h"
#include "./cLibSibyl_internal.h"
#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"

#include <assert.h>
#include <stdlib.h>
#include <math.h>


/**
 * \brief	This is a helper function that is able to check the association.
 *
 * The last known associationis passed into this this function and the list of
 * grid points. This function will then try to find the new association.
 * If this happens succed it will return 0. If the new association could
 * not be found a positive value is returned, thus recomputing must be done.
 * A negative value indicates an error.
 *
 * \param  a 		Last knwo association.
 * \param  mPos		Position of the marker.
 * \param  gPoints	Grid points.
 * \param  nPoints 	Number of grid points.
 */
static
Int_t
cLibSibyl_quickCheckAssociation(
	      Int_t* const restrict 		a,
	const Numeric_t 			mPos,
	const Numeric_t* const restrict 	gPoints,
	const Int_t 	 			nPoints);



/*
 * ==========================
 * Working functions
 */

Int_t
cLibSibyl_findAssociation(
	Numeric_t* restrict 		yPos,
	Numeric_t* restrict 		xPos,
	Int_t 				nMarker,
	Numeric_t* restrict 		gPosY,
	Numeric_t* restrict 		gPosX,
	Int_t 				nGridY,
	Int_t 				nGridX,
	Int_t 				isTight,
	Int_t* restrict 		I /* OUT */,
	Int_t* restrict 		J /* OUT */)
{
	Int_t m;
	Index_t i, j;
	Numeric_t x, y;

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;


	if(nMarker < 0)
		{cLibSibylRT_exit(10);};
	if(nGridX < 2)
		{cLibSibylRT_exit(11);};
	if(nGridY < 2)
		{cLibSibylRT_exit(12);};

	for(m = 0; m < nMarker; ++m)
	{
		x = xPos[m];	/* load positions */
		y = yPos[m];
			csibyl_assert2(isfinite(x), 13);
			csibyl_assert2(isfinite(y), 14);

		i = cLibSibyl_findAssocIdx_(y, gPosY, nGridY, isTight);
		j = cLibSibyl_findAssocIdx_(x, gPosX, nGridX, isTight);
			csibyl_assert2(i >= 0, i);
			csibyl_assert2(j >= 0, j);
			csibyl_assert2(i < nGridY, i);
			csibyl_assert2(j < nGridX, j);
			csibyl_assert2(gPosX[j] <= x, 1000);
			csibyl_assert2(gPosY[i] <= y, 1001);
			csibyl_assert2(i < (nGridY - 1) ? (y < gPosY[i + 1]) : 1, -i - 1);
			csibyl_assert2(j < (nGridX - 1) ? (x < gPosX[j + 1]) : 1, -j - 1);

		I[m] = i;
		J[m] = j;
	}; /* End for(m): */

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
}; /* End: find association */


Int_t
cLibSibyl_findAssociationK(
	Numeric_t* restrict 		yPos,
	Numeric_t* restrict 		xPos,
	Int_t 				nMarker,
	Numeric_t* restrict 		gPosY,
	Numeric_t* restrict 		gPosX,
	Int_t 				nGridY,
	Int_t 				nGridX,
	Int_t 				isTight,
	Int_t* restrict 		K /* OUT */)
{
	Int_t m, n, o;
	Index_t i, j;
	Numeric_t x, y;
	Int_t* nGridX_mult = NULL;

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;


	if(nMarker < 0)
		{cLibSibylRT_exit(10);};
	if(nGridX < 2)
		{cLibSibylRT_exit(11);};
	if(nGridY < 2)
		{cLibSibylRT_exit(12);};


	/* Allocate a Lookup table
	 * and fill it */
	nGridX_mult = cLibSibyl_iArray(nGridY);

	for(i = 0; i != nGridY; ++i)
	{
		nGridX_mult[i] = i * nGridX;
	};

	n = nGridX * nGridY; 	/* Total number of cells there is */

	for(m = 0; m < nMarker; ++m)
	{
		x = xPos[m];	/* load positions */
		y = yPos[m];
			csibyl_assert2(isfinite(x), 13);
			csibyl_assert2(isfinite(y), 14);

		i = cLibSibyl_findAssocIdx_(y, gPosY, nGridY, isTight);
		j = cLibSibyl_findAssocIdx_(x, gPosX, nGridX, isTight);
			csibyl_assert2(i >= 0, i);
			csibyl_assert2(j >= 0, j);
			csibyl_assert2(i < nGridY, i);
			csibyl_assert2(j < nGridX, j);
			csibyl_assert2(gPosX[j] <= x, 1000);
			csibyl_assert2(gPosY[i] <= y, 1001);
			csibyl_assert2(i < (nGridY - 1) ? (y < gPosY[i + 1]) : 1, -i - 1);
			csibyl_assert2(j < (nGridX - 1) ? (x < gPosX[j + 1]) : 1, -j - 1);

		o = nGridX_mult[i] + j;
			csibyl_assert2(o < n, 88);
			csibyl_assert2(0 <= o, 88);

		K[m] = o;
	}; /* End for(m): */

	CSIBYL_RT_FUNC_CLEANUP:
	cLibSibyl_iFree(&nGridX_mult);

	CSIBYL_RT_FUNC_RETURN;

#ifdef NDEBUG
	(void)n;
#endif
};


Int_t
cLibSibyl_updateAssociationK(
	const Numeric_t* const restrict 	yPos,
	const Numeric_t* const restrict 	xPos,
	const Int_t 				nMarker,
	const Numeric_t* const restrict 	gPosY,
	const Numeric_t* const restrict 	gPosX,
	const Int_t 				nGridY,
	const Int_t 				nGridX,
	const Int_t 				isTight,
	      Int_t*     const restrict 	K /* IN/OUT */)
{
	Int_t m, n, k;
	Int_t recompI, recompJ;
	Int_t updateI, updateJ;
	Int_t i, j;
	Numeric_t x, y;

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;


	if(nMarker < 0)
		{cLibSibylRT_exit(10);};
	if(nGridX < 2)
		{cLibSibylRT_exit(11);};
	if(nGridY < 2)
		{cLibSibylRT_exit(12);};

	n = nGridX * nGridY; 	/* Total number of cells there is */

	for(m = 0; m < nMarker; ++m)
	{
		recompI = 1;	/* store that we want to recompute both indexes */
		recompJ = 1;

		x = xPos[m];	/* load positions */
		y = yPos[m];
		k = K   [m];
			csibyl_assert2(isfinite(x), 13);
			csibyl_assert2(isfinite(y), 14);


		/* Test if k appears to be an invalid index */
		if(k < 0 || n <= k)
			{goto RECOMPUTE;};

		i = k / nGridX;		/* Decompose the index, compiler should be able to fuse this */
		j = k % nGridX;
			csibyl_assert2(i >= 0, i);
			csibyl_assert2(j >= 0, j);
			csibyl_assert2(i < nGridY, i);
			csibyl_assert2(j < nGridX, j);


		/* Now we perform an update operation */
		updateI = cLibSibyl_quickCheckAssociation(&i, y, gPosY, nGridY);
		if(updateI == 0)		/* Update succeeded */
			{recompI = 0;}
		else if(updateI < 0)
			{cLibSibylRT_exit2(updateI, "UpdateI failed.");};

		updateJ = cLibSibyl_quickCheckAssociation(&j, x, gPosX, nGridX);
		if(updateJ == 0)
			{recompJ = 0;}
		else if(updateJ < 0)
			{cLibSibylRT_exit2(updateJ, "UpdateJ failed.");};



		/* ==================
		 * Recompute the index */
		RECOMPUTE:

		if(recompI != 0)
		{
			i = cLibSibyl_findAssocIdx_(y, gPosY, nGridY, isTight);
		}
		if(recompJ != 0)
		{
			j = cLibSibyl_findAssocIdx_(x, gPosX, nGridX, isTight);
		}
			csibyl_assert2(i >= 0, i);
			csibyl_assert2(j >= 0, j);
			csibyl_assert2(i < nGridY, i);
			csibyl_assert2(j < nGridX, j);
			csibyl_assert2(gPosX[j] <= x, 1000);
			csibyl_assert2(gPosY[i] <= y, 1001);
			csibyl_assert2(i < (nGridY - 1) ? (y < gPosY[i + 1]) : 1, -i - 1);
			csibyl_assert2(j < (nGridX - 1) ? (x < gPosX[j + 1]) : 1, -j - 1);

		k = nGridX * i + j;		/* calculate the new index */
			csibyl_assert2(k < n, 88);
			csibyl_assert2(0 <= k, 88);

		K[m] = k;			/* store the index */
	}; /* End for(m): */

	CSIBYL_RT_FUNC_CLEANUP:

	CSIBYL_RT_FUNC_RETURN;

#ifdef NDEBUG
	(void)n;
#endif
};





/*
 * ========================
 * Helper implementation
 */
Int_t
cLibSibyl_quickCheckAssociation(
	Int_t* const restrict 			a_,
	const Numeric_t 			mPos,
	const Numeric_t* const restrict 	gPoints,
	const Int_t 	 			nPoints)
{
	const Int_t     a       = *a_;		/* load the value of the index into a variable */
	const Numeric_t gX      = gPoints[a];	/* load the node position */
	const Int_t 	lastIdx = nPoints - 1;	/* this is the last node a marker can be associated with */
#	ifndef NDEBUG
	if(mPos < gPoints[0])
		{ CSIBYL_RT_INSTALL_ERROR(-8989, "Marker with too low coordinate.");
		  return (-8989); }
#	endif

	const Int_t searchRadius = 4;	/* maximum number of moves we do to find a node */

	if(a == lastIdx)
	{
		/* marker is associated to the last node */

		if(gX <= mPos) 	/* Marker's position is still larger than the node, so association is still given */
			{return 0;};

		/* Starting to search for the position
		 * only make small probing */

		/* we do not go deeper than this */
		const Int_t maxMovement = iMax(0, a - searchRadius);

		/* Going down and search for the position
		 * Since the marker moved to lower values, we must go downwoards. */
		for(Int_t k = a - 1; k >= maxMovement; --k)
		{
			const Numeric_t gXk = gPoints[k];	/* load the grid point */
			if(gXk <= mPos)
			{
				/* We must go down until the grid node is left from the
				 * marker again */
				*a_ = k;
				return 0;
			};
		}; /* end for(k): */

		/* If we are here the marker has moved quite a bit, so we
		 * will return 1 to indicate to the function that recomputing must be done */
		return 10;
	}
	if(a == 0)
	{
		/* The marker was associated to the first one.
		 * Since of the grid layout, and the test from above, we know
		 * That the marker can not move below the first (0) index.
		 * So we will must test if it is still inside */
		if(mPos < gPoints[1])
		{
			/* The marker is not larger than the second possible
			 * node, so it must still be associated to the first
			 * one */
			return 0;
		};

		const Int_t maxMovement = iMin(nPoints, searchRadius);		/* do not go farther than this */

		/* Now we will check the larger nodes. We will go upwards
		 * until we find a node that is right from the marker.
		 * The node before that node is the node where the marker is associated to */
		for(Int_t k = 1; k != maxMovement; ++k)
		{
			const Numeric_t gXk = gPoints[k];

			if(!(gXk <= mPos))
			{
				*a_ = k - 1;
				return 0;
			};
		}; /* end for(k): moving upwards */


		/* If we are here, then we where not able to find the new
		 * node where the marker was associated to */
		return 20;
	};


	/* If we are here, then we know that we are not at the exremes.
	 * We also nwo that we can safly check if the marker is still inside
	 * the associated range */
	{
		const Numeric_t gXp = gPoints[a + 1];

		if((gX <= mPos) && (mPos < gXp))
		{
			/* it is still in the same cell */
			return 0;
		};
	}; /* end scope: is marker still inside the cell */


	/*
	 * Now we make a search in a specific direction.
	 */
	if(mPos <  gX)
	{
		/* The marker has gone downwards */

		/* check if it is in the cell below */
		const Numeric_t gXm = gPoints[a - 1];
		if(gXm <= mPos)
		{
			/* he is in the cell below */
			*a_ = a - 1;
			return 0;
		};

		/* We have not found the marker so give
		 * the command to perform a search */
		return 30;
	}
	else
	{
		/* he has gone upwards */

		/* check if the marker in the cell above */
		const Numeric_t gXp = gPoints[a + 1];

		if(gXp <= mPos)
		{
			/* could be inside the upper cell */
			if(lastIdx == (a+1))
			{
				/* for sure in the upper cell, because no other cell */
				*a_ = a + 1;
				return 0;
			};

			/* here we know that we can safly access the next nxt one */
			const Numeric_t gXpp = gPoints[a + 2];

			if(mPos < gXpp)
			{
				/* is in the upper cell too */
				*a_ = a + 1;
				return 0;
			};

			/* We have not found anything, so we must issue a research */
			return 40;
		}
		else
		{
			assert(0 && "This is handled by thie inside test above");
			return -4;
		};


		/* If we are here, then the marker has moved a lot
		 * So start a search */
		return 50;
	};


	assert(0 && "Unreadchable code.");
	return -5;
}; /* end: quickCheckAssociatin */


