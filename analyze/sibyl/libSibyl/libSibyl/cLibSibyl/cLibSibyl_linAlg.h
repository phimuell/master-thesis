/**
 * \brief	This file contains some functions that perform linear algebra operations.
 *
 * Note they are not very efficiently implemented. This is a quick and dirty way.
 * They are mostly build on the Numerical Recipies, they are solid textbook
 * implementations, but not optimized for speed.
 *
 * All matrices are assumed to be row mayjor.
 *
 * Generally it is unspecific if the functions will set an error code or not into
 * the runtime. However each error will return a globaly, to the linear algebra
 * functions, unique error code, some functions call assert.
 *
 * All functions will return zero on success.
 */
#ifndef CLIBSIBYL_LINALG_HEADER
#define CLIBSIBYL_LINALG_HEADER

/* includes */
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"


/*
 * ============================
 * High Level Functions - LAPACK
 * These function implements some high
 * level routines.
 */


/**
 * \brief	This function applys a Savitzky-Golay Smoothing step
 * 		 to the passed data.
 *
 * This function works on the full data set that has to be smoothed.
 * It will also write the smoothed value into the the array. The main
 * difference of this implementation is that it does not need equispaced
 * values. But this imposes the need to do heavy computation on each call.
 *
 * Assume the following data set $ \Big{ \left(x_{i}, y_{i} \right)
 * }_{i = 0}^{n - 1} $. This function allows to compute the smoothed
 * data set $\Big{ \left(x_{i}, \hat{y}_{i} \right) }_{i = 0}^{n - 1} $
 * One call to this function will compute $\hat{y}_{m}$ as an average
 * of $\Big{(x_{j}, y_{j})}_{j = m - nL}^{m + nR}$. This means the
 * window size is $nL + nR + 1$. Inside this window a polynomial of
 * degree deg is fitted, and this is used to compute the value.
 *
 * The value m is interpreeted globaly .
 *
 * This function also allows to compute smoothed derivatives.
 * This can be selected with the dy parameter. A zeero means no derivative.
 *
 * It needs working memory. That must be passed to it, there is a
 * function to generate it.
 *
 * \param  x		Places where the value was recorded, array of length n.
 * \param  y		Recorded observation, array of length n.
 * \param  n		Number of observation.
 * \param  yHat		Smoothed values, array of length n.
 * \param  deg 		The degree of the polynomial to fit.
 * \param  dy 		Numbers of derivatives.
 * \param  nL		Numbers of smoothing points to the left.
 * \param  m		Which observation to smooth.
 * \param  nR		Number of smoothing points to the right.
 * \param  w 		Working memory.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_savgolSmoothStep(
	const Numeric_t* restrict 	x,
	const Numeric_t* restrict 	y,
	const Int_t 			n,
	      Numeric_t* restrict 	yHat,
	const Int_t 			deg,
	const Int_t 			dy,
	const Int_t 			nL,
	const Int_t 			m,
	const Int_t 			nR,
	      void*      restrict 	w);


/**
 * \brief	This function returns the number of bytes that would be allocated
 * 		 by the work memory function.
 *
 * This function returns a negative value on an error. By setting the
 * last argument to true, this function will try to install the error
 * in the runtime and return, on FALSE, no such attemp is made, and a
 * negative value is returned.
 * Remember this function is in bytes, or what sizeof() understands of it.
 * Also remember that zero is an invalid value as well.
 *
 * \param  deg 		The degree of the polynomial to fit.
 * \param  nL		Numbers of smoothing points to the left.
 * \param  nR		Number of smoothing points to the right.
 * \param  repErr	Install the error in the runtime.
 */
extern
CSIBYL_INTERN
Index_t
cLibSibyl_savgolWorkMemorySize(
	const Int_t 			deg,
	const Int_t 			nL,
	const Int_t 			nR,
	const Bool_t 			repError);

/**
 * \brief	This function allocates working memory for the savgol smoother.
 *
 * This function allocates a helper array that is used by the savgol solver to
 * perform calöculations.
 * It is just a wrapper arround malloc, the memory can be freed by the normal
 * free function.
 *
 * \param  deg 		The degree of the polynomial to fit.
 * \param  nL		Numbers of smoothing points to the left.
 * \param  nR		Number of smoothing points to the right.
 *
 * This function will never fail.
 */
extern
CSIBYL_INTERN
void*
cLibSibyl_savgolWorkMemory(
	const Int_t 			deg,
	const Int_t 			nL,
	const Int_t 			nR)
 CSIBYL_ATTR_MALLOC;











/*
 * ============================
 * Matrix operations - BLAS
 */

/**
 * \brief	This function multiplies two matrices together in the way GEMM does.
 *
 * This function basically implements:
 * 	\mat{C} <- \Mat{A} \Mat{B} + \gamma * \Mat{C}
 *
 * This means C is overwritten. \Mat{A} is a $n \times p$ matrix
 * and \Mat{B} is a $p \times m$ matrix.
 *
 * This function returns zero on success and non zero on failure.
 * It does not modify the internal error state.
 *
 * \param  gamma
 * \param  A		Input
 * \param  B 		Input
 * \param  C		Output/input
 * \param  n		Rows of A and C
 * \param  p		Columns of A and rows of B
 * \param  m 		Columns of B and C
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_dGEMM(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	const Numeric_t* restrict 	B,
	      Numeric_t* restrict 	C,
	const Int_t 			n,
	const Int_t 			p,
	const Int_t 			m);


/**
 * \brief	This function multiplies two matrices together in the way GEMM does.
 *
 * This function basically implements:
 * 	\mat{C} <- \Mat{A} \Mat{B} + \gamma * \Mat{C}
 *
 * This means C is overwritten. \Mat{A} is a $n \times p$ matrix
 * and \Mat{B} is a $p \times m$ matrix.
 *
 * This function returns zero on success and non zero on failure.
 * It does not modify the internal error state.
 *
 * \param  gamma
 * \param  A		Input
 * \param  B 		Input
 * \param  C		Output/input
 * \param  n		Rows of A and C
 * \param  p		Columns of A and rows of B
 * \param  m 		Columns of B and C
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_dGEMM(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	const Numeric_t* restrict 	B,
	      Numeric_t* restrict 	C,
	const Int_t 			n,
	const Int_t 			p,
	const Int_t 			m);


/**
 * \brief	This function performs a special kind of matrix operation.
 *
 * In the end this function performs:
 * 	\Mat{C} <- \Mat{A} \cdot (\Mat{A}^{T}) + \gamma \cdot \Mat{C}.
 *
 * All matrices are assumed to be in row major order. Note that this
 * function is very efficient, since it can traverse matrix \Mat{A},
 * in the optional way.
 * Matrix \Mat{A} is expected to be a $n \times m$ matrix and \Mat{C}
 * is expected to be $n \times n$.
 *
 * \param  gamma
 * \param  A		Input
 * \param  C		In/Output
 * \param  n		Numbers of rows of A
 * \param  m		Numbers of columns of A.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_dMtSquare(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	      Numeric_t* restrict 	C,
	const Int_t 			n,
	const Int_t 			m);





/**
 * \brief	This function performs a matrix vector multiplication.
 *
 * The operation that is performed is:
 * 	\Vec{y} <- \Mat{A} \Vec{x} + \gamma \Vec{y}
 *
 * The matrix is a $n \times m$ matrix vector \Vec{y} is a vector of
 * length $n$ and vector \vec{x} is of length $m$.
 * This function returns zero on success and non zero on failure.
 * It will not set the error state.
 *
 * \param  gamma
 * \param  A	Input
 * \param  x	Input
 * \param  y	Input and Output
 * \param  n	Rows of A and length of y.
 * \param  m	Columns of A and length of x.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_dGEMV(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	const Numeric_t* restrict 	x,
	      Numeric_t* restrict 	y,
	const Int_t 			n,
	const Int_t 			m);


/**
 * \brief	This function performs a matrix vector multiplication,
 * 		 where the matrix is transposed.
 *
 * The operation that is performed is:
 * 	\Vec{y} <- \Mat{A}^{T} \Vec{x} + \gamma \Vec{y}
 *
 * Note you still pass \Mat{A} instead of \Mat{A}^{T} to this function.
 * This means the size parameter are the ones of teh untransposed matrix.
 * This function will handle the transposition internaly, by reorder
 * the loops.
 *
 * \param  gamma
 * \param  A	Input
 * \param  x	Input
 * \param  y	Input and Output
 * \param  n	Rows of A and length of x.
 * \param  m	Columns of A and length of y.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_dGEMtV(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	const Numeric_t* restrict 	x,
	      Numeric_t* restrict 	y,
	const Int_t 			n,
	const Int_t 			m);


/*
 * =========================
 * Matrix Utility
 */


/*
 * \brief	This function allows to set a vector to a constat value
 *
 * \param  v 		Vector
 * \param  alpha 	The desired value.
 * \param  n		Length of vector.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_setVec(
	      Numeric_t* restrict 	v,
	const Numeric_t 		alpha,
	const Int_t 			n);


/**
 * \brief	This function writes a range.
 *
 * \param  lowest	Lowest value, value of zerothst index.
 * \param  v		Vector to set.
 * \param  n		Length of the vector.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_setRange(
	const Index_t 		lowest,
	      Index_t* restrict v,
	const Int_t 		n);


/**
 * \brief	Creating a range of integers
 *
 * \param  lowest	Lowest value, value of zerothst index.
 * \param  v		Vector to set.
 * \param  n		Length of the vector.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_iSetRange(
	const Int_t 		lowest,
	      Int_t* restrict 	v,
	const Int_t 		n);


/*
 * \brief	This function allows to set a vector to a constat value
 *
 * This is the version for ints.
 *
 * \param  v 		Vector
 * \param  alpha 	The desired value.
 * \param  n		Length of vector.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_iSetVec(
	      Int_t* restrict 	v,
	const Int_t 		alpha,
	const Int_t 		n);






/**
 * \brief	This function generates A transposed matrix.
 *
 * This operates on the matrices \Math{A} and \Mat{B}.
 * Matrix \Mat{A} is a $n \times m$ matrix and \Mat{B}
 * is a $m \times n $ matrix. Tĥis function will transpose
 * \Mat{A} and store the result in \Mat{B}.
 *
 * \param  A	Input
 * \param  B	Output
 * \param  n	Rows of A and columns of B
 * \param  m	Columns of A and rows of B
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_transpose(
	const Numeric_t* restrict 	A,
	      Numeric_t* restrict 	B,
	const Int_t 			n,
	const Int_t 			m);


/**
 * \brief	This function prints a mtrix to standard out.
 *
 * \param  A
 * \param  r 	rows of the matrix
 * \param  c	cols of the matrix.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_printMatrix(
	const Numeric_t* 	A,
	const Int_t 		r,
	const Int_t 		c);



/*
 * =================================
 * Decomposition and solvers
 */

/**
 * \brief	This function calculates the LU decomposition of A, inplace.
 *
 * For this Crout's algorithm is used. The decomposition is stored directly
 * inside A. The permutation index will be filled is no imput argument.
 * The implementation is based on Numerical Recipies.
 *
 * \param  A		The matrix that is decomposed, is nodified.
 * \param  n		The number of equations and unkowns in the system.
 * \param  perIdx	Permutation order, output.
 * \param  d 		Will be multiplied with +1 if even permutaions are done and -1 when odd.
 *
 * Note if d is not +1 or -1 it will be set to +1.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_luDecomp(
	      Numeric_t* restrict 	A,
	const Int_t 			n,
	      Index_t* restrict 	perIdx,
	      Int_t* 			d);


/**
 * \brief	This function implements the backwards and
 * 		 forwards substitution.
 *
 * This function operates on a decomposed matrix that was generated
 * by the LU decomposition routine.
 * b is the input and contains the rhs, but is overwritten with
 * the solution.
 *
 * \param  LU		The LU decomposition.
 * \param  n 		The size of the problem.
 * \param  perIdx	The permutation index.
 * \param  b		This is the right hand side and the solution.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_luSolve(
	const Numeric_t* restrict 	LU,
	const Int_t 		  	n,
	const Index_t*   restrict 	perIdx,
	      Numeric_t* restrict 	b);







#endif /* end guard */

