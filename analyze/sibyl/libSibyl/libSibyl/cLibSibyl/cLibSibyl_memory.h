/**
 * \brief	This file contains memeory allocation functions
 *
 * Note that all this functions are guaranteed to succeed.
 * If the underliyng allocation of mememory fails panic is called.
 * Note that there is no gurantee that the memory will be set
 * to any specific value.
 *
 * The allocation and deallocation function in this header does
 * not requiere that the runtime system is initialized.
 */
#ifndef CLIBSIBYL_MEMORY_HEADER
#define CLIBSIBYL_MEMORY_HEADER

/* Includes */
#include "./cLibSibyl.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"

/**
 * \brief	This function allocates n bytes.
 *
 * \param  n		Number of bytes to allocate.
 */
extern
CSIBYL_INTERN
void*
cLibSibyl_malloc(
	Index_t 	n)
 CSIBYL_ATTR_MALLOC;


/**
 * \brief	This function performs allocation of n bits, and sets them to zero.
 *
 * This function behaves as the calloc function from teh c standard. This function
 * is guaranteed to succeed.
 *
 * \param  n	Number of bytes to allocate.
 */
extern
CSIBYL_INTERN
void*
cLibSibyl_calloc(
	Index_t 	n)
 CSIBYL_ATTR_MALLOC;



/**
 * \brief	This function allocates a (nRows \times nCols) matrix of numerics.
 *
 * \param nRows		Number of rows.
 * \param nCols		Number of columns.
 */
extern
CSIBYL_INTERN
Numeric_t*
cLibSibyl_dMatrix(
	Index_t 	nRows,
	Index_t 	nCols)
 CSIBYL_ATTR_MALLOC;


/**
 * \brief	This function allocates a array of numerics if nength N.
 */
extern
CSIBYL_INTERN
Numeric_t*
cLibSibyl_dArray(
	Index_t 	N)
 CSIBYL_ATTR_MALLOC;


/**
 * \brief 	This function allocates a (nRows \times nCols) matrix of ints
 *
 * \param nRows		Number of rows.
 * \param nCols		Number of columns.
 */
extern
CSIBYL_INTERN
Int_t*
cLibSibyl_iMatrix(
	Index_t 	nRows,
	Index_t 	nCols)
 CSIBYL_ATTR_MALLOC;



/**
 * \brief	This function allocates a vector of ints of length N.
 */
extern
CSIBYL_INTERN
Int_t*
cLibSibyl_iArray(
	Index_t 	N)
 CSIBYL_ATTR_MALLOC;


/**
 * \brief 	This function allocates a (nRows \times nCols) matrix of indexes
 */
extern
CSIBYL_INTERN
Index_t*
cLibSibyl_idxMatrix(
	Index_t 	nRows,
	Index_t 	nCols)
 CSIBYL_ATTR_MALLOC;


/**
 * \brief	This function allocates a vector of indexes of length N.
 */
extern
CSIBYL_INTERN
Index_t*
cLibSibyl_idxArray(
	Index_t 	N)
 CSIBYL_ATTR_MALLOC;


/**
 * \brief	This function releases the allocated memeory.
 *
 * It also sets the pointer to NULL.
 */
extern
CSIBYL_INTERN
void
cLibSibyl_free(
	void** 	p);


extern
CSIBYL_INTERN
void
cLibSibyl_iFree(
	Int_t** 	p);


extern
CSIBYL_INTERN
void
cLibSibyl_dFree(
	Numeric_t** 	p);

extern
CSIBYL_INTERN
void
cLibSibyl_idxFree(
	Index_t** 	p);


#endif /* end guard */

