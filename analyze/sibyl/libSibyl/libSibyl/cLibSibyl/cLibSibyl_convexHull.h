/**
 * \brief	This file contains the functions to compute a convex hull from a set of points.
 *
 * This file is only concerned with building and managing the convex hull.
 * There are files that also contains operations defined on them.
 */
#ifndef CLIBSIBYL_CONVEXHULL_HEADER
#define CLIBSIBYL_CONVEXHULL_HEADER

/* Includes */
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"

/**
 * \brief	This is a container for storing the convex hull and associated data.
 * \struct	sibyl_convexHull
 *
 * This struct stores the data for the convex hull. It is designed such that it
 * can also operate on a subset. The pointers {x,y}Pos points to the beginning
 * of the marker positions, its length is nTotMarkers.
 *
 * This is acieved by operating on an index array, which stores the order of
 * the points, its length is nMarkers. Its values are index that points into
 * the {x, y}Pos arrays.
 *
 * The convex hull is stored in the hull array. Technically it must have length
 * 2*nMarkers, but in our setting we can assume that nMarkers+1 are sufficient.
 * The hull array is also an indexing array. However its values, are index that
 * points into the {x,y}Pos array and not into the idx array.
 * Note that first an last point are identicall.
 *
 * Note that hull is stored as a series of points, they start at the left most
 * point and then goews counter clockwise through the points. Note that this means
 * that first the lower hull is traversed before the upper hull is traversed.
 *
 */
struct sibyl_convexHull_t
{
	//Positions
	const Numeric_t* 	xPos;		//!< Pointer to the array of the x positions.
	const Numeric_t* 	yPos;		//!< Pointer to the array of the y positions.
	Int_t 			nTotMarkers;	//!< Total number of markers

	//Indexes
	Int_t* 			idx;		//!< Pointer to the list of all indexes, lenght = nMarkers
	Int_t 			nMarkers;	//!< The number of markers in the set.

	Int_t* 			hull;		//!< Pointer to the beginning of the convex hull, length = nMarkers+1.
	Int_t 			nHull;		//!< Markers in the convex hull, indexes into the {x,y}Pos array.
}; //End struct(sibyl_convexHull_t)


/**
 * \brief	This function allocates a sibyl_convexHull struct.
 *
 * Note that this function is guaranteed to succeed, so it will
 * return the pointer to the struct directly. It is possible to
 * pass the index list directly to the function, if the pointer
 * is NULL, all points will be considered, it is also possible
 * pass a non NULL pointer, but for nSubSet a zero.
 *
 * \param  xPos			Pointer to the beginning of the x positions.
 * \param  yPos			Pointer to the beginning of the y positions.
 * \param  nTotMarkers		Total number of markers.
 * \param  subSet		Pointer to the subset index array.
 * \param  nSubSet		Number of points in the sub set.
 */
CSIBYL_INTERN
struct sibyl_convexHull_t*
csibyl_allocCHull(
	const Numeric_t* const restrict 	xPos,
	const Numeric_t* const restrict 	yPos,
	const Int_t 				nTotMarkers,
	const Int_t*     const restrict		subSet,
	const Int_t 				nSubSet)
 CSIBYL_ATTR_MALLOC;


/**
 * \brief	This function will free the convex hull struct.
 *
 * This function is able to deal with only partiall allocated structures.
 * The passed hull will be set to NULL.
 * If this function is not able to succeed an error is created.
 *
 * \param  cHull	Pointer pointer to the convex hull.
 */
CSIBYL_INTERN
void
csibyl_freeCHull(
	struct sibyl_convexHull_t** 	cHull);


/**
 * \brief	This function will compute the convex hull.
 *
 * The result is stored inside the passed hull object. This
 * function will basically manipulate the hull pointer of
 * its argument. Note that the first element and the last
 * element are equal, to get a full cycle.
 * Also the hull start at the left most position.
 *
 * \param  cHull	The hull object that should be manipulated.
 */
extern
CSIBYL_INTERN
Int_t
csibyl_computeConvexHull(
	struct sibyl_convexHull_t* 	cHull);




/*
 * ==========================
 * Internal functions
 */

/**
 * \brief	This function sorts the points, such that the convex
 * 		 hull algorithm can process it.
 *
 * This function will sort the points such that the are sorted with
 * accending x value, the y coordinate is used as a tie breker.
 * This function will not reorder the point arrays, but will only
 * reorder the idx array.
 * Sorting is performed with qsort.
 * This function returns zero on success or an error code on failure.
 * This function will install an error.
 *
 * \param  cHull 	The hull object that is beeing modified.
 */
extern
CSIBYL_INTERN
Int_t
csibyl_sortPoints(
	struct sibyl_convexHull_t* 	cHull);



#endif /* end guard */

