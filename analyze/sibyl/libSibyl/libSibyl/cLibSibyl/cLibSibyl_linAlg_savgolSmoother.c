/**
 * \brief	This file contains the functions that performs the savgol smoothing step.
 */

/* includes */
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_linAlg.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_internal.h"

#include <math.h>
#include <limits.h>
#include <float.h>
#include <assert.h>




/*
 * =========================
 * Static helper functions
 */

/**
 * \brief	This function will compute the number of bytes
 * 		 the matrix needs.
 *
 * This function will ensure that the space is used is a multiply
 * of the chach size.
 */
static
Int_t
cLibSibyl_savgol_spaceDesignMatrix(
	const Int_t 			deg,
	const Int_t 			nL,
	const Int_t 			nR)
{
	const Int_t nNumeric = (deg + 1) * (nL + nR + 1);
	const Int_t nBytes   = nNumeric * sizeof(Numeric_t);
	const Int_t nBytesA  = iRoundUp(nBytes, 32);

	return nBytesA;
};


/**
 * \brief	This function will compute the number of bytes
 * 		 the rhs needs.
 *
 * Note for several reasons the length of the rhs vector is
 * not (deg + 1), which is the length of the transformed vector,
 * but is nL + nR + 1. The reason of this is, because at
 * certain posints of the algorithm, such a vector is needed.
 */
static
Int_t
cLibSibyl_savgol_spaceRHS(
	const Int_t 			deg,
	const Int_t 			nL,
	const Int_t 			nR)
{
	const Int_t nNumeric = (nL + nR + 1);	//Longest vector.
	const Int_t nBytes   = nNumeric * sizeof(Numeric_t);
	const Int_t nBytesA  = iRoundUp(nBytes, 32);

	return nBytesA;
	(void)deg;
};


/**
 * \brief	This function will calculate the number of bytes
 * 		 the pseudo matrix uses.
 *
 * This function will compyl with teh cache lines.
 */
/**
 * \brief	This function will compute the number of bytes
 * 		 the rhs needs.
 *
 * This function will ensure that the space is used is a multiply
 * of the chach size.
 */
static
Int_t
cLibSibyl_savgol_pseudoMatrix(
	const Int_t 			deg,
	const Int_t 			nL,
	const Int_t 			nR)
{
	const Int_t nNumeric = (deg + 1) * (deg + 1);
	const Int_t nBytes   = nNumeric * sizeof(Numeric_t);
	const Int_t nBytesA  = iRoundUp(nBytes, 32);

	return nBytesA;
	(void)nL; (void)nR;
};


/**
 * \brief	This function will compute the number of bytes
 * 		 the permutation vector will need.
 *
 * This function will ensure that the space is used is a multiply
 * of the chach size.
 */
static
Int_t
cLibSibyl_savgol_spacePerIdx(
	const Int_t 			deg,
	const Int_t 			nL,
	const Int_t 			nR)
{
	const Int_t nNumeric = (deg + 1);
	const Int_t nBytes   = nNumeric * sizeof(Index_t);
	const Int_t nBytesA  = iRoundUp(nBytes, 32);

	return nBytesA;
	(void)nL; (void)nR;
};


/*
 * ======================
 * Functions
 */

Index_t
cLibSibyl_savgolWorkMemorySize(
	const Int_t 			deg,
	const Int_t 			nL,
	const Int_t 			nR,
	const Bool_t 			repError)
{
	/* This is a macro that helps to install the library better */
#	define INSTALL(e, m) if(IS_TRUE(repError) ) {CSIBYL_RT_INSTALL_ERROR( (e), (m) );} return (e)

	if(deg < 1)
		{INSTALL(-51001, "Degree is too small.");};
	if(nL < 0)
		{INSTALL(-51002, "nL is negative.");};
	if(nR < 0)
		{INSTALL(-51003, "nR is negative.");};
	if((nL + nR + 1) <= 3)
		{INSTALL(-51004, "window size is too small.");};

	const Int_t nMat = cLibSibyl_savgol_spaceDesignMatrix(deg, nL, nR);
	const Int_t nPat = cLibSibyl_savgol_pseudoMatrix     (deg, nL, nR);
	const Int_t nRhs = cLibSibyl_savgol_spaceRHS         (deg, nL, nR);
	const Int_t nIdx = cLibSibyl_savgol_spacePerIdx      (deg, nL, nR);

	if(nMat <= 0)
		{INSTALL(-51005, "Space for designe matrix is too small.");};
	if(nPat <= 0)
		{INSTALL(-51005, "Space for reduced matrix is too small.");};
	if(nRhs <= 0)
		{INSTALL(-51006, "Space for rhs is too small.");};
	if(nIdx <= 0)
		{INSTALL(-51007, "Space for the index vector is too small.");};

	Index_t totSize = ((Index_t)nMat + (Index_t)nPat) + ((Index_t)nRhs + (Index_t)nIdx);	/* calculating the total size */
	if(totSize <= 0)
		{INSTALL(-51008, "Total space is too small.");};


	return totSize;
#	undef INSTALL
}; /* end: calculate size */



void*
cLibSibyl_savgolWorkMemory(
	const Int_t 			deg,
	const Int_t 			nL,
	const Int_t 			nR)
{
	/* calculating the size to allocate
	 * also try to install the error if fails. */
	Index_t totSize = cLibSibyl_savgolWorkMemorySize(deg, nL, nR, TRUE);

	/* This function is guranteed to succedd, so we have to call panic.
	 * It could be that at some day, the runtime is able to handle this.
	 * So that installing an error, can survive */
	if(totSize <= 0)
		{cLibSibylRT_panic("Calculation for the working space in savgol failed.");};

	void* p = cLibSibyl_malloc(totSize);

	return p;
}; /* End: allocating functions */


Int_t
cLibSibyl_savgolSmoothStep(
	const Numeric_t* restrict 	x,
	const Numeric_t* restrict 	y,
	const Int_t 			n,
	      Numeric_t* restrict 	yHat,
	const Int_t 			deg,
	const Int_t 			dy,
	const Int_t 			nL,
	const Int_t 			m,
	const Int_t 			nR,
	      void*      restrict 	w)
{
	if(x == NULL)
		{return 50000;};
	if(y == NULL)
		{return 50001;};
	if(yHat == NULL)
		{return 50002;};
	if(w == NULL)
		{return 50004;};
	if(deg < 1)		/* we requiere deg at least one */
		{return 50003;};
	if(n <= 0)
		{return 50005;};
	if(nL < 0 || nR < 0)
		{return 50006;};
	if(nL == 0 && nR == 0)
		{return 50007;};
	if(dy < 0)
		{return 50008;};
	if(dy > deg)
		{return 50009;};
	if(m < 0 || m >= n)
		{return 50010;};

	const Int_t startRange = m - nL;	/* index of left most value that is processed */
	const Int_t  endeRange = m + nR + 1;	/* past the end index of the right most value */
	const Int_t wSize      = nL + nR + 1;	/* window size */
	const Int_t mLoc       = nL + 1;	/* this is m expressed in local coordinates */

	if(startRange < 0)
		{return 50011;};
	if(endeRange > n)
		{return 50012;};
	if(wSize < (deg + 1))		/* otherwhise matrix not solvable, allows for square matrix */
		{return 50013;};

	/* decompose the working memory */
	const Int_t nMat = cLibSibyl_savgol_spaceDesignMatrix(deg, nL, nR);
	const Int_t nPat = cLibSibyl_savgol_pseudoMatrix     (deg, nL, nR);
	const Int_t nRhs = cLibSibyl_savgol_spaceRHS         (deg, nL, nR);

	Numeric_t* restrict Tt     = (Numeric_t*)(w		        );	/* transpose designe matrix */
	Numeric_t* restrict B	   = (Numeric_t*)(w + nMat              );	/* this will contain the matrix to inv */
	Numeric_t* restrict rhs    = (Numeric_t*)(w + nMat + nPat       );	/* the rhs and solution     */
	Index_t*   restrict perIdx = (  Index_t*)(w + nMat + nPat + nRhs);	/* the permutation index    */


	/* Building of the designe matrix.
	 *
	 * Inside the window, which we assume starts at index 0 and goes up
	 * to index p-1 (p ~ wSize), in the following all indexes are relative
	 * to that window. $m$ is the index of the observation we want to smooth.
	 * We can write the polynomial of degree d
	 * as following:
	 * 	a_{0} + a_{1} * t_{(i)} + a_{2} * t_{(i)}^{2} + ... + a_{d} * t_{(i)}^{d}
	 * Where
	 * 	t_{(i)} := x_i - x_m
	 *
	 * For each datapoint we have a different polynomial, thus we have $p$ many polynomial.
	 * $t_{(i)}$ is known, only the coefficients are unknown. On the rihs of the polynomials
	 * we have the value $y_i$.
	 * So we can write this as matrix in the following form:
	 * 	\Mat{T} \cdot \Vec{a} = \vec{y}
	 *
	 * With $\Vec{a}_{l} = a_l$, with $l = 0_d$. $\Vec{y}_i = y_i$.
	 * And the designe matrix:
	 * 	\Mat{T}_{i, j} = t_{(i)}^{j}
	 *
	 * For very strange reasons, we will not build the designematrix itself. Rather we will
	 * build the transpose of the design matrix. The reason for thsi is, that we can fast
	 * multiply it with itself.
	 * Building is not so efficient, but I think that it will pay off in the later steps.
	 */
	const Int_t nRowsTt  = deg + 1;	/* rows of transposed designe matrix */
	const Int_t nColsTt  = wSize;	/* columns of the designe matrix, equal the window size */
	const Numeric_t xRef = x[m];	/* This is the reference x coordinate; point where we want to smooth. */

	/* We now makes the firs row of Tt.
	 * This is one for all entries */
	for(Int_t l = 0; l != nColsTt; ++l)
		{Tt[l] = 1.0;};


	/* Now we will compute the second row.
	 * This is just t_i := x_i - x_m */
	for(Int_t i = 0; i != nColsTt; ++i)
	{
		const Int_t     globI = startRange + i;	/* this is the global index */
		const Numeric_t    xI = x[globI];	/* load the x position */
		const Numeric_t    tI = xI - xRef;	/* this is the t value */

		Tt[nColsTt + i]	= tI;			/* store the value in the designe matrix */
	}; /* end for(i): computing the second row, actuall distances */


#	if 0
	/* Now we compute the sum of all distances
	 * We do this by going from m to the two ends */
	Numeric_t posSumT = 0.0;	/* sum of all positive distances */
	Numeric_t negSumT = 0.0;	/* sum of all negative distances */

	for(Int_t l = 0   ; l != mLoc ; ++l)
		{negSumT += Tt[nColsTt + l];};
	for(Int_t l = mLoc; l != wSize; ++l)
		{posSumT += Tt[nColsTt + l];};

	/* get the full sum */
	const Numeric_t fullTSum = posSumT - negSumT;
	if(fullTSum <= 0.0)
		{return 50014;};
	const Numeric_t meanTValue = fullTSum / wSize;		/* the mean value is used as a scaling base */

	/* Now we caluclating the scaling coefficient.
	 * Note that the scaling now also depends on the row.
	 * But it is possible to write it as a diagonal matrix
	 * the scalling for a row is this scaling constant to
	 * the power of the row id. */
	const Numeric_t TtScalingBase = 1.0 / meanTValue;
	if(!isfinite(TtScalingBase))
		{return 50015;};


	/* Now we will apply the scaling to the seind row
	 * linear coefficient. In the first row the scaling
	 * constant is 1. */
	const Int_t endFirstTwoRows = 2 * nColsTt;
	for(Int_t l = nColsTt; l != endFirstTwoRows; ++l)
		{Tt[l] *= TtScalingBase;};

	TODO UNDO SCALLING
#	endif

	/* Now we will handle the last rows.
	 * The scaling constant is implicitly applied */
	for(Int_t rTt = 2; rTt != nRowsTt; ++rTt)
	{
		const Int_t startRowIdxTt     = rTt * nColsTt;			/* start of the row we handle now */
		const Int_t startLastRowIdxTt = startRowIdxTt - nColsTt;	/* start of the last row */
		const Int_t startOfTRowIdxTt  = nColsTt;			/* start of the row where t is pure */

		/* Iterating through the different t */
		for(Int_t l = 0; l != nColsTt; ++l)
		{
			const Numeric_t t_l    = Tt[ startOfTRowIdxTt + l];		/* this is the pure t value */
			const Numeric_t t_last = Tt[startLastRowIdxTt + l];		/* this the t value from before */
			const Numeric_t thisT  = t_last * t_l;

			Tt[startRowIdxTt + l]  = thisT;					/* write back the new t */
		}; /* End for(l): */
	}; /* End for(rTt): iterating through the rows */


	/* We will use a least square solution to get the coefficient.
	 * We can write the equation system we have in matrix form as:
	 * 	\Mat{T} \cdot \Vec{a} = \Vec{y}
	 *
	 * Applying least square gives us:
	 * 	\Mat{T}^{T} \cdot \Mat{T} \cdot \Vec{a} = \Mat{T}^{T} \cdot \Vec{y}
	 * 	\Mat{B} \cdot \Vec{a} = \hat{\Vec{y}}
	 *  \Mat{B}       := \Mat{T}^{T} \cdot \Mat{T}
	 *  \hat{\Vec{q}} := \Mat{T}^{T} \cdot \Vec{y}
	 *
	 * The scaling is moved inside \Vec{a} and will then be reverted. */

	/* Now we will create \hat{\Vec{q}}. This will involve different steps.
	 * First we will load all y values into the rhs vector. Then we will
	 * multiply it with \Mat{T}^{T}, this result will be temporary stored
	 * in the B matrix and then moved back. */

	for(Int_t k = 0; k != nColsTt; ++k)	/* extracting the observations */
	{
		const Int_t     gK  = startRange + k;	/* the global index of the observation */
		const Numeric_t obs = y[gK];		/* load the observation */

		rhs[k] = obs;				/* store the observation in the matrix */
	}; /* end for(k): */

	/* Now we perform the matrix vector multiplication, we use store the
	 * result in the B matrix temporaraly */
	const Int_t mvm_TtY_status = cLibSibyl_dGEMV(
			0.0,			/* set value in B to zero */
			Tt, 			/* matrix */
			rhs, 			/* vector to multiply with */
			B,			/* target for the multiplication */
			nRowsTt, nColsTt);	/* size of the matrix */
	if(mvm_TtY_status != 0)
		{return mvm_TtY_status;};

	/* Copying the result back */
	for(Int_t k = 0; k != nRowsTt; ++k)
		{rhs[k] = B[k];};


	/* Now we compute \Mat{B}.
	 *   The function computes $\Mat{A} * \Mat{A}^{T}$
	 *   But since our \Mat{A} is \Mat{T}^{T} we actually
	 *   have $\Mat{T}^{T} * \Mat{T}. */
	const Int_t BcompStatus = cLibSibyl_dMtSquare(
			0.0, 			/* mutliply all in B with */
			Tt,			/* matrix to square */
			B,			/* target */
			nRowsTt, nColsTt);	/* size of matrix */
	if(BcompStatus != 0)
		{return BcompStatus;};


	/* Now we compute a decomposition of the matrix B
	 * This is done by the LU solver */
	Int_t d = 1;
	const Int_t luDcompStatus = cLibSibyl_luDecomp(
			B, nRowsTt, perIdx, &d);
	if(luDcompStatus != 0)
		{return luDcompStatus;};


	/* Now we compute the solution, this will be
	 * placed in the right hand */
	const Int_t luSolveStatus = cLibSibyl_luSolve(
			B, nRowsTt, perIdx, rhs);
	if(luSolveStatus != 0)
		{return luSolveStatus;};

	/* Now we have to extract the value for the solution vector. */
	Numeric_t smoothedYValue = NAN;
	if(dy == 0)
	{
		/* we are interested not in the derivative */
		smoothedYValue = rhs[0];
	}
	else
	{
		/* we are interested in the derivative, we also
		 * have to include the faculty, */
		const Numeric_t facDY = dFac(dy);
		if(!isfinite(facDY))
			{return 50015;};
		const Numeric_t iFacDY = 1.0 / facDY;
		const Numeric_t c_v    = rhs[dy];	/* the calculated coefficient */

		/* correct for the derivative */
		smoothedYValue = c_v * iFacDY;
	}/* end if: load value */

	if(!isfinite(smoothedYValue))
		{return 50016;};

	/* write it back */
	yHat[m] = smoothedYValue;

	return 0;
	(void)mLoc;
}; /* End: smoother step function */



