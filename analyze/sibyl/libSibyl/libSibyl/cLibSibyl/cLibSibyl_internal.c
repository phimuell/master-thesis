/**
 * \brief	This file contains the implementation of several internal functions.
 */

/* Includes */
#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_internal.h"
#include "./cLibSibyl_runtime.h"

#include <math.h>



Int_t
cLibSibyl_diff_(
	Numeric_t* restrict 	x,
	Int_t 			N,
	Numeric_t* restrict 	d)
{
	Int_t i, Nend;
	Numeric_t x_ip1, x_i, diff;

	CSIBYL_RT_FUNC_START;

	if(x == NULL)
		{cLibSibylRT_exit(1);};
	if(N < 2)
		{cLibSibylRT_exit(2);};
	if(d == NULL)
		{cLibSibylRT_exit(3);};

	Nend = N -1 ;
	for(i = 0; i != Nend; ++i)
	{
		x_ip1 = x[i+1];		csibyl_assert2(isfinite(x_ip1), -2);
		x_i   = x[i  ];		csibyl_assert2(isfinite(x_i  ), -3);

		diff = x_ip1 - x_i;	csibyl_assert2(diff > 0       , -4);

		d[i] = diff;
	}; /* End for(i): */

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
};/* End: sibyl diff */


Int_t
cLibSibyl_strLen_(
	const Char_t* const 		s,
	Int_t 		 		n_)
{
	if(s == NULL)
		{return 0;};
	if(n_ == 0)
		{return -1;};

	uInt_t n = (n_ < 0) ? (uInt_t)(-1) : (uInt_t)(n_);

	for(uInt_t i = 0; i != n; ++i)
	{
		if(s[i] == '\0')
			{return i;};
	};

	return -2;
}; /* End: strLen */

