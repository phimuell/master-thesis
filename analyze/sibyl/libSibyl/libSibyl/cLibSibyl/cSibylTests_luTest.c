/**
 * \brief	This file implements a test for the lu solver
 */

#include "./cLibSibyl_linAlg.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"
#include "./cSibylTests.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

Int_t
cSibylTest_luDecomp(void)
{
#if 1
	Numeric_t A[] = {-842,  768,   213, -318,
			  381,  633,  -666, -500,
			 -970, -709,   940, -873,
			  102, -811, -1000,  625 };
#else
	Numeric_t A[] = {1, 0, 0, 0,
		         0, 1, 0, 0,
		         0, 0, 1, 0,
		         0, 0, 0, 2000};

#endif
	Numeric_t b[] = {1, 0, 1000, 2000};
	Numeric_t sol[] = {-1.24679, -1.13430, -1.47261, -0.42457};
	Index_t perIdx[4];
	Int_t d = 0;

	Int_t e = cLibSibyl_luDecomp(A, 4, perIdx, &d);

	if(e != 0)
	{
		printf("There was an error in the decomposition %d\n", e);
	};

	e = cLibSibyl_luSolve(A, 4, perIdx, b);
	if(e != 0)
	{
		printf("There was an error in the elimination %d\n", e);
		abort();
	};

	printf("\n\nComputed solution:\n");
	cLibSibyl_printMatrix(b, 1, 4);

	printf("\n\nExact solution:\n");
	cLibSibyl_printMatrix(sol, 1, 4);


	return 0;
}; /* END lu decomp */




