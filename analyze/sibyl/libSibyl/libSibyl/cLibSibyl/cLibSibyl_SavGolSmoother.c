/**
 * \brief	This function contains the code for performing the smoothing with the SavGol smoother.
 *
 * Note that this function contains the facade implementation of the process.
 * The file cLibSibyl_linAlg_savgolSmoother.c only contains the function to perform one step.
 */

/* includes */
#include "./cLibSibyl.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_linAlg.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_internal.h"
#include "./cLibSibyl_assert.h"

#include <math.h>
#include <limits.h>
#include <float.h>
#include <assert.h>


Int_t
cLibSibyl_SavGolSmooth(
	const Numeric_t* const restrict 	x,
	const Numeric_t* const restrict 	y,
	      Numeric_t* const restrict 	yHat,
	const Int_t 				dy,
	const Int_t 				deg,
	const Int_t 				wSize,
	const Numeric_t 			mR,
	const Int_t 				n,
	const Int_t 				begin,
	const Int_t				end,
	const Int_t 				doCopy,
	const Int_t 				noSkew)
{
	Int_t m;		/* loop indexes                                      */
	void* w = NULL;		/* Working memory for the step                       */
	Int_t nL_normal;	/* this is the window size to the left, by default   */
	Int_t nR_normal;	/* This is the window size to the right, by default  */
	Int_t nL_m, nR_m;	/* wondow sizes right and left for the current m     */
	Int_t leftP, rightP;	/* points to the left and right                      */
	Int_t wShift;		/* How much we have to adjust when correcting window */
	Int_t savGolStatus;	/* Status of the sav gol error 			     */
	Index_t wSpaceB;	/* size of the working space in bytes                */
	Int_t compBegin,	/* begin and end, if no noSkewing is requested 	     */
	      compEnd;


	CSIBYL_RT_FUNC_START;	/* start the function */
	CSIBYL_RT_IS_INIT;

	if(x == NULL)
		{cLibSibylRT_exit2(1, "The x array is NULL.");};
	if(y == NULL)
		{cLibSibylRT_exit2(2, "The y array is NULL.");};
	if(yHat == NULL)
		{cLibSibylRT_exit2(3, "The yHat array is NULL.");};
	if(dy < 0)
		{cLibSibylRT_exit2(10000 - dy, "The dy value is negative.");};
	if(deg <= 1)
		{cLibSibylRT_exit2(4, "The degree must be at least 2.");};
	if(deg <= dy)
		{cLibSibylRT_exit2(5, "Derivative is larger than polynomial degree.");};
	if(wSize < 3)
		{cLibSibylRT_exit2(6, "The window size must be at least 3.");};
	if(deg >= wSize)
		{cLibSibylRT_exit2(7, "The degree is larger than the window size. Undetermined system.");};
	if((!isfinite(mR)) || (mR < 0.0) || (mR > 1))
		{cLibSibylRT_exit2(8, "The mR value is invalid.");};
	if(n <= 0)
		{cLibSibylRT_exit2(9, "The length of the array is too short.");};
	if((begin < 0) || (begin >= n))
		{cLibSibylRT_exit2(10, "The begin parameter is out of range.");};
	if((end <= 0) || (end > n))	/* equality is alowed above, but prohibited below */
		{cLibSibylRT_exit2(11, "The end parameter is out of range.");};
	if(begin >= end)
		{cLibSibylRT_exit2(12, "The begin parameter is larger than the end parameter.");};
	if((end - begin) < (2 * wSize))
		{cLibSibylRT_exit2(13, "Supplied range is too small for the window.");};

	/* this are the extension we use by default */
	nL_normal = (wSize - 1) * mR;
	nR_normal = wSize - nL_normal - 1;	/* minus one is for acounting for the middle point */

	/* test if we have to adjust for non seweing */
	if(noSkew != 0)
	{
		/* we do not want skewing */
		compBegin = begin + nL_normal;
		compEnd   = end   - nR_normal;
	}
	else
	{
		/* we do allow skewing */
		compBegin = begin;
		compEnd   = end;
	};

	if(!(compBegin < compEnd))
		{cLibSibylRT_exit2(14, "No skewing leads to an empty range.");};


	/* Note: for calculating the working array it is more important,
	 * to know how large the window is than how large nL and nR are
	 * so the array remains walid even if something changed */
	wSpaceB = cLibSibyl_savgolWorkMemorySize(deg, nL_normal, nR_normal, TRUE);	/* try installing an error */
	if(wSpaceB <=  0)
		{cLibSibylRT_exit(15);};	/* error is installed, error id, will not propaate because of this, but the error that is returned is will be the set one here */


	/* Allocate the memory that we use during the iterations
	 *  Guaranetted to work */
	w = cLibSibyl_savgolWorkMemory(deg, nL_normal, nR_normal);


	/* Now we iterate value for value through the position array
	 * and smooth each point */
	for(m = compBegin; m != compEnd; ++m)
	{
		/* This is was we want, that the request about the size
		 * of the window could be fullfiled.
		 * We know that this is true if we selected the no skewing
		 * parameter, and if we are inside the middle of the domain */
		nL_m = nL_normal;
		nR_m = nR_normal;

		if(noSkew == 0)		/* skewing is enabled, so we have to adapt */
		{
			/* We allow skewing, so we must adapt them.
			 * Note in this case comp{Begin, End} is the
			 * same as the begin, end variable. */

			/* We now calculat the space we have aviable in boath direction
			* This is like the maximal possible window size */
			leftP  = m   - begin;			/* how much space to the left we have, must use real begin, since compBegin is artificial restricted, but range is usable */
			rightP = end - m - 1;			/* maximal extension to the right; -1 for accounting for zero based indexing, and that end is the past the end index */

			/* Because of the guarantees we have about the domain, we know that only one
			 * side is violated, and that even a shifting will not hurt this.
			 * Thus we will check which one is violated and shift it */
			if(leftP < nL_normal)
			{
				/* On the left we have less space, than we would need, so we
				 * must shift everything to the right */
				wShift = nL_normal - leftP;	/* is positive */

				nL_m -= wShift;		/* shrink the left side of the window */
				nR_m += wShift;		/* increase the right side of the window */

					csibyl_assert2(rightP >= nR_normal, 16);	/* Ensure that we have plenty of space on the right */
			}
			else if(rightP < nR_normal)
			{
				/* We have plenty of space at the left, because we have not taken the above
				 * branch, but not at the right. So we have to shift the window to the left */
				wShift = nR_normal - rightP;

				nL_m += wShift;		/* increase window to the right */
				nR_m -= wShift;		/* decrease window size to the right */
			} /* ende elif */

			/* If we have not taken a branch then we know that we are save
			 * thus we are inside the middle of the domain */
		}; /* End if: adjusting the window extension */


		csibyl_assert2(nL_m >= 0, 17);		/* make some tests; == is allowed at the edges */
		csibyl_assert2(nR_m >= 0, 18);
		csibyl_assert2(begin <= (m - nL_m), 19);
		csibyl_assert2((m + nR_m) < end   , 20);	/* note we will access the entry (m + nR_m) */
		csibyl_assert2((nL_m + nR_m + 1) == wSize, 21);
		csibyl_assert2(wSpaceB == cLibSibyl_savgolWorkMemorySize(deg, nL_m, nR_m, FALSE), 22); 	/* <= is not possible because of the alignments to cache lines */
		csibyl_assert2((noSkew != 0) ? ((nR_m == nR_normal) && (nL_m == nL_normal)) : 1, 23);		/* no skew forces this */

		/* Performing the smoothing */
		savGolStatus = cLibSibyl_savgolSmoothStep(
				x, y, n,	/* observation */
				yHat,		/* solution */
				deg,		/* degree */
				dy,		/* no derivative */
				nL_m, m, nR_m,	/* smoothing location */
				w);
		if(savGolStatus != 0)
			{cLibSibylRT_exit2(savGolStatus, "The SavGol smoother implementation failed.");};
	}; /* end for(m) */


	/* Handle the ignored ranges */
	if(doCopy == 0 || dy > 0)
	{
		/* simply set everything to zero */
		for(m = 0; m != compBegin; ++m)
			{yHat[m] = 0.0;};
		for(m = compEnd; m != n; ++m)
			{yHat[m] = 0.0;};
	}
	else
	{
		/* copy the values */
		for(m = 0; m != compBegin; ++m)
			{yHat[m] = y[m];};
		for(m = compEnd; m != n; ++m)
			{yHat[m] = y[m];};
	};


	/*
	 * Clean up */
	CSIBYL_RT_FUNC_CLEANUP:
	cLibSibyl_free(&w);

	CSIBYL_RT_FUNC_RETURN;
}; /* End: SavGolFullSMoother */



