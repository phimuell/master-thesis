/**
 * \brief	Implements the allocation functions
 */

#define _POSIX_C_SOURCE 200112L

/* Includes */
#include "./cLibSibyl.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_runtime.h"

#include <stdlib.h>
#include <string.h> 	/* memset */

void*
cLibSibyl_malloc(
	Index_t 	n)
{
	void* p;
	posix_memalign(((void**)(&p)), 32, n);

	if(p == NULL)
		{cLibSibylRT_panic("Failed to allocate memeory.");};

	return p;
}; /* end: malloc */


void*
cLibSibyl_calloc(
	Index_t 	n)
{
	/* call malloc that is guarantted to succeed */
	void* p = cLibSibyl_malloc(n);

	/* set the memory to zero */
	memset(p, 0, n);

	return p;
}; /* end: calloc */


Numeric_t*
cLibSibyl_dMatrix(
	Index_t 	nRows,
	Index_t 	nCols)
{
	Numeric_t* p;
	posix_memalign(((void**)(&p)), 32, nRows * nCols * sizeof(Numeric_t));

	if(p == NULL)
		{cLibSibylRT_panic("Failed to allocate memeory.");};
	return p;
}; /* END: Allocate it */


Numeric_t*
cLibSibyl_dArray(
	Index_t 	N)
{
	return cLibSibyl_dMatrix(N, 1);
};


Int_t*
cLibSibyl_iMatrix(
	Index_t 	nRows,
	Index_t 	nCols)
{
	Int_t* p;
	posix_memalign(((void**)(&p)), 32, nRows * nCols * sizeof(Int_t));

	if(p == NULL)
		{cLibSibylRT_panic("Failed to allocate memeory.");};

	return p;
}; /* END: Allocate it */


Int_t*
cLibSibyl_iArray(
	Index_t 	N)
{
	return cLibSibyl_iMatrix(N, 1);
};


Index_t*
cLibSibyl_idxMatrix(
	Index_t 	nRows,
	Index_t 	nCols)
{
	Index_t* p;
	posix_memalign(((void**)(&p)), 32, nRows * nCols * sizeof(Index_t));

	if(p == NULL)
		{cLibSibylRT_panic("Failed to allocate memeory.");};

	return p;
}; /* END: Allocate it */


Index_t*
cLibSibyl_idxArray(
	Index_t 	N)
{
	return cLibSibyl_idxMatrix(N, 1);
};



void
cLibSibyl_free(
	void** 	p)
{
	if(p == NULL)
		{return;};

	free(*p);
	*p = NULL;

	return;
};//End free


void
cLibSibyl_iFree(
	Int_t** 	p)
{
	cLibSibyl_free((void**)(p));
	return;
};


void
cLibSibyl_dFree(
	Numeric_t** 	p)
{
	cLibSibyl_free((void**)(p));
	return;
};


void
cLibSibyl_idxFree(
	Index_t** 	p)
{
	cLibSibyl_free((void**)(p));
	return;
};


