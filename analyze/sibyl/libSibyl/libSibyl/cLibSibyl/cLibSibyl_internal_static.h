/**
 * \brief	This file contains the implementation of the static functions that are internal.
 *
 * Static functions must be defined in a compile unit, if they are needed. And GCC generate an
 * error if the static function is declared, but not defined, even if said function is not used.
 *
 * Thus this file conatins the definitions of the static internal function. This file is
 * automatically included by the header, such that the definition is avaiable, even if not needed.
 * Note that some, esspecially longer functions are implemented in theier own file, that is also
 * automatically included.
 *
 * A special note to some math functions. They are static, but not declared in the main internal
 * header, but only here. This is done to keep the header clean.
 */
#ifndef CLIBSIBYL_INTERNALS_STATIC
#define CLIBSIBYL_INTERNALS_STATIC

/* Includes */
#include "./cLibSibyl_internal.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_assert.h"

#include <math.h>

/*
 * ==========================
 * Helper functions for math
 * They are static but not in the designated static file.
 */

/**
 * \brief	This function rounds upwards.
 *
 * It will returns the smallest number that is larger or
 * equal n, such that its modulo with p is zero.
 * Passing 0 as p, is invalid.
 * Note that only possitive values are handled.
 *
 * \param  n		The value to maximize.
 * \param  p		The divisor.
 */
static
CSIBYL_ATTR_UNUSED
Int_t
iRoundUp(
	const Int_t 	n,
	const Int_t 	p)
{
#	ifndef NDEBUG
	if(p == 0 || n < 0)
		{cLibSibylRT_panic(NULL);};
# 	endif

	const Int_t m = n % p;
	if(m == 0)
		{return n;};

	const Int_t nn = n + p - m;	/* adjust */

	return nn;
}; /* End: round up */


/**
 * \brief	This function returns the factoriel of n.
 *
 * Note that n is an integer, wich must not be negative
 * but the result is a float. This function does not check
 * if an overflow occures.
 *
 * \param  n
 */
static
CSIBYL_ATTR_UNUSED
Numeric_t
dFac(
	const Int_t 	n)
{
	if(n < 0)
		{return NAN;};
	if(n == 0 || n == 1)
		{return 1.0;};

	Index_t f = 1;
	for(Int_t i = 2; i <= n; ++i)
		{f *= i;};

	return ((Numeric_t)(f));
};


/**
 * \brief	returns the absolute value of a
 */
static
CSIBYL_ATTR_UNUSED
Numeric_t
dAbs(
	Numeric_t a)
{
	return (a < 0.0) ? (-a) : (a);
};


static
CSIBYL_ATTR_UNUSED
Int_t
iAbs(
	Int_t a)
{
	return (a < 0) ? (-a) : (a);
};

static
CSIBYL_ATTR_UNUSED
Index_t
idxAbs(
	Index_t a)
{
	return (a < 0) ? (-a) : (a);
};


/**
 * \brief	Returns the minimum of the two values.
 *
 * \param  a
 * \param  b
 */
static
CSIBYL_ATTR_UNUSED
Int_t
iMin(
	Int_t 		a,
	Int_t 		b)
{
	return (a < b) ? a : b;
};


static
CSIBYL_ATTR_UNUSED
Numeric_t
dMin(
	Numeric_t 	a,
	Numeric_t 	b)
{
	return (a < b) ? a : b;
};


static
CSIBYL_ATTR_UNUSED
Index_t
idxMin(
	Index_t 	a,
	Index_t		b)
{
	return (a < b) ? a : b;
};


/**
 * \brief	Returns the maximum of the two values.
 *
 * \param  a
 * \param  b
 */
static
CSIBYL_ATTR_UNUSED
Int_t
iMax(
	Int_t 		a,
	Int_t 		b)
{
	return (a > b) ? a : b;
};


static
CSIBYL_ATTR_UNUSED
Numeric_t
dMax(
	Numeric_t 	a,
	Numeric_t 	b)
{
	return (a > b) ? a : b;
};


static
CSIBYL_ATTR_UNUSED
Index_t
idxMax(
	Index_t 	a,
	Index_t		b)
{
	return (a > b) ? a : b;
};


#endif /* end guard */



/* ============================
 * Include the implementation of the function
 * that have a seperate file for implementation. */

#include "./cLibSibyl_internal_static_findAssocIndex.h"


