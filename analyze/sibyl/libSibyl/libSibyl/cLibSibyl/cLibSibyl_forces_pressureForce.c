/**
 * \brief	This file contains the implementation of the pressure force functionality.
 */

/* Includes */
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_convexHull.h"
#include "./cLibSibyl_forces.h"
#include "./cLibSibyl_assert.h"

#include <math.h>
#include <stdio.h>

/**
 * \brief	This function can compute the pressure value at a point.
 *
 * Note that some assumptions are made, for example, the function assumes
 * equal grid spacing, also the external pressure grid is assued.
 *
 * The pressure matrix is assumed to be in row major order. Note that inside EGD they
 * are column major order, but inside CSIBYL, they are row major order. Also we assume
 * that the number or colums is equal the number of x grid points and number of rows
 * is equal y grid points.
 *
 */
static
Int_t
csibyl_interpolatePressure(
	const Numeric_t* const restrict 	pressure,
	const Numeric_t* const restrict 	xPressNode,
	const Numeric_t* const restrict 	yPressNode,
	const Int_t 				nPressNodeX,
	const Int_t 				nPressNodeY,
	const Numeric_t 			dx,
	const Numeric_t 			dy,
	const Numeric_t 			x,
	const Numeric_t 			y,
	      Numeric_t* 			p);



/**
 * \brief	This function computes the pressure force on the convex hull.
 *
 * See also cLibSibyl_computePressureForce() function for a better description.
 *
 * \param  pressure	Pointer to the pressure field.
 * \param  xPressNode	Pointer to the x coordinates of the pressure point.
 * \param  yPressNode	Pointer to the y coordinates of the pressure point.
 * \param  nPressNodeX	Number of pressure nodes in x direction.
 * \param  nPressNodeY	Number of pressure nodes in y direction.
 * \param  pForce	Array of length two to the pressure force.
 */
Int_t
csibyl_convHull_computePressureForce(
	struct sibyl_convexHull_t* 		cHull,
	const Numeric_t* const restrict 	pressure,
	const Numeric_t* const restrict 	xPressNode,
	const Numeric_t* const restrict 	yPressNode,
	const Int_t 				nPressNodeX,
	const Int_t 				nPressNodeY,
	      Numeric_t* const restrict 	pForce)
{
	const Numeric_t *xPos = NULL, *yPos = NULL;	/* alias pointers int the hull */
	Int_t* hull = NULL;
	Int_t  nTotMarkers = -1, nHull = -1;	 	/* sizes */
	Int_t  i;					/* iterator */
	Int_t  idx_1, idx_2;				/* local variables */
	Numeric_t x_1, x_2, y_1, y_2;
	Numeric_t p_1, p_2;				/* pressure value at the points */
	Numeric_t dx, dy;				/* grid spacing */
	Numeric_t pF_1 = 0.0, pF_2 = 0.0;		/* summation variables for the force */
	Numeric_t p_m;					/* pressure at midpoint */
	Numeric_t d_x, d_y;				/* move vector */
	Numeric_t n_x, n_y;				/* non normalizing vector that points outwards */
#	define SUM_N 8					/* number of partial sums that are used */
	Numeric_t pFPart_x[SUM_N];			/* Partial sum array for x component */
	Numeric_t pFPart_y[SUM_N];			/* Partial sum array for y component */


	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	csibyl_assert2(cHull != NULL, 94001);
	csibyl_assert2(xPressNode != NULL, 94002);
	csibyl_assert2(yPressNode != NULL, 94003);
	csibyl_assert2(pressure   != NULL, 94004);
	csibyl_assert2(0 < nPressNodeX, 94005);
	csibyl_assert2(0 < nPressNodeY, 94006);
	csibyl_assert2(cHull->nHull >= 2, 94101);
	csibyl_assert2(cHull->nHull <= (cHull->nMarkers + 1), 94102);
	csibyl_assert2(cHull->hull[0] == cHull->hull[cHull->nHull - 1], 94103);

	/* set the partial sum arrais to zero */
	for(i = 0; i != (SUM_N); ++i)
	{
		pFPart_x[i] = 0.0;
		pFPart_y[i] = 0.0;
	};/* end for(i): setting partial sums to zero */

	/* unpack the structure */
	hull = cHull->hull;
	xPos = cHull->xPos;
	yPos = cHull->yPos;
	nHull       = cHull->nHull;
	nTotMarkers = cHull->nTotMarkers;

	/* compute the grid specing */
	dx = xPressNode[1] - xPressNode[0];
	dy = yPressNode[1] - yPressNode[0];

	/* load the position of the element of the hull */
	idx_1 = hull[0];
		csibyl_assert2(idx_1 >= 0          , 94301);
		csibyl_assert2(idx_1 <  nTotMarkers, 94302);
	x_1   = xPos[idx_1];
	y_1   = yPos[idx_1];

	/* Compute the first point pressure point */
	CSIBYL_CALL(csibyl_interpolatePressure,
				pressure,
				xPressNode, yPressNode,
				nPressNodeX, nPressNodeY,
				dx, dy,
				x_1, y_1,
				&p_1);

	/* iterate through the segments and sum up the
	 * pressure contribution, we start at 1, because we have
	 * already handled/computed for the first value */
	for(i = 1; i != nHull; ++i)
	{
		/* we only have to load _2, which correspons to i */
		idx_2 = hull[i];		/* index    */
			csibyl_assert2(idx_2 >= 0          , 94303);
			csibyl_assert2(idx_2 <  nTotMarkers, 94304);
		x_2   = xPos[idx_2];		/* position */
		y_2   = yPos[idx_2];

		/* calculate the pressure in point 2 */
		CSIBYL_CALL(csibyl_interpolatePressure,
				pressure,
				xPressNode, yPressNode,
				nPressNodeX, nPressNodeY,
				dx, dy,
				x_2, y_2,
				&p_2);

		/* first we compute the pressure in the midpoint
		 * This is just average the two pressures we already have */
		p_m = (p_1 + p_2) * 0.50;

		/* now we compute the magnitude of the pressure force
		 * that is experienced on the current segment. FOr that
		 * we just multiply with teh length of the segment.
		 * However it is not needed, because it will later cancel
		 * out. Instead we will calculate the displacement,
		 * between the two points */
		d_x = x_2 - x_1;
		d_y = y_2 - y_1;

		/* swapping and a minus, trivial, where the minus for
		 * outwards pointing, make a small drawing */
		n_x = -d_y;
		n_y =  d_x;

		/* now compute the contribution of the current segment,
		 * Here it is obvious that the normalization drops out */
		pF_1 = n_x * p_m;
		pF_2 = n_y * p_m;
			csibyl_assert2(isfinite(pF_1), 94201);
			csibyl_assert2(isfinite(pF_2), 94202);

		/* sum it up */
		pFPart_x[i % (SUM_N)] += pF_1;
		pFPart_y[i % (SUM_N)] += pF_2;


		/* printf("PRESS(%d): p_m = %f, pF_1 = %f, pF_2 = %f\n", i, p_m, n_x * p_m, n_y * p_m); */

		/* Now move _2 into _1 for the next iteration */
		  x_1 =   x_2;
		  y_1 =   y_2;
		  p_1 =   p_2;
		idx_1 = idx_2;
	}; /* end for(i): */

	/* Compute the reduce the arraies */
	pF_1 = 0.0;
	pF_2 = 0.0;
	for(i = 0; i != (SUM_N); ++i)
	{
		pF_1 += pFPart_x[i];
		pF_2 += pFPart_y[i];
	};/* end for(i): setting partial sums to zero */

	/* Now we have to to multiply the computed force with -1.
	 * The reason for this is, is of the definition of the normal
	 * and pressure. */
	pF_1 = -pF_1;
	pF_2 = -pF_2;

	/* Store the result back */
	pForce[0] = pF_1;
	pForce[1] = pF_2;

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;

#	undef SUM_N
}; /* end: pressure force */



/*
 * ==========================
 * Internal helper functions
 */
Int_t
csibyl_interpolatePressure(
	const Numeric_t* const restrict 	pressure,
	const Numeric_t* const restrict 	xPressNode,
	const Numeric_t* const restrict 	yPressNode,
	const Int_t 				nPressNodeX,
	const Int_t 				nPressNodeY,
	const Numeric_t 			dx,
	const Numeric_t 			dy,
	const Numeric_t 			x,
	const Numeric_t 			y,
	      Numeric_t* 			p)
{
	Numeric_t xStart, yStart;		/* start of the domain */
	Numeric_t w_00, w_01, w_10, w_11;	/* wights */
	Numeric_t compX, compY;			/* helper variables for the weights */
	Numeric_t p_00, p_01, p_10, p_11;	/* pressure values */
	Numeric_t x_a, y_a;			/* adjusted positions */
	Numeric_t xFrac, yFrac;			/* fractal index */
	Int_t i, j;				/* the real index */

	CSIBYL_RT_FUNC_START;


	xStart = xPressNode[0];		/* load the start of the domain */
	yStart = yPressNode[0];
	x_a    = x - xStart;		/* compute the adjusted marker position */
	y_a    = y - yStart;
	xFrac  = x_a / dx;		/* compute the fractional index */
	yFrac  = y_a / dy;
	i      = (Int_t)(yFrac);	/* get the real index */
	j      = (Int_t)(xFrac);
		csibyl_assert2(0     <= i          , 95001);
		csibyl_assert2(0     <= j          , 95002);
		csibyl_assert2((i+1) <  nPressNodeY, 95003);	/* we need to be able to add one */
		csibyl_assert2((j+1) <  nPressNodeX, 95004);

	/* computing the weights, could become negative, a bit */
	compX = (x   - xPressNode[j]) / dx;
	compY = (y   - yPressNode[i]) / dy;
	w_00  = (1.0 - compX) * (1.0 - compY);
	w_01  = (      compX) * (1.0 - compY);
	w_10  = (1.0 - compX) * (      compY);
	w_11  = (      compX) * (      compY);

	p_00  = pressure[nPressNodeX *  i    +  j   ];
	p_01  = pressure[nPressNodeX *  i    + (j+1)];
	p_10  = pressure[nPressNodeX * (i+1) +  j   ];
	p_11  = pressure[nPressNodeX * (i+1) + (j+1)];

	/* compute the value */
	*p =   p_00 * w_00
	     + p_01 * w_01
	     + p_10 * w_10
	     + p_11 * w_11;
		csibyl_assert2(isfinite(*p), 95005);

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
}; /* End: interpolating */


