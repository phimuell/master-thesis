/**
 * \brief	This file contains the implementation of the count function.
 */

#include "./cLibSibyl.h"
#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_runtime.h"

#include <stdlib.h>
#include <math.h>

Int_t
cLibSibyl_countAssocMarkers(
	Int_t* restrict 	yAssoc,
	Int_t* restrict  	xAssoc,
	Int_t 			nMarker,
	Int_t 			nY,
	Int_t 			nX,
	Int_t* restrict		nCellCount /* OUT */ )
{
	Int_t nCC_x, nCC_y, nCC_stride;
	Int_t i, j, k, m;

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	/* Test if the poInt_ters are okay */
	if(yAssoc     == NULL ||
	   xAssoc     == NULL ||
	   nCellCount == NULL   )
		{cLibSibylRT_exit(1);};

	/* Test if the sizes are correct */
	if(nY <= 1 || nX <= 1)
		{cLibSibylRT_exit(2);};

	/* Test if the marker numbers are okay */
	if(nMarker < 0)
		{cLibSibylRT_exit(3);};

	/* Sizes of the output matrix */
	nCC_x = nX;
	nCC_y = nY;

	/* This is the stride for the nCellCount matrix */
	nCC_stride = nX;

	/*
	 * Setting the output matrix to zero
	 */
	k = 0;
	for(i = 0; i != nCC_y; ++i)
	{
		for(j = 0; j != nCC_x; ++j)
		{
			nCellCount[k++] = 0;
		}; /* End for(j): */
	}; /* End for(i): */


	/*
	 * Iterate through the associations and count them
	 */
	for(m = 0; m < nMarker; ++m)
	{
		/* load the associations */
		i = yAssoc[m];
		j = xAssoc[m];

		/* Test if the association is correct */
		if(i < 0 || i >= nCC_y)	  /* because of zero based */
			{cLibSibylRT_exit(-1);};
		if(j < 0 || j >= nCC_x)
			{cLibSibylRT_exit(-2);};

		/* Calculating the index */
		k = nCC_stride * i + j;

		//Increasing the value
		nCellCount[k] += 1;
	}; /* End for(m): */

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
}; //End: countMarkerInCell


Int_t
cLibSibyl_countAssocMarkersK(
	Int_t* restrict 	K,
	Int_t 			nMarker,
	Int_t 			nY,
	Int_t 			nX,
	Int_t* restrict		nCellCount /* OUT */ )
{
	Int_t nCC_x, nCC_y;
	Int_t i, j, k, m;

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	/* Test if the poInt_ters are okay */
	if(K          == NULL ||
	   nCellCount == NULL   )
		{cLibSibylRT_exit(1);};

	/* Test if the sizes are correct */
	if(nY <= 1 || nX <= 1)
		{cLibSibylRT_exit(2);};

	/* Test if the marker numbers are okay */
	if(nMarker < 0)
		{cLibSibylRT_exit(3);};

	/* Sizes of the output matrix */
	nCC_x = nX;
	nCC_y = nY;

	/*
	 * Setting the output matrix to zero
	 */
	k = 0;
	for(i = 0; i != nCC_y; ++i)
	{
		for(j = 0; j != nCC_x; ++j)
		{
			nCellCount[k++] = 0;
		}; /* End for(j): */
	}; /* End for(i): */


	/*
	 * Iterate through the associations and count them
	 */
	for(m = 0; m < nMarker; ++m)
	{
		/* load the associations */
		k = K[m];

		/* Test if the association is correct */
		csibyl_assert2( k          >= 0    , -1);
		csibyl_assert2((k / nCC_x) <  nCC_y, -2);
		csibyl_assert2((k % nCC_x) <  nCC_x, -3);

		//Increasing the value
		nCellCount[k] += 1;
	}; /* End for(m): */

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
}; //End: countMarkerInCell


