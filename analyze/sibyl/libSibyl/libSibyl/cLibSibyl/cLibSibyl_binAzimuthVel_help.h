/**
 * \brief	This file contians helper functions for the binAtimuth function.
 * 		 Only intended for it.
 */
#ifndef CLIBSIBYL_ATZIMUTH_HELP_HEADER
#define CLIBSIBYL_ATZIMUTH_HELP_HEADER

#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_statistic.h"

#include <stdlib.h>
#include <math.h>

/**
 * \brief	This function is able to find the bin in which the marker is.
 *
 * It does return the bin number, the value is guaranteed to be smaller than
 * nBins. It also expects the same edge format.
 * Negative value indicates special situations.
 * 	> -1: The passed value is belwo the smallest bin.
 * 	> -2: The passed value is above the largest bin.
 * All other negative value indicates an error.
 *
 * Thie function does not check the validity of its arguments.
 *
 * \param  binEdges	Array with the bin edges positions.
 * \param  r 		Position to determine.
 * \param  nBins 	Number of bins to determine.
 */
static
Int_t
cSibyl_aziVelHelp_findBin(
	const Numeric_t* const restrict 	binEdges,
	const Numeric_t 			r,
	const Int_t 				nBins)
{
	/* make some simple checks */
	if(binEdges == NULL)
		{ return -6; };
	if(r < binEdges[0])
		{ return -1; };
	if(binEdges[nBins] < r)
		{ return -2; };

	/* the position is now inside a bin */
    	Int_t it    = 0,
    	      count = nBins + 1,
    	      first = 0;
    	Int_t step;


    	while (count > 0)
    	{
		it = first;
		step = count / 2;
		it += step;
			if(it < 0)
				{ return -3; };
			if(nBins < it)
				{ return -4; };

		if(binEdges[it] < r)
		{
			first  = ++it;
			count -= step + 1;
		}
		else
		{
			count = step;
		};
    	}; /* END: while */

    	/* This is the index that we have found */
    	it = first;

    	/* Handle corner cases */
    	if(it == 0)
	{
		return it;
	}
	if(it >= nBins)		/* handling numerical issues */
	{
		return (nBins - 1);
	}

	/* Since we found the index that is not less, we
	 * are one too large, so we have to substract one */
	it -= 1;

	return it;
}; /* END:	find bin */


#endif /* END: guard */

