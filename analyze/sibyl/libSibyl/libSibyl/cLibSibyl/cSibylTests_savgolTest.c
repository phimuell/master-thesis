/**
 * \brief	This function timplements a test for the savgol filter.
 */

#include "./cLibSibyl_linAlg.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"
#include "./cSibylTests.h"
#include "./cLibSibyl.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

Int_t
cSibylTest_savgol(void)
{
#	define			N 	50000
	const Numeric_t 	xStart 	=  0.0;
	const Numeric_t 	xEnd 	= 10.0;

	/* this are our x points */
	const Numeric_t h = (xEnd - xStart) / (N - 1);
	Numeric_t X[N];
	for(Int_t i = 0; i != N; ++i)
		{X[i] = xStart + i * h;};

	/* now we make the y value, we generate them
	 * from a polynomial of degree 3 */
	Numeric_t Y[N];
	for(Int_t i = 0; i != N; ++i)
	{
		const Numeric_t x = X[i];	/* load the x value */
		const Numeric_t y = 2 * x * x * x + 3 * x * x + 4 * x + 2;
		Y[i] = y;
	};

	/* Generate the scartch memory */
	void* w = cLibSibyl_savgolWorkMemory(2, 2, 2);

	/* This is the smoothed polynomial */
	Numeric_t yHat[N];	/* this is the computed */
	Numeric_t yHat_a[N];	/* this is the analytical one */
	for(Int_t k = 0; k != N; ++k)
		{yHat[k] = 0.0; yHat_a[k] = 0.0;};

	/* We use a window of size 2 in each direction also a second degree value is used
	 * The reason is that we can analyticaly compute the solution */
	for(Int_t m = 2; m != (N - 2); ++m)
	{
		const Int_t savGolRes = cLibSibyl_savgolSmoothStep(
				X, Y, N,	/* observation */
				yHat,		/* solution */
				2,		/* degree */
				0,		/* no derivative */
				2, m, 2,	/* smoothing location */
				w);

		if(savGolRes != 0)
		{
			printf("Detected an error in smoothing position %d, error was %d\n", m, savGolRes);
			return 1;
		};

		/* Compute the analytuical value
		 * 	https://en.wikipedia.org/wiki/Savitzky%E2%80%93Golay_filter#Applications */
		const Numeric_t y_mm2 = Y[m - 2];
		const Numeric_t y_mm1 = Y[m - 1];
		const Numeric_t y_m   = Y[m    ];
		const Numeric_t y_mp1 = Y[m + 1];
		const Numeric_t y_mp2 = Y[m + 2];

		const Numeric_t y_au  = -3 * y_mm2 + 12 * y_mm1 + 17 * y_m + 12 * y_mp1 - 3 * y_mp2;
		const Numeric_t y_a   = y_au / 35.0;

		yHat_a[m] = y_a;
	}; /* end for(m) */


	printf("\nFirst test\n");
	Numeric_t l2Error = 0;
	for(Int_t m = 2; m != (N - 2); ++m)
	{
		const Numeric_t yh   = yHat[m];
		const Numeric_t yh_a = yHat_a[m];
		const Numeric_t diff = yh_a - yh;
		l2Error += diff * diff;
		//printf("\t %d -> %e  (exact: %e),  diff ~ %e\n", m, yHat[m], yHat_a[m], diff);
	};
	//printf("\n");
	l2Error = sqrt(l2Error) / (N - 4);
	printf("l2 = %e\n\n", l2Error);
	cLibSibyl_free(&w);


	/*
	 * Second test
	 *
	 * Here we will use the fact that we can determine the
	 * polynomial exactly if degree and poinst are coosen correctly.
	 * Hower since the underlaing data is realy a 4. order polynomial,
	 * and we useing one too fit one, we can use any wiondow size.
	 */
	const Int_t wSizeTest2 = 4;
	const Int_t degTest2   = 4;
	w = cLibSibyl_savgolWorkMemory(degTest2, wSizeTest2, wSizeTest2);

	/* Now we create a new polynomial, one of degree 4,
	 * thie means exact reconstruction with 5 points */
	Numeric_t dY[N];
	for(Int_t i = 0; i != N; ++i)
	{
		const Numeric_t x  = X[i];	/* load the x value */
		const Numeric_t y  = (x * x) * (x * x) + 2 * x * x * x + 3 * x * x + 4 * x + 2;
		const Numeric_t dy = (4 * x) * (x * x) + 2 * 3 * x * x + 3 * 2 * x + 4 * 1;
		Y[i] = y;
		dY[i] = dy;
		yHat[i] = 0.0;
		yHat_a[i] = 0.0;
	};


	/* Window size of 2 in each direction and
	 * degree 4 */
	Numeric_t yHatD[N];
	for(Int_t m = wSizeTest2; m != (N - wSizeTest2); ++m)
	{
		const Int_t savGolRes = cLibSibyl_savgolSmoothStep(
				X, Y, N,	/* observation */
				yHat,		/* solution */
				degTest2,	/* degree */
				0,		/* no derivative */
				wSizeTest2, 	/* left window extension */
				m, 		/* smoothing extension */
				wSizeTest2, 	/* rirght window extension */
				w);

		if(savGolRes != 0)
		{
			printf("Detected an error in smoothing position %d, error was %d\n", m, savGolRes);
			return 1;
		};

		const Int_t savGolResD = cLibSibyl_savgolSmoothStep(
				X, Y, N,	/* observation */
				yHatD,		/* solution */
				degTest2,	/* degree */
				1,		/* no derivative */
				wSizeTest2, 	/* left window extension */
				m, 		/* smoothing extension */
				wSizeTest2, 	/* rirght window extension */
				w);

		if(savGolResD != 0)
		{
			printf("Detected an error in smoothing position %d, during derivative, error was %d\n", m, savGolResD);
			return 1;
		};

	}; /* end for(m) */

	printf("\nSecond test\n");
	          l2Error  = 0;	/* error in smoosthed value */
	Numeric_t l2ErrorD = 0;	/* error for derivative     */
	for(Int_t m = wSizeTest2; m != (N - wSizeTest2); ++m)
	{
		const Numeric_t yh    = yHat[m];
		const Numeric_t yh_a  = Y[m];
		const Numeric_t dyh   = yHatD[m];
		const Numeric_t dyh_a = dY[m];
		const Numeric_t diff  = yh_a - yh;
		const Numeric_t Ddiff = dyh_a - dyh;

		l2Error  +=  diff *  diff;
		l2ErrorD += Ddiff * Ddiff;
		//printf("\t %d -> %e  (exact: %e),  diff ~ %e\n", m, dyh, dyh_a, Ddiff);
		//printf("\t %d -> (D) %e  (exact: %e),  diff ~ %e\n", m, yh, yh_a, diff);
	};
	//printf("\n");
	l2Error  = sqrt(l2Error ) / (N - 2 * wSizeTest2);
	l2ErrorD = sqrt(l2ErrorD) / (N - 2 * wSizeTest2);
	printf("l2  = %e\n"  , l2Error );
	printf("Dl2 = %e\n\n", l2ErrorD);

	cLibSibyl_free(&w);


	/*
	 * ====================
	 * Third test
	 */
	const Int_t wwww = cLibSibyl_SavGolSmooth(
			X, Y, yHat,
			0,	//derivative
			degTest2,
			5,  //wSize
			0.5, 	//mR
			N,
			0, N,	//Begin start
			1, 0);

	if(wwww != 0)
	{
		printf("Error in the thrid test: %d\n", wwww);
		return 1;
	};

	printf("\nThird test\n");
	          l2Error  = 0;	/* error in smoosthed value */
	for(Int_t m = 0; m != N; ++m)
	{
		const Numeric_t yh    = yHat[m];
		const Numeric_t yh_a  = Y[m];
		const Numeric_t diff  = yh_a - yh;

		l2Error  +=  diff *  diff;
		//printf("\t %d -> %e  (exact: %e),  diff ~ %e\n", m, yh, yh_a, diff);
	};
	//printf("\n");
	l2Error  = sqrt(l2Error ) / (N);
	printf("l2  = %e\n"  , l2Error );

	return 0;
}; /* END */




