/**
 * This is the main header of cLibSibyl, a C extension of Sibyl.
 *
 * As a general note, cLibSibyl intends to work as a C library.
 * so it expects its (matrix) argument to be in row major order.
 *
 * The functions that are defined in this file requieres that the
 * runtime is initialized. This is done automatically at startup.
 * However they will test this condition.
 */
#ifndef CLIBSIBYL_MAIN_HEADER
#define CLIBSIBYL_MAIN_HEADER

/* Includes */
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"


#include <stddef.h>


/*
 * =========================
 *  F U N C T I O N S
 */

/**
 * \brief	This function will find the theoretical extension of each marker.
 *
 * Assuming that in cell i there are Ni markers. Then the theoretical extension
 * of all markers in that cell is given by $Ai / Ni$, where Ai is the spatial
 * extension of that cell.
 *
 * The function expects the grid node positions and the marker positions.
 * Note that this function assumes that the passed grid points are the one of
 * the basic nodal grid. The theoretical extension is then returned by the argument.
 *
 * It the computation is successfull, zero is returned. If an error happens a
 * non zero value is returned.
 *
 * \param  yPos		y position of markers, array of length nMarkers.
 * \param  xPos		x position of markers, array of length nMarkers.
 * \param  nMarkers	The number of markers.
 * \param  gPosY	y position of grid points.
 * \param  gPosX	x position of grid points.
 * \param  nGridY	Numbers of grid points in y direction.
 * \param  nGridX	Numbers of grid points in x direction.
 * \param  theoExten	The theoretical extesnion, array of length nMarkers.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_computeTheoreticalExtension(
	const Numeric_t* const restrict 	yPos,
	const Numeric_t* const restrict 	xPos,
	const Int_t 				nMarker,
	const Numeric_t* const restrict 	gPosY,
	const Numeric_t* const restrict 	gPosX,
	const Int_t 				nGridY,
	const Int_t 				nGridX,
	      Numeric_t* const restrict 	theoExten /* OUT */)
 CSIBYL_ATTR_HOT;


/**
 * \brief	This function will update the theoretical extnsion rather than recompute it.
 *
 * For this an extra argument that encoodes the association of the markers must be passed.
 * This vector contains the last known association. Rather than performing a spearch, this
 * function will check if the association is still valid. For this the update function of the
 * association is used. Beside this, this function works the same as the other one.
 *
 * \param  yPos		y position of markers, array of length nMarkers.
 * \param  xPos		x position of markers, array of length nMarkers.
 * \param  nMarkers	The number of markers.
 * \param  gPosY	y position of grid points.
 * \param  gPosX	x position of grid points.
 * \param  nGridY	Numbers of grid points in y direction.
 * \param  nGridX	Numbers of grid points in x direction.
 * \param  K 		The association, that is known, will be modified.
 * \param  theoExten	The theoretical extesnion, array of length nMarkers.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_updateTheoreticalExtension(
	const Numeric_t* const restrict 	yPos,
	const Numeric_t* const restrict 	xPos,
	const Int_t 				nMarker,
	const Numeric_t* const restrict 	gPosY,
	const Numeric_t* const restrict 	gPosX,
	const Int_t 				nGridY,
	const Int_t 				nGridX,
	      Int_t*     const restrict 	K /* IN/OUT */,
	      Numeric_t* const restrict 	theoExten /* OUT */)
 CSIBYL_ATTR_HOT;


/**
 * \brief	This function will update the theoretical extnsion rather than recompute it,
 * 		 but operates on an already reduced association.
 *
 * This function is provided to split the computation of the extension further. It assumes
 * that the assocaition vector was already upodated and is stored in K, and that it was
 * reduced as well, this means the number of associations is stored in the nCellCount matrix.
 *
 * \param  nMarkers	The number of markers.
 * \param  gPosY	y position of grid points.
 * \param  gPosX	x position of grid points.
 * \param  nGridY	Numbers of grid points in y direction.
 * \param  nGridX	Numbers of grid points in x direction.
 * \param  K 		The association, that is known, it is assumed to be correct.
 * \param  nCellCount	The association count, that is based on K (no associatin to last node).
 * \param  theoExten	The theoretical extesnion, array of length nMarkers.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_computeTheoreticalExtensionOnCA(
	const Int_t 				nMarker,
	const Numeric_t* const restrict 	gPosY,
	const Numeric_t* const restrict 	gPosX,
	const Int_t 				nGridY,
	const Int_t 				nGridX,
	const Int_t*     const restrict 	K /* IN */,
	const Int_t* 	 const restrict         nCellCount /* IN */,
	      Numeric_t* const restrict 	theoExten /* OUT */)
 CSIBYL_ATTR_HOT;


/**
 * \brief	This function computes the first derivative of the given series.
 *
 * This function uses either forward or backwards differentiation. At the end
 * always the only possible one is selected. Note that this function can operate
 * on non uniform spaced grids.
 *
 * \param  y 		y values, to derive.
 * \param  x		Positions where the value was taken.
 * \param  dy 		Derivative value
 * \param  N		Number of points that are derivied.
 * \param  scheme	Which scheme sould be used.
 * \param  order 	The order of accuracy, must be one.
 *
 * schemes:
 * 	* -1  -> 	Backwards differenciation.
 * 	* +1  -> 	Forward differenciation.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_firstDerivative(
	Numeric_t* restrict 	y,
	Numeric_t* restrict 	x,
	Numeric_t* restrict 	dy,
	Int_t 			N,
	Int_t 			scheme,
	Int_t 			order);


/**
 * \brief	This function is a Savitzky–Golay filer for non equidistant grids.
 *
 * Being non-equidistant means that for each position a linear system of equation
 * must be solved. Currently the matrix is not scalled, so it can become nearly singular.
 * The solver will most likely handle this, but the quality can become low, especially
 * if the degree is large.
 *
 * This function uses a moving window approache to determine a smoothed value.
 * The size of this window is controlled by wSize. This means it operates on index distances.
 * Currently there is no way, of specifing a distance, such that the window would also become
 * dependend on the density.
 * The point that is smoothed, inside the moving window can also be selected by the parameter
 * mR. This is is a float that redscribes the fraction relative to the start. A value if 0.5,
 * will make the window symetric, use an odd number window points.
 * At the edges where this can not be maintained, the point is adjusted to meet it.
 * It is possible to diable the adjusting. Conceptionaly this will modify begin and end,
 * but not impose a non access restriction.
 *
 * It is not only possible to smooth the observed data, but also its derivative. The derivative
 * is controlled by the dy value. A zero means that the function is smoothed. The derivative
 * must be smaller than the degree, also it is recomended to use a degree larger than 4.
 *
 * It is also possible to ignore the beginning and the end of the array, note that these values
 * will never be considered. This is done by the parameters begin and end. Note that n is
 * considered to be the full length.
 * Values that are ignored by this can be either copied from the unsmoothed array, or be
 * set to zero. Note that if dy > 0, then they will be set to zero.
 *
 * \param  x		Array of length n, that contains the x observation points.
 * \param  y 		Array of length n, that contains the observed values, they will be smoothed.
 * \param  yHat		Array of length n, that will contain the smoothed values, will be written to.
 * \param  dy		Derivative that should be found.
 * \param  wSize	Number of points that should be used for the smoothing.
 * \param  mR		Where the central point, the one that will be fitted lies, relative to the start of the window.
 * \param  n 		Length of the input arrays.
 * \param  begin	Index of the first element that should be handled.
 * \param  end		Past the end index of the last element that should be handled.
 * \param  doCopy	If non zero, non determent values will be copied, if zero they will be set to zero.
 * \param  noSkew	Do not skew the window at the edges, shring the domain instead.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_SavGolSmooth(
	const Numeric_t* const restrict 	x,
	const Numeric_t* const restrict 	y,
	      Numeric_t* const restrict 	yHat,
	const Int_t 				dy,
	const Int_t 				deg,
	const Int_t 				wSize,
	const Numeric_t 			mR,
	const Int_t 				n,
	const Int_t 				begin,
	const Int_t				end,
	const Int_t 				doCopy,
	const Int_t 				noSkew);


/**
 * \brief	This function computes the pressure force that is acting on a set of points.
 *
 * This function will first calculate the convex hull and then perform the integration over
 * the surface. It will also integrate the pressure furce over the interface. Note that this
 * function assumes that the pressure grid that is passed is defined on the extended pressure
 * grid. This implies that all markers have an surrounding node.
 *
 * Note that thie function will compute the force that acts on the hull. This means that the
 * following integral is evaluated:
 * 	\int_{\partial \mathcal{H}} -p(\vec{x}) \vec{n}(\vec{x}) \dvol{\vec{x}}
 *
 * In the above integral $\partial \mathcal{H}$ is the surface of the convex hull and $\vec{n}$
 * is a unit vector pointing outwards of the hull. The minus in front of the p, which is the
 * pressure value, is needed such that the force has the correct meaning.
 *
 * It also assumes that the grid is uniformly spaced. Note the normal component points outwards.
 *
 * Note that it is legal to pass 0 for nSubSet, in that case all markers will be selected.
 * This does not select none.
 *
 * The pressure matrix is assumed to be in row major order. Note that inside EGD they
 * are column major order, but inside CSIBYL, they are row major order. Also we assume
 * that the number or colums is equal the number of x grid points and number of rows
 * is equal y grid points.
 *
 * This function will also compute the volume that is occupied by the selected markers.
 *
 * \param  x 		Pointer to the x coordinate of the markers.
 * \param  y 		Pointer to the y coordinate of the markers.
 * \param  nMarkers	The number of the markers.
 * \param  subSet	Pointer to the subset array.
 * \param  nSubSet	Numbers of markers in the subset array.
 * \param  pressure	Pointer to the pressure field.
 * \param  xPressNode	Pointer to the x coordinates of the pressure point.
 * \param  yPressNode	Pointer to the y coordinates of the pressure point.
 * \param  nPressNodeX	Number of pressure nodes in x direction.
 * \param  nPressNodeY	Number of pressure nodes in y direction.
 * \param  pForce	Array of length two to the pressure force.
 * \param  vol 		In this address the volume will be written.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_computePressureForce(
	const Numeric_t* const restrict 	x,
	const Numeric_t* const restrict 	y,
	const Int_t 				nMarkers,
	const Int_t*     const restrict 	subSet,
	const Int_t 				nSubSet,
	const Numeric_t* const restrict 	pressure,
	const Numeric_t* const restrict 	xPressNode,
	const Numeric_t* const restrict 	yPressNode,
	const Int_t 				nPressNodeX,
	const Int_t 				nPressNodeY,
	      Numeric_t* const restrict 	pForce,
	      Numeric_t* const restrict 	vol);


/**
 * \brief	This function is able to analize the azimuth velocity of some markers.
 *
 * The ranges is binned and the mean and std are calculated for them.
 * All markers that are not in a bin will be ignored.
 *
 * \param  x 		Pointer to the x coordinate of the markers.
 * \param  y 		Pointer to the y coordinate of the markers.
 * \param  vx 		Pointer to the x velocity of the markers.
 * \param  vy 		Pointer to the y velocity of the markers.
 * \param  nMarkers	The number of the markers.
 * \param  binEdges	Pointer to the bin edges, including right most point (nBin + 1).
 * \param  nBins	Number of bins, one more edges.
 * \param  binMean	Pointer to the mean azimuth vel in each bin, length nBins.
 * \param  binStd	Pointer to the sample std of the azimuth vel in each bin, length nBins.
 * \param  orig 	Origin of the the radial distribution.
 * \param  nPorcess 	Number of markers that were processed.
 * \param  nLow		Number of markers that were too low.
 * \param  nUp		Number of markers that were too far away.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_binAzimuthVel(
	const Numeric_t* const restrict 	x,		/* IN;  len = nMarkers 	*/
	const Numeric_t* const restrict 	y,		/* IN;  len = nMarkers 	*/
	const Numeric_t* const restrict 	vx,		/* IN;  len = nMarkers 	*/
	const Numeric_t* const restrict 	vy,		/* IN;  len = nMarkers 	*/
	const Int_t 				nMarkers,	/* IN 			*/
	const Numeric_t* const restrict 	binEdges,	/* IN;  len = nBins + 1	*/
	const Int_t 				nBins,		/* IN 			*/
	      Numeric_t* const restrict 	binMean,	/* OUT; len = nBins 	*/
	      Numeric_t* const restrict 	binStd,		/* OUT; len = nBins 	*/
	const Numeric_t* const restrict 	orig,		/* IN;  len = 2; opt.	*/
	      Int_t*     const restrict 	nProcess,	/* OUT; opt.		*/
	      Int_t* 	 const restrict 	nLow,		/* OUT;	opt.		*/
	      Int_t* 	 const restrict 	nUp);		/* OUT; opt.		*/



/*
 * =============================
 * Low Level Functions
 */


/**
 * \brief	This function finds the association of all markers.
 *
 * This function outputs two arrays of length nMarkers. I is the association
 * in the y coordinate (first index), and J is the one for x (second coordinate).
 * If isTight is set to a non zero value, it is assumed that the grid positions
 * will complety bound the markers.
 *
 * This function applies laxer rules as EGD.
 *
 * This function returns zero on success and a non zero value, on failure.
 *
 * \param  yPos		y position of markers, array of length nMarkers.
 * \param  xPos		x position of markers, array of length nMarkers.
 * \param  nMarkers	The number of markers.
 * \param  gPosY	y position of grid points.
 * \param  gPosX	x position of grid points.
 * \param  nGridY	Numbers of grid points in y direction.
 * \param  nGridX	Numbers of grid points in x direction.
 * \param  isTight 	are grid bounding.
 * \param  I 		Assocaitaion in Y.
 * \param  J 		Association in X.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_findAssociation(
	Numeric_t* restrict 		yPos,
	Numeric_t* restrict 		xPos,
	Int_t 				nMarker,
	Numeric_t* restrict 		gPosY,
	Numeric_t* restrict 		gPosX,
	Int_t 				nGridY,
	Int_t 				nGridX,
	Int_t 				isTight,
	Int_t* restrict 		I /* OUT */,
	Int_t* restrict 		J /* OUT */)
 CSIBYL_ATTR_HOT;


/**
 * \brief	This function finds the association of all markers.
 *
 * This function is simillar to the findAssocation function, but
 * uses a different output format. instead of outputting the
 * indeces seperately they are combined to a linear index k.
 * k is composed as:
 * 	k := i * nGridX + j
 * This is suitable to address a matrix in row major order.
 *
 * \param  yPos		y position of markers, array of length nMarkers.
 * \param  xPos		x position of markers, array of length nMarkers.
 * \param  nMarkers	The number of markers.
 * \param  gPosY	y position of grid points.
 * \param  gPosX	x position of grid points.
 * \param  nGridY	Numbers of grid points in y direction.
 * \param  nGridX	Numbers of grid points in x direction.
 * \param  isTight 	are grid bounding.
 * \param  K		Linear index of association.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_findAssociationK(
	Numeric_t* restrict 		yPos,
	Numeric_t* restrict 		xPos,
	Int_t 				nMarker,
	Numeric_t* restrict 		gPosY,
	Numeric_t* restrict 		gPosX,
	Int_t 				nGridY,
	Int_t 				nGridX,
	Int_t 				isTight,
	Int_t* restrict 		K /* OUT */)
 CSIBYL_ATTR_HOT;


/**
 * \brief	This function will update the association that is
 * 		 passed to it.
 *
 * It assumes that the association was computed by a find association
 * or is "around for some time". It will then check if the association
 * is still valid and then update it. This function can handle large
 * movements of markers.
 *
 * It ecepts The input in the following format:
 * 	k := i * nGridX + j
 * This is suitable to address a matrix in row major order.
 *
 * Invalid values will be considered unkown and will be recomputed,
 * by the function. It is recomended to use negative numbers for this.
 * Instead of large positive values.
 *
 * \param  yPos		y position of markers, array of length nMarkers.
 * \param  xPos		x position of markers, array of length nMarkers.
 * \param  nMarkers	The number of markers.
 * \param  gPosY	y position of grid points.
 * \param  gPosX	x position of grid points.
 * \param  nGridY	Numbers of grid points in y direction.
 * \param  nGridX	Numbers of grid points in x direction.
 * \param  isTight 	are grid bounding.
 * \param  K		Linear index of association.
 *
 * This file is iomplemented in the findAssociation file.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_updateAssociationK(
	const Numeric_t* const restrict 	yPos,
	const Numeric_t* const restrict 	xPos,
	const Int_t 				nMarker,
	const Numeric_t* const restrict 	gPosY,
	const Numeric_t* const restrict 	gPosX,
	const Int_t 				nGridY,
	const Int_t 				nGridX,
	const Int_t 				isTight,
	      Int_t*     const restrict 	K /* IN/OUT */)
 CSIBYL_ATTR_HOT;


/**
 * \brief	This functin is used to count the number of markers that are
 * 		 associated to a certain grid node.
 *
 * It operates directly on already computed associations. An assoicated node
 * to a marker, is the one that is top left to that marker. All arguments are
 * expected to be passed in row major order.
 *
 * \param  yAssoc	The association in y direction, first index.
 * \param  xAssoc	The assoication in x direction, second index.
 * \param  nMarker	The number of markers.
 * \param  nY		Number of nodes in y direction.
 * \param  nX		Number of nodes in x direction.
 * \param  nCellCount	Output array.
 *
 * This function expects that the values of associations are greater than 0,
 * but strictly smaller than n{X, Y}. The output matrix cCellCount is expected
 * to be a $nY \times nX$ matrix, in row major format with a stride of nX.
 *
 * \return	The function returns an integer that indicates the reult of the
 * 		 computation. A zero value means success, a non zero value means
 * 		 an error has occured and the computation was aborded. Positive
 * 		 value indicating wrong arguments negative value indicating
 * 		 runtime problems.
 *
 * Note this function is expected to work with basic nodal grids. in which case
 * the last row/column must be zero.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_countAssocMarkers(
	Int_t* restrict 	yAssoc,
	Int_t* restrict  	xAssoc,
	Int_t 			nMarker,
	Int_t 			nY,
	Int_t 			nX,
	Int_t* restrict		nCellCount /* OUT */ )
 CSIBYL_ATTR_HOT;


/**
 * \brief	This functin is used to count the number of markers that are
 * 		 associated to a certain grid node.
 *
 * This function is like the normal count function, but operates on the linear index.
 *
 * \param  K 		The linear index of association.
 * \param  nMarker	The number of markers.
 * \param  nY		Number of nodes in y direction.
 * \param  nX		Number of nodes in x direction.
 * \param  nCellCount	Output array.
 *
 * \return	The function returns an integer that indicates the reult of the
 * 		 computation. A zero value means success, a non zero value means
 * 		 an error has occured and the computation was aborded. Positive
 * 		 value indicating wrong arguments negative value indicating
 * 		 runtime problems.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibyl_countAssocMarkersK(
	Int_t* restrict 	K,
	Int_t 			nMarker,
	Int_t 			nY,
	Int_t 			nX,
	Int_t* restrict		nCellCount /* OUT */ )
 CSIBYL_ATTR_HOT;

#endif /* end guard */

