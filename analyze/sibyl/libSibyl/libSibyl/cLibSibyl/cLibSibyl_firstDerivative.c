/**
 * \brief	This file implements the routine to compute the first derivative.
 */

#include "./cLibSibyl.h"
#include "./cLibSibyl_internal.h"
#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_runtime.h"

#include <assert.h>
#include <stdlib.h>
#include <math.h>

Int_t
cLibSibyl_firstDerivative(
	Numeric_t* restrict 	y,
	Numeric_t* restrict 	x,
	Numeric_t* restrict 	dy,
	Int_t 			N,
	Int_t 			scheme,
	Int_t 			order)
{
	Numeric_t x1, x2, y1, y2, hx, hy, diffY;
	Numeric_t x_m1, x_0, x_p1, h_p, h_m, y_m1, y_0, y_p1, a_m, a_0, a_p;	/* central scheme */
	Int_t i, endIt;

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	if(y  == NULL)
		{cLibSibylRT_exit(1);};
	if(x  == NULL)
		{cLibSibylRT_exit(2);};
	if(dy == NULL)
		{cLibSibylRT_exit(3);};
	if(N < 2)
		{cLibSibylRT_exit(4);};

	if(scheme == 1)
	{
		if(order != 1)
			{cLibSibylRT_exit(5);};

		/* Forward Differenciation */
		endIt = N - 1;
		for(i = 0; i != endIt; ++i)
		{
			x2 = x[i+1];	csibyl_assert2(isfinite(x2), -1);
			x1 = x[i  ];	csibyl_assert2(isfinite(x1), -2);
					csibyl_assert2(x2 > x1     , -3);
			y2 = y[i+1];	csibyl_assert2(isfinite(y2), -4);
			y1 = y[i  ];	csibyl_assert2(isfinite(y1), -5);

			hx = x2 - x1;
			hy = y2 - y1;

			diffY = hy / hx;	csibyl_assert2(isfinite(diffY), -6);

			dy[i] = diffY;
		}; /* End for(i): */

		/* Handle the last element */
		i = N - 1;
		{
			x2 = x[i  ];	csibyl_assert2(isfinite(x2), -1);
			x1 = x[i- 1];	csibyl_assert2(isfinite(x1), -2);
					csibyl_assert2(x2 > x1     , -3);
			y2 = y[i  ];	csibyl_assert2(isfinite(y2), -4);
			y1 = y[i-1];	csibyl_assert2(isfinite(y1), -5);

			hx = x2 - x1;
			hy = y2 - y1;

			diffY = hy / hx;	csibyl_assert2(isfinite(diffY), -6);

			dy[i] = diffY;
		}; /* End scope: handle end */
	}
	else if(scheme == -1)
	{
		/* baxkwards Differenciation */
		if(order != 1)
			{cLibSibylRT_exit(6);};

		endIt = N;
		for(i = 1; i != endIt; ++i)
		{
			x2 = x[i  ];	csibyl_assert2(isfinite(x2), -20);
			x1 = x[i- 1];	csibyl_assert2(isfinite(x1), -21);
					csibyl_assert2(x2 > x1     , -22);
			y2 = y[i  ];	csibyl_assert2(isfinite(y2), -23);
			y1 = y[i-1];	csibyl_assert2(isfinite(y1), -24);

			hx = x2 - x1;
			hy = y2 - y1;

			diffY = hy / hx;	csibyl_assert2(isfinite(diffY), -25);

			dy[i] = diffY;
		}; /* End for(i): */


		/* Handle the first element */
		i = 0;
		{
			x2 = x[i+1];	csibyl_assert2(isfinite(x2), -26);
			x1 = x[i  ];	csibyl_assert2(isfinite(x1), -27);
					csibyl_assert2(x2 > x1     , -28);
			y2 = y[i+1];	csibyl_assert2(isfinite(y2), -29);
			y1 = y[i  ];	csibyl_assert2(isfinite(y1), -30);

			hx = x2 - x1;
			hy = y2 - y1;

			diffY = hy / hx;	csibyl_assert2(isfinite(diffY), -31);

			dy[i] = diffY;
		}; /* End scope: handle first element */
	}
	else if(scheme == 0)
	{
		/* Central scheme
		 * See: https://www.nada.kth.se/kurser/kth/2D1263/l6.pdf */

		if(order != 2)
			{cLibSibylRT_exit(7);};

		endIt = N -1;
		for(i = 1; i != endIt; ++i)
		{
			/* loading */
			x_m1 = x[i - 1];	csibyl_assert2(isfinite(x_m1), -40);
			x_0  = x[i    ];	csibyl_assert2(isfinite(x_0 ), -41); csibyl_assert2(x_m1 < x_0 , -42);
			x_p1 = x[i + 1];	csibyl_assert2(isfinite(x_p1), -43); csibyl_assert2(x_0  < x_p1, -44);

			y_m1 = y[i - 1];
			y_0  = y[i    ];
			y_p1 = y[i + 1];

			/* distances */
			h_m  = x_0  - x_m1;
			h_p  = x_p1 - x_0 ;

			/* coefficientc */
			a_m  = (-h_p      ) / (h_m * (h_p + h_m));
			a_0  = ( h_p - h_m) / (       h_p * h_m );
			a_p  = (       h_m) / (h_p * (h_p + h_m));

			/* calculate derivative */
			diffY = a_m * y_m1 + a_0 * y_0 + a_p * y_p1;	csibyl_assert2(isfinite(diffY), -45);

			/* write back */
			dy[i] = diffY;
		}; /* end for(i): */


		/* Handle the edges with a first order scheme */

		/* Handle the first element */
		i = 0;
		{
			x2 = x[i+1];	csibyl_assert2(isfinite(x2), -46);
			x1 = x[i  ];	csibyl_assert2(isfinite(x1), -47);
					csibyl_assert2(x2 > x1     , -48);
			y2 = y[i+1];	csibyl_assert2(isfinite(y2), -49);
			y1 = y[i  ];	csibyl_assert2(isfinite(y1), -50);

			hx = x2 - x1;
			hy = y2 - y1;

			diffY = hy / hx;	csibyl_assert2(isfinite(diffY), -51);

			dy[i] = diffY;
		}; /* End scope: handle first element */

		/* Handle the last element */
		i = N - 1;
		{
			x2 = x[i  ];	csibyl_assert2(isfinite(x2), -52);
			x1 = x[i- 1];	csibyl_assert2(isfinite(x1), -53);
					csibyl_assert2(x2 > x1     , -54);
			y2 = y[i  ];	csibyl_assert2(isfinite(y2), -55);
			y1 = y[i-1];	csibyl_assert2(isfinite(y1), -56);

			hx = x2 - x1;
			hy = y2 - y1;

			diffY = hy / hx;	csibyl_assert2(isfinite(diffY), -57);

			dy[i] = diffY;
		}; /* End scope: handle end */
	}
	else
	{
		/* Scheme unkwonn */
		cLibSibylRT_exit(66);
	};

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
};

