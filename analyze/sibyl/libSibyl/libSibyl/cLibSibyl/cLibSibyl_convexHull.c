/**
 * \brief	This file implements the convex hull functions
 */

/* In order for qsort_r to be present, we need to define _GNU_SOURCE.
 * see QSORT(3) for more */
#if !defined(_GNU_SOURCE)
#	define _GNU_SOURCE 1
#endif


/* Includes */
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_convexHull.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_assert.h"

#include <stdlib.h>
#include <string.h> 	/* for memmove */
#include <math.h>






/*
 * ====================
 * Actuall computation
 */

/**
 * \brief	This function determines if a cw or ccw turn at the point O happens.
 *
 * In essence this function computes the third component of an ordinary cross product.
 * If the the line segement OAB, makes a counter-clockwise turn, a positive value
 * is returned. If the turn is clockwise direction, a negative value is returned.
 * Zero for a colinear segements.
 *
 * This function expects its argument, the first three one, to be indexes into the
 * {x,y}Pos arrays. They must be inside the idx set, but this is not enforced or
 * checked.
 *
 * Note that this function will return a double and no status code. If an error
 * occures during computation, the error is installed, and the return argument
 * is set to the error code. Note that in that case the function returns NAN.
 *
 * \param  O 		Index of the O point.
 * \param  A		Index of the A point.
 * \param  B 		Index of the B point.
 * \param  cHull	Pointer to the cHull object.
 * \param  errCode	The error code of the function.
 */
static
Numeric_t
csibyl_cHull_cross(
	const Int_t 					O,
	const Int_t 					A,
	const Int_t 					B,
	const struct sibyl_convexHull_t* const 		cHull,
	      Int_t* const 				errCode)
{
	Int_t O_g = -1, A_g = -1, B_g;	/* this are indexes in the global posuition array */
	Numeric_t A_x, A_y, B_x, B_y, O_x, O_y;
	Int_t nTotMarkers = -1, nMarkers = -1, nHull = -1;
	const Numeric_t *xPos, *yPos;
	Numeric_t val;

	CSIBYL_RT_FUNC_START;

	/* test if the hull is given */
	csibyl_assert2(cHull   != NULL, 91000);
	csibyl_assert2(errCode != NULL, 91001);

	/* unpack the scruct */
	nTotMarkers = cHull->nTotMarkers;
	nMarkers    = cHull->nMarkers;
	nHull       = cHull->nHull;
		csibyl_assert2(0     <= nHull   , 91002);
		csibyl_assert2(nHull <= nMarkers, 91003);

	/* copy the indexes */
	O_g = O;
	A_g = A;
	B_g = B;

	/* now we check this layer */
	csibyl_assert2(0 <= O_g         , 91201);
	csibyl_assert2(O_g < nTotMarkers, 91202);
	csibyl_assert2(0 <= A_g         , 91203);
	csibyl_assert2(A_g < nTotMarkers, 91204);
	csibyl_assert2(0 <= B_g         , 91205);
	csibyl_assert2(B_g < nTotMarkers, 91206);

	/* load the position values */
	xPos = cHull->xPos;
	yPos = cHull->yPos;
	A_x = xPos[A_g];
	A_y = yPos[A_g];
	B_x = xPos[B_g];
	B_y = yPos[B_g];
	O_x = xPos[O_g];
	O_y = yPos[O_g];

	/* compuet the value */
	val = (A_x - O_x) * (B_y - O_y) - (A_y - O_y) * (B_x - O_x);

	/* if we are here, then no error has happened so we return the
	 * value and set the error code */
	*errCode = CSIBYL_RT_LOC_RETURN_VAR;

	/* now we can return the value */
	return val;

	/* If we are here then an error has happened.
	 * We set the error value and return NAN */
	CSIBYL_RT_FUNC_CLEANUP:

	*errCode = CSIBYL_RT_LOC_RETURN_VAR;
	return NAN;
}; /* End: corss product */



Int_t
csibyl_computeConvexHull(
	struct sibyl_convexHull_t* 	cHull)
{
	Int_t nTotMarkers = -1, nMarkers = -1;
	Int_t k = 0, t;		/* helper variables */
	Int_t i, ii;		/* iterator variables */
	Int_t errCode;		/* error codes */
	Int_t *idx, *hull;	/* helper pointers to ease the expression */


	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	/* Test input argument */
	csibyl_assert2(cHull != NULL, 92000);

	/* unpack the struct */
	nTotMarkers = cHull->nTotMarkers;
	nMarkers    = cHull->nMarkers;
	hull 	    = cHull->hull;
	idx         = cHull->idx;

	/* Test for consistency */
	csibyl_assert2(0 < nTotMarkers, 92001);
	csibyl_assert2(0 < nMarkers   , 92002);


	/* Modified and adapted version from:
	 *  https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain#C++ */

	/* Now we perform a sort operation, to sort them lexocographic */
	csibyl_sortPoints(cHull);
	cHull->nHull = 0;	/* set number markers in the hull to zero */

	/* Handle the case were we have less or equal three points
	 * In this case the cpmvex hull is the same as the full set
	 * of points */
	if(nMarkers <= 3)
	{
		CSIBYL_CALL(csibyl_sortPoints, cHull);

		/* Full up the hull array, first coping the index in the idx set. */
		for(i = 0; i != nMarkers; ++i)
		{
			ii = cHull->idx[i];
			csibyl_assert2(0 <= ii, 92100);
			csibyl_assert2(ii < nMarkers, 92101);

			cHull->hull[i] = ii;
		}; //Copy the list

		/* put the first marker again at the end */
		cHull->hull[nMarkers] = cHull->idx[0];

		/* set the size */
		cHull->nHull = nMarkers + 1;

		/* exit the computations */
		cLibSibylRT_exit(0);
	}; /* end: handling trivial case */

	/* if we are here then we have to perform the full algorithm
	 * The points have already been sorted */

	/*
	 * Build lower hull
	 */
	k = 0;
	for(i = 0; i < nMarkers; ++i)
	{
		errCode = 0;

		/* find the next marker */
		while((k >= 2) && csibyl_cHull_cross(hull[k-2], hull[k-1], idx[i], cHull, &errCode) <= 0)
		{
			/* test if an error has happened, if so then we abort here */
			if(errCode != 0)
			{
				/* Note the error is already installed */
				cLibSibylRT_exit(errCode);
			};
			k--;
		}; /* end while: searching for the new point */

		if(errCode != 0)
		{
			/* Note the error is already installed */
			cLibSibylRT_exit(errCode);
		};

		/* test if we can store the additional marker */
		if(cHull->nHull >= nMarkers)
		{
			cLibSibylRT_exit2(92201, "The hull array is too small.");
		};

		/* now we save the marker into the list */
		hull[k]      = idx[i];
		cHull->nHull = k + 1;	/* remmber case k == 0 */
		k += 1;
	}; /* end for(i): */

	/*
	 * Build the upper hull
	 */
	for(i = nMarkers - 1, t = k+1; i > 0; --i)
	{
		errCode = 0;
		while (k >= t && csibyl_cHull_cross(hull[k-2], hull[k-1], idx[i-1], cHull, &errCode) <= 0)
		{
			/* test if an error has happened, if so then we abort here */
			if(errCode != 0)
			{
				/* Note the error is already installed */
				cLibSibylRT_exit(errCode);
			};
			k--;
		}; /* end while */

		if(errCode != 0)
		{
			/* Note the error is already installed */
			cLibSibylRT_exit(errCode);
		};

		/* test if we can store the additional marker */
		if(cHull->nHull >= nMarkers)
		{
			cLibSibylRT_exit2(92301, "The hull array is too small.");
		};

		/* store the new point in the list */
		hull[k]      = idx[i - 1];
		cHull->nHull = k + 1;
		k += 1;
	} /* end for(i): */

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
}; /* End: convex hull */



/*
 * ======================
 * Sorting
 */

/*
 * \brief	This is the compare function.
 *
 * This functin returns a negatve value if the first argument
 * is less then the second a positive argument if the reverse
 * is true. Zero is returned if the two are the same
 * It exoects pointers to an index.
 *
 * \param  lhs		The left argument.
 * \param  rhs		The right argument.
 * \param  ctx		The context.
 */
CSIBYL_INTERN
int
csibsl_hull_cmp(
	const void* 	lhs_,
	const void* 	rhs_,
	      void* 	ctx_)
{
	const Int_t lhs = *((Int_t*)lhs_);
	const Int_t rhs = *((Int_t*)rhs_);
	const struct sibyl_convexHull_t* const cHull = (const struct sibyl_convexHull_t*)ctx_;

	const Numeric_t lhs_x = cHull->xPos[lhs];
	const Numeric_t lhs_y = cHull->yPos[lhs];
	const Numeric_t rhs_x = cHull->xPos[rhs];
	const Numeric_t rhs_y = cHull->yPos[rhs];

	if((lhs_x == rhs_x) && (lhs_y == rhs_y))
		{ return 0; };

	if(lhs_x < rhs_x)
	{
		return -1;
	}
	else if((lhs_x == rhs_x) && (lhs_y < rhs_y))
	{
		return -1;
	}
	else
	{
		return 1;
	};
}; //End: compare function


/* This is the actuall sort function */
Int_t
csibyl_sortPoints(
	struct sibyl_convexHull_t* 	cHull)
{
	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	if(cHull == NULL)
		{ cLibSibylRT_exit2(90001, "The cHull pointer is NULL."); };

	qsort_r((void*)(cHull->idx), cHull->nMarkers, sizeof(Int_t), csibsl_hull_cmp, (void*)cHull);

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
}; /* end sort points */



/*
 * ====================
 * Memory Managment
 */

struct sibyl_convexHull_t*
csibyl_allocCHull(
	const Numeric_t* const restrict 	xPos_,
	const Numeric_t* const restrict 	yPos_,
	const Int_t 				nTotMarkers_,
	const Int_t*     const restrict		subSet,
	const Int_t 				nSubSet)
{
	if(xPos_ == NULL)
		{ cLibSibylRT_panic("Passed the NULL ptr for the xPos."); };
	if(yPos_ == NULL)
		{ cLibSibylRT_panic("Passed the NULL ptr for the yPos."); };
	if(subSet == NULL)
	{
		if(nSubSet > 0)
			{ cLibSibylRT_panic("No subset passed, but nSubSet is not zero."); };
	}
	else
	{
		if(nSubSet < 0)		/* allow zero */
			{ cLibSibylRT_panic("Subset passed but invalid size value."); };
		if(nTotMarkers_ < nSubSet)
			{ cLibSibylRT_panic("Subset is larger than nTot."); };
	};
	if(nTotMarkers_ <= 0)
		{ cLibSibylRT_panic("nTotMarkers is negative or zero."); };
	/* End check */

	/* allocate the structure
	 * This call wll set all to NULL, guaranteed to succeed */
	struct sibyl_convexHull_t* cHull = cLibSibyl_calloc(sizeof(struct sibyl_convexHull_t));

	/* Storing the point informations */
	cHull->xPos        = xPos_;
	cHull->yPos        = yPos_;
	cHull->nTotMarkers = nTotMarkers_;

	/* Handle the index array */
	if((subSet != NULL) && (nSubSet != 0))
	{
		/* The index array was given */
		cHull->nMarkers = nSubSet;
		cHull->idx      = cLibSibyl_iArray(nSubSet);

		/* copy the array */
		(void)memmove(cHull->idx, subSet, nSubSet * sizeof(Int_t));
	}
	else
	{
		/* The array was not given, so we have to use all */
		cHull->nMarkers = nTotMarkers_;
		cHull->idx      = cLibSibyl_iArray(nTotMarkers_);

		/* setting the range */
		for(Int_t i = 0; i != nTotMarkers_; ++i)
			{ cHull->idx[i] = i; };
	}; /* end if: filling the index */

	/* Now allocating the hull parameter */
	cHull->nHull = 0;					/* nothing is in the convex hull yet */
	cHull->hull  = cLibSibyl_iArray(cHull->nMarkers + 1);	/* one more than markers */

	/* set all the parameter to -1, to indicate that not used yet */
	const Int_t nHullSize = cHull->nMarkers + 1;
	for(Int_t i = 0; i != nHullSize; ++i)
		{ cHull->hull[i] = -1; };

	/* Return */
	return cHull;
}; /* End: allocate */


void
csibyl_freeCHull(
	struct sibyl_convexHull_t** 	cHull_)
{
	if(cHull_ == NULL)
		{ cLibSibylRT_panic("Passed a NULL pointer pointer to the free function."); };

	/* test if the real pointer is NULL, if so we are done */
	if((*cHull_) == NULL)
		{ return; };

	struct sibyl_convexHull_t* cHull = *cHull_; 	/* make an alias */

	/* This function is ghuaraneteed to work with
	 * partiall allocated sttrucure, since cHull is
	 * is allocated using calloc, the pointers are
	 * null and safe to delete */

	cLibSibyl_iFree(&(cHull->hull));
	cHull->nHull = 0;

	cLibSibyl_iFree(&(cHull->idx));
	cHull->nMarkers = 0;

	cLibSibyl_free((void**)cHull_); /* Free the struct itself */
	*cHull_ = NULL;

	return;
}; /* end free cHull structuire */




