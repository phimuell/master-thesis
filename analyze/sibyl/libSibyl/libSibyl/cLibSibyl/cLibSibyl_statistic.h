/**
 * \brief	This file provides statitsical functions.
 */
#ifndef CLIBSIBYL_STATISTIC_HEADER
#define CLIBSIBYL_STATISTIC_HEADER


/* Includes */
#include "cLibSibyl_types.h"
#include "cLibSibyl_runtime.h"


#include <math.h>



/*
 * \brief	This struct stores the state for the mean algorithm.
 * \struct 	cSibyl_meanState_t
 *
 * This class implements the state for Wellford's algorithm.
 * This is a stable algorithm
 */
struct cSibyl_meanState_t
{
	Real_t 		m_mean;
	Real_t 		m_M2;
	Size_t 		m_count;
}; /* end: struct(cSibyl_meanState_t) */


/*
 * \brief	This structure stores an array of states.
 * \struct 	cSibyl_meanStateArray_t
 *
 * This struct contains many states.
 * There is an initialization macro, that should be used.
 * It will create a state that is safe to dealocate anytime.
 */
struct cSibyl_meanStateArray_t
{
	struct cSibyl_meanState_t* 	m_states;
	Index_t				m_nStates;
}; /* end: struct(cSibyl_meanState_t) */




/*
 * ======================
 * Single state
 *
 * All these functions operates on a single state.
 */


/*
 * \brief	This function initializes a state.
 *
 * This function returns zero on success.
 *
 */
static
Int_t
cSibyl_initMeanState(
	struct cSibyl_meanState_t* const 	state)
{
	if(state == NULL)
		{return 1;};

	state->m_count = 0;
	state->m_mean  = 0.0;
	state->m_M2    = 0.0;

	return 0;
}; /* End: init mean state */





/*
 * \brief	This function will add a new number to the state.
 *
 * This function returns zero upon success and a non zero value on error.
 *
 * \param  state		The state that should be updated.
 * \param  newVal		The new value that should be incooperated.
 */
static
Int_t
cSibyl_updateMeanState(
	struct cSibyl_meanState_t* const 	state,
	const Numeric_t 			newVal)
{
	if(state == NULL)
		{ return 1; };
#if !defined(NDEBUG)
	if(isfinite(newVal) == 0)
		{ return 2; };
#endif
	/* Load the state from the struct */
	Size_t count = state->m_count;
	Real_t mean  = state->m_mean;
	Real_t M2    = state->m_M2;

	/* Update, see Python code on wikipeda */
	             count += 1;
	const Real_t delta  = newVal - mean;
    	             mean  += delta / count;
	const Real_t delta2 = newVal - mean;
    			M2 += delta * delta2;

#if !defined(NDEBUG)
	if(isfinite(mean) == 0)
		{ return 3; };
	if(isfinite(M2) == 0)
		{ return 4; };
#endif

    	/* Now write the state back */
    	state->m_count = count;
    	state->m_mean  = mean;
    	state->m_M2    = M2;

    	return 0;
}; /* End: update a single state */


/**
 * \brief	This function returns the numbers of observations.
 *
 * \param  state 	The state on which we sould operate on.
 * \param  nObs   	Argument for passing the number of observations arround.
 */
static
Int_t
cSibyl_getCount(
	const struct cSibyl_meanState_t* const 	state,
	Index_t* const 				nObs)
{
	if(state == NULL)
		{ return 1; };
	if(nObs == NULL)
		{ return 2; };

	*nObs = state->m_count;

	return 0;
}; /* End: get numbers of observations */


/**
 * \brief	This function will extract the mean from the state.
 *
 * This function returns a zero on success.
 *
 * \param  state 	The state on which we sould operate on.
 * \param  mean   	Argument for passing the mean arround.
 */
static
Int_t
cSibyl_getMean(
	const struct cSibyl_meanState_t* const 	state,
	Numeric_t* const 			mean)
{
	if(state == NULL)
		{ return 1; };
	if(mean == NULL)
		{ return 2; };

	*mean = NAN;	/* Setting the return value to a nonsensical value */

	if(state->m_count == 0)
		{ return 3; };
	if(state->m_count < 0)
		{ return 4; };
#if !defined(NDEBUG)
	if(isfinite(state->m_mean) == 0)
		{ return 5; };
#endif

	*mean = state->m_mean;

	return 0;
}; /* End: get mean */



/*
 * \brief	This function returns the sample std.
 *
 * This function returns a zero on success.
 *
 * \param  state 	The state on which we sould operate on.
 * \param  sStd   	Argument for passing the sample standard deviation arround.
 */
static
Int_t
cSibyl_getSampleStd(
	const struct cSibyl_meanState_t* const 	state,
	Numeric_t* const 			sStd)
{
	if(state == NULL)
		{ return 1; };
	if(sStd == NULL)
		{ return 2; };

	*sStd = NAN;	/* Set the return argument to a non sensical value */

	if(state->m_count == 0)
		{ return 3; };
	if(state->m_count <  0)
		{ return 4; };
#if !defined(NDEBUG)
	if(isfinite(state->m_M2) == 0)
		{ return 5; };
#endif

	/* Depending on the number of samples we will use special
	 * definition to still be able to compute the std */
	if(state->m_count == 1)
	{
		/* Just one recond, we define the std as zero in this case */
		*sStd = 0.0;
	}
	else
	{
		/* We can estimate it */
		const Real_t sVar = (state->m_M2) / ((state->m_count) - 1);
		*sStd = sqrt(sVar);
	};

#if !defined(NDEBUG)
	if(isfinite(*sStd) == 0)
		{ return 6; };
#endif

	return 0;
}; /* END: get sample standard deviation */




/*
 * =============================
 * ARRAY
 *
 * These functions operates on arraies instead of simple state.
 * There is a special struct for representing it.
 *
 * The array functions simply passes the desired state to the
 * respective functions. They will return zero on success.
 * A negative number is returned if an error in indexing is
 * detected. A positive number insicates an error in the
 * calculations.
 */


/*
 * \brief	This macro initializes an meanStateArray struct, but does not allocate.
 * \define	CSIBYL_MEAN_ARRAY_INIT
 *
 * Use this in an initialization statement to get a valid array,
 * that can be passed safly to free.
 */
#define CSIBYL_MEAN_ARRAY_INIT { .m_nStates = 0, .m_states = NULL }



/*
 * \brief	This function will create an array of mean states.
 *
 * The memory is allocated and initialized. The array that is passed
 * is modified, memory that may be allocated is not freed.
 * This function never fails.
 *
 * Note the array structure itself is not allocated.
 *
 * \param  N 		Number of states that should be created.
 * \param  states	Target for allocation.
 */
extern
CSIBYL_INTERN
void
cSibyl_allocMeanStateArray(
 	Index_t					N,
 	struct cSibyl_meanStateArray_t* 	states)
 /* CSIBYL_ATTR_MALLOC (because not returned) */;


/**
 * \brief	This function frees an state array.
 *
 * The array structure itslef is not freed, only
 * the referenced memory.
 *
 * \param  states 	Array that should be modified.
 */
extern
CSIBYL_INTERN
void
cSibyl_freeMeanStateArray(
	struct cSibyl_meanStateArray_t* 	states);


/**
 * \brief	This function also updates the state,
 * 		 however it operates on an array.
 *
 * This function will return zero on success. Positive value
 * upon error in the updateing, and negative values if
 * an error in indexing occured.
 *
 * \param  state	Pointer to the array of states.
 * \param  i 		The state that should be updated.
 * \param  newVal	The new value that should be incooperated.
 */
static
Int_t
cSibyl_updateMeanStateArray(
	struct cSibyl_meanStateArray_t* const 	states,
	const Index_t 				i,
	const Numeric_t 			newVal)
{
	if(states == NULL)
		{ return -1; };
#if !defined(NDEBUG)
	if(states->m_states == NULL)
		{ return -4; };
	if(states->m_nStates <= 0)
		{ return -5; };
#endif
	if(i < 0)
		{ return -2; };
	if((states->m_nStates) <= i)
		{ return -3; };

	/* Now we simply call the underlying function */
	return cSibyl_updateMeanState(states->m_states + i, newVal);
}; /* end: update one from the array */


/*
 * \brief	This function returns the number of observations
 * 		 in the ith state.
 *
 * It only operates on one state not on all states.
 *
 * \param  states	Pointer to the array of states.
 * \param  i 		The state that should be updated.
 * \param  nObs		Argument for passing the mean arround.
 */
static
Int_t
cSibyl_getCountArray(
	const struct cSibyl_meanStateArray_t* const 	states,
	const Index_t 					i,
	Index_t* const 					nObs)
{
	if(states == NULL)
		{ return -1; };
#if !defined(NDEBUG)
	if(states->m_states == NULL)
		{ return -4; };
	if(states->m_nStates <= 0)
		{ return -5; };
#endif
	if(i < 0)
		{ return -2; };
	if((states->m_nStates) <= i)
		{ return -3; };

	/* Now we simply call the underlying function */
	return cSibyl_getCount(states->m_states + i, nObs);
}; /* END: get Mean of an array */


/*
 * \brief	This function will return the mean of the ith
 * 		 state in the array.
 *
 * It only operates on one state not on all states.
 *
 * \param  states	Pointer to the array of states.
 * \param  i 		The state that should be updated.
 * \param  mean		Argument for passing the mean arround.
 */
static
Int_t
cSibyl_getMeanArray(
	const struct cSibyl_meanStateArray_t* const 	states,
	const Index_t 					i,
	Numeric_t* const 				mean)
{
	if(states == NULL)
		{ return -1; };
#if !defined(NDEBUG)
	if(states->m_states == NULL)
		{ return -4; };
	if(states->m_nStates <= 0)
		{ return -5; };
#endif
	if(i < 0)
		{ return -2; };
	if((states->m_nStates) <= i)
		{ return -3; };

	/* Now we simply call the underlying function */
	return cSibyl_getMean(states->m_states + i, mean);
}; /* END: get Mean of an array */


/*
 * \brief	This function will return the sample std of the ith
 * 		 state in the array.
 *
 * It only operates on one state not on all states.
 *
 * \param  states	Pointer to the array of states.
 * \param  i 		The state that should be updated.
 * \param  sStd		Argument for passing the sample standard deviation arround.
 */
static
Int_t
cSibyl_getSampleStdArray(
	const struct cSibyl_meanStateArray_t* const 	states,
	const Index_t 					i,
	Numeric_t* const 				sStd)
{
	if(states == NULL)
		{ return -1; };
#if !defined(NDEBUG)
	if(states->m_states == NULL)
		{ return -4; };
	if(states->m_nStates <= 0)
		{ return -5; };
#endif
	if(i < 0)
		{ return -2; };
	if((states->m_nStates) <= i)
		{ return -3; };

	/* Now we simply call the underlying function */
	return cSibyl_getSampleStd(states->m_states + i, sStd);
}; /* END: get Mean of an array */


#endif  /* END GUARD */

