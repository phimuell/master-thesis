/**
 * \brief	This file contains the implementation of the static
 * 		 function for finding the associated index.
 *
 * This function is implemented in its own file, because it is a very
 * long function, that should be located seperatly.
 *
 */
#ifndef CLIBSIBYL_INTERNALS_STATIC_FINDASSOCIDX
#define CLIBSIBYL_INTERNALS_STATIC_FINDASSOCIDX

/* Includes */
#include "./cLibSibyl_internal.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_assert.h"

#include <math.h>

#if defined(CSIBYL_NO_CHECKS_IN_FINDIDX) && (CSIBYL_NO_CHECKS_IN_FINDIDX != 0)
#	pragma message "This isnstance of the findAssocIdx function is compiled with disabled checks."
#endif


/*
 * ===================================
 * IMPL of static functions
 */
Index_t
cLibSibyl_findAssocIdx_(
	const Numeric_t 		pos,
	const Numeric_t* restrict 	gPos,
	const Int_t 			nG,
	const Bool_t 			isTight)
{
	Index_t count = nG, step = -1, it = 0, first = 0;
	Numeric_t firstPos, lastPos;

	CSIBYL_RT_FUNC_START;

#	if !(defined(CSIBYL_NO_CHECKS_IN_FINDIDX) && (CSIBYL_NO_CHECKS_IN_FINDIDX != 0))
	csibyl_assert2(nG > 1, -199);
	csibyl_assert2(isfinite(pos), -198);
#	endif

	/* Load some parameters */
	firstPos = gPos[0     ];
	lastPos  = gPos[nG - 1];

#	if !(defined(CSIBYL_NO_CHECKS_IN_FINDIDX) && (CSIBYL_NO_CHECKS_IN_FINDIDX != 0))
	/* Test if the values are valid */
	csibyl_assert2(isfinite(firstPos), -200);
	csibyl_assert2(isfinite(lastPos ), -201);
#	endif

	/* Test if we have an error */
	if(pos < firstPos)
		{cLibSibylRT_exit(-1);};

	if(isTight != FALSE)
	{
		if(lastPos < pos)
			{cLibSibylRT_exit(-2);};
	};

	/* Handle the case of equal last one */
	if(lastPos <= pos)	/* this is a sinn */
		{return (nG - 1);};


	/* Now we use binary search,
	 * note that this will give use the first index
	 * that is not less than pos
	 * Code adapted from cppreference.com */
    	while (count > 0)
    	{
        	it = first;
        	step = count / 2;
        	it += step;
        	if(gPos[it] < pos)
		{
            		first = ++it;
            		count -= step + 1;
            	}
        	else
		{
            		count = step;
		};
    	}; /* END: while */
    	it = first;

    	/* The above code, finds the first index (it) that is not less than
    	 * it, so we have to modify it.
    	 */
    	if(gPos[it] != pos)	/* also possible gPos[it] >= pos */
	{
		it -= 1;
	};

#	if !(defined(CSIBYL_NO_CHECKS_IN_FINDIDX) && (CSIBYL_NO_CHECKS_IN_FINDIDX != 0))
	csibyl_assert2(0        <= it,  -101);
	csibyl_assert2(it       <  nG , -102);
	csibyl_assert2(gPos[it] <= pos, -103);
	csibyl_assert2((it < (nG-1)) ? (pos < gPos[it + 1]) : 1, -104);
#	endif

	/* Normal exit */
	return it;

	/* Special exit for the assert */
	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
}; /* END: find associated index */


#endif /* end guard */

