/**
 * \brief	This file provides a custum assert.
 *
 * The problem is that if an assert triggers the interpreter will die.
 * Which is not good, so a custom assert is provided to circumite this.
 */
#ifndef CLIBSIBYL_ASSERT_HEADER
#define CLIBSIBYL_ASSERT_HEADER

#ifdef CSIBYL_STRINGIFY_INTERNAL
#	undef CSIBYL_STRINGIFY_INTERNAL
#endif
#define CSIBYL_STRINGIFY_INTERNAL(e) #e


/**
 * \brief	This macro will expand to a test, that will abort the process in a safe manner.
 * \define 	csibyl_assert
 *
 * If the passed confition cond fail, this function will call the ext function of the cLibSibyl
 * runtime, with the reserved exit code -9999.
 * If a custom exit code is needed, use the assert2 macro.
 * If NDEBUG is defined, the macro expands to "{}".
 *
 *
 * \param  cond 	The condition that should hold.
 */
#if defined(NDEBUG)
#	define csibyl_assert(cond) {}
#else
#	define csibyl_assert(cond) if(!(cond)) { cLibSibylRT_exit2(-99999, "!(" CSIBYL_STRINGIFY_INTERNAL(cond) ")"); }
#endif


/**
 * \brief	This macro allows to specify a condition and a custom error code.
 * \define	csibyl_assert2
 *
 * This allows to distinguish different errors.
 *
 * \param  cond 	The condition that should hold.
 * \param  id		The id of the error.
 */
#if defined(NDEBUG)
#	define csibyl_assert2(cond, id) {}
#else
#	define csibyl_assert2(cond, id) if(!(cond)) { cLibSibylRT_exit2(id, "!(" CSIBYL_STRINGIFY_INTERNAL(cond) ")"); }
#endif


/**
 * \brief	This macro is simmilar to assert,
 * 		 but is always active.
 *
 * This macro can be used to enforec some conditions.
 * It is like the assert macro, but is not influenced
 * by the state of NDEBUG, use with care.
 *
 * \param cond 		The condition that should be tested.
 * \param errCode	The error code that should be used.
 */
#define csibyl_enforce2(cond, id) if(!(cond)) { cLibSibylRT_exit2(id, "!(" CSIBYL_STRINGIFY_INTERNAL(cond) ")"); }


/**
 * \brief	Like the enforce2 macro, but uses
 * 		 the default enforce code.
 */
#define csibyl_enforce(cond) csibyl_enforce2(cond, -99998)


#undef CSIBYL_STR

#endif  /* END GUARD */

