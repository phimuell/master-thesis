/**
 * \brief	This file contains a function that is able to compute the theoretical extension of the markers.
 */

#include "./cLibSibyl.h"
#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_statistic.h"
#include "./cLibSibyl_internal.h"
#include "./cLibSibyl_internal_static.h"

#include "./cLibSibyl_binAzimuthVel_help.h"

#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <float.h>


Int_t
cLibSibyl_binAzimuthVel(
	const Numeric_t* const restrict 	X,		/* IN;  len = nMarkers 	*/
	const Numeric_t* const restrict 	Y,		/* IN;  len = nMarkers 	*/
	const Numeric_t* const restrict 	VX,		/* IN;  len = nMarkers 	*/
	const Numeric_t* const restrict 	VY,		/* IN;  len = nMarkers 	*/
	const Int_t 				nMarkers,	/* IN 			*/
	const Numeric_t* const restrict 	binEdges,	/* IN;  len = nBins + 1	*/
	const Int_t 				nBins,		/* IN 			*/
	      Numeric_t* const restrict 	binMean,	/* OUT; len = nBins 	*/
	      Numeric_t* const restrict 	binStd,		/* OUT; len = nBins 	*/
	const Numeric_t* const restrict 	orig,		/* IN;  len = 2; opt.	*/
	      Int_t*     const restrict 	nProcess,	/* OUT; opt.		*/
	      Int_t* 	 const restrict 	nLow,		/* OUT;	opt.		*/
	      Int_t* 	 const restrict 	nUp)		/* OUT; opt.		*/
{
	/* Main variables */
	Numeric_t x, y, vx, vy, uTheta, r;		  /* marker specific values */
	Int_t     iBin;					  /* index for bin (multiple uses) */
	Numeric_t xOrg = 0.0, yOrg = 0.0;		  /* origin that is used */
	Int_t nProcessed = 0, nTooLow = 0, nTooLarge = 0; /* Statistic during processing */
	struct cSibyl_meanStateArray_t			  /* State of the statistic in a bin */
		binStates = CSIBYL_MEAN_ARRAY_INIT;

	/* helper variables */
	Int_t m;					  /* iteration */
	Numeric_t tmp_rx, tmp_ry, tmp_r2;		  /* Temporal calculations */
	Numeric_t sMean, sStd;				  /* extraction */
	Index_t   nObs;					  /* extraction */

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	if(nMarkers < 0)
		{ cLibSibylRT_exit2(10, "Too few markers."); };
	if(nBins < 2)
		{ cLibSibylRT_exit2(11, "Too few bins."); };
	if(X  == NULL)
		{ cLibSibylRT_exit2(12, "Pointer to X is NULL."); };
	if(Y  == NULL)
		{ cLibSibylRT_exit2(13, "Pointer to Y is NULL."); };
	if(VX == NULL)
		{ cLibSibylRT_exit2(14, "Pointer to VX is NULL."); };
	if(VY == NULL)
		{ cLibSibylRT_exit2(15, "Pointer to VY is NULL."); };
	if(binEdges == NULL)
		{ cLibSibylRT_exit2(16, "Pointer to bin edges is NULL."); };
	if(binMean == NULL)
		{ cLibSibylRT_exit2(17, "Pointer to binMean is NULL."); };
	if(binStd == NULL)
		{ cLibSibylRT_exit2(17, "Pointer to binStd is NULL."); };

	/* If orig is given set them */
	if(orig != NULL)
	{
		xOrg = orig[0]; 	csibyl_assert2(isfinite(xOrg), 20);
		yOrg = orig[1]; 	csibyl_assert2(isfinite(yOrg), 21);
	}; /* end: load origin */

	/* allocate the states for the bins */
	cSibyl_allocMeanStateArray(nBins, &binStates);

	/* Iterating through the markers to determine where
	 * tehy are located. */
	for(m = 0; m != nMarkers; ++m)
	{
		/* Load the current marker */
		 x =  X[m];	csibyl_assert2(isfinite( x), 10001);
		 y =  Y[m];	csibyl_assert2(isfinite( y), 10002);
		vx = VX[m];	csibyl_assert2(isfinite(vx), 10003);
		vy = VY[m];	csibyl_assert2(isfinite(vy), 10004);

		/* calculate to distance to the origin */
		tmp_rx = (x - xOrg);
		tmp_ry = (y - yOrg);
		tmp_r2 = (tmp_rx * tmp_rx) + (tmp_ry * tmp_ry);
		r      = sqrt(tmp_r2);

		/* Test if r iss too small to be handled accuratly */
		if(r < (100 * DBL_EPSILON))
			{ continue; };

		/* find the bin in which the marker belongs */
		iBin = cSibyl_aziVelHelp_findBin(binEdges, r, nBins);
			csibyl_assert2(iBin < nBins, 11000);

		if(iBin < 0)
		{
			if(iBin == -1)
			{
				nTooLow += 1;
				continue;
			}
			else if(iBin == -2)
			{
				nTooLarge += 1;
				continue;
			}
			else
			{
				cLibSibylRT_exit2(11001, "Failed to find the markers bin.");
			};
		}; /* END if: special situation */

		/* Increase the count */
		nProcessed += 1;

		/* We now need to calclate the velocity.
		 * we will first caluclate the angular velocity, kind of and then
		 * the azimuth velocity.
		 *
		 * 	\omega     = (\Vek{\omega})_{z}) = ( \frac{ \Vek{r} \cross \Vek{v} }{ r^2 } )_{z}
		 * 	u_{\theta} = r \cdot \omega
		 */
		uTheta = (x * vy - y * vx) / r;

		/* New we update the state */
		cSibyl_updateMeanStateArray(&binStates, iBin, uTheta);
	}; /* End for(m): */


	/* Now we process extract the data form the state and store
	 * them in the output variables */
	for(iBin = 0; iBin != nBins; ++iBin)
	{
		/* get the number of observations */
		CSIBYL_MCALL("Failed to extract the number of observations.",
				cSibyl_getCountArray, &binStates, iBin, &nObs);
			csibyl_assert2(nObs >= 0, 11001);

		/* No observation, thus it is zero
		 * This is not so a good ide, but the best idea I can come up with */
		if(nObs == 0)
		{
			binMean[iBin] = 0.0;
			binStd [iBin] = 0.0;
		}; /* end if: no observations */

		/* Extract mean and std */
		CSIBYL_MCALL("Failed to extract the mean.",
				cSibyl_getMeanArray, &binStates, iBin, &sMean);
		CSIBYL_MCALL("Failed to extract the std.",
				cSibyl_getSampleStdArray, &binStates, iBin, &sStd);

		/* Write back */
		binMean[iBin] = sMean;
		binStd [iBin] = sStd;
	}; /* End for(iBin): extracting */

	/* If the values are passed wirite them back */
	if(nProcess != NULL)
		{ *nProcess = nProcessed; };
	if(nUp != NULL)
		{ *nUp = nTooLarge; };
	if(nLow != NULL)
		{ *nLow = nTooLow; };


	/* Exit and cleanup code */
	CSIBYL_RT_FUNC_CLEANUP:
	cSibyl_freeMeanStateArray(&binStates);

	CSIBYL_RT_FUNC_RETURN;
}; /* END: bin azimuth */

