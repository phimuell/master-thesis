/*
 * This file is contains some code that is associated to the assert functionality of sibly
 *
 */

#if defined(NDEBUG)
#	pragma message "Asserts are deactivated"
#else
#	pragma message "Asserts are activated"
#endif
