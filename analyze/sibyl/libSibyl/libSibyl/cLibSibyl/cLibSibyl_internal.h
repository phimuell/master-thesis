/**
 * \brief	This file defines functions and macros, that are used internaly inside CSIBYL.
 *
 * These are functions that are usefull in other functions, but note usefull to export to PYTHON.
 *
 * Note that some functions are declared static. That are function that are hotspots. Declaring
 * them static allows the compiler to "inline" them or addapt them.
 * They are implemented in a different file, that is automatically included by this file.
 * Note that some file, especially the small mathematical helper functions, are not declared
 * here, but are defined directly in the external file. This is done to keep this file clean.
 */
#ifndef CLIBSIBYL_INTERNALS_HEADER
#define CLIBSIBYL_INTERNALS_HEADER

/* Includes */
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"

#include <math.h>


/*
 * ========================
 * Control Macros
 *
 * These macro can control how the internal function
 * compiles.
 */


/**
 * \brief	This Macro allows to disabl all tests inside the
 * 		 find assoc index function.
 * \define	CSIBYL_NO_CHECKS_IN_FINDIDX
 *
 * If this macro is defined and is set to non zero.
 * Note that not all checks will be disabled.
 * So it is still possible that the function
 * returns a negative value.
 */
/*
#define CSIBYL_NO_CHECKS_IN_FINDIDX 1
*/



/*
 * ========================
 * Functions
 */


/**
 * \brief	This function is able to find the associated index of a marker.
 *
 * This function only works in one dimension. It determines the index to which
 * it is associated. This is it returns the index that points to the largest
 * value that is smaller or equal the position.
 * The array of grid node positions is expected to be ordered.
 *
 * This function assumes that the no marker position can be smaller than the first
 * grid point, and an error is generated if so, -1, is returned.
 *
 * The behaviour if the marker position is larger than the last node depends.
 * It isTight is FALSE, then it belongs to the largest grid point. If isTight
 * is TRUE, then it is assumed that no marker position can be larger than the
 * last grid node, in this case, -2, is returned.
 * Note if pos is equal the last position it is considered associated to the last one.
 *
 * \return 	If this function generated an error a negative number, less than zero,
 * 		 will be rturned, on success a positive number will be returned, that
 * 		 indicates the grid node to which the marker is associated to.
 *
 * \param  pos		The marker positions.
 * \param  gPos		Sorted array of grid nodes.
 * \param  nG 		Number of grid poinst.
 * \param  isTight	Indicates if the grid node position is considered tight.
 *
 * \note	The macro CSIBYL_NO_CHECKS_IN_FINDIDX has an effect on the checking
 * 		 that are performed in this functon.
 */
static
Index_t
cLibSibyl_findAssocIdx_(
	const Numeric_t 		pos,
	const Numeric_t* restrict 	gPos,
	const Int_t 			nG,
	const Bool_t 			isTight)
 CSIBYL_ATTR_HOT
 CSIBYL_ATTR_UNUSED;


/**
 * \brief	This function computes the diff of x.
 *
 * The input array x has the length N, the output array d, has the length
 * N-1. Each component of d is defined as:
 * 	d[i] := x[i+1] - x[i]
 *
 * Zero is returned upon success.
 *
 * \param  x	The source array.
 * \param  N 	The length of x
 * \param  d	The differecne array.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_diff_(
	Numeric_t* restrict 	x,
	Int_t 			N,
	Numeric_t* restrict	d);


/**
 * \brief	This function returns the number of caracters in the
 * 		 string pointed to by s.
 *
 * The null character will be excluded.
 * If s is the null pointer zero will be returned. A negative value
 * will be returned if the string was not found within the first
 * n characters.
 *
 * \param  s		The string to scan.
 * \param  n		The number of char to scan, -1 for without limits.
 */
extern
CSIBYL_INTERN
Int_t
cLibSibyl_strLen_(
	const Char_t*const  		s,
	Int_t 				n);


#endif /* end guard */



/*
 * Include the static implementations
 */
#include "./cLibSibyl_internal_static.h"


