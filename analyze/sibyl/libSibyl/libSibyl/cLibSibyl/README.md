# Info
This folder contains the code for the parts of SIBYL that are written in C.
It is loaded into SIBYL by using the CTYPE interface system.

Not it is written in PURE C, with an archaic flavour.
Because every time I write C, I remember that "C, is a spartan language".
At least Linus said this, before he decided that he needs mental healthcare.


## DEBUG
If you execute the Makefile in this folder, then the debug constructs are activated by default.
It is possible to deactivate them by supplying "NO_DEBUG=1" to make.
Setting it to zero, will reactivate them.

This is different from the Makefile in the parent folder, which will deactivate the debug facilities.
This behaviour can not be changed.



