/**
 * \brief	This file contains the function to compute the pressure force.
 */

#include "./cLibSibyl.h"
#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_convexHull.h"
#include "./cLibSibyl_forces.h"
#include "./cLibSibyl_convexHull_ops.h"

#include <stdlib.h>
#include <math.h>


Int_t
cLibSibyl_computePressureForce(
	const Numeric_t* const restrict 	x,
	const Numeric_t* const restrict 	y,
	const Int_t 				nMarkers,
	const Int_t*     const restrict 	subSet,
	const Int_t 				nSubSet,
	const Numeric_t* const restrict 	pressure,
	const Numeric_t* const restrict 	xPressNode,
	const Numeric_t* const restrict 	yPressNode,
	const Int_t 				nPressNodeX,
	const Int_t 				nPressNodeY,
	      Numeric_t* const restrict 	pForce,
	      Numeric_t* const restrict 	vol)
{
	struct sibyl_convexHull_t* cHull;	/* the c hull object */

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	csibyl_assert2(x        != NULL, 96001);
	csibyl_assert2(y        != NULL, 96002);
	csibyl_assert2(nMarkers >  0   , 96003);
	csibyl_assert2(xPressNode  != NULL, 96004);
	csibyl_assert2(yPressNode  != NULL, 96005);
	csibyl_assert2(nPressNodeX >  0   , 96006);
	csibyl_assert2(nPressNodeY >  0   , 96007);
	csibyl_assert2(pForce      != NULL, 96008);
	csibyl_assert2(pressure    != NULL, 96009);
	csibyl_assert2(vol         != NULL, 96010);

	/* allocating the chull object */
	cHull = csibyl_allocCHull(x, y, nMarkers, subSet, nSubSet);

	/* compute the convex hull */
	CSIBYL_CALL(csibyl_computeConvexHull, cHull);

	/* compute the forces on the convex hull */
	CSIBYL_CALL(csibyl_convHull_computePressureForce,
			cHull,
			pressure,
			xPressNode, yPressNode,
			nPressNodeX, nPressNodeY,
			pForce);

	/* compute the volume */
	CSIBYL_CALL(csibyl_convecHull_volume,
			vol, cHull);

	/* free the convex hull object */
	csibyl_freeCHull(&cHull);


	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
};/* end:_ compute pressure force on markers */



