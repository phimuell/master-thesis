/**
 * \brief	This file implements a test for the convex hull
 */

#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_convexHull.h"
#include "./cSibylTests.h"
#include "./cLibSibyl_forces.h"
#include "./cLibSibyl_convexHull_ops.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

Int_t
cSibylTest_convexHull(void)
{
	/* parameters
	 */

	volatile Int_t retValue = 0;

	/* Resolution */
	Int_t NX = 300;
	Int_t NY = 300;
	Int_t N  = NX * NY + 6;

	/* geometrical size */
	Numeric_t R  = 0.9;
	Numeric_t dx = 3.0 / NX;
	Numeric_t dy = 3.0 / NY;

	/* Index and counts for filling */
	Int_t sIdx = 0;		/* iundex set */
	Int_t pIdx = 0;		/* position set */

	/* Arrays */
	Numeric_t* X     = cLibSibyl_dArray(N);
	Numeric_t* Y     = cLibSibyl_dArray(N);
	Int_t*     inSet = cLibSibyl_iArray(N);

	for(Int_t ix = 0; ix != NX; ++ix)
	{
		for(Int_t iy = 0; iy != NY; ++iy)
		{
			/* compute the current location */
			const Numeric_t x = dx * (ix + 0.5) - 1.5;
			const Numeric_t y = dy * (iy + 0.5) - 1.5;

			/* add the location to the array of positions */
			X[pIdx] = x;
			Y[pIdx] = y;

			if((x * x + y * y) <= (R * R))
			{
				inSet[sIdx] = pIdx;
				sIdx += 1;
			}; //End if:

			pIdx += 1;
		}; //End for(iy):
	};//End for(ix):

	//this is the number of indexes in the circle
	//it is also the past the end index in the inset map
	const Int_t sIdxCircle = sIdx;

	/* adding the four points which are the convex hull */
#	define ADD(x, y) {X[pIdx] = (x); Y[pIdx] = (y); inSet[sIdx] = pIdx; sIdx++; pIdx++;}
	ADD( 1.,  1.);
	ADD(-1.,  1.);
	ADD( 1., -1.);
	ADD(-1., -1.);
#	undef ADD

	/* create a sibyl structure, with all points also the four corner points */
	struct sibyl_convexHull_t* cHull = csibyl_allocCHull(X, Y, pIdx, inSet, sIdx);

	Int_t eCH = csibyl_computeConvexHull(cHull);
	if(eCH != 0)
	{
		printf("Could not compute the convex hull, error was %d\n", eCH);
		return -10;
	};

	printf("Could compute the convex hull!\n");
	printf("Numbers of points in the hull %d (correct answer 5)\n", cHull->nHull);

	printf("The convex hull is:\n");
	for(Int_t i = 0; i != cHull->nHull; ++i)
	{
		Int_t ii = cHull->hull[i];
		Numeric_t x = cHull->xPos[ii];
		Numeric_t y = cHull->yPos[ii];

		printf("\t%d ~ (%f, %f)\n", i, x, y);
	}; //End for(i):


	//test if this test succeeded
	if(cHull->nHull != 5)
	{
		printf("The computed hull does not have the correct numbers of points, it has %d but expected 5\n", cHull->nHull);
		return -11;
	};


	//Compute the volume
	Numeric_t cHullVol = 0.0;
	Int_t eCHvol = csibyl_convecHull_volume(&cHullVol, cHull);
	if(eCHvol != 0)
	{
		printf("Error ocured during computation of volume %d\n", eCHvol);
		return -40;
	};

	printf("The computed volume of cHull was %f, expected 4.0\n", cHullVol);


	/*
	 * ========================
	 * Integration test
	 *
	 * We will now test the integration test.
	 * We will also change the convex hull, we will use the
	 * ball, if the pressure field is also rotationally symetric.
	 * Then the resulting forces will be zero.
	 */

	//Make a new cHull
	struct sibyl_convexHull_t* cHull2 = csibyl_allocCHull(X, Y, pIdx, inSet, sIdxCircle);

	const Int_t eCHull2 = csibyl_computeConvexHull(cHull2);
	if(eCHull2 != 0)
	{
		printf("Failed to compute the second convex hull, error was %d\n", eCHull2);
		return -12;
	};

	Int_t NPX = NX / 10;
	Int_t NPY = NY / 10;

	Numeric_t* P  = cLibSibyl_dMatrix(NPY, NPX);
	Numeric_t* xP = cLibSibyl_dArray(NPX);
	Numeric_t* yP = cLibSibyl_dArray(NPY);

	Numeric_t dxP = 3.0 / NPX;
	Numeric_t dyP = 3.0 / NPY;

	for(Int_t ix = 0; ix != NPX; ++ix)
	{
		for(Int_t iy = 0; iy != NPY; ++iy)
		{
			/* compute the current location */
			const Numeric_t x = dxP * (ix + 0.5) - 1.5;
			const Numeric_t y = dyP * (iy + 0.5) - 1.5;

			/* add the location to the array of positions */
			xP[ix] = x;
			yP[iy] = y;

			P[NPX * iy + ix] = copysign(x * x + y * y, 1);
		}; //End for(iy):
	};//End for(ix):

	//Compute the volume
	Numeric_t cHullVol2 = 0.0;
	Int_t eCH2vol = csibyl_convecHull_volume(&cHullVol2, cHull2);
	if(eCH2vol != 0)
	{
		printf("Error ocured during computation of volume %d\n", eCH2vol);
		return -40;
	};
	printf("The computed volume of the second hull was %f, expected %f\n", cHullVol2, M_PI * R * R);



	/*
	 * Integrate the pressure force
	 */
	Numeric_t presForce[2];

	const Int_t ePressInt = csibyl_convHull_computePressureForce(cHull2, P, xP, yP, NPX, NPY, presForce);
	if(ePressInt != 0)
	{
		printf("Error while computing the pressure force, error was %d\n", ePressInt);
		return -13;
	};


	printf("\n\nPressure force was computed as: F_x = %f, F_y = %f\n", presForce[0], presForce[1]);
	printf("\tThe expected result is 0.0, due to the simetric problem. But numerical cancelation can generate errors.\n");


	/*
	 * ======================
	 * End and clean up
	 *
	 * Note only performed if everything was correctly.
	 * A clean cleanup is to cmbersom.
	 */
	csibyl_freeCHull(&cHull);
	cLibSibyl_dFree(&X);
	cLibSibyl_dFree(&Y);
	cLibSibyl_iFree(&inSet);
	cLibSibyl_dFree(& P);
	cLibSibyl_dFree(&xP);
	cLibSibyl_dFree(&yP);

	return retValue;
}; /* END convex hull */




