/**
 * \brief	This header contains the functions that are related to calculating forces.
 *
 * This is done on a primitive bases, but is not uitable for python.
 */
#ifndef CLIBSIBYL_FORCES_HEADER
#define CLIBSIBYL_FORCES_HEADER

/* Includes */
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"


/**
 * \brief	This function computes the pressure force on the convex hull.
 *
 * See also cLibSibyl_computePressureForce() function for a better description.
 * In short this function computes the force that acts on the hull.
 *
 * The pressure matrix is assumed to be in row major order. Note that inside EGD they
 * are column major order, but inside CSIBYL, they are row major order. Also we assume
 * that the number or colums is equal the number of x grid points and number of rows
 * is equal y grid points.
 *
 *
 *
 *
 * \param  cHull	The convex hull object.
 * \param  pressure	Pointer to the pressure field.
 * \param  xPressNode	Pointer to the x coordinates of the pressure point.
 * \param  yPressNode	Pointer to the y coordinates of the pressure point.
 * \param  nPressNodeX	Number of pressure nodes in x direction.
 * \param  nPressNodeY	Number of pressure nodes in y direction.
 * \param  pForce	Array of length two to the pressure force.
 */
extern
CSIBYL_INTERN
Int_t
csibyl_convHull_computePressureForce(
	struct sibyl_convexHull_t* 		cHull,
	const Numeric_t* const restrict 	pressure,
	const Numeric_t* const restrict 	xPressNode,
	const Numeric_t* const restrict 	yPressNode,
	const Int_t 				nPressNodeX,
	const Int_t 				nPressNodeY,
	      Numeric_t* const restrict 	pForce);



#endif /* end guard */

