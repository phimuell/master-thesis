/**
 * \brief	This file contains the functions that deals with the implementation of matrix functions.
 */

/* includes */
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_linAlg.h"
#include "./cLibSibyl_memory.h"

#include <math.h>
#include <stdio.h>
#include <assert.h>

/**
 * \brief	Defining the macro and the unroll factor will be 2.
 */
#define CSIBYL_LINAL_SMALL_UNROLL 0


/*
 * ======================
 * BLAS
 */


/**
 * \brief	This function multiplies two matrices together in the way GEMM does.
 *
 * This function basically implements:
 * 	\mat{C} <- \alpha * \Mat{A} \Mat{B} + \beta * \Mat{C}
 *
 * This means C is overwritten. \Mat{A} is a $n \times p$ matrix
 * and \Mat{B} is a $p \times m$ matrix.
 *
 * This function returns zero on success and non zero on failure.
 * It does not modify the internal error state.
 *
 * \param  alpha
 * \param  beta
 * \param  A		Input
 * \param  B 		Input
 * \param  C		Output
 * \param  n		Rows of A and C
 * \param  p		Columns of A and rows of B
 * \param  m 		Columns of B and C
 */
Int_t
cLibSibyl_dGEMM(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	const Numeric_t* restrict 	B,
	      Numeric_t* restrict 	C,
	const Int_t 			n,
	const Int_t 			p,
	const Int_t 			m)
{
	if(!isfinite(gamma) )
		{return 1001;};
	if(A == NULL        )
		{return 1003;};
	if(B == NULL        )
		{return 1004;};
	if(C == NULL        )
		{return 1005;};
	if(n <= 0 || p <= 0 || m <= 0)
		{return 1006;};

	/* Performing the \gamma * \Mat{C} part */
	const Int_t sizeC = n * m;
	const Int_t sizeA = n * p;
	for(Int_t k = 0; k != sizeC; ++k)
		{C[k] *= gamma;};

	/* allocate a helper construct */
	Numeric_t* B_slice = cLibSibyl_dArray(p);

	/* For unrolling */
#	if (defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
	const Int_t  unrollFactor     = 2;
#	else
	const Int_t  unrollFactor     = 4;			/* Number of unrolls we do, must be 4 */
# 	endif
	const Int_t  leftOver         = p % unrollFactor;	/* how many are not handled by the unrolled version */
	const Int_t  endeUnrolledLoop = p - leftOver;		/* Index at which we stop the unrolled loop */
		assert(endeUnrolledLoop <= m);


	/*
	 * Performing the operation, but copying the
	 * colum of B into a temporaty memory
	 */
	for(Int_t j = 0; j != m; ++j)
	{
		/* Copy the culum is a temporary location */
		for(Int_t k = 0; k != p; ++k)		/* TODO: unroll */
			{B_slice[k] = B[k * m + j];};

		for(Int_t i = 0; i != n; ++i)
		{
			Int_t a_rowStart = i * p;	/* Index of the start of the a row */

			Numeric_t cDot_0 = 0.0;			/* accumulators */
			Numeric_t cDot_1 = 0.0;
			Numeric_t cDot_2 = 0.0;
			Numeric_t cDot_3 = 0.0;

			for(Int_t k = 0; k != endeUnrolledLoop; k += unrollFactor)
			{
				const Int_t aStartIdxK  = a_rowStart  + k;	/* Index calcualtion */
					assert(k          + unrollFactor <= p    );
					assert(aStartIdxK + unrollFactor <= sizeA);

				const Numeric_t a_0 = A      [aStartIdxK + 0];	/* Load */
				const Numeric_t b_0 = B_slice[         k + 0];
				const Numeric_t a_1 = A      [aStartIdxK + 1];
				const Numeric_t b_1 = B_slice[         k + 1];
#				if !(defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
				const Numeric_t a_2 = A      [aStartIdxK + 2];
				const Numeric_t b_2 = B_slice[         k + 2];
				const Numeric_t a_3 = A      [aStartIdxK + 3];
				const Numeric_t b_3 = B_slice[         k + 3];
#				endif


				cDot_0 += a_0 * b_0;				/* compute */
				cDot_1 += a_1 * b_1;
#				if !(defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
				cDot_2 += a_2 * b_2;
				cDot_3 += a_3 * b_3;
#				endif
			}; /* end for(k): do the unrolled loop */

			/* Handle the left over */
			for(Index_t k = endeUnrolledLoop; k != p; ++k)
			{
				const Int_t aStartIdxK  = a_rowStart  + k;	/* Index calcualtion */

				const Numeric_t a_0 = A      [aStartIdxK + 0];	/* Load */
				const Numeric_t b_0 = B_slice[         k + 0];

				cDot_0 += a_0 * b_0;				/* compute */
			}; /* End for(k): handle the left over */

			/* Do the reduction */
			const Numeric_t cDot_01  = cDot_0  + cDot_1;
			const Numeric_t cDot_23  = cDot_2  + cDot_3;
			const Numeric_t cDot     = cDot_01 + cDot_23;

			Int_t c_dotIdx   = i * m + j;	/* Calculating the cirrent location */
			C[c_dotIdx] += cDot;		/* Write the result back */
		}; /* ENd for(i): going through the rows of A */
	}; /* End for(j): column of C */

	/* CLEARING */
	cLibSibyl_dFree(&B_slice);

	return 0;
}; /* END: GMM */


Int_t
cLibSibyl_dMtSquare(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	      Numeric_t* restrict 	C,
	const Int_t 			n,
	const Int_t 			m)
{
	if(!isfinite(gamma) )
		{return 2001;};
	if(A == NULL        )
		{return 2003;};
	if(C == NULL        )
		{return 2005;};
	if(n <= 0 || m <= 0 )
		{return 2006;};

	/* Performing the \gamma * \Mat{C} part */
	const Int_t sizeC = n * n;
	const Int_t sizeA = n * m;
	for(Int_t k = 0; k != sizeC; ++k)
		{C[k] *= gamma;};

	/* For unrolling */
#	if (defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
	const Int_t  unrollFactor     = 2;
#	else
	const Int_t  unrollFactor     = 4;			/* Number of unrolls we do, must be 4 */
# 	endif
	const Int_t  leftOver         = m % unrollFactor;	/* how many are not handled by the unrolled version */
	const Int_t  endeUnrolledLoop = m - leftOver;		/* Index at which we stop the unrolled loop */
		assert(endeUnrolledLoop <= m);


	/*
	 * Performing the operation, but copying the
	 * colum of B into a temporaty memory
	 */
	for(Int_t j = 0; j != n; ++j)
	{
		for(Int_t i = 0; i != n; ++i)
		{
			const Int_t aStartIdx   = i * m;	/* here the row in A starts */
			const Int_t atStartIdx  = j * m;	/* here we start in A^T */

			Numeric_t cDot_0 = 0.0;			/* accumulators */
			Numeric_t cDot_1 = 0.0;
			Numeric_t cDot_2 = 0.0;
			Numeric_t cDot_3 = 0.0;

			for(Int_t k = 0; k != endeUnrolledLoop; k += unrollFactor)
			{
				const Int_t aStartIdxK  = aStartIdx  + k;	/* Index calcualtion */
				const Int_t atStartIdxK = atStartIdx + k;
					assert(atStartIdxK + unrollFactor <= sizeA);
					assert( aStartIdxK + unrollFactor <= sizeA);

				const Numeric_t  a_0 = A[ aStartIdxK + 0];	/* load values */
				const Numeric_t at_0 = A[atStartIdxK + 0];
				const Numeric_t  a_1 = A[ aStartIdxK + 1];
				const Numeric_t at_1 = A[atStartIdxK + 1];
#				if !(defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
				const Numeric_t  a_2 = A[ aStartIdxK + 2];
				const Numeric_t at_2 = A[atStartIdxK + 2];
				const Numeric_t  a_3 = A[ aStartIdxK + 3];
				const Numeric_t at_3 = A[atStartIdxK + 3];
#				endif

				cDot_0 += a_0 * at_0;				/* compute */
				cDot_1 += a_1 * at_1;
#				if !(defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
				cDot_2 += a_2 * at_2;
				cDot_3 += a_3 * at_3;
#				endif
			}; /* end for(k): do the unrolled loop */

			/* Handle the left over */
			for(Index_t k = endeUnrolledLoop; k != m; ++k)
			{
				const Int_t aStartIdxK  = aStartIdx  + k;	/* Index calcualtion */
				const Int_t atStartIdxK = atStartIdx + k;

				const Numeric_t  a_0 = A[ aStartIdxK];	/* load values */
				const Numeric_t at_0 = A[atStartIdxK];

				cDot_0 += a_0 * at_0;				/* compute */
			}; /* End for(k): handle the left over */

			/* Do the reduction */
			const Numeric_t cDot_01  = cDot_0  + cDot_1;
			const Numeric_t cDot_23  = cDot_2  + cDot_3;
			const Numeric_t cDot     = cDot_01 + cDot_23;

			/* Store */
			const Index_t cIdx = i * n + j;		/* where into the C matrix the dot product belongs */
			C[cIdx] += cDot;		/* Write the result back */
		}; /* ENd for(i): going through the rows of A */
	}; /* End for(j): column of C */

	return 0;
	(void)sizeA;
}; /* END: transposed square */


Int_t
cLibSibyl_dGEMV(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	const Numeric_t* restrict 	x,
	      Numeric_t* restrict 	y,
	const Int_t 			n,
	const Int_t 			m)
{
	if(!isfinite(gamma) )
		{return 3001;};
	if(A == NULL        )
		{return 3003;};
	if(x == NULL        )
		{return 3004;};
	if(y == NULL        )
		{return 3005;};
	if(n <= 0 || m <= 0 )
		{return 3006;};

	/* Apply the gamma multiplication */
	for(Int_t i = 0; i != n; ++i)
		{ y[i] *= gamma;};

	/* For unrolling */
#	if (defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
	const Int_t  unrollFactor     = 2;
#	else
	const Int_t  unrollFactor     = 4;			/* Number of unrolls we do, must be 4 */
# 	endif
	const Int_t  leftOver         = m % unrollFactor;	/* how many are not handled by the unrolled version */
	const Int_t  endeUnrolledLoop = m - leftOver;		/* Index at which we stop the unrolled loop */
		assert(endeUnrolledLoop <= m);

	for(Int_t i = 0; i != n; ++i)
	{
		Int_t a_rowStartIdx = i * m;	/* start of current row */

		Numeric_t cDot_0 = 0.0;			/* accumulators */
		Numeric_t cDot_1 = 0.0;
		Numeric_t cDot_2 = 0.0;
		Numeric_t cDot_3 = 0.0;

		for(Int_t k = 0; k != endeUnrolledLoop; k += unrollFactor)
		{
			const Int_t a_rowStartIdxK  = a_rowStartIdx + k;	/* Index calcualtion */

			const Numeric_t a_0 = A[a_rowStartIdxK + 0];
			const Numeric_t x_0 = x[k              + 0];
			const Numeric_t a_1 = A[a_rowStartIdxK + 1];
			const Numeric_t x_1 = x[k              + 1];
#			if !(defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
			const Numeric_t a_2 = A[a_rowStartIdxK + 2];
			const Numeric_t x_2 = x[k              + 2];
			const Numeric_t a_3 = A[a_rowStartIdxK + 3];
			const Numeric_t x_3 = x[k              + 3];
#			endif

			cDot_0 += a_0 * x_0;				/* compute */
			cDot_1 += a_1 * x_1;
#			if !(defined(CSIBYL_LINAL_SMALL_UNROLL) && (CSIBYL_LINAL_SMALL_UNROLL != 0))
			cDot_2 += a_2 * x_2;
			cDot_3 += a_3 * x_3;
#			endif
		}; /* end for(k): unrolled loop */


		/* Handle the left over */
		Numeric_t cDot_l = 0.0;		/* accumulator for left overs */
		for(Int_t j = endeUnrolledLoop; j != m; ++j)
		{
			const Int_t     a_idx = a_rowStartIdx + j;
			const Numeric_t a     = A[a_idx];
			const Numeric_t x_j   = x[j];

			cDot_l += a * x_j;	/* accumulation */
		};

		/* Do the reduction */
		const Numeric_t cDot_01  = cDot_0  + cDot_1;
		const Numeric_t cDot_23  = cDot_2  + cDot_3;
		const Numeric_t cDot_a   = cDot_01 + cDot_23;
		const Numeric_t cDot     = cDot_a  + cDot_l;

		y[i] += cDot;	/* Write back */
	}; /* END for(i): going through the rows */

	return 0;
}; /* END: GMV */



Int_t
cLibSibyl_dGEMtV(
	const Numeric_t 		gamma,
	const Numeric_t* restrict 	A,
	const Numeric_t* restrict 	x,
	      Numeric_t* restrict 	y,
	const Int_t 			n,
	const Int_t 			m)
{
	if(!isfinite(gamma) )
		{return 4001;};
	if(A == NULL        )
		{return 4003;};
	if(x == NULL        )
		{return 4004;};
	if(y == NULL        )
		{return 4005;};
	if(n <= 0 || m <= 0 )
		{return 4006;};

	/* Apply the gamma multiplication */
	for(Int_t i = 0; i != m; ++i)
		{ y[i] *= gamma;};

	/* Since A is in rowMaj order, but is implicitly transposed,
	 * one can consider it as a colMaj matrix. This means we must
	 * multiply each column with the coresponding value in x
	 * and sum up it in y. */
	for(Int_t i = 0; i != n; ++i)		/* handle the column */
	{
		const Int_t 	a_startMemRowIdx = i * m;	/* this is the start of the colum that should be multiplied */
		const Numeric_t xVal             = x[i];	/* this is the value we have to multiply with */

		/* Now we iterate through a column */
		for(Int_t k = 0; k != m; ++k)
		{
			const Int_t     a_idx = a_startMemRowIdx + k;	/* index of the array element that is processed */
			const Numeric_t aVal  = A[a_idx];		/* value of said array element */
			const Numeric_t yVal  = y[k    ];		/* y value; summation target */

			const Numeric_t yValNew = yVal + aVal * xVal;	/* perform the update */

			y[k] = yValNew;					/* store back */
		}; /* End for(k): perfromiung the update */
	}; /* End for(i): */

	return 0;
}; /* end: transposed matrix */



/*
 * ==============================
 * UTIL
 */

Int_t
cLibSibyl_transpose(
	const Numeric_t* restrict 	A,
	      Numeric_t* restrict 	B,
	const Int_t 			n,
	const Int_t 			m)
{
	if(A == NULL        )
		{return 101;};
	if(B == NULL        )
		{return 102;};
	if(n <= 0 || m <= 0)
		{return 103;};

	/* we iterate through the untransposed matrix A */
	for(Int_t iA = 0; iA != n; ++iA)
	{
		for(Int_t jA = 0; jA != m; ++jA)
		{
			const Int_t iB    = jA;			/* swap the order to get the b index */
			const Int_t jB    = iA;
			const Int_t a_idx = iA * m + jA;	/* calculate the matrix index */
			const Int_t b_idx = iB * n + jB;

			const Numeric_t a = A[a_idx];		/* load the value of A */

			B[b_idx] = a;				/* store the value inside B */
		}; /* End for(jA): */
	}; /* End for(iA) */

	return 0;
}; /* end: transposing */


Int_t
cLibSibyl_setVec(
	      Numeric_t* restrict 	v,
	const Numeric_t 		alpha,
	const Int_t 			n)
{
	if(!isfinite(alpha) )
		{return 201;};
	if(v == NULL        )
		{return 203;};
	if(n <= 0           )
		{return 204;};

	for(Int_t i = 0; i != n; ++i)
		{v[i] = alpha;};

	return 0;
}; /* END: set vector to value. */


Int_t
cLibSibyl_setRange(
	const Index_t 		lowest,
	      Index_t* restrict v,
	const Int_t 		n)
{
	if(v == NULL        )
		{return 301;};
	if(n <= 0           )
		{return 302;};

	for(Int_t i = 0; i != n; ++i)
		{v[i] = lowest + i;};

	return 0;
}; /* END: set vector to value. */


Int_t
cLibSibyl_iSetRange(
	const Int_t 		lowest,
	      Int_t* restrict 	v,
	const Int_t 		n)
{
	if(v == NULL        )
		{return 301;};
	if(n <= 0           )
		{return 302;};

	for(Int_t i = 0; i != n; ++i)
		{v[i] = lowest + i;};

	return 0;
}; /* END: set vector to value. (INT) */


Int_t
cLibSibyl_iSetVec(
	      Int_t* restrict 	v,
	const Int_t 		alpha,
	const Int_t 		n)
{
	if(v == NULL)
		{return 203;};
	if(n <= 0   )
		{return 204;};

	for(Int_t i = 0; i != n; ++i)
		{v[i] = alpha;};

	return 0;
}; /* End: setting int value */


Int_t
cLibSibyl_printMatrix(
	const Numeric_t* 	A,
	const Int_t 		r,
	const Int_t 		c)
{
	if(A == NULL)
		{return 401;};
	if(r <= 0)
		{return 402;};
	if(c <= 0)
		{return 403;};

	/* write the start */
	printf("[");

	for(Int_t i = 0; i != r; ++i)
	{
		if(i == 0)
			{printf("[");}
		else
			{printf(" [");};

		for(Int_t j = 0; j != c; ++j)
		{
			if(j != 0)				/* write the separation of numbers */
				{printf(", ");};

			const Int_t      idx = i * c + j;	/* load index and value to print */
			const Numeric_t  a   = A[idx];

			printf("%8.6e", a);			/* actual printing */
		}; /* End forUj) */

		/* write the closing of of the current */
		if(i+1 != r)
			{printf("],\n");}
		else
			{printf("]");};
	}; /* end for (rows */

	printf("]");

	return 0;
}; /* end printing */


