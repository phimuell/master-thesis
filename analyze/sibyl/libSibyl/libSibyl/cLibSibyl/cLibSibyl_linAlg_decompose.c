/**
 * \brief	This file contains all functions that deals with the decomposing of the matrix.
 */

/* includes */
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_linAlg.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_internal.h"

#include <math.h>
#include <limits.h>
#include <float.h>


Int_t
cLibSibyl_luDecomp(
	      Numeric_t* restrict 	A,
	const Int_t 			n,
	      Index_t* restrict 	perIdx,
	      Int_t* 			d)
{
	if(A == NULL)
		{return 40001;};
	if(n <= 0 )
		{return 40002;};
	if(perIdx == NULL )
		{return 40003;};

	Int_t dI = 1;		/* This is the internal d */

	/*
	 * This will hold the scaling, it is such that the largest coeficient
	 * in a row will become one. It is in numerical indexing, not matematical
	 * indexing, so scaling[i] will always refere to the ith row of
	 * the memory and not to the matrix. */
	Numeric_t* scaling = cLibSibyl_dArray(n);
	cLibSibyl_setVec(scaling, 0.0, n);

	/*
	 * In each row we will now look for the largest element.
	 * This is done in numerical indexing. */
	for(Int_t i = 0; i != n; ++i)
	{
		Numeric_t biggestValue = 0.0;		/* zero is the only safe value */
		const Int_t iRowStart  = i * n;		/* start of this index */

		for(Int_t j = 0; j != n; ++j)
		{
			const Numeric_t a = A[iRowStart + j];
# 			ifndef NDEBUG
			if(!isfinite(a) )
			{
				cLibSibyl_dFree(&scaling);
				return 4;
			};
#			endif
			const Numeric_t abs_a = dAbs(a);

			if(abs_a > biggestValue)
				{biggestValue = abs_a;};
		}; /* End for(j): scanning a row */

		if(biggestValue == 0.0)		/* A zero row was found */
		{
			cLibSibyl_dFree(&scaling);
			return 40005;
		};
		if(scaling[i] != 0.0)		/* we found an index twice */
		{
			cLibSibyl_dFree(&scaling);
			return 40006;
		};

		scaling[i] = 1.0 / biggestValue;	/* compute the scaling value */
	}; /* End for(i_): scaling, traverse rows */


	/*
	 * Looping over the columns
	 */
	for(Int_t j = 0; j != n; j++)
	{
		/* This is the first step, it implements equation (2.3.12)
		 * This will be skiped if in the first step */
		for(Int_t i = 0; i < j; i++)
		{
			/* precalculating indexes */
			const Int_t      a_rowStartIdx = i * n;	/* where does a row in A start */
			      Int_t      a_idx_kj      = j;	/* strenth reduction */
			      Numeric_t  sum           = 0;	/* this is the summation operator in the equation */
			for(Int_t k = 0; k < i; k++)
			{
				const Numeric_t  alpha_ik = A[a_rowStartIdx + k];
				const Numeric_t  alpha_kj = A[a_idx_kj         ];
				                 sum     += alpha_ik * alpha_kj ;
				a_idx_kj += n;
			}; /* End for(k): computing the summation operator */

			/* this commputes beta_ij in equation (2.3.12) */
			const Numeric_t     a_ij = A[a_rowStartIdx + j];
			const Numeric_t  beta_ij = a_ij - sum;

			A[a_rowStartIdx + j] = beta_ij;  /* write back */
		} /* End for(i): handling the first step */

		/* This loop finds the pivot row, but at the same time also
		 * performs the second setp that has to be done, partially.
		 * It computes equation (2.3.13), without the division */
		Index_t    pivotRow = -1;		/* Index of the row with the pivor */
		Numeric_t  bigVal   = 0.0;		/* This is the value of the largest pivoting */
		for(Int_t i = j; i < n; i++)
		{
			const Int_t      a_rowStartIdx = i * n;	/* where does a row in A start */
			      Int_t      a_idx_kj      = j;	/* strenth reduction */
			      Numeric_t  sum           = 0;	/* this is the summation operator in the equation */

			for(Int_t k = 0; k < j; k++)
			{
				Numeric_t  a_ik = A[a_rowStartIdx + k];
				Numeric_t  a_kj = A[a_idx_kj	     ];
				           sum += a_ik * a_kj;
				a_idx_kj += n;
			};
			const Numeric_t alpha_ij = A[a_rowStartIdx + j];
			const Numeric_t tmp      = alpha_ij - sum;

			A[a_rowStartIdx + j] = tmp;	/* Write back */

			/* Test if we have found a better pivot */
			const Numeric_t dum = scaling[i] * dAbs(tmp);	/* sum is different in NR and here */
			if(dum >= bigVal)	/* = is needed for first line */
			{
				bigVal    = dum;
				pivotRow  = i;
			}
		} /* End for(i): searching for a pivot and partially second part */

		if(pivotRow == -1)
		{
			cLibSibyl_dFree(&scaling);
			return 40007;
		};
		if(pivotRow < j)
		{
			cLibSibyl_dFree(&scaling);
			return 40008;
		};
		if(pivotRow >= n)
		{
			cLibSibyl_dFree(&scaling);
			return 40009;
		};
		if(pivotRow < 0)
		{
			cLibSibyl_dFree(&scaling);
			return 40010;
		};


		/* Test if we have to permute the rows */
		if(j != pivotRow)
		{
			const Int_t a_rowStartIdx_piv = pivotRow * n;	/* start of the row that was selected to become pivot */
			const Int_t a_rowStartIdx_j   = j * n;		/* start of the current pivot row that will be moved */

			for(Int_t k = 0; k < n; k++)
			{
				/* Load the values */
				const Numeric_t a_pk = A[a_rowStartIdx_piv + k];
				const Numeric_t a_jk = A[a_rowStartIdx_j   + k];

				/* Write the values back in different order */
				A[a_rowStartIdx_piv + k] = a_jk;
				A[a_rowStartIdx_j   + k] = a_pk;
			} /* End for(k): moving the rows asside */

			/* copy scaling, we do not need the old value, wo we can overwrite it */
			scaling[pivotRow] = scaling[j];

			dI = -dI;	/* Update permutation */
		}; /* End if: permute rows */

		/* Store the permutation in the output vector */
		perIdx[j] = (Index_t)pivotRow;

		/* This allows us to also handle near singlualr value It is not so good
		 * but it is consistent with what NR does */
		const Int_t      a_diagIdx      =   j * n + j ;
		const Numeric_t  a_diagValueRaw = A[a_diagIdx];
		if (a_diagValueRaw == 0.0)
		{
			A[a_diagIdx] = DBL_EPSILON;
		};

		/* Apply the division, this complets the
		 * equation (2.3.13) from above */
		if (j != n-1)
		{
			const Numeric_t beta_jj = A[a_diagIdx];
			const Numeric_t invBeta_jj = 1.0 / beta_jj;
			for(Int_t i = j+1; i < n; i++)
			{
				A[i * n + j] *= invBeta_jj;
			};
		} /* end if: apply inversion */
	} /* End for(j): going through the coumns */

	/* update the D if given */
	if(d != NULL)
	{
		if((*d != -1) && (*d != 1))
			{ *d = 1;};
		*d = dI * *d;
	}; /* End if: set d */

	return 0;
}; /* END: luDecompos */


Int_t
cLibSibyl_luSolve(
	const Numeric_t* restrict 	LU,
	const Int_t 		  	n,
	const Index_t*   restrict 	perIdx,
	      Numeric_t* restrict 	b)
{
	if(LU == NULL)
		{return 30001;};
	if(n <= 0 )
		{return 30002;};
	if(perIdx == NULL )
		{return 30003;};
	if(b == NULL)
		{return 30004;};

	/* Forward subsititutaion
	 * Because of the structure we can do an optimization.
	 * If the rhs is zero, the coressponding solution value
	 * is zero as well, so until we find a non zero rhs,
	 * the solution is zero, note that this holds also for
	 * first non zero element */
	Int_t ii = 0;			/* index of first non zero rhs PLUS 1 */
	for(Int_t i = 0; i < n; i++)
	{
		const Int_t      i_permute = perIdx[i];		/* apply permutation to rhs */
		const Numeric_t  b_permute = b[i_permute];	/* Load the permuted b value */

		b[i_permute] = b[i];				/* reorder b */

		if (ii != 0)
		{
			      Numeric_t  sum           = b_permute;
			const Int_t      a_rowStartIdx = i * n;
			for(Int_t j = ii-1; j < i; j++)	/* -1 because of the definition of ii */
			{
				const Numeric_t a_ij = LU[a_rowStartIdx + j];
				const Numeric_t b_j  = b[j];
				const Numeric_t dot  = a_ij * b_j;
				sum -= dot;
			}; /* end for(k): */

			b[i] = sum;	/* write back */
		}
		else
		{
			if(b_permute != 0.0)
			{
				/* we have found a non zero element */
				ii=i+1;
			};

			/* writting the value back */
			b[i] = b_permute;	/* can be zero */
		}; /* End else: */
	} /* end for(i): forward */


	/* Here we do backward substitution.
	 * We have already undo the permutation */
	for(Int_t i = n-1; i >= 0; i--)
	{
		      Numeric_t sum           = b[i];	/* load the value */
		const Int_t     a_rowStartIdx = i * n;	/* start of the row */
		for(Int_t j = i+1; j < n; j++)
		{
			const Numeric_t a_ij = LU[a_rowStartIdx + j];
			const Numeric_t b_j  = b[j];
			const Numeric_t dot  = a_ij * b_j;

			sum -= dot;
		};

		/* finaly updating */
		const Int_t aIdx_ii = i * n + i;
		const Numeric_t a_ii = LU[aIdx_ii];

		b[i] = sum / a_ii;
	} /* end for(i): backward */

	return 0;
}; /* end solve */


