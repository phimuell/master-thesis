/**
 * \brief	This file contains the implementation for the operations on convex hulls.
 */

/* Includes */
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_convexHull.h"
#include "./cLibSibyl_convexHull_ops.h"

#include <math.h>


Int_t
csibyl_convecHull_middlePoints(
	Numeric_t* restrict 		Xm,
	Numeric_t* restrict 		Ym,
	struct sibyl_convexHull_t* 	cHull)
{
	const Numeric_t *xPos = NULL, *yPos = NULL;	/* alias pointers int the hull */
	Int_t* hull = NULL;
	Int_t  nTotMarkers = -1, nHull = -1;		/* sizes */
	Int_t  nMiddP = -1;				/* number of midpoints */
	Int_t  i;					/* iterator */

	Int_t  idx_1, idx_2;				/* local variables */
	Numeric_t x_1, x_2, y_1, y_2;
	Numeric_t mp_x, mp_y;

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	csibyl_assert2(Xm    != NULL, 93001);
	csibyl_assert2(Ym    != NULL, 93002);
	csibyl_assert2(cHull != NULL, 93003);
	csibyl_assert2(cHull->nHull >= 2, 93101);
	csibyl_assert2(cHull->nHull <  (cHull->nMarkers + 1), 93102);
	csibyl_assert2(cHull->hull[0] == cHull->hull[cHull->nHull - 1], 93103);

	/* unpack the structure */
	hull = cHull->hull;
	xPos = cHull->xPos;
	yPos = cHull->yPos;
	nHull       = cHull->nHull;
	nTotMarkers = cHull->nTotMarkers;
	nMiddP      = nHull - 1;

	for(i = 0; i != nMiddP; ++i)
	{
		idx_1 = hull[i    ];	/* load the index */
		idx_2 = hull[i + 1];
			csibyl_assert2(0     <= idx_1      , 93101);
			csibyl_assert2(idx_1 <  nTotMarkers, 93102);
			csibyl_assert2(0     <= idx_2      , 93103);
			csibyl_assert2(idx_2 <  nTotMarkers, 93103);

		x_1 = xPos[idx_1];	/* load positions */
		x_2 = xPos[idx_2];
		y_1 = yPos[idx_1];
		y_2 = yPos[idx_2];

		mp_x = (x_1 + x_2) * 0.5;	/* compute middle point */
		mp_y = (y_1 + y_2) * 0.5;

		Xm[i] = mp_x;			/* write back */
		Ym[i] = mp_y;
	}; /* end for(i): */

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
	(void)nTotMarkers;
}; /* end: find midpoints */



Int_t
csibyl_convecHull_volume(
	Numeric_t* const 			vol,
	struct sibyl_convexHull_t* const 	cHull)
{
	const Numeric_t *xPos = NULL, *yPos = NULL;	/* alias pointers int the hull */
	Int_t* hull = NULL;
	Int_t  nTotMarkers = -1, nHull = -1;	 	/* sizes */
	Int_t  nSeg = -1;				/* The number of segments */
	Int_t  i;					/* iterator */
	Int_t  idx_1, idx_2;				/* local variables */
	Numeric_t x_1, x_2, y_1, y_2;			/* current points */
	Numeric_t h, m, V;				/* variables for computing the volume */
#	define SUM_N 8					/* number of partial sums that are used */
	Numeric_t Vpartial[SUM_N];			/* array for partial volume */

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	csibyl_assert2(vol   != NULL, 97001);
	csibyl_assert2(cHull != NULL, 97002);
	csibyl_assert2(cHull->nHull   >   2                            , 97101);
	csibyl_assert2(cHull->nHull   <  (cHull->nMarkers + 1)         , 97102);
	csibyl_assert2(cHull->hull[0] ==  cHull->hull[cHull->nHull - 1], 97103);

	/* Unpacking the Hull */
	hull        = cHull->hull;
	xPos        = cHull->xPos;
	yPos        = cHull->yPos;
	nHull       = cHull->nHull;
	nSeg 	    = nHull - 1;
	nTotMarkers = cHull->nTotMarkers;

	/* Setting the partial array to zero */
	for(i = 0; i != (SUM_N); ++i)
		{ Vpartial[i] = 0.0; };

	/* Load the first two positions */
	idx_1 = hull[0];
		csibyl_assert2(0     <= idx_1      , 97201);
		csibyl_assert2(idx_1 <  nTotMarkers, 97202);
	x_1   = xPos[idx_1];
	y_1   = yPos[idx_1];


	/* Now we go through the convex hull
	 *
	 * This loop basically computes the folowing integral
	 * 	\int_{x_{min}}^{x_{max}} u(x) - l(x) \d{x}
	 * where $x_{min/max}$ is the smallest/largest x value
	 * of the points inside the convex hull. u(x) is the
	 * function that decribe the upper hull and l(x) describes
	 * the lower hull.
	 *
	 * Ask tile@ethz.ch for more detail, he explained it to me.
	 */
	for(i = 0; i != nSeg; ++i)
	{
		/* load the upper point, lower was loaded before */
		idx_2 = hull[i + 1];
			csibyl_assert2(0     <= idx_2      , 97303);
			csibyl_assert2(idx_2 <  nTotMarkers, 97304);
		x_2   = xPos[idx_2];
		y_2   = yPos[idx_2];

		/* Calculate the volume of the current segment.
		 * We have to add them */
		h = x_2 - x_1;		/* on upper segment positive on lower segments negative */
		m = (y_1 + y_2) * 0.5;	/* can be negative */

		/* this is now the current volume */
		V = h * m;
			csibyl_assert2(isfinite(V), 97305);

		/* Now we accumulate the volume.
		 *
		 * Note it is important that we have a negative contribution
		 * and no positive contribution. The reason is that the implementation
		 * assumes that we first walk the upper boundary and then the lower boundary.
		 * however this is not the case, since first comes the lower.
		 * So the contribution has inverted, which is accounted by a - sign in
		 * the reduction. */
		Vpartial[i % (SUM_N)] -= V;		/* -= is important */

		/* make the _2 the new _1 point */
		x_1   = x_2;		/* move the new one, to the last one */
		y_1   = y_2;
		idx_1 = idx_2;
	}; /* end for(i): */

	/* perform the reducing */
	V = 0.0;
	for(i = 0; i != (SUM_N); ++i)
	{
		V += Vpartial[i];
	}; /* end for(i): reducing */

	/* write the result back */
	*vol = V;

	CSIBYL_RT_FUNC_CLEANUP:
	CSIBYL_RT_FUNC_RETURN;
#	undef SUM_N
}; /* end: compute volume */




