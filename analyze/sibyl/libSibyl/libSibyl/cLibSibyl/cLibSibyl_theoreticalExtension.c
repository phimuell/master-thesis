/**
 * \brief	This file contains a function that is able to compute the theoretical extension of the markers.
 */

#include "./cLibSibyl.h"
#include "./cLibSibyl_assert.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"
#include "./cLibSibyl_linAlg.h"

#include <stdlib.h>
#include <math.h>

CSIBYL_ATTR_HOT
Int_t
cLibSibyl_computeTheoreticalExtension(
	const Numeric_t* const restrict 	yPos,
	const Numeric_t* const restrict 	xPos,
	const Int_t 				nMarker,
	const Numeric_t* const restrict 	gPosY,
	const Numeric_t* const restrict 	gPosX,
	const Int_t 				nGridY,
	const Int_t 				nGridX,
	      Numeric_t* const restrict 	theoExten /* OUT */)
{
	Int_t *K = NULL;
	Int_t rS;

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;

	if(nMarker < 0)
		{return 10;};
	if(nGridX < 2)
		{return 11;};
	if(nGridY < 2)
		{return 12;};

	/* Allocating memroy, this call will succeed or sibyl will panic */
	K  = cLibSibyl_iArray(nMarker);

	/* Technically it is not needed to set the values of the assoiction vector
	 * to a obvious wrong value. But it will help to speed up, since the
	 * function will then immediatly jump to the recompution */
	rS = cLibSibyl_iSetVec(K, -1, nMarker);  /* Giving them a value */
	if(rS != 0)
		{cLibSibylRT_exit2(rS, "Setting the vector failed.");};

	CSIBYL_CALL(cLibSibyl_updateTheoreticalExtension,
			yPos, xPos, nMarker,
			gPosY, gPosX, nGridY, nGridX,
			K, theoExten);

	/* Exit and cleanup code */
	CSIBYL_RT_FUNC_CLEANUP:
	cLibSibyl_iFree(& K);

	CSIBYL_RT_FUNC_RETURN;
}; /* END: theoretical expension */



CSIBYL_ATTR_HOT
Int_t
cLibSibyl_updateTheoreticalExtension(
	const Numeric_t* const restrict 	yPos,
	const Numeric_t* const restrict 	xPos,
	const Int_t 				nMarker,
	const Numeric_t* const restrict 	gPosY,
	const Numeric_t* const restrict 	gPosX,
	const Int_t 				nGridY,
	const Int_t 				nGridX,
	      Int_t*     const restrict 	K /* IN/OUT */,
	      Numeric_t* const restrict 	theoExten /* OUT */)
{
	Int_t* restrict CC = NULL;		/* POINTERS */
	Numeric_t* S = NULL;

	Int_t m, i, j, iNc, k;
	Int_t lastValidX = nGridX - 1, lastValidY = nGridY - 1;
	Numeric_t hx, hy, Ac, dNc, theoExten_m;
#	ifndef NDEBUG
	Size_t nProcessed;
#	endif

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;


	if(nMarker < 0)
		{return 10;};
	if(nGridX < 2)
		{return 11;};
	if(nGridY < 2)
		{return 12;};

	/* Allocating memroy, this call will succeed or sibyl will panic */
	S  = cLibSibyl_dMatrix(nGridY, nGridX);
	CC = cLibSibyl_iMatrix(nGridY, nGridX);

	CSIBYL_CALL(cLibSibyl_updateAssociationK,
			yPos, xPos, nMarker,
			gPosY, gPosX, nGridY, nGridX,
			TRUE,	//Because we are on the basic grid
			K);

	CSIBYL_CALL(cLibSibyl_countAssocMarkersK,
			K, nMarker,
			nGridY, nGridX,
			CC);

#	ifndef NDEBUG
	nProcessed = 0;
# 	endif
	for(i = 0; i != nGridY; ++i)
	{
		for(j = 0; j != nGridX; ++j)
		{
			k = i * nGridX + j;
			S[k] = 0.0;

			if((i >= lastValidY) || (j >= lastValidX))
				{continue;};
				csibyl_assert2((i+1) <  nGridY, 103);	/* This check must come afterwards, because */
				csibyl_assert2((j+1) <  nGridX, 104);	/* only here we can be sure that it exists */

			iNc = CC[k];	/* Number of markers in that cell, must not be negative, because of the marker dump. */
				csibyl_assert2(iNc >= 0, 105);

			if(iNc == 0)	/* Because of our dump policy this can happen */
				{continue;};

			hx = gPosX[j + 1] - gPosX[j];	/* extension of the cell */
			hy = gPosY[i + 1] - gPosY[i];
			Ac = hx * hy;			/* area of the cell */

			dNc = (Numeric_t)iNc;

			theoExten_m = Ac / dNc;		/* Calculate the extension */

			csibyl_assert2(isfinite(theoExten_m), 70);
			csibyl_assert2(theoExten_m > 0.0    , 71);

			S[k] = theoExten_m;	/* Write the extension back */

#			ifndef NDEBUG
			nProcessed += 1;	/* Increment count for processed elements */
#			endif
		}; /* End for(j): */
	}; /* End for(i): */


	/* Now here comes the real part
	 * We will now compute the theoretical extension */
#	ifndef NDEBUG
	nProcessed = 0;
#	endif
	for(m = 0; m != nMarker; ++m)
	{
		theoExten[m] = 0.0;  /* Setting the value to zero */
		k = K[m];

		/* Test if the association is correct */
		csibyl_assert2( k           >= 0     , -1);
		csibyl_assert2((k / nGridX) <  nGridY, -2);
		csibyl_assert2((k % nGridX) <  nGridX, -3);

		theoExten_m = S[k];

		theoExten[m] = theoExten_m;	/* Write the extension back */

#		ifndef NDEBUG
		nProcessed += 1;		/* Increment count for processed elements */
#		endif
	}; /* END for(m): */

	csibyl_assert2(nProcessed, 80);		/* test if at least something was processed */

	/* Exit and cleanup code */
	CSIBYL_RT_FUNC_CLEANUP:
	cLibSibyl_dFree(& S);
	cLibSibyl_iFree(&CC);

	CSIBYL_RT_FUNC_RETURN;
}; /* end: update theoretical extension */



CSIBYL_ATTR_HOT
Int_t
cLibSibyl_computeTheoreticalExtensionOnCA(
	const Int_t 				nMarker,
	const Numeric_t* const restrict 	gPosY,
	const Numeric_t* const restrict 	gPosX,
	const Int_t 				nGridY,
	const Int_t 				nGridX,
	const Int_t*     const restrict 	K /* IN */,
	const Int_t* 	 const restrict         nCellCount /* IN */,
	      Numeric_t* const restrict 	theoExten /* OUT */)
{

	Numeric_t* restrict S = NULL;

	Int_t m, i, j, iNc, k;				/* iterators and indexing */
	Int_t lastValidX, lastValidY;			/* limits                 */
	Numeric_t hx, hy, Ac, dNc, theoExten_m;		/* numeric stuff          */

#	ifndef NDEBUG
	Size_t nProcessed;
#	endif

	CSIBYL_RT_FUNC_START;
	CSIBYL_RT_IS_INIT;


	if(nMarker < 0)
		{return 310;};
	if(nGridX < 2)
		{return 311;};
	if(nGridY < 2)
		{return 312;};
	if(nCellCount == NULL)
		{return 313;};
	if(K == NULL)
		{return 314;};

	/* The last node is forbiddedn, becauase we asume tight grid.
	 * So the shrings the size by one in each direction */
	lastValidX = nGridX - 1;
	lastValidY = nGridY - 1;

	/* Allocating memroy, this call will succeed or sibyl will panic */
	S  = cLibSibyl_dMatrix(nGridY, nGridX);

	/* We will now compute the theoretical extension from each cell.
	 * Since the extension is the same for all markers in a given cell
	 * we can compute them once and then reuse them */
#	ifndef NDEBUG
	nProcessed = 0;
# 	endif
	for(i = 0; i != nGridY; ++i)
	{
		for(j = 0; j != nGridX; ++j)
		{
			k = i * nGridX + j;	/* linear index */
			S[k] = 0.0;		/* set to zero  */

			/* Test if we are in the valid range */
			if((i >= lastValidY) || (j >= lastValidX))
				{continue;};
				csibyl_assert2((i+1) <  nGridY, 103);	/* This check must come afterwards, because */
				csibyl_assert2((j+1) <  nGridX, 104);	/* only here we can be sure that it exists */

			iNc = nCellCount[k];	/* Number of markers in that cell, must not be negative, because of the marker dump. */
				csibyl_assert2(iNc >= 0, 105);

			if(iNc == 0)	/* Because of our dump policy this can happen */
				{continue;};

			hx = gPosX[j + 1] - gPosX[j];	/* extension of the cell */
			hy = gPosY[i + 1] - gPosY[i];
			Ac = hx * hy;			/* area of the cell */

			dNc = (Numeric_t)iNc;

			theoExten_m = Ac / dNc;		/* Calculate the extension */

			csibyl_assert2(isfinite(theoExten_m), 70);
			csibyl_assert2(theoExten_m > 0.0    , 71);

			S[k] = theoExten_m;	/* Write the extension back */

#			ifndef NDEBUG
			nProcessed += 1;	/* Increment count for processed elements */
#			endif
		}; /* End for(j): */
	}; /* End for(i): */
	csibyl_assert2(nProcessed, 80);		/* test if at least something was processed */


	/* New we will copy the theoretical extension from the cells, where we have
	 * precvomputed them to the markers */
	for(m = 0; m != nMarker; ++m)
	{
		theoExten[m] = 0.0;  /* Setting the value to zero */
		k = K[m];

		/* Test if the association is correct */
		csibyl_assert2( k           >= 0     , -1);
		csibyl_assert2((k / nGridX) <  nGridY, -2);
		csibyl_assert2((k % nGridX) <  nGridX, -3);

		theoExten_m = S[k];

		theoExten[m] = theoExten_m;	/* Write the extension back */
	}; /* END for(m): */

	/* Exit and cleanup code */
	CSIBYL_RT_FUNC_CLEANUP:
	cLibSibyl_dFree(&S);

	CSIBYL_RT_FUNC_RETURN;
}; /* end: compute on prepocessed associations */


