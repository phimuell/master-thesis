/**
 * This is the type header of Sibyl. It includes definitionof types.
 */
#ifndef CLIBSIBYL_TYPES_HEADER
#define CLIBSIBYL_TYPES_HEADER

/* Includes */
#include <stddef.h>


/* Typedefinitions */
typedef   signed int      Int_t;		/* signed int */
typedef unsigned int     uInt_t;		/* unsigned int */
typedef    ptrdiff_t    Index_t;		/* indexing and pointer difference */
typedef       double  Numeric_t;		/* Numerical calculation */
typedef  long double     Real_t;		/* Largest known float */
typedef         char     Bool_t;		/* This is a bool value */
typedef         char     Char_t;		/* This is the char type */
typedef       size_t     Size_t;		/* This is the size type */

/*
 * Here are some helps with bools
 */
#define TRUE  ((Bool_t)(!(0)))
#define FALSE ((Bool_t)(  0 ))

/**
 * \brief	This macro checks if an expression is true.
 * \define 	IS_TRUE
 *
 * This macro will comapre its argument to FALSE.
 *
 * \param  b		The value to test.
 */
#define IS_TRUE(b) ((b) != FALSE)


/*
 * Structts
 */

/* FWD of the convex hull object */
struct sibyl_convexHull_t;


#endif /* end guard */

