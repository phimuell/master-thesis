/**
 * \brief	This file provides the implementation of statitsical functions.
 */

/* Includes */
#include "cLibSibyl_types.h"
#include "cLibSibyl_runtime.h"
#include "cLibSibyl_memory.h"

#include "cLibSibyl_statistic.h"


#include <math.h>




void
cSibyl_allocMeanStateArray(
 	Index_t					N,
 	struct cSibyl_meanStateArray_t* 	states)
{
	if(states == NULL)
		{ cLibSibylRT_panic("Pased states argument is nullpointer."); };
	if(N <= 0)
		{ cLibSibylRT_panic("Passed number of states is zero."); };


	/* We will simply allocating the structures we needs.
	 * No freeing */
	states->m_states  = cLibSibyl_malloc(sizeof(struct cSibyl_meanState_t) * N);
	states->m_nStates = N;

	/* initializing */
	Index_t i;
	for(i = 0; i != N; ++i)
	{
		if(cSibyl_initMeanState(states->m_states + i) != 0)
			{ cLibSibylRT_panic("Failed to initialize a state."); };
	}; /* End for(i): init */

	return;
}; /* END:		Allocating */


void
cSibyl_freeMeanStateArray(
	struct cSibyl_meanStateArray_t* 	states)
{
	if(states == NULL)
		{ cLibSibylRT_panic("Pased states argument is nullpointer."); };

	cLibSibyl_free((void**)(&(states->m_states)));
	states->m_nStates = 0;

	return;
}; /* END: free */



