/**
 * \brief	This file contains some test functions
 *
 * Note they are implemented in files with the extension cc.
 * This is not C++ but C, it was used to trick Make and YCM.
 */
#ifndef CSIBYL_TEST_FUNCTIONS_HEADER
#define CSIBYL_TEST_FUNCTIONS_HEADER

#include "./cLibSibyl_linAlg.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_memory.h"


/**
 * \brief	This function tests GM{M, v} implementation
 */
extern
CSIBYL_EXPORT
Int_t
cSibylTest_matProd(void);


/**
 * \brief	Test the Lu decomposition
 */
extern
CSIBYL_EXPORT
Int_t
cSibylTest_luDecomp(void);


/**
 * \brief	Tests the savgol filter.
 */
extern
CSIBYL_EXPORT
Int_t
cSibylTest_savgol(void);


/**
 * \brief	Test the convex hull implementation.
 */
extern
CSIBYL_EXPORT
Int_t
cSibylTest_convexHull(void);



#endif	/* end guard */

