/**
 * \brief	This file declares some runtime parts of the library.
 */

/* Includes */
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_internal.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdatomic.h>
#include <threads.h>
#include <time.h>

#include <unistd.h>
#include <sys/types.h>

#if !(defined(CSIBYL_USE_ATTRIBUTE) && (CSIBYL_USE_ATTRIBUTE != 0))
#	pragma message "Attributes are deactivated."
#endif


/*
 * ===================
 * Static variables
 */

/* Error Code */
static volatile Int_t cLibSibylRT_errorCode_var = 0;
static volatile Int_t cLibSibylRT_errorFlag_var = 0;	/* this is a flag that is used to syncronize */

/* Condition/Description */
static 	        Char_t* volatile cLibSibylRT_errorCondC_var = NULL;
static volatile Int_t   	 cLibSibylRT_errorCondN_var = 0;

/* Function */
static          Char_t* volatile cLibSibylRT_errorFuncC_var = NULL;
static volatile Int_t   	 cLibSibylRT_errorFuncN_var = 0;
static volatile Int_t   	 cLibSibylRT_errorLine_var  = 0;

/* Runtime system internals */
static volatile Int_t   cLibSibylRT_isInit_var = 1;	/* ZERO means initialized		*/
static          mtx_t   cLibSibylRT_errorLock_var;	/* Lock for error state 		*/
static 		mtx_t   cLibSibylRT_pLogLock_var;	/* Lock for the panic output 		*/
static 		mtx_t 	cLibSibylRT_OutLock_var;	/* Lock for the normal output stream 	*/

/* This is the initial PID */
static volatile pid_t 	cLibSibylRT_initPID_var = -1;	/* Initial PID 	*/


/*
 * ======================
 * Helper Functions
 */

/*
 * This function is a helper function to set the code
 */
static
Int_t
cLibSibyl_setString(
	const    Char_t* 		  s,
	         Char_t* volatile*	  destS,
	volatile Int_t* 	          destN)
{
	if(destS == NULL)
		{cLibSibylRT_panic(NULL);};
	if(destN == NULL)
		{cLibSibylRT_panic(NULL);};
	if(*destS != NULL)
		{return -10;};
	if(*destN != 0)
		{return -11;};

	/* Handle the NULL case */
	if(s == NULL)
	{
		*destN = 0;
		*destS = NULL;
		return 0;
	};


	/* get the length of the string */
	Int_t n = cLibSibyl_strLen_(s, 1000000);
	if(n <= 0)
		{return -12;};

	*destN = n;	/* store the length of the string */

	/* allocating memory */
	*destS = malloc(sizeof(Char_t) * (n + 3));
	if(*destS == NULL)
		{return -13;};

	/* copy the string */
	strncpy(*destS, s, n + 3);

	return 0;
}; /* End: setting the error */


/*
 * =======================
 * Helper macro
 */

/**
 * \brief	This macro will generate code that will aquiere a lock.
 * \define 	CSIBYL_RT_GET_LOCK
 *
 * If this fails, panic will be called.
 *
 * \param  l	The lock to aquiere.
 */
#define CSIBYL_RT_GET_LOCK(l)   if(mtx_lock(&l) != thrd_success) {cLibSibylRT_panic("Could not aquire look " #l);}


/**
 * \brief	This macro will generate code to unlock a lock.
 * \define 	CSIBYL_RT_REL_LOCK
 *
 * If this fail, panic will be called.
 *
 * \param  l	The lock to unlock.
 */
#define CSIBYL_RT_REL_LOCK(l)   if(mtx_unlock(&l) != thrd_success) {cLibSibylRT_panic("Could not release look " #l);}



/*
 * ====================
 * RT Functions
 */

Int_t
cLibSibylRT_isInit(void)
{
		__asm__ volatile ("mfence" ::: "memory");
	const Int_t isInit_copy = cLibSibylRT_isInit_var;
		__asm__ volatile ("mfence" ::: "memory");
	if(isInit_copy == 0)
	{
		return 0;
	}
	return -1;
}; /* End: is initialized */


/**
 * \brief	This is the constructor function.
 *
 * This function is automatically called by the runtime
 * and will set up all internal states.
 */
void
__attribute__((constructor(65000)))
cLibSibylRT_init(void)
{
	/* test if already initialized, good practice to test */
	if(cLibSibylRT_isInit_var == 0)
		{ return; };


	/* Initialite the lock for the panic output */
	if(mtx_init(&cLibSibylRT_pLogLock_var, mtx_plain) != thrd_success)
	{
		cLibSibylRT_quietPanic();
		return;
	};

	/* Initialized the look for the error state */
	if(mtx_init(&cLibSibylRT_errorLock_var, mtx_plain) != thrd_success)
	{
		cLibSibylRT_panic("Could not initialize the lock for the error state.");
		return;
	};

	/* Initialite the lock for the normal output */
	if(mtx_init(&cLibSibylRT_OutLock_var, mtx_plain) != thrd_success)
	{
		cLibSibylRT_panic("Could not initialize the lock for the panic lock.");
		return;
	};

	/* Get the initial PID, this function is always successfull */
	cLibSibylRT_initPID_var = cLibSibylRT_getPID();

#	if !defined(NDEBUG)
	if(sizeof(pid_t) != sizeof(Int_t))
		{ cLibSibylRT_panic("The size of the PID and Int differs."); };
#	endif

	/* Now we set the value to a non zero value */
		__asm__ volatile ("mfence" ::: "memory");
	cLibSibylRT_isInit_var = 0;
		__asm__ volatile ("mfence" ::: "memory");


	/* Write to disc */
	cLibSibylRT_printf("Starting CSIBYLRT");

	return;
}; /* End: init function */



void
cLibSibylRT_panic(
	char* msg)
{
	/* Test if the runtime is not initialized */
	if(cLibSibylRT_isInit() != 0)
		{ cLibSibylRT_quietPanic(); };

	/* Load the panic lock.
	 * Note that there is not need to release it again */
	CSIBYL_RT_GET_LOCK(cLibSibylRT_pLogLock_var);

	Int_t w = -1;	/* This variable is used to store the number of characters written */


	/* Get the current time */
	time_t t = time(NULL);
	struct tm* tmp = localtime(&t);

	if(tmp == NULL)
		{ cLibSibylRT_quietPanic(); };

	/* copy it into a better format */
	struct tm buf = *tmp;

	/* This is the variable for storing the filename */
	Char_t fileName[256];

	/* composing the filename */
	w = snprintf(fileName, 255, "cLibSibyl_panic_pid%d.log", cLibSibylRT_getInitialPID());
	if((w <= 0) || (w >= 250))
		{ cLibSibylRT_quietPanic(); };

	/* Now we try to open a file */
	FILE* panicLog = fopen(fileName, "a+");

	if(panicLog == NULL)
		{ cLibSibylRT_quietPanic(); };

	/* Now start real writing */
	w = fprintf(panicLog, "%d-%d-%d %d:%d :: PANIC was called from within CSIBYL",
			buf.tm_year + 1900, buf.tm_mon + 1, buf.tm_mday + 1,
			buf.tm_hour, buf.tm_min);
	if(w <= 0)	/* Test if something was written */
		{ cLibSibylRT_quietPanic(); };

	/* Test if a message was given. */
	if(msg != NULL)
	{
		w = fprintf(panicLog, "  the reason was \"%s\"", msg);
		if(w <= 0)	/* Test if something was written */
			{ cLibSibylRT_quietPanic(); };
	};

	/* Print a new line to the file */
	fprintf(panicLog, "\n");

	/* Close the file */
	fclose(panicLog);

	/* Call now the quite version of this function */
	cLibSibylRT_quietPanic();
}; /* End panic */


void
cLibSibylRT_quietPanic()
{
	/* we imediatly call the abort function */
	abort();
}; /* End: quitePanic */


void
cLibSibylRT_outputImpl(
	char* msg)
{
	/* Test if the runtime is not initialized */
	if(cLibSibylRT_isInit() != 0)
		{ cLibSibylRT_panic("Tried to print but runtime is not initialized."); };

	/* Get teh lock for the file
	 *  We only rease this lock at the end of the function.
	 *  Since all other "exit path" will call panic. */
	CSIBYL_RT_GET_LOCK(cLibSibylRT_OutLock_var);

	/* This is the variable for storing the number of written characters */
	Int_t w = -1;


	/* Get the current time */
	time_t t = time(NULL);
	struct tm* tmp = localtime(&t);
		if(tmp == NULL)
			{ cLibSibylRT_panic("Unable to load time."); };

	/* copy it into a better format */
	struct tm buf = *tmp;

	/* This is the variable for storing the filename */
	Char_t fileName[256];

	/* composing the filename */
	w = snprintf(fileName, 255, "cLibSibyl_out_pid%d.txt", cLibSibylRT_getInitialPID());
	if((w <= 0) || (w >= 250))
		{ cLibSibylRT_quietPanic(); };

	/* Now we try to open a file */
	FILE* streamOut = fopen(fileName, "a+");
		if(streamOut == NULL)
			{ cLibSibylRT_panic("Unable to open the file for writting a message."); };

	/* Now start real writing */
	w = fprintf(streamOut, "%d-%d-%d %d:%d :: ",
			buf.tm_year + 1900, buf.tm_mon + 1, buf.tm_mday + 1,
			buf.tm_hour, buf.tm_min);
		if(w <= 0)
			{ cLibSibylRT_panic("Unable to write time."); };

	/* Write the message that was passed by the user, if any. */
	if(msg == NULL)
	{
		w = fprintf(streamOut, "NO MESSAGE WAS GIVEN");
	}
	else
	{
		/* The message is given, so write it */
		w = fprintf(streamOut, "%s", msg);
	}
	if(w <= 0)
		{ cLibSibylRT_panic("Unable to write message."); };


	/* We only need to realase the lock at this point.
	 * Since all other exits are associated with absolute panic. */
	CSIBYL_RT_REL_LOCK(cLibSibylRT_OutLock_var);


	/* cloase the file stream */
	fprintf(streamOut, "\n");
	fclose(streamOut);

	return;
}; //End write outsideImpl


/*
 * ========================
 * Error status control
 */

Int_t
cLibSibylRT_lastExitStatus(void)
{
	CSIBYL_RT_GET_LOCK(cLibSibylRT_errorLock_var);

		__asm__ volatile ("mfence" ::: "memory");
	Int_t dolly = cLibSibylRT_errorCode_var;
		__asm__ volatile ("mfence" ::: "memory");

	CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var);
	return dolly;
};


Int_t
cLibSibylRT_aquireErrorState(void)
{
	CSIBYL_RT_GET_LOCK(cLibSibylRT_errorLock_var);

		__asm__ volatile ("mfence" ::: "memory");
	Int_t dolly = cLibSibylRT_errorFlag_var;	/* get the current error flag */
	cLibSibylRT_errorFlag_var += 1;			/* increase the error flag, I do not thing we will overflow */
		__asm__ volatile ("mfence" ::: "memory");

	CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var);

	return dolly;		/* return the old state */
};


Int_t
cLibSibylRT_errorLineNumber(void)
{
	return cLibSibylRT_errorLine_var;
};


Int_t
cLibSibylRT_errorFuncNameN(void)
{
	return cLibSibylRT_errorFuncN_var;
};


Char_t
cLibSibylRT_errorFuncNameChar(
	Int_t 			n)
{
	if(!((0 <= n) && (n < cLibSibylRT_errorFuncN_var)))
	{
		cLibSibylRT_panic("Function name is out of bound.");
	};
	if(cLibSibylRT_errorFuncC_var == NULL)
	{
		cLibSibylRT_panic("Function name string is not allocated.");
	};

	return cLibSibylRT_errorFuncC_var[n];
};


Int_t
cLibSibylRT_errorCondN(void)
{
	return cLibSibylRT_errorCondN_var;
};


Char_t
cLibSibylRT_errorCondChar(
	Int_t 			n)
{
	if(!((0 <= n) && (n < cLibSibylRT_errorCondN_var)))
	{
		cLibSibylRT_panic("Condition string is out of bound.");
	};
	if(cLibSibylRT_errorCondC_var == NULL)
	{
		cLibSibylRT_panic("Condition string is not allocated.");
	};

	return cLibSibylRT_errorCondC_var[n];
};


Int_t
cLibSibylRT_errorClear(void)
{
	/* check if the runtime is initialized */
		__asm__ volatile ("mfence" ::: "memory");
	if(cLibSibylRT_isInit() != 0)
		{cLibSibylRT_panic("Wanted to set the error, without an initailized runtime.");};

	/* At least, the flag must be greater than zero, even here, not no race condition */
		__asm__ volatile ("mfence" ::: "memory");
	if(cLibSibylRT_errorFlag_var == 0)
		{cLibSibylRT_panic("No flag was ever aquiered.");};

	/* aquiere the lock */
	CSIBYL_RT_GET_LOCK(cLibSibylRT_errorLock_var);

	/* No error was set */
	if(cLibSibylRT_errorCode_var == 0)
		{return 1;};

	/* clear linenumber */
	cLibSibylRT_errorLine_var = -1;

	/* Clear condition string */
	if(cLibSibylRT_errorCondN_var > 0)
	{
		free(cLibSibylRT_errorCondC_var);
		cLibSibylRT_errorCondC_var = NULL;
		cLibSibylRT_errorCondN_var = 0;
	};
	if(cLibSibylRT_errorCondC_var != NULL)
		{CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var); return -1;};

	/* Clear function string */
	if(cLibSibylRT_errorFuncN_var > 0)
	{
		free(cLibSibylRT_errorFuncC_var);
		cLibSibylRT_errorFuncC_var = NULL;
		cLibSibylRT_errorFuncN_var = 0;
	};
	if(cLibSibylRT_errorFuncC_var != NULL)
		{CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var); return -2;};

	cLibSibylRT_errorCode_var = 0;		/* Clear the error code */
	cLibSibylRT_errorFlag_var = 0;		/* reset the flag       */

	/* Relaease the lock */
	CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var);

	return 0;
};


Int_t
cLibSibylRT_errorSetError(
	const    Int_t 		errCode,
	const    Char_t*	cond,
	const    Char_t* 	funcName,
	const    Int_t 		lineNo,
	volatile Int_t* const 	retVal)
{
	/* check if the runtime is initialized */
	if(cLibSibylRT_isInit() != 0)
		{ cLibSibylRT_quietPanic(); };

	/* aquiere the lock */
	CSIBYL_RT_GET_LOCK(cLibSibylRT_errorLock_var);


	/* Set the passed variable to the given value.
	 * if it has not yet a non zero value
	 * This is done before we check if an error is
	 * already installed.
	 * This will allow us to reach consensus in a multi threaded
	 * C code */
	if(retVal != NULL)
	{
		/* Set only if it does not have a non zero value yet. */
		if((*retVal) == 0)
			{*retVal = errCode;};
	}; /* End if: set the passed variable */

	/* Test if an error is already installed in the runtime.
	 * If so, we have to exit */
	if(cLibSibylRT_errorCode_var != 0)
		{CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var); return 11;};

	/* zero will generate an error */
	if(errCode == 0)
		{CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var); return -1;};

	/* Set line number, map negative numbers to -1 */
	cLibSibylRT_errorLine_var = (lineNo <= 0) ? -1 : lineNo;

	/* set the error code */
	cLibSibylRT_errorCode_var = errCode;

	/* Store the function name */
	Int_t fnInstall = cLibSibyl_setString(funcName, &cLibSibylRT_errorFuncC_var, &cLibSibylRT_errorFuncN_var);
	if(fnInstall != 0)
		{CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var); return (-10000 + fnInstall);};

	/* Store the condition */
	Int_t confInstall = cLibSibyl_setString(cond, &cLibSibylRT_errorCondC_var, &cLibSibylRT_errorCondN_var);
	if(confInstall != 0)
		{CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var); return (-20000 + confInstall);};

	/* Relaease the lock */
	CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var);

	return 0;
}; /* End: set the error */


Int_t
cLibSibylRT_syncRetValueSet(
 	volatile Int_t* const 	retVal,
 	const    Int_t		desValue)
{
	/* Test if the runtime system is initialized.
	 * No message possible */
	if(cLibSibylRT_isInit() != 0)
		{ cLibSibylRT_quietPanic(); };

	/* Test if arguments are valid */
	if(retVal == NULL)	/* No variable is given */
		{return -1;};
	if(desValue == 0)	/* this makes it a no-ops */
		{return 1;};


	/* aquiere the lock */
	CSIBYL_RT_GET_LOCK(cLibSibylRT_errorLock_var);

	/* Test if the return variable is already set */
	if((*retVal) != 0)
	{
		/* The return variable is already set, so we return */
		CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var);
		return 2;
	}
	else
	{
		/* The return variable was not set yet, so we set it and return */
		*retVal = desValue;
		CSIBYL_RT_REL_LOCK(cLibSibylRT_errorLock_var);
		return 0;
	};

	return -3;
}; /* end: set error code */


Int_t
cLibSibylRT_getPID()
{
	const pid_t pid = getpid();

	return ((Int_t)(pid));
}; /* end: get PID */


Int_t
cLibSibylRT_getInitialPID()
{
	/* We have to check if RT is initialized. */
	if(cLibSibylRT_isInit() != 0)
		{ cLibSibylRT_quietPanic(); };

	/* return the value */
	return cLibSibylRT_initPID_var;
}; /* End: get initial PID */


Int_t
cLibSibylRT_samePID()
{
	/* Load the two PID */
	Int_t initPID = cLibSibylRT_getInitialPID();
	Int_t currPID = cLibSibylRT_getPID();

	/* They are the same */
	if(initPID == currPID)
		{ return 0; };

	/* not the same */
	return (!(0));
}; /* end: test if the PID has changed */


