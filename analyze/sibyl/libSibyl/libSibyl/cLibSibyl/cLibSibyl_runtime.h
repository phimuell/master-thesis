/**
 * \brief	This file declares some runtime parts of the library.
 */
#ifndef CLIBSIBYL_RUNTIME_HEADER
#define CLIBSIBYL_RUNTIME_HEADER

/* includes */
#include "./cLibSibyl_types.h"

#include <stdio.h>


/*
 * This is visibility controll
 * Note that "default" means public.
 */
#if !defined(__GNUC__)
#	error "Only gcc is supported."
#elif __GNUC__ < 4
#	error "gcc must have at least version 4."
#else

/*
 * Define expot macros
 * default mean public
 */
#	define CSIBYL_EXPORT    __attribute__ ((visibility ("default")))
#	define CSIBYL_IMPORT    __attribute__ ((visibility ("default")))
#	define CSIBYL_INTERN    __attribute__ ((visibility ("hidden")))

#endif

#ifdef CSIBYL_STRINGIFY_INTERNAL
#	undef CSIBYL_STRINGIFY_INTERNAL
#endif
#define CSIBYL_STRINGIFY_INTERNAL(e) #e


/**
 * \brief	Enables debugging capability.
 * \define	CSIBYL_REAL_ASSERT
 *
 * Defining this macro, to a value other than zero, will enable
 * debug facility in CSIBYL. It will make asserts and exit into
 * real asserts. Exit will generate only an assert if the exit
 * state is not zero.
 */
#define CSIBYL_REAL_ASSERT 0


#if defined(CSIBYL_REAL_ASSERT) && (CSIBYL_REAL_ASSERT != 0)
#	include <assert.h>
#endif





/*
 * ========================
 * ATTRIBUTE
 * Here are some attribute from GCC
 */
#if defined(CSIBYL_USE_ATTRIBUTE) && (CSIBYL_USE_ATTRIBUTE != 0)

	/**
	 * \brief	Tells GCC to spend more time to optimize the function
	 */
#	define CSIBYL_ATTR_HOT 		__attribute__((hot))

	/**
	 * \brief	Tells GCC that the function is a malloc like function
	 */
#	define CSIBYL_ATTR_MALLOC 	__attribute__((malloc))

	/**
	 * \brief	Tells GCC that the function will not return
	 */
#	define CSIBYL_ATTR_NORETURN	__attribute__ ((noreturn))

	/**
	 * \brief	Tells GCC that the function is maybe unused and no warning should be generated.
	 */
#	define CSIBYL_ATTR_UNUSED 	__attribute__((unused))

#else
	/*
	 * No attributes are requiered, so we null the macro
	 */
#	define CSIBYL_ATTR_HOT
#	define CSIBYL_ATTR_MALLOC
#	define CSIBYL_ATTR_NORETURN
#	define CSIBYL_ATTR_UNUSED

#endif /* END ATTRIBUTES */




/*
 * =======================
 * Controle Macros
 * cLibSibyl is not used as a stand allone library,
 * but has to work in conjunction with the Python
 * interpreter, so a set of controll macros are
 * provided to achieve that in a uniform way.
 *
 * Note that this imply a special way of programming.
 * All variables have to be declared at the beginning.
 * Also all pointer variables, that will point to
 * dynamically allocated memeory must be initialized
 * with NULL. Then the start macro has to be placed,
 * and a special end statement has to be used, together
 * with a clean up section, that can be executed.
 * Note that gotos are used for that purpose.
 *
 * Note that the assert system of cSibyl is incooperated
 * into the runtime
 */

/**
 * \brief	This macro marks the begin of an cSibyl function.
 * \def 	CSIBYL_RT_FUNC_START
 *
 * All variable declarations, must be done above this macro.
 * All pointers must be inizialized with NULL.
 *
 * Note this macro will not generate code that checks if the
 * runtim is initialized.
 */
#define CSIBYL_RT_FUNC_START Int_t volatile CSIBYL_FKT_RET_VAL __attribute__ ((aligned))  = 0


/**
 * \brief	This macro can be used to check if the runtime
 * 		 is initialized.
 * \brief	CSIBYL_RT_IS_INIT
 *
 * If the library is not initialized and generate an error otherwise.
 * Since the runtime system is not initialized, this macro will go to
 * the clean up section execute it and then return the custom error
 * code '-123456789'.
 * This macro must be placed after the mark of the start macro.
 * It also only works if the function is integrated into the
 * runtime system, but the runtime system does not work.
 *
 * This macro is for thingle threaded code.
 */
#define CSIBYL_RT_IS_INIT  if(cLibSibylRT_isInit() != 0) {CSIBYL_FKT_RET_VAL = -123456789; goto CSIBYL_RT_FUNC_CLEANUP;}	/* we can not use the safe synchronization */


/**
 * \brief	This is a jump mark that marks the beginning of the
 * 		 cleanup code.
 * \define 	CSIBYL_RT_FUNC_CLEANUP
 *
 * This is a jump mark, target of goto, so it must be terminated
 * with ":". If the function does not need cleanup, then
 * it must be laced imediatly before the return macro.
 */
#define CSIBYL_RT_FUNC_CLEANUP cSibylCleanUpSectionStart


/**
 * \brief	This macro is like the return statement.
 * \define 	CSIBYL_RT_FUNC_RETURN
 *
 * It will return the status code of the function.
 * It must be placed imediatly after the clean up
 * code.
 */
#define CSIBYL_RT_FUNC_RETURN return ((Int_t)CSIBYL_FKT_RET_VAL)


/**
 * \brief	This macro expands to local variable, if sibyl context are used,
 * 		 which is the return variable.
 * \define	CSIBYL_RT_LOC_RETRUN_VAR
 */
#define CSIBYL_RT_LOC_RETURN_VAR  ((const Int_t)(CSIBYL_FKT_RET_VAL))



/**
 * \brief	This function like macro, works like the exit command of the C library.
 * \define 	cLibSibylRT_exit
 *
 * This macro call is redirected to the exit2 macro.
 * The message that is used is the "Unspecific error occured.".
 *
 * \param  retCode 	The return code of the function.
 */
#define cLibSibylRT_exit(retCode) {cLibSibylRT_exit2(retCode, "Unspecific error occurred.");}


/**
 * \brief	This function like macro, works like the exit command from the C library.
 * 		 But an error message can be specified.
 * \define 	cLibSibylRT_exit2
 *
 * This function will install an error message in the runtime system.
 * This message can then by extracted by Python. After the error is
 * installed, execution will jump to the cleanup mark and the function
 * will exit.
 *
 * Installation of an error is synchronized and happens in a single
 * total order. If an error is already installed in the runtime system,
 * then this error will not be installed and no action on the global
 * state is done. However in either case, the local return code can
 * be modified. If the local error code is non zero, it will be
 * set to the given error value. In the case the local error number
 * is already non zero, no action will be performed. This allows
 * a team of thread to reach consensus about the return value.
 *
 * This function needs an initialized runtime and a CSIBYL context.
 *
 * \param  retCode 	The return code of the function.
 * \param  desc		The description that should be included.
 *
 * \note	Errors during the installation of the error, will be ignored.
 */
#if defined(CSIBYL_REAL_ASSERT) && (CSIBYL_REAL_ASSERT != 0)
#	define cLibSibylRT_exit2(retCode, desc) {if((retCode) != 0) {assert(0 && desc); }; goto CSIBYL_RT_FUNC_CLEANUP;}
#else
#	define cLibSibylRT_exit2(retCode, desc) {if((retCode) != 0) {cLibSibylRT_errorSetError((Int_t)(retCode), desc, __func__, __LINE__, &CSIBYL_FKT_RET_VAL);}; goto CSIBYL_RT_FUNC_CLEANUP;}
#endif


/**
 * \brief	This macro will jump to the cleanup section.
 * \define 	cLibSibylRT_abort
 *
 * This function aborts the computation without installing an
 * error in the runtime. This function guarantees consensus.
 * Meaning that if the return variable was already set to
 * a specific value, then no assignment will take place.
 * This can happen in a multi threaded context and guarantees
 * that only one error propagated, if this is desirable
 * is an other discussion.
 *
 * After the setting the return variable, it will jump to the
 * cleanup section.
 *
 * Note that this function allows the value zero as erro code.
 * In this case the function is considered a no ops, even
 * the lock is not aquiered. In this case a positive value
 * is returned.
 *
 * Zero is returned if the value could be set, also a positive
 * value is returned if the return variable was already set
 * and a negative value is set upon an error.
 *
 * \param  errCode	The error code to set, zero is legal.
 */
#if defined(CSIBYL_REAL_ASSERT) && (CSIBYL_REAL_ASSERT != 0)
#	define cLibSibylRT_abort(errCode) { assert(0 && "ABORT");};
#else
#	define cLibSibylRT_abort(errCode) {if(cLibSibylRT_syncRetValueSet(&CSIBYL_FKT_RET_VAL, (errCode)) < 0) {cLibSibylRT_panic("ABORT FAILED");};goto CSIBYL_RT_FUNC_CLEANUP;}
#endif


/**
 * \briefe 	This is the panic function. It will lead to the abortion of the process.
 *
 * This function will call exit the program, which also aborts the Python interpreter.
 * This is a not desirable effect, and should only be called if necessary.
 * The function will print the passed text to a file.
 *
 * The file name has the following pattern "cLibSibyl_panic_pid${PID}.log", where $PID}
 * is the initial pid of the interpreter.
 *
 * Note calling this function, even with NULL as argument, needs runtime support.
 * If you are unable to guarante a proper runtime situations, you should consider
 * using cLibSibylRT_quietPanic(), which does not requiere runtime stupport.
 *
 * If the runtime is not initialized, quietPanic will be called.
 *
 * \param  msg		Optional message argument, set to NULL to not print.
 */
extern
CSIBYL_INTERN
void
cLibSibylRT_panic(
	char* msg)
 CSIBYL_ATTR_NORETURN;


/**
 * \brief	This is the quiet panic function.
 *
 * What differes this function from the other one, it does not requieres
 * the runtime to be pressent.
 *
 * It will imediatly call abort() of the C-Runtime.
 */
extern
CSIBYL_INTERN
void
cLibSibylRT_quietPanic()
 CSIBYL_ATTR_NORETURN;


/**
 * \brief	This function checks if the runtime is initialized.
 *
 * This function returns zero if the library is initialized and can
 * be used. It returns a non zero value if not.
 */
extern
CSIBYL_EXPORT
Int_t
cLibSibylRT_isInit(void);


/**
 * \brief	This function is able to write to a fictional output.
 *
 * This is the base implementation. It is only able to process
 * NULL terminated string buffers, there is a macro that is able
 * to emulate printf syntax.
 *
 * The string will be written to a file, cLibSibyl_out_pid${PID}.txt.
 * ${PID} will be substituted by the initial PID.
 *
 * \param  msg 		The string that should be written.
 */
extern
CSIBYL_INTERN
void
cLibSibylRT_outputImpl(
	char* msg);


/*
 * \brief	This is a macro that is able to emulate printf behaviour.
 *
 * It composes a string and will then pass it to the outputImpl function.
 * Note it is only possible to process strings up to 10'000 char.
 *
 * \param  fmt 		The format.
 * \param  ...		The formating stuff.
 */
#define cLibSibylRT_printf(fmt, ...) {					\
	Char_t buff[10010];						\
	const Int_t n = snprintf(buff, 10010, fmt __VA_OPT__(,)  __VA_ARGS__);	\
	if(n >= 10000) {cLibSibylRT_panic("Tried to write too long buffer.");}; \
	cLibSibylRT_outputImpl(buff); }



/**
 * \brief	This macro allows to call a cLibSibyl function and call the exit routine on failure.
 * \define	CSLIBYL_CALL
 *
 * This function will not work with the association function.
 * Also it needs a sibyl context beeing pressent.
 *
 * \param  fName 	The function name.
 * \param  ..		Arguments
 */
#define CSIBYL_CALL(fName, ...) { CSIBYL_MCALL("The function \"" CSIBYL_STRINGIFY_INTERNAL(fName) "\" has exited with a non success error code.", fName, __VA_ARGS__);}


/**
 * \brief	Like CSIBYL_CALL but with an error message, first argument.
 * \define	CSLIBYL_MCALL
 *
 * This function will not work with the association function.
 * Also it needs a sibyl context beeing pressent.
 *
 * \param  msg		An error message.
 * \param  fName 	The function name.
 * \param  ..		Arguments
 */
#define CSIBYL_MCALL(msg, fName, ...) { Int_t r = fName(__VA_ARGS__); {if(r != 0) {cLibSibylRT_exit2(r, msg);}}   }


/*
 * ==========================
 * Installing an error
 * These functions allows to install an error into the runtime.
 * Without calling exit. These function should only be used
 * internaly and with great care.
 */

/**
 * \brief	This macro will try to install an error into the system.
 * \define	CSIBYL_RT_INSTALL_ERROR
 *
 * This function will only install the error and no further
 * actions are taken, such as the exit macro does. However
 * this macro does not need a sibly context. If the library
 * is not initialized, the error will not be installed.
 * Also an error in the instaliation will be ignored.
 *
 * This macro evaluate to zero if the installation was
 * successfull. A positive value means that an error
 * was already installed. A negative value indicates
 * an internal error, including non initialized
 * runtimes.
 *
 * \param  errCode	The error code to install.
 * \param  msg		The error message to install.
 */
#if defined(CSIBYL_REAL_ASSERT) && (CSIBYL_REAL_ASSERT != 0)
#	define CSIBYL_RT_INSTALL_ERROR(errCode, msg)  assert(0 && msg)
#else
#	define CSIBYL_RT_INSTALL_ERROR(errCode, msg)  ((cLibSibylRT_isInit() == 0) ? cLibSibylRT_errorSetError((Int_t)(errCode), msg, __func__, __LINE__, NULL) : -777)
#endif


/*
 * ========================
 * Error Status
 * These functions allows to return the error status and more information about
 * the runtime. This functions are exported, such that they could be used from
 * Python. They will bypass the non existiung stderr/stdout stream.
 */

/**
 * \brief	This function returns the exit status of the last call.
 *
 * Note that status will not be changed until it is cleared.
 * Note that this function will aquiere the lock. It is not guaranteed
 * that a complete syncronization has been establiched.
 *
 * This function can be called without calling the aquiereing function.
 * It can be used to test if the error belongs to a thread or not.
 */
extern
Int_t
cLibSibylRT_lastExitStatus(void)
 CSIBYL_EXPORT;


/**
 * \brief	This function returns the line number of the error.
 *
 * This function returns -1, if no line number is known.
 *
 * Note this function is only allowed to be called after the
 * aquireErrorState function had been called and the return
 * value was zero.
 * A zero value means that the thread is responsible for it.
 */
extern
Int_t
cLibSibylRT_errorLineNumber(void)
 CSIBYL_EXPORT;


/**
 * \brief 	This function returns the number of characters of the
 * 		 function where the error was recorded.
 *
 * The number does not include the null character at the end of the function name.
 * If no name is stored, 0 will be returned.
 *
 * Note this function is only allowed to be called after the
 * aquireErrorState function had been called and the return
 * value was zero.
 * A zero value means that the thread is responsible for it.
 */
extern
Int_t
cLibSibylRT_errorFuncNameN(void)
 CSIBYL_EXPORT;


/**
 * \brief	This function returns the ith caracter of the function
 * 		 where the error was recorded.
 *
 * If n is out of range, panic will be called.
 *
 * Note this function is only allowed to be called after the
 * aquireErrorState function had been called and the return
 * value was zero.
 * A zero value means that the thread is responsible for it.
 *
 * \param  n 	The caracter that
 */
extern
Char_t
cLibSibylRT_errorFuncNameChar(
	Int_t 			n)
 CSIBYL_EXPORT;


/**
 * \brief 	This function returns the number of characters of the
 * 		 condition or description that faild.
 *
 * The number does not include the null character at the end of the function name.
 * If no name is stored, 0 will be returned.
 *
 * Note this function is only allowed to be called after the
 * aquireErrorState function had been called and the return
 * value was zero.
 * A zero value means that the thread is responsible for it.
 */
extern
Int_t
cLibSibylRT_errorCondN(void)
 CSIBYL_EXPORT;


/**
 * \brief	This function returns the nth character of the condition/
 * 		 description that has failed.
 *
 * If n is out of range, panic will be called.
 *
 * Note this function is only allowed to be called after the
 * aquireErrorState function had been called and the return
 * value was zero.
 * A zero value means that the thread is responsible for it.
 *
 * \param  n 	The caracter that
 */
extern
Char_t
cLibSibylRT_errorCondChar(
	Int_t 			n)
 CSIBYL_EXPORT;


/**
 * \brief	This function returns zero if the thread has granted
 * 		 the permission to read the error state.
 *
 * This function is provided to reach some synchronization.
 * Only thread that called this function with a zero return value
 * are allowed/requiered to call the clear function afterwards.
 *
 * This function will aquire the lock.
 */
extern
Int_t
cLibSibylRT_aquireErrorState(void)
 CSIBYL_EXPORT;


/**
 * \brief	This function clears the error status.
 *
 * This function will return zero if the status was cleared.
 * It will return a positive number if no error code was stored
 * at all. An negtive number if the error was set but freeing
 * it failed.
 *
 * Note this function is only allowed to be called after the
 * aquireErrorState function had been called and the return
 * value was zero.
 * A zero value means that the thread is responsible for it.
 *
 */
extern
Int_t
cLibSibylRT_errorClear(void)
 CSIBYL_EXPORT;


/**
 * \brief	This function allows to set the error.
 *
 * This function will return zero if the condition was
 * set successfully. A positive number if the error was
 * not set, because another condition was set before.
 * A negative value if the an error during the setting
 * occured.
 *
 * The error value must be non zero, the other values
 * are basically free. All strings must be null terminated.
 *
 * The last argument of this function is a pointer, that is
 * optional. This pointer points to the return variable.
 * If the pointer is given it will only process it, in
 * the lock section, meaning all participating thread in
 * context will synchronize. The variable is set to the
 * passed error code, only if the variable is non zero.
 * This means that all thread of a contxct will reach
 * consensus about an error status. And only the first
 * error, will be the return type.
 * Note that there is no guarantee that the error was
 * installed. However this synchronization is guaranteed
 * even if the error was not installed, because an other
 * python thread has propagated.
 *
 * \param  errCode	The error code that should be stored.
 * \param  cond 	The condition or description of the error.
 * \param  funcName	The name of the function where the error occured.
 * \param  lineNo	The line number where the error occured.
 * \param  retVal 	Pointer to the return value, can be NULL.
 */
extern
Int_t
cLibSibylRT_errorSetError(
	const    Int_t 		errCode,
	const    Char_t*	cond,
	const    Char_t* 	funcName,
	const    Int_t 		lineNo,
	volatile Int_t* const 	retVal)
 CSIBYL_INTERN;


/**
 * \brief	This function is able to set the return value of a sibyl context.
 *
 * This function will aquiere the the lock that protects the error state and
 * will then set the passed variable to the desired value, if the passed variable
 * is not set already to a none zero value.
 *
 * Put it differently, it will perform the assignment only if the
 * value was not set already to a non zero value.
 *
 * This function allows to agree a team of threads about an error code.
 * This function ignores the error code that is installed in the runtime.
 * This is different from the behaviour before.
 *
 * \param  retVar	Pointer to the variable to set.
 * \param  desVal	The desired value.
 */
extern
Int_t
cLibSibylRT_syncRetValueSet(
 	volatile Int_t* const 	retVal,
 	const    Int_t		desValue)
 CSIBYL_INTERN;


/*
 * =============================
 *	PID
 *
 * This allows to manage the PID of the interpeter.
 */

/**
 * \brief	This function is able to return the PID of the interpeter.
 *
 * For this the underling OS functions are used. It thus returns the
 * _actual_ PID of the process. Thus there is no guarantee that the
 * returned value remains the same accross different calles.
 */
extern
Int_t
cLibSibylRT_getPID()
 CSIBYL_EXPORT;


/**
 * \brief	This function is able to return the _initial_ PID of the interpeter.
 *
 * This function returns the initial PID of the runtime. This number was queried
 * at the start up of the interpeter. It is possible that the value differs
 * from the one that is returned by getPID().
 */
extern
Int_t
cLibSibylRT_getInitialPID()
 CSIBYL_INTERN;


/**
 * \brief	Thus function returns zero if the current and the initial PID are the same.
 *
 * It returns a non-zero number if the two PID differs.
 * Returns zero if they are the same.
 */
extern
Int_t
cLibSibylRT_samePID()
 CSIBYL_INTERN;


/*
 * =========================
 * Multi Threading
 * Here are helper structure for the case that
 * openMP is used to parallelize the loop.
 *
 * openMP is expected that no paralell for is used
 * directly. Instead a parallel section should be opened
 * and then the for loop annotated.
 * And CSIBYL_FKT_RET_VAL must be set to shared.
 *
 * Also no exits and asserts are allowed.
 * There is a special kind of assert that does this.
 *
 * Note that the error handling, is not fully thread safe.
 * It can lead to memory leaks in C if an error happens
 * in multiple threads, and are not seperated by enough times.
 * Problems can also arrise, if CSIBYL is called from multiple
 * python threads and errors happens. then it could
 * even lead to segmentation faults. This can be avoided by
 * not useing the error reporting functions.
 */
#if 0

/**
 * \brief	This macro must be the first statement inside the loop body.
 *
 * This macro will generate code that will prevent the execution of the loop
 * if an error was detected.
 *
 * See "https://stackoverflow.com/questions/9793791/parallel-openmp-loop-with-break-statement/9836422#9836422"
 */
#if defined(_OPENMP)
#	define CSIBYL_PARRT_START_BODY if(CSIBYL_PARRT_HAD_ERROR) {continue;}
#else
#	define CSIBYL_PARRT_START_BODY if(CSIBYL_PARRT_HAD_ERROR) {break;};
#endif


/**
 * \brief	This macro marks the end of the parallel section, but must be outside of it.
 *
 * This macrto must be placed right after the parallel region, as first command.
 * It will check if an error had occured and will call exit on the behave of the user.
 */
#define CSIBYL_PARRT_END_PAR_REGION if(CSIBYL_PARRT_HAD_ERROR) { cLibSibylRT_exit(CSIBYL_FKT_RET_VAL); }


/**
 * \brief	This macro tests if parallel loop should be canceled.
 *
 * This basically tests if an error was detected.
 */
#define CSIBYL_PARRT_HAD_ERROR ((CSIBYL_FKT_RET_VAL) != 0)


/**
 * \brief	This macro will stop the exectution of the current parallel loop.
 *
 * Note that this macro oly works if the value that was passed to it was non zero.
 * But this is not checked. Note that there is no guarantee which value
 * will we written to the error if an error was generated.
 *
 * \param  errCode	The error code
 */
#if defined(_OPENMP)
#	define cLibSibylPARRT_exit(errCode) { CSIBYL_FKT_RET_VAL = (Int_t)(errCode);}
#else
#	define cLibSibylPARRT_exit(errCode) cLibSibylRT_exit((errCode))
#endif


/**
 * \brief	This macro expans to the number of threads that should be used.
 *
 * This macro is zero if openMP is not activated othervise a positve number
 */
#if defined(_OPENMP)
#	define CSIBYL_NTHREADS 2
#else
#	define CSIBYL_NTHREADS 0
#endif


/**
 * \brief	This macro is simmilar to teh assert3 macro, but works in parallel loops.
 *
 * \param  cond		The conition to evaluate.
 * \param  id		The error code to generate.
 */
#if defined(NDEBUG)
#	define csibyl_assertPAR(cond, id) {}
#else
#	define csibyl_assertPAR(cond, id) if(!(cond)) { cLibSibylPARRT_exit(id); }
#endif
#endif 	/* MULTI THREADING */

#endif /* end guard */

