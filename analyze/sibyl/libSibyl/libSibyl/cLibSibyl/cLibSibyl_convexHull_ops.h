/**
 * \brief	This file contains functions that operates on the convex hull.
 *
 * Note the force header also contains some functions that deals with
 * operations on the convex hull.
 */
#ifndef CLIBSIBYL_CONVEXHULL_OPS_HEADER
#define CLIBSIBYL_CONVEXHULL_OPS_HEADER

/* Includes */
#include "./cLibSibyl_runtime.h"
#include "./cLibSibyl_types.h"
#include "./cLibSibyl_convexHull.h"


/*
 * ==============================
 * Operations on a convex hull
 */

/**
 * \brief	This function will calculate the midd points of the
 * 		 convex hull.
 *
 * This function will computes the midpoints of the segment of a
 * convex hull. It expects that its pointer arguments are arrays
 * of length nHull - 1.
 *
 * \param  Xm		Array for the x coordinate.
 * \param  Ym		Array for the y coordinate.
 * \param  cHull	The convex hull object.
 */
extern
CSIBYL_INTERN
Int_t
csibyl_convecHull_middlePoints(
	Numeric_t* restrict 		Xm,
	Numeric_t* restrict 		Ym,
	struct sibyl_convexHull_t* 	cHull);


/**
 * \brief	This function will calculate the volume that is enclosed
 * 		 by the convex hull.
 *
 * \param  vol		Argument for returning the volume.
 * \param  cHull	Pointer to the convex hull objct.
 */
extern
CSIBYL_INTERN
Int_t
csibyl_convecHull_volume(
	Numeric_t* const 			vol,
	struct sibyl_convexHull_t* const 	cHull);




#endif /* end guard */

