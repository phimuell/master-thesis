# This file is part of Sibyl.
#
# This file contains the reducers that works on state series.
# This basically reduces each recoded state into an array.


# Normal
import h5py
import numpy as np
import math
import copy

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
import libSibyl.sibyl_traits         as sTraits
import libSibyl.sibyl_seriesOps      as sOps


# This function applies a reduce operation to the
#  passed series. The result is a vector with the
#  length equal to the number of states in teh series.
#  Note that the reduced value is casted to a float.
#
#   s   The series that should be reduced.
#   op  The reduce operation that is used.
#
def generalSeriesReduction(s, op):

    if(sTraits.isSeries(s) == False):
        raise ValueError("You have passed a non series object to teh reduce function. Its type was {}.".format(type(s)))
    # end if: is series

    if(sTraits.isValidSeries(s, doThrow = True, noValCheck = False) == False):
        raise ValueError("You have passed an invalid series, no finite check was done.")
    # end if: is valid

    if(op is None):
        raise ValueError("Passed None as operation.")
    # end if: is None

    # Get the number of states
    nStates = sTraits.nStates(s)

    # Allocate the output array
    redSer = np.zeros(nStates)

    #
    # Now performing the reduction
    for i in range(nStates):
        redSer[i] = float(op(s[i]))
    # end for(i):

    # Return the value
    return redSer
# end def: general series reductor


# This function reduces the passed series to a mean.
#  This function basically calls the general series
#  reduction function, with np.mean as op.
#
#   s   Series to reduce
#
def reduceToMean(s):
    return generalSeriesReduction(s = s, op = np.mean)
# end def: reduce to mean


# This function reduces the passed series to a minimum.
#  This function basically calls the general series
#  reduction function, with np.min as op.
#
#   s   Series to reduce
#
def reduceToMin(s):
    def op(s):
        return s.min()
    # end def: op

    return generalSeriesReduction(s = s, op = op)
# end def: reduce to mean


# This function reduces the passed series to a maximum
#  This function basically calls the general series
#  reduction function, with np.max as op.
#
#   s   Series to reduce
#
def reduceToMax(s):
    def op(s):
        return s.max()
    # end def: op

    return generalSeriesReduction(s = s, op = op)
# end def: reduce to max


# This function reduces two series, to the mean
#  squarted error. This is done by first calling
#  sOps.squareError() and then pass it to the
#  mean reducer.
#
#   s1      The first series
#   s2      The second series
#
def meanSquareError(s1, s2, sameLength = True):

    return generalSeriesReduction(
            s  = sOps.squareDifferenceSeries(s1 = s1, s2 = s2, sameLength = sameLength),
            op = np.mean)
# end def: mean square error



