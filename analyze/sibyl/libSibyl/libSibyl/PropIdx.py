# This file is part of SIBYL2.0
#
# This file oprovides a simple way of creating a map that is based on
# enumerations. Note that this is a work in progress and the keys are
# only added on a "on request" base.
# They are basically a port of the PropIdx enum class that is porvided
# in EGD.


# Import the enum
from enum import Enum

# This is a class that is used to automumber the enum constants
# This is from "https://docs.python.org/3.4/library/enum.html#autonumber"
class AutoNumberEnum(Enum):
    def __new__(cls):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj
    # End: __new__
# End class(AutoNumberEnum):


#
# This is the class for naming the properties
class PropIdx(AutoNumberEnum):

    #
    # These are the different names
    Density                 = ()
    Viscosity               = ()
    Temperature             = ()
    RhoCp                   = ()
    ThermConduct            = ()
    ThermExpansAlpha        = ()
    CompressibililityBeta   = ()
    StrainRate              = ()
    Stress                  = ()
    RadioEnergy             = ()
    AdiabaticHeat           = ()
    ShearHeat               = ()
    FullHeatingTerm         = ()
    Type                    = ()

    # Note that on the grid they are fack properties.
    #  They will me mapped to the mechanical solution.
    #  Or the marker velocities.
    VelX                    = ()
    VelY                    = ()
    Pressure                = ()

    # This property is for accessing the velocity that was
    #  interpolated from the markers to the gird and
    #  was used for inertia (intrinsic velocity).
    #  They are NOT treated as velocities.
    gVelX                   = ()
    gVelY                   = ()

    # This is a fake property that allows accessing
    #  the temperature solution.
    TempDiff                = ()

    # This is the feel velocity, it is the velocity that was used
    #  to move the markers, it is only defined at the markers location.
    #  Also note that it is different from the intrinsic velocity.
    #  Also note that this property is not a velocity and is also not special, but it is on C++
    FeelVelX                = ()
    FeelVelY                = ()

    # This is a pseudo property that is only usefull
    # on markers, it is their position
    PosX                    = ()
    PosY                    = ()

    # This is a fake property, it is used to denores
    #  special properties, such as derived ones
    SPECIAL                 = ()

    # This are plotting properties.
    #  They are only present for plotting
    TemperaturePlotBG       = ()
    TemperaturePlotCC       = ()



    #
    # These are the functions

    # This transforms *self into a name
    def toStr(self):
        return self.name
    # end: toStr


    # This function allows the conversion of *self to string automatically
    def __str__(self):
        return self.toStr()
    # end: __str__



    #
    # Test Functions
    #  These are functions that can be used to identify some classes of properties


    # This function returns True if *self is a position.
    def isPosition(self):
        if(self == PropIdx.PosX or self == PropIdx.PosY):
            return True
        return False
    # end def: is Position


    # This function returns True if *self is a velocity
    #  This function does not consider gVel{X,Y} to be
    #  velocities. Note feel velocities are not velocities
    def isVelocity(self):
        if(self == PropIdx.VelX or self == PropIdx.VelY):
            return True
        return False
    # end def: isVelocity


    # Returns ture if *self is a feel velocity
    def isFeelVelocity(self):
        if(self == PropIdx.FeelVelX or self == PropIdx.FeelVelY):
            return True
        return False
    # end def: isFeelVel


    # This function returns True if *self is a
    #  grid Velocity
    def isGridVelocity(self):
        if(self == PropIdx.gVelX or self == PropIdx.gVelY):
            return True
        return False
    # end def: isGridVelocity


    # This function returns True if *self is a Type
    def isType(self):
        if(self == PropIdx.Type):
            return True
        return False
    # end def: isType


    # This function returns True if *self is a
    #  special property
    def isSpecial(self):
        return True if (self == PropIdx.SPECIAL) else False
    # end: isSpecial


    # This function tests if *self is a plot properrty
    def isPlotProperty(self):
        return True if ((self == PropIdx.TemperaturePlotBG) or (self == PropIdx.TemperaturePlotCC)) else False
    # end def: isPlot

# End class(PropIdx)


