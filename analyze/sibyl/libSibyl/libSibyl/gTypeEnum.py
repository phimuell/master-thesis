# This file is part of SIBYL2.0
#
# This file contains an enum for encodeing the grid type.
#

# Import the enum
from enum import Enum

# This is a class that is used to automumber the enum constants
# This is from "https://docs.python.org/3.4/library/enum.html#autonumber"
class AutoNumberEnum3(Enum):
    def __new__(cls):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj
    # End: __new__
# End class(AutoNumberEnum2):


#
# This class can be used to encode the grid type
class eGridType(AutoNumberEnum3):

    #
    # These are the different Types
    RegBasic       = ()
    RegStVx        = ()
    RegStVy        = ()
    RegStPress     = ()
    RegCC          = ()

    ExtStVx        = ()
    ExtStVy        = ()
    ExtStPress     = ()
    ExtCC          = ()

    #
    # These are the functions

    # This transforms *self into a name
    def toStr(self):
        return self.name
    # end: toStr


    # This function allows the conversion of *self to string automatically
    def __str__(self):
        return self.toStr()
    # end: __str__

# End class(eGridType):



