# This file is part of Sibyl.
#
# This file contains functions that helps with teh formating
# of the outputting.


# This function transforms seconds into years.
#  Note that this function is for outputting, such that it returns a string
#  The util header has also a function with the same name. But that function
#  operates on numbers this one on strings.
def secToYear(sec, granularity = 2):

    if(sec < 0):
        raise ValueError("Passed negative time.")
    # end if

    # See: https://stackoverflow.com/questions/4048651/python-function-to-convert-seconds-into-minutes-hours-and-days/24542445#24542445
    intervals = (
        ('years', 31556952),
        ('months', 2629746),
        ('weeks', 604800),  # 60 * 60 * 24 * 7
        ('days', 86400),    # 60 * 60 * 24
        ('hours', 3600),    # 60 * 60
        ('minutes', 60),
        ('seconds', 1),
        )

    result = []
    vals = []

    if(sec == 0):
        return "0 second"

    if(sec < 1.0):
        sec *= 1000.0 * 1000.0
        sec = int(sec)
        sec = sec / 1000.0

        return "{}ms".format(sec)
    # ms Range

    for name, count in intervals:
        value = sec // count
        if value:
            sec -= value * count
            if value == 1:
                name = name.rstrip('s')
            result.append("{} {}".format(int(value), name))
            vals.append(int(value))

    if(vals[0] > 100):
        # We have more than 100 years it is stupid to print month from now on
        return result[0]

    return ', '.join(result[:granularity])
# end: secToYearStr





