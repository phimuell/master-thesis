# This file is part of Sibly.
# It contains code that allows to plot states.

# Normal
import h5py
import numpy as np
import math

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
from .PropIdx import PropIdx
from .simSeries import SimSeries
from .dataBase import DataBase
from libSibyl.extractCommands import toExtract
import libSibyl.sibyl_util as util
import libSibyl.sibyl_traits as sTraits
import libSibyl.sibyl_fmtUtil as fmtUtil
import libSibyl.sibyl_extractor as sExtr
import libSibyl.sibyl_genPlot_helper as plotHelper


# This function can plot a state in a pcolormesh like fashion.
#  It operates on many different objects and allows complex
#  manipulations. It only mandatory argument is the src argument.
#  Which is the source from plotting. all other argument are then
#  determined by that one.
#
#   src     The source what to plot. can be a dtatbase a series or a matrix.
#
#   idx     The index (time) that should be plotted, is needed in case of database or series.A
#   prop    The property that should be plotted only needed in case of database.
#   comp    The component that should be ploted, in case of tensor.
#
#   title   The tile that should be used.
#   xLab    The label for the x axis, defaults to 'x'.
#   yLab    The label for the y axis, defaults to 'y'.
#
#   xPos    Grid position x coordinate.
#   yPos    Grid position y coordinate.
#
#   velArrow    Should vcelocity arrows be plotted, only supported with a database.
#   xStep   Steps for the velocity plotter, defaults 5.
#   yStep   Steps for the velocity plotter, defaults 5.
#
#   mIdx    If given markers will be plottd.
#   mColor  Color for the markers, see SCATTER for informations.
#
#   cMap    The color map to use, defaults to 'jet'.
#   vRange  Tuple for the plotting range in color.
#
#   noBar   Should a color bar be plotted.
#
#
def plotField(
        src,

        idx = None,
        prop = None, comp = None,

        title = None,
        xLab = 'x', yLab = 'y',

        velArrow = False,
        xStep = 5, yStep = 5,

        xPos = None, yPos = None,

        mIdx = None,
        mColor = 'k',

        cMap = 'jet',
        vRange = None,

        noBar = False,

        fName = None
    ):

    # Extract the field
    pField = sExtr.extractState(src = src, idx = idx, prop = prop, comp = comp, onMarker = False)

    #
    # This is the plot points
    plotPoints = (xPos, yPos)
    hasPoints = True if (xPos is not None) else False

    if((xPos is None) != (yPos is None)):
        raise ValueError("plot positions are not conform.")
    # end if: check plots points

    # Generate positions if needed
    if(hasPoints == False):
        plotPoints = sExtr.extractPlotPos(src = src, idx = idx, prop = prop, comp = comp, onMarker = False)
    # end if


    #
    # Determine a good title
    plotTitle = title

    if(plotTitle is None):
        if(sTraits.isSimSeries(src) == True):
            plotTitle = 'Simulation state of {} at time {}'.format(src.getProp().toStr(), fmtUtil.secToYear(src.getTime(idx)) )
        # end if: src is a simulation series
        elif(sTraits.isDataBase(src) == True):
            plotTitle = 'Simulation state of {} at time {}'.format(prop.toStr(), fmtUtil.secToYear(src.getTime(idx)) )
        # end if: src is a database
    # end if: no title given


    #
    # Extract the range
    vMin = None
    vMax = None

    if(vRange is not None):
        vMin = vRange[0]
        vMax = vRange[1]

        if(vMax <= vMin):
            raise ValueError("The passed range was wrong.")
        # end if: check
    # end if: unpacking

    #
    # Now the plotting can be done
    plt.pcolormesh(
            plotPoints[0], plotPoints[1], pField,
            cmap = cMap,
            vmin = vMin, vmax = vMax)

    # Enable the color bar
    if(noBar == False):
        plt.colorbar()
    #end if: noBar


    #
    # Plot the velocity arrows if requested
    if(velArrow == True):

        if(sTraits.isDataBase(src) == False):
            raise ValueError("The passed source is not a database.");
        # end if: no db

        # Extract the velocities
        vx = src.getSimState(prop = PropIdx.VelX, stateIdx = idx, onMarker = False, comp = None)
        vy = src.getSimState(prop = PropIdx.VelY, stateIdx = idx, onMarker = False, comp = None)

        plt.quiver(plotPoints[0][::yStep, ::xStep], plotPoints[1][::yStep, ::xStep], vx[::yStep, ::xStep], -vy[::yStep, ::xStep] )
    # end if: no velocity arrows


    #
    # Plot the markers if requested
    if(mIdx is not None):

        if(sTraits.isDataBase(src) == False):
            raise ValueError("Requested to plot marker mpsotion, but no database was passed.")
        # end if: no database

        # Get the unique list of markers
        plotIdx = list(np.unique(mIdx))

        # Get the positions of the markers
        markerPosX = src.getSimState(prop = PropIdx.PosX, stateIdx = idx, onMarker = True)[plotIdx]
        markerPosY = src.getSimState(prop = PropIdx.PosY, stateIdx = idx, onMarker = True)[plotIdx]

        # Make the scatter plot
        plt.scatter(markerPosX, markerPosY, c = mColor)
    # end if: plot marker position


    # plot the title
    if((plotTitle is not None) or
       (plotTitle != ''      )   ):

        plt.title(plotTitle)
    # end if:

    plt.xlabel(xLab)
    plt.ylabel(yLab)


    # Invert the axis and ajust ratio
    plt.gca().invert_yaxis()
    plt.gca().set_aspect("equal", adjustable = "box")

    # If fName is not None, then call the save function
    if(not sTraits.isNone(fName) ):
        plt.savefig(fName)
    # end if: save


    # Call the show function, such that the plot is shown
    plt.show()
# end def: plotField

#
# This function isable to plot summaries of databases. It allows to plot
#  the reduced state informations, such as the minimum or so. It is very
#  general and operates on two inputs. The most efficient way is to build
#  a simulation series directly, but this function will build one if
#  needed. Start and end times are related to the first source.
#
#   src         This is the standard source
#   src2        This is the second source.
#   prop        The property that should be extracted.
#   prop2       The property from teh second source, defaults to first property.
#   comp        The component of the first source.
#   comp2       The component from teh second source, defaults to the first one.
#   toExtr      What should be extracted.
#   toExtr2     What should be extracted from teh second source.
#
#   vsIdx       If true force the plotting of index.
#   startTime   The start time, that should be used.
#   endTime     The end time, that should be used.
#
#   xLab        The x label.
#   yLab        The y label.
#   title       The tzitle that should be used
#   lab         The lable of the first property.
#   lab2        The label of the second thing to plot.
#
def plot(
        src,
        src2 = None,
        prop = None, prop2 = None,
        comp = None, comp2 = None,
        toExtr = None, toExtr2 = None,
        onMarker = False,

        vsIdx = False,
        startTime = 0, endTime = -1, inYr = False,

        xLab = None, yLab = None,
        xLim = None, yLim = None,
        title = None,
        lly   = False,
        lab = None, lab2 = None, leg = True,
        fmt = '-b', fmt2 = '-r',

        fName = None
    ):

    # Check input
    if(src is None):
        raise ValueError("Passed None as primary source")
    # end if

    # Link secondary to primary source
    if(toExtr2 is None):
        toExtr2 = toExtr
    if(prop2 is None):
        prop2 = prop
    if(comp2 is None):
        comp2 = comp

    # Filling some default values
    if(util.isValidStr(xLab, False) == False):
        xLab = 'Sim Idx' if vsIdx else 'Time [yr]'
    if(util.isValidStr(yLab, False) == False):
        if(prop is not None):
            yLab = str(prop)
        elif(prop2 is not None):
            yLab = str(prop2)
        elif(sTraits.isSimSeries(src) ):
            if(toExtr is not None):
                yLab = str(toExtr) + " of " + str(src.getProp())
            else:
                yLab = str(src.getProp())
        elif(sTraits.isSimSeries(src2) ):
            if(toExtr is not None):
                yLab = str(toExtr2) + " of " + str(src2.getProp())
            else:
                yLab = str(src2.getProp())
        else:
            yLab = 'NA'
    # end if: yLab

    if(sTraits.isNone(lab ) and (not sTraits.isNone(prop )) ):
        lab  = str(prop )
        if(not sTraits.isNone(toExtr ) ):
            lab  = str(toExtr ) + " of " + lab
    if(sTraits.isNone(lab2) and (not sTraits.isNone(prop2)) ):
        lab2 = str(prop2)
        if(not sTraits.isNone(toExtr2) ):
            lab2 = str(toExtr2) + " of " + lab2
    # end if: set a label automatically


    # These are the values that we will use for plotting
    y1 = None
    y2 = None
    x1 = None
    x2 = None

    # Load and plot the first data source
    x1, y1 = plotHelper.plotExtractor_helper(src = src,
                    prop = prop, comp = comp, toExtr = toExtr, onMarker = onMarker,
                    vsIdx = vsIdx, startTime = startTime, endTime = endTime, inYr = inYr)

    # Check if log log plotting is enabled
    if(lly == True):
        plt.gca().set_yscale('log')
    # end if: setScale

    # Plot the first data
    if(util.isValidStr(lab)):
        plt.plot(x1, y1, fmt, label = lab)
    else:
        plt.plot(x1, y1, fmt)
    # end if: plotting the first data


    # Load the second source, if needed
    if(src2 is not None):
        # Load and plot the first data source
        x2, y2 = plotHelper.plotExtractor_helper(src = src2,
                        prop = prop2, comp = comp2, toExtr = toExtr2, onMarker = onMarker,
                        vsIdx = vsIdx, startTime = startTime, endTime = endTime, inYr = inYr)

        # Check if log log plotting is enabled
        if(lly == True):
            plt.gca().set_yscale('log')
        # end if: setScale

        # Plot the first data
        if(util.isValidStr(lab2)):
            plt.plot(x2, y2, fmt2, label = lab2)
        else:
            plt.plot(x2, y2, fmt2)
        # end if: plotting the first data
    # end if: Second sources


    #
    # Perfomr the labeling
    plt.xlabel(xLab)
    plt.ylabel(yLab)

    if(sTraits.isNone(title) == False):
        plt.title(title)
    # endif: title

    if(xLim is not None):
        plt.xlim(xLim)
    if(yLim is not None):
        plt.ylim(yLim)
    # end if: set limits

    if(util.isValidStr(lab2) or util.isValidStr(lab2)):
        plt.legend()
    # end if: legend

    # If fName is not None, then call the save function
    if(not sTraits.isNone(fName) ):
        plt.savefig(fName)
    # end if: save

    plt.show()
# end def: general plot




