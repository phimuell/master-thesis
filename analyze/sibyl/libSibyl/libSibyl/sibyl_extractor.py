# This file is part of Sibyl.
#
# This file contains extractor functions. These are functions that
#  are able to extract data in a uniform and type independand mannor.
#  They where one part of the utility, but because of a cycle in the
#  import system it does not work. I thought C's include statement is
#  strange, but Python's import is way stranger.

# Normal
import h5py
import numpy as np
import math
import copy

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
from .PropIdx import PropIdx
from libSibyl.simSeries import SimSeries
from libSibyl.dataBase import DataBase
from libSibyl.extractCommands import toExtract
import libSibyl.sibyl_traits         as sTraits
import libSibyl.sibyl_util           as util
import libSibyl.sibyl_seriesRed      as sRed



# This function allows to determine which index should of a series should be extracted.
#  This function operates on a time series and on bounds. A list with all index of states
#  that should be extracted is returned. If one of the bounds is an float the value of
#  the time series is considered.
#  If ende is a negative float, then it is considered as the last time that is recorded
#  plus 10 seconds.
#  If both are int then just indexes are condidered.
#  For compability ende has a strange behaviour if it is negative integer.
#       -1 ->   Last accessable index, equal to len(times)
#       -2 ->   second last index.
#       -3 ->   ...
#  This convention is different from other python slicing conventions.
#  However None is also interprted as -1, and should be prefered.
#  Note that start has the usual slicing in place.
#
#   Times       The array of indeces that should be condidered.
#   start       The first intdex that should be included (or start time).
#   ende        The last index, that should be included.
#   inYr        If this flag is true, then the passed times, which implies that
#                at least one of them is a float, is interpreted in years and not seconds.
#
def getTimeIndices(times, start, ende, inYr = False):

    # Handle ende is a none
    if(ende is None):
        ende = -1
    # end if: make ende into a number if it is none

    # Test if one of them is float
    if(util.isRealTimeSeries(start, ende) == True):
        #
        # We are in the float section

        Times = None        # This is a variable that we use as a place holder
        if(sTraits.isDataBase(times) or sTraits.isSimSeries(times)):
            Times = times.getTimes()
        elif(sTraits.isArray(times) ):
            Times = times
        else:
            raise ValueError("Unkown time type: {}".format(type(times)))

        if(util.isSorted(Times) == False):
            raise ValueError("The time array was not sorted.")
        # end checks:

        # Make loats out of the input
        startTime = float(start)
        endTime   = float(ende )

        # A negative value of end is interpreted as the end.
        # This is for consitency with the int interface
        if(endTime < 0.0):
            endTime = Times[-1] + 10.0
        #end if: resetting time

        # If they are not yet year make year out of them
        if(inYr == True):
            startTime = util.yearsToSec(startTime)
            endTime   = util.yearsToSec(endTime  )
        # end if: make year out of them

        if(startTime >= endTime):
            raise ValueError("The starttime is larger than the end time; start = {}, end = {} ({})".format(start, ende, endTime))
        # end if: test concistency

        # Shortcut for the case that all are selected
        if((startTime <= Times[0]) and (Times[-1] <= endTime)):
            # We have to return the full array
            return list(range(len(Times) ))
        # end if: short cut

        # This is the vector of indexes that should be considered
        toLoadIndexes = []

        # Going through the times and selecting the one we need
        for i in range(len(Times)):

            # Load the current time point, make year out of them
            ti = Times[i]

            # Comapre
            if((startTime <= ti) and (ti <= endTime)):
                toLoadIndexes.append(i)
            # end if: check if inside the range

            # Preemtive exit
            if(ti > endTime):
                break
            # end if: exit
        # end for(i):

        if(len(toLoadIndexes) <= 0):
            raise ValueError("The set of selected indexes was empty. N = {}; start = {}; ende = {}; endIdx = {}".format(N, start, ende, endTime))
        # end if: check

        # Return the list of indexes
        return toLoadIndexes
    # end if: handle time aware case


    #
    # If we are here then we expect to see integers
    # What is also important we do not need times
    N = None        # This is a helper

    if(sTraits.isStateSeries(times) or
       sTraits.isSimSeries  (times) or
       sTraits.isDataBase   (times) or
       sTraits.isArray      (times)   ):
        N = len(times)
    elif(sTraits.isInt      (times)   ):
        N = times
    else:
        raise ValueError("Can not extract indeces from {}".format(type(times)))
    # end checks:

    # Make ints out of the indexes
    startIdx = int(start)
    endIdx   = int(ende )

    if(startIdx < 0):
        raise ValueError("The start index was too small, it was {}".format(startIdx))
    # end if:

    # Handling our convention for the last index
    if(endIdx == -1):
        endIdx = N
    # end if

    # Now translate the slicing convention
    if(endIdx < 0):
        endIdx += 1
    #end if:

    # Make the indexes
    toLoadIndexes = list(range(N))
    toLoadIndexes = toLoadIndexes[startIdx:endIdx]

    if(len(toLoadIndexes) <= 0):
        raise ValueError("The set of selected indexes was empty. N = {}; start = {}; ende = {}; endIdx = {}".format(N, start, ende, endIdx))
    if(util.isSorted(toLoadIndexes, True) == False):
        raise ValueError("The set of selected indexes was not sorted, very strange. N = {}; start = {}; ende = {}; endIdx = {}".format(N, start, ende, endIdx))
    # end if: check

    # Return the selection
    return toLoadIndexes
# end def: getTimeIndices



# This function extracts a single state from the passed object.
#  This can be a series, a database or something different.
#  It also operates on NumPy array, in which case a copy is made.
#
#   src     The source what to plot. can be a dtatbase a series or a matrix.
#
#   idx     The index (time) that should be plotted, is needed in case of database or series.A
#   prop    The property that should be plotted only needed in case of database.
#   comp    The component that should be ploted, in case of tensor.
#   onMarker    If a marker property must be loaded, defaults to false.
#
def extractState(src, idx = None, prop = None, comp = None, onMarker = False):

    # src shall not be none
    if(src is None):
        raise ValueError("Passed None as source.")
    # end if:


    #
    # This is the variable where we store it
    eField = None

    #
    # Extract
    if(sTraits.isDataBase(src) == True):
        if(idx is None):
            raise ValueError("Passed None as index.")
        if(prop is None):
            raise ValueError("Passed None as property.")
        # end checks

        # This is already copied
        eField = src.getSimState(stateIdx = idx, prop = prop, onMarker = onMarker, comp = comp)
    # end if: src is a database

    elif(sTraits.isSimSeries(src) == True):
        if(idx is None):
            raise ValueError("Passed None as index.")
        # end checks

        # The returned field is already copied
        eField = src.getSimState(stateIdx = idx, prop = prop, onMarker = onMarker, comp = comp)
    # end if: src is a simulation series

    elif(sTraits.isStateSeries(src) == True):
        if(idx is None):
            raise ValueError("Passed a None as index.")
        # end check

        # This is an list, we now assume that it is a list of matrix
        # We reduce the handling to call owerself, we will make sure
        eField = extractState(src = src[idx], prop = None, idx = None, comp = None, onMarker = onMarker)
    #end if: src is list of matrix

    elif(sTraits.isSimState(src) == True):
        #   If we are here we have most likely a numpy array before us
        #   So we just coping it
        eField = copy.deepcopy(src)
    # end if: sim state

    else:
        raise ValueError("Does not know how to handle type {}".format(type(src)))
    # end else:


    #
    # Test the extracted field
    if(eField is None):
        raise ValueError("The field could not be extracted.")
    # end if: no exctract

    if(onMarker == True):
        if(len(pField.shape) != 1):
            raise ValueError("Requested a marker property, but got some stange one it hat a shape of {}".format(eField.shape))
        # end if: len
    #end if: is marker

    if(eField.size <= 0):
        raise ValueError("The size of the property was too small, the field was: {}".format(pField.size))
    # end if: size check

    #
    # Return the extracted field
    return eField
# end def(extractState):


#
# This function can be used to extract the summary statistics that are
#  included in a simulation series. This are the min series, max series
#  and so on. This function allows to extract them in a generic way.
#  To select which one should be extracted an enum is used.
#  Note that this function works with databases, simulation series
#  and state series. If a database is given, the function will construct
#  a series of it. Note that only the valus that are needed are
#  considered. Note that start and end are ignored for a simulation series
#
#   src         The source we want to extract from.
#   toExtr      What to extract.
#   prop        Property in case of a database.
#   comp        component in case of a database.
#   onMarker    Are we on markers, in case of database.
#   startTime   When to start.
#   endTime     When to end.
#
# Note that this function is not very efficient, and could be imporoved much.
#
def extractSummary(
        src, toExtr,

        startTime = 0, endTime = -1, inYr = False,

        prop = None, comp = None, onMarker = False
    ):

    if(isinstance(toExtr, toExtract) == False):
        raise ValueError("The extract falg is not a toExtract enum, instead it is {}".format(type(toExtr)))
    if(src is None):
        raise ValueError("The passed source is None.")
    # end if: check input

    if(sTraits.isDataBase(src)):
        # src is a database, so we make a series out of it and call ourself
        tmpSeries = src.getStateSeries(prop = prop, onMarker = onMarker,
                startTime = startTime, endTime = endTime, inYr = inYr,
                withTime = False)   # Just return the series
        return extractSummary(src = tmpSeries, toExtr = toExtr,
                startTime = 0, endTime = -1,    # Was considered during the construction of series
                prop = prop, comp = comp, onMarker = onMarker)
    # end if: handle database

    # Thsi bool indicates if the input is a simulation series or not
    srcIsSimSeries = True

    if(sTraits.isStateSeries(src)):
        srcIsSimSeries = False
    else:
        if(not sTraits.isSimSeries(src)):
            raise ValueError("Does not know how to handle {}".format(type(src)))
    # end if


    #
    # Now we are in the core sequence we will now extract everything into
    res = None

    if(toExtr == toExtract.Min):
        res = src.getMinSeries() if srcIsSimSeries else sRed.generalSeriesReduction(src, lambda x : np.amin(x) )

    elif(toExtr == toExtract.AbsMin):
        res = src.getAbsMinSeries() if srcIsSimSeries else sRed.generalSeriesReduction(src, lambda x : np.amin(np.abs(x)) )

    elif(toExtr == toExtract.Max):
        res = src.getMaxSeries() if srcIsSimSeries else sRed.generalSeriesReduction(src, lambda x : np.amax(x) )

    elif(toExtr == toExtract.AbsMax):
        res = src.getAbsMaxSeries() if srcIsSimSeries else sRed.generalSeriesReduction(src, lambda x : np.amax(np.abs(x)) )

    elif(toExtr == toExtract.Mean):
        res = src.getMeanSeries() if srcIsSimSeries else sRed.generalSeriesReduction(src, lambda x : np.mean(x) )

    elif(toExtr == toExtract.AbsMean):
        res = src.getAbsMeanSeries() if srcIsSimSeries else sRed.generalSeriesReduction(src, lambda x : np.mean(np.abs(x)) )

    elif(toExtr == toExtract.Std):
        res = src.getStdSeries() if srcIsSimSeries else sRed.generalSeriesReduction(src, lambda x : np.std(x) )

    else:
        raise ValueError("Not implemented reducer {}".format(toExtr.toStr()))
    # end if:

    N = len(res)
    if(N <= 0):
        raise ValueError("The length of the extracted state is too short.")

    # Get the index that we want to extract
    idxToReturn = getTimeIndices(times = src, start = startTime, ende = endTime, inYr = inYr)
    nIdx = len(idxToReturn)

    if(N == nIdx):
        return res
    elif(N < nIdx):
        raise ValueError("More index to extracts tan states. N = {}, nIdx = {}".format(N, nIdx))
    #end

    # Just return the extracted sub set
    return res[idxToReturn]
# end: extract summary



# This function is used to extract the plotting positions from the passed source.
#  If this source is a plain object, meaning a numpy array arange is used to
#  Generate the positions.
#  Note that the returned positions are inside a tuple, first X and then Y.
#  Also note that this function has plot pos applied to it.
#  In marker mode this si not the case and only the positions are in place.
#  Note that the index has only a meaning in marker mode.
#
#   src     The source what to plot. can be a dtatbase a series or a matrix.
#
#   idx     The index (time) that should be plotted, is needed in case of database or series.A
#   prop    The property that should be plotted only needed in case of database.
#   comp    The component that should be ploted, in case of tensor.
#   onMarker    Is marker mode enabled, defaults to False.
#   noMeshing   This flag can be used to deactivate meshing, defaults to false
#
def extractPlotPos(src, idx = None, prop = None, comp = None, onMarker = False, noMeshing = False):

    if(src is None):
        raise ValueError("passed None as source.")
    #end if:

    if((               onMarker == True ) and
       (sTraits.isDataBase(src) == False)    ):
        raise ValueError("Requested the positions for markers, but does not passed a database.")
    #end if: check


    # Thsi is teh object we return
    plotPos = None

    # Make the checks
    if(sTraits.isDataBase(src) == True):
        # Handling the Database

        if(onMarker == False):
            # Handle ordinary grid
            plotPos = src.getPlotPos(prop = prop, comp = comp, idx = idx, noMeshing = noMeshing)
        else:
            # Handle marker positions

            if(idx is None):
                raise ValueError("Passed None as index.")

            X = src.getSimState(stateIdx = idx, prop = PropIdx.PosX, onMarker = True)
            Y = src.getSimState(stateIdx = idx, prop = PropIdx.PosY, onMarker = True)
            plotPos = (X, Y)
        # end else: marker
    # end if: database

    elif(sTraits.isSimSeries(src) == True):
        # Handling the Simulation series
        plotPos = src.getPlotPos(prop = prop, comp = comp, idx = idx, noMeshing = noMeshing)
    # end elif: simseries

    elif(sTraits.isStateSeries(src) == True):
        useIdx = idx        # This is the index we use for accessing

        if(useIdx is None):     # Use a default value
            useIdx = 0
        # end check

        # We get a list, so we will apply the element fo that list
        # to this function, we will make sure, that everything is acording to planes.
        plotPos = extractPlotPos(src[idx], idx = None, prop = None, comp = None, onMarker = onMarker, noMeshing = noMeshing)
    # end if: passed a list

    elif(sTraits.isSimState(src) == True):
        # Handling the raw case

        if(len(src.shape) != 2):
            raise ValueError("Passed an unexpected array. Expected a two dimensional array, but got {}".format(src.shape))

        nY, nX = src.shape      # Load the size, note the ordnering that will be swapped

        X = np.arange(int(nX), dtype = np.dtype(float))     # Note the ordering that is swapped
        Y = np.arange(int(nY), dtype = np.dtype(float))

        if(noMeshing == True):
            plotPos = (X, Y)
        else:
            plotPos = np.meshgrid(X, Y)
    # end if: handle the case

    else:
        raise ValueError("Does now know how to handle {}".format(type(src)))
    # end else:

    if((plotPos is None  ) or
       (len(plotPos) != 2)   ):
        raise ValueError("Could not generate one.")
    # end if:

    return plotPos
# end def: extractPlotPos


# This function can be used to extract the time points of from an object.
#  Note that this function tries to query the indexes from the passed
#  object but will fall back to pure index if needed. It will honor,
#  the time restrictions. The inYr flag will cause the system to interprete
#  start and ende to be a time expressed in years instead of seconds.
#  Note that src will still be interpreted as time in seconds.
#
#   src     The source that is used
#   start   The start time
#   ende    The end time
#   inYr    start & ende will be interpret in years instead of seconds
#   fIdx    This falag forces index to return.
#   alsoIdx This flag instructs to return a function, first is the extracted time points and seocond is the index.
#   onlyIdx This flag instructs the functions to only turns the index.
#
def extractTimePoints(src, start = 0, ende = -1, inYr = False, fIdx = False, alsoIdx = False, onlyIdx = False):

    if(src is None):
        raise ValueError("Passed None as src.")
    if(onlyIdx and alsoIdx):
        raise ValueError("Can not return just the index and also the index.")
    #end if: input


    if(sTraits.isDataBase(src) or sTraits.isSimSeries(src)):
        times = src.getTimes()      # Load the times
        extrIdx = getTimeIndices(times = times, start = start, ende = ende, inYr = inYr)

        if(alsoIdx):
            return (times[extrIdx], extrIdx)
        elif(onlyIdx):
            return extrIdx

        return times[extrIdx]
    # end if: is Database

    elif(sTraits.isStateSeries(src) ):
        if(util.isRealTimeSeries(start = start, ende = ende) ):
            raise ValueError("Passed a state series and a real time series wihich is not supported.")
        if(not onlyIdx):
            raise ValueError("Only state series, so can only generate index.")
        # end: checks

        extrIdx = getTimeIndices(times = src, start = start, ende = ende, inYr = inYr)

        return extrIdx
    # end elif: simseries

    elif(sTraits.isArray(src) ):
        # Anaray is treated as the list of times
        extrIdx = getTimeIndices(times = src, start = start, ende = ende, inYr = inYr)

        if(alsoIdx):
            return (src[extrIdx], extrIdx)
        elif(onlyIdx):
            return extrIdx

        return src[extrIdx]
    # end elif: array passed directly

    else:
        raise ValueError("Does not know how to handle {}".format(type(src)))
    # end else:
# end def: extractTimePoints



# This function can be used to create a special property from a list of poperties.
#  This is basically a wrapper function around the constructor of the simulatioin
#  series, which some nice defaults. A lot can be extracted from a databse.
#  If a database is given then only the property (prop), the component (comp)
#  and onMarker are condsidered, all other arguments will be ignored.
#  It is also supported to pass a range of indexes to *this, in that case only
#  the selected subset is returned. Note that this subset is applied to everything.
#
#   src         This is the fictional series.
#
#   db          A database, if passed, will be used for extracting.
#                a property and maybe a comp is needed for the
#   prop        Propety used for determing the positions.
#   comp        Component used for determing the positions.
#
#   onMarker    Indicates if on markers, defaults to False.
#
#   Times       Times where the recoding is done, defaults to 0,1,2,..
#
#   idx         A subset that should be extracted.
#
#   xPos        x positions, not mesh grided, if not given, will be extracted.
#   yPos        y positions, not mesh grided, if not given, will be extracted.
#
def creatSpecialSimSeries(
        src,

        db = None,
        prop = None, comp = None,
        onMarker = False,

        Times = None,

        idx = None,

        xPos = None, yPos = None
    ):

    # Check some input arguments
    if((xPos is None) != (yPos is None)):
        raise ValueError("Inconsistent positions.")
    if(src is None):
        raise ValueError("Passed source is None.")
    if(onMarker != sTraits.onMarker(src)):
        raise ValueError("Marker selection and property are missmatched.")
    # End: checks


    # These are the variables that we are using for the actual construction.
    useTime = Times
    usePosX = xPos
    usePosY = yPos

    # Get the number of passed states
    nStates = int(len(src))


    # Descide if we should use the database or not
    if(db is not None):
        if(sTraits.isDataBase(db) == False):
            raise ValueError("Passed an object as database that is now an object.")
        # end if: test

        # We have a database to extract everything from it

        # We do not use a onMarkers because we do not need it
        usePosX, usePosY = extractPlotPos(src = db, prop = prop, comp = comp, onMarker = False, noMeshing = True)

        # Extract times if needed
        if(useTime is None):
            useTime = db.getTimes()
    #end if: database was passed

    else:
        # We do not habve a database, so we will make them our own

        if(Times is None):
            useTime = np.arange(nStates, dtype = np.dtype(float))
        # end if: times

        if(usePosX is None):
            usePosX, usePosX = extractPlotPos(src = src, idx = 0, prop = None, comp = None, onMarker = False, noMeshing = True)
        # end position extracting
    # end else: no database

    # Now we will determine which states we are extracting
    toExtractStateIdx = idx if (idx is not None) else list(range(nStates))

    # This lists are used for the subsets
    #  Wo do not need to deep copy, this will be done by the constructor
    toExtStates = []
    toExtTimes  = []

    # Load the shape of the first one
    fShape = src[0].shape

    #
    # This is the extraction loop
    for i in toExtractStateIdx:

        if(fShape != src[i].shape):
            raise ValueError("Fund a strange shape, teh shape was {}, but expectd {}.".format(src[i].shape, fShape))
        # end if: check shape

        toExtTimes.append(useTime[i])
        toExtStates.append(src[i])
    # end for(i): extraction loop


    #
    # Now build the simulation series
    specialSeries = SimSeries(
                        Times    = toExtTimes,
                        Values   = toExtStates,
                        onMarker = onMarker,
                        Prop     = PropIdx.SPECIAL,
                        Comp     = comp,
                        XNodal   = usePosX,
                        YNodal   = usePosY)
    return specialSeries
# end def: creatSpecialSimSeries


