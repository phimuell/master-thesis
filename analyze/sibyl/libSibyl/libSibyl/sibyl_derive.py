# This file is part of Sibyl.
#
# This file contains code that makes it easier to compare two
# databases with each other. it allows to derive new series.
#

# Normal
import h5py
import numpy as np
import math

# Ploting

# Sibyl
from .PropIdx import PropIdx
from .simSeries import SimSeries
from .dataBase import DataBase
import libSibyl.sibyl_util as util
import libSibyl.sibyl_traits as sTraits
import libSibyl.sibyl_seriesOps as sOps
import libSibyl.sibyl_seriesRed as sRed
import libSibyl.sibyl_extractor as sExtr


# Thsi function is able to generate a difference series from two databases.
#  This function basically automates the usage of the differenceSeries()
#  function, it will read out everything from the database and then
#  generate a new derived database. The positions are extracted from database one.
#
#   db1     The first DB
#   db2     The decond db.
#   prop    The property that should be examined.
#   onMarker    Should a marker property be used.
#   comp    The component.
#   Times   The time to use
def makeDiffSeries(db1, db2, prop,

        onMarker = False, comp = None,
        Times = None,
        startTime = 0.0, endTime = -1,
    ):

    if(sTraits.isDataBase(db1) == False):
        raise ValueError("db1 is not a database.")
    if(sTraits.isDataBase(db2) == False):
        raise ValueError("db2 is not a database.")
    # end if

    N1 = sTraits.nStates(db1)
    N2 = sTraits.nStates(db2)

    if(N1 != N2):
        raise ValueError("The two databases have a different amount of steps, db1 has {}, but bd2 has {}".format(N1, N2))
    # end if: check

    # Extract the series
    s1 = db1.getSimSeries(prop = prop, comp = comp, onMarker = onMarker,
                          startTime = startTime, endTime = endTime)
    s2 = db2.getSimSeries(prop = prop, comp = comp, onMarker = onMarker,
                          startTime = startTime, endTime = endTime)

    if(len(s1) != len(s2)):
        raise ValueError("The extracting of the two series resulted in different length list. len(s1) = {}; len(s2) = {}.".format(len(s1), len(s2)))
    # end if: check

    # Make a difference list
    diffStateList = sOps.differenceSeries(s1, s2)

    # Now create a new difference series
    diffSimSeries = sExtr.creatSpecialSimSeries(
                        src = diffStateList,
                        db = None,
                        prop = prop, comp = comp, onMarker = onMarker,
                        Times = None,
                        xPos = s1.getXPos(), yPos = s1.getYPos()
                    )

    return diffSimSeries
# end def: get Diff series



# Thsi function is similar to the one with the same name in the
#  sRed file, but operates on databases. For a description.
#  see the makeDiffferenceSeries() function.
#
#
def meanSquareError(db1, db2, prop,

        onMarker = False, comp = None,
        startTime = 0.0, endTime = -1,
    ):

    if(sTraits.isDataBase(db1) == False):
        raise ValueError("db1 is not a database.")
    if(sTraits.isDataBase(db2) == False):
        raise ValueError("db2 is not a database.")
    # end if

    N1 = sTraits.nStates(db1)
    N2 = sTraits.nStates(db2)

    if(N1 != N2):
        raise ValueError("The two databases have a different amount of steps, db1 has {}, but bd2 has {}".format(N1, N2))
    # end if: check

    # Extract the series
    s1 = db1.getSimSeries(prop = prop, comp = comp, onMarker = onMarker,
                          startTime = startTime, endTime = endTime)
    s2 = db2.getSimSeries(prop = prop, comp = comp, onMarker = onMarker,
                          startTime = startTime, endTime = endTime)

    if(len(s1) != len(s2)):
        raise ValueError("The extracting of the two series resulted in different length list. len(s1) = {}; len(s2) = {}.".format(len(s1), len(s2)))
    # end if: check

    return sRed.meanSquareError(s1 = s1, s2 = s2, sameLength = True)
# end def: mean square error


# This is a function that tries to find the location, timestep,
#  where the error starts to grow strongly. It will return an
#  index that points to the supplied error array.
#  The sSeq parameter allows to equilibrate the detection.
#  This is like the range where no sever change is detected.
#
#   err     The array that names the error.
#   sSec    Smooth section
#
def findDeviatingStep(
        err, sSec,
        sFac = 10.0
    ):

    if(sSec <= 0):
        raise ValueError("You have not passed a smoosthing section.")

    # Calculate the differences
    diff = np.diff(err)

    # test if we have to perform a smooth operation
    sSmooth = np.mean(np.abs(diff[0:int(sSec)]))

    # Now add a safty factor
    sSmoothSafe = sSmooth * sFac


    # Now we search for the position where to start
    for i in range(sSec, len(diff)):
        v = diff[i]
        if(abs(v) > sSmoothSafe):
            return i
        # end if: found position
    # end for(i): searching

    raise ValueError("Could not found the point where the increase happens.")
# end def: findDeviaticStep









