# This file is part of Sibyl.
#
# It is the utility file.
# It implements some very usefull functions.

# Normal
import h5py
import numpy as np
import math
import copy

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation



# This function transforms seconds into years.
#  This function does not return a string, but a scallar.
#  It operates on whatever is passed to it.
#  Note that the fmtUtil file has a function that has the same
#  name, but that one operates on strings.
def secToYear(sec):
    return (sec / (31556952.0))
# end def: secToYear


# This function translate a time duration that is expressed in years
#  into the same duration, but expressed in seconds
#
#   yr  Duration in years
#
def yearsToSec(yr):
    return (yr * 31556952.0)
# end def: yearToSec


# This function tests if text is a valid string.
#  A valid string is of type str. Its length is
#  matters, but can be disabled by a flag.
#
#   text
#
def isValidStr(text, noEmpty = True):
    if(text is None):
        return False

    if(isinstance(text, str)):
        if(noEmpty):
            return True if (len(text) > 0) else False
        return True
    # end if: is text

    # Nothing found that satisfy us
    return False
# end def: isValidStr


# This function tests if text is an empty string.
#
#   text    The object to analyize
#
def isEmptyString(text):
    if(isinstance(text, str) == False):
        raise ValueError("YOu have not passed a string, instead passed a {}".format(type(text)))
    # end if:

    return True if (len(text) == 0) else False
# end def: empty string


# This function performs concatenation of tuples or non tuples.
#
#   A       First object
#   B       Second Object
#
def catTuple(A, B):

    if((A is None) or (B is None)):
        raise ValueError("Passed a Non to the concatenation function.")
    # end if

    at = isinstance(A, tuple)
    bt = isinstance(B, tuple)

    R = None

    if(at ):
        R = A
    else:
        R = (A,)
    if(bt ):
        R = (*R, *B)
    else:
        R = (*R, B)

    return R
# end def: catTuple




# This function tests if the given sevuence is sorted.
#
#   seq     The sequaence to check
#   strict  Should the order be strinct
#
def isSorted(seq, strict = False):
    if(len(seq) <= 1):
        return True

    if(strict == True):
        return all(seq[i] <  seq[i+1] for i in range(len(seq)-1))
    else:
        return all(seq[i] <= seq[i+1] for i in range(len(seq)-1))
#end def: isSorted


# This function returns True if c1 and c2 are the same components.
#  The rule for this are quite complicated. So if both are None
#  they are the same. Also lower/upper does not count.
#  also "xy" is "yx", is symmetric is set to True.
#
#   c1      First component.
#   c2      Second component.
#   isSym   Bool to indicate if it is symmetrix
#
def isSameComp(c1, c2, isSym = True):

    if((c1 is None) and (c2 is None)):
        return True
    # end if: both are none

    if((c1 is None) != (c2 is None)):
        return False
    # end if: one is none and the other is not

    # Now both are not none now
    if((isinstance(c1, str) and isinstance(c2, str)) == False):
        raise ValueError("Not both are strings. c1 ~ {}; c2 ~ {}.".format(type(c1), type(c2)))
    if((len(c1) != 2) or (len(c2) != 2)):
        raise ValueError("The length of the strings is not two, len(c1) = {}, len(c2) = {}.".format(len(c1), len(c2)))
    # end if: checks

    if(c1.lower() == c2.lower()):
        return True
    elif(isSym == True):
        c1l = c1.lower()
        c2s = "{}{}".format(c1l[1], c1l[0])

        if(c2.lower() == c2s):
            return True
    # elif:

    return False
# end def: isSameComp


# This function is able to determine if the start, ende range, is considered
#  as a time or index range. It applies the rule that if at least one argument
#  is of type float it is considered as time points, if not and both are
#  ints it is considered as index rage.
#  Note this function ignores the values of the arguments, only types are
#  considered.
#
#   start       The first intdex that should be included (or start time).
#   ende        The last index, that should be included.
#
def isRealTimeSeries(start, ende):

    startValidType = isinstance(start, float) or isinstance(start, int)
    endeValidType  = True if (ende is None) else (isinstance(ende, float) or isinstance(ende, int))

    if(startValidType == False):
        raise ValueError("start has a non valid type of {}".format(type(start)) )
    if(endeValidType == False):
        raise ValueError("ende has a non valid type of {}".format(type(ende)) )

    if(isinstance(start, float) or isinstance(ende, float)):
        return True

    return False
# end def: isRealTimeSeries


# This function determines if the passed range is an index range or not.
def isIndexTimeSeries(start, ende):
    if(isRealTimeSeries(start = start, ende = ende)):
        return False
    return True
#end def: is Index time series


# This function is able to determine if the range is a full range.
#  meaning that all index are selected or not.
#  Note that the time index is only considered in certain cases.
#
#   times       The array of indeces that should be condidered.
#   start       The first intdex that should be included (or start time).
#   ende        The last index, that should be included.
#   inYr        If this flag is true, then the passed times, which implies that
#                at least one of them is a float, is interpreted in years and not seconds.
#
def isFullTimeRange(start, ende, inYr = False, times = None):

    # Note: not all cases are considered

    if(isRealTimeSeries(start, ende) == True):      # Thsi function will also make a validity test
        if((float(start) <= 0.0) and (isinstance(ende, None) or (float(ende) < 0.0))):
            return True
        return False
    # end if

    if(isinstance(start, int) and (start <= 0)):
        if(isinstance(ende, None) or (isinstance(ende, int) and (ende == -1))):
            return True
    #end if: first stage

    return False
# end def: isFullTimeRange


# This function is equiivalent with the getTimeIndices fucntion form the
#  extractor file. Because Python has problem with foprward declaring.
#  It surprieses me that there is a language, that has an even brainless
#  import/include system than C. This function only operates on
#  Arrays of times
#
#   Times       The array of indeces that should be condidered.
#   start       The first intdex that should be included (or start time).
#   ende        The last index, that should be included.
#   inYr        If this flag is true, then the passed times, which implies that
#                at least one of them is a float, is interpreted in years and not seconds.
#
def getTimeIndicesRANGE(times, start, ende, inYr = False):
    # Handle ende is a none
    if(ende is None):
        ende = -1
    # end if: make ende into a number if it is none

    # Test if one of them is float
    if(isRealTimeSeries(start, ende) == True):
        #
        # We are in the float section

        if(sibylInternal_isArray(times) == False):
            raise ValueError("This function onyl works with arrays, you used a {}".format(type(times)))
        # end if

        Times = times
        N = len(Times)

        if(isSorted(Times) == False):
            raise ValueError("The time array was not sorted.")
        # end checks:

        # Make loats out of the input
        startTime = float(start)
        endTime   = float(ende )

        # If they are not yet year make year out of them
        if(inYr == False):
            startTime = secToYear(startTime)
            endTime   = secToYear(endTime  )
        # end if: make year out of them

        # A negative value of end is interpreted as the end.
        # This is for consitency with the int interface
        if(endTime < 0.0):
            endTime = Times[-1] + 10.0
        #end if: resetting time

        if(startTime >= endTime):
            raise ValueError("The starttime is larger than the end time; start = {}, end = {} ({})".format(start, ende, endTime))
        # end if: test concistency

        # Shortcut for the case that all are selected
        if((startTime <= Times[0]) and (Times[-1] <= endTime)):
            # We have to return the full array
            return list(range(len(Times) ))
        # end if: short cut

        # This is the vector of indexes that should be considered
        toLoadIndexes = []

        # Going through the times and selecting the one we need
        for i in range(N):

            # Load the current time point, make year out of them
            ti = secToYear(Times[i])

            # Comapre
            if((startTime <= ti) and (ti <= endTime)):
                toLoadIndexes.append(i)
            # end if: check if inside the range

            # Preemtive exit
            if(ti > endTime):
                break
            # end if: exit
        # end for(i):

        if(len(toLoadIndexes) <= 0):
            raise ValueError("The set of selected indexes was empty. N = {}; start = {}; ende = {}; endIdx = {}".format(N, start, ende, endTime))
        # end if: check

        # Return the list of indexes
        return toLoadIndexes
    # end if: handle time aware case


    #
    # If we are here then we expect to see integers
    # What is also important we do not need times
    N = None        # This is a helper

    if(sibylInternal_isArray(times) ):
        N = len(times)
    elif(isinstance(times, int) ):
        N = times
    else:
        raise ValueError("Can not extract indeces from {}".format(type(times)))
    # end checks:

    # Make ints out of the indexes
    startIdx = int(start)
    endIdx   = int(ende )

    if(startIdx < 0):
        raise ValueError("The start index was too small, it was {}".format(startIdx))
    # end if:

    # Handling our convention for the last index
    if(endIdx == -1):
        endIdx = N
    # end if

    # Now translate the slicing convention
    if(endIdx < 0):
        endIdx += 1
    #end if:

    # Make the indexes
    toLoadIndexes = list(range(N))
    toLoadIndexes = toLoadIndexes[startIdx:endIdx]

    if(len(toLoadIndexes) <= 0):
        raise ValueError("The set of selected indexes was empty. N = {}; start = {}; ende = {}; endIdx = {}".format(N, start, ende, endIdx))
    if(isSorted(toLoadIndexes, True) == False):
        raise ValueError("The set of selected indexes was not sorted, very strange. N = {}; start = {}; ende = {}; endIdx = {}".format(N, start, ende, endIdx))
    # end if: check

    # Return the selection
    return toLoadIndexes
#end def: getTimeIndicesRANGE


# INternal Version, Python is so stupid
def sibylInternal_isArray(src):
    #
    #   N O T E
    # This function is a copy from the one that is implemented
    # in the traits header, modify that one and copies the
    # changes backs.

    if(isinstance(src, np.ndarray) == True):
        if(src.size <= 0):
            raise ValueError("The numpy array is empty.")
        if(len(src.shape) != 1):
            return False
        if(isinstance(src[0], float) == False):
            return False

        return True
    # end if: np array

    if(isinstance(src, list) == True):
        if(len(src) <= 0):
            raise ValueError("The Python array is empyt.")
        # For performance reasons, we only check the first entry, but this is not completly safe
        if(isinstance(src[0], float) == False):
            return False

        return True
    # end if: python list

    # nothing of the above
    return False

# end def: isArray



