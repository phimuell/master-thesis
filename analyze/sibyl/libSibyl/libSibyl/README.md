# Info
This folder contains the supporting code for Sibyl. The main bulk of the support
code is written in Python. However some part of the code was written in C. CTYPE
is used to make it accessible inside Python.

DOMINATOR.sh will compile the code before python is loaded.
The Makefile in this folder will compile the C code that is needed by SIBYL.
But it will only call the Makefile that is located in the C-Folder.
But there is an important remark, about debugging.
The Makefile in this folder, does DISABLE all debugging functionality of the code.

As a small side note, if you modify the code (Python or C), it will take effect only
after you have restarted the Compute-Kernel of the notebook and recompiled the code.


## Main Classes
Here we will give a small description about the classes that were written for SIBYL.
You can use help(INSTANCE) inside a notebook, and it will print out _some_ description
of INSTANCE. However we recommend reading the code.

### DATABASE
Is located in the file "dataBase.py". This is a wrapper around the HDF5 file.
It allows to access them in a unform way. Its main duty is to compose paths
inside the file. Also the most functions will operate directly on the data base.

### PropIdx
This is an enum which allows to denote properties, such as density.
It is mostly for convenience.


## Folder

## CLIBSIBYL
This is the part of sibyl that is written in C.
It is loaded with CTYPE.
It contains functions that have no NumPy equivalent and would need to be implemented in pure Python.
The Python wrapper are implemented in the "sibyl_cLibImpl.py" file.

In this folder a separate Makefile is located, which actually compile the code.


