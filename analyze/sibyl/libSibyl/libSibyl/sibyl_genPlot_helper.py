# This file is part of Sibly.
#
# This file contans functions to ease the development of plot functions.

# Normal
import h5py
import numpy as np
import math

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
from .PropIdx import PropIdx
from .simSeries import SimSeries
from .dataBase import DataBase
from libSibyl.extractCommands import toExtract
import libSibyl.sibyl_util as util
import libSibyl.sibyl_traits as sTraits
import libSibyl.sibyl_fmtUtil as fmtUtil
import libSibyl.sibyl_extractor as sExtr


#
# This function is used to extract what we need for plotting.
#  It will return a pair, first is an array that contains the
#  x values and second are the y values.
#
#   src         This is the standard source
#   src2        This is the second source.
#   prop        The property that should be extracted.
#   prop2       The property from teh second source, defaults to first property.
#   comp        The component of the first source.
#   comp2       The component from teh second source, defaults to the first one.
#   toExtr      What should be extracted.
#   toExtr2     What should be extracted from teh second source.
#
#   vsIdx       If true force the plotting of index.
#   startTime   The start time, that should be used.
#   endTime     The end time, that should be used.
#
def plotExtractor_helper(
        src,
        prop = None,
        comp = None,
        toExtr = None,
        onMarker = False,

        vsIdx = None,
        startTime = 0, endTime = -1, inYr = False
    ):

    if(src is None):
        raise ValueError("Passed None as source, can not extract from it.")
    # end if: test input


    # Test if it is an array, then we know what to to
    if(sTraits.isArray(src) == True):
        idxToExtract = getTimeIndices(times = src, start = startTime, ende = endTime, inYr = inYr)
        return (np.arange(len(src))[idxToExtract], src[idxToExtract] )
    # end if: is an array

    #
    # Now we know that we can simply use the extractor

    # Test if we have to force the the index
    forceIdx = False        # Indicates if times should be loaded at all.
    if(vsIdx is not None):
        if(vsIdx == True):
            forceIdx  = True
    # end if: force index

    # Load the recorded times and index, not only the selected index
    times, idxToExtract = sExtr.extractTimePoints(src, start = 0, ende = -1, inYr = False, fIdx = False, alsoIdx = True)

    # Now we load the full data that we are interested in
    fullData = sExtr.extractSummary(src = src, toExtr = toExtr,
                        startTime = 0, endTime = -1, inYr = inYr,
                        prop = prop, comp = comp, onMarker = onMarker)

    # Reduce the values to the ones that are needed
    resData  = fullData[idxToExtract]

    # Honor force index flag
    if(forceIdx):
        return (idxToExtract, resData)
    # end if:

    # Return them
    return (times, resData)
# end def: plotExtractor_helper


