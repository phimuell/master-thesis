# This file is part of Sibyl.
#
# It contains the code that is needed to generate animations.

# Normal
import h5py
import numpy as np
import math

# Ploting
from   matplotlib import pyplot    as plt
import matplotlib                  as mpl
from   matplotlib import animation

# Sibyl
from .PropIdx   import PropIdx
from .simSeries import SimSeries
from .dataBase  import DataBase
import libSibyl.sibyl_util         as util
import libSibyl.sibyl_traits       as sTraits
import libSibyl.sibyl_fmtUtil      as fmtUtil
import libSibyl.sibyl_extractor    as sExtr



# This function generates the animation.
#  An animation object is retunred that has to be
#  evaluated. This function operates on databases,
#  thus it is possible to also enable plotting of the
#  velocity vectors.
#
#  prop         The property that should be plotted.
#  comp         The component for tewnsor valued functions.
#  fixCRange    The collor range is fix or can change during the animation
#  cRange       Allows to pass the collor range, must be a tuple of two
#  xLab         The label for the xAxis; default x in m
#  yLab         The label for the yAxis; default y in m
#  cMap         The color map that is used; default jet
#  noTitle      If set to true, no titlke will be drawn
#
#  frames       This is a way of selecting a subset of the domain.
#                this can be a number then it is equivalent to ::frames or it
#                it can be a list of indexes. in that case only they will be selected.
#                Note that they are applied on the result of the extracted indexes.
#
#  velArrow     Acivate the Plotting of velocity arrows.
#  xStep        Slicing step for plotting the velocity arrows in x direction.
#  yStep        Slicing step for plotting the velocity arrows in y direction.
#
#  idx          Select some markers that should be plotted.
#  mColor       This is the color that is used for the marker plotting, see SCATTER
#
#  startFrame   First frame in series
#  endFrame     Index of the past the end frame; -1 for last one in series
def genAnime(db, prop,

        comp = None,

        fixCRange = True,
        cMap = 'jet',
        cRange = None,

        useKM = True,           # Should the position be expressed in Kilometers
        xLab = None,
        yLab = None,
        noTitle = False,

        frames = None,

        velArrow = True,
        xStep = 5,
        yStep = 5,

        idx = None,
        mColor = 'k',

        startFrame = 0,
        endFrame   = -1,
        inYr       = False,

        doDebug = False
    ):

    if(sTraits.isDataBase(db) ):
        # Nothing to do
        None
    # end if: is DB

    elif(sTraits.isSimSeries(db) ):
        # It is a simulation series, so we have to deactivate everything

        if(velArrow == True):
            raise ValueError("Velocity vectors are only supported if a database is supplied.")
        # end if: velArrow

        if(idx is not None):
            raise ValueError("Activated the plotting of markers during the simulation, but no database was passed.")
        # end if: no idx

    # end if: is sim series

    else:
        raise ValueError("Does not know how to make an animation from {}".format(type(db)))
    # end else:


    if(fixCRange == False):
        raise ValueError("Currently no dynamic color bars are possible.")
    # end if:

    if(velArrow and (xStep <= 0 or yStep <= 0) ):
        raise ValueError("The velocity arrow steps are too small, you passed {} and {}".format(xStep, yStep))
    # end if: check arropw steps

    # Extract the time
    timesToAni, sIdxToAni = sExtr.extractTimePoints(src = db.getTimes(), start = startFrame, ende = endFrame, inYr = inYr, alsoIdx = True)


    #
    # Loading the properties
    # We will load the property and the velocities into series
    # This will make handling easier

    # Load the series of the property
    pSeries = db.getSimSeries(prop = prop,
                onMarker = False, comp = comp,
                startTime = startFrame, endTime = endFrame, inYr = inYr,
                withInitialState = False)

    # Defines the variables for storing the velocities
    vxSeries = None
    vySeries = None

    # Test if we have to load the velocity series
    if(velArrow == True):

        # Load the velocities
        vxSeries = db.getSimSeries(prop = PropIdx.VelX,
                onMarker = False, comp = None,
                startTime = startFrame, endTime = endFrame, inYr = inYr,
                withInitialState = False)

        vySeries = db.getSimSeries(prop = PropIdx.VelY,
                onMarker = False, comp = None,
                startTime = startFrame, endTime = endFrame, inYr = inYr,
                withInitialState = False)
    # end if: load velocities


    # Tis are the marker positions
    markerPosX = None
    markerPosY = None
    if(idx is not None):

        # Make the index uniqes
        uniqueIdx = list(np.unique(idx))

        # Load the marker positions
        markerPosX = db.getSimSeries(prop = PropIdx.PosX,
                        onMarker = True, comp = None,
                        startTime = startFrame, endTime = endFrame, inYr = inYr,
                        withInitialState = False)
        markerPosY = db.getSimSeries(prop = PropIdx.PosY,
                        onMarker = True, comp = None,
                        startTime = startFrame, endTime = endFrame, inYr = inYr,
                        withInitialState = False)
    # end if: load the positions


    # Now we load the number of series we actually have to plot
    nStates = pSeries.nStates()

    # This is the old number of states
    #  is fix
    oldNStates = pSeries.nStates()

    # Make a list of indexes
    statesToPlot = list(range(nStates))

    # Test How we should select the indexes
    if(sTraits.isInt(frames) ):
        statesToPlot = statesToPlot[slice(0, statesToPlot[-1], frames)]
    elif(sTraits.isIndexSeries(frames) ):
        statesToPlot = statesToPlot[frames]
    elif(sTraits.isNone(frames) ):
        # No nothing
        None
    else:
        raise ValueError("Does not know how tho handle the frame argument, it was a {}".format(type(frames)))
    # end: selection

    # Update the conter
    nStates = len(statesToPlot)

    if(doDebug == True):
        print("Frames to print: {}".format(statesToPlot))

    # Get the ranges
    #  TODO: In frame selcting case, there could be lower bounds
    if(sTraits.isNone(cRange) == True):
        # cRrange is not given
        if(fixCRange == True):
            maxVal = pSeries.getMax(None) * np.ones(oldNStates)     # Must be old, because therwise out pf bound
            minVal = pSeries.getMin(None) * np.ones(oldNStates)
        else:
            maxVal = pSeries.getMaxSeries()
            minVal = pSeries.getMinSeries()
        # end else:
    # end if: cRange not given

    else:
        # cRange is given
        if(sTraits.isTuple(cRange, n = 2) == False):
            raise ValueError("Illegal cRange attribute passed, passed type {}".format(type(cRange)))
        # end if: check

        maxVal = cRange[1] * np.ones(oldNStates)     # Must be old, because therwise out pf bound
        minVal = cRange[0] * np.ones(oldNStates)
    # end else: cRange given


    # Axe labels
    if(xLab is None):
        xLab = 'x in [km]' if useKM else 'x in [m]'
    # end if: xLab
    if(yLab is None):
        yLab = 'y in [km]' if useKM else 'y in [m]'
    # end if: yLab


    # Get the figure
    fig, ax = plt.subplots()

    # Here we will save everything
    Frames = []

    # Plot the first picture, this will also set all the values
    # Plot settings that are needed.

    if((xLab is not None) and (len(xLab) > 0)):
        plt.xlabel(xLab)
    if((yLab is not None) and (len(yLab) > 0)):
        plt.ylabel(yLab)
    # end if: labelds

    # Thsi must be here, in order that colorbar does not complain
    # It will not enter the animation
    plt.pcolormesh(np.zeros((0,0)), cmap = cMap, vmin = minVal[0], vmax = maxVal[0])

    # Some settings
    plt.gca().invert_yaxis()
    plt.gca().set_aspect("equal", adjustable = "box")
    plt.colorbar()

    # Generate all the figures that are needed.
    #TODO: PARALELIZE THIS LOOP
    #    EXTRACTING WAS DONE, so we must start with one and so on
    for i in statesToPlot:

        # Get the plot points
        X, Y = sExtr.extractPlotPos(src = db, prop = prop, comp = comp, idx = i)

        # Check if we have to make Kilometers out of them
        if(useKM == True):
            X = X / 1000.0
            Y = Y / 1000.0
        #end if: Kilometers


        if(doDebug == True):
            print("State {} of {}; old states {}".format(i, nStates, oldNStates))

        # Plot it important we must store a tuple in the list.
        Frame_i = plt.pcolormesh(X, Y, pSeries[i],
                animated = True,
                #antialiased = True,
                cmap = cMap, vmin = minVal[i], vmax = maxVal[i])

        # This is the tuple that we will add to the list
        thisFrame = tuple([Frame_i])

        # Plotting the velocities if requested
        if(velArrow == True):

            # Load teh velocities into temporaries
            vx = vxSeries[i]
            vy = vySeries[i]

            # Load the positions of the velocity fields
            #XPosVelX, YPosVelX = vxSeries.getPlotPos(prop = vxSeries.getProp(), comp = None, idx = i, noMeshing = False)
            #XPosVelY, YPosVelY = vySeries.getPlotPos(prop = vySeries.getProp(), comp = None, idx = i, noMeshing = False)
            # Check if we have to make Kilometers out of them
            #if(useKM == True):
            #    XPosVelX = XPosVelX / 1000.0;
            #    YPosVelX = YPosVelX / 1000.0;
            #    XPosVelY = XPosVelY / 1000.0;
            #    YPosVelY = YPosVelY / 1000.0;
            ##end if: Kilometers

            # NOTE:
            #   There is a small inconsistency in the plotting. Because the velocities are defined
            #   at different grid points, each component is at a different location.
            #   Also the point where we plot is at a different location. This is not so nice
            #   but this will not be fixed.
            #
            #  For minus see the db plot function
            velFrame_i = plt.quiver(X[::yStep, ::xStep], Y[::yStep, ::xStep], vx[::yStep, ::xStep], -vy[::yStep, ::xStep] )

            # We must extend the tuple
            thisFrame = (*thisFrame, velFrame_i)
        #end if: plot vel arrows

        # Plotting markers
        if(idx is not None):

            # Load the positions
            mX = markerPosX[i][uniqueIdx]
            mY = markerPosY[i][uniqueIdx]

            if(useKM == True):
                mX = mX / 1000.0
                mY = mY / 1000.0
            # end if:

            # Plot the marker
            mPos = plt.scatter(mX, mY, c = mColor)

            # Add it to the frame tuple
            thisFrame = (*thisFrame, mPos)
        # end if: plot markers


        # Test if we have to update the title
        # See: https://stackoverflow.com/questions/47421486/matplotlib-artist-animation-title-or-text-not-changing/47421950#47421950
        if(noTitle == False):
            realTxt = "Evolution of {}; time = {} (idx = {})".format(prop.toStr(), fmtUtil.secToYear(pSeries.getTime(i)), (startFrame + i))
            title = plt.text(0.5, 1.01, realTxt, horizontalalignment='center', verticalalignment='bottom', transform=ax.transAxes)

            # Extend the tuple with the title
            thisFrame = (*thisFrame, title)
        # end if: no title


        #
        # Add the tuple, tit must be one to the list of frames
        Frames.append(thisFrame)
    # end for(i): draw all ranges

    if(doDebug == True):
        print("Loop ended")

    # Generate the animation
    anim = animation.ArtistAnimation(
        fig,
        Frames,
        interval = int(1000.0 / 20),
        blit     = False,
        repeat   = False
    )
    plt.close()

    if(doDebug == True):
        print("Anime ended")

    return anim
# end: genAnime

