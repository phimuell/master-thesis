# This file is part of SIBYL2.0
#
# This file contains an ENUM, that can be used to extract
#  several state information from a simulation series.
#  This enum allows to specifies the summary statistic
#  That is needed.

# Import the enum
from enum import Enum

# This is a class that is used to automumber the enum constants
# This is from "https://docs.python.org/3.4/library/enum.html#autonumber"
class AutoNumberEnum2(Enum):
    def __new__(cls):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj
    # End: __new__
# End class(AutoNumberEnum2):


#
# This is the class for naming the summaries
class toExtract(AutoNumberEnum2):

    #
    # These are the different names
    Min         = ()
    AbsMin      = ()

    Max         = ()
    AbsMax      = ()

    Mean        = ()
    AbsMean     = ()

    Std         = ()


    #
    # These are the functions

    # This transforms *self into a name
    def toStr(self):
        return self.name
    # end: toStr


    # This function allows the conversion of *self to string automatically
    def __str__(self):
        return self.toStr()
    # end: __str__

# End class(toExtract)


