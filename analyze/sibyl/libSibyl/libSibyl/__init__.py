# libSibyl package

# Needs for the setup
from matplotlib import pyplot as plt
import matplotlib as mpl

# This functions sets up some variables, they are recomended
# By Simon Frasch (fraschs@student.ethz.ch).
def sibyl_setUp():
    mpl.style.use('seaborn')
    plt.rcParams["figure.figsize"] = (10,6)

    mpl.rc("savefig", dpi=300)
    mpl.rcParams['figure.dpi'] = 300
# end Sibyl_setUp


