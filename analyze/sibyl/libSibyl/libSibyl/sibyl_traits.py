# This file is part of Sibyl.
#
# This is the traits file. It contains some functions that
#  eases the identification of types.

# Normal
import numpy as np

# Ploting

# Sibyl
from .PropIdx import PropIdx
from .simSeries import SimSeries
from libSibyl.gTypeEnum import eGridType
import libSibyl.dataBase as dataBase

# This function checks if src is a database
#
#   src     The object that should be tested
#
def isDataBase(src):

    if(src is None):
        raise ValueError("Passed None.")

    if(isinstance(src, dataBase.DataBase) == True):
        return True

    return False
# end def: isDatabase


# This function returns True if src is a
#  simulationseries.
#
#   src     The object to test
#
def isSimSeries(src):

    if(src is None):
        raise ValueError("Passed None.")

    if(isinstance(src, SimSeries) == True):
        return True

    return False
# end def: isSimSeries


# This function returns True if src is a state.
#  A State is a numpy array.
#
#   src     Object to test
#
def isSimState(src):

    if(src is None):
        raise ValueError("Passed None.")

    if(isinstance(src, np.ndarray) == True):
        if(src.size <= 0):
            return False
        # end if: the state is empty

        return True
    # end if: it is an array

    return False
# end def: isSimState


# This function returns True if src is a state series.
#  A state series is a non empty list of simulation states.
#
#   src     Object to test
#
def isStateSeries(src):

    if(src is None):
        raise ValueError("Passed None.")

    if(isinstance(src, list) == False):
        return False

    if(len(src) <= 0):
        return False

    if(isSimState(src[0]) == False):
        return False

    return True
# end def: isStateSeries


# This function returns true if src is an array of floats.
#  Also note that empty arrays, objects for which len()
#  returns zero, will generate an error.
#  Valid objects are python lists, of floats. For performance
#  only the first element is checked to be of type float.
#  Also numpy arrays are condsidered. Note that they must
#  Have a shape of length one.
#
#   src     Object to test
#
def isArray(src):
    #
    #   N O T E
    # For several reasons, the util header needs this functionality
    # too, but can not include this file. Fo after an update to this
    # function copy the body to the util header.

    if(isinstance(src, np.ndarray) == True):
        if(src.size <= 0):
            raise ValueError("The numpy array is empty.")
        if(len(src.shape) != 1):
            return False
        if(isinstance(src[0], float) == False):
            return False

        return True
    # end if: np array

    if(isinstance(src, list) == True):
        if(len(src) <= 0):
            raise ValueError("The Python array is empyt.")
        # For performance reasons, we only check the first entry, but this is not completly safe
        if(isinstance(src[0], float) == False):
            return False

        return True
    # end if: python list

    # nothing of the above
    return False
# end def: isArray


# This function returns true if src is a series
#  object. A series object is an object that has
#  a operator[] which returns a state.
#
#   src     Object to test
#
def isSeries(src):

    if(src is None):
        raise ValueError("Passed None.")

    if(isSimSeries(src) or isStateSeries(src)):
        return True

    return False
# end def: isSeries


# This function tests if src is an integer.
#  This is basicaly a way for avoiding writing
#  isinstance(src, int), which is ugly.
#
#   src     The object to test
#
def isInt(src):
    if(isinstance(src, int     ) or
       isinstance(src, np.int64) or
       isinstance(src, np.int32)   ):
        return True
    return False
# end def: isInt


# This function returns true if src is considered a
#  float, this is eaither the python float or the
#  numpy float
#
#   src     The object to test.
#
def isFloat(src):
    if(isinstance(src, float     ) or
       isinstance(src, np.float64) or
       isinstance(src, np.float_ ) or
       isinstance(src, np.float32)   ):
        return True
    return False
# end def: isFloat


# This function returnd True if src is a tuple.
#  The type is ignored, whith the opttional parameter
#  the supposed length can be specified
#
#   src     The object to test.
#   n       The supposed length
#
def isTuple(src, n = None):

    # Test if it is the correct type
    if(isinstance(src, tuple) == False):
        return False
    # end if:

    if(isInt(n) ):
        return True if (len(src) == n) else False
    # end if: length test

    # If no length test is requiered
    return True
# end def: isTuple


# This function returns true if src is a list
#
#   src     The object to test
#
def isList(src):
    if(isinstance(src, list) ):
        return True
    return False
# end def: isList



# This function tests if src is None.
#  This is done with the "is" keyword
#
#   src     The object to test
#
def isNone(src):
    if(src is None):
        return True
    return False
# end def:


# This function returns True if src is an grid type enum.
#
#   src     The object to test
#
def isGridTypeEnum(src):
    if(isinstance(src, eGridType) ):
        return True
    return False
# end def: isGridTypeEnim


# Tis function tests if src is an index series.
#  An index series is a list of integers.
#  Also slicing objects are supported.
#
#   src     The object to test
#
def isIndexSeries(src):

    # If a slice is passed is it also okay
    if(isinstance(src, slice) ):
        return True
    # end if: isSlice

    if(isinstance(src, list) == True):
        if(len(src) == 0):
            raise ValueError("Passewd an empty list.")

        # For performance we only test the first one
        if(isInt(src[0]) == False):
            return False

        # If we are here, the list meet our expactaions
        return True
    # end if: is not even a list

    if(isinstance(src, np.ndarray) == True):
        if(src.size <= 0):
            raise ValueError("The numpy array is empty.")
        if(len(src.shape) != 1):
            return False
        if(isInt(src[0]) ):
            return True

        return False
    # end if: np array

    # If we are here, no indication of an index array was found.
    return False
# end def: isIndexSeries


# This function returns true if prop is a marker property.
#  Note that prop must be a simulation state for that.
#
#   prop    The object to be juged.
#
def isMarkerProp(prop):

    if(prop is None):
        raise ValueError("Passed None.")

    # Test if it is a simulation state
    if(isSimState(prop) == False):
        return False

    nDims = len(prop.shape)     # Get the numbers of dimensions

    if(prop.size <= 0):
        raise ValueError("The size is not correct.")

    if(nDims == 1):
        return True
    elif(nDims == 2):
        return False
    # end:

    raise ValueError("Got a property with shape {}".format(prop.shape))
# end def: isMarkerProp


# This function tests if src is a simulation state object that
#  contains marker information. In the case of a database or
#  simulation series, this means on marker returns True and
#  on a state series, that its states are on markers.
#
#   src     Object to test.
#
def onMarker(src):

    if(src is None):
        raise ValueError("Passed None.")

    if(isSimSeries(src) or
       isDataBase( src)   ):
        return src.onMarker()

    if(isStateSeries(src) == True):
        return isMarkerProp(src[0])

    if(isSimState(src) == True):
        return isMarkerProp(src)

    raise ValueError("Passed an unkonwn instance to the onMarker function of type {}".format(type(src)))
#end def: onMarker


# This function returns the number of markers in src.
#  For this to work src must be something that holds markers.
#
#   src     Object to test
#
def nMarkers(src):

    if(src is None):
        raise ValueError("Passed None.")

    if(isDataBase(src) == True):
        return src.nMarkers()

    if(isSimSeries(src) == True):
        if(src.onMarker() == False):
            raise ValueError("Tried to call nMarkers() on a simulation series that is not a marker.")

        return src.nMarkers()
    # end if: sim series

    if(isSimState(src) == True):
        if(onMarker(src) == False):
            raise ValueError("The passed simulation state is not on a marker.")
        return src.size
    # end if: sim state

    if(isStateSeries(src) == True):
        return nMarkers(src[0])

    raise ValueError("Encountered an object that is unknown, its type was {}".format(type(src)))
# end def: nMarkers


# This function returns the number of states in the series src.
#  This function works on all knwon object, with the exception of
#  the raw simulation state.
#
#   src     Object from which to interfere
#
def nStates(src):

    if(src is None):
        raise ValueError("Passed None.")

    if(isSimSeries(src)    or
       isStateSeries(src)    ):
        return len(src)

    if(isDataBase(src) ):
        return src.nStates()

    if(isSimState(src) ):
        raise ValueError("Can not interfere the number of states form a single state.")

    raise ValueError("Passed an unknown object to the function, its type was {}".format(type(src)))
# end def: nStates


# This function checks if the series s is valid.
#  A valid series has a length greater than one.
#  And all its substates have the same shape
#  And are finite. Note that databases are valid
#  by default.
#
#   s       The series to test.
#   noVal   No value check, meaning no infinite values are detected.
#   doThrow Instead of returning false, throw an error.
#
def isValidSeries(
        s,

        noValCheck = False,
        doThrow    = False
    ):

    if(isSimState(s) == True):
        if(s.size <= 0):
            if(doThrow == True):
                raise ValueError("Passes a single state ogf the check function, and detected a wrong size.")
            return False
        else:
            if(noValCheck == False):
                if(np.all(np.isfinite(s), axis = None) == False):
                    if(doThrow == True):
                        raise ValueError("Passed a single state that contains NAN.")
                    return False
                # end if: found NAN
            # end if: need to check

            return True
    # end if: single simulation state


    if(isDataBase(s) == True):
        return True
    # end if: handle the case of a database


    if(isSeries(s) == False):
        raise ValueError("You have not passed an object that looks like a series. Passed object has type {}".format(type(s)))
    # end if: no series

    if(len(s) <= 0):
        raise ValueError("You have passed an empty series.")
    # end if: empyt series

    # This is the first shape
    fShape = s[0].shape

    N = len(s)  # Number of states


    # Going through all the states
    for i in range(N):

        if(s[i].shape != fShape):
            if(doThrow == True):
                raise ValueError("Found a state, index {}, with a missmatching shape. It had shape {}, but expected {}.".format(i, s[i].shape, fShape))
            return False
        # end if: not the correct shape

        if(noValCheck == False):
            if(np.all(np.isfinite(s[i]), axis = None) == False):
                if(doThrow == True):
                    raise ValueError("Found a non finite value in state {}.".format(i))
                return False
            # end if: there was an error
        # end if: no value checking
    # end for(i):

    return True
# end def: isValid series


