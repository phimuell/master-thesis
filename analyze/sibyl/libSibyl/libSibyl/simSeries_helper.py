# This file is part of SIBYL.
#
# Again I must say that the include system of C is way better
# and way more advanced than the import system of Python.
#

# Normal
import numpy as np



# This function is a copy of the function of the same
#  name form the traits header.
#
#   src     Object to test
#
def isArray(src):
    #
    #   N O T E
    # For several reasons, the util header needs this functionality
    # too, but can not include this file. Fo after an update to this
    # function copy the body to the util header.

    if(isinstance(src, np.ndarray) == True):
        if(src.size <= 0):
            raise ValueError("The numpy array is empty.")
        if(len(src.shape) != 1):
            return False
        if(isinstance(src[0], float) == False):
            return False

        return True
    # end if: np array

    if(isinstance(src, list) == True):
        if(len(src) <= 0):
            raise ValueError("The Python array is empyt.")
        # For performance reasons, we only check the first entry, but this is not completly safe
        if(isinstance(src[0], float) == False):
            return False

        return True
    # end if: python list

    # nothing of the above
    return False
# end def: isArray


# This function returns true if src is a list
#
#   src     The object to test
#
def isList(src):
    if(isinstance(src, list) ):
        return True
    return False
# end def: isList


