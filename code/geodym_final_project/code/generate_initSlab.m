function [tempMarker, cpMarker, rhoMarker, etaMarker, kMarker, radHeatingMarker, alphaMarker, betaMarker, strengthMarker, mTypeMarker] = generate_initSlab(xMarker, yMarker)
%{
	This function is able to generate the initial conditions for the slab.
	It does not respect the thermal insulation.
	It sets all the parameter to their initial conditions.

	The size of the computational domain is the same as in the setup.

	The returned quantities, on the node, are:
		> tempMarker		Temperature of the marker
		> cpMarker		The heat capacity at constant presure of a marker
		> rhoMarker 		The density of the marker
		> etaMarker		Viscosity of the marker
		> kMarker 		Thermal conductivity of the marker
		> radHeatMarker		Radioactive heating capacity of the marker
					  Is assumed to be zero
		> alphaMarker 		Thermal expansion of the marker
		> betaMarker 		Compressibility of the marker (Inverse of bulk modulus)
		> strengthMarker 	The yield strength of the material
		> mTypeMarker 		The material type of the marker, given by the globals

	The input variables are selfexplaining.
	But it is important, that the input is in SI units, meaning meters.
	BUT this function calculates the stuff in kilometers.

%}

% Import the global material constants
global GLOB_TYPE_AIR GLOB_TYPE_PLATE GLOB_TYPE_MANTLE GLOB_TYPE_NECKING GLOB_TYPE_INVALID GLOB_TYPE_SLAB;

% %%%%%%%%%%%%%%%%%%
Nm = numel(xMarker);		% Number of nodes

cc = 0;


% Allocating the arrays
tempMarker 		= zeros(size(xMarker));
cpMarker 		= zeros(size(xMarker));
rhoMarker 		= zeros(size(xMarker));
etaMarker 		= zeros(size(xMarker));
kMarker 		= zeros(size(xMarker));
radHeatingMarker	= zeros(size(xMarker));	% Is assumed to be zero
alphaMarker 		= zeros(size(xMarker));
betaMarker 		= zeros(size(xMarker));
strengthMarker 		= zeros(size(xMarker));
mTypeMarker 		= ones (size(xMarker)) * GLOB_TYPE_INVALID;

% This are functions that are used to caculate the boundings of the thorn.
% The naming is from the drawing
t_1 = @(X)(100 + (X - 220));
t_2 = @(X)(50  + (X - 230));
t_3 = @(X)(100 - 5 * (X - 220));
t_4 = @(X)(250);


% This are functions to determine the temperature in the thorn
% See also "https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line"
h = 60 / sqrt(2);
phi_1 = 430; phi_2 = 250;
gamma_1 = 230; gamma_2 = 50;
helperD = sqrt( ((phi_1 - gamma_1)^2) + (phi_2 - gamma_2)^2 );
d_x = @(x0, y0)( abs( (phi_2 - gamma_2) * x0 - (phi_1 - gamma_1) * y0 + phi_1 * gamma_2 - phi_2 * gamma_1 ) / helperD  );



% Iterate through all markers to set their values.
for m = 1:1:Nm

	% Get the coordinates
	% NOTICE: They are in killometers
	x = xMarker(m) / 1000;
	y = yMarker(m) / 1000;

	% Test if it is air
	if(y <= 50)
		% It is air
		mTypeMarker(m) 		= GLOB_TYPE_AIR;

		% Setting quantities
		tempMarker(m) 		= 273;
		cpMarker(m) 		= 3300000;
		etaMarker(m) 		= 1e+18;
		rhoMarker(m) 		= 1;
		%strengthMarker(m) 	= 1e+8;
		% Uncomentedhoping matlab is smart enough, what will not be the case
		kMarker(m) 		= 3000; 	% Is fix and does not change too!
							% Is also set in the calculate_kValue
		% Other quantities are not relevant for the air so we set them to zero
		alphaMarker(m) 		= 0;
		betaMarker(m) 		= 0;
		radHeatingMarker(m) 	= 0;

		continue;
	end % if: air

	%
	% This part is only entered if we are not in the air
	%
	% Continue is important here
	%

	% If we are here, we know, that we have stone in from of us, so we set the all stone quantities
	alphaMarker(m) 		= 3e-5;
	betaMarker(m) 		= 1e-11;
	cpMarker(m)		= 1000;
	radHeatingMarker(m) 	= 0;


	% Test if plate, here the neking area is excluded, but we compose this thing in different steps
	% In fact it is overwritten.
	if(y >= 50 && y <= 100)
		% It could be the plate woithout slap
		% Notice that a part of the thorn coulde also be here, but it is overwritten
		mTypeMarker(m) 		= GLOB_TYPE_PLATE;

		% Setting the quantities
		rhoMarker(m) 		= 3400;
		strengthMarker(m) 	= 1e+8;
		etaMarker(m) 		= 1e+23;

		tempMarker(m) 		= 273 + (1573 - 273) * (y - 50) / 50;

		% endif: plate without slap

	else % We assume that we are in the mantle
		% It coulde be mantle or thorn, we now handle the mantle case.
		mTypeMarker(m) 		= GLOB_TYPE_MANTLE;

		% Setting the
		etaMarker(m) 		= 1e+20;
		rhoMarker(m) 		= 3250;
		strengthMarker(m) 	= 5e+7;
		tempMarker(m) 		= 1573;
	end % if: upper plate or mantle


	% We first test if we are in a box around the thorn (slab)
	if((x >= 220) && (x <= 430) && (y >= 50) && (y <= 250) )

		% We are in the bounding box


		% Now we test if we are in the thorn.
		% if we are there we coulde also be in the necking area
		% See drawing for explanation about the naming
		% We now calculate the respective points
		y_1 = t_1(x);
		y_2 = t_2(x);
		y_3 = t_3(x);
		y_4 = t_4(x);



		% Remember the orientation
		if( (y >= y_3) && (y >= y_2) && (y <= y_1) && (y <= y_4) );
			% We are in the slab (this includes the necking)
			cc = cc + 1;

			% Assign the temperature
			% In necking and rest it is the same
			tempMarker(m) = 273 + d_x(x, y) * (1573 - 273) / h;

			% Now we test if we are in the necking area
			if((y >= 100) && (y <= 150))
				% Necking area
				mTypeMarker(m) 		= GLOB_TYPE_NECKING;

				% Setting the values
				etaMarker(m) 		= 1e+23;
				rhoMarker(m) 		= 3400;
				strengthMarker(m)	= 2e+7;
			else
				% Not necking area
				mTypeMarker(m) 		= GLOB_TYPE_SLAB;

				% Setting the values
				etaMarker(m) 		= 1e+23;
				rhoMarker(m) 		= 3400;
				strengthMarker(m) 	= 1e+8;
			end % fi: necking area or not
		end % if: marker is in thorn

	end % if: we are in the box sorounding the thorn

end % for(m): Processing the markers

%fprintf('c = %d\n', cc);

% Test if all are set
if(sum(mTypeMarker(:) == GLOB_TYPE_INVALID) ~= 0)
	error('Not all markers where processed');
end


% generating the kValues

kMarker = calculate_kMarker(mTypeMarker, tempMarker);















end % function: generate_initSlab
