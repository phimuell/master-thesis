function [vxMarker, vyMarker] = interpolGridToMarker(vxNode, vyNode, xMarker, yMarker, Nx, Ny, dx, dy)
%{
	This function can interpolate the extended grid, to the marker.
	The extended gird is the gridd that has the boundary nodes around the computational domain.
	The Nx, Ny Values are the size of the computational domain.
	But the vxNode and vyNode are the velocities at the extended grid.
%}

N_m = numel(xMarker);
xsize = dx * (Nx - 1);
ysize = dy * (Ny - 1);

if( (Ny + 1 ~= size(vyNode, 1)) || (Nx + 1 ~= size(vyNode, 2)))
	size(vyNode)
	size(vxNode)
	error('Dimension Missmatch');
end

if( size(vyNode) ~= size(vxNode))
	error('Dimesion missmatch, Velocities are not the smae size')
end

% First process the Vx direction.
vxMarker = zeros(N_m, 1);	% Allocate space for the matrix, that stores the x velocities of the markers
vyMarker = zeros(N_m, 1);

% Iterate through the marker and interpolate the velocities
for m = 1:1:N_m

	if(xMarker(m) > xsize | xMarker(m) < 0)
		%error('Exceecing x');
	end
	if(yMarker(m) > ysize | yMarker(m) < 0)
		%error('Eyceecing y');
	end

	% Calculate the indecies
	j = fix(xMarker(m) / dx + 0.5) + 1;
	i = fix(yMarker(m) / dy + 0.5) + 1;

	% Now we calculate the differences
	delta_x_j = xMarker(m) - (dx * (j - 1) - dx * 0.5);	% no changes in the x delta
	delta_y_i = yMarker(m) - (dy * (i - 1) - dy * 0.5);	% This is like shifting.

	% Compute the wights
	% Speed up, compute 'delta / di' once
	omega_i_j   = (1.0 - delta_x_j / dx) * (1.0 - delta_y_i / dy);
	omega_i1_j  = (1.0 - delta_x_j / dx) * (      delta_y_i / dy);
	omega_i1_j1 = (      delta_x_j / dx) * (      delta_y_i / dy);
	omega_i_j1  = (      delta_x_j / dx) * (1.0 - delta_y_i / dy);


	if(abs(omega_i_j + omega_i1_j + omega_i_j1 + omega_i1_j1 - 1) > 1000 * eps)
		error('Error in interpolation.');
	end

	vxMarker(m) = omega_i_j * vxNode(i, j) + omega_i1_j * vxNode(i + 1, j) + omega_i_j1 * vxNode(i, j + 1) + omega_i1_j1 * vxNode(i + 1, j + 1);
	vyMarker(m) = omega_i_j * vyNode(i, j) + omega_i1_j * vyNode(i + 1, j) + omega_i_j1 * vyNode(i, j + 1) + omega_i1_j1 * vyNode(i + 1, j + 1);

end % end for(m)



if(sum(isnan(vyMarker(:))) > 0 || sum(isnan(vxMarker(:))) > 0)
	error('Nan in x node calculations');
end


if(sum(isinf(vyMarker(:))) > 0 || sum(isinf(vxMarker(:))) > 0)
	error('Nan in y node calculations');
end






end % end function
