function [vxExtGrid, vyExtGrid] = interpolGridToMarker(vxNode, vyNode, Nx, Ny, dx, dy)
%{
	This routine is able to map the velocities from the grid nodes to tha further grid.
	This grid consists of the velocities that are defined at the center of the cells.
	Unlike before, we have external nodes that are arround the whole grid.
	They also have a physical meaning to them, they are recovered by applying the boundary conditions.

	If the underlying grid is a $N_y \times N_x$ grid, then the returned grids are $(N_y + 1) \times (N_x + 1)$.
%}

% The new sizes of the array
NPx = Nx + 1;
NPy = Ny + 1;

vxExtGrid = zeros(NPy, NPx);
vyExtGrid = zeros(NPy, NPx);


for i = 2:1:Ny 	% Only until Ny not NPy
	for j = 2:1:Nx 	% Only until Nx not NPx

		% Now calculate the average for the extended grid
		vxExtGrid(i, j) = (vxNode(i, j - 1) + vxNode(i, j)) * 0.5;
		vyExtGrid(i, j) = (vyNode(i - 1, j) + vyNode(i, j)) * 0.5;

	end % for(j)
end % for(i)



%
% Now handling the boundary condition
%

%% X

% First for the left and right, for now we exclude the top and bottom corner
vxExtGrid(2:Ny, 1) 	= -vxExtGrid(2:Ny, 2);	% Left side
vxExtGrid(2:Ny, Nx + 1) = -vxExtGrid(2:Ny, Nx);	% Right side


% Now here we handing the top
vxExtGrid(1, 1:NPx) 	= vxExtGrid(2, 1:NPx);	% Top
vxExtGrid(NPy, 1:NPx) 	= vxExtGrid(NPy - 1, 1:NPx);	% Bottom


%% Y

% First the top and the bottom
vyExtGrid(1, 2:Nx) 	= -vyExtGrid(2, 2:Nx);	% Top
vyExtGrid(Ny + 1, 2:Nx) = -vyExtGrid(Ny, 2:Nx);	% Bottom

% Now the left and right sideA
vyExtGrid(:, 1) 	= vyExtGrid(:, 2);	% Left
vyExtGrid(:, NPx) 	= vyExtGrid(:, NPx - 1);% Right













end % function
