function TempMarker = tempBackToMarker(tempDelta, oldTempMarker, xMarker, yMarker, Nx, Ny, dx, dy)
%{
	This function interpolates the difference in temperature from the nodes to the
	markers.
	the variable oldTempMarker contains the old temperature of the markers.
%}

% the number of markers
Nm = numel(xMarker);

% The normalizing constant is for each property the same.
%WeightSum 	= zeros(Ny, Nx);	% The normalizing constant, for both the same

xsize = dx * (Nx - 1);
ysize = dy * (Ny - 1);

% Allocating space for the new marker temperature
TempMarker = zeros(Nm, 1);

if(size(oldTempMarker) ~= size(TempMarker))
	error('Size missmatch');
end


% Iterating throgh all the marker, to find the solution
for m = 1:1:Nm

	if(xMarker(m) > xsize | xMarker(m) < 0)
		xMarker(m)
		xsize
		error('Exceecing x');
	end
	if(yMarker(m) > ysize | yMarker(m) < 0)
		yMarker(m)
		error('Eyceecing y');
	end

	% Determing the indexes
	j = fix(xMarker(m) / dx) + 1;
	i = fix(yMarker(m) / dy) + 1;

	if(i == Ny)
		i
		m
		yMarker(m)
		error('i is to large');
	end

	if(j == Nx)
		i
		m
		error('j is to large');
	end

	% The distances between nodal point and marker
	delta_x_j = xMarker(m) - dx * (j - 1);
	delta_y_i = yMarker(m) - dy * (i - 1);

	% Compute the wights
	omega_i_j   = (1.0 - delta_x_j / dx) * (1.0 - delta_y_i / dy);
	omega_i1_j  = (1.0 - delta_x_j / dx) * (      delta_y_i / dy);
	omega_i1_j1 = (      delta_x_j / dx) * (      delta_y_i / dy);
	omega_i_j1  = (      delta_x_j / dx) * (1.0 - delta_y_i / dy);

	% Now we compute the differences
	diffInTemp = omega_i_j * tempDelta(i, j) + omega_i1_j * tempDelta(i + 1, j) + omega_i_j1 * tempDelta(i, j + 1) + omega_i1_j1 * tempDelta(i + 1, j + 1);

	% Now appling the change
	% Test maybe here a plus
    % !!!: confusing way of computing DT
% 	TempMarker(m) = oldTempMarker(m) - diffInTemp;
	TempMarker(m) = oldTempMarker(m) + diffInTemp;

	if(abs(omega_i_j + omega_i1_j + omega_i_j1 + omega_i1_j1 - 1) > 100 * eps)
		error('Error in interpolation.');
	end

end % end for(m)


end % end function(tempBackToMarker)








