function adiabaticHeat = impl_calcAdiabaticHeatingTerm(currTempNodes, vxNodes, vyNodes, rhoNodes, alphaNodes, gravityY)
%{
	This function calculates the adiabatic heating term.
	It only computes them at the internal nodes because it uses an average.

	it assumes that one can reduce the formula to
		H_a = T_0 \cdot \alpha \cdot v_y \cdot \rho \cdot g_y

	All quantities in the above equations are defined at the basic nodal points.

	The input arguments are:
	 - currTempNodes 	-> The current temperature at the basic nodal point
	 - vxNodes  		-> The x velocity at the x nodes of the stagered grid
	 - vyNodes 		-> The y velocities at the y nodes of the stagered grid
	 - rhoNodes 		-> The density at the basic nodal points
	 - alphaNodes 		-> The thermal expansion at the nodal points
	 - gravityY 		-> The gravity in y direction

	The vy is averaged.
%}

% Define the array
adiabaticHeat = zeros(size(currTempNodes));

Ny = size(currTempNodes, 1);
Nx = size(currTempNodes, 2);

for j = 2:1:(Nx - 1)
	for i = 2:1:(Ny - 1)

		% This is the velociy at the basic nodal point
		vyAtNodalPoint = (vyNodes(i, j) + vyNodes(i, j + 1)) * 0.5;

		% Now calculateing the adiabatic heat according to the formula
		adiabaticHeat(i, j) = currTempNodes(i, j) * alphaNodes(i, j) * vyAtNodalPoint * rhoNodes(i, j) * gravityY;

	end % for(i)
end % for(j)




end % function: impl_calcAdiabaticHeatingTerm
