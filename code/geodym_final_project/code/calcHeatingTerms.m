function [adiabaticHeating, radHeating, shearHeating] = calcHeatingTerms(radHeatingNodes, alphaNodes, rhoNodes, GravityY, TempNodes, vxNodes, vyNodes, etaNodes, dx, dy)
%{
	This function does calculate the different heating terms.
	In some cases this is trivial, but we do it heare in a central manner to canalize everything.
	But we provide some internal functions, that does the jobs.

	All heatings are calculated at the basic nodal point of the grid.
	The abiadiabatic and the shear heating are only computed in the internally node points.
	The reason for that is that computing them involves some average, so computing them there is not possible.
	Radioactive heating is computed also at the basic nodal points, but it is somputed at all points.

	The imput arguments are:
		- radHeatingNodes 		-> The radioactive heating at the node (basic)
		- alphaNodes			-> The thermal expanding (basic)
		- rhoNodes 			-> The density at the nodes (basic)
		- GravityY 			-> The gravity in y direction
		- TempNodes 			-> The temperature at the nodes (basic)
		- vxNodes			-> The x velocity at the x nodes of the stagered grid
		- vyNodes 			-> The y velocity at the y nodes of the stagered grid
		- etaNodes 			-> The viscosity, defined in the basic nodal point
%}

	% Radioactive heating is the same as the inputvalue
	radHeating = zeros(size(radHeatingNodes, 1), size(radHeatingNodes, 2));
    	radHeating(2:end-1, 2:end-1) = radHeatingNodes(2:end-1, 2:end-1);

	% Caclulating the adiabatic heat
	adiabaticHeating = impl_calcAdiabaticHeatingTerm(TempNodes, vxNodes, vyNodes, rhoNodes, alphaNodes, GravityY);

	% Calculating shear and strain rate
	[epsilon_xx, epsilon_yy, epsilon_yx, sigma_xx, sigma_yy, sigma_yx] = calculateStressStrain(vxNodes, vyNodes, etaNodes, dx, dy);

	% Calculate the shear heat value
	shearHeating = impl_calcShearHeating(epsilon_xx, epsilon_yy, epsilon_yx, sigma_xx, sigma_yy, sigma_yx);

end % function: calcHeatingTerms


