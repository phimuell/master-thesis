function [Vx_node, Vy_node, P] = stockesSolver(ETA, RHO, Nx, Ny, dx, dy, kindOfBC, dt, GY)
%{
	This function finds the velocity, where ETA, ROH represents the Viscosity and the density, at the grid points.
	The Nx, and Ny, are the numbers of gridpoints, and dx and dy are the distance between the points

	kindOfBoundary controles the type of boundaary, which is used:
		> 3  -> No Slip
		> 1  -> Free Slip

	The velocities that are calculated are not located at the basic nodal points.
	Instead the are located at their respective location, in the stagred grid.

	dt is the last time step, it is used as an aproximation for the next timestep.

	Remember not all nodal points that are acaculated have a physical meaning
%}

% Introducing scaled pressure
pScale = 1e+21 / dx;
%pScale = 1e+10;

% introducied velocity scaling
% This must be 1 allways
%vScale = 1e-7;
vScale = 1;

if(kindOfBC ~= 1)
	error('You using the wrong BC!\n');
end

% Gravity poitential
%GY = 9.81;

% Total numbers of unknows
N = Nx * Ny * 3;	% The three comes from the staggred grid

global L;
%L = spalloc(N, N, 19 * N);	% Matrix of coefficients (left Part)
R = zeros(N, 1);	% Vector of right parts


% %%%%%%%%%%%%%
% Compoese global matrix
% Going through all points of the 2D grid and composing respective equations
for j = 1:1:Nx
	for i = 1:1:Ny
		% Define global index in algebraic space
		k_vx = ((j - 1) * Ny + i - 1) * 3 + 1;
		k_vy = ((j - 1) * Ny + i - 1) * 3 + 2;
		K_pm = ((j - 1) * Ny + i - 1) * 3 + 3;

		% v_x Part
		if(i == 1 || i == 2 || i == Ny || j == 1 || j == Nx)

			% Fictional Point
			%if( i == 1 || j == 1  )
			if( i == 1 )
				L(k_vx, k_vx) = 1;
				R(k_vx) = 0;
			end

			% FIRST FREE SLIP CONDITIONS
			% x-Velocity does not penetrate the x boundary
			% INCONSISTENCY
			% 	We will never see j == 2, because it is not at the boundary-
			%	It jould be $j == 1$ instead $j == 2$. The reason why it works is
			%	above. There j == 1 is handeled correctly.
			%if( j == 2 || j == Nx )
			if( j == 1 || j == Nx )
				L(k_vx, k_vx) 	= 1;
				R(k_vx)		= 0;
			end

			% d_y[ v_x ] = 0     at the boundary at x_{wall}

			% upper wall
			if( i == 2 )
				L(k_vx, k_vx) 		=  1;
				L(k_vx, k_vx + 3) 	= -1.0 / kindOfBC;
				R(k_vx) 		=  0;
			end

			%At the lower wall
			if( i == Ny )
				L(k_vx, k_vx)		=  1;
				L(k_vx, k_vx - 3) 	= -1.0 / kindOfBC;
				R(k_vx)			=  0;
			end

		else
			% Inside the domain

			% Calculating of eta_p1
			ETA_p1 = eta_presure(ETA, i, j);
			ETA_p2 = eta_presure(ETA, i, j + 1);

			% d_x[\sigamo_{xx}^{\prime}]
			%
			% NOTICE: For this equation, we can assume, that we are the first one writing to the matrx
			%
			L(k_vx, k_vx)		= -2.0 * ETA_p2 / (dx^2) - 2.0 * ETA_p1 / (dx^2);	% Coefficient for v_x3
			L(k_vx, k_vx) = L(k_vx, k_vx) * vScale;		% Scaling of velocity
			L(k_vx, k_vx - 3 * Ny)	= vScale *  2.0 * ETA_p1 / (dx^2);				% Coefficient for v_x1
			L(k_vx, k_vx + 3 * Ny)	= vScale *  2.0 * ETA_p2 / (dx^2);				% Coefficient for v_x5


			% d_y[\sigma_{xy}^{\prime}]
			%
			% NOTICE: Now we can't neccessaraly assume, that we are the first one, who write to a coefficient.
			%	   After analysing the paper work, I conclude, that this is only the case for v_x3.
			%	   So for this coefficient, we must take special care.
			ETA_1 = ETA(i-1, j);
			ETA_2 = ETA(i, j);

            		% ERROR !!!: factor 2 wrong
% 			L(k_vx, k_vx)		= L(k_vx, k_vx) +  vScale * ( -2.0 * ETA_2 / (dy^2) - 2.0 * ETA_1 / (dy^2));	% COefficient for v_x3
			L(k_vx, k_vx)		= L(k_vx, k_vx) +  vScale * ( -ETA_2 / (dy^2) - ETA_1 / (dy^2));	% COefficient for v_x3
			L(k_vx, k_vx - 3)	= vScale *  ETA_1 / (dy^2);					% Coefficient for v_x2
			L(k_vx, k_vx + 3)	= vScale * ETA_2 / (dy^2);					% Coefficient for v_x4

			% Now we do the y stuff
			%
			%  (v_y1) ----------- (v_y3)
			%    |			|
			%    | 			|
			%  (v_y2)*----------- (v_y4)
			% We are sitting at the node, markded with *

			L(k_vx, k_vy - 3)	= vScale * ETA_1 / (dx * dy);		% This is the v_y1, at the node one above us
			L(k_vx, k_vy)		= vScale * (-ETA_2) / (dx * dy);		% This is v_y2, at our node
			L(k_vx, (k_vy -3) + 3 * Ny) = vScale * (-ETA_1) / (dx * dy);	% This is v_y3, at the node that
			L(k_vx, k_vy + 3 * Ny)	= vScale * ETA_2 / (dx * dy);		% This is v_y4, at the node to our right


			% Now also the presure
			L(k_vx, K_pm)		= pScale / dx;
			L(k_vx, K_pm + 3 * Ny)	= -pScale / dx;

			% RHS, that is composed of the presure
			R(k_vx) = 0;
		end


		% v_y Part
		if( j == 1 || j == 2 || j == Nx || i == 1 || i == Ny)
			% Boundary part

			% Fictonal points
			if( j == 1  || i == 1)
				L(k_vy, k_vy) 	= 1;
				R(k_vy) 	= 0;
			end


			% For free slip v_y at the y bondary is zero
			% There the noes sitting directly on the boundary
			if( i == 2 || i == Ny)
				L(k_vy, k_vy) 	= 1;
				R(k_vy) 	= 0;
			end


			% This is the left wall
			if( j == 2 )
				L(k_vy, k_vy) 		=  1;
				L(k_vy, k_vy + 3 * Ny) 	= -1.0 / kindOfBC;
				R(k_vy)			=  0;
			end

			% This is the right wall
			if( j == Nx )
				L(k_vy, k_vy)		=  1;
				L(k_vy, k_vy - 3 * Ny)	= -1.0 / kindOfBC;
				R(k_vy)			=  0;
			end
		else
			% Inside the domain

			ETA_p1 = eta_presure(ETA, i ,j);
			ETA_p2 = eta_presure(ETA, i + 1, j);

			% First d_y[\sigma_{yy}^{\prime}
			% As for the x part, we can here assume, that we are the first one, that tried to write coefficients to the matrix
			L(k_vy, k_vy - 3) 	=  vScale * 2.0 * ETA_p1 / (dy^2);		% This is the v_y2
			L(k_vy, k_vy)		= -2.0 * ETA_p2 / (dy^2) - 2.0 * ETA_p1 / (dy^2);	% This is v_y3
			L(k_vy, k_vy) = L(k_vy, k_vy) * vScale;
			L(k_vy, k_vy + 3)	=  vScale * 2.0 * ETA_p2 / (dy^2);		% This is v_y4

			% d_x[\sima_{yx}^{\prime}]
			%
			% Asbefore, we didn't have written to the coefficient of this equation, except the v_y3 coefficient
			ETA_1 = ETA(i, j -1);	% Eta accociated, with the node to our left
			ETA_2 = ETA(i, j);	% ETA assosicated with or node
			L(k_vy, k_vy - 3 * Ny)	= vScale *  ETA_1 / (dx^2);			% v_y1
			L(k_vy, k_vy) = L(k_vy, k_vy) + vScale * ( -ETA_2 / (dx^2) - ETA_1 / (dx^2) );	% v_y3, We have allready written to it
			L(k_vy, k_vy + 3 * Ny)	= vScale * ETA_2 / (dx^2);		% v_y5

			% now the x stuff
			%
			%  (v_x1) ----------- (v_x3)*
			%    |			|
			%    | 			|
			%  (v_x2) ----------- (v_x4)
			% We are sitting at the node, marked with *

			L(k_vy, k_vx - 3 * Ny)	= vScale * ETA_1 / (dx * dy);		% The v_x1, the node to our left
			L(k_vy, k_vx - 3 * Ny + 3 ) = vScale * (-ETA_1) / (dx * dy);	% The node for v_x2
			L(k_vy, k_vx)		= vScale * (-ETA_2) / (dx * dy);		% The v_x3 node, where we are
			L(k_vy, k_vx + 3)	= vScale * ETA_2 / (dx * dy);		% The v_x4 node, the node below us

			% Now also the presure
			L(k_vy, K_pm)		= pScale / dy;
			L(k_vy, K_pm + 3)	= -pScale / dy;

			% RHS, that is composed of the presure
			R(k_vy) = -GY * (RHO(i, j) + RHO(i, j - 1)) / 2.0;
			%L(k_vy, k_vy)

			% Stabilizing of the stokes eq
			% Here we stabilize the equation, be making the solution dependding on the next density ditribution.
			% For that we need a timestep, this step is equal the last one, as an aproximation for the current timestep.
			% In the first timestep, we will use 0.


			% First the
			% 	-v_y * d{Rho}/d{y} * dt
			% v_y = v_y_{3} 	The node we currently process
			%
			% d{Rho}/d{y} = (1/2) * ( \frac{rho(i + 1, j - 1) - rho(i - 1, j - 1)}{2 * dy} + \frac{rho(i + 1, j) - rho(i - 1, j)}{2 * dy} )
			% ERROR !!!: wrong index
%             		dRho = 0.5 * ( (RHO(i + 1, j - 1) - RHO(i + 1, j - 1)) / (2 * dy) + (RHO(i + 1, j) - RHO(i - 1, j)) / (2 * dy));
            		dRho = 0.5 * ( (RHO(i + 1, j - 1) - RHO(i - 1, j - 1)) / (2 * dy) + (RHO(i + 1, j) - RHO(i - 1, j)) / (2 * dy));
			L(k_vy, k_vy) = L(k_vy, k_vy) - dt * dRho * GY;	% The minus is because we did not considered it further, till now


			% now the
			% 	-dt * d{Rho}/d{x} * v_{x}^{\Delta t}
			%
			dRho = (RHO(i, j) - RHO(i, j - 1)) / dx;
			coeff = -dt * dRho * 0.25 * GY * vScale;	% This is the coefficient, also the minus and the 1/4 and the dt are moved to this

			L(k_vy, k_vx) 		= L(k_vy, k_vx) + coeff;
			L(k_vy, k_vx + 3) 	= L(k_vy, k_vx + 3) + coeff;
			L(k_vy, k_vx - 3 * Ny) 	= L(k_vy, k_vx - 3 * Ny) + coeff;
			L(k_vy, k_vx - 3 * Ny + 3) = L(k_vy, k_vx - 3 * Ny + 3) + coeff;

		end



		% Presure part
		if(j == 1 || i == 1 || ( (j == 2 && i == 2) || (j == 2 && i == Ny) || (j == Nx && i == 2) || (j == Nx && i == Ny) || (j == 3 && i == 2)))

			% Fictional points
			if( i == 1 || j == 1 )
				L(K_pm, K_pm) 		= 1;
				R(K_pm)			= 0;
			end

			% Real boundary for pressure
			if(i == 2 && j == 3)
				L(K_pm, K_pm) = 1.0 * pScale;
				R(K_pm) = 1e+9;
			end


			% Now we make, that they have the same values
			% Top left corner
			if( i == 2 && j == 2 )
				L(K_pm, K_pm) 		=  1;
				L(K_pm, K_pm + 3 * Ny)	= -1;
				R(K_pm)			=  0;
			end

			% Bottom left corner
			if( i == Ny && j == 2 )
				L(K_pm, K_pm)		=  1;
				L(K_pm, K_pm + 3 * Ny)	= -1;
				R(K_pm) 		=  0;
			end

			% Top right corner
			if( i == 2 && j == Nx )
				L(K_pm, K_pm) 		=  1;
				L(K_pm, K_pm - 3 * Ny)	= -1;
				R(K_pm)			=  0;
			end

			% bottom right corner
			if( i == Ny && j == Nx )
				L(K_pm, K_pm)		=  1;
				L(K_pm, K_pm - 3 * Ny) 	= -1;
				R(K_pm)			=  0;
			end
		else
			% Inside the boundary
			% Inside this, we solve the continue equation
			% Is still the same
			L(K_pm, k_vx) 		= vScale / dx;
			L(K_pm, k_vx - Ny * 3)	= -vScale / dx;
			L(K_pm, k_vy)		= vScale / dy;
			L(K_pm, k_vy - 3) 	= -vScale / dy;

			% RHS
			R(K_pm) = 0;
		end
	end % End for(i)
end %End for(j)

%fprintf('Cond of L matrix: %e\n', condest(L, 5));

% Solving the matrices
S = L \ R;






% Calculating the velocities
Vx_node = zeros(Ny, Nx);
Vy_node = zeros(Ny, Nx);
P  = zeros(Ny, Nx);

% Reload the Data
for j = 1:1:Nx
	for i = 1:1:Ny

		k_vx = ((j - 1) * Ny + i - 1) * 3 + 1;
		k_vy = ((j - 1) * Ny + i - 1) * 3 + 2;
		K_pm = ((j - 1) * Ny + i - 1) * 3 + 3;

		Vy_node(i, j) = S(k_vy) * vScale;
		Vx_node(i, j) = S(k_vx) * vScale;
		P(i, j) = S(K_pm) * pScale;	% Restore the real pressure

	end % End for(i)
end %End for(j)

fprintf('VX(unscaled): mean = %e; max = %e\n', mean(abs(Vx_node(:) / vScale)), max(abs(Vx_node(:) / vScale)))
fprintf('VY(unscaled): mean = %e; max = %e\n', mean(abs(Vy_node(:) / vScale)), max(abs(Vy_node(:) / vScale)))
fprintf('P(unscaled):  mean = %e; max = %e\n', mean(abs(P(:) / pScale)), max(abs(P(:) / pScale)))

if(sum(P(:) < 0.0) > 0)
	error('Found negative pressures.')
end



%nnz(L) - 19 * N
end % end function(stockesSolver)





function eta_p = eta_presure(ETA, i, j)
	Eta1 = ETA(i    , j    );
	Eta2 = ETA(i    , j - 1);
	Eta3 = ETA(i - 1, j    );
	Eta4 = ETA(i - 1, j - 1);

	eta_p = 1.0 / Eta1 + 1.0 / Eta2 + 1.0 / Eta3 + 1.0 / Eta4;
	eta_p = 4.0 / eta_p;

	if(isnan(eta_p))
		error('eta p macht probleme');
	end
end


