function shearHeatingNodes = impl_calcShearHeating(epsilon_xx, epsilon_yy, epsilon_yx, sigma_xx, sigma_yy, sigma_yx)
%{
	This function calculates the shear heating terms.
	This term is only calculated at the interial nodes.
	It assumes that the array format is in the same format as the one produced by the calulateStressStrain function.

	The heatingterm is calculated in the basic nodal points.
	Notice, that the input quantities, beside the yx quantities are not defined there.
	To get the values at the nodal points we use averaging.

	H_s = \sigma_{xx}^{\prime} \cdot \dot{\epsilon}_{xx} + \sigma_{yy}^{\prime} \cdot \dot{\epsilon}_{yy} + 2 \cdot \sigma_{xy}^{\prime} \cdot \dot{\epsilon}_{xy}

	The products \sigma \cdot \dot{\epsilon} are calculated at each of the center of the 4 sourounding cells, and then averaged before summing them up.
	Notice that yx is already defined at the correct location.
%}

% Get the size of the system
Nx = size(epsilon_xx, 2);
Ny = size(epsilon_xx, 1);


% Define the return value
shearHeatingNodes = zeros(Ny, Nx);


% iterating through the internal nodes to get the heating terms
for j = 2:1:(Nx - 1)
	for i = 2:1:(Ny - 1)

		xxTerm = mean(sigEpsInCenter(epsilon_xx, sigma_xx, i, j));
		yyTerm = mean(sigEpsInCenter(epsilon_yy, sigma_yy, i, j));
		yxTerm = epsilon_yx(i, j) * sigma_yx(i, j);

		shearHeatingNodes(i, j) = xxTerm + yyTerm + 2 * yxTerm;
	end % for(i)
end % for(j)









end % function: impl_calc_shearHeating


% This function does calculate the product of sigma and dot{epsilon} in the center of the four cells sourinding node i and j
% the returned is a matrix
function valueInCenters = sigEpsInCenter(epsilon, sigma, i, j)

	valueInCenters = zeros(2, 2);

	valueInCenters(1, 1) = epsilon(i    , j    ) * sigma(i    , j    );
	valueInCenters(2, 1) = epsilon(i + 1, j    ) * sigma(i + 1, j    );
	valueInCenters(1, 2) = epsilon(i    , j + 1) * sigma(i    , j + 1);
	valueInCenters(2, 2) = epsilon(i + 1, j + 1) * sigma(i + 1, j + 1);

	% Make a vector out of the matrix
	valueInCenters = valueInCenters(:);

end % function: sigEpsInCenter



