function s = secToYear(y)
% Imput seconds and transform it to years
s = y .* ( 3600 * 24 * 7 * 52 );
end
