function [newMarkerTemp, newTempDeltaNode] = subGridDiffusion(tempDeltaNode, oldTempNode, oldTempMarker, rhocpMarker, kMarker, xMarker, yMarker, Nx, Ny, dx, dy, dt)
%{
	This function is able to perform subgrid diffusion. of the temperature.
	It does everything in one step.

	The argument of this functions are the following:
		> tempDeltaNode:	The temperature difference a the nodes. Is calculated by the solver
		> oldTempNode:		The old or current temperature at the node, this is calculated by the interpolation
		> oldTempMarker:	The old or current temperature of the marker.
		> rhocpMarker:		The volumetric heat capacity of the marker.
		> kMarker:	 	Thermal conductivity of the marker.
		> xMarker:		The x position of the markers.
		> yMarker: 		The y Position of the markers.
		> Nx:			The number of basic grid points in x (second index) direction.
		> Ny:			The number of basic grid points in y (first  index) direction.
		> dx, dy 		The grid spacing, space between two basic/nodal grid points.
		> dt: 			The timestep, that is performed.

	This function splits the Diffusion of into two parts.
	It still operates on the temperature differences at a node DT.

		DT = DT_{sg} + DT_{r}

	DT_{sg}	- Is the subgrid diffusion
	DT_{r}  - Is the rest of the heat

	The temperature, that is given by the nodal point is an average and the marker temperature, can be quite off.
	If let be a T_{n}^{0}, be the temperature of the nodal temperature field at the current time, at the location of the marker.
	We imagine that the there is a node at the location at the marker.
	Let T_{m}^{0} be the old/current marker temperature.
	The different in temperature at the current times between them is given as DT_{m, n}^{0} = T_{m}^{0} - T_{n}^{0}
	The diffusion on the subgrid is given as:

		DT_{m, n}(t) = DT_{m, n}^{0} \cdot \Exp{ - \frac{ 2 \cdot k_{m}}{rhocp_{m}} \ctot t \cdot ( \frac{1}{dx^2} + \frac{1}{dy^2} ) }

	We now relax the difference in temperature for dt time.
	Do effectively we calculate DT_{m, n}(dt).

	We now defines the change in the change of the temperature differences.

		DDT_{m, n} = DT_{m, n}(dt) - DT_{m, n}^{0}

	We now updates the markers in a \emph{first} step.
	This is essentialy the change to subgrid.

		T_{m}^{0, dt} = T_{m}^{0} + DDT_{m, n}

	This is the marker temperature, first updated.

	We now also interpolate DDT_{m, n} \emph{back} to the nodes.
	!! We do this by this conservative scheme, with the rhocp. !!
	This will give us, DT_{sg} for the grid.

	Then we can compute DT_{r} = DT - DT_{sg}.
	Then we interpolate DT_{r} to the marker in the same way as we did it before, we can use the same routine as before.

	In this whole procedere, we assume, that the nodes are not moved yet.
%}


% The numbers of markers
Nm = numel(xMarker);


% Allocate output arrays
newMarkerTemp 		= zeros(size(xMarker));
newTempDeltaNode 	= zeros(size(tempDeltaNode));		% The new difference in temperature at the nodes


%
% Allocate helper arrays and fill them

% This is the current temperature at the nodes
currentNodeTemp 	= oldTempNode;

% This is the subgrid diffusion at the nodes
diffTempSG_Node 	= zeros(size(currentNodeTemp));

% This is the weight sum for calculating the subgrid diffusion on the node
WeightSum 		= zeros(size(oldTempNode));




% This is a part of the exponent in the diffusion formula
% It contains everything except the k and rhocp
constantPartOfDiffusionExponent = -2.0 * dt * ( (1.0 / (dx * dx)) + (1.0 / (dy * dy)) );

% The size of the computational domain
xsize = dx * (Nx - 1);
ysize = dy * (Ny - 1);



% Iterate through all the markers
for m = 1:1:Nm

	if(xMarker(m) > xsize | xMarker(m) < 0)
		xMarker(m)
		xsize
		error('Exceecing x');
	end
	if(yMarker(m) > ysize | yMarker(m) < 0)
		yMarker(m)
		error('Eyceecing y');
	end

	% Determing the indexes
	j = fix(xMarker(m) / dx) + 1;
	i = fix(yMarker(m) / dy) + 1;

	if(i == Ny)
		i
		m
		yMarker(m)
		error('i is to large');
	end

	if(j == Nx)
		i
		m
		error('j is to large');
	end

	% The distances between nodal point and marker
	delta_x_j = xMarker(m) - dx * (j - 1);
	delta_y_i = yMarker(m) - dy * (i - 1);

	% Compute the wights
	omega_i_j   = (1.0 - delta_x_j / dx) * (1.0 - delta_y_i / dy);
	omega_i1_j  = (1.0 - delta_x_j / dx) * (      delta_y_i / dy);
	omega_i1_j1 = (      delta_x_j / dx) * (      delta_y_i / dy);
	omega_i_j1  = (      delta_x_j / dx) * (1.0 - delta_y_i / dy);

	% Now we compute the contributiojn of each marker to the 4 grid points

	% Calculate the temperature of a fictuional node at the markers position
	currTempNodeAtMarkerLocation =    omega_i_j * currentNodeTemp(i, j) ...
					+ omega_i1_j * currentNodeTemp(i + 1, j) ...
					+ omega_i_j1 * currentNodeTemp(i, j + 1) ...
					+ omega_i1_j1 * currentNodeTemp(i + 1, j + 1);

	% Calculate the difference in temperature between the fictional node node and the marker
	diffTempNodeMarker = oldTempMarker(m) - currTempNodeAtMarkerLocation;

	% Diffuse the difference in the temperature nof the fictional node and the mrker
	diffusedTempDifferenceMarker = (exp( constantPartOfDiffusionExponent * kMarker(m) / rhocpMarker(m)) - 1.0 ) * diffTempNodeMarker;

	% Calculate the new temperature of the marker, with the change due to subgrid diffusion
	newMarkerTemp(m) = oldTempMarker(m) + diffusedTempDifferenceMarker;


	% Interpolate the subgrid diffusion to the nodes, using the conservative scheme
	diffTempSG_Node(i, j) 		= diffTempSG_Node(i, j) 	+ rhocpMarker(m) * diffusedTempDifferenceMarker * omega_i_j;
	diffTempSG_Node(i + 1, j) 	= diffTempSG_Node(i + 1, j) 	+ rhocpMarker(m) * diffusedTempDifferenceMarker * omega_i1_j;
	diffTempSG_Node(i + 1, j + 1) 	= diffTempSG_Node(i + 1, j + 1) + rhocpMarker(m) * diffusedTempDifferenceMarker * omega_i1_j1;
	diffTempSG_Node(i, j + 1) 	= diffTempSG_Node(i, j + 1) 	+ rhocpMarker(m) * diffusedTempDifferenceMarker * omega_i_j1;


	% Weight sum we need
	WeightSum(i, j) 	= WeightSum(i, j) 	+ rhocpMarker(m) * omega_i_j;
	WeightSum(i + 1, j) 	= WeightSum(i + 1, j) 	+ rhocpMarker(m) * omega_i1_j;
	WeightSum(i + 1, j + 1) = WeightSum(i + 1, j + 1) + rhocpMarker(m) * omega_i1_j1;
	WeightSum(i, j + 1) 	= WeightSum(i, j + 1) 	+ rhocpMarker(m) * omega_i_j1;


	if(abs(omega_i_j + omega_i1_j + omega_i_j1 + omega_i1_j1 - 1) > 100 * eps)
		error('Error in interpolation.');
	end

end % end for(m)


% Determine the subgrid diffused temperature
% Maybe ensure that all are none zero
diffTempSG_Node = diffTempSG_Node ./ WeightSum;

if(sum(isnan(diffTempSG_Node(:))) > 0)
	error('Nan in calculation of subgrid diffusion occured.');
end

% Calculate the remaining part of the diffusied temperature in the on the grid
newTempDeltaNode = tempDeltaNode - diffTempSG_Node;




end % function: subGridDiffusion
