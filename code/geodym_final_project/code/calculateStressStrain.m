function [epsilon_xx, epsilon_yy, epsilon_yx, sigma_xx, sigma_yy, sigma_yx] = calculateStressStrain(vxNodes, vyNodes, etaNodes, dx, dy)
%{
	This function calculates the stress and the strain rate.
	sigma_{xx, yy} and epsilon_{xx, yy} are defined at the pressure points.
	{epsilon, sigma}_xy are defined in the basic nodal points.

	The returned arrays does have the same size as the the grid, but not all entries carries a meaning.

	The xx quantities are only defined at the center of a cell.
	So the first row and first column does not have any meaning.
	The 'epsilon_xx(2,2)' is the strain rate for the first cell.

	The xy quantities are defined at the inner grid points.
	So all boundary etries does not have any meaning.

	NOTE:
	We label the quantities sigma, but it is actually the deviatoric shear stress.

%}

% get the size of the domain
Ny = size(etaNodes, 1);
Nx = size(etaNodes, 2);

% Allocate the output arrays
epsilon_xx = zeros(Ny, Nx);
epsilon_yy = zeros(Ny, Nx);
epsilon_yx = zeros(Ny, Nx);

sigma_xx = zeros(Ny, Nx);
sigma_yy = zeros(Ny, Nx);
sigma_yx = zeros(Ny, Nx);

%
% We first calculate epsilon before we calculate sigma


% Calcualte epsilon_xy
%  and sigma_xy
for j = 2:1:(Nx - 1)
	for i = 2:1:(Ny - 1)

		% Strain rate
		epsilon_yx(i, j) = 0.5 * ( (vyNodes(i, j+1) - vyNodes(i, j)) / dx + (vxNodes(i + 1, j) - vxNodes(i, j)) / dy );

		% deviatoric strress
		% Here we can use the viscosity, that is defined at the basic nodal points
		sigma_yx(i, j)   = 2.0 * etaNodes(i, j) * epsilon_yx(i, j);

	end % for: i
end % for: j



% Calculate the epsilon_{xx, yy}
% Here we go until the end, becasue there is still a cell
for j = 2:1:Nx
	for i = 2:1:Ny

		% Calculate the shear stress
		epsilon_xx(i, j) = (vxNodes(i, j) - vxNodes(i, j - 1)) / dx;
		epsilon_yy(i, j) = (vyNodes(i, j) - vyNodes(i - 1, j)) / dy;

		% We need to calculate the viscosity in the pressure point.
		% For that we use the harmonic average of the 4 nodal points
		etaPressurePoint = eta_presure(etaNodes, i, j);
		sigma_xx(i, j) 	 = 2.0 * etaPressurePoint * epsilon_xx(i, j);
		sigma_yy(i, j) 	 = 2.0 * etaPressurePoint * epsilon_yy(i, j);

	end % for: i
end % for: j

PRINT(epsilon_xx, 'epsXX')
PRINT(epsilon_yy, 'epsYY')
PRINT(epsilon_yx, 'epsYX')


end % function: calculateStressStrain

function PRINT(E, s)
	E = E(:);

	fprintf('max(%s)  = %e\n', s, max(E));
	fprintf('min(%s)  = %e\n', s, min(E));
	fprintf('mean(%s) = %e\n', s, mean(E));

end % function print





% This function calculates the viscosity in the pressure point that is
% associated with the node i, j.
% The viscossity ETA is the viscosity defined at the basic nodal points
% This function is taken from the solver
function eta_p = eta_presure(ETA, i, j)
	Eta1 = ETA(i    , j    );
	Eta2 = ETA(i    , j - 1);
	Eta3 = ETA(i - 1, j    );
	Eta4 = ETA(i - 1, j - 1);

	eta_p = 1.0 / Eta1 + 1.0 / Eta2 + 1.0 / Eta3 + 1.0 / Eta4;
	eta_p = 4.0 / eta_p;

	if(isnan(eta_p))
		error('eta p macht probleme');
	end
end


