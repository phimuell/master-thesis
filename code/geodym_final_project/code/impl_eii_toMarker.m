function [eiiMarker, pressMarker] = impl_eii_toMarker(Iepsilon_xx, Iepsilon_yy, epsilon_xy, IpressNode, xMarker, yMarker, dx, dy)
%{
	This function is able to calculate the \dot{\epsilon_{\text{II}}} for each MARKER.
	In order to achieve that, the different epsilons are first interpolated to the marker's position.
	Then the the value \dot{\epsilon_{\text{II}}} is calculated for the marker as:

		\dot{\epsilon_{\text{II}}} = \sqrt{ (\epsilon_{xx}^{2} + \epsilon_{yy}^{2}) / 2 + \epsilon_{xy} }

	The interpolation is not "unfiorm", meaning that that the different epsilons are not defined at the same locations.
	The xy are defined at the inner nodal points only, meaning that they are not defined at the boundary.
	For the interolation we assume that they are zero there and procede as usual.

	The xx and yy quantitties are defined at the pressure points, but only at the real one and not the fictional.
	For the interpolation, we extend the grid, such that we have pressure points arround the domain.
	We assume they are zero and interpolate as usual.

	For special reason, I am lazy, this function also interpolates the presure to the markers.
	The interpolation is doen the same way as the for the xx quantities.
	Meaning that they are also not defined at the sorounding pressure points.
%}

% Get the size
Nm 	= size(xMarker, 1);
Ny 	= size(Iepsilon_xx, 1);
Nx 	= size(Iepsilon_xx, 2);

% Size of the computational domain
xsize = dx * (Nx - 1);
ysize = dy * (Ny - 1);

% Setting the boundary to zero, only as insurance
% We do not need to expand this
epsilon_xy(1, :)   = 0;
epsilon_xy(end, :) = 0;
epsilon_xy(:, 1)   = 0;
epsilon_xy(:, end) = 0;

% Expand the computational domain of the xx and yy
% We expand and copy for the uniformity of numbering.
epsilon_xx = zeros(Ny + 1, Nx + 1);
epsilon_xx(2:end-1, 2:end-1) = Iepsilon_xx(2:end, 2:end);	% Copy

epsilon_yy = zeros(Ny + 1, Nx + 1);
epsilon_yy(2:end-1, 2:end-1) = Iepsilon_yy(2:end, 2:end);	% Copy

pressNode = zeros(Ny + 1, Nx + 1);
pressNode(2:end-1, 2:end-1) = IpressNode(2:end, 2:end);		% Copy


% For exporting
% Used in ploting
global dEiiMarker_global;

% Defining output array
eiiMarker 	= zeros(size(xMarker));
pressMarker 	= zeros(size(xMarker));


% Iterate through the marker and interpolate the velocities
for m = 1:1:Nm

	if(xMarker(m) > xsize | xMarker(m) < 0)
		error('Exceecing x');
	end
	if(yMarker(m) > ysize | yMarker(m) < 0)
		error('Eyceecing y');
	end

	%
	% Interpolating the xx and yy quantities
	%

	% Calculate the indecies
	% This are the indices for the left upper presserpoint, that could be located outside the original domain
	% We add +0.5 because the (0, 0) node is conceptualy located at (-0.5 dx, -0.5 dy).
	j = fix(xMarker(m) / dx + 0.5) + 1;
	i = fix(yMarker(m) / dy + 0.5) + 1;

	% Now we calculate the differences
	% Here we actually \emph{add} 0.5 of the grid spacing, cecause of the all enclosing mius.
	% We do this because of the definition of the location of the pressure point.
	delta_x_j = xMarker(m) - (dx * (j - 1) - dx * 0.5);
	delta_y_i = yMarker(m) - (dy * (i - 1) - dy * 0.5);


	% Compute the wights
	% Speed up, compute 'delta / di' once
	omega_i_j   = (1.0 - delta_x_j / dx) * (1.0 - delta_y_i / dy);
	omega_i1_j  = (1.0 - delta_x_j / dx) * (      delta_y_i / dy);
	omega_i1_j1 = (      delta_x_j / dx) * (      delta_y_i / dy);
	omega_i_j1  = (      delta_x_j / dx) * (1.0 - delta_y_i / dy);

	if(abs(omega_i_j + omega_i1_j + omega_i_j1 + omega_i1_j1 - 1) > 100 * eps)
		error('Error in interpolation.');
	end

	% Interpolating the the values
	% Save in temporaries
	dExx_Marker = epsilon_xx(i, j) * omega_i_j + epsilon_xx(i + 1, j) * omega_i1_j + epsilon_xx(i, j + 1) * omega_i_j1 + epsilon_xx(i + 1, j + 1) * omega_i1_j1;
	dEyy_Marker = epsilon_yy(i, j) * omega_i_j + epsilon_yy(i + 1, j) * omega_i1_j + epsilon_yy(i, j + 1) * omega_i_j1 + epsilon_yy(i + 1, j + 1) * omega_i1_j1;

	% The pressure can be interpolated the same way, but directly
	% We save it directly in the array
	pressMarker(m) = pressNode(i, j) * omega_i_j + pressNode(i + 1, j) * omega_i1_j + pressNode(i, j + 1) * omega_i_j1 + pressNode(i + 1, j + 1) * omega_i1_j1;



	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	% interpolating the xy quantities
	%

	% Here we recaculate the indices and weights
	% Because here the nodes are the same as the basic nodal point.
	% We want to remember that only the inner point carries a meaning and the boundary is assumed (and set) zero
	j = fix(xMarker(m) / dx) + 1;
	i = fix(yMarker(m) / dy) + 1;

	% Now we calculate the differences
	delta_x_j = xMarker(m) - (dx * (j - 1));
	delta_y_i = yMarker(m) - (dy * (i - 1));


	% Compute the wights
	% Speed up, compute 'delta / di' once
	omega_i_j   = (1.0 - delta_x_j / dx) * (1.0 - delta_y_i / dy);
	omega_i1_j  = (1.0 - delta_x_j / dx) * (      delta_y_i / dy);
	omega_i1_j1 = (      delta_x_j / dx) * (      delta_y_i / dy);
	omega_i_j1  = (      delta_x_j / dx) * (1.0 - delta_y_i / dy);

	% Interpolating the values
	% For the xy quantities, save in a temporary
	dExy_Marker = epsilon_xy(i, j) * omega_i_j + epsilon_xy(i + 1, j) * omega_i1_j + epsilon_xy(i, j + 1) * omega_i_j1 + epsilon_xy(i + 1, j + 1) * omega_i1_j1;



	% Now caculate the the dEii
	eiiMarker(m) = sqrt( ((dExx_Marker * dExx_Marker) + (dEyy_Marker * dEyy_Marker)) * 0.5  + (dExy_Marker * dExy_Marker) );

	% Now update the global variable for storing eiiMarker
	dEiiMarker_global(m) = eiiMarker(m);

end % end for(m)

fprintf('max(e_II)  = %e\n', max(eiiMarker))
fprintf('min(e_II)  = %e\n', min(eiiMarker))
fprintf('mean(e_II) = %e\n', mean(eiiMarker))



end % function: impl_eii_toMarker



