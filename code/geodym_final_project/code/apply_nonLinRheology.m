function [rhoMarker, etaMarker, kMarker] = apply_nonLinRheology(xMarker, yMarker, vxNodes, vyNodes, etaNodes, dx, dy, alphaMarker, betaMarker, mTypeMarker, tempMarker, pressNode)
%{
	This function is able to apply the non linear rheology.
	It applies the formula given on the project setup.

	It is important to note, that the pressures have some points that are unphysically.
	The unphysical values are
		pressNode(1, :) and pressNode(:, 1)
	The same restriction also holds for the epsilon_{xx, yy}
%}

	% calculate the strain rate (again)
	[epsilon_xx, epsilon_yy, epsilon_xy, ~, ~, ~] = calculateStressStrain(vxNodes, vyNodes, etaNodes, dx, dy);

	% Calcualte the pressure and invariants for the markers
	[eiiMarker, pressMarker] = impl_eii_toMarker(epsilon_xx, epsilon_yy, epsilon_xy, pressNode, xMarker, yMarker, dx, dy);

	% Calculate rho
	rhoMarker = calcRho(betaMarker, pressMarker, alphaMarker, tempMarker, mTypeMarker);

	% Calculate eta
	etaMarker = calcEta(eiiMarker, pressMarker, tempMarker, mTypeMarker);

	% calculate k
	kMarker = calculate_kMarker(mTypeMarker, tempMarker);

end % function: apply_nonLinRheology

% This function caculates rho accoriding to the given formula
% It requires that the correct possitions are returned
function rhoMarker = calcRho_int(rho0, betaMarker, pressMarker, alphaMarker, tempMarker)

	rhoMarker = rho0 * (1 + betaMarker .* ( pressMarker - 1e+5)) ./ ( 1 + alphaMarker .* (tempMarker - 273));

end % function: calcRho_int



% This function makrs the same thing as the one above, but a bit nicer
function rhoMarker = calcRho(betaMarker, pressMarker, alphaMarker, tempMarker, mTypeMarker)

	%global GLOB_TYPE_AIR GLOB_TYPE_PLATE GLOB_TYPE_MANTLE GLOB_TYPE_NECKING GLOB_TYPE_INVALID;
	global GLOB_TYPE_AIR GLOB_TYPE_PLATE GLOB_TYPE_MANTLE GLOB_TYPE_NECKING GLOB_TYPE_INVALID GLOB_TYPE_SLAB;

	% Generate output arrays
	Nm = size(betaMarker, 1);
	rhoMarker = zeros(Nm, 1);

	% Handling the plate and neck
	indexPlateAndNeck 	     = mTypeMarker(:) == GLOB_TYPE_PLATE | mTypeMarker(:) == GLOB_TYPE_NECKING | mTypeMarker(:) == GLOB_TYPE_SLAB;
	rhoMarker(indexPlateAndNeck) = calcRho_int(3400, betaMarker(indexPlateAndNeck), pressMarker(indexPlateAndNeck), alphaMarker(indexPlateAndNeck), tempMarker(indexPlateAndNeck));

	% Handling the mantle
	indexMantle 		     = mTypeMarker(:) == GLOB_TYPE_MANTLE;
	rhoMarker(indexMantle) 	     = calcRho_int(3300, betaMarker(indexMantle), pressMarker(indexMantle), alphaMarker(indexMantle), tempMarker(indexMantle));

	% Handling the air
	rhoMarker(mTypeMarker(:) == GLOB_TYPE_AIR) = 1;

end % function: calcRho


% This function calculates the eta of the markers
function etaMarker = calcEta(dEiiMarker, pressMarker, tempMarker, mTypeMarker)

	global GLOB_TYPE_AIR GLOB_TYPE_PLATE GLOB_TYPE_MANTLE GLOB_TYPE_NECKING GLOB_TYPE_INVALID GLOB_TYPE_SLAB;

	Nm = size(dEiiMarker, 1);

	%
	% Now we reconstruct the constant values
	%

	indexPlate = mTypeMarker(:) == GLOB_TYPE_PLATE;
	indexNecki = mTypeMarker(:) == GLOB_TYPE_NECKING;
	indexMantl = mTypeMarker(:) == GLOB_TYPE_MANTLE;
	indexAir   = mTypeMarker(:) == GLOB_TYPE_AIR;
	indexSlab  = mTypeMarker(:) == GLOB_TYPE_SLAB;
	indexDryOl = or(or(indexPlate, indexNecki), indexSlab);
	indexWetOl = indexMantl;
	%indexNonLin= or(or(indexNecki, or(indexMantl, indexPlate)), indexSlab);
	indexNonLin= or(indexDryOl, indexWetOl);


	NnonLinMarker = sum(indexNonLin);
	ETA_MIN = 1e+18 * ones(NnonLinMarker, 1);
	ETA_MAX = 1e+24 * ones(NnonLinMarker, 1);

	% Define the different valueArrays

	% Nvalue
	Nvalue     		= zeros(Nm, 1);
	Nvalue(indexDryOl) 	= 3.5;
	Nvalue(indexWetOl) 	= 4.0;

	% AD
	ADvalue			= zeros(Nm, 1);
	ADvalue(indexDryOl) 	= 2.5e-17;
	ADvalue(indexWetOl) 	= 2e-21;

	% Va
	VAvalue 		= zeros(Nm, 1);
	VAvalue(indexDryOl) 	= 8e-6;
	VAvalue(indexWetOl) 	= 4e-6;

	% EA
	EAvalue 		= zeros(Nm, 1);
	EAvalue(indexDryOl) 	= 532000;
	EAvalue(indexWetOl) 	= 471000;

	% sigmaMax
	sigmaYield 		= zeros(Nm, 1);
	sigmaYield(indexPlate) 	= 1e+8;
	sigmaYield(indexSlab) 	= 1e+8;
	sigmaYield(indexNecki) 	= 2e+7;
	sigmaYild(indexMantl) 	= 5e+7;

	% Output array
	etaMarker = zeros(Nm, 1);



	% Calculate the ductile eta
	etaDuctile = 0.5 * (1.0 ./ ( (ADvalue(indexNonLin)) .^ (1.0 ./ (Nvalue(indexNonLin))) ) ) .* ...
		( ( (dEiiMarker(indexNonLin)) .^ ((1.0 ./ (Nvalue(indexNonLin)) ) -1.0   ) ) ) .* ...
		exp( (EAvalue(indexNonLin) + pressMarker(indexNonLin) .* VAvalue(indexNonLin)) ./ (8.314 * tempMarker(indexNonLin) .* Nvalue(indexNonLin)) );

	% calculate the bulk strength, that is given by the sigmaYild
	etaBulk = sigmaYield(indexNonLin) ./ ( 2 * dEiiMarker(indexNonLin) );

	% Calculate the final value
	etaMarker(indexNonLin) = min(ETA_MAX, max(ETA_MIN, min(etaBulk, etaDuctile ) ) );


	% Set the eta for the air
	etaMarker(indexAir) = 1e+18;

end % function: calcEta





