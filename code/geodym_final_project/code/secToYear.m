function years = secToYear(s)
% Imput seconds and transform it to years
years = s ./ ( 3600 * 24 * 7 * 52 );
end
