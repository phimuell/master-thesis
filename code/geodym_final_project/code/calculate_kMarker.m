function kMarker = calculate_kMarker(mTypeMarker, tempMarker)
%{
	This function calculate the thermal conductivity for the markers.
	It needs the material type.
	For the air the fix value is used, for the other types, the formula is used
%}


% Import the global variables
global GLOB_TYPE_AIR GLOB_TYPE_PLATE GLOB_TYPE_MANTLE GLOB_TYPE_NECKING GLOB_TYPE_INVALID;

nonAirIndexis = mTypeMarker(:) ~= GLOB_TYPE_AIR;

% Generate output array
kMarker 	= zeros(size(mTypeMarker));

% calculate the kValue of the non air value
kMarker(nonAirIndexis) = 0.73 + 1293 ./ (tempMarker(nonAirIndexis) + 77);

% Set the value for the air
kMarker(mTypeMarker(:) == GLOB_TYPE_AIR) = 3000;



end % function: calculate_kMarker
