function [xMarkerNew, yMarkerNew] = moveMarkerRK4(vxNode, vyNode, xMarker, yMarker, Nx, Ny, dx, dy, dt)
%{
	This function moves the markers.
	It uses RK4, it does not evaluate the velocities anew, but it interpolate them differently.
	such as it has moved.


	RK4 does the following things
		x^{(k + 1)} = x^{(k)} + dt * v_{eff}

	And v_eff is defined by
		v_eff = \frac{1}{6} ( v_A + v_B + v_C + v_D)

	where v_{i} is the velocities of markers at point i.
	Point i can be found by:
		x_A = "Initial position"
		x_B = x_A + dt * v_A / 2
		x_C = x_A + v_B * dt / 2
		x_D = x_A + dt * v_c
%}

N_m = numel(xMarker);
xsize = dx * (Nx - 1);
ysize = dy * (Ny - 1);

if( (Ny + 1 ~= size(vyNode, 1)) || (Nx + 1 ~= size(vyNode, 2)))
	size(vyNode)
	size(vxNode)
	error('Dimension Missmatch');
end

if( size(vyNode) ~= size(vxNode))
	error('Dimesion missmatch, Velocities are not the smae size')
end

% calculating v_a
[vx_a, vy_a] = interpolGridToMarker(vxNode, vyNode, xMarker, yMarker, Nx, Ny, dx, dy);

% xaclulating pos_b
x_b = xMarker + 0.5 * dt * vx_a;
y_b = yMarker + 0.5 * dt * vy_a;

% calculating v_b
[vx_b, vy_b] = interpolGridToMarker(vxNode, vyNode, x_b, y_b, Nx, Ny, dx, dy);

% Caclulating x_c
x_c = xMarker + vx_b * dt * 0.5;
y_c = yMarker + vy_b * dt * 0.5;

% Calculating v_c
[vx_c, vy_c] = interpolGridToMarker(vxNode, vyNode, x_c, y_c, Nx, Ny, dx, dy);


% Calculating x_d
x_d = xMarker + dt * vx_c;
y_d = yMarker + dt * vy_c;

% calculating v_d
[vx_d, vy_d] = interpolGridToMarker(vxNode, vyNode, x_d, y_d, Nx, Ny, dx, dy);



% Calculating the effective velocity
vx_eff = (vx_a + 2 * vx_b + 2 * vx_c + vx_d) / 6;
vy_eff = (vy_a + 2 * vy_b + 2 * vy_c + vy_d) / 6;

% Now we can do the evolution
xMarkerNew = xMarker + dt * vx_eff;
yMarkerNew = yMarker + dt * vy_eff;






























end % function



