function deltaTemp = tempDiff_imp(RHOCP, TEMP, K, HEATING, dt, Nx, Ny, dx, dy)
%{
	This function calculates the temperature DIFFERENCE.
	Its imput arguments are:
		RHOCP 		Is the Volumentiric Heatcapacity
		TEMP 		The current Temperature, defined on the basic nodal point
		K 		Thermal Constant, that is not constant
		dt 		Timestep
		Nx 		Size in x direction
		Ny 		Size in y direction

	The boundary condituins must be fullfiled for the TEMP.
	The boundary conditions for the next timestep are applied directly inside the solver
%}
% Numbers of unkowns in the implicit equation
N = Nx * Ny;

% Timestep
a_factor 	= 4;

TEMP_TOP 	= 273;		% Temperature at the top
TEMP_BOTTOM 	= 1573; 	% Temperature at the bottom

% Using the global matrix
global LT;

% Allocating the global matrix
%LT 	= spalloc(N, N, 6 * N);		% System matrix
RT 	= zeros(N, 1);			% RHS of system


% Fill the system matrix
for  j = 1:1:Nx
	for i = 1:1:Ny

		% We use the FORTRAN indexing convention
		kT1 = Ny * (j - 2) + i; 	% (i, j-1) : The left point of the cross
		kT2 = Ny * (j - 1) + i - 1; 	% (i-1, j) : This is the top
		kT3 = Ny * (j - 1) + i;		% (i, j)   : The middle of the cross
		kT4 = Ny * (j - 1) + i + 1;	% (i+1, j) : The lower point of the cross
		kT5 = Ny * (j + 0) + i;		% (i, j+1) : The point to the right of the cross


		% Test if this is on the boundary. The boundary is simple, because it is given
		if(i == 1 || j == 1 || i == Ny || j == Nx)
			%
			% Boundary
			%

			% The left and right boundary they do not incluse the corner points
			% This is the left boundary
			if(i ~= 1 && i ~= Ny)
				if(j == 1)
					LT(kT3, kT3) 	= 1.0;
					LT(kT3, kT5) 	= -1.0;
					RT(kT3) 	=  0.0;
				end % if: left boundary

				% This is the righ boundary
				if(j == Nx)
					LT(kT3, kT3) 	= 1.0;
					LT(kT3, kT1) 	= -1.0;
					RT(kT3) 	=  0.0;
				end % if: right boundary
			end % if: not corners

			% Now comes the top and the left row
			% They does contains the corner points

			% Top row
			if(i == 1)
				LT(kT3, kT3) 	=  1.0;
				RT(kT3) 	=  TEMP_TOP;
			end % if: top row

			% Bottom row
			if(i == Ny)
				LT(kT3, kT3) 	=  1.0;
				RT(kT3) 	= TEMP_BOTTOM;
			end % if: bottom row
		else
			%
			% Interior points
			%

			% The k Values
			k_y_1 = 0.5 * ( K(i - 1, j    ) + K(i, j) );
			k_y_2 = 0.5 * ( K(i + 1, j    ) + K(i, j) );
			k_x_1 = 0.5 * ( K(i    , j - 1) + K(i, j) );
			k_x_2 = 0.5 * ( K(i    , j + 1) + K(i, j) );

			% Go trough all of the coefficient
			% EVTL HIER MINUSE

			% Coeficient of T1
			LT(kT3, kT1) = k_x_1 / (dx^2);

			% Coefficent of T2
			LT(kT3, kT2) = k_y_1 / (dy^2);

			% Coefficient of T3
			LT(kT3, kT3) = -(  RHOCP(i, j) / dt + (k_x_2 + k_x_1) / (dx^2) + (k_y_1 + k_y_2) / (dy^2)    );

			% Coefficient of T4
			LT(kT3, kT4) = k_y_2 / (dy^2);

			% Coefficient of T5
			LT(kT3, kT5) = k_x_2 / (dx^2);

			% Now the right hand side
			% Test is sign is correct
			RT(kT3) = -TEMP(i, j) * RHOCP(i, j) / dt - HEATING(i, j);

		end % End of boundary if
	end % for(i)
end % for(j)

%
% Now we solve the system matrix
%
ST = LT \ RT;


% This is the solution array
deltaTemp = zeros(Ny, Nx);

%
% Reload solution
% We retun the differences
%
for  j = 1:1:Nx
	for i = 1:1:Ny

		% We use the FORTRAN indexing convention
		kT3 = Ny * (j - 1) + i;		% (i, j)   : The middle of the cross

		% Calculate the difference from the new temperatures to the current temperatures
		deltaTemp(i, j) = ST(kT3) - TEMP(i, j);
	end
end

end % function(tempDiff_imp)


