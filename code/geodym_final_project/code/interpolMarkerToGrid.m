function [etaNode, rhoNode, kNode, TempNode, rhocpNode, radHeatNode, alphaNode, betaNode, mTypeNode, dEiiNode] = interpolMarkerToGrid(etaMarker, rhoMarker, kMarker, TempMarker, rhocpMarker, radHeatMarker, alphaMarker, betaMarker, mTypeMarker, xMarker, yMarker, Nx, Ny, dx, dy)
%{
	This function interpolates the values, that are caried by the marker to the grid.
	Please notice, that unlike in the 'inverse' of this function, the function that
	translate the values from the grid to the marker, we just have one grid here.

	This is because the grid points we need the properties at and the basic grid points
	are the same.

	etaNode and rhoNode are matrices of size Nx by Ny.

	TempMarker, is the temperature, that each marker carries.
	The oubdary conditions are applied during integrating.
	For interpolating we use the thermal average

	kMarker is the thermal conductivity that that reach marker has

	rhocpMarker is the volumetric heat capacity

	radHeatMarker is the radioactive heating capacity of the marker

	alphaMarker is the thermal conductivity of the marker

	This function also interpolates the marker property to the nodes.

	This function also interploates dEii to the node, the marker quantity is taken from the global variable

%}

% the number of markers
Nm = numel(etaMarker);

% For importing
global dEiiMarker_global;

TEMP_TOP 	= 273;		% Temperature at the top
TEMP_BOTTOM 	= 1573; 	% Temperature at the bottom

% Allocate the matrices for the properties,
% we will use the {rho, eta}Node matrix as the su matrix, in the end we will normalizing both of them
etaNode 	= zeros(Ny, Nx);
rhoNode 	= zeros(Ny, Nx);
rhocpNode 	= zeros(Ny, Nx);
kNode 		= zeros(Ny, Nx);
TempNode 	= zeros(Ny, Nx);
radHeatNode 	= zeros(Ny, Nx);
alphaNode 	= zeros(Ny, Nx);
betaNode 	= zeros(Ny, Nx);
mTypeNode 	= zeros(Ny, Nx);
dEiiNode 	= zeros(Ny, Nx);


% The normalizing constant is for each property the same.
WeightSum 	= zeros(Ny, Nx);	% The normalizing constant, for both the same

xsize = dx * (Nx - 1);
ysize = dy * (Ny - 1);


% Iterating throgh all the marker, to find the solution
for m = 1:1:Nm

	if(xMarker(m) > xsize | xMarker(m) < 0)
		xMarker(m)
		xsize
		error('Exceecing x');
	end
	if(yMarker(m) > ysize | yMarker(m) < 0)
		yMarker(m)
		error('Eyceecing y');
	end

	% Determing the indexes
	j = fix(xMarker(m) / dx) + 1;
	i = fix(yMarker(m) / dy) + 1;

	if(i == Ny)
		i
		m
		yMarker(m)
		error('i is to large');
	end

	if(j == Nx)
		i
		m
		error('j is to large');
	end

	% The distances between nodal point and marker
	delta_x_j = xMarker(m) - dx * (j - 1);
	delta_y_i = yMarker(m) - dy * (i - 1);

	% Compute the wights
	omega_i_j   = (1.0 - delta_x_j / dx) * (1.0 - delta_y_i / dy);
	omega_i1_j  = (1.0 - delta_x_j / dx) * (      delta_y_i / dy);
	omega_i1_j1 = (      delta_x_j / dx) * (      delta_y_i / dy);
	omega_i_j1  = (      delta_x_j / dx) * (1.0 - delta_y_i / dy);

	% Now we compute the contributiojn of each marker to the 4 grid points

	% Eta
	etaNode(i, j)         = etaNode(i, j)         + etaMarker(m) * omega_i_j;
	etaNode(i + 1, j)     = etaNode(i + 1, j)     + etaMarker(m) * omega_i1_j;
	etaNode(i + 1, j + 1) = etaNode(i + 1, j + 1) + etaMarker(m) * omega_i1_j1;
	etaNode(i, j + 1)     = etaNode(i, j + 1)     + etaMarker(m) * omega_i_j1;


	% dEii
	dEiiNode(i, j)         = dEiiNode(i, j)         + dEiiMarker_global(m) * omega_i_j;
	dEiiNode(i + 1, j)     = dEiiNode(i + 1, j)     + dEiiMarker_global(m) * omega_i1_j;
	dEiiNode(i + 1, j + 1) = dEiiNode(i + 1, j + 1) + dEiiMarker_global(m) * omega_i1_j1;
	dEiiNode(i, j + 1)     = dEiiNode(i, j + 1)     + dEiiMarker_global(m) * omega_i_j1;

	% Density
	rhoNode(i, j) 	      = rhoNode(i, j)         + rhoMarker(m) * omega_i_j;
	rhoNode(i + 1, j)     = rhoNode(i + 1, j)     + rhoMarker(m) * omega_i1_j;
	rhoNode(i + 1, j + 1) = rhoNode(i + 1, j + 1) + rhoMarker(m) * omega_i1_j1;
	rhoNode(i, j + 1)     = rhoNode(i, j + 1)     + rhoMarker(m) * omega_i_j1;

	% Thermal conductivity
	kNode(i, j) 		= kNode(i, j) 		+ kMarker(m) * omega_i_j;
	kNode(i + 1, j) 	= kNode(i + 1, j)     	+ kMarker(m) * omega_i1_j;
	kNode(i + 1, j + 1)	= kNode(i + 1, j + 1) 	+ kMarker(m) * omega_i1_j1;
	kNode(i, j + 1) 	= kNode(i, j + 1) 	+ kMarker(m) * omega_i_j1;

	% RhoCP
	rhocpNode(i, j) 	= rhocpNode(i, j) 	+ rhocpMarker(m) * omega_i_j;
	rhocpNode(i + 1, j) 	= rhocpNode(i + 1, j) 	+ rhocpMarker(m) * omega_i1_j;
	rhocpNode(i + 1, j + 1) = rhocpNode(i + 1, j + 1) + rhocpMarker(m) * omega_i1_j1;
	rhocpNode(i, j + 1) 	= rhocpNode(i, j + 1) 	+ rhocpMarker(m) * omega_i_j1;

	% Radioactive heating term
	radHeatNode(i, j) 	= radHeatNode(i, j) 	+ radHeatMarker(m) * omega_i_j;
	radHeatNode(i + 1, j) 	= radHeatNode(i + 1, j) 	+ radHeatMarker(m) * omega_i1_j;
	radHeatNode(i + 1, j + 1) = radHeatNode(i + 1, j + 1) + radHeatMarker(m) * omega_i1_j1;
	radHeatNode(i, j + 1) 	= radHeatNode(i, j + 1) 	+ radHeatMarker(m) * omega_i_j1;

	% Thermal expansion
	alphaNode(i, j) 	= alphaNode(i, j) 	+ alphaMarker(m) * omega_i_j;
	alphaNode(i + 1, j) 	= alphaNode(i + 1, j) 	+ alphaMarker(m) * omega_i1_j;
	alphaNode(i + 1, j + 1) = alphaNode(i + 1, j + 1) + alphaMarker(m) * omega_i1_j1;
	alphaNode(i, j + 1) 	= alphaNode(i, j + 1) 	+ alphaMarker(m) * omega_i_j1;

	% Compresibility
	betaNode(i, j) 		= betaNode(i, j) 		+ betaMarker(m) * omega_i_j;
	betaNode(i + 1, j) 	= betaNode(i + 1, j) 	+ betaMarker(m) * omega_i1_j;
	betaNode(i + 1, j + 1) 	= betaNode(i + 1, j + 1) + betaMarker(m) * omega_i1_j1;
	betaNode(i, j + 1) 	= betaNode(i, j + 1) 	+ betaMarker(m) * omega_i_j1;

	% mTypeNode
	mTypeNode(i, j) 		= mTypeNode(i, j) 		+ mTypeMarker(m) * omega_i_j;
	mTypeNode(i + 1, j) 	= mTypeNode(i + 1, j) 	+ mTypeMarker(m) * omega_i1_j;
	mTypeNode(i + 1, j + 1) 	= mTypeNode(i + 1, j + 1) + mTypeMarker(m) * omega_i1_j1;
	mTypeNode(i, j + 1) 	= mTypeNode(i, j + 1) 	+ mTypeMarker(m) * omega_i_j1;

	% Temperature, but with thermal average
	TempNode(i, j) 		= TempNode(i, j) 	+ rhocpMarker(m) * TempMarker(m) * omega_i_j;
	TempNode(i + 1, j) 	= TempNode(i + 1, j) 	+ rhocpMarker(m) * TempMarker(m) * omega_i1_j;
	TempNode(i + 1, j + 1) 	= TempNode(i + 1, j + 1) + rhocpMarker(m) * TempMarker(m) * omega_i1_j1;
	TempNode(i, j + 1) 	= TempNode(i, j + 1) 	+ rhocpMarker(m) * TempMarker(m) * omega_i_j1;

	% Update the normalizations constants
	WeightSum(i, j)         = WeightSum(i, j)         + omega_i_j;
	WeightSum(i + 1, j)     = WeightSum(i + 1, j)     + omega_i1_j;
	WeightSum(i + 1, j + 1) = WeightSum(i + 1, j + 1) + omega_i1_j1;
	WeightSum(i, j + 1)     = WeightSum(i, j + 1)     + omega_i_j1;


	if(abs(omega_i_j + omega_i1_j + omega_i_j1 + omega_i1_j1 - 1) > 100 * eps)
		error('Error in interpolation.');
	end

end % end for(m)

if false
	colormap('Jet');
	pcolor(0:(500 / (Nx- 1)):500, 0:(400 / (Ny - 1)):400, WeightSum);
	light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
end

if(sum( abs(WeightSum(:) < 100 * eps ) ) > 0)
	error('WeightSum has zeros. This impleies that some markers have left a cell.');
end

if(sum( abs(rhocpNode(:) < 100 * eps ) ) > 0)
	error('rhocpNode has zeros');
end

% The normalization in the temp is not the same as the other one, it is the sums of the rhocp
TempNode = TempNode ./ rhocpNode;

% Allpying the boudary condition to the temperature
% In contrast to the solver, we do not need a minus sign here, because it is naturaly introduced by the
% definition of the differential quotient.
TempNode(:, 1) 		= TempNode(:, 2);
TempNode(:, Nx) 	= TempNode(:, Nx - 1);
TempNode(1, :) 		= TEMP_TOP;	% Top
TempNode(end, :) 	= TEMP_BOTTOM; 	% Bottom

% Now normalize the values
etaNode = etaNode ./ WeightSum;
rhoNode = rhoNode ./ WeightSum;
kNode 	= kNode ./ WeightSum;
rhocpNode = rhocpNode ./ WeightSum;
alphaNode = alphaNode ./ WeightSum;
betaNode  = betaNode ./ WeightSum;
radHeatNode = radHeatNode ./ WeightSum;
mTypeNode = mTypeNode ./ WeightSum;
dEiiNode = dEiiNode ./ WeightSum;


if(sum(isnan(etaNode(:))) > 0 || sum(isnan(rhoNode(:))) > 0)
	error('Nan in node calculations');
end


end % end function(interpolateMarkerToGrid)








