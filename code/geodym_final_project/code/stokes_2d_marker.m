% Ercercisse 7
% Philip Müller
%

% Clearing memory and figure
clear all; clf;

% 1) Define Numerical Model
xsize = 500000; 		% Horizontal model size, [m]
ysize = 400000;		% Vertical model size, [m]
%ysize = 600000;		% Vertical model size, [m]
Nx = 101;		% Horizontal resolution
Ny = 81;		% Vertical resolution
%Ny = 121;		% Vertical resolution

N = Nx * Ny * 3;	% Total Numbers of unkown

dx = xsize / (Nx - 1);	% Horizontal grid step
dy = ysize / (Ny - 1);	% Vertical grid step

% Vextors
x = 0:dx:xsize;		% Horizontal coordinates of (basic) grid points, [m]
y = 0:dy:ysize;		% Vertical coordinates of (basic) gridpoints, [m]


% If this variable is set to a non zero value, then subgrid diffusion is enabled
USE_SUBGRID_DIFFUSION		= 1;


% A global variable, this makes thing faster.
% We can do, this because the structure of the matrix is fix.
% A _REAL_ language, would allow a better preallocation scheme!!!
global L;
L = spalloc(N, N, 19 * N);	% Matrix of coefficients (left Part)

% Temperatrure matrix is now also global.
% It is not needed but better for debugging
global LT;
NT      = Nx * Ny;			% Size of the system matrix
LT 	= spalloc(NT, NT, 6 * NT);	% System matrix


% This are "global variables" that are used as matterial type
global GLOB_TYPE_AIR GLOB_TYPE_PLATE GLOB_TYPE_MANTLE GLOB_TYPE_NECKING GLOB_TYPE_INVALID GLOB_TYPE_SLAB;

GLOB_TYPE_INVALID 	= 0;	% Material type for indicating invalid stuff
GLOB_TYPE_AIR 		= 1; 	% Material type for air
GLOB_TYPE_PLATE 	= 3;	% Material type for the plate, WITHOUT slab and necking
GLOB_TYPE_NECKING 	= 5;	% Material type for the necking area (ONLY)
GLOB_TYPE_MANTLE 	= 7;	% Material type for the mantle
GLOB_TYPE_SLAB 		= 9;	% Material type for the slab, WITHOUT the necking
% %%%%%%

% Garavaty potential in y direction
gravY 		= 9.81;		% Gravitational potential in v direction


%
% Define markers
%

Nx_m = (Nx - 1) * 5;	% Resolution of markers in horizonatal direction
Ny_m = (Ny - 1) * 5;	% Resolution of markers in vertical direction


% Numbers of marker
nMarker = Nx_m * Ny_m;	% 64 times more markers as gridpoints

% Arrays for marker
x_m = zeros(nMarker, 1);	% x-coordinates of the markers
y_m = zeros(nMarker, 1);	% y.coordinates of the markers

dxm_init = xsize / Nx_m;	% Initial marker grid step in horizontal direction, [m]
dym_init = ysize / Ny_m;	% Initial marker grid step in vertical direction, [m]
initial_marker_counter = 1;	% Counter used inside the loop
for  jm = 1:1:Nx_m
	for im = 1:1:Ny_m
		% Define marker coordinates
		x_m(initial_marker_counter) = dxm_init * 0.5 + (jm - 1) * dxm_init;  % + (rand - 0.5) * dxm_init;
		y_m(initial_marker_counter) = dym_init * 0.5 + (im - 1) * dym_init;  % + (rand - 0.5) * dym_init;A

		initial_marker_counter = initial_marker_counter + 1;
	end % for(im)
end % for(jm)


% This is a global variable, for passing arround the schearStressII
% At the marker
global dEiiMarker_global;
dEiiMarker_global = zeros(nMarker, 1);





%
% Timestep stuff
%


dt_default 		= yearToSec(400);	% The default timestep
timeSteps 		= 400;			% Number of timesteps
MIN_TIMESTEP		= 20; %yearToSec(1e-6);	% Minimum timestep (1e-6yr ~ 30s)
SAFTY_FACTOR 		= 1;			% A safty factor

% Restriction creteria
TempMaxChange 		= 20;		% Maximal temperature change during a timestep, K
MoveMaxChangeDef	= 0.4;		% Maximal coordinate change per timestep, in grid step fraction (default)
ADDAPTE_STEP 		= 60;		% How many steps will be performed to lineary go to the default movement (After TINY_STEPS)
TINY_STEPS 		= 30;		% How many tiny steps should be performed (Sincluding  the super restricted steps)
TINY_MOVE 		= 3e-9;		% This is the maximal movement of markers in cells, during the tiny steps
SUPER_RESTRICTED_STEPS 	= 12; 		% How many super restricted steps should be performed
FIRST_TIMESTEP_MAX 	= 20;		% Duration, seconds, of the first timestep steps (this steps are called 'super restiricted steps')




% Boundary conditions
% Set the value kindOfBC to:
%	3:	NoSlip Condition
%	1:	FreeSlip Condition
kindOfBC = 1;

% If set to a value other than 0 the program will output timings
PRINT_TIMINGS 	= 0;


% Calculate initial condition
[tempm, cpMarker, rhom, etam, km, radHeadtingm, alphaExpm, betaMarker, strengthMarker, mTypeMarker] = generate_initSlab(x_m, y_m);

% Calculate the rhocpm, A relict
rhocpm = cpMarker .* rhom;


% Subplot variables
SP_X = 3;
SP_Y = 4;

% This are all y that are larger than 60km
% Needed for visualization
indexYLargerThan60 = y(:) >= 60000;
indexYLessThan60   = y(:) <= 60000;
indexYLargerThan60_5 = indexYLargerThan60;
indexYLargerThan60_5_temp = indexYLargerThan60_5;
indexYLargerThan60_5_temp(1:5:end) = 0;
indexYLargerThan60_5 = and(indexYLargerThan60_5, not(indexYLargerThan60_5_temp));



dt = MIN_TIMESTEP;		% Timestep used for stabilization
numberOfTempRestriction = 0;	% How many times does the restriction kicks in

% Now we are doing the integration
GES = tic;
currentTime 	= 0;		% The current time of the simulation
for t = 1:1:timeSteps


	fprintf('\n\n================================ \n');
	fprintf('Step t = %i\n', t);

	T = tic;
	% interpolate the velocities from the markers to the grid
	[etaNode, rhoNode, kNode, TempNode, rhocpNode, radHeatNode, alphaNode, betaNode, mTypeNode, dEiiNode] = interpolMarkerToGrid(etam, rhom, km, tempm, rhocpm, radHeadtingm, alphaExpm, betaMarker, mTypeMarker, x_m, y_m, Nx, Ny, dx, dy);

	fprintf("RHO(41,51) = %d\n", rhoNode(41, 51))

	if(PRINT_TIMINGS ~= 0)
		fprintf('INTERPOL = %e\n', toc(T));
	end

	T = tic;

	% Calculating the velocities
	[vxNode, vyNode, P] = stockesSolver_2(etaNode, rhoNode, Nx, Ny, dx, dy, kindOfBC, dt, gravY);

	fprintf("VX_max = %e\n", max(abs(vxNode(:))))
	fprintf("dt = %e\n", dt)

	if(PRINT_TIMINGS ~= 0)
		fprintf('SOLVING = %e\n', toc(T));
	end


    	% here dt is still the last timestep. It is used by the stock solver to
    	% stabilize the system

	% Visualize
    	fig = figure(1);

    	T = tic;


	subplot(SP_X, SP_Y, 8);
	colormap('Jet');
	pcolor(x, y, mTypeNode);
	%light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	hold on;
	title('Material Type');
	hold off;


	subplot(SP_X, SP_Y, 12);
	colormap('Jet');
	pcolor(x, y, log10(dEiiNode));
	%light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	hold on;
	title('lg dEii (last)');
	hold off;

	subplot(SP_X, SP_Y, 1);
	%colormap('parula');
	colormap('Jet');
	pcolor(x, y(indexYLargerThan60), rhoNode(indexYLargerThan60, :));
	%pcolor(x, y, rhoNode);
	%light;
	hold on;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	q = quiver(x(1:5:Nx), y(indexYLargerThan60_5), vxNode(indexYLargerThan60_5, 1:5:Nx), vyNode(indexYLargerThan60_5, 1:5:Nx), 'k' );
	title('Density >60km');
	hold off;

	subplot(SP_X, SP_Y, 2);
	colormap('Jet');
	% I choosed ln to see better difference
	pcolor(x, y(indexYLessThan60), log2(rhoNode(indexYLessThan60, :)));
	%light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	title('lb Density <60km');
	hold off;


	subplot(SP_X, SP_Y, 6);
	%colormap('parula');
	colormap('Jet');
	pcolor(x, y, log10(etaNode));
	%light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	title('lg Viscosoity');
	hold off;


	subplot(SP_X, SP_Y, 3);
	colormap('Jet');
	pcolor(x, y, vxNode);
	%light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	hold on;
	quiver(x(1:5:Nx), y(1:5:Ny), vxNode(1:5:Ny, 1:5:Nx), vyNode(1:5:Ny, 1:5:Nx), 'k' );
	title('Clormap of Vx');
	hold off;
	%pause(0.1);

	subplot(SP_X, SP_Y, 4);
	colormap('Jet');
	pcolor(x, y, vyNode);
	%light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	hold on;
	quiver(x(1:5:Nx), y(1:5:Ny), vxNode(1:5:Ny, 1:5:Nx), vyNode(1:5:Ny, 1:5:Nx), 'k' );
	title('Clormap of Vy');
	hold off;

	subplot(SP_X, SP_Y, 5);
	colormap('Jet');
	pcolor(x, y, log10(P));
	%light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	hold on;
	title('lg Presure');
	hold off;


	if(PRINT_TIMINGS ~= 0)
		fprintf('PLOTING = %e\n', toc(T));
	end



	T = tic;

	%% Maps the velocities to the pressure gird
	[vxExtGrid, vyExtGrid] = interpolGridToPressurePointVel(vxNode, vyNode, Nx, Ny, dx, dy);


	if(PRINT_TIMINGS ~= 0)
		fprintf('VEL_TO_PRES = %e\n', toc(T));
	end

	T = tic;

	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	% Here we overwrite the dt value from the previous run with the default
	% value!
	%

    	dt = dt_default;
	% calculate the timestep


	% Here we use a trick, since we have to bootstrap the solution, we use a very small timestep for the first few iterations.
	% we do this for a fixed amount of steps. Atfter that steps we use the regular one
	MoveMaxChange = 0;
	if(t <= TINY_STEPS)
		fprintf('We are in the tiny-step phase of the simulation.\n');

		MoveMaxChange = TINY_MOVE;
	else
		if(t <= (TINY_STEPS + ADDAPTE_STEP) )
			% Now we make a slowly addaption towards the final time
			fprintf('We are in the addaptive time step phase of the simulation.\n');

			MoveMaxChange = (MoveMaxChangeDef - TINY_MOVE) / ADDAPTE_STEP * (t - TINY_STEPS) + TINY_MOVE;
			MoveMaxChange = min(MoveMaxChange, MoveMaxChangeDef);
		else
			% Normal we chose the normal way
			MoveMaxChange = MoveMaxChangeDef;
		end % if: Addapte
	end % if: tinystep

	% Get the highes velocity at the node (x and y)
	% This could be incooperated into the solver, for more performance
	maxvx=max((abs(vxNode(:))));
	maxvy=max((abs(vyNode(:))));


	fprintf('Max allowed move change:    %8.6f\n', MoveMaxChange);
	fprintf('Move change (unrestricted): %8.6f\n', max(dt * maxvy / dy, dt * maxvx / dx));

	% Test the x constaint
	if(dt * maxvx > MoveMaxChange * dx)
		dt_OLD__ = dt;
		dt= MoveMaxChange * dx / maxvx * SAFTY_FACTOR;
		fprintf('Vx: Restricted timestep from %10.8f yr  to %10.8f yr\n', secToYear(dt_OLD__), secToYear(dt) );
	end

	% test the y constraint
	if(dt * maxvy > MoveMaxChange * dy)
		dt_OLD__ = dt;
		dt= MoveMaxChange * dy / maxvy * SAFTY_FACTOR;
		fprintf('Vy: Restricted timestep from %10.8f yr  to %10.8f yr\n', secToYear(dt_OLD__), secToYear(dt) );
	end

	fprintf('Max move change: %8.7f\n', max(dt * maxvy / dy, dt * maxvx / dx));

	%
	% The minimu time shall not be zero
	if(dt < MIN_TIMESTEP)
		fprintf('Timestep, %e yr, is smaller than minimum, set it to minimum!\n', secToYear(dt));
		dt = MIN_TIMESTEP;
	end


	% For the first few timesteps we enforece a very small maximal timestep.
	if(t <= SUPER_RESTRICTED_STEPS)
		% We are in the beginning of the simulation
		fprintf('We are in the super restricted phase of the simulation.\n');

		if(FIRST_TIMESTEP_MAX < dt)
			% The timestep we currently have, is bigger than than the current maximal timestep, so we
			% reduce it
			fprintf('Initial restriction of the timestep. Timestep before was %e seconds.\n', dt);
			dt = FIRST_TIMESTEP_MAX;
		end
	end


	if(PRINT_TIMINGS ~= 0)
		fprintf('RESTRIC TIME = %e\n', toc(T));
	end

	%
	% Temperature iteration
	T = tic;

	% Calculate the heating term
	[adiaHeatNode, radHeatNode, shearHeatNode] = calcHeatingTerms(radHeatNode, alphaNode, rhoNode, gravY, TempNode, vxNode, vyNode, etaNode, dx, dy);

	%Summ up all the heating terms
	heatingTerms = adiaHeatNode + radHeatNode + shearHeatNode;

	% Now we solve the Temperature equation
	deltaTemp = tempDiff_imp(rhocpNode, TempNode, kNode, heatingTerms, dt, Nx, Ny, dx, dy);

	% Apply thermal timesteping creteria
	maxDTcurrent = max(abs(deltaTemp(:)));
	fprintf('Max temp change: %6.2f K\n', maxDTcurrent);


	% Test if the requierment is meet
	if(maxDTcurrent > TempMaxChange)
		dt_OLD__ = dt;

		% We assume proportional dependency of the temperature and the timestep
		dt = dt * TempMaxChange / maxDTcurrent * SAFTY_FACTOR;

		fprintf('Temp: Restrict timestep. Max change is %f\n', maxDTcurrent);
		fprintf('       from %10.8f yr  ->  %10.8f yr\n', secToYear(dt_OLD__), secToYear(dt));

		% Count how many times we have to restrict
		numberOfTempRestriction = numberOfTempRestriction + 1;

		% try new solution with new time
		% We assume that the maximal change in temperature is then less the maximal we allow
		% But we do not check this again.
		deltaTemp = tempDiff_imp(rhocpNode, TempNode, kNode, heatingTerms, dt, Nx, Ny, dx, dy);
	end


	if(PRINT_TIMINGS ~= 0)
		fprintf('TEMP = %e\n', toc(T));
	end



	% Plot the temperature
	subplot(SP_X, SP_Y, 7);
	colormap('Jet');
	pcolor(x, y, deltaTemp + TempNode);
	%light;
	axis ij image
	%lighting phong;
	shading interp
	colorbar
	hold on;
	title('Temperature');
	hold off;



	% Plot the adiabatic heat
	subplot(SP_X, SP_Y, 9);
	colormap('Jet');
	pcolor(x, y, adiaHeatNode);
	%light;
	axis ij image
	lighting phong;
	shading interp
	colorbar
	hold on;
	title('Adiabatic Heat');
	hold off;


	% Plot the sear heat
	subplot(SP_X, SP_Y, 10);
	colormap('Jet');
	pcolor(x(2:end-1), y(2:end-1), log10(shearHeatNode(2:end-1, 2:end-1)));
	%light;
	axis ij image
	%lighting phong;
	shading interp
	colorbar
	hold on;
	title('lg Shear Heat');
	hold off;


	% Plot the sear heat
	subplot(SP_X, SP_Y, 11);
	colormap('Jet');
	pcolor(x(2:end-1), y(2:end-1), shearHeatNode(2:end-1, 2:end-1));
	%light;
	axis ij image
	%lighting phong;
	shading interp
	colorbar
	hold on;
	title('Shear Heat');
	hold off;

	suptitle(sprintf('Timestep %i, t = %e yr, dt = %e yr', t, secToYear(currentTime), secToYear(dt) ));


	% Print the thing out
	aaa(1,1)=vxNode(10,10);
	aaa(2,1)=vyNode(10,10);
	aaa(3,1)=P(10,10);
	aaa(4,1)=TempNode(15, 15)+deltaTemp(15, 15);

	% Get the min and maximum of all marker that are in the necking area and are below 105km
	minNeck = min(tempm(and(mTypeMarker(:) == GLOB_TYPE_NECKING, y_m(:) >= 105000)));
	maxNeck = max(tempm(and(mTypeMarker(:) == GLOB_TYPE_NECKING, y_m(:) >= 105000)));
	meanNeck = mean(tempm(mTypeMarker(:) == GLOB_TYPE_NECKING));

	fprintf('Current time = %13.6f yr,  timestep = %13.8f yr\n', secToYear(currentTime), secToYear(dt));
	fprintf('\n');

	fprintf('Some reference values:\n');
	fprintf('vx(10, 10) = %e\n', aaa(1, 1));
	fprintf('vy(10, 10) = %e\n', aaa(2, 1));
	fprintf('p(10, 10)  = %e\n', aaa(3, 1));
	fprintf('Tdt(15,15) = %e\n', aaa(4, 1));
	fprintf('Neck-Temp (min):  %7.2f\n', minNeck);
	fprintf('Neck-Temp (max):  %7.2f\n', maxNeck);
	fprintf('Neck-Temp (mean): %7.2f\n', meanNeck);
	fprintf('\n\n');

	T = tic;

	% Now interpolate the temperature difference back to the marker
	% We does interpolate it directly in the first step
	if(t == 1)
		% First timestep
		% The old temperature are zeros and the delta is the found values
		tempm = tempBackToMarker(deltaTemp + TempNode, zeros(size(tempm)), x_m, y_m, Nx, Ny, dx, dy);

	else
		% none first timestep

		%
		% Subgrid diffusion
		if(USE_SUBGRID_DIFFUSION ~= 0)
			[tempm, deltaTemp] = subGridDiffusion(deltaTemp, TempNode, tempm, rhocpm, km, x_m, y_m, Nx, Ny, dx, dy, dt);
		end

		% Interpolating the remaining temperature to marker
		tempm = tempBackToMarker(deltaTemp, tempm, x_m, y_m, Nx, Ny, dx, dy);
	end

	% Add the time
	currentTime = currentTime + dt;


	if(PRINT_TIMINGS ~= 0)
		fprintf('HANDLING TEMP = %e\n', toc(T));
	end

	T = tic;


	%
	% Non linear rheology
	%

	% Here we implement the non linear rheology
	% Meaning before we move the markers, we calculate the new properties.
	[rhom, etam, km] = apply_nonLinRheology(x_m, y_m, vxNode, vyNode, etaNode, dx, dy, alphaExpm, betaMarker, mTypeMarker, tempm, P);

	% Updating RHOCP
	rhocpm = rhom .* cpMarker;



	if(PRINT_TIMINGS ~= 0)
		fprintf('RHEOLOGY = %e\n', toc(T));
	end



	%
	% MOVEMENT
	%

	T = tic;

	% Integrating the movement
	[x_m, y_m] = moveMarkerRK4(vxExtGrid, vyExtGrid, x_m, y_m, Nx, Ny, dx, dy, dt);


	if(PRINT_TIMINGS ~= 0)
		fprintf('MOVEMENT = %e\n', toc(T));
	end


	pause(0.1);



end % end for(t): Time intergration


TotCompTime = toc(GES);

fprintf('\n\n[============= END ====================]\n');
fprintf('Simulation-Time:   %f yr\n', secToYear(currentTime));
fprintf('Simulation-Steps:  %d\n', timeSteps);
fprintf('Compute-Time:      %f s\n', TotCompTime);
fprintf('TimePerIt:         %f s\n', TotCompTime / timeSteps);





