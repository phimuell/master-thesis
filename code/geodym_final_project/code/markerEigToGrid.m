function markerNode = markerEigToGrid(markerMarker, xMarker, yMarker, Nx, Ny, dx, dy)

% the number of markers
Nm = numel(markerMarker);


% Allocate the matrices for the properties,
% we will use the {rho, eta}Node matrix as the su matrix, in the end we will normalizing both of them
markerNode 	= zeros(Ny, Nx);

% The normalizing constant is for each property the same.
WeightSum 	= zeros(Ny, Nx);	% The normalizing constant, for both the same

xsize = dx * (Nx - 1);
ysize = dy * (Ny - 1);


% Iterating throgh all the marker, to find the solution
for m = 1:1:Nm

	if(xMarker(m) > xsize | xMarker(m) < 0)
		xMarker(m)
		xsize
		error('Exceecing x');
	end
	if(yMarker(m) > ysize | yMarker(m) < 0)
		yMarker(m)
		error('Eyceecing y');
	end

	% Determing the indexes
	j = fix(xMarker(m) / dx) + 1;
	i = fix(yMarker(m) / dy) + 1;

	if(i == Ny)
		i
		m
		yMarker(m)
		error('i is to large');
	end

	if(j == Nx)
		i
		m
		error('j is to large');
	end

	% The distances between nodal point and marker
	delta_x_j = xMarker(m) - dx * (j - 1);
	delta_y_i = yMarker(m) - dy * (i - 1);

	% Compute the wights
	omega_i_j   = (1.0 - delta_x_j / dx) * (1.0 - delta_y_i / dy);
	omega_i1_j  = (1.0 - delta_x_j / dx) * (      delta_y_i / dy);
	omega_i1_j1 = (      delta_x_j / dx) * (      delta_y_i / dy);
	omega_i_j1  = (      delta_x_j / dx) * (1.0 - delta_y_i / dy);

	% Now we compute the contributiojn of each marker to the 4 grid points

	% Eta
	markerNode(i, j)         = markerNode(i, j)         + markerMarker(m) * omega_i_j;
	markerNode(i + 1, j)     = markerNode(i + 1, j)     + markerMarker(m) * omega_i1_j;
	markerNode(i + 1, j + 1) = markerNode(i + 1, j + 1) + markerMarker(m) * omega_i1_j1;
	markerNode(i, j + 1)     = markerNode(i, j + 1)     + markerMarker(m) * omega_i_j1;

	% Update the normalizations constants
	WeightSum(i, j)         = WeightSum(i, j)         + omega_i_j;
	WeightSum(i + 1, j)     = WeightSum(i + 1, j)     + omega_i1_j;
	WeightSum(i + 1, j + 1) = WeightSum(i + 1, j + 1) + omega_i1_j1;
	WeightSum(i, j + 1)     = WeightSum(i, j + 1)     + omega_i_j1;


	if(abs(omega_i_j + omega_i1_j + omega_i_j1 + omega_i1_j1 - 1) > 100 * eps)
		error('Error in interpolation.');
	end

end % end for(m)

if(sum(isnan(markerNode(:))) > 0 )
	error('Nan in node calculations, before division');
end


% Now normalize the values
markerNode = markerNode ./ WeightSum;

if(sum(isnan(markerNode(:))) > 0)
	error('Nan in node calculations');
end


end % end function(interpolateMarkerToGrid)








