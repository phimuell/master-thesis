# Info
This is the code folder. It contains all code that is
written for this project and is not related to analyzing.


## Sub Folders

## GEODYM_FINAL_PROJECT
This folder contains the code that was written for the course “Geodynamic modelling”.
You can consider it as a prototype of the code.

## SCORPIA_CPP
This folder contains the main work of this thesis, it contains the implementation of
EGD and CORELLO, as well as a version of PGL.

