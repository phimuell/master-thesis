#!/bin/bash
#
# This file starts all simulation it can find. This is, it will scan the current folder
# for .ini files and then start them. However it is also possible to start a specific
# selection  of controle files. All arguments that comes after the flags are treated
# as controle files that should be started, they are assumed to be in the same folder.
#
#   This script supports the following syntax:
#	./euler_starter.sh [LONG] [FORCE] [NAME id] [{FILE1} [{FILE2} [... [{FILEN}]]]]
#
# The script supports some flags that influences its behaviour.
#  It is important that the order must be the same as it is listed here.
#
#   > LONG:	Will request 48 hours runtime istead of 20 hours.
#   > FORCE:	Will start Coreello with the force flag, which deletes HDF5 files.
#   > NAME:	Instructs to use the next argument as description of the run, will influence the dump location.
#
############################################
#

if [ -z "${SCRATCH}" ]
then
	echo "The scaratch variable is empty."
	exit 20
fi


# This is the current data
CURR_DATE="$(date +%Y-%m-%d)"

# This is the job name is composed of the data and if given the name
JOB_NAME="$(date +%Y-%m-%d_%H-%M)"

# This is the folder that is scanned
SCAN_FOLDER="${PWD}"

# This is the original PWD folder
OPWD="${PWD}"

# This is the folder where the HDF5 files are stored, we use some kind of organization.
#  That uses the date to structure it further
SCRATCH_FOLDER="${SCRATCH}/${CURR_DATE}"

# This is the number of processors that are needed
N_CPU=1

# This is the force flag for corello, it is empty
FORCE_FLAG=""

# This is the amount of memory that is needed in megabytes
MEM_NEED=8192

# This is the runtime that the simulation needs, in HH:MM format
RUN_TIME="20:00"

# The termination grace time, in HH:MM
TERM_GRACE="20"

# Access the scratch directory
cat "${SCRATCH}/__USAGE_RULES__"  2>&1 > /dev/null

#####
#	Test for FLAGs

# Test if the LONG flag was given
if [ $# -ne 0 ] && [ "$1" = "LONG" ]
then
	echo "The LONG flag was given."
	RUN_TIME="48:00"
	shift
fi

# Test for the FORCE flag
if [ $# -ne 0 ] && [ "$1" = "FORCE" ]
then
	echo "The FORCE flag was given."
	FORCE_FLAG="--force"
	sleep 3
	shift
fi

# test for the name flag
if [ $# -ne 0 ] && [ "$1" = "NAME" ]
then
	shift   # to get the id

	if [ $# -eq 0 ]
	then
		echo "The NAME falg was encountered, but no additional file name was found"
		exit 40
	fi

	# Get the name
	RUN_NAME="$(echo -n "$1" | tr '[:space:]\.' '_')"
	shift   # shift the name away

	# Recompose the storage
	SCRATCH_FOLDER="${SCRATCH}/${CURR_DATE}__${RUN_NAME}"

	# Update the job name
	JOB_NAME="${JOB_NAME}__${RUN_NAME}"

	echo "NAME flag was found storage path was modified, new path is:"
	echo -e "\t\"${SCRATCH_FOLDER}\""
fi

# 	End of FLAG testing
#####


# Create the folder if it does not exists
if [ ! -e "${SCRATCH_FOLDER}" ]
then
	mkdir -p "${SCRATCH_FOLDER}"

	if [ $? -ne 0 ]
	then
		echo "Failed to create the scratch folder \"${SCRATCH_FOLDER}\""
		exit 10
	fi
fi

# Test if the folder realy exists and if it is a folder
if [ ! -d "${SCRATCH_FOLDER}" ]
then
	echo "The path \"${SCRATCH_FOLDER}\" exists, but is not a folder."
	exit 11
fi


# Test if we have to scan or to read in slected indexes
if [ $# -ne 0 ]
then
	while [ $# -ne 0 ]
	do
		# Thsi is teh file we are starting
		F="${1}"

		if [ ! -f "${F}" ]
		then
			echo "Could not find file \"${F}\"."
			exit 30
		fi

		echo "Submitting file \"${F}\""

		bsub -J "${JOB_NAME}" -W "${RUN_TIME}" -wa USR2 -wt "${TERM_GRACE}" -R "rusage[mem=${MEM_NEED}]" -r -N -B -n ${N_CPU} -u ${USER} -o "${F}.out" ./egd_progs/corello/Corello -c "${SCAN_FOLDER}/${F}" ${FORCE_FLAG} --dir "${SCRATCH_FOLDER}"

		if [ $? -ne 0 ]
		then
			echo "While deploying \"${F}\" lsf reproted an error."
			exit 1
		fi

		sleep 1s

		# Consume the current argument
		shift
	done

	# End:	start by argument
	#######################
else
	ls -1 "${SCAN_FOLDER}" | grep -iP '\.ini$' | while read F
	do
		echo "Submitting file \"${F}\""

		bsub -J "${JOB_NAME}" -W "${RUN_TIME}" -wa USR2 -wt "${TERM_GRACE}" -R "rusage[mem=${MEM_NEED}]" -r -N -B -n ${N_CPU} -u ${USER} -o "${OPWD}/${F}.out" ./egd_progs/corello/Corello -c "${SCAN_FOLDER}/${F}" ${FORCE_FLAG} --dir "${SCRATCH_FOLDER}"

		if [ $? -ne 0 ]
		then
			echo "While deploying \"${F}\" lsf reproted an error."
			exit 1
		fi

		sleep 1s
	done

	# End: start by scanning
	#############
fi

echo ""
echo "started all jobs"

echo ""
echo""

echo "Job list is:"
bjobs

exit 0


