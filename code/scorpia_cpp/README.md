# Info
This folder contains the rewritten code for the master thesis.
It is organized in several folder that separates the code into logical blocks.

Some parts of the code are reused parts from other projects I did.
The building of the code is done by means of CMake, which must be installed to compile the code.
Other requirements are.
	- Boost, no specific version, but it should be relatively new.
	- Eigen, no bleeding edge is needed.
	- CMake, this must be a fairly new version, this is needed for using the building process.
	- PGL, this is a library I once wrote and reuse since then, it is contained in this project.
	- HDF5, this is needed for the dumping.
	- gcc, is the only supported compiler.

## COMPILING
We will now describe how the code can be compiled. We use CMake to compile it.
The steps to compile the code are as follows:
	1) Create a build folder:
		mkdir build
	     Any folder that starts with build is ignored by git.
	     And enter the folder.
	2) Call CMake:
		cmake .. [OPTIONS]
	     for the Options see below.
	3) call make
		make [-j [N]]
	     This will build the actual code. N is the number of parallel builds you request.
	     Omitting N will use as many as possible.

### Building on Leonhard
In order to build the project on Leonhard some tricks are needed.

#### Modules
Beside the standard module, the following modules must be loaded
	- gcc/8.2.0
	- openmpi/4.0.1
	- hdf5/1.10.5
	- cmake/3.13.4
	- boost/1.69.0

#### Eigen
Eigen is needed internally, however the one that is provided does not work correctly, I do not
know why. Thus a working version is provided. Unpack the file myEigen.tar.gz into your home
directory, it will contain a folder of the same name. This folder contains a working version of
Eigen.

#### CMake Options
A working invokation of CMake is:
	cmake .. -DCMAKE_BUILD_TYPE=Release 	\
		 -DDUMP_PLOT_PROPERTIES=OFF 	\
		 -DIGNORE_NEG_PRESSURE=ON 	\
		 -DPRINT_PRUNING=ON 		\
		 -DDISABLE_PRUGING=OFF  	\
		 -DNO_PRESSURE_SCALING=OFF 	\
		 -DNO_TEST_PROGS=YES 		\
		 -DPRINT_NEG_PRESSURE=OFF   	\
		 -DUSE_MPI=ON 			\
		 -DCORELLO_INIT_RNG=ON 		\
		 -DFORCE_EIGEN_FB=ON


### Binaries
We assume that you are in the build folder. After the make has built the code,
several folders are created inside the build folder.

The CORELLO binary can be invoked by
	./egd_prog/corello/Corello


## Main Components
Conceptually the code is split into two parts.

### EGD
EGD is a recursive acronym for 'EGD Geo Dynamic', but it was once just a normal acronym for 'Ellert Geo Dynamic'.
Ellert is a minor character from the science fiction series "Perry Rhodan".
If you do not like that you can also read the "E" as ETH.

#### CORELLO
EGD is just a libarary, the actual simulation is implemented in CORELLO, which is located in the EGD_PROG folder.


### PGL
This is a library that I wrote for my bachelor thesis. Since them I use it when I do a larger
project. It mainly contains some helper code that implements some common task.
Like a better assert.

#### NDEBUG
PGL also overrides the NDEBUG macro. Currently it is not possible to disable this behaviour.
This means that all asserts and debug code are activated.


## Folders
Here is a list and descriptions of the folders.
Many of them contains more README files that gives more details.

### PGL
This is a library that I wrote for my bachelor thesis and use since then.
During its evolution it has become a bit messy. Also we will not use its
main purpose, the graph. This is stone age code.


### EGD
This folder contains all the code that belongs to EGD and was actually written for this task.
This folder will contain code that interests you the most.


### EGD_TESTS
This folder contains small programs to test small functions that belongs to the EGD library.
These programs are intended to test if they work as expected.
All tests have been passed, but some details were changed, which renders some of them incompatible
with the current version.
I know it is not very professional, but this is life.


### EGD_PROGS
This folder contains programs that uses EGD to simulate something.
It is basically the actual program that was written for the thesis.


### SCRIPTS
This folder contains a script that is able to issue many jobs on LEONHARD at once.


### BUGS
This folder contains information about bugs.
For more information use "git log -p -- FILE"

