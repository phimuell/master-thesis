/**
 * \brief	This file contains the functions that controles the dumping of a full marker property.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


void
egd_dumperFile_t::dumpGridProperty(
	const GridProperty_t& 	 	gProp,
	const GroupPath_t& 		groupPath_,
	const bool 			absPath)
{
	//load the property index
	const egd_propertyIndex_t prop = gProp.getPropIdx();
		pgl_assert(prop.isValid(),
			   this->isOpen() );

	if(groupPath_.empty() )
	{
		throw PGL_EXCEPT_InvArg("The passed save path is empty.");
	};

	//Create the save path
	std::string savePath_ = groupPath_;
	if(absPath == false)
	{
		if(savePath_.back() != '/')
		{
			savePath_ += "/" + prop.print();
		}
		else
		{
			savePath_ += prop.print();
		};
	}; //End if: no abs path
	const std::string savePath = std::move(savePath_);


	/*
	 * We simply call the internal function.
	 *
	 * There is a quite tricky twist here.
	 * HDF5 uses the C memory order, aka row major. However
	 * Eigen uses column major order for compability with
	 * FORTRAN. So reading in a pointer, the data pointer
	 * results in an implicit transponation of the matrix.
	 * So we have also to swap the number of rows and
	 * number of columns.
	 */
	this->priv_storeData(gProp.data(),
			     gProp.cols(), gProp.rows(),	//Swaped order.
			     savePath                   );

	/*
	 * Now we annotate the property
	 */
	this->priv_annotateGridProp(gProp, savePath);

	return;
}; //End: dump a grid property.



bool
egd_dumperFile_t::dumpGridContainer(
	const GridContainer_t& 		grid,
	const GroupPath_t& 		BASE_PATH)
{
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The file is not open.");
	};
	if(BASE_PATH.empty() )
	{
		throw PGL_EXCEPT_RUNTIME("The BASE_PATH is empty.");
	};
	if(m_stateDumpIdx < 0)
	{
		throw PGL_EXCEPT_InvArg("The state index is negative it is " + std::to_string(m_stateDumpIdx));
	};

	bool createdNewGeometryVersion = false;


	/*
	 * Test if a new version of the grid is present.
	 * If so then create it.
	 */
	if(grid.isNewVersion() == true)
	{
			pgl_assert(grid.getGeometry().isNewVersion());
		this->priv_createNewGridGeometryVersion(grid.getGeometry() );
		createdNewGeometryVersion = true;
	}; //End if: new version


	//Create the save path
	std::string savePath_ = BASE_PATH;
	if(savePath_.back() != '/')
	{
		savePath_ += "/";
	}
	const std::string savePath_SAVE = std::move(savePath_) + "grid/";

	Index_t nDump = 0;	//Counts how many indexes we have dumped

	/*
	 * Now we iterate through all the grid properties and dumps them
	 */
	for(const auto& it : grid)
	{
		const PropIdx_t pIdx = it.first;	//load the grid property
			pgl_assert(pIdx.isValid() );

		//
		//Test if we have to dump

		if(this->m_gridPropIgn.count(pIdx) != 0) //Test if in ignore list
		{
			continue;	//it is in the ignore list so we skip it
		};//End: ignore list

		if((pIdx.isPlotProperty()   == false) &&    //not a plot property
		   (this->m_onlyRepGridDump == true )   )   //only representant are requested
		{
			if(pIdx.isRepresentant() == false)
			{
				continue;	//Not a representant, so we ignore it
			};//End if: is not a representant
		}; //End if: not a plot property

		//
		//Here we dump it

		//Call the property dump function, naming is handled by it
		this->dumpGridProperty(it.second, savePath_SAVE);

		nDump += 1; //Increment the counter
	}; //End for(it):

	if(nDump <= 0)
	{
		throw PGL_EXCEPT_RUNTIME("There was not dumping event for the grid container, nDump was " + std::to_string(nDump));
	};


	/*
	 * Create an attribute that contains the version of the grid geometry.
	 */
	{
		const Index_t currGeoVersion = this->m_geoVersion;

		this->priv_attacheAttribute(
				&currGeoVersion,
				BASE_PATH,
				"gVersion");
	};

	return createdNewGeometryVersion;
}; //End: dump grid container




/*
 * =================
 * Annotating
 */
void
egd_dumperFile_t::priv_annotateGridProp(
	const GridProperty_t& 	gProp,
	const GroupPath_t& 	annotateTo)
{
	pgl_assert(gProp.hasValidGridType(),
		   annotateTo.size() > 0);

	this->priv_attacheGridSize(gProp          , annotateTo);
	this->priv_attacheGridType(gProp.getType(), annotateTo);

	return;
}; // End annotate: grid


void
egd_dumperFile_t::priv_attacheGridType(
	const eGridType 	gType,
	const GroupPath_t& 	attacheTo)
{
	pgl_assert(isValidGridType(gType),
		   attacheTo.size() > 0);

	//load the type of the grid
	const std::string loadType = this->priv_gTypeStr(gType);

	//load the paths to X and Y
	GroupPath_t X, Y;
	std::tie(X, Y) = this->priv_getCurrentGridGeometry(gType);


	/*
	 * Now write the attributes
	 */
	this->priv_attacheAttribute(loadType,	//Write a clear name
		attacheTo, "gType");
	this->priv_attacheAttribute(X,
		attacheTo, "X"    );
	this->priv_attacheAttribute(Y,
		attacheTo, "Y"    );

	return;
}; //End: attache grid type


void
egd_dumperFile_t::priv_attacheGridSize(
	const GridProperty_t& 	gProp,
	const GroupPath_t& 	attacheTo)
{
	pgl_assert(gProp.hasValidGridType(),
		   attacheTo.size() > 0);

	//load the size
	const Index_t Nx = gProp.Nx();
	const Index_t Ny = gProp.Ny();

	//
	//Write the attributes
	this->priv_attacheAttribute(&Ny, attacheTo, "Ny");
	this->priv_attacheAttribute(&Nx, attacheTo, "Nx");

	return;
}; //End write fize of basic nodal grid



PGL_NS_END(egd)


