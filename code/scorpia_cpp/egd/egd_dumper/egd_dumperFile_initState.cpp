/**
 * \brief	This file contains the functions to record the very initial state.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


void
egd_dumperFile_t::dumpInit(
	const GridContainer_t& 		grid,
	const MarkerCollection_t& 	mColl)
{
	const GridGeometry_t& gridGeo = grid.getGeometry();

	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("Can not dump an grid that is not set.");
	};
	if(mColl.nMarkers() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The marker collection is empty.");
	};
	if(m_stateDumpIdx != -1)
	{
		throw PGL_EXCEPT_LOGIC("The state index is not -1, instead " + std::to_string(m_stateDumpIdx)
				+ ", this means the dumper was already active.");
	};

	//This is the base name for the state dumping
	const std::string BASE_GROUP = "/initState";	//This is the name of the dump folder without a trailing /
	const std::string BASE_NAME  = BASE_GROUP + "/";


	/*
	 * Open The file
	 */
	//Store if the file is open.
	const bool fileWasOpen = this->isOpen();

	//Open the file if not open
	if(fileWasOpen == false)
	{
		this->openFile();
			pgl_assert(this->isOpen());
	};


	/*
	 * INitialize the grid version
	 */
	{
		this->priv_createNewGridGeometryVersion(gridGeo);
	}; //End scope: grid geometry dump


	/*
	 * Initialize the marker version
	 */
	{
		this->priv_createNewMarkerVersion(mColl);
	};//End scope: create new marker version.


	/*
	 * We do not dump the state of the full marker collection
	 * and the grid property. The reason is that they are not
	 * needed anymore. Since we have reordered the dumping
	 * order in CORELLO. The init state and the first state
	 * are equivalent. So we will not dump it any longer.
	 */



	/*
	 * If the file was open we will not close it,
	 * but if we opened it, we will colse it
	 */
	if(fileWasOpen == false)
	{
		// The file was not open so we close it now.
		this->closeFile();
	}
	else
	{
		//The file was open, so we flush
		this->flushFile();
	};


	/*
	 * Now increase the internal index by one.
	 * This means it is now 0, and ready for use.
	 */
	this->m_stateDumpIdx += 1;
		pgl_assert(this->m_stateDumpIdx == 0);

	return;
}; //End: init dumping


PGL_NS_END(egd)


