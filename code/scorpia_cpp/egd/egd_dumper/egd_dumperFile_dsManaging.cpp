/**
 * \brief	This file contains the functions that deals with the managing of datasets.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include HDF5
#include <hdf5.h>	//We include the full library
//#include <H5Opublic.h>	//For the informations


//Include STD
#include <string>


PGL_NS_START(egd)


//Anonymus namespace
namespace
{

/**
 * \brief	This function returns true if char is a valid group name.
 *
 * Note that this function also consideres the slash as invalid.
 * The reason is that this function onyl checks for valid group names
 * and the slash is used to seperate groups into a hirarchy.
 *
 * \param  ch	The character that should be tested.
 *
 * \note	This function assumes ASCII
 */
bool
egdInternal_isValidGroupChar(
	const char 	ch)
{
	return (('a' <= ch && ch <= 'z') ||
		('A' <= ch && ch <= 'Z') ||
		('0' <= ch && ch <= '9') ||
		('_' == ch             )   );

};
#define isValidGroupChar(ch) egdInternal_isValidGroupChar(ch)


/**
 * \brief	This function returns true if all characters
 * 		 of groupName are valid.
 *
 * This function tests if isValidGroupChar returns true for all
 * elements of the path or it it is a "/".
 * Note that an empty name is not valid.
 *
 * \param  groupName	The group name to check.
 * \param  isAbs	If true the group name must be absolute
 */
bool
egdInternal_isValidGroupName(
	const egd_dumperFile_t::GroupPath_t& 	groupName,
	const bool 				isAbs = true)
{
	if(groupName.empty() )
	{
		return true;
	};

	if(isAbs )
	{
		if(groupName.front() != '/')
		{
			return false;
		};
	}; //End: isAbs

	//going through the group name
	for(const char c : groupName)
	{
		if(!(c == '/' || egdInternal_isValidGroupChar(c)) )
		{
			return false;
		};
	};//End for(c):

	return true;
}; //End: is valid group name

}; //End of anonymous namespace



void
egd_dumperFile_t::priv_createHardLink(
	const GroupPath_t& 	target,
	const GroupPath_t& 	newName)
{
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_RUNTIME("*this is not open.");
	};
	if((egdInternal_isValidGroupName(target, true) == false) &&
	   (                             target.back() != '/'  )   )  //Must map to a real data set
	{
		throw PGL_EXCEPT_InvArg("The target is not a valid name.");
	}
	if(egdInternal_isValidGroupName(newName, true) == false)
	{
		throw PGL_EXCEPT_InvArg("The new name is not a valid name.");
	};


	/*
	 * =====================================
	 * When we want to create a Link we must ensure that the name
	 * is not already taken. At the same time we must ensure that
	 * all intermediate setps exists.
	 */
	if(newName.at(0) != '/')
	{
		throw PGL_EXCEPT_InvArg("The passed group path \"" + newName + "\" did not start with a spash.");
	};

	//Count the number of slashes in the path
	const Size_t nSlashes = std::count(newName.cbegin(), newName.cend(), '/');
		pgl_assert(nSlashes >= 1);

	//Find the last slash; since we requiere that the path is absolute, we know that we
	//have at least one slash in the path and thus also a last one.
	const auto  lastSlash = pgl::find_last_if(newName.cbegin(), newName.cend(),
			[](const char ch) -> bool { return (ch == '/');});
		pgl_assert(lastSlash < newName.cend());


	/*
	 * If the number of slashes is larter than zero,
	 * we have to make sure that the full path exists
	 */
	if(nSlashes > 0)
	{
		/*
		 * We test if the last slash is not at the end of the group path.
		 * This means that we not have a trailing slash situation.
		 */
		if(lastSlash == (newName.cend() - 1))
		{
			throw PGL_EXCEPT_InvArg("The passed group path, \"" + newName + "\", has trailing slash and is considered ilegal.");
		}; //End if: trailing slash

		//This is the tentative test path that we buiild for testing the path
		GroupPath_t testPath;
		testPath.reserve(newName.size() + 9);

		auto readIt = newName.cbegin();	//This is the path that is read from and inserted into test path

		/*
		 * We must not consume here.
		 * Because otherwhise we will run over the string.
		 */

		/*
		 * We will now check all subpathes.
		 */
		while(readIt != lastSlash)
		{
			pgl_assert( readIt <  lastSlash,
 				   *readIt == '/'       );

			/*
			 * When we are here readIt points to a slash that is not the last slash
			 * This slash is either the leading slash of the abolut path or was the slash that made
			 * the last loop above finish in the LAST iteration.
			 * So we must now consume it
			 */
			testPath.push_back(*readIt);
			++readIt;

			//Consume the path of the group into the test
			//string, until a slash is found
			while(*readIt != '/')
			{
				pgl_assert( readIt <  lastSlash,
					   *readIt != '/'       );

				testPath.push_back(*readIt);		//Consume the character
				++readIt;
			}; //End while: consume
			//Note readIt can now point to lastSlash

			//Now we perform the checking if the group doies exists
			//and if not we create it
			{
				//Perform the call to the test function
				const auto testRes = H5Lexists(m_id, testPath.c_str(), H5P_DEFAULT);

				if(testRes > 0)
				{
					/*
				 	 * The group exists so no problem and nothing to do.
					 */
				}
				else if(testRes < 0)
				{
					throw PGL_EXCEPT_RUNTIME("The testing for existence of \"" + testPath + "\" resulted in an error.");
				}
				else
				{
					/*
					 * The group does not exist
					 * We will now create it.
					 */
					const auto groupId = H5Gcreate(m_id, testPath.c_str(),
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

					if(groupId < 0)
					{
						throw PGL_EXCEPT_RUNTIME("Could not create the group \"" + testPath + "\"");
					};

					// We have no use for the group so we close it again.
					if(H5Gclose(groupId) < 0)
					{
						throw PGL_EXCEPT_RUNTIME("Could not colse the group \"" + testPath + "\"");
					};
				}; //End else: it does not exists

				/*
				 * We do not consuzme the path here further, we did thatbefore but it was wrong.
				 * We consume the character at the beginning of the loop.
				 */
			}; //End check scope
		}; //End: search while loop

		pgl_assert(readIt == lastSlash);
	}; //End if: nSlashes positive


	/*
	 * Now after we have created and ensured that the path existed
	 * we can now create the data set.
	 * Bowever we will first test if we the data set does exists.
	 * Because it must not exist.
	 */
	{
		const auto existDataSet = H5Lexists(m_id, newName.c_str(), H5P_DEFAULT);

		if(existDataSet == 0)
		{
			/*
			 * It does not exists, and it does not so we are save.
			 */
		}
		else if(existDataSet > 0)
		{
			throw PGL_EXCEPT_LOGIC("The data set \"" + newName + "\" already exists, but it should not.");
		}
		else
		{
			throw PGL_EXCEPT_RUNTIME("The existence test for \"" + newName + "\" produced an error.");
		};
	}; //End: exists test scope


	/*
	 * End created all the groups that are needed
	 * ===================
	 */


	//Now we create the hard link
	const auto statusLink = H5Lcreate_hard(
			this->m_id, target.c_str(),
			this->m_id, newName.c_str(),
			H5P_DEFAULT, H5P_DEFAULT);

	if(statusLink < 0)
	{
		throw PGL_EXCEPT_RUNTIME("COuld not create the link " + newName + " that points to " + target);
	};

	return;
}; //End: create hard link



egd_dumperFile_t::HID_t
egd_dumperFile_t::priv_createDataSet(
	const GroupPath_t& 	groupPath,
	const HID_t 		dType,
	const Size_t 		rows,
	const Size_t 		cols)
{
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_LOGIC("The file must be open, to create a dataset.");
	};
	if(dType < 0)
	{
		throw PGL_EXCEPT_InvArg("The data type does not apears to be valid.");
	};


	/*
	 * We will no test if the path is a valid path
	 */
	{
		//Must not be empty
		if(groupPath.empty() == true)
		{
			throw PGL_EXCEPT_InvArg("The goupPath is empty.");
		};

		//Must be absolute
		if(groupPath.front() != '/')
		{
			throw PGL_EXCEPT_InvArg("The passed group path \"" + groupPath + "\" is not absolute.");
		};
		if(groupPath.back()  == '/')
		{
			throw PGL_EXCEPT_InvArg("The passed group path \"" + groupPath + "\" has a tailing slash.");
		};

		// this bool stores if the last character that was found
		// was a slash
		bool lastWasSlash = false;

		//Test all the characters for validity
		for(uSize_t i = 0; i != groupPath.size(); ++i)
		{
			//Load the character
			const char ch = groupPath[i];

			//Test if the current character is a slash
			const bool isCurrSlash = (ch == '/');

			if(isCurrSlash == false)
			{
				//Check for valid character
				if(!(isValidGroupChar(ch)))
				{
					throw PGL_EXCEPT_InvArg("The group path \"" + groupPath + "\" contained the invalid character \'" + std::string(1, ch) + "\' at its " + std::to_string(i) + " position.");
				};
			}
			else
			{
				//In the case of a slash we must check if the last one was also a slash
				if(lastWasSlash == true)
				{
					throw PGL_EXCEPT_InvArg("The group path \"" + groupPath + "\" contained two consecutive slashes.");
				};
			};
		}; //End for(i):
	}; //End scope: test if valid path

	/*
	 * This is a hack, we requiere that this function gets absolute paths
	 * This solves many problems
	 *
	 * This check is repeted here to enphazise that this function build
	 * on the fact that the path is absolute.
	 */
	if(groupPath.at(0) != '/')
	{
		throw PGL_EXCEPT_InvArg("The passed group path \"" + groupPath + "\" did not start with a spash.");
	};

	//Count the number of slashes in the path
	const Size_t nSlashes = std::count(groupPath.cbegin(), groupPath.cend(), '/');
		pgl_assert(nSlashes >= 1);

	//Find the last slash; since we requiere that the path is absolute, we know that we
	//have at least one slash in the path and thus also a last one.
	const auto  lastSlash = pgl::find_last_if(groupPath.cbegin(), groupPath.cend(),
			[](const char ch) -> bool { return (ch == '/');});
		pgl_assert(lastSlash < groupPath.cend());


	/*
	 * If the number of slashes is larter than zero,
	 * we have to make sure that the full path exists
	 */
	if(nSlashes > 0)
	{
		/*
		 * We test if the last slash is not at the end of the group path.
		 * This means that we not have a trailing slash situation.
		 */
		if(lastSlash == (groupPath.cend() - 1))
		{
			throw PGL_EXCEPT_InvArg("The passed group path, \"" + groupPath + "\", has trailing slash and is considered ilegal.");
		}; //End if: trailing slash

		//This is the tentative test path that we buiild for testing the path
		GroupPath_t testPath;
		testPath.reserve(groupPath.size() + 9);

		auto readIt = groupPath.cbegin();	//This is the path that is read from and inserted into test path

		/*
		 * We must not consume here.
		 * Because otherwhise we will run over the string.
		 */

		/*
		 * We will now check all subpathes.
		 */
		while(readIt != lastSlash)
		{
			pgl_assert( readIt <  lastSlash,
 				   *readIt == '/'       );

			/*
			 * When we are here readIt points to a slash that is not the last slash
			 * This slash is either the leading slash of the abolut path or was the slash that made
			 * the last loop above finish in the LAST iteration.
			 * So we must now consume it
			 */
			testPath.push_back(*readIt);
			++readIt;

			//Consume the path of the group into the test
			//string, until a slash is found
			while(*readIt != '/')
			{
				pgl_assert( readIt <  lastSlash,
					   *readIt != '/'       );

				testPath.push_back(*readIt);		//Consume the character
				++readIt;
			}; //End while: consume
			//Note readIt can now point to lastSlash

			//Now we perform the checking if the group doies exists
			//and if not we create it
			{
				//Perform the call to the test function
				const auto testRes = H5Lexists(m_id, testPath.c_str(), H5P_DEFAULT);

				if(testRes > 0)
				{
					/*
				 	 * The group exists so no problem and nothing to do.
					 */
				}
				else if(testRes < 0)
				{
					throw PGL_EXCEPT_RUNTIME("The testing for existence of \"" + testPath + "\" resulted in an error.");
				}
				else
				{
					/*
					 * The group does not exist
					 * We will now create it.
					 */
					const auto groupId = H5Gcreate(m_id, testPath.c_str(),
							H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

					if(groupId < 0)
					{
						throw PGL_EXCEPT_RUNTIME("Could not create the group \"" + testPath + "\"");
					};

					// We have no use for the group so we close it again.
					if(H5Gclose(groupId) < 0)
					{
						throw PGL_EXCEPT_RUNTIME("Could not colse the group \"" + testPath + "\"");
					};
				}; //End else: it does not exists

				/*
				 * We do not consuzme the path here further, we did thatbefore but it was wrong.
				 * We consume the character at the beginning of the loop.
				 */
			}; //End check scope
		}; //End: search while loop

		pgl_assert(readIt == lastSlash);
	}; //End if: nSlashes positive


	/*
	 * Now after we have created and ensured that the path existed
	 * we can now create the data set.
	 * Bowever we will first test if we the data set does exists.
	 * Because it must not exist.
	 */
	{
		const auto existDataSet = H5Lexists(m_id, groupPath.c_str(), H5P_DEFAULT);

		if(existDataSet == 0)
		{
			/*
			 * It does not exists, and it does not so we are save.
			 */
		}
		else if(existDataSet > 0)
		{
			throw PGL_EXCEPT_LOGIC("The data set \"" + groupPath + "\" already exists, but it should not.");
		}
		else
		{
			throw PGL_EXCEPT_RUNTIME("The existence test for \"" + groupPath + "\" produced an error.");
		};
	}; //End: exists test scope



	/*
	 * Now we can create the data set.
	 */

	//First create the data space
	hsize_t SIZES[2] = {(hsize_t)rows, (hsize_t)cols};

	//
	//Create a property list and enable compression
	const auto plist = H5Pcreate(H5P_DATASET_CREATE);
	if(plist < 0)
	{
		throw PGL_EXCEPT_RUNTIME("The creation of the property list failed.");
	};


	//Enable chunking, using the same chunk size as we have data
	//I have no idea if this is good:
	//chunking is needed for compression.
	if(H5Pset_chunk(plist, 2, SIZES) < 0)
	{
		H5Pclose(plist);	//Close the list
		throw PGL_EXCEPT_RUNTIME("Was not able to enable chunking.");
	};


	//Enable compressing '9' means best
	if(H5Pset_deflate(plist, 9) < 0)
	{
		H5Pclose(plist);
		throw PGL_EXCEPT_RUNTIME("Was not able to enable compressing.");
	};

	//create the data space
	const auto spaceRet = H5Screate_simple(2, SIZES, nullptr);
	if(spaceRet < 0)
	{
		H5Pclose(plist);
		throw PGL_EXCEPT_RUNTIME("The creation of the data space for \"" + groupPath + "\" failed.");
	};

	// Now we create the data set
	const auto setRet = H5Dcreate(m_id, groupPath.c_str(), dType, spaceRet,
			H5P_DEFAULT,
			plist,		//Use chunking and copression
			H5P_DEFAULT);

	if(setRet < 0)
	{
		H5Pclose(plist);
		H5Sclose(spaceRet);
		throw PGL_EXCEPT_RUNTIME("could not create the data set \"" + groupPath + "\"");
	};

	//Cloase the space since it is not needed any longer
	H5Sclose(spaceRet);

	//Close the property list
	H5Pclose(plist);

	//Return the ID of the newly created data set back to the caller
	return setRet;
}; //End: priv_createDataset

#undef isValidGroupChar


PGL_NS_END(egd)


