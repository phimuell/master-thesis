/**
 * \brief	This file implements the function to controll the dumping of the markers.
 *
 * Note that the file about the marker dumping does something similar, than this one.
 * And sometimes one could argue to which file a function belongs.
 * Generally here are public functions.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)

bool
egd_dumperFile_t::isInConstMarkerSet(
	const PropIdx_t& 	pIdx)
 const
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed pIdx is not valid.");
	};
	if(pIdx.isRepresentant() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index " + pIdx
				+ ", is not a representant, it would be " + pIdx.getRepresentant() );
	};

	if(m_constMarkerProp.count(pIdx) == 0)	//Test if not inside
	{
		return false;
	};

	return true;
}; //End: isInside const ste


bool
egd_dumperFile_t::isRecordedConstMarkerProperty(
	const PropIdx_t& 	pIdx)
 const
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed pIdx is not valid.");
	};
	if(pIdx.isRepresentant() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index " + pIdx
				+ ", is not a representant, it would be " + pIdx.getRepresentant() );
	};

	//Test if it is not in the recorded set
	if(this->m_recordedConstMarkerProp.count(pIdx) == 0)
	{
		//It is not inside
		return false;
	}; //End not inside

	//If it is inside it must be in the greater set, this is jhust for consistency
	if(this->m_constMarkerProp.count(pIdx) == 0)
	{
		throw PGL_EXCEPT_RUNTIME("The property " + pIdx + ", was found in the set of recorded constant marker properties."
				" But it is not found in the set of knwon constant marker properties.");
	}; //End if: not consistenzt

	return true;
}; //End: isRecordedMarkerProperty



egd_dumperFile_t&
egd_dumperFile_t::addConstantMarkerProperty(
	const egd_propertyIndex_t& 	prop)
{
	if(prop.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed marker property is not valid.");
	};
	if(prop.isPosition() == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not ignore positions on markers.");
	};
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open, so it is not possible to extend the list of ignoried marker properties.");
	};
	if(prop.isFeelVel() == true)
	{
		throw PGL_EXCEPT_LOGIC("The feel velocity can not be a constant property.");
	};
	if(this->isIgnoredMarkerProperty(prop) == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to add the markerproperty " + prop + ", to the constant marker properties"
				", but it is already ignored.");
	};
		pgl_assert(this->m_recordedConstMarkerProp.empty() );

	//Add the property prop to the ignore list
	this->m_constMarkerProp.insert(prop);

	return *this;
}; //End: ignore marker property


egd_dumperFile_t&
egd_dumperFile_t::addIgnoredMarkerProperty(
	const PropIdx_t& 	prop)
{
	if(prop.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed marker property is not valid.");
	};
	if(prop.isPosition() == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not ignore positions on markers.");
	};
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open, so it is not possible to extend the list of ignoried marker properties.");
	};
	if(this->isInConstMarkerSet(prop) == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to add the " + prop + " marker property to the set of the ignored property"
				", but it is alreay inside the constant marker properties.");
	};

	//Add it
	this->m_ignoredMarkerProp.insert(prop);

	return *this;
}; //End: add ignored property


egd_dumperFile_t&
egd_dumperFile_t::rmConstantMarkerProperty(
	const egd_propertyIndex_t& 	prop)
{
	if(prop.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed marker property is not valid.");
	};
	if(prop.isPosition() == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not ignore positions on markers.");
	};
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open, so it is not possible to extend the list of ignoried marker properties.");
	};
	/* no check if prop is inside the ignore list, this is different
	 * than the add function handles this. */

	//Remove the value
	const Size_t n = this->m_constMarkerProp.erase(prop);	//n ~ number of removed elements

	return *this;
	(void)n;
}; //End: remove constant marker property


bool
egd_dumperFile_t::isIgnoredMarkerProperty(
	const PropIdx_t& 	pIdx)
 const
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed pIdx is not valid.");
	};
	if(pIdx.isRepresentant() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index " + pIdx
				+ ", is not a representant, it would be " + pIdx.getRepresentant() );
	};

	if(m_ignoredMarkerProp.count(pIdx) == 0)
	{
		return false;
	};

	return true;
};//End: isInside the ignored list.


egd_dumperFile_t&
egd_dumperFile_t::markerDumpProbability(
	const Numeric_t 	p)
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already opened.");
	};
	if(pgl::isValidFloat(p) == false)
	{
		throw PGL_EXCEPT_InvArg("The probability was not a valid number.");
	};
	if(p <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The dump probability was too small, it was " + std::to_string(p));
	};
	if(m_idxDumpMarker.empty() == false)
	{
		throw PGL_EXCEPT_LOGIC("The marker dumper mod was already set.");
	};

	/*
	 * Make values that are larger than one to one
	 */
	if(p >= 1.0)
	{
		//All markers should join
		m_markerDumpProb = 1.0;
		return *this;
	};

	//Set the dump probability in the normal case
	m_markerDumpProb = p;

	//Return a reference to *this
	return *this;
};//End: setMarker dumping interval


egd_dumperFile_t&
egd_dumperFile_t::ignoreMarkerVel()
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open, so it is not possible to extend the list of ignoried marker properties.");
	};

	this->rmConstantMarkerProperty(PropIdx::VelX() )
	     .rmConstantMarkerProperty(PropIdx::VelY() )
	     .addIgnoredMarkerProperty(PropIdx::VelX() )
	     .addIgnoredMarkerProperty(PropIdx::VelY() );

	return *this;
}; //End: ignore marker vel


bool
egd_dumperFile_t::markerVelAreDumped()
 const
{
	if((this->m_ignoredMarkerProp.count(PropIdx::VelX() ) == 0) &&
	   (this->m_ignoredMarkerProp.count(PropIdx::VelY() ) == 0)   )
	{
		return true;
	};

	return false;
};//End: marker vel are dumped


egd_dumperFile_t&
egd_dumperFile_t::dumpFeelVelocity()
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open, so it is not possible to extend the list of ignoried marker properties.");
	};

	//Instruct the dumper to dump the velocity
	this->m_dumpFellMarkerVel = true;

	//remove the feel velocity from the ignored set
	this->m_ignoredMarkerProp.erase(PropIdx::FeelVelX() );
	this->m_ignoredMarkerProp.erase(PropIdx::FeelVelY() );
	this->m_constMarkerProp  .erase(PropIdx::FeelVelX() );
	this->m_constMarkerProp  .erase(PropIdx::FeelVelY() );

	return *this;
}; //End: dump feel velocity


bool
egd_dumperFile_t::feelVelAreDumped()
 const
{
	//Test if it is not in the ignore set
	if((this->m_ignoredMarkerProp.count(PropIdx::FeelVelX() ) == 0) &&
	   (this->m_ignoredMarkerProp.count(PropIdx::FeelVelY() ) == 0)   )
	{
		//Test if it is not deactivated
		if(this->m_dumpFellMarkerVel == true)
		{
			return true;
		};
	}; //End: we do it

	return false;
}; //End: do dumping feel vel


egd_dumperFile_t&
egd_dumperFile_t::ignoreGridProp(
	const egd_propertyIndex_t& 	prop)
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open, so it is not possible to extend the list of ignoried marker properties.");
	};
	if(prop.isFullHeatingTerm() == true)
	{
		throw PGL_EXCEPT_LOGIC("Ignoring the full heating term is not allowed.");
	};
	if(prop.isPlotProperty() == true)
	{
		throw PGL_EXCEPT_InvArg("Tried to add the plot property " + prop + ", to the list of ignored marker properties.");
	};

	//Add the property to the list
	m_gridPropIgn.insert(prop);
	pgl_assert(m_gridPropIgn.count(prop) == 1);

	return *this;
}; //End: ignore grid property


egd_dumperFile_t&
egd_dumperFile_t::onlyFullHeatingTerm()
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open.");
	};

	//Store that only the heating term is used
	m_onlyFullHeatTerm = true;

	return *this;
}; //End: only full heating term


egd_dumperFile_t&
egd_dumperFile_t::noStrainRateDump()
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open.");
	};

	//Store it
	m_noStrain = true;

	return *this;
}; //End: nostrain rate


egd_dumperFile_t&
egd_dumperFile_t::noStressDump()
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open.");
	};

	//store it
	m_noStress = true;

	return *this;
};//End: no stress dump


egd_dumperFile_t&
egd_dumperFile_t::onlyDumpRepOnGrid()
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open.");
	};

	//store
	m_onlyRepGridDump = true;

	return *this;
};//End: only dump reps


egd_dumperFile_t&
egd_dumperFile_t::noTempSol()
{
	if(m_wasOpen == true)
	{
		throw PGL_EXCEPT_LOGIC("The file was already open.");
	};

	//store
	m_noTempSolDump = true;

	return *this;
};//end: no temp solution


PGL_NS_END(egd)

