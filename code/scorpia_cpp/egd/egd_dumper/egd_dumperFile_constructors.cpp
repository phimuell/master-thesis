/**
 * \brief	This file implements the constructors of the dump file.
 *
 * Note that the destructor is handled as a managmement function is not
 * implemented here, but in the naaging file.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


/*
 * The default constructor is defaulted, but it will
 * uses the values that are assigned in the header.
 */
egd_dumperFile_t::egd_dumperFile_t()
 = default;

egd_dumperFile_t::egd_dumperFile_t(
		const FilePath_t& 	filePath)
 :
  egd_dumperFile_t()	//Call the default implementation
{
	if(filePath.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("The passed file path is empty.");
	};

	//Set the variable for the file path
	m_filePath = filePath;
}; //End: building constructor


 egd_dumperFile_t&
 egd_dumperFile_t::operator= (
 	egd_dumperFile_t&& other)
{
	if(this == &other)	//No self assignment
	{
		return *this;
	};

	//If *this is an open file try to close it
	if(this->isOpen() == true)
	{
		if(this->closeFile() != false)
		{
			throw PGL_EXCEPT_RUNTIME("An assignment requiered to close the file, but the exit status of the close function was wrong.");
		};
	}; //End if: close file


	//Load all the variableinto *this
	this->m_id       =           other.m_id;
	this->m_isOpen   =           other.m_isOpen;
	this->m_filePath = std::move(other.m_filePath);

	//Now overwrite the status of other
	other.m_id     = -1;	//negative values are invalid in HDF5
	other.m_isOpen = false; //This is always the case
	(void)other.m_filePath;	//This is done by the move
		pgl_assert(other.m_filePath.empty());

	//Now return *This
	return *this;
};


egd_dumperFile_t::egd_dumperFile_t(
	egd_dumperFile_t&& other)
 :
  egd_dumperFile_t() 	//Call the default constructor, we will overwrite it
{
	//Now assigne other to this, using move assignment
	this->operator=(std::move(other));
}; //End: move constructor.




PGL_NS_END(egd)


