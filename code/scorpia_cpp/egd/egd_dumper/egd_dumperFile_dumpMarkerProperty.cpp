/**
 * \brief	This file implements the functions that enables the dumping of a
 * 		 single marker property. See description of the functions for more.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>
#include <random>


PGL_NS_START(egd)


void
egd_dumperFile_t::dumpMarker(
	const MarkerProperty_t& 	mProp,
	const egd_propertyIndex_t&	prop,
	const GroupPath_t& 		groupPath_,
	const bool 			absPath)
{
	pgl_assert(prop.isValid(),
		   this->isOpen() );

	if(groupPath_.empty() )
	{
		throw PGL_EXCEPT_InvArg("The group path is empty.");
	};

	/*
	 * Test that the number of markers is the same as it was before
	 */
	if(mProp.size() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The number of markers is too small");
	};
	if(m_nMarkers == -1)	//Set the internal variable for the markers, could also be
	{			// set in the dump a collection

		m_nMarkers = mProp.size();
	}; //End if: set marker counter internals

	if(m_nMarkers != mProp.size())
	{
		throw PGL_EXCEPT_RUNTIME("The number of markers has changed. The dumper recorded "
				+ std::to_string(m_nMarkers) + ", but the collection has "
				+ std::to_string(mProp.size()) + " many markers.");
	};


	/*
	 * Test if we have to generate the marker filter
	 */
	if((m_markerDumpProb <  1.0 ) &&	//Smaller than one, so we have to select a subset, this will 1 make all dump
	    m_idxDumpMarker.empty()     )	//We have not created the lindex vector
	{
		const Index_t nMarkers      = mProp.size();
		m_idxDumpMarker.reserve(Size_t(nMarkers * (m_markerDumpProb + 0.06)));	//6% more than expected

		//create the generator and the distribution
		std::mt19937_64 geni(::pgl::pgl_seedRNG());	//use pgl geny to seed
		std::bernoulli_distribution dist(m_markerDumpProb);


		for(Index_t m = 0; m != nMarkers; ++m)
		{
			if(dist(geni) == true)
			{
				m_idxDumpMarker.push_back(m);
			};
		}; //End for(m):

		if(m_idxDumpMarker.empty() )
		{
			throw PGL_EXCEPT_RUNTIME("The vector with the index marker is empty.");
		};
	}; //End if: generate the index list


	//Create the save path
	std::string savePath_ = groupPath_;
	if(absPath == false)
	{
		if(savePath_.back() != '/')
		{
			savePath_ += "/" + prop.print();
		}
		else
		{
			savePath_ += prop.print();
		};
	}; //End if: is abs path
	const std::string savePath = std::move(savePath_);


	if(m_idxDumpMarker.empty() )
	{
		/*
		 * We have to dump everything, so we can
		 * simply call the internal function.
		 */
		this->priv_storeData(mProp.data(),
				mProp.cols(), mProp.rows(),	//Swaped order.
				savePath                   );
	}
	else
	{
		/*
		 * We have to dump a subset.
		 * So we will store it into a tenmporary and dump that.
		 * This is not so efficient.
		 */
		const Index_t NDump = m_idxDumpMarker.size();
		egd_markerProperty_t tmp(NDump);
			pgl_assert(tmp.size() == NDump);

		if((m_idxDumpMarker.front() <  0           ) ||
		   (m_idxDumpMarker.back()  >= mProp.size())   )
		{
			throw PGL_EXCEPT_RUNTIME("There are problems with the index of dumping."
					  " Smallest " + std::to_string(m_idxDumpMarker.front())
					+ ", Largest " + std::to_string(m_idxDumpMarker.back())
					+ ", but only " + std::to_string(mProp.size())
					+ " many markers.");
		}; //End if: too large
			pgl_assert(std::is_sorted(m_idxDumpMarker.cbegin(), m_idxDumpMarker.cend()));

		for(Index_t i = 0; i != NDump; ++i)
		{
			const Index_t m = m_idxDumpMarker[i];	//load the real index of teh marker
				pgl_assert(0 <= m, m < mProp.size());
			const auto    v = mProp[m];		//Load the value

			tmp[i] = v;	//Store the value
		}; //End for(i):

		/* Now dump it 	 */
		this->priv_storeData(tmp.data(),
				tmp.cols(), tmp.rows(),	//Swaped order.
				savePath               );
	};//End else: dump only subset


	/*
	 * Now anootate the property
	 * This will write the size of teh actual markers
	 */
	this->priv_annotateMarker(mProp, savePath);

	return;
}; //End: dump a marker property


bool
egd_dumperFile_t::dumpMarkerCollection(
	const MarkerCollection_t& 	mColl,
	const GroupPath_t& 		BASE_PATH)
{
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The file is not open.");
	};
	if(BASE_PATH.empty() )
	{
		throw PGL_EXCEPT_RUNTIME("The BASE_PATH is empty.");
	};
	if(mColl.nMarkers() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The number of markers is too small");
	};
	if(m_stateDumpIdx < 0)
	{
		throw PGL_EXCEPT_InvArg("The state index was negative, it was " + std::to_string(m_stateDumpIdx));
	};

	bool createdNewMarkerVersion = false;


	/*
	 * Test if the marker collection was modified.
	 */
	if(mColl.isNewVersion() == true)
	{
		/*
		 * There was a modification, so we must create a new version,
		 * before we can proceede.
		 */
		this->priv_createNewMarkerVersion(mColl);
		createdNewMarkerVersion = true;
	}; //End if: marker collection is new version


	//Create the save path
	std::string savePath_ = BASE_PATH;
	if(savePath_.back() != '/')
	{
		savePath_ += "/";
	}
	const std::string savePath_SAVE = std::move(savePath_) + "marker/";


	/*
	 * Iterate through all the stored grid collection
	 */
	for(const auto& it : mColl)
	{
		const PropIdx_t pIdx = it.first;	//unpack the index
			pgl_assert(pIdx.isValid()       ,
				   pIdx.isRepresentant() );

		//test if we ignore it completly
		if(this->isIgnoredMarkerProperty(pIdx) == true)
		{
			continue;
		};

		//test if it is a feel velocity
		if(pIdx.isFeelVel() == true)
		{
			//test if we ignore feel velocities
			if(this->m_dumpFellMarkerVel == false)
			{
				continue;
			}; //End: ignoring fell velocities
		}; //End if: is feel vel


		/* Test how we have to handle the dump, note that this test will
		 * only look at the actuiall recorded marker properties. This means
		 * that we could dump a marker property that was markerd constant,
		 * but was not pressent, at the creation of the marker version.
		 * Is this a bug? */
		if(this->isRecordedConstMarkerProperty(pIdx) == true)
		{
			/*
			 * It is a recorded marker property.
			 * So we have to load the current version of it
			 * and then create a simlink
			 */
			const GroupPath_t currVersion = this->priv_getCurrentMarkerPropPath(pIdx);	//Current
			const GroupPath_t thisMarker  = savePath_SAVE + pIdx.print();			//The alias name

			//create
			this->priv_createHardLink(currVersion, thisMarker);
		//End if: recorded case
		}
		else
		{
			/*
			 * It is not a recorded dump.
			 * So we can just all the dumping function.
			 * Naming is done by it.
			 */
			this->dumpMarker(it.second, it.first, savePath_SAVE);
		}; //End else: normal dump
	}; //End for(it):


	/*
	 * Create an attribute with the used marker vertsion
	 */
	{
		const Index_t currMarkerVersion = this->m_markerVersion;

		this->priv_attacheAttribute(
				&currMarkerVersion,
				BASE_PATH,
				"mVersion");
	};


	return createdNewMarkerVersion;
}; //End: dump a collection

/*
 * =========
 * Annotating
 */
void
egd_dumperFile_t::priv_annotateMarker(
	const MarkerProperty_t& 	mProp,
	const GroupPath_t& 		annotateTo)
{
	pgl_assert(     mProp.size() > 0,
		   annotateTo.size() > 0 );

	//load the size
	const Index_t nMarkers = mProp.size();

	//
	//Write the attributes
	this->priv_attacheAttribute(&nMarkers, annotateTo, "nMarkers");

	return;
};//END: Annotate marker



/*
 * ========================
 * Version managing
 */
egd_dumperFile_t&
egd_dumperFile_t::priv_createNewMarkerVersion(
	const MarkerCollection_t& 	markerColl)
{
	if(this->m_markerVersion == -1)
	{
		//We have not seen an iteration yet, so the state index must be -1 as well
		if(m_stateDumpIdx != -1)
		{
			throw PGL_EXCEPT_RUNTIME("Marker version is -1, not set yet, but state index is not -1, instead "
					+ std::to_string(m_stateDumpIdx) );
		};
	}
	else
	{
		//Grid geo is set so state index must be larger/equal 0
		if(m_stateDumpIdx < 0)
		{
			throw PGL_EXCEPT_RUNTIME("The state indes is not positive it is " + std::to_string(m_stateDumpIdx));
		};
	};


	/*
	 * Ensuring that the Type property is inside the list
	 */
	if(m_constMarkerProp.count(PropIdx::Type()) == 0)
	{
		m_constMarkerProp.insert(PropIdx::Type() );
	}; //end if: no type property inside const list


	//Store if it was open or not, needed for automatic resource closing
	const bool isAlreadyOpen = this->isOpen();

	if(isAlreadyOpen == false)	//Open the file if not
	{
		this->openFile();
	};
		pgl_assert(this->isOpen());

	//
	//DUMPING
	this->m_recordedConstMarkerProp.clear();	//Clear the list of the recorded constant marker properties.

	this->m_markerVersion += 1;			//Get to the new version

#	if 0
	/*
	 * Activate this code section in order to enable that the number
	 * of marker could be changed during the simulation. I do not
	 * consider this a good idea, but if it is needed, it done here.
	 */
	//This will set the number of markers to its default value
	this->m_nMarkers = -1;

#	endif /* Changing markers */

	//This will instruct the dump function to recreate the index vector anew,
	// if needed.
	this->m_idxDumpMarker.clear();

	//This is the path where we dump all marker properties to
	GroupPath_t THIS_MARKER_BASE_PATH = this->priv_getCurrentConstMarkerGroupPath();

	/*
	 * Iterate through the marker collection and store them
	 */
	for(const auto& it : markerColl)
	{
		//test if the marker is inside the constant set (this function will also check arguments)
		if(this->isInConstMarkerSet(it.first) == true)
		{
				pgl_assert(this->isIgnoredMarkerProperty(it.first) == false);

			//We dump it, the dump function will do all the naming
			this->dumpMarker(it.second, it.first, THIS_MARKER_BASE_PATH);

			//Record it in the collection
			this->m_recordedConstMarkerProp.insert(it.first);
		}; //End if: inside constant set
	};//End for(it): going through all markers

	//Store the index when it was introduced
	this->priv_attacheAttribute(m_stateDumpIdx, THIS_MARKER_BASE_PATH, "stateIdx");

	//Force a flush operation
	this->flushFile();

	//If the file was not open close it
	if(isAlreadyOpen == false)
	{
		this->closeFile();	//Was not open, so we close it
	};

	//Return *this, to method chaining
	return *this;
}; //End: create new marker version.


egd_dumperFile_t::GroupPath_t
egd_dumperFile_t::priv_getCurrentConstMarkerGroupPath()
 const
{
	const static GroupPath_t BASE_CONST_MARKER = "/constMarker/";

	if(this->m_markerVersion < 0)
	{
		throw PGL_EXCEPT_RUNTIME("The marker version was not iniialized.");
	};

	return GroupPath_t(BASE_CONST_MARKER + "ver" + std::to_string(this->m_markerVersion) + "/");
}; //End: current marker version path


egd_dumperFile_t::GroupPath_t
egd_dumperFile_t::priv_getCurrentMarkerPropPath(
	const PropIdx_t& 	pIdx)
 const
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed pIdx is not valid.");
	};
	if(pIdx.isRepresentant() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index " + pIdx
				+ ", is not a representant, it would be " + pIdx.getRepresentant() );
	};
	if(this->isRecordedConstMarkerProperty(pIdx) == false)
	{
		throw PGL_EXCEPT_LOGIC("The passed property index " + pIdx + " is not inside the recorded constnat marker properties.");
	};

	//get the current full path
	const GroupPath_t FULL_VERSION_PATH = this->priv_getCurrentConstMarkerGroupPath();

	return GroupPath_t(FULL_VERSION_PATH + pIdx.print());
}; //End: current marker path


PGL_NS_END(egd)


