/**
 * \brief	This file implements the functions that are needed for the processing of the grid geometry.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)

void
egd_dumperFile_t::dumpGridPos(
	const eGridType 	gType,
	const GridPosition_t& 	X,
	const GridPosition_t& 	Y,
	const GroupPath_t& 	groupPath)
{
	if(::egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};
	if(groupPath.empty() )
	{
		throw PGL_EXCEPT_InvArg("Passed an empty group path.");
	};
	if(X.size() < 2)
	{
		throw PGL_EXCEPT_InvArg("The size of the X nodes is too small.");
	};
	if(Y.size() < 2)
	{
		throw PGL_EXCEPT_InvArg("The size of the Y nodes is too small.");
	};
		pgl_assert(X.array().isFinite().all(),
			   Y.array().isFinite().all() );

	//Create the save path
	std::string savePath_ = groupPath;
	std::string gTypeStr  = this->priv_gTypeStr(gType);
	if(savePath_.back() != '/')
	{
		savePath_ += "/" + gTypeStr;
	}
	else
	{
		savePath_ += gTypeStr;
	};
	savePath_ += "/";
	const std::string savePath = std::move(savePath_);

	/*
	 * We have to dump everything, so we can
	 * simply call the internal function.
	 */
	this->priv_storeData(X.data(),
			X.cols(), X.rows(),	//Swaped order.
			savePath + "X"     );
	this->priv_storeData(Y.data(),
			Y.cols(), Y.rows(),	//Swaped order.
			savePath + "Y"     );

	return;
}; //End: dumpGruidPos


/*
 * ========
 * Helper
 */
std::string
egd_dumperFile_t::priv_gTypeStr(
	const eGridType 	gType)
 const
{
	if(::egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The grid type is not valid.");
	};

	/*
	 * What is the size of the grid property.
	 */
	const std::string SizeAtt = (::egd::isExtendedGrid(gType) ? "Ext" : "Reg");


	/*
	 * What is teh grid property grid type.
	 */
	std::string TypeAtt;

#define CASE(t, name) case eGridType:: t : TypeAtt = name; break;
	switch(mkCase(gType))
	{
		CASE(BasicNode , "Basic"  );
		CASE(StVx      , "StVx"   );
		CASE(StVy      , "StVy"   );
		CASE(StPressure, "StPress");
		CASE(CellCenter, "CC"     );

	  default:
		throw PGL_EXCEPT_InvArg("The grid type was not known.");
	  break;
	}; //End switch(gType):
		pgl_assert(TypeAtt.size() > 0,
			   SizeAtt.size() > 0 );

	//Combine the twos
	const std::string loadType = SizeAtt + TypeAtt;

	return loadType;
}; //End: tzranslator


/*
 * ==============
 * Managing Geometry versions
 */

egd_dumperFile_t&
egd_dumperFile_t::priv_createNewGridGeometryVersion(
	const GridGeometry_t& 		gridGeo)
{
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid geometry is not set.");
	};
	if(this->m_geoVersion == -1)
	{
		//We have not seen an iteration yet, so the state index must be -1 as well
		if(m_stateDumpIdx != -1)
		{
			throw PGL_EXCEPT_RUNTIME("Grid version is -1, not set yet, but state index is not 0, instead "
					+ std::to_string(m_stateDumpIdx));
		};
	}
	else
	{
		//Grid geo is set so state index must be larger/equal 0
		if(m_stateDumpIdx < 0)
		{
			throw PGL_EXCEPT_RUNTIME("The state indes is not positive it is " + std::to_string(m_stateDumpIdx));
		};
	};

	//Store if it was open or not, needed for automatic resource closing
	const bool isAlreadyOpen = this->isOpen();

	if(isAlreadyOpen == false)	//Open the file if not
	{
		this->openFile();
	};
		pgl_assert(this->isOpen());


	//
	//DUMPING

	//We now increment the version index
	this->m_geoVersion += 1;

	//This is the path that is used to store all the grid, not that this is a base location
	const GroupPath_t THIS_VERSION_BASE_GROUP = this->priv_getCurrentGeoGroupPath();

	//This is a list of all grid types that are in place
	eGridType allGTypes[] = { mkReg(eGridType::BasicNode  ),
				  mkReg(eGridType::StVx       ), mkReg(eGridType::StVy       ),
				  mkReg(eGridType::CellCenter ), mkReg(eGridType::StPressure ),
				  mkExt(eGridType::StVx       ), mkExt(eGridType::StVy       ),
				  mkExt(eGridType::CellCenter ), mkExt(eGridType::StPressure ) };

	/*
	 * Iterating through the list and dumping all of them
	 */
	for(const eGridType gType : allGTypes)
	{
		const auto nodeLoc = gridGeo.getGridPoints(gType);	//Node position

		//now we dump the grid, psotion, the nameing is done automatically
		//so we just can call the base function
		this->dumpGridPos(gType, nodeLoc, THIS_VERSION_BASE_GROUP);
	}; //end for(gType):

	/*
	 * Store the state index in which the version was created.
	 */
	this->priv_attacheAttribute(m_stateDumpIdx, THIS_VERSION_BASE_GROUP, "stateIdx");

	//Force a flush operation
	this->flushFile();

	//If the file was not open close it
	if(isAlreadyOpen == false)
	{
		this->closeFile();	//Was not open, so we close it
	};


	//Return *this, to method chaining
	return *this;
}; //End: create a new version



egd_dumperFile_t::GroupPath_t
egd_dumperFile_t::priv_getCurrentGeoGroupPath()
 const
{
	const static GroupPath_t BASE_GEO_PATH = "/gridGeometry/";

	if(this->m_geoVersion < 0)
	{
		throw PGL_EXCEPT_RUNTIME("The geometry was not yet initalized.");
	};

	//compose the group path
	return GroupPath_t(BASE_GEO_PATH + "ver" + std::to_string(this->m_geoVersion) + "/");
}; //End: getCurrent geometry group path


std::pair<egd_dumperFile_t::GroupPath_t, egd_dumperFile_t::GroupPath_t>
egd_dumperFile_t::priv_getCurrentGridGeometry(
	const eGridType 	gType)
 const
{
	if(::egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid types are invalid.");
	};

	const GroupPath_t GEO_PATH = this->priv_getCurrentGeoGroupPath();
	const std::string gTypeStr = this->priv_gTypeStr(gType);

	return std::make_pair(
			GroupPath_t(GEO_PATH + gTypeStr + "/X"),
			GroupPath_t(GEO_PATH + gTypeStr + "/Y") );
};//End: getCurrentGridGeometry




PGL_NS_END(egd)


