/**
 * \brief	This file contains teh function that dumps a complete simulation step.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


namespace
{
/**
 * \brief	This file generates a string with padding.
 *
 * It is like a to_string function that supports filling with a character.
 * This function ly accepts positive values.
 *
 * \param  val		The number that should be stringify.
 * \param  nPad		How many digit the number should at least have.
 * \param  pChar	The character that should be used for padding.
 */
std::string
paddedString(
	const Index_t 		val,
	const uSize_t		nPad,
	const char 		pChar = '0')
{
	if(val < 0)
	{
		throw PGL_EXCEPT_InvArg("Can not pad the number \"" + std::to_string(val) + "\"");
	};

	//make a string
	std::string s = std::to_string(val);

	if(s.size() > nPad)
	{
		throw PGL_EXCEPT_RUNTIME("Too many states.");
	};

	//Now expand the string, it is not that efficient but I do not care
	while(s.size() < nPad)
	{
		s.insert(0, 1, pChar);
	}; //End while:
	pgl_assert(s.size() >= nPad);

	return s;
}; //End: paddedString

}; //End: anaopymus namespace


void
egd_dumperFile_t::dumpState(
	const GridContainer_t& 		grid,
	const MarkerCollection_t& 	mColl,
	const DeviatoricStress_t&	stress,
	const DeviatoricStrainRate_t&	strainRate,
	const MechSolverResult_t& 	mechSol,
	const TempSolverResult_t&	tempSol,
	const HeatingTerm_t& 		heatingTerm,
	const Index_t 			tStepIdx,
	const Numeric_t 		currTime,
	const Numeric_t 		thisDt)
{
	const static Index_t N_PADS = 5;

	if(grid.getGeometry().isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("Can not dump an grid that is not set.");
	};
	if(mColl.nMarkers() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The marker collection is empty.");
	};
	if(tStepIdx < 0)
	{
		throw PGL_EXCEPT_InvArg("The time step index, " + std::to_string(tStepIdx) + ", is invalid.");
	};
	if(m_stateDumpIdx < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Internal consistency is violated, detected a negative dump index counter of "
				+ std::to_string(m_stateDumpIdx));
	};
	if(m_stateDumpIdx > tStepIdx)
	{
		throw PGL_EXCEPT_InvArg("Inconsistency in the time step. The internal (real dump) counter is "
				+ std::to_string(m_stateDumpIdx) + ", but passed time step is "
				+ std::to_string(tStepIdx));
	};
	if(thisDt <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The time step value, " + std::to_string(thisDt) + ", is negative.");
	};
	pgl_assert(pgl::isValidFloat(thisDt)  ,
		   pgl::isValidFloat(currTime) );

	//Safe if the file is open or not
	const bool fileWasOpen = this->isOpen();

	//open the file if needed
	if(fileWasOpen == false)
	{
		this->openFile();
			pgl_assert(this->isOpen());
	}; //End if: open the file

	//This is the padded index number, use the internal counter.
	const std::string IDX = paddedString(m_stateDumpIdx, N_PADS);

	//This is the base name for the state dumping
	const std::string BASE_GROUP = "/simulation_states/" + IDX;	//This is the name of the dump folder without a trailing /
	const std::string BASE_NAME  = BASE_GROUP + "/";

	/*
	 * Dumping the grid
	 */
	const bool createdNewGeometryVersion = this->dumpGridContainer(grid, BASE_NAME);


	/*
	 * Dump the marker collection.
	 */
	const bool createdNewMarkerVersion = this->dumpMarkerCollection(mColl, BASE_NAME);


	/*
	 * Dump Strain and stress
	 * The function will respect the settings
	 */
	{
		this->dumpStrainStress(stress, strainRate, BASE_NAME);
	}; //End scope: stress and strain


	/*
	 * Dumping Mechanical solution
	 */
	{
		this->dumpMechSol(mechSol, BASE_NAME);
	}; //End scope: mech solution


	/*
	 * Dumping the temperature Solution
	 */
	{
		this->dumpTemperatureSol(tempSol, heatingTerm, BASE_NAME);
	}; //End scope Temp sol and heating term



	/*
	 * Writting attributes
	 */
	{
		this->priv_attacheAttribute(&thisDt                   , BASE_GROUP, "dt"                );   //times
		this->priv_attacheAttribute(&currTime                 , BASE_GROUP, "currtime"          );
		this->priv_attacheAttribute(&tStepIdx                 , BASE_GROUP, "trueStateIdx"      );   //true state
		this->priv_attacheAttribute( createdNewMarkerVersion  , BASE_GROUP, "newMarkerVersion"  );   //versioning
		this->priv_attacheAttribute( createdNewGeometryVersion, BASE_GROUP, "newGeometryVersion");
	}; //End: writting attributes


	/*
	 * Create the link to the current grid geometry
	 */
	{
		this->priv_createHardLink(this->priv_getCurrentGeoGroupPath(), BASE_NAME + "gridGeo");
	}; //End scope: grid geo


	/*
	 * If the file was open we will not close it,
	 * but if we opened it, we will colse it
	 */
	if(fileWasOpen == false)
	{
		// The file was not open so we close it now.
		this->closeFile();
	}
	else
	{
		//The file was open, so we flush
		this->flushFile();
	};


	/*
	 * Increase the internal counter for the dumper state
	 */
	m_stateDumpIdx += 1;

	return;
}; //End: dump Complete state.




PGL_NS_END(egd)


