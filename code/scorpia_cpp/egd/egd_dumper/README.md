# Info
This folder contains the code that is responsible for writing the state of the simulation to a persistent medium.
Which basically means the file system, for that HDF5 is used.
The code here can also be seen as a simple HDF5 wrapper.

This folder implements a single class. In order to access the file we recommend in using SIBYL.



