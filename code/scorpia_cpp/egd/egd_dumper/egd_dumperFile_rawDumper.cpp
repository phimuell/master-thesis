/**
 * \brief	This file contains the raw dumper implementation.
 *
 * The functions taht are implemented here operate just on data.
 * Thy do nothing more, no logic or so.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include HDF5
#include <hdf5.h>	//We include the full library


//Include STD
#include <string>


PGL_NS_START(egd)

void
egd_dumperFile_t::priv_storeData(
	const Numeric_t* const 	data,
	const Size_t 		rows,
	const Size_t 		cols,
	const GroupPath_t&	groupPath)
{
	//All checks are done inside the actual implementation
	//To make debugging a bit easier, we will do some asserts here
	pgl_assert(
		data     != nullptr,
		isOpen() == true   ,
		rows     >  0      ,
		cols     >  0       );

	/*
	 * Call the implementation
	 */
	this->priv_storeData(
			static_cast<const void* const>(data),	//Cast to a void pointer to call the implementation.
			rows, cols,				//Size of the matrix
			groupPath,				//Where to save
			H5T_NATIVE_DOUBLE);			//Datatype

	//We want to ensure that we take the correct one
	static_assert(std::is_same<Numeric_t, double>::value, "Wrong data type in the dumper.");

	return;
}; //End: dumper for numeric type




/*
 * ====================================
 * I M P L E M E N T A T I O N
 */

void
egd_dumperFile_t::priv_storeData(
	const void* const 	data,
	const Size_t 		rows,
	const Size_t 		cols,
	const GroupPath_t& 	groupPath,
	const HID_t 		dType)
{
	/*
	 * This function requieres that *this is an open file
	 */
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_LOGIC("The dummper needs to be opened in order to write.");
	};
	if(groupPath.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("The argument for the group path is empty.");
	};
	if(data == nullptr)
	{
		throw PGL_EXCEPT_InvArg("The data pointer is empty.");
	};
	if(cols <= 0)
	{
		throw PGL_EXCEPT_InvArg("The column argument \"" + std::to_string(cols) + "\" was invalid.");
	};
	if(rows <= 0)
	{
		throw PGL_EXCEPT_InvArg("The row argument \"" + std::to_string(rows) + "\" was invalid.");
	};
	if(dType < 0)
	{
		throw PGL_EXCEPT_InvArg("The data type was invalid (" + std::to_string(dType) + ").");
	};


	//We only accept absolute paths
	if(groupPath.at(0) != '/')
	{
		throw PGL_EXCEPT_InvArg("The group path \"" + groupPath + "\" is not absolute.");
	};


	/*
	 * Now we generate the data set and data space.
	 * This will be done by another function that also handles
	 * parent paths.
	 */
	const auto dataSetID = this->priv_createDataSet(groupPath, dType, rows, cols);
		pgl_assert(dataSetID >= 0);


	/*
	 * Now we write into the newly created data set.
	 */
	const auto writeStatus = H5Dwrite(
			dataSetID,		//Data set ID
			dType,			//Data type of the memory
			H5S_ALL, H5S_ALL,	//What we read (hyperslap)
			H5P_DEFAULT,		//Mysterious parameter
			data);			//Pointer to the data in memeory

	if(writeStatus < 0)
	{
		H5Dclose(dataSetID);
		throw PGL_EXCEPT_RUNTIME("Write for the data set \"" + groupPath + "\" failed.");
	};


	//CLose the data set
	if(H5Dclose(dataSetID) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Failed to close the data set \"" + groupPath + "\".");
	};

	return;
}; //End: priv_storeData

PGL_NS_END(egd)


