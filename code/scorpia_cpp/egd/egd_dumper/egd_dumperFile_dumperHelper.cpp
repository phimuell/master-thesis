/**
 * \brief	This file contains functions that dumps certain asspects.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


void
egd_dumperFile_t::dumpStrainStress(
	const DeviatoricStress_t& 		stress,
	const DeviatoricStrainRate_t&		strainRate,
	const GroupPath_t& 			BASE_PATH)
{
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_RUNTIME("*this is not open.");
	};
	if(BASE_PATH.empty() )
	{
		throw PGL_EXCEPT_RUNTIME("The BASE_PATH is empty.");
	};

	//Create the save path
	std::string savePath_ = BASE_PATH;
	if(savePath_.back() != '/')
	{
		savePath_ += "/";
	}
	const std::string savePath_SAVE = std::move(savePath_);


	// S T R E S S
	if(m_noStress == false)
	{
		//This is the group we use for the stress dumping
		const GroupPath_t STRESS_GROUP = savePath_SAVE + "Stress/";

		//save the comnponenents, using absolute path
		this->dumpGridProperty(stress.xx(), STRESS_GROUP + "xx", true);
		this->dumpGridProperty(stress.yy(), STRESS_GROUP + "yy", true);
		this->dumpGridProperty(stress.xy(), STRESS_GROUP + "xy", true);

		//Create a hardlink for symmetry
		this->priv_createHardLink(STRESS_GROUP + "xy", STRESS_GROUP + "yx");
	}; //End if: stress


	// S T R A I N R A T E
	if(m_noStrain == false)
	{
		//This is teh strain rate group
		const GroupPath_t STRAIN_GROUP = savePath_SAVE + "StrainRate/";

		//Save the components
		this->dumpGridProperty(strainRate.xx(), STRAIN_GROUP + "xx", true);
		this->dumpGridProperty(strainRate.yy(), STRAIN_GROUP + "yy", true);
		this->dumpGridProperty(strainRate.xy(), STRAIN_GROUP + "xy", true);

		//Create a hardlink for symmetry
		this->priv_createHardLink(STRAIN_GROUP + "xy", STRAIN_GROUP + "yx");
	}; //End: dumping strain rate

	return;
}; //Dump Strain and stress


void
egd_dumperFile_t::dumpTemperatureSol(
	const TempSolverResult_t& 		tempSol,
	const HeatingTerm_t& 			heatingTerm,
	const GroupPath_t& 			BASE_PATH)
{
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_RUNTIME("*this is not open.");
	};
	if(BASE_PATH.empty() )
	{
		throw PGL_EXCEPT_RUNTIME("The BASE_PATH is empty.");
	};

	//Create the save path
	std::string savePath_ = BASE_PATH;
	if(savePath_.back() != '/')
	{
		savePath_ += "/";
	}
	const std::string savePath_SAVE = std::move(savePath_);


	if(this->m_noTempSolDump == true)	//If no temperature solution is requiered, we will
	{					//return imediatly
		return;
	};

	/*
	 * Dumping the temperature differences
	 */
	{
		const std::string DUMP_PATH = savePath_SAVE + "tempSol" + "/";
		const std::string DUMP_LOC  = DUMP_PATH + PropIdx::TempDiff().print();	//We write it to that location
		this->dumpGridProperty(tempSol.cgetTempDiff(), DUMP_LOC, true);

		if(tempSol.hasTimeStep() == true)
		{
			const Numeric_t tempTimeStep = tempSol.getTimeStep();
			this->priv_attacheAttribute(&tempTimeStep, DUMP_LOC, "timeDifference");
		}; //End if: has time step
	};//End scope: dumping


	/*
	 * Dumping the heating term
	 *
	 * The heatingterm is not considered special (Anymore)
	 * This means it will be dumped into the normal grid subfolder.
	 * This makes the data base easier.
	 */
	{
		//This is where we dump the individual terms
		const std::string DUMP_PATH = savePath_SAVE + "grid" + "/";

		//Dump the full heatingterm
		this->dumpGridProperty(heatingTerm.getSource(), DUMP_PATH + PropIdx::FullHeatingTerm().print(), true);

		//Dump the components if supported
		if(this->m_onlyFullHeatTerm == false)
		{
			const GroupPath_t DUMP_PATH_HEATINGTERM = DUMP_PATH + "heatingTerm/";
#			define SAVE(s) DUMP_PATH_HEATINGTERM + PropIdx:: s ().print(), true
			this->dumpGridProperty(heatingTerm.shearHeating()    , SAVE(ShearHeat));
			this->dumpGridProperty(heatingTerm.adiabaticHeating(), SAVE(AdiabaticHeat));
			this->dumpGridProperty(heatingTerm.radioHeating()    , SAVE(RadioEnergy));
#			undef SAVE
		}; //End if: dump the components too
	}; //End: heatingterm

	return;
}; //End: dump temperature sol and source


void
egd_dumperFile_t::dumpMechSol(
	const MechSolverResult_t& 		mechSol,
	const GroupPath_t& 			BASE_PATH)
{
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_RUNTIME("*this is not open.");
	};
	if(BASE_PATH.empty() )
	{
		throw PGL_EXCEPT_RUNTIME("The BASE_PATH is empty.");
	};

	//Create the save path
	std::string savePath_ = BASE_PATH;
	if(savePath_.back() != '/')
	{
		savePath_ += "/";
	}
	const std::string savePath_SAVE = std::move(savePath_) + "mechSol" + "/";

	const Index_t N = mechSol.nSolFields();
	for(Index_t i = 0; i != N; ++i)
	{
		this->dumpGridProperty(mechSol.getSolField(i), savePath_SAVE);
	}; //End for(i):


	if(mechSol.hasTimeStep() == true)
	{
		const Numeric_t mechTimeStep = mechSol.getTimeStep();
		this->priv_attacheAttribute(&mechTimeStep, savePath_SAVE, "timeDifference");
	}; //ENd if: mech sol has time step

	return;
}; //End: dump mech sol


PGL_NS_END(egd)


