/**
 * \brief	This file contains teh functions that are dealing with the creation of attributes.
 *
 * These are scalar valus that are attached to a data set.
 * It can also be a text.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include HDF5
#include <hdf5.h>	//We include the full library


//Include STD
#include <string>


PGL_NS_START(egd)


void
egd_dumperFile_t::priv_attacheAttribute(
	const Numeric_t* const 	data,
	const GroupPath_t& 	attacheTo,
	const std::string&	attrName)
{
	pgl_assert(
		data != nullptr,
		attacheTo.size() > 0,
		attrName.size() > 0 );

	//Call the underlying implementation
	this->priv_attacheAttribute(
			static_cast<const void* const>(data),		//Cast to void to call the right implementation.
			attacheTo,					//Pass through
			attrName,					//Pass through
			H5T_NATIVE_DOUBLE);				//It is a double
	static_assert(std::is_same<double, Numeric_t>::value, "Numeric_t must be double for the dumper.");

	return;
}; //End: dumper attribute for numeric types


void
egd_dumperFile_t::priv_attacheAttribute(
	const Index_t* const 	data,
	const GroupPath_t& 	attacheTo,
	const std::string&	attrName)
{
	pgl_assert(
		data != nullptr,
		attacheTo.size() > 0,
		attrName.size() > 0 );

	//Call the underlying implementation
	this->priv_attacheAttribute(
			static_cast<const void* const>(data),		//Cast to void to call the right implementation.
			attacheTo,					//Pass through
			attrName,					//Pass through
			H5T_NATIVE_LLONG);				//It is a double
	static_assert(std::is_same<Index_t, signed long long int>::value, "Index_t must be LLONG");

	return;
}; //End: dumper attribute for numeric types


void
egd_dumperFile_t::priv_attacheAttribute(
	const bool 		data_,
	const GroupPath_t& 	attacheTo,
	const std::string& 	attrName)
{
	pgl_assert(attacheTo.size() > 0,
		   attrName.size()  > 0);

	const bool data = data_;	//Make a copy
	const bool* const dataP = &data;

	this->priv_attacheAttribute(
			static_cast<const void* const>(dataP),
			attacheTo,
			attrName,
			H5T_NATIVE_HBOOL);
	static_assert(std::is_same<hbool_t, bool>::value, "Bool must be the same.");

	return;
}; //End: add a boolean attribute


void
egd_dumperFile_t::priv_attacheAttribute(
	const std::string& 	data,
	const GroupPath_t& 	attacheTo,
	const std::string& 	attrName)
{
	/*
	 * Implementation is based on:
	 * 	https://support.hdfgroup.org/ftp/HDF5/current/src/unpacked/examples/h5_attribute.c
	 */
	pgl_assert(attacheTo.size() > 0,
		   attrName.size()  > 0);

	if(data.empty() )
	{
		throw PGL_EXCEPT_InvArg("The passed string is empty.");
	};

	//This is the length of the string, with a safty margin
	const auto lengthOfData = data.size() + 10;

   	//Create the special data type that we need for storing a string object
   	const HID_t atype = H5Tcopy(H5T_C_S1);		//Copy the string datatype
   	if(atype < 0)
	{
		throw PGL_EXCEPT_RUNTIME("COuld not create the string data type for " + attacheTo + ":" + attrName);
	};

        H5Tset_size(atype, lengthOfData);		//Modify the data type
        H5Tset_strpad(atype, H5T_STR_NULLTERM);

        //Pass everything to the underling function
        this->priv_attacheAttribute(
        		static_cast<const void* const>(data.c_str() ),
			attacheTo,
			attrName,
			atype);

	//Close the attribute type again
	if(H5Tclose(atype) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Could not close the string attribute data space.");
	};

	return;
}; //End: add a string




/*
 * This is the core implementation.
 * This implementation works on void*
 */

void
egd_dumperFile_t::priv_attacheAttribute(
	const void* const 	data,
	const GroupPath_t& 	attacheTo,
	const std::string&	attrName,
	const HID_t 		dType)
{
	if(data == nullptr)
	{
		throw PGL_EXCEPT_InvArg("The data is the null pointer.");
	};
	if(attacheTo.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No object given to which we want to attache the attribute.");
	};
	if(attrName.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("The attribute name is empty.");
	};
	if(dType < 0)
	{
		throw PGL_EXCEPT_InvArg("The type identifier apears to be invalid.");
	};
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_LOGIC("The file is not open.");
	};


	/*
	 * Ensure that the object we want to attache the attribute to
	 * realy does exist.
	 */
	{
		const auto testResult = H5Lexists(m_id, attacheTo.c_str(), H5P_DEFAULT);

		if(testResult > 0)
		{
			/*
			 * No problem, the object exist
			 */
		}
		else if(testResult == 0)
		{
			throw PGL_EXCEPT_InvArg("Can not attache an attribute to \"" + attacheTo + "\" because it does not exist.");
		}
		else
		{
			throw PGL_EXCEPT_RUNTIME("The look up if \"" + attacheTo + "\" resulted in an error.");
		};
	}; //End scope: test if the target exist

	/*
	 * Now we open the object.
	 * Since we have no idea what it is we use the
	 * general open function that works on objects.
	 */
	const auto attacheObj = H5Oopen(m_id, attacheTo.c_str(), H5P_DEFAULT);

	if(attacheObj < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Trying to open the object at \"" + attacheTo + "\" resulted in an error.");
	};


	/*
	 * Now we create the simple scalar data space for the attribute.
	 */
	const auto attrSpaceID = H5Screate(H5S_SCALAR);

	if(attrSpaceID < 0)
	{
		throw PGL_EXCEPT_RUNTIME("The attempt to create the data space for \"" + attacheTo + "\" resulted in an error.");
	};


	/*
	 * Now create the attribute
	 */
	const auto attrID = H5Acreate(attacheObj, attrName.c_str(), dType, attrSpaceID, H5P_DEFAULT, H5P_DEFAULT);
	if(attrID < 0)
	{
		throw PGL_EXCEPT_RUNTIME("The attempt to create the attribute for \"" + attacheTo + "\" resulted in an error.");
	};


	/*
	 * Write the attribute
	 */
	if(H5Awrite(attrID, dType, data) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("The attempt to write the attribute for \"" + attacheTo + "\" resulted in an error.");
	};


	/*
	 * Now closing everything
	 */
	if(H5Aclose(attrID) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Could not close the attribute for \"" + attacheTo + "\"");
	};
	if(H5Sclose(attrSpaceID) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Could not close the attribute data space for \"" + attacheTo + "\"");
	};
	if(H5Oclose(attacheObj) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Could not close the to annotate object \"" + attacheTo + "\"");
	};

	return;
}; //End: attache at


PGL_NS_END(egd)


