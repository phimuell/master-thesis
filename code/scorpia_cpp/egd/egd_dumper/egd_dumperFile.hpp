#pragma once
/**
 * \brief	This function represents the file we dump to
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_phys/egd_heatingTerm.hpp>
#include <egd_phys/egd_stress.hpp>
#include <egd_phys/egd_strainRate.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_set.hpp>
#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


/**
 * \brief	This file is the dumper controller.
 * \class 	egd_dumpFile_t
 *
 * This class is a bit badly designed, it does too many things. It is responsible
 * for managing the file and writing to it. For that it uses the HDF5 C API directly.
 * The reason is that the C API is documented better than the C++ API is, at least
 * in my view.
 *
 * This file allows to do very simple stuff and is tailored for operating in the egd
 * contex, meaning it operates directly on on the provided containers. Note that this
 * class currently only works on POSIX.
 *
 * Note that this class has a quite interesting feature. It will store that matrices,
 * aka properties, in transposed order! This comes from the fact that Eigen uses ColMaj
 * order but HDF5/C uses RowMaj order.
 *
 * THe new version of the dumper exploits the capabilities of HDF5 better. So it will
 * uses links to reduce the storage needs, while maintaining the illusion of a full dump.
 *
 * Grid Properties (only the ones that are in the grid container):
 * There are two ways to controle how grid properties are dumped. First there is a flag,
 * that controles if only representants should be stored, meaning that isRepresentant()
 * returns true. Note that this excludes plot properties.
 * The second mechanism is, that the dumper maintains a list. This list is a list of
 * properties that are ignored and not dumped.
 *
 * Marker Properties (only the ones that are stored inside the marker collection):
 * There are three mechanism to controll the dumping.
 * First of all a filter mechanism is provided that alows to dump a certain subset.
 * This works by specifing a probability p, number greater than zero (values larger one
 * will have the same effect as one). If a new version of markers checked in,
 * the subset of markers that will be dumped is created. A marker will be dumped
 * with probabilty p. This allows to create a dump pattern without a bias, also
 * a smoother selection is acived. Note that the set is recreated each time a new
 * version is created and is not the same.
 * The second mechanism is the one of the constant properties. There are properties that
 * are defined on markers, usualy properties that depends on the marker type, that are
 * constant during the execution of the simulation. They are dumped once, and then
 * linked into the respective area of the dumping hirarchy. It is possible to instruct the
 * dumping that the markers have been changed, then a new dump of the properties will be done.
 * This is called version.
 * It is also possible to completly ignore a marker. This means that it will never ever be dumped.
 * Note even in the initial state.
 *
 * Grid Geometry:
 * The grid geometry is also dumped. It is possible to update them and then everything is
 * updated. They will be linked into every dump state, by a softlink that points to the
 * current grid geometry.
 *
 * Special Attributes:
 * Each grid property that is dumped, including the solutions, strain rate and stress,
 * several special attributes are attached to. One is "gType", which is a description
 * of the grid that was used. "X" and "Y" are strings, that contains a link. They points
 * to the node locations the values are defined on.
 *
 * Strain rate and Stress:
 * It is possible to disable the dumping of them.
 *
 * Temperature Solution & Heating Term:
 * It is possible to disable the dumping of the temperature solution. It will not be pressent.
 * In addition it is possible to dump only the full heating term and not its components.
 * If the temperature solution is deactivated, then the heating term will not be dumped.
 *
 * Randomness:
 * To generate randomness std::mt19937_64 is used. In order to seed this generator the
 * pgl seed generator is used.
 *
 * State Index:
 * The dumper will ensure that the states are labeled in a consecutive way. This means
 * that simulation code can just check if a dump is requiered and perform a dump if needed.
 * the state index does not need to be addapted for this, this is handled on the
 * dumper level directly.
 *
 */
class egd_dumperFile_t
{
	/*
	 * ======================
	 * Typedef
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using GridGeometry_t 		= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.

	using TempSolverResult_t 	= egd_tempSolverResult_t;	//!< This is the result container for the temperature solver.
	using MechSolverResult_t 	= egd_mechSolverResult_t;	//!< This is the result container for the mechanical solver.
	using DeviatoricStress_t 	= egd_deviatoricStress_t;	//!< This is the container for the deviatoric stress.
	using DeviatoricStrainRate_t 	= egd_deviatoricStrainRate_t;	//!< This is the container for the deviatoric strain rate.
	using HeatingTerm_t 		= egd_heatingTerm_t;		//!< This is the heating term.

	//These are the two types that will be supported for the dumping.
	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;	//!< This is the type that stores a marker property.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;	//!< This is a grid property.
	using PropIdx_t 		= GridProperty_t::PropIdx_t;		//!< The property index.
	using GridPosition_t 		= GridGeometry_t::NodePosition_t;	//!< Type for encoding positions of grid nodes.

	using FilePath_t 		= ::boost::filesystem::path;		//!< This is the path to a file.

	/**
	 * \brief	Thsi is the type that is used to store the grid properties, that are ignored for dumping.
	 * \typedef 	GridPropIgnoreSet_t
	 *
	 * This is a container for storing grid properties. It is used to store which
	 * properties should be ignored when they are dumped.
	 */
	using GridPropIgnoreSet_t 		= ::pgl::pgl_set_t<PropIdx>;


	/**
	 * \brief	This is the type for storing the marker properties that should be ignored.
	 */
	using MarkerPropIgnoreSet_t 		= ::pgl::pgl_set_t<PropIdx_t>;


	/**
	 * \brief	Thsi is the type that is used to store the marker properties, that are constant.
	 * \typedef 	ConstMarkerPropSet_t
	 *
	 * This set stores the marker properties which are considered to be constant
	 * and are dumped only if the marker configuration has changed.
	 */
	using ConstMarkerPropSet_t 		= ::pgl::pgl_set_t<PropIdx>;


	/**
	 * \brief	This is the group path type.
	 * \typedef 	GroupPath_t
	 *
	 * HDF5 is a file format that emulates an POSIX file system.
	 * This type represents a path to a "folder" inside the file,
	 * such a folder is called group in HDF5 speak.
	 */
	using GroupPath_t = ::std::string;


private:
	/**
	 * \brief	Identifier type for an HDF5 object.
	 * \typedef	HID_t
	 *
	 * This type is used as an identiefier for HDF5 objects.
	 * According to the header hid_t is typedeffed to be an
	 * int, so we do this here too. We do this to avoid
	 * ioncluding HDF5 into this header. There is a check that
	 * is done in the CPP file to ensure that the type is correct.
	 */
	using HID_t = int64_t;


	/**
	 * \brief	This is an index array.
	 * \typedef	IndexArray_t
	 *
	 * It is used to store the indexes of markers that are
	 * to be dumped.
	 */
	using IndexArray_t = ::pgl::pgl_vector_t<Size_t>;


	/*
	 * ===================================
	 * Constructor
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor takes a file path that will then be
	 * used as the dump file's location.
	 *
	 * \param  filePath 	Where the file is located.
	 */
	egd_dumperFile_t(
		const FilePath_t& 	filePath);


	/**
	 * \brief	The copy constructor.
	 *
	 * Is forbidden.
	 */
	egd_dumperFile_t(
		const egd_dumperFile_t&)
	 = delete;


	/**
	 * \brief	The copy assignment.
	 *
	 * Is forbidden.
	 */
	egd_dumperFile_t&
	operator= (
		const egd_dumperFile_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * This will empty other and transfer it to *this.
	 * Note that this constructor is implemented by hand.
	 *
	 * \param  other	The source.
	 */
	egd_dumperFile_t(
		egd_dumperFile_t&& other);


	/**
	 * \brief	Move assignment.
	 *
	 * This function moves other into *this.
	 * If *this is an open file, it will be tried
	 * to close it.
	 *
	 * \param  other
	 */
	egd_dumperFile_t&
	operator= (
		egd_dumperFile_t&& other);


	/**
	 * \brief	Destructor.
	 *
	 * Will colse the file, if open.
	 * Note that due to the designe of HDF5
	 * it could be, that the file remains open.
	 * But if this class is used properly, this
	 * will not happen.
	 *
	 * This funtion is implemented by hand.
	 * And is for technicall reason considered as
	 * a managing function.
	 */
	~egd_dumperFile_t();


	/**
	 * \brief	The default constructor.
	 *
	 * Is private and defaulted.
	 */
private:
	egd_dumperFile_t();


	/*
	 * =======================
	 * Status Functions
	 */
public:
	/**
	 * \brief	Returns true if a file association is present.
	 *
	 * A file assoication is that a file name is regisytered inside *this.
	 */
	bool
	hasFile()
	 const
	 noexcept
	{
		return (m_filePath.size() > 0);
	};


	/**
	 * \brief	This fucntions returns the associated file.
	 *
	 * \throw 	If *this as no associatetion.
	 */
	const FilePath_t&
	getFilePath()
	 const;


	/**
	 * \brief	Returns true if the file *thsi referenes to exists.
	 *
	 * \throw 	If *this has no association.
	 */
	bool
	fileExist()
	 const;


	/**
	 * \brief	Returns true if *this represnets an open file.
	 *
	 * \throw	If no file association is pressent an exception is generated.
	 */
	bool
	isOpen()
	 const;


	/*
	 * ====================
	 * Dump Controll Functions
	 *
	 * These functions deals with the controlling
	 * of the dunp process.
	 */
public:
	/**
	 * \brief	This function returns true, if pIdx is inside
	 * 		 the constant marker property set.
	 *
	 * The set of constant marker is different from the set of the _recorded_
	 * constant marker. The recorded markers are a subset of constant marker.
	 * The set of recorded const markers is created when a new marker version
	 * is created. All properteries that are inside the set of constant markers
	 * and are also inside the set of constant markers will be added to the
	 * set of recorded const marker.
	 * You can see this set as the set of marker properties that could potentially
	 * be considered constant and the other as the set of properties that are
	 * considered constant.
	 * Also this set is technically fix, during the lifetime of the dumper.
	 * It is also filled by the user.
	 *
	 * \param  pIdx		The index to test.
	 *
	 * \note	It is not clear (anymore) why this distinction was made.
	 */
	bool
	isInConstMarkerSet(
		const PropIdx_t& 	pIdx)
	 const;


	/**
	 * \brief	This function returns true if pIdx is a recorded
	 * 		 constant property.
	 *
	 * See also the explanation of the isInConstMarkerSet() function for a discussion.
	 * This set is only static until teh next marker version was created. It is also
	 * controlled by the dumper object.
	 *
	 * \param  pIdx		The index to test.
	 */
	bool
	isRecordedConstMarkerProperty(
		const PropIdx_t& 	pIdx)
	 const;


	/**
	 * \brief	This function returns true, if pIdx is a marker property
	 * 		 that is completly ignored.
	 *
	 * \param  pIdx 	The index to test.
	 */
	bool
	isIgnoredMarkerProperty(
		const PropIdx_t& 	pIdx)
	 const;


	/**
	 * \brief	This function adds a marker property to the ignore list.
	 *
	 * \param  pIdx		Marker property to add.
	 */
	egd_dumperFile_t&
	addIgnoredMarkerProperty(
		const PropIdx_t& 	pIdx);


	/**
	 * \brief	This function adds the given property to the list of
	 * 		 constant marker properties.
	 *
	 * The property pIdx, which must be valid and a representant, is added
	 * to the list of constant marker properties, this means they are
	 * not dumped during a regular state dumping, but hadlinked.
	 * Note that this only works if *this was not yet open.
	 * It is not possible to add positions to the list.
	 *
	 * \param  prop		The position to i
	 */
	egd_dumperFile_t&
	addConstantMarkerProperty(
		const egd_propertyIndex_t& 	prop);


	/**
	 * \brief	This function allows to remove a marker property from
	 * 		 the set of constant markers.
	 *
	 * This function is intended for the automatic configuration of the dumper
	 * by other objects. This function imposes the same requierement on the
	 * property as the addConstantMarkerProperty() function.
	 * If the marker property is not found in the set this function has no
	 * effect, thus it is not considered an error. It is also not considered
	 * an error if the property is uinside the ignored list. In that case the
	 * marker property will still be ignored. This is a different behaviour
	 * than the add functionality exhibit, which considers this as an error.
	 *
	 * \param  prop		The property to remove from the list.
	 */
	egd_dumperFile_t&
	rmConstantMarkerProperty(
		const egd_propertyIndex_t& 	prop);


	/**
	 * \brief	This function will add the marker velocity to the set
	 * 		 of the ignored properties.
	 *
	 * This functionis basically a short hand for the full expression.
	 * It is provided for uniformity.
	 */
	egd_dumperFile_t&
	ignoreMarkerVel();


	/**
	 * \brief	This function returns true if none of the marker
	 * 		 velocities are inside the the ignore list.
	 *
	 * This is the intrinsic marker velocity and not the feel velocity.
	 */
	bool
	markerVelAreDumped()
	 const;


	/**
	 * \brief	This function instructs *this to dump the feel velocity.
	 *
	 * The fell velocity is the velocity that is used to move the markers.
	 * Note that it is also possible to control the dumping over the
	 * ignore interface.
	 * Note that this function will also remove the feel velocity from the
	 * set of ignored markers.
	 */
	egd_dumperFile_t&
	dumpFeelVelocity();


	/**
	 * \brief	This function returns true if feel velocities are dumped.
	 *
	 * This function will not only check the internal state, but also the
	 * ignore set. Note the constant set is not checked.
	 */
	bool
	feelVelAreDumped()
	 const;


	/**
	 * \brief	This function allows to set the probability that a marker
	 * 		 will be dumped or not.
	 *
	 * Note this is only determined before a version is created. The set will
	 * remain fix until the new version is generated.
	 *
	 * \param  p		The dump probability.
	 *
	 */
	egd_dumperFile_t&
	markerDumpProbability(
		const Numeric_t 	p);



	/**
	 * \brief	Add the property prop to the list of properties
	 * 		 that are ignored for the grid.
	 *
	 * Note that it is not possible to ignore the heating term.
	 * However you can ignore the orther parts of the heating term.
	 * See the onlyFullHeatingTerm() function for more.
	 *
	 * \param  prop 	The property that should be ignored.
	 */
	egd_dumperFile_t&
	ignoreGridProp(
		const egd_propertyIndex_t& 	prop);


	/**
	 * \brief	This function allows only to dump the full heating term.
	 *
	 * This will instruct the dumper to fully ignore the different components
	 * of the heating term. This will not manipulate the ignore lists, but
	 * set a flag in the the class itself.
	 */
	egd_dumperFile_t&
	onlyFullHeatingTerm();


	/**
	 * \brief	This function will disable the dumping of strain.
	 *
	 * This means the strain rate is not dumped. This is only possible
	 * if *this was not yet open.
	 */
	egd_dumperFile_t&
	noStrainRateDump();


	/**
	 * \brief	This function will disable the dumping of the stress.
	 *
	 * If this function was called, the stress will not be dumped by *this.
	 * If the file was already opened an error is generated.
	 */
	egd_dumperFile_t&
	noStressDump();


	/**
	 * \brief	If this function was called, only grid properties that are
	 * 		 representants are dumped, others are ignored.
	 */
	egd_dumperFile_t&
	onlyDumpRepOnGrid();


	/**
	 * \brief	This function will instzruct the dumper to not
	 * 		 dump the temperature solution.
	 *
	 * This function should only be used if the null temperature solver
	 * is used. Also it has only an effect if the file was not opened
	 * before.
	 * Note if this is optuion is in place, the heating term will also be
	 * not written.
	 */
	egd_dumperFile_t&
	noTempSol();



	/*
	 * ======================
	 * Managing Functions
	 */
public:
	/**
	 * \brief	This function opens the file.
	 *
	 * This functions opens the associated file.
	 * If *this is already open an error is generated.
	 * The open status is returned, so it should return true,
	 * unless yolu are in big truble.
	 *
	 * This function will open the file in read write state
	 * and if the file does not exist it will be created.
	 *
	 * \throw 	If an error is detected.
	 */
	bool
	openFile();


	/**
	 * \brief	This functon closes the assoicated file.
	 *
	 * This function closes the file.
	 * If *this is not open an error is generated.
	 *
	 * This function returns the open status of *this.
	 * Thus it should return false, if not you have problems.
	 */
	bool
	closeFile();


	/**
	 * \brief	This function tries to flash the file.
	 *
	 * This function will call the internaly flash function of
	 * the HDF5 file, als pointed out in the documenteation,
	 * the OS is responsibyl to do it in the end.
	 *
	 * \throw	If an error occured during flushing or
	 * 		 if *this is not open.
	 */
	void
	flushFile();



	/*
	 * =========================
	 * HL Dumper
	 */
public:
	/**
	 * \brief	This function is used to commit the inital
	 * 		 state of the simulation.
	 *
	 * This function will not initiate a full dump of the markers
	 * nor the grid properties.
	 *
	 * Instead it will create the first version of the constant
	 * markers, which involves dumping the constant markers into
	 * a special region.
	 * And creating the first version of the grid geometry.
	 *
	 * It is assumed that the initial state is contained in
	 * the first dump of the simulation.
	 *
	 * \param  grid		The grid that should be dumped.
	 * \param  mColl 	The initial marker collection.
	 *
	 * \throw	If an error happens.
	 */
	void
	dumpInit(
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl);


	/**
	 * \brief	This function will dump the passed state to file.
	 *
	 * This function will handle anything on its own. It will open the
	 * file if needed and then it will write everything to the dump file.
	 * It will also close itself it was requested.
	 * If the file was open it will issue a flush operatioon, to
	 * ensure that all was written to disc.
	 *
	 * It will dump the data into a unique location.
	 *
	 * \param  grid		The grid that should be dumped.
	 * \param  mColl 	The marker collection.
	 * \param  stress 	The stress.
	 * \param  strainRate	The strain rate.
	 * \param  mSol 	The solution of the Stokes solver.
	 * \param  tSol 	The solution of the temperature solver.
	 * \param  heatingTerm	This is the heatingterm.
	 * \param  tStepIdx	The id of the current step.
	 * \param  currTime	The current time.
	 * \param  thisDT	The used timestep.
	 *
	 * This function should be called at the very end.
	 * The current time is the time of the time the markers are
	 * and dt is the timestep that will be applied, to evolve the system.
	 *
	 * Inside the base folder a link will be created that points to the
	 * current grid geometry.
	 *
	 * Note if calling code use a conditional dumping, the dumper
	 * itself will ensure that the states are labeled in a consecuive
	 * way, the state index has not be tweaked to ensure this.
	 * An attribute will be created to store the real simulation index.
	 */
	void
	dumpState(
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl,
		const DeviatoricStress_t&	stress,
		const DeviatoricStrainRate_t&	strainRate,
		const MechSolverResult_t& 	mechSol,
		const TempSolverResult_t&	tempSol,
		const HeatingTerm_t& 		heatingTerm,
		const Index_t 			tStepIdx,
		const Numeric_t 		currTime,
		const Numeric_t 		thisDt);


	/**
	 * \brief	This function is for dumping the final state.
	 *
	 * This function should be called after the main loop exited.
	 * It is provided to store the last changes that are made
	 * and not associated to any state. Since thsi only affects
	 * the markers a dump of the markers is performed, but no
	 * other actions.
	 * The data is stored in a spearate section of the file.
	 *
	 * The passed step index should be the global index.
	 *
	 * \param  mColl 	The marker collection.
	 * \param  finalTime	This is the final time that is recorded.
	 */
	void
	dumpFinal(
		const MarkerCollection_t& 		mColl,
		const Numeric_t 			finalFime,
		const Index_t 				stateIdx);



	/*
	 * =========================
	 * High Level Dumper
	 * This functions are the high level dumper.
	 * They operates on properties directly.
	 */
public:
	/**
	 * \brief	This function dumps a marker property.
	 *
	 * This function is used to dump a marker property into
	 * the given location. *this must be open otherwhise an
	 * error is generated.
	 *
	 * Note that this function will restec the marker filter
	 * that is defined by the modulo operation. It will also
	 * call the annotate function for marker properties.
	 *
	 * However it will not check if the property is inside
	 * the constant list, it will dump app passed properties.
	 *
	 * If the list indexing does not exists, but should exists
	 * it will create it automatically.
	 *
	 * Note that this function will record the number of markers
	 * that are dump, on its first call. It will then check
	 * on each of the following calls, that the number remained
	 * the same.
	 *
	 * \param  mProp	The marker property to dump.
	 * \param  prop 	The property of the grid that should be dumped.
	 * \param  groupPath	Where to save the property. This is only the path
	 * 			 The name is determined by the property index.
	 * \param  absPath	If set to true, groupPath is interpreted as final
	 * 			 dump location.
	 *
	 *
	 * \throw	If an error happens or *this is not open.
	 *
	 * \info	Because of the fact that the markerproperties
	 * 		 and grid positions are the same type, and different
	 * 		 functions are needed for them, they are distinguished
	 * 		 by name and not by type.
	 */
	void
	dumpMarker(
		const MarkerProperty_t& 	mProp,
		const egd_propertyIndex_t& 	prop,
		const GroupPath_t& 		groupPath,
		const bool 			absPath = false);


	/**
	 * \brief	This function dumps a grid property.
	 *
	 * This function is used to dump a grid property
	 * into the gven location. *this must be open
	 * otherwhise an error is generated.
	 *
	 * Note that this class stores the matrix in transposed
	 * fassion. The reason is that the memorey order between
	 * C/HDF5 and Eigen is different.
	 *
	 * Note that this function will not apply the filerting,
	 * for grid properties, this means it sill store it at
	 * any location that is requested.
	 * It will also call automatically the annotation function.
	 *
	 * \param  gProp	The grid property to dump.
	 * \param  groupProp	Where to save the property. This is only the path.
	 * 			 the "file name" is determioned by the property index.
	 * \param  absPath	If set the groupPath will be consideret the final path.
	 * 			 And no modifications are done on it.
	 *
	 * \throw	If an error happens or *this is not open.
	 */
	void
	dumpGridProperty(
		const GridProperty_t& 		gProp,
		const GroupPath_t& 		groupPath,
		const bool 			absPath = false);


	/**
	 * \brief	This function is able to dump a grid position.
	 *
	 * This function stores a passed grid position to a certain location.
	 * It expects the path to a group, in this group it will create a
	 * subgrup with the name of the grid type, encoded as by priv_gTypeStr().
	 * Inside this group it will create two data sets, called "X" and "Y".
	 *
	 * \param  gType	The grid type that is used.
	 * \param  X		x node positions.
	 * \param  Y 		y node positions.
	 * \param  groupPath	The group to where we save to.
	 */
	void
	dumpGridPos(
		const eGridType 	gType,
		const GridPosition_t& 	X,
		const GridPosition_t& 	Y,
		const GroupPath_t& 	groupPath);


	/**
	 * \brief	This is a function that allows to be compatible with
	 * 		 the function of the grid geometery.
	 */
	void
	dumpGridPos(
		const eGridType 	gType,
		const std::pair<GridPosition_t, GridPosition_t>& 	nodePos,
		const GroupPath_t& 	groupPath)
	{
		this->dumpGridPos(gType, nodePos.first, nodePos.second, groupPath);
		return;
	};


	/**
	 * \brief	This is a function that allows to be compatible with
	 * 		 the function of the grid geometery.
	 */
	void
	dumpGridPos(
		const eGridType 	gType,
		const GridGeometry_t&	gridGeo,
		const GroupPath_t& 	groupPath)
	{
			pgl_assert(::egd::isValidGridType(gType));
		this->dumpGridPos(gType, gridGeo.getGridPoints(gType), groupPath);
		return;
	};


	/*
	 * ==========================
	 * Container Dumpers
	 * These functions dumping properties.
	 * They operate in full containers.
	 */
private:
	/**
	 * \brief	This function dumps a grid contaihner.
	 *
	 * It will respect the settings of *this, meaning teh representants and
	 * the ignore list, are consulted before dumnping.
	 * This function expects to have a group name, where the state is dumped.
	 * It will create a subgroup with the name "grid" and dump the properties
	 * there. For that it will use the high level dumper.
	 *
	 * It this function detectes a modified grid, meaning isNewVersion()
	 * returns true, new a new version of the geometry is created, before
	 * the dumping is initiated.
	 *
	 * This function will use the internal count for the versions.
	 *
	 * It return value indicates if a new grid geometry was created,
	 * i.e. if the version was increased (true) or not (false).
	 *
	 * \param  grid			The grid container, with geometry that should be dumped.
	 * \param  BASE_PATH		The location where the state should be dumped.
	 */
	bool
	dumpGridContainer(
		const GridContainer_t& 		grid,
		const GroupPath_t& 		BASE_PATH);


	/**
	 * \brief	This function dumps a marker collection.
	 *
	 * It expects to get the group path of the sate. It will
	 * create a subgrup "marker" and dump all properties
	 * inside it.
	 * This function will respect the constant setting.
	 * Meaning that if the marker property is constant, it
	 * will create a hadlink instead.
	 *
	 * It this function detects a new version in of the marker,
	 * it will automatically create a new version of the markers
	 * before the dumping of the collection starts.
	 *
	 * This function will use the internal state index.
	 *
	 * It return value indicates if a new versions of the constant
	 * were created or not.
	 *
	 * \param  mColl		The marker collection that is used.
	 * \param  BASE_PATH		The location where the state should be dumped.
	 */
	bool
	dumpMarkerCollection(
		const MarkerCollection_t& 	mColl,
		const GroupPath_t& 		BASE_PATH);


	/**
	 * \brief	This function dumps stress and strain.
	 *
	 * This function expects to get a path to the base group.
	 * It will create a group "Stress" and "StrainRate" respectively.
	 * It will then dump tensors inside there.
	 * Note this function will respect the settings of the stress and
	 * strain dumping controle.
	 *
	 * \param  Stress	The stress object.
	 * \param  StrainRate	The strain rate object.
	 * \param  BASE_PATH	The base path where we dump to.
	 */
	void
	dumpStrainStress(
		const DeviatoricStress_t& 		stress,
		const DeviatoricStrainRate_t&		strainRate,
		const GroupPath_t& 			BASE_PATH);


	/**
	 * \brief	This function will dump the temperature solution
	 * 		 and the heatoing term.
	 *
	 * It will respect all settings, it will dump it into the subgroup
	 * "grid" and thus fake a grid property.
	 * The components of the heating term, will be dumped in a special
	 * sub folder if requested.
	 */
	void
	dumpTemperatureSol(
		const TempSolverResult_t& 		tempSol,
		const HeatingTerm_t& 			heatingTerm,
		const GroupPath_t& 			BASE_PATH);


	/**
	 * \brief	This function will dump the mechanical solution.
	 */
	void
	dumpMechSol(
		const MechSolverResult_t& 		mechSol,
		const GroupPath_t& 			BASE_PATH);



	/*
	 * =========================
	 * =========================
	 * Private Functions
	 */

	/*
	 * =======================
	 * Geometry Version Functions
	 * Here are internal functions that are concerned with
	 * the managiung of the grid geometry version.
	 */
private:
	/**
	 * \brief	This function creates a new version of the grid geometry.
	 *
	 * This will increase the version counting of the geometry by one.
	 * It will annotate the version with the internal dump state.
	 * Note if this is the first version, it will annotate the state index with
	 * the non existing state -1.
	 *
	 * \param  gridGeo		The grid geometry that should be dumped.
	 */
	egd_dumperFile_t&
	priv_createNewGridGeometryVersion(
		const GridGeometry_t& 		gridGeo);



	/**
	 * \brief	This function returns a name of the passed grid type.
	 *
	 * This name is such that it can be used as path.
	 * This function should be used exclusively, if a grid
	 * type should be translated to text.
	 *
	 * \param  gType	The grid type that should be translated.
	 */
	std::string
	priv_gTypeStr(
		const eGridType 	gType)
	 const;


	/**
	 * \brief	This function returns the path to the currently active grid
	 * 		 geometry.
	 *
	 * This function returns a pair, first is the path to X and second is the
	 * path to Y. This function will call the geo path function and append
	 * the grid type encodeing.
	 *
	 * \param  gType	The grid type that should be examined.
	 */
	std::pair<GroupPath_t, GroupPath_t>
	priv_getCurrentGridGeometry(
		const eGridType 	gType)
	 const;


	/**
	 * \brief	This function returns the current group path of the geometry.
	 *
	 * The returned path will point to a group.
	 * this group will contain all grid geometry positions. For each grid
	 * type a subgroup is created, containing two data sets "X" and "Y" with
	 * obvious content.
	 *
	 * Note that the returned path has a trailing "/".
	 */
	GroupPath_t
	priv_getCurrentGeoGroupPath()
	 const;


	/*
	 * =====================
	 * Marker Version Helper Function
	 * This function helps with the managing of the
	 * marker versions.
	 */
private:
	/**
	 * \brief	This function creates a new marker version.
	 *
	 * It will iterate over all marker properties, stored inside the
	 * marker collection all marker properties that are found, will
	 * be dumped into a new version folder.
	 * It will anotate the dump version folder with the internal
	 * dump state index.
	 *
	 * The function has an inactive code section. Activating the section
	 * would allow to change the numbers of markers that are inside the
	 * collection, otherwhise the number has to remain fix during the
	 * whole simulation.
	 *
	 * \param  markerColl		The marker collection that is dumped.
	 */
	egd_dumperFile_t&
	priv_createNewMarkerVersion(
		const MarkerCollection_t& 	markerColl);


	/**
	 * \brief	This function returns the the currently active marker version path.
	 *
	 * The returned path, with trailing "/", points to a group that contains all
	 * marker properties that are considered constant.
	 */
	GroupPath_t
	priv_getCurrentConstMarkerGroupPath()
	 const;


	/**
	 * \brief	This function returns the path to the constant marker property.
	 *
	 * Note that the passed marker must be inside the constant property set.
	 *
	 * \param  pIdx		The marker property that we want.
	 */
	GroupPath_t
	priv_getCurrentMarkerPropPath(
		const PropIdx_t& 	pIdx)
	 const;







	/*
	 * =============================
	 * Annotating
	 * This functions allows to annotate datasets.
	 * This are functions that creats certain attributes
	 * on a data set.
	 */
private:
	/**
	 * \brief	This function will annotate the grid property that
	 * 		 is located at annotateTo.
	 *
	 * This function calls the size and the type annotate function.
	 *
	 * \param  gProp	The grid property.
	 * \param  annotateTo	Path to the data set that should be annotated.
	 */
	void
	priv_annotateGridProp(
		const GridProperty_t& 	gProp,
		const GroupPath_t& 	annotateTo);


	/**
	 * \brief	This function annotates the marker property that
	 * 		 is located at annotateTo.
	 *
	 * This function will add an attribut with the name "nMarkers" to
	 * the location annotateTo.
	 *
	 * \param  mProp	The marker property.
	 * \param  annotateTo	Path to the data set that should be annotated.
	 */
	void
	priv_annotateMarker(
		const MarkerProperty_t& 	mProp,
		const GroupPath_t& 		annotateTo);


	/**
	 * \brief	This function is a helper function that is used by the
	 * 		 property dumper.
	 *
	 * It will anotate the data set of a property with information about
	 * which grid type was used. This is done simply by creating the
	 * appropriate attribute.
	 * Actually it will create two of them, one for the extension size
	 * and one for the type.
	 *
	 * 	Ext     :->	Extended grid
	 * 	Reg     :->	Regular grid
	 * 	StVx    :->	Staggered VX grid
	 * 	StVy    :->	Staggered VY grid
	 * 	StPress :->	Staggered pressure grid
	 * 	CC      :->	Cell centre grid
	 * 	Basic  	:->	The basic nodal points.
	 *
	 * \param  gType	The grid type that should be encoded.
	 * \param  attachTo	Which data set should be annotated.
	 */
	void
	priv_attacheGridType(
		const eGridType 	gType,
		const GroupPath_t& 	attachTo);


	/**
	 * \brief	This function is used to write the size
	 * 		 of the propery to attributes.
	 *
	 * This will not write the size of the matrix, but the
	 * size of the basic nodal grid, meaning Nx() and Ny().
	 *
	 * \param  gProp	The grid property.
	 * \param  attacheTo	Which dataset we update.
	 */
	void
	priv_attacheGridSize(
		const GridProperty_t& 	gProp,
		const GroupPath_t& 	attacheTo);


	/*
	 * ========================
	 * Raw Dumping
	 *
	 * These function performs raw damping operations.
	 * Thy operate on pointers directly.
	 */
private:
	/**
	 * \brief	This function is the internal work horse for dumping files.
	 *
	 * This function is possible to write something into the file.
	 * For this *this must be open. It operates on consecutive memory
	 * It assumes that a full array is passed to it and is written.
	 * There is no possibility to extract something (hyperslap).
	 *
	 * It wiill write the pointer array, to the location inside the file
	 * that is specified, if the folder does not exists it will
	 * create it. If the name however already exists it will fail.
	 *
	 * \param  data 	This is the pointer to the data that should be written.
	 * \param  rows		The number of rows the matrix has, counted in elements.
	 * \param  cols 	The number of cols the matrix has, counted in elements.
	 * \param  groupPath 	Where to save the data set.
	 * \param  dType	The data type that should be used.
	 *
	 * Technically this function could be constant, but it is conseptionaly bad
	 * to make it constant.
	 *
	 */
	void
	priv_storeData(
		const void* const 	data,
		const Size_t 		rows,
		const Size_t 		cols,
		const GroupPath_t& 	groupPath,
		const HID_t 		dType);


	/**
	 * \brief	This is the specialization for the numeric type.
	 *
	 * This function bascally is a wrapper.
	 * See the main store function.
	 */
	void
	priv_storeData(
		const Numeric_t* const 	data,
		const Size_t 		rows,
		const Size_t 		cols,
		const GroupPath_t&	groupPath);



	/*
	 * =======================
	 * Attribute
	 * This functions allows to create attributes.
	 * This are low level functions
	 */
public:
	/**
	 * \brief	This function creates a scalar atribute.
	 *
	 * The attribute is attached to the named data set, which is opened.
	 * Thy type of the scallar is determined by the type to write.
	 *
	 * \param  data 	Pointer to the SCALAR that is used as atribute.
	 * \param  attacheTo 	To which object the attributed should be attached to.
	 * \param  attrName	The name of the attribute (should only contain ASCII).
	 * \param  dType 	The data type that is used
	 */
	void
	priv_attacheAttribute(
		const void* const 	data,
		const GroupPath_t& 	attacheTo,
		const std::string&	attrName,
		const HID_t 		dType);


	/**
	 * \brief	This is an atribute for a float.
	 *
	 * See general atribute function.
	 */
	void
	priv_attacheAttribute(
		const Numeric_t* const 	data,
		const GroupPath_t& 	attacheTo,
		const std::string&	attrName);


	/**
	 * \brief	This is an atribute for an int
	 *
	 * See general atribute function.
	 */
	void
	priv_attacheAttribute(
		const Index_t* const 	data,
		const GroupPath_t& 	attacheTo,
		const std::string&	attrName);


	/**
	 * \brief	This function attaches a string attribute to
	 * 		 to the dataset.
	 *
	 * For more information see the general function.
	 */
	void
	priv_attacheAttribute(
		const std::string& 	data,
		const GroupPath_t& 	attacheTo,
		const std::string& 	attrName);


	/**
	 * \brief	This function creates a boolean attribute.
	 *
	 * See generall attribute function.
	 */
	void
	priv_attacheAttribute(
		const bool 		data,
		const GroupPath_t& 	attacheTo,
		const std::string& 	attrName);





	/*
	 * ===========================
	 * Dataset Managing
	 * This functions operates directly on the managing
	 * of the data sets.
	 */
private:
	/**
	 * \brief	This function creates the dataset that is named by the path.
	 *
	 * The last element of the groupPath is interpreted as the datasets name.
	 * all groups that does not exists in the path will be created.
	 *
	 * The function returns the identifier for the data set.
	 * Note that the set is open.
	 *
	 * \param  groupPath 	The group path; last element is interpreted as dataset name.
	 * \param  dType	The type of the data set.
	 * \param  rows		The number of rows in the new data set.
	 * \param  cols		The number of columns in the new data set.
	 *
	 * This function will also create a data space object.
	 * Since it is requiered for the datset to be created.
	 * The data space will be closed by ths function.
	 *
	 */
	HID_t
	priv_createDataSet(
		const GroupPath_t& 	groupPath,
		const HID_t 		dType,
		const Size_t 		rows,
		const Size_t 		cols);


	/**
	 * \brief	This function creates a link.
	 *
	 * It will creat a hardlink at the location newName that
	 * points to the location target. It will check if target
	 * exists. Also the paths have to be absolute paths.
	 * *this needs to be open.
	 *
	 * \param  target	The source or target for the link.
	 * \param  newName	The new name of src.
	 */
	void
	priv_createHardLink(
		const GroupPath_t& 	target,
		const GroupPath_t& 	newName);



	/*
	 * =======================
	 * Private Members
	 */
private:
	//
	//File Managing
	HID_t 			m_id 	  = -666;		//!< This is the ID of teh file for HDF5, it is
								//!<  basically a *this pointer.
	FilePath_t 		m_filePath;			//!< This is the path to the HDF5 path.
								//!<  If empyt, then not associateion is pressent.

	bool 			m_isOpen  = false;		//!< Indicates if the file is open.
	bool 			m_wasOpen = false;		//!< This bool indicates if *this was open.
	Index_t 		m_nMarkers = -1;		//!< This is a variable that stores the numbers of markers.

	Index_t 		m_stateDumpIdx = -1;		//!< This is the internal index that is used for counting
								//!<  how many full state dumps are performed.
								//!<  -1 indicates that the init function has not yet run.

	//
	// Dump managing

	//General grid dumping
	GridPropIgnoreSet_t 	m_gridPropIgn;			//!< Properties on the grid to ignore.
	bool 			m_onlyRepGridDump = false;	//!< If true only representants are dumped.

	//Marker Dumping
	ConstMarkerPropSet_t 	m_constMarkerProp;		//!< Marker properties considered constant.
	ConstMarkerPropSet_t  	m_recordedConstMarkerProp;	//!< This is a list of the ACTUALL stored constant marker properties.
	MarkerPropIgnoreSet_t 	m_ignoredMarkerProp;		//!< Set of ignored marker properties.
	Numeric_t 		m_markerDumpProb = 1.0;		//!< This is the probability of the dumping marker.
	IndexArray_t 		m_idxDumpMarker;		//!< This is the index array of all markers we will dump.
	Int_t 			m_markerVersion = -1;		//!< This is a counter that is used count the version of markers that is in place.

	//Marker velocitie controll
	bool  			m_dumpFellMarkerVel = false;	//!< Indicates that the fell velocity of the marker should be dumped.

	//Grid Geometry dumper
	Int_t 			m_geoVersion = -1;		//!< This is the version of the grid geometry that is currently in place.

	//Temperature dumping
	bool 			m_onlyFullHeatTerm = false;	//!< Indicates if only the full heating term should be included.
	bool 			m_noTempSolDump   = false;	//!< If true the temperature solution will not be dumped.

	//Strain and stress controll
	bool 			m_noStrain = false;		//!< Strain is not dumped.
	bool 			m_noStress = false;		//!< Disable the dunmping of stress.
}; //End class(egd_dumperFile_t)



PGL_NS_END(egd)


