/**
 * \brief	This file contains the functions to dump the final state.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)



void
egd_dumperFile_t::dumpFinal(
	const MarkerCollection_t& 	mColl,
	const Numeric_t 		finalTime,
	const Index_t 			stateIdx)
{
	if(mColl.nMarkers() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The marker collection is empty.");
	};
	if(stateIdx < 0)
	{
		throw PGL_EXCEPT_InvArg("The state index was invalid it was " + std::to_string(stateIdx));
	};
	if(m_stateDumpIdx < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Internal state index is invalid, it was " + std::to_string(m_stateDumpIdx));
	};
	if(m_stateDumpIdx > stateIdx)
	{
		throw PGL_EXCEPT_LOGIC("The internal state index, " + std::to_string(m_stateDumpIdx)
				+ ", is larger than the global one, " + std::to_string(stateIdx));
	};
	if(pgl::isValidFloat(finalTime) == false)
	{
		throw PGL_EXCEPT_InvArg("The final time is invalid.");
	};
	if(finalTime < 0.0)
	{
		throw PGL_EXCEPT_InvArg("The final time is negative, it was " + std::to_string(finalTime));
	};


	//Store if the file is open.
	const bool fileWasOpen = this->isOpen();

	//Open the file if not open
	if(fileWasOpen == false)
	{
		this->openFile();
			pgl_assert(this->isOpen());
	};

	//This is the base name for the state dumping
	const std::string BASE_GROUP = "/finalState";	//This is the name of the dump folder without a trailing /
	const std::string BASE_NAME  = BASE_GROUP + "/";


	/*
	 * Dump the marker collection.
	 */
	{
		this->dumpMarkerCollection(mColl, BASE_NAME);	//Also use the internal counting
	}; //End scope: dump the marker

	/*
	 * Writting the time as attribute
	 */
	{
		this->priv_attacheAttribute(&finalTime, BASE_GROUP, "finalTime"    );
		this->priv_attacheAttribute(&stateIdx , BASE_GROUP, "trueStateIdx" );	//Here real state index
	}; //End: writting time


	/*
	 * If the file was open we will not close it,
	 * but if we opened it, we will colse it
	 */
	if(fileWasOpen == false)
	{
		// The file was not open so we close it now.
		this->closeFile();
	}
	else
	{
		//The file was open, so we flush
		this->flushFile();
	};

	return;
}; //End: final dumping


PGL_NS_END(egd)


