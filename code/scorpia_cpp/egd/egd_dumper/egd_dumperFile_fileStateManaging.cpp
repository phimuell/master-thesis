/**
 * \brief	This file implements the file state managing.
 *
 * This are functions such as open file, close it or flushing the file to disc.
 * Note that the destructor also belongs to this category.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>


//Include HDF5
#include <hdf5.h>	//We include the full library


//Include STD
#include <string>


PGL_NS_START(egd)



bool
egd_dumperFile_t::openFile()
{
	/*
	 * Test if the type we use for the identifier is the
	 * same that is used inside HDF5.
	 *
	 * If this check fail, the figuring out which HDF5 is included.
	 * CMake will tell you that in its output.
	 * Then go there and use grep to search for the type definition
	 * of "hid_t" and change it in the header.
	 */
	static_assert(std::is_same<egd_dumperFile_t::HID_t, hid_t>::value, "HDF5 uses a different identifier, see longer comment in source.");


	if(this->hasFile() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not open the file, because no associated file name.");
	};
	if(this->isOpen() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to open a file, that is already open.");
	};

	//First we must create and modify the access property list this is needed to make them safe
	//against stuff that is open. This will populate the class with default values
	const auto accesPropList = H5Pcreate(H5P_FILE_ACCESS);
	if(accesPropList < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Could not copy the default property list.");
	};

	/*
	 * Before we do anything, we will set the close degree to "SEMI".
	 * This means that open objects from the file, like a group will
	 * not go unnoticed, if that is the case, the call will end in
	 * failure.
	 */
	if(H5Pset_fclose_degree(accesPropList, H5F_CLOSE_SEMI) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Could not set the closing degree of the file, while closing.");
	};


	/*
	 * First we must test how we do proceeded.
	 * If the file exists we must open it and if not
	 * we must create it.
	 */
	if(this->fileExist() == true)
	{
		/*
		 * The file exists so we must open it
		 */
		m_id = H5Fopen(m_filePath.c_str(), H5F_ACC_RDWR, accesPropList);
	}
	else
	{
		/*
		 * The file does not exists, so we must create it.
		 * We will never overwrite data.
		 * We also use default property
		 */
		m_id = H5Fcreate(m_filePath.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, accesPropList);
	};

	/*
	 * Test if it has happened.
	 * Both return a negative value if the call failed.
	 */
	if(m_id < 0)
	{
		throw PGL_EXCEPT_RUNTIME("There was an error while accessing the HDF5 file.");
	};

	//Close the list
	if(H5Pclose(accesPropList) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("CXlould not close the property list.");
	};

	//Now set the flag for an opened file to true.
	this->m_isOpen  = true;
	this->m_wasOpen = true;		//We will never set it to false.


	return this->isOpen();		//Return the open status of *this
}; //End open file.


bool
egd_dumperFile_t::closeFile()
{
	//Can only close if *This is open
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to close a file that is not open.");
	};
	pgl_assert(this->hasFile()  ,	//Make some tests, I think it is nicer to check it that way
		   this->fileExist() );

	/*
	 * To be sure that an error does the least possible amount of damage
	 * we will now perform a flush operation and hope
	 * that the entiere file is safly writen to the disc.
	 */
	this->flushFile();


	/*
	 * Now we will close the file.
	 * This means that allo objects must be closed as well
	 * otherwhise this call will fail.
	 */
	if(H5Fclose(m_id) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("Could not close the file. Maybe still open objects, but also an error.");
	};


	/*
	 * Set the state of the internal valables.
	 * Also reset the ID
	 */
	m_id     = -666;
	m_isOpen =  false;


	//Return the file status
	return this->isOpen();
}; //End close


void
egd_dumperFile_t::flushFile()
{
	if(this->isOpen() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not flush a close file.");
	};

	/*
	 * We use the global scope to ensure that the file is realy flushed.
	 */
	if(H5Fflush(m_id, H5F_SCOPE_GLOBAL) < 0)
	{
		throw PGL_EXCEPT_RUNTIME("The flushing of the file failed.");
	};

	return;
}; //End: fluish the file


bool
egd_dumperFile_t::isOpen()
	const
{
	if(this->hasFile() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not determine the open status of an unassociated dumper.");
	};

	if(this->m_isOpen == true)
	{
		pgl_assert(this->fileExist(),	//File has to exists
			   m_id >= 0         ); //valid identifiers are not negative
		return m_isOpen;
	};

	//The file is not open
	return false;
}; //End: isOpen


bool
egd_dumperFile_t::fileExist()
 const
{
	if(this->hasFile() == false)
	{
		throw PGL_EXCEPT_LOGIC("The dump file has no associated file.");
	};

	return (::boost::filesystem::exists(m_filePath) == true);
};


const egd_dumperFile_t::FilePath_t&
egd_dumperFile_t::getFilePath()
 const
{
	if(this->hasFile() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to access the file path, but the dumper does not have an associated file.");
	};

	return m_filePath;
};


egd_dumperFile_t::~egd_dumperFile_t()
{
	/*
	 * The constructor will close the file.
	 * This will be done by the close function.
	 * However we will guard the call.
	 * Also if an exception happens unexpected
	 * will be called.
	 */
	if(this->isOpen() == true)
	{
		try
		{
			/*
			 * We now try to close the file.
			 */
			if(this->closeFile() != false)
			{
				throw PGL_EXCEPT_RUNTIME("The return status of the close function is not false, while called from the destructor.");
			};
		}
		catch(const std::exception& e)	//Catches all, also the pgl ones
		{
			std::cerr
				<< "Closing of the dumper file " << m_filePath << " has resulted in an exception.\n"
				<< "Its text reads as:\n"
				<< e.what()
				<< std::endl;

			std::terminate();
		}
		catch(...)
		{
			std::cerr
				<< "An unkown exception was generated while the dumper file was closed."
				<< std::endl;

			std::terminate();
		};
	}; //End if: has an open file
}; //End destructor

PGL_NS_END(egd)

