#pragma once
/**
 * \brief	This file contains an interface for the temperature solver.
 *
 * The temperature solver solves the temperaure equation and allows to optain
 * the temperature distribution of the next time step.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverArgument.hpp>

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_TempSolver_i
 * \brief	Interface for solving the temperature equation.
 *
 * This solver solves the following equation on the grid:
 * 	 \rho C_p \cdot \D_t[T] = - \div[\vec{q}] + H_s
 *
 * Where:
 * 	\vec{q}  := -k * \grad_x[T] 	~ The temperature flow.
 * 	k        :~  	Thermal conductivity (variable)
 * 	\rho C_p :~ 	Volumetric heat capacity.
 *
 * The solution is computed at the new timestep.
 * It is also important that the object that is returned,
 * does not represent the new temperature distribution, but
 * the difference between the new temperature distribution
 * and the current one, this is described in the
 * egd_TempSolverResult_t object.
 */
class egd_TempSolver_i
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using Matrix_t 	= egd_Matrix_t<Numeric_t>;	//!< This is the type of the reuslts.

	using SolverResult_t 	= egd_tempSolverResult_t;		//!< This is the result type of the temperature solver.
	using SolverResult_ptr 	= SolverResult_t::TempSolverResult_ptr;	//!< Pointer to the result type.

	using SolverArg_t 	= egd_tempSolverArg_t;			//!< This is the argument type that is used.
	using SolverArg_ptr 	= SolverArg_t::TempSolverArg_ptr;	//!< This is the pointer type of the argument.

	using TempSourceTerm_t  = SolverArg_t::TempSourceTerm_t;	//!< This is the temperature source term.
	using TempSourceTerm_ptr= SolverArg_t::TempSourceTerm_ptr;	//!< This is the managed instance of the source term.

	using GridContainer_t 	= egd_gridContainer_t;			//!< This is the grid container.
	using HeatingTerm_t 	= egd_heatingTerm_t;			//!< This is the heating term for the temperature equation.

	using TempSolver_ptr    = std::unique_ptr<egd_TempSolver_i>;	//!< This is the pointer type for the temperature solver.

	using PropToGridMap_t   = GridContainer_t::PropToGridMap_t;	 //!< This is the map that deperimes which times are pressent.


	/*
	 * ===========================
	 * Constructors
	 *
	 * The destructor is public and virtual.
	 * All other constructors are defaulted
	 * and protected.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is virtual and defaulted.
	 */
	virtual
	~egd_TempSolver_i()
	 noexcept;

protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_TempSolver_i()
	 noexcept;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_TempSolver_i(
		const egd_TempSolver_i&)
	 noexcept;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_TempSolver_i&
	operator= (
		const egd_TempSolver_i&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_TempSolver_i(
		egd_TempSolver_i&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_TempSolver_i&
	operator= (
		egd_TempSolver_i&&)
	 noexcept;


	/*
	 * ========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function compoutes the new
	 * 		 temperature distribution.
	 *
	 * Remember only the difference is returned.
	 * This function solves the temperature equation and
	 * computes the new distribution. It assumes that the
	 * termal conductivity does sptially vary.
	 *
	 * The temperature of the current step is taken from
	 * the grid object, as well as all other parameters.
	 * The temperature of the current state has to respect
	 * the boundary conditions of the temperature equation.
	 *
	 * It is allowed that the passed argument type is modified.
	 *
	 * \param  grid		The grid container that should be used.
	 * \param  heatingTerm	This is the heating term, it is the source.
	 * \param  solArg	This is the argument type.
	 * \param  solResult	This is the colver result.
	 *
	 * \throw	An error is raised if something happened.
	 *
	 */
	virtual
	void
	solve(
		const GridContainer_t& 		grid,
		const HeatingTerm_t& 		heatingTerm,
		SolverArg_t* const 		solArg,
		SolverResult_t* const 		solResult)
	 = 0;


	/**
	 * \brief	This function is the same as the one above,
	 *
	 * but operates on the managed pointer types directly.
	 * This function is purly convenient.
	 *
	 * This function is final.
	 */
	virtual
	void
	solve(
		const GridContainer_t& 		grid,
		const HeatingTerm_t& 		heatingTerm,
		SolverArg_ptr& 			solArg,
		SolverResult_ptr& 		solResult)
	 final;


	/**
	 * \brief	This fucntion returns an instance of the result object.
	 *
	 * This function is something like the builder function of the
	 * result class. The returned object must not be initalized.
	 *
	 * This function has a default implementation and generates the
	 * standard result class.
	 */
	virtual
	SolverResult_ptr
	creatResultContainer()
	 const;


	/**
	 * \brief	This function generates a standard argument object.
	 */
	virtual
	SolverArg_ptr
	createArgument()
	 const;


	/**
	 * \brief	This function creats a temeperature source term.
	 *
	 * This source term is not allocated but only default constructed.
	 * The user must allocate it manualy.
	 * Deriving classes should implement that function.
	 */
	virtual
	TempSourceTerm_ptr
	creatSourceTerm()
	 const;


	/**
	 * \brief	This function returns a property map that is needed by the solver.
	 *
	 * This function allows to solver independend create the needed property.
	 * The returned property map must then be incooperated by the setup object.
	 *
	 * There is not default implementation.
	 */
	virtual
	PropToGridMap_t
	mkNeededProperty()
	 const
	 = 0;


	/*
	 * ================================
	 * Status functions.
	 *
	 * These functions allows to quiery the status of *this.
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 *
	 * This is mostly used for debugging and output.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/**
	 * \brief	This function returns the grid type
	 * 		 on which the solver operates.
	 *
	 * This function allows to query on which grid the
	 * solver is operating.
	 */
	virtual
	eGridType
	getGridType()
	 const
	 = 0;


	/**
	 * \brief	This function return true if *this operates
	 * 		 on an extended grid.
	 *
	 * The default of this function is implemented by calling
	 * the getGridType() function.
	 */
	virtual
	bool
	onExtendedGrid()
	 const;


	/**
	 * \brief	This function returns true if *this operates
	 * 		 on a regular grid.
	 *
	 * The default of this function is reduced to getGridType().
	 */
	virtual
	bool
	onRegularGrid()
	 const;



	/*
	 * ======================================
	 * F A C T O R Y
	 */
public:
	/**
	 * \brief	This function creates the implementation
	 * 		 of the temperature solver.
	 *
	 * This function uses the passed configuration object to
	 * determine which solver should be created. It will then
	 * forward the configuration object to the appropriate
	 * implementation.
	 *
	 * \param  confObj	The configuration object.
	 */
	static
	TempSolver_ptr
	BUILD(
		const egd_confObj_t& 		confObj);

}; //End: class(egd_TempSolver_i)

PGL_NS_END(egd)


