#pragma once
/**
 * \brief	This interface is responsible for setting up the problem.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_MarkerRehology_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)


//FWD of teh dumper.
class egd_dumperFile_t;


/**
 * \class 	egd_MarkerSetUp_i
 * \brief	This class will set up the grid and the marer collection.
 *
 * This class will set up and create the marker collection and the grid
 * container. This interface represnets a higly problem depending
 * task so a lot is left to the implementing sub class.
 *
 * Note that this is not a pure interface. It implements a managing function.
 * Maybe the functionality will be moved to some base class, but since the
 * functions are only internal, it does not mather.
 */
class egd_MarkerSetUp_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Size_t 	 = ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
							//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	 = ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	 = ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t  = ::pgl::Numeric_t;		//!< This is the float type that is used

	using GridContainer_t 		= egd_gridContainer_t;			//!< This is the container for the grid.
	using PropIdx_t 		= GridContainer_t::PropIdx_t;		//!< This i sthe property index
	using PropList_t 		= PropIdx_t::PropList_t;		//!< List of several properties.
	using PropToGridMap_t 		= GridContainer_t::PropToGridMap_t;	//!< This is the property grid map.
	using GridGeometry_t 		= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.

	using MarkerCollection_t	= egd_markerCollection_t;		//!< This is the marker collection type.
	using MarkerPositions_t 	= MarkerCollection_t::MarkerProperty_t;	//!< Position of the markers.
	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;	//!< Property of the marker.

	using StokesSolver_i		= egd_StokesSolver_i;		       //!< This is the stokes solver interface.
	using StokesSolver_ptr 		= StokesSolver_i::StokesSolver_ptr;    //!< Managed pointer of the stokes solver.
	using TemperatureSolver_i 	= egd_TempSolver_i;		       //!< This is the temperature interface function.
	using TemperatureSolver_ptr 	= TemperatureSolver_i::TempSolver_ptr; //!< This is a managed pointer to the stokes solver.

	using MarkerIntegrator_i 	= egd_MarkerIntegrator_i;		//!< This is the type that is used to move the markers.
	using MarkerIntegrator_prt 	= MarkerIntegrator_i::MarkerIntegrator_ptr;

	using MarkerRehology_i 		= egd_MarkerRehology_i;			//!< This is the marker rehology interface
	using MarkerRehology_ptr 	= MarkerRehology_i::MarkerRehology_ptr;


	/**
	 * \typedef 	CreationResult_t
	 * \brief	Return type of the interface function.
	 *
	 * The job of this interface is to create the grid
	 * and the marker collection and this is the return type
	 * of the function. It is a pair, such that everything can
	 * completly created internaly.
	 */
	using CreationResult_t 		= ::std::pair<GridContainer_t, MarkerCollection_t>;

	using MarkerSetUp_ptr 	= ::std::unique_ptr<egd_MarkerSetUp_i>;	//!< This is the pointer type.

	/*
	 * =========================
	 * Constructors
	 * All constructors, except the destructors are protected.
	 */
public:


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_MarkerSetUp_i();


	/**
	 * \brief	This function is responsible for building *this.
	 *
	 * This function will use the passed configutation object to determine,
	 * which implemmentation should be created, it will then call the
	 * constructor of the implementation and return the result.
	 * The config object is forwarded/passed to the constructor.
	 *
	 * \param  confObj 	The configuration object.
	 */
	static
	MarkerSetUp_ptr
	BUILD(
		const egd_confObj_t& 	confObj);


protected:
	/**
	 * \brief	Default constructor.
	 *
	 */
	egd_MarkerSetUp_i();



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_MarkerSetUp_i(
		const egd_MarkerSetUp_i&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_MarkerSetUp_i&
	operator= (
		const egd_MarkerSetUp_i&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_MarkerSetUp_i(
		egd_MarkerSetUp_i&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_MarkerSetUp_i&
	operator= (
		egd_MarkerSetUp_i&&);


	/*
	 * =========================
	 * Interface Functions
	 */
public:
	/**
	 * \brief	This function creates the marker collection.
	 *
	 * This function is responsible for creating the marker collection,
	 * distribute the marker across the domain, as it sees fit and
	 * to set up initial values for the properties.
	 * It is the responsibility of the subclass to set up everything corectly.
	 *
	 * Note that this task is a highly problem depending implementation.
	 */
	virtual
	CreationResult_t
	creatProblem()
	 = 0;


	/**
	 * \brief	This function outputs information about the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/**
	 * \brief	This function is provided for inspecting the solver.
	 *
	 * It is provided such that concrete classes can addapt their behaviour
	 * on the concrete instance that is used.
	 * This function operates on the mechanical solver, but also a different
	 * one for the temperature solver is provided.
	 *
	 * This function provides a default implemenbtation that loads grid type
	 * of the solbver into *this. Note that this only works once.
	 * And is the only way for setting them.
	 *
	 * \param  solver	The mechanical solver to inspect.
	 */
	virtual
	void
	inspectSolver(
		StokesSolver_i* const 		solver);


	/**
	 * \brief	This function is for convinience.
	 *
	 * It is the same function as the inspect Stoke solver, but operates
	 * on a managed pointer. It will be forwarded to the true interface
	 *
	 * \param  solver	The mechanical solver.
	 */
	virtual
	void
	inspectSolver(
		StokesSolver_ptr& 	solver)
	 final;


	/**
	 * \brief	This function is provided for inspecting the temperature solver.
	 *
	 * See the description for the mechanical solver.
	 *
	 * \param  solver	The temperature solver to inspect.
	 */
	virtual
	void
	inspectSolver(
		TemperatureSolver_i* const 	solver);


	/**
	 * \brief	Convinience fucntion.
	 *
	 * Forwards its argument to the raw pointer version.
	 *
	 * \param  solver	The temperature solver to inspect.
	 */
	virtual
	void
	inspectSolver(
		TemperatureSolver_ptr& 	solver)
	 final;


	/**
	 * \brief	This function is used such that the setup object can inspect the
	 * 		 integrator that is used to move the markers.
	 *
	 * This function is usefull for finetuning the integrator. As an example for this
	 * is the ignore list, that managed which markers are unmovable.
	 *
	 * The default implementation of this function inspects the integrator and
	 * stores if it computes the feel velocities or not.
	 *
	 * \param  integrator		Pointer to the used integrator.
	 */
	virtual
	void
	inspectIntegrator(
		const MarkerIntegrator_i* const 		integrator);


	/**
	 * \brief	This function is an overload of the integrator inspector,
	 * 		 that operates on the managed type.
	 *
	 * Can not be overridden.
	 */
	virtual
	void
	inspectIntegrator(
		const MarkerIntegrator_prt& 		integrator)
	 final;


	/**
	 * \brief	This function allows the setup function to inspect teh dunmper.
	 *
	 * There specific settings can be made, such as ignoring a property or not.
	 * It is recomended taht this si the only action that is done by this function.
	 *
	 * The default of this function does nothing.
	 *
	 * \param  dumper	This si is the dumper.
	 */
	virtual
	void
	inspectDumper(
		egd_dumperFile_t& 	dumper)
	 const;


	/**
	 * \brief	This function is used to inspect the rehology object.
	 *
	 * This function can be used to perform configuration of the initial
	 * setting according to the material model that is being used.
	 *
	 * Thed default of this function does nothing.
	 *
	 * \param  rheology	This is the rehology.
	 */
	virtual
	void
	inspectRehology(
		MarkerRehology_i* const 	rehology);



	/**
	 * \brief	This is a conveninet function that operates directly
	 * 		 on managed pointers.
	 *
	 * \param  rheology
	 */
	virtual
	void
	inspectRehology(
		MarkerRehology_ptr 		rehology)
	 final;





	/*
	 * ========================
	 * Protected Interface functions
	 *
	 * These functions performs some ever returning functions.
	 */
protected:
	/**
	 * \brief	Return the type on which the temperature equation is solved.
	 *
	 * If the grid is invalid an error is generated.
	 */
	eGridType
	getTempGrid()
	 const;


	/**
	 * \brief	Return the type on which the stokes equation is solved.
	 *
	 * If the grid is invalid an error is returned.
	 */
	eGridType
	getStokeGrid()
	 const;


	/**
	 * \brief	Returns true if the temperature equation is solveed on an extended grid.
	 *
	 * If the grid is invalid an error is generated.
	 */
	bool
	isTempOnExtGrid()
	 const;


	/**
	 * \brief	Returns true if the Stoke equation is solved on the extended grid.
	 * If the grid is invalid an error is generated.
	 */
	bool
	isStokeOnExtGrid()
	 const;


	/**
	 * \brief	This function returns a reference to the stored grid property map.
	 *
	 * This is the property map, that was optained during inspection.
	 */
	const PropToGridMap_t&
	getSolverPropMap()
	 const;


	/**
	 * \brief	Returns true if the feel velocity are aviable.
	 *
	 * Note this function will query an internal state. The correct
	 * working of this function depends on the fact that the inspect
	 * function of the integrator that is implemented by the interfcae
	 * itself was called.
	 */
	bool
	isFellVelPresent()
	 const;


	/**
	 * \brief	Returns true if the stokes solver is a interia solver.
	 */
	bool
	isIntertiaSolver()
	 const;



	/**
	 * \brief	This function returns true if the stokes solver was inspected.
	 */
	bool
	wasStokesSolverInspected()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if the temperature solver was inspected.
	 */
	bool
	wasTempSolverInspected()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if the rehology was inspected.
	 */
	bool
	wasRehologyInspected()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if the integrator is inspected.
	 */
	bool
	wasIntegratorInspected()
	 const
	 noexcept;


	/*
	 * ========================
	 * Private Memebers
	 */
private:
	eGridType 	m_tempGrid = eGridType::INVALID;	//!< This is the grid on which we solve the temperature equation.
	eGridType 	m_mechGrid = eGridType::INVALID;	//!< This is the grid on which we solve the temperature equation.

	/*
	 * Grid propertyies
	 * These are the properties that are needed by the grid.
	 * This are optained from the two solver.
	 */
	PropToGridMap_t 	m_solverGridProperties;	//!< This are the properties that are needed by the solver.

	bool 	m_needsFeelVel = false;			//!< Stores the result of the test if the integrator provides the feel velocities.
	bool 	m_wasIntegratorInstepcted = false;	//!< Stores if the integrator was inspected.
	bool  	m_wasRehologyInspected = false;		//!< Stores if the rehology was inspected.
	bool 	m_inertiaSolver = false;		//!< If true the solver needs velocity on the marker properties.
}; //End: class(egd_MarkerSetUp_i)

PGL_NS_END(egd)

