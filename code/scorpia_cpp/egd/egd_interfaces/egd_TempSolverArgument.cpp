/**
 * \brief	This file contains the code of some of the functions of
 * 		 the temperature solver argument implementation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_generalSolverArgument.hpp>
#include <egd_interfaces/egd_generalSolverSourceTerm.hpp>
#include <egd_interfaces/egd_TempSolverSourceTerm.hpp>
#include <egd_interfaces/egd_TempSolverArgument.hpp>

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/*
 * ==================
 * Heating term
 */

bool
egd_tempSolverArg_t::hasHeatingTerm()
 const
  noexcept
{
	return (m_heat != nullptr)
		? true
		: false;
}; //End: has heating term.


void
egd_tempSolverArg_t::resetHeatingTerm()
{
	if(this->hasHeatingTerm() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to reset a heating term that did not exist.");
	};

	m_heat = nullptr;	//Rest

	return;
}; //End: reset heating term


void
egd_tempSolverArg_t::setHeatingTerm(
	const HeatingTerm_t& 		newHeat)
{
	if(this->hasHeatingTerm() == true)
	{
		throw PGL_EXCEPT_LOGIC("Argument has already a heating term.");
	};

	m_heat = &newHeat;	//Store the heating term.

	return;
}; //End: set heating term



const egd_tempSolverArg_t::HeatingTerm_t&
egd_tempSolverArg_t::getHeatingTerm()
 const
{
	if(this->hasHeatingTerm() == false)
	{
		throw PGL_EXCEPT_LOGIC("The argument does not store a heating term.");
	};

	return (*m_heat);
}; //End: get heating


/*
 * =======================
 * Time Step
 */

bool
egd_tempSolverArg_t::hasTimeStep()
 const
{
	if(pgl::isValidFloat(m_dt) == true)
	{
		pgl_assert(m_dt > 0.0);
		return true;
	};

	return false;
};//End: hasTimeStep


egd_tempSolverArg_t::Numeric_t
egd_tempSolverArg_t::getDT()
 const
{
	if(this->hasTimeStep() == true)
	{
		return m_dt;
	};

	throw PGL_EXCEPT_LOGIC("No time step was installed.");
	return NAN;
};


egd_tempSolverArg_t::Numeric_t
egd_tempSolverArg_t::setDT(
	const Numeric_t 	newDT)
{
	if(::pgl::isValidFloat(newDT) && newDT > 0)
	{
		const Numeric_t o = m_dt;	//Store the old one
		m_dt = newDT;			//Set the new one
		return o;			//Return the old one
	};

	throw PGL_EXCEPT_InvArg("The passed timestep was invalid.");
	return NAN;
};


egd_tempSolverArg_t::Numeric_t
egd_tempSolverArg_t::getTimeStep()
 const
{
	return this->getDT();
};


void
egd_tempSolverArg_t::setTimeStep(
	const Numeric_t 	newTimeStep,
	Numeric_t* const 	oldTimeStep)
{
	const Numeric_t oldDT = this->setDT(newTimeStep);

	if(oldTimeStep != nullptr)
	{
		*oldTimeStep = oldDT;
	};

	return;
};



/*
 * =====================
 * Source Tem functions
 */
egd_tempSolverArg_t::TempSourceTerm_t&
egd_tempSolverArg_t::getSourceTerm()
{
	if(this->hasSourceTerm() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to access the sourceterm that does not exist.");
	};

	return *(static_cast<TempSourceTerm_t*>(this->prot_getGenSourceTerm()));
};


const egd_tempSolverArg_t::TempSourceTerm_t&
egd_tempSolverArg_t::getSourceTerm()
 const
{
	if(this->hasSourceTerm() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to access the sourceterm that does not exist.");
	};

	return *(static_cast<const TempSourceTerm_t*>(this->prot_cgetGenSourceTerm()));
};


const egd_tempSolverArg_t::TempSourceTerm_t&
egd_tempSolverArg_t::cgetSourceTerm()
 const
{
	if(this->hasSourceTerm() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to access the sourceterm that does not exist.");
	};

	return *(static_cast<const TempSourceTerm_t*>(this->prot_cgetGenSourceTerm()));
};


const egd_tempSolverArg_t::Source_t&
egd_tempSolverArg_t::cgetSourceFor()
 const
{
	return this->Base_t::cgetSourceFor(PropIdx_t::Temperature());
};


const egd_tempSolverArg_t::Source_t&
egd_tempSolverArg_t::getSourceFor()
 const
{
	return this->Base_t::cgetSourceFor(PropIdx_t::Temperature());
};


egd_tempSolverArg_t::Source_t&
egd_tempSolverArg_t::getSourceFor()
{
	return this->Base_t::getSourceFor(PropIdx_t::Temperature());
};


void
egd_tempSolverArg_t::loadSourceTerm(
	TempSourceTerm_ptr 	sourceTerm)
{
	this->prop_adoptSourceTerm(GenSourceTerm_ptr(std::move(sourceTerm)));
};



/*
 * ======================
 * Interface fucntions
 */
bool
egd_tempSolverArg_t::isFinite()
 const
{
	//Test if the timestep is valid
	if(!(::pgl::isValidFloat(m_dt) && m_dt > 0))
	{
		return false;
	};

	if(this->hasSourceTerm() == true)
	{
		const TempSourceTerm_t& source = this->cgetSourceTerm();
		if(source.isFinite() == false)
		{
			return false;
		};
	};

	if(this->hasHeatingTerm() == true)
	{
		if(this->m_heat->isValid() == false)
		{
			return false;
		};
	};

	return true;
};//End: isFinite


/*
 * ==========================
 * Constructors
 */
egd_tempSolverArg_t::~egd_tempSolverArg_t()
 noexcept
 = default;


egd_tempSolverArg_t::egd_tempSolverArg_t(
	const Numeric_t 	dt)
 :
  egd_tempSolverArg_t()
{
	this->setDT(dt);
};


egd_tempSolverArg_t::egd_tempSolverArg_t()
 noexcept
 = default;


egd_tempSolverArg_t::egd_tempSolverArg_t(
	egd_tempSolverArg_t&&)
 noexcept
 = default;


egd_tempSolverArg_t&
egd_tempSolverArg_t::operator= (
	egd_tempSolverArg_t&&)
 = default;


PGL_NS_END(egd)

