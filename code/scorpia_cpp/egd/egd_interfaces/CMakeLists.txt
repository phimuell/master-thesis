# This CMake file will add all the files in the current folder
# to the EGD library.

target_sources(
	egd
  PRIVATE
  	# General Solver
	"${CMAKE_CURRENT_LIST_DIR}/egd_generalSolverResult.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_generalSolverArgument.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_generalSolverSourceTerm.cpp"

  	# Mech Solver & Co.
  	"${CMAKE_CURRENT_LIST_DIR}/egd_StokesSolver_interface.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MechSolverArgument.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MechSolverResult.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MechSolverSourceTerm.cpp"

	# Temp Solver & Co.
  	"${CMAKE_CURRENT_LIST_DIR}/egd_TemperatureSolver_interface.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_TempSolverResult.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_TempSolverSourceTerm.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_TempSolverArgument.cpp"

	# Other interfaces
  	"${CMAKE_CURRENT_LIST_DIR}/egd_applyBoundary_interface.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpol_interface.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_GridToMarkerInterpol_interface.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_interface.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerRehology_interface.cpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerSetUp_interface.cpp"
  PUBLIC
  	# General Solver
	"${CMAKE_CURRENT_LIST_DIR}/egd_generalSolverResult.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_generalSolverArgument.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_generalSolverSourceTerm.hpp"

  	# Mech Solver & Co.
  	"${CMAKE_CURRENT_LIST_DIR}/egd_StokesSolver_interface.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MechSolverResult.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MechSolverArgument.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MechSolverSourceTerm.hpp"

	# Temp Solver & Co.
  	"${CMAKE_CURRENT_LIST_DIR}/egd_TemperatureSolver_interface.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_TempSolverResult.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_TempSolverArgument.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_TempSolverSourceTerm.hpp"

	# Other interfaces
  	"${CMAKE_CURRENT_LIST_DIR}/egd_applyBoundary_interface.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpol_interface.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_GridToMarkerInterpol_interface.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_interface.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerRehology_interface.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerSetUp_interface.hpp"
)

