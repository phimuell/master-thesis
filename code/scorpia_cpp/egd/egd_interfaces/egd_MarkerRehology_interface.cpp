/**
 * \brief	This file contains the functions that are declared by the rehology interface.
 * 		 This are primarly the constructors.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkerRehology_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)


/*
 * =======================
 * Quering functions
 */
bool
egd_MarkerRehology_i::hasInitialValue(
	const PropIdx_t& 	pIdx,
	const Int_t 		mType)
 const
{
		pgl_assert(pIdx.isValid());
	return false;
	PGL_UNUSED(pIdx, mType);
};


egd_MarkerRehology_i::Numeric_t
egd_MarkerRehology_i::getInitialValue(
	const PropIdx_t& 	pIdx,
	const Int_t 		mType)
 const
{
	throw PGL_EXCEPT_illMethod("This method is not implemented.");
	return NAN;
	PGL_UNUSED(pIdx, mType);
};


/*
 * =======================
 * Inspecting functions
 */
void
egd_MarkerRehology_i::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	/* Do nothing */
	return;
	PGL_UNUSED(dumper);
};


/*
 * =========================
 * Special functions
 */
egd_MarkerRehology_i::~egd_MarkerRehology_i()
 = default;


egd_MarkerRehology_i::egd_MarkerRehology_i()
 noexcept
 = default;


egd_MarkerRehology_i::egd_MarkerRehology_i(
	const egd_MarkerRehology_i&)
 noexcept
 = default;


egd_MarkerRehology_i&
egd_MarkerRehology_i::operator= (
	const egd_MarkerRehology_i&)
 noexcept
 = default;


egd_MarkerRehology_i::egd_MarkerRehology_i(
	egd_MarkerRehology_i&&)
 noexcept
 = default;


egd_MarkerRehology_i&
egd_MarkerRehology_i::operator= (
	egd_MarkerRehology_i&&)
 noexcept
 = default;


PGL_NS_END(egd)

