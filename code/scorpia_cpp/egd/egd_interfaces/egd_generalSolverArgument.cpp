/**
 * \brief	This file contains the code for the functions of the general argument type.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_generalSolverArgument.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/*
 * =========================
 * Time step managing
 */
bool
egd_generalSolverArg_i::hasTimeStep()
 const
{
	return false;
};


egd_generalSolverArg_i::Numeric_t
egd_generalSolverArg_i::getTimeStep()
 const
{
	throw PGL_EXCEPT_illMethod("This function must be implemented by the deriving class.");
	return NAN;
};


void
egd_generalSolverArg_i::setTimeStep(
	const Numeric_t 	newTimeStep,
	Numeric_t* const 	oldTimeStep)
{
	pgl_assert(::pgl::isValidFloat(newTimeStep) && newTimeStep > 0.0);
	throw PGL_EXCEPT_illMethod("This method must be interpreted by the deriving classes.");

	return;
	(void)newTimeStep;
	(void)oldTimeStep;
};


/*
 * =============================
 * Source managing.
 */
bool
egd_generalSolverArg_i::hasSourceTerm()
 const
 noexcept
{
	return (m_source == nullptr)
		? false
		: true;
};


bool
egd_generalSolverArg_i::hasSourceFor(
	const PropIdx_t 	pIdx)
 const
{
	if(this->hasSourceTerm() == false)
	{
		throw PGL_EXCEPT_NULL("*this does not have a source term.");
	};

	return this->m_source->hasSourceFor(pIdx);
}; //End: hasSourceFor


eGridType
egd_generalSolverArg_i::getGridType(
	const PropIdx_t 	pIdx)
 const
{
	if(this->hasSourceTerm() == false)
	{
		throw PGL_EXCEPT_NULL("*this does not have a source term.");
	};

	return this->m_source->getGridType(pIdx);
}; //End: getGridType



egd_generalSolverArg_i::Source_t&
egd_generalSolverArg_i::getSourceFor(
	const PropIdx_t 	pIdx)
{
	return this->prot_getGenSourceTerm()->getSource(pIdx);
};


const egd_generalSolverArg_i::Source_t&
egd_generalSolverArg_i::getSourceFor(
	const PropIdx_t 	pIdx)
 const
{
	return this->prot_getGenSourceTerm()->getSource(pIdx);
};


const egd_generalSolverArg_i::Source_t&
egd_generalSolverArg_i::cgetSourceFor(
	const PropIdx_t 	pIdx)
 const
{
	return this->prot_getGenSourceTerm()->getSource(pIdx);
};



/*
 * =========================
 * Protected Source Term Functions
 */
egd_generalSolverArg_i::GenSourceTerm_t*
egd_generalSolverArg_i::prot_getGenSourceTerm()
{
	if(this->hasSourceTerm() == false)
	{
		throw PGL_EXCEPT_NULL("Tried to access a source that is zero.");
	};

	return m_source.get();
};


const egd_generalSolverArg_i::GenSourceTerm_t*
egd_generalSolverArg_i::prot_getGenSourceTerm()
 const
{
	if(this->hasSourceTerm() == false)
	{
		throw PGL_EXCEPT_NULL("Tried to access a source that is zero.");
	};

	return m_source.get();
};


const egd_generalSolverArg_i::GenSourceTerm_t*
egd_generalSolverArg_i::prot_cgetGenSourceTerm()
 const
{
	if(this->hasSourceTerm() == false)
	{
		throw PGL_EXCEPT_NULL("Tried to access a source that is zero.");
	};

	return m_source.get();
};


void
egd_generalSolverArg_i::prop_adoptSourceTerm(
	GenSourceTerm_ptr 	sourceTerm)
{
	if(this->hasSourceTerm() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to set the source term a second time.");
	};
	if(sourceTerm == nullptr)
	{
		throw PGL_EXCEPT_InvArg("Passed null as source term.");
	};
	if(sourceTerm->nSources() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The new source type has too few sources.");
	};

	//Move the source term into the pointer to
	//take over the (world) resources.
	this->m_source = std::move(sourceTerm);

	return;
};



egd_generalSolverArg_i::Size_t
egd_generalSolverArg_i::getStateIdx()
 const
{
		pgl_assert(this->m_stateIdx != (Size_t(-1)));
	return (this->m_stateIdx);
};


egd_generalSolverArg_i::Size_t
egd_generalSolverArg_i::setStateIdx(
	const Size_t 		stateIdx)
{
	if(stateIdx == ((Size_t)(-1)))
	{
		throw PGL_EXCEPT_InvArg("Passed a negative state index.");
	};
	if((this->m_stateIdx > stateIdx) && (this->m_stateIdx != (Size_t(-1))))
	{
		throw PGL_EXCEPT_InvArg("Tried to lower the state index which is not supported, current index is " + std::to_string(this->m_stateIdx)
				+ ", but argument is " + std::to_string(stateIdx));
	};

	const Size_t t = this->m_stateIdx;		//copy stored value
	this->m_stateIdx = stateIdx;			//update value
	return t;					//return old value
};


egd_generalSolverArg_i::Numeric_t
egd_generalSolverArg_i::getCurrentTime()
 const
{
		pgl_assert(::pgl::isValidFloat(this->m_currTime));
	return (this->m_currTime);
};


egd_generalSolverArg_i::Numeric_t
egd_generalSolverArg_i::setCurrentTime(
	const Numeric_t 	currTime)
{
	if(::pgl::isValidFloat(currTime) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid float.");
	};
	if(this->m_currTime > currTime)
	{
		throw PGL_EXCEPT_InvArg("Tried to lower time, current time is " + std::to_string(this->m_currTime)
				+ ", but passed " + std::to_string(currTime));
	};

	const Numeric_t t = this->m_currTime;		//Copy the current time
	this->m_currTime  = currTime;			//Update the current time
	return t;					//Return the value
};



/*
 * ==============================
 * Constructors
 */
egd_generalSolverArg_i::egd_generalSolverArg_i()
 noexcept
 = default;


egd_generalSolverArg_i::~egd_generalSolverArg_i()
 noexcept
 = default;


egd_generalSolverArg_i::egd_generalSolverArg_i(
	const egd_generalSolverArg_i& o)
 :
  m_source(std::make_unique<GenSourceTerm_t>(*(o.m_source)))
{};


egd_generalSolverArg_i&
egd_generalSolverArg_i::operator= (
	const egd_generalSolverArg_i& o)
{
	this->m_source = std::make_unique<GenSourceTerm_t>(*(o.m_source));
	return *this;
}; //End: assignemnt copy



egd_generalSolverArg_i::egd_generalSolverArg_i(
	egd_generalSolverArg_i&&)
 noexcept
 = default;


egd_generalSolverArg_i&
egd_generalSolverArg_i::operator= (
	egd_generalSolverArg_i&&)
 = default;



PGL_NS_END(egd)


