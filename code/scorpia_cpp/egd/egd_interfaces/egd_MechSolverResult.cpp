/**
 * \brief	This file contains the result of the mechanical solver.
 *
 * This file contains the implementation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD
#include <memory>


PGL_NS_START(egd)

#if defined(EGD_PRINT_NEG_PRESSURE) && (EGD_PRINT_NEG_PRESSURE != 0)
#	pragma message "Negative pressure values will be reproted to stderr."
#endif

#if defined(EGD_IGNORE_NEG_PRESSURE) && (EGD_IGNORE_NEG_PRESSURE != 0)
#	pragma GCC warning "Negative pressure values, will not cause an error."

#	if !(defined(EGD_PRINT_NEG_PRESSURE) && (EGD_PRINT_NEG_PRESSURE != 0))
#		pragma message "No report about negative pressure values will be outputted."
#	endif
#endif


egd_mechSolverResult_t::egd_mechSolverResult_t()
 :
  m_velX(    PropIdx::VelX()    ),
  m_velY(    PropIdx::VelY()    ),
  m_pressure(PropIdx::Pressure())
{
};


egd_mechSolverResult_t::~egd_mechSolverResult_t()
 = default;


egd_mechSolverResult_t::egd_mechSolverResult_t(
	const egd_mechSolverResult_t&)
 = default;


egd_mechSolverResult_t&
egd_mechSolverResult_t::operator= (
	const egd_mechSolverResult_t&)
 = default;


egd_mechSolverResult_t::egd_mechSolverResult_t(
	egd_mechSolverResult_t&&)
 noexcept
 = default;


egd_mechSolverResult_t&
egd_mechSolverResult_t::operator= (
	egd_mechSolverResult_t&&)
 = default;


egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::getVelX()
	noexcept
{
	pgl_assert(m_velX.hasValidGridType());
	return m_velX;
};


const egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::getVelX()
 const
 noexcept
{
	pgl_assert(m_velX.hasValidGridType());
	return m_velX;
};


const egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::cgetVelX()
 const
 noexcept
{
	pgl_assert(m_velX.hasValidGridType());
	return m_velX;
};


Numeric_t
egd_mechSolverResult_t::getMaxAbsVx()
 const
 noexcept
{
	pgl_assert(this->isFinite());
	pgl_assert(m_velX.hasValidGridType());
	return m_velX.getAbsMaxCoeff();
};


egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::getVelY()
 noexcept
{
	pgl_assert(m_velY.hasValidGridType());
	return m_velY;
};


const egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::getVelY()
 const
 noexcept
{
	pgl_assert(m_velY.hasValidGridType());
	return m_velY;
};


const egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::cgetVelY()
 const
 noexcept
{
	pgl_assert(m_velY.hasValidGridType());
	return m_velY;
};


Numeric_t
egd_mechSolverResult_t::getMaxAbsVy()
 const
 noexcept
{
	pgl_assert(this->isFinite());
	pgl_assert(m_velY.hasValidGridType());
	return m_velY.getAbsMaxCoeff();
};


egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::getPressure()
 noexcept
{
	pgl_assert(m_pressure.hasValidGridType());
	return m_pressure;
};


const egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::getPressure()
 const
 noexcept
{
	pgl_assert(m_pressure.hasValidGridType());
	return m_pressure;
};


const egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::cgetPressure()
 const
 noexcept
{
	pgl_assert(m_pressure.hasValidGridType());
	return m_pressure;
};


bool
egd_mechSolverResult_t::isFinite()
 const
{
	pgl_assert(m_velX.hasValidGridType());
	pgl_assert(m_velY.hasValidGridType());
	pgl_assert(m_pressure.hasValidGridType());

	if(m_velX.array().isFinite().all() == false)
	{
		pgl_assert(false && "The x component of the velocity contains non finite values.");
		return false;
	};

	if(m_velY.array().isFinite().all() == false)
	{
		pgl_assert(false && "The y component of the velocity contains non finite values.");
		return false;
	};

	if(m_pressure.array().isFinite().all() == false)
	{
		pgl_assert(false && "The pressure contains non finite values.");
		return false;
	};

	volatile Index_t c = -1, r = -1;
	const volatile Numeric_t minPressValue = m_pressure.array().minCoeff(&r, &c);
	const volatile bool isSmall = ::pgl::isVerySmall(minPressValue);
	/*
	 * An die Nachgeborenen
	 *
	 * It could happens that this test fails in mesterious
	 * ways. The explanation is very simple. This test is
	 * done several times, this should be considered a designe
	 * feature. First this is called in the redistribution
	 * function, and afterwards in the postprocessing function.
	 * Since the post processing function undo the pressure
	 * scalling, the value is multiplied with a very large number.
	 * Thus making even very small numbers large.
	 * And the test fails.
	 */
	if(isSmall == false)
	{
#	if defined(EGD_PRINT_NEG_PRESSURE) && (EGD_PRINT_NEG_PRESSURE != 0)
		const auto nNegPres = (m_pressure.array() < 0.0).count(); //https://eigen.tuxfamily.org/bz/show_bug.cgi?id=426
		const auto nPres    = m_pressure.size();

		std::cerr
			<< " !>>> Found negative pressure in solution."
			<< " The pressure value was " << minPressValue << " (comment about where.)."
			<< " Found " << nNegPres << " (" << ((100.0 * nNegPres) / nPres) << "%), many neg values.";
		if(nNegPres == 1)
		{
			std::cerr
				<< " The index was (" << r << ", " << c << ").";
		};

		std::cerr
			<< std::endl;
#	endif

#	if !(defined(EGD_IGNORE_NEG_PRESSURE) && (EGD_IGNORE_NEG_PRESSURE != 0))
		pgl_assert(false && "Found negative pressure value, see comment above.");
		return false;
#	endif
	};

	return true;
}; //End: isFinite


bool
egd_mechSolverResult_t::allocateSize(
	const yNodeIdx_t	Ny,
	const xNodeIdx_t 	Nx,
	const bool 		isExt)
{
	if(Ny <= Index_t(1))
	{
		throw PGL_EXCEPT_InvArg("The number of y grid points are too small.");
	};
	if(Nx <= Index_t(1))
	{
		throw PGL_EXCEPT_InvArg("The number of x grid points are too small.");
	};

	//Test if the allocation had happened and we have to use different scheme
	if(m_velX.hasValidGridType() == true)
	{
		pgl_assert(m_velY.hasValidGridType(), m_pressure.hasValidGridType());

		//Test if the sizes wgere the same
		if((Ny == m_velX.Ny() && Ny == m_velY.Ny() && Ny == m_pressure.Ny())  &&
		   (Nx == m_velX.Nx() && Nx == m_velY.Nx() && Nx == m_pressure.Nx())  &&
		   (m_velX.isExtendedGrid() == isExt)                                   )
		{
				pgl_assert(m_velY.isExtendedGrid() == isExt, m_pressure.isExtendedGrid() == isExt);
			/*
			 * The sizes where the same, so we set *this to zero.
			 * And return true, such that the reallocation happened.
			 */
			this->setZero();
			return true;
		}; //End if: fake reallocation

		/*
		 * The sizes are not the same, so we generate an error
		 */
		throw PGL_EXCEPT_InvArg("Rreallocation failed, because the sizes are not correct.");
		return false;
	}; //End: possibliy allocated

	/*
	 * Perform the real allocation, in the case tehy are not done yet.
	 */

#define mkGrid(g) ((isExt == true) ? (mkExt(g)) : (mkReg(g)))
	m_velX.allocateGrid(Ny, Nx, mkGrid(GridType_e::StVx), PropIdx::VelX() );
	m_velY.allocateGrid(Ny, Nx, mkGrid(GridType_e::StVy), PropIdx::VelY() );
	m_pressure.allocateGrid(Ny, Nx, mkGrid(GridType_e::StPressure), PropIdx::Pressure() );
#undef mkGrid

	return true;
}; //End: allocate Size


egd_mechSolverResult_t::GridType_e
egd_mechSolverResult_t::getGridType()
 const
{
	pgl_assert(m_velX.hasValidGridType());
	pgl_assert(m_velY.hasValidGridType());
	pgl_assert(m_pressure.hasValidGridType());

	GridType_e gType = m_velX.getType() | m_velY.getType() | m_pressure.getType();
		pgl_assert(::egd::isValidGridType(gType));

	return gType;
}; //End: getGridType


bool
egd_mechSolverResult_t::isExtendedGrid()
 const
{
	return this->onExtendedGrid();
}; //End: isExtendedGrid


bool
egd_mechSolverResult_t::isRegularSize()
 const
{
	return this->onRegularGrid();
}; //End: isExtendedGrid



bool
egd_mechSolverResult_t::isInertiaSolution()
 const
{
	//Currently only an alias
	return this->hasTimeStep();
};



/*
 * ==========================
 * Interface functions to the general result
 */
egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::getSolField(
	const Index_t 	i)
{
	switch(i)
	{
	  case 0:
		return this->getVelX();
	  break;

	  case 1:
		return this->getVelY();
	  break;

	  case 2:
		return this->getPressure();
	  break;

	  default:
	  break;
	}; //End switch

	throw PGL_EXCEPT_OutOfBound("Tried to access the non existing member " + std::to_string(i));
}; //End: getSolution field


const egd_mechSolverResult_t::SolField_t&
egd_mechSolverResult_t::getSolField(
	const Index_t 	i)
 const
{
	switch(i)
	{
	  case 0:
		return this->getVelX();
	  break;

	  case 1:
		return this->getVelY();
	  break;

	  case 2:
		return this->getPressure();
	  break;

	  default:
	  break;
	}; //End switch

	throw PGL_EXCEPT_OutOfBound("Tried to access the non existing member " + std::to_string(i));
}; //End: getSolution field


Index_t
egd_mechSolverResult_t::nSolFields()
 const
 noexcept
{
	return 3;
};


void
egd_mechSolverResult_t::hook_setZero()
{
	//We run before anything is done, so we do some checks
	pgl_assert(m_velX.hasValidGridType());
	pgl_assert(m_velY.hasValidGridType());
	pgl_assert(m_pressure.hasValidGridType());

	return;
}; //End: hook_setZero



PGL_NS_END(egd)


