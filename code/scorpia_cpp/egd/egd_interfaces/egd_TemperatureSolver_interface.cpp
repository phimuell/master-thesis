/**
 * \brief	This file implements the default operations of the temperature solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_TempSolver_i::~egd_TempSolver_i()
 noexcept
 = default;

egd_TempSolver_i::egd_TempSolver_i()
 noexcept
 = default;


egd_TempSolver_i::egd_TempSolver_i(
	const egd_TempSolver_i&)
 noexcept
 = default;


egd_TempSolver_i&
egd_TempSolver_i::operator= (
	const egd_TempSolver_i&)
 noexcept
 = default;


egd_TempSolver_i::egd_TempSolver_i(
	egd_TempSolver_i&&)
 noexcept
 = default;


egd_TempSolver_i&
egd_TempSolver_i::operator= (
	egd_TempSolver_i&&)
 noexcept
 = default;


egd_TempSolver_i::SolverResult_ptr
egd_TempSolver_i::creatResultContainer()
 const
{
	return std::make_unique<SolverResult_t>();
};


void
egd_TempSolver_i::solve(
	const GridContainer_t& 		grid,
	const HeatingTerm_t& 		heatingTerm,
	SolverArg_ptr& 			solArg,
	SolverResult_ptr& 		solResult)
{
	this->solve(grid, heatingTerm, solArg.get(), solResult.get());	//Forward request to the true implementing function.

	return;
};

egd_TempSolver_i::SolverArg_ptr
egd_TempSolver_i::createArgument()
 const
{
	return ::std::make_unique<SolverArg_t>();
};


egd_TempSolver_i::TempSourceTerm_ptr
egd_TempSolver_i::creatSourceTerm()
 const
{
	return ::std::make_unique<TempSourceTerm_t>();
};


bool
egd_TempSolver_i::onExtendedGrid()
 const
{
	const eGridType gType = this->getGridType();
	if(egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_RUNTIME("The returned gridtype is wrong.");
	};

	return egd::isExtendedGrid(gType);
}; //End: onExtendedGrid


bool
egd_TempSolver_i::onRegularGrid()
 const
{
	const eGridType gType = this->getGridType();
	if(egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_RUNTIME("The returned gridtype is wrong.");
	};

	return egd::isRegularGrid(gType);
}; //End: onRegularGrid


PGL_NS_END(egd)


