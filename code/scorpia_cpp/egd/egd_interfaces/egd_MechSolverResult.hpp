#pragma once
/**
 * \brief	This file contains the result of the mechanical solver.
 *
 * This type is used by the mechanical solvers, meaning they contain the
 * solution to the Navier-Stokes equation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_generalSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_mechSolverResult_t
 * \brief	This class is the result value of the Stoke solver.
 *
 * This type is used to store the result of the stoke solver.
 * Meaning the mechanical component of problem. It allows an
 * extensions, by providing a virtual destructor, but its
 * base implements the mechanical solution for the original
 * problem.
 *
 * The class was designed to allow an extension of the class
 * without changing much. It assumes that no more values than
 * the velocities and the pressure are needed.
 * But the functions are designed, such that this could be changed.
 *
 * The timestep of *this is only used if the problem involved
 * inertia, otherwhise the time step is deactivated. If the time
 * step is set, it denotes the last time step, that was used to
 * evolve the system into its current state.
 */
class egd_mechSolverResult_t : public egd_generalSolverResult_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Size_t 	 = ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
							//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	 = ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	 = ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t  = ::pgl::Numeric_t;		//!< This is the float type that is used
	using GridType_e = eGridType;			//!< This is the type of the grid where the result is interpreted.
	using SolField_t = egd_gridProperty_t;		//!< This is the type of the result.
	using Matrix_t 	 = egd_Matrix_t<Numeric_t>;	//!< A matrix class.
	using Base_t 	 = egd_generalSolverResult_i; 	//!< This is the base type.

	using MechSolverResult_ptr 	= ::std::unique_ptr<egd_mechSolverResult_t>;	//!< This is the pointer type.

	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is the building constructor, at least one part
	 * of it. Due to some C++ speciality, it is not possible
	 * to perform a virtual call inside a constructor, that
	 * will be redirected to the subclass. So this virtual
	 * call has to be performed outside by calling the
	 * allocateSize() function.
	 */
	egd_mechSolverResult_t();


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_mechSolverResult_t();


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_mechSolverResult_t(
		const egd_mechSolverResult_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_mechSolverResult_t&
	operator= (
		const egd_mechSolverResult_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_mechSolverResult_t(
		egd_mechSolverResult_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_mechSolverResult_t&
	operator= (
		egd_mechSolverResult_t&&);


	/*
	 * ======================
	 * General interface functions
	 */
public:
	/**
	 * \brief	Returns the number of different solution fields.
	 *
	 * In this case this are 3.
	 */
	virtual
	Index_t
	nSolFields()
	 const
	 noexcept
	 override;


	/**
	 * \brief	This function returns a mutable reference
	 * 		 to the ith solution field.
	 *
	 * Counting starts with 0 and goes up to nSolFields() - 1.
	 * The ordering must be the same as the ordering is in the solver.
	 *
	 * \param  i 	The index of the solutuion field.
	 *
	 * 0 :-> 	VX
	 * 1 :->	VY
	 * 2 :-> 	P
	 */
	virtual
	SolField_t&
	getSolField(
		const Index_t 	i)
	 override;


	/**
	 * \brief	This function returns a constant reference
	 * 		 to the ith solution field.
	 */
	virtual
	const SolField_t&
	getSolField(
		const Index_t 	i)
	 const
	 override;

	//Will load the versions that operates on pIdx
	using Base_t::getSolField;


	/*
	 * The timestep has the meaning of how far we ebvolved
	 * the system in the previous time step. It is also
	 * only present if *this was the solution of an inertia
	 * problem.
	 */
	using Base_t::hasTimeStep;
	using Base_t::getTimeStep;
	using Base_t::setTimeStep;
	using Base_t::isTimeDependendSol;


	/**
	 * \brief	This function returns true if *this is an
	 * 		 inertia solution.
	 *
	 * This function is currently an alias of hasTimeStep().
	 * However this could change.
	 */
	virtual
	bool
	isInertiaSolution()
	 const;




	/*
	 * ===================
	 * Member access functions
	 */
public:
	/**
	 * \brief	Return a reference to the x vecocity solution.
	 */
	SolField_t&
	getVelX()
	 noexcept;


	/**
	 * \brief	Return a constant reference to the x velocity solution.
	 */
	const SolField_t&
	getVelX()
	 const
	 noexcept;


	/**
	 * \brief	Return a constant reference to the x veclocity solution.
	 * 		 Explicit version.
	 */
	const SolField_t&
	cgetVelX()
	 const
	 noexcept;


	/**
	 * \brief	Returns the maximum value of the absolute velocity in x direction.
	 *
	 * This function returns max(abs(v^{x}_{i,j})) for all location.
	 */
	Numeric_t
	getMaxAbsVx()
	 const
	 noexcept;


	/**
	 * \brief	Return a reference to the y vecocity solution.
	 */
	SolField_t&
	getVelY()
	 noexcept;


	/**
	 * \brief	Return a constant reference to the y velocity solution.
	 */
	const SolField_t&
	getVelY()
	 const
	 noexcept;


	/**
	 * \brief	Return a constant reference to the y veclocity solution.
	 * 		 Explicit version.
	 */
	const SolField_t&
	cgetVelY()
	 const
	 noexcept;


	/**
	 * \brief	Returns the maximum value of the absolute velocity in y direction.
	 *
	 * This function returns max(abs(v^{y}_{i,j})) for all location.
	 */
	Numeric_t
	getMaxAbsVy()
	 const
	 noexcept;


	/**
	 * \brief	Return a reference to the computed pressure.
	 */
	SolField_t&
	getPressure()
	 noexcept;


	/**
	 * \brief	Return a constant reference to the computed pressure.
	 */
	const SolField_t&
	getPressure()
	 const
	 noexcept;


	/**
	 * \brief	Return a constant reference to the computed pressure.
	 * 		 Explicit version.
	 */
	const SolField_t&
	cgetPressure()
	 const
	 noexcept;


	/*
	 * ==================
	 * Managing.
	 *
	 * Here are some easy to use managing functions.
	 */
public:
	/**
	 * \brief	Resize all matrices to the given size.
	 *
	 * This function will call resize on the matrices.
	 * After the resizing happened the matrices will be set
	 * to zero. Note that this function is not guaranteed to
	 * succeed, if the matrix was already allocated. True is
	 * returned if the action was performed, false of an
	 * error if not.
	 *
	 * This function is virtuel.
	 * The default implementation allocates the grid on the rgular
	 * staggered grids. Note that if *this was already set up, and
	 * the passed sizes are the same as the original ones, then this
	 * function calls the setZero() function and returns treu.
	 * Otherwhise an error is generated.
	 *
	 *
	 * \note	This function must be called after the building
	 * 		 consttructor was called.
	 *
	 * \param  Ny		The number of rows that should be allocated.
	 * \param  Nx		The number of columns that should be used.
	 * \param  isExt	Bool to indicate if the grids should be extended or not.
	 */
	virtual
	bool
	allocateSize(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const bool 		isExt);


	/**
	 * \brief	This function tests if all enteries of *this are finite.
	 *
	 * In debug mode an assert will be triggered if a test fails.
	 * If asserts are disabled, true will be returned if no error
	 * was detected and false otherwise.
	 */
	virtual
	bool
	isFinite()
	 const
	 override;


	/**
	 * \brief	Return the type of the solution grid.
	 *
	 * This function returns the grid type.
	 * This is done by oring the grid types of the properyt
	 * together.
	 */
	virtual
	GridType_e
	getGridType()
	 const;


	using Base_t::onRegularGrid;
	using Base_t::onExtendedGrid;


	/**
	 * \brief	This function returns true if *this is on extended grids.
	 *
	 * It is an alias of the general result function onExtendedGrid().
	 * Note that this function is depricated.
	 */
	[[deprecated]]
	virtual
	bool
	isExtendedGrid()
	 const
	 final;


	/**
	 * \brief	This function returns true if *this is on regular grid sizes.
	 *
	 * It is an alias of the general result function onRegularGrid().
	 * Note that this function is depricated.
	 */
	[[deprecated]]
	virtual
	bool
	isRegularSize()
	 const;


	/*
	 * =============================
	 * Hooks
	 */
protected:
	/**
	 * \brief	This function is the cleaning hook of the result.
	 *
	 * Since *this does not have any other member beside the fields,
	 * and they are cleaned by the base, the function does nothing.
	 */
	virtual
	void
	hook_setZero()
	 override;


	/*
	 * ================
	 * Private Members
	 */
private:
	SolField_t 	m_velX;		//!< Velocity in the x direction.
	SolField_t 	m_velY;		//!< Velocity in the y direction.
	SolField_t 	m_pressure;	//!< The pressure.
}; //End: class(egd_mechSolverResult_t)



PGL_NS_END(egd)











