/**
 * \brief	This file contains the implementation of the interface function for the set up process.
 *
 * This are primary the constructors.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD
#include <memory>


PGL_NS_START(egd)


egd_MarkerSetUp_i::egd_MarkerSetUp_i()
 = default;


egd_MarkerSetUp_i::~egd_MarkerSetUp_i()
 = default;


egd_MarkerSetUp_i::egd_MarkerSetUp_i(
	const egd_MarkerSetUp_i&)
 = default;


egd_MarkerSetUp_i::egd_MarkerSetUp_i(
	egd_MarkerSetUp_i&&)
 noexcept
 = default;


egd_MarkerSetUp_i&
egd_MarkerSetUp_i::operator= (
	const egd_MarkerSetUp_i&)
 = default;


 egd_MarkerSetUp_i&
 egd_MarkerSetUp_i::operator= (
 	egd_MarkerSetUp_i&&)
 = default;


/*
 * Convinience functions for inspecting
 */

void
egd_MarkerSetUp_i::inspectSolver(
	StokesSolver_ptr& 		solver)
{
	pgl_assert(solver != nullptr);
	this->inspectSolver(solver.get());
	return;
}; //End: inspect


void
egd_MarkerSetUp_i::inspectSolver(
	TemperatureSolver_ptr& 		solver)
{
	pgl_assert(solver != nullptr);
	this->inspectSolver(solver.get());
	return;
}; //End: inspect


void
egd_MarkerSetUp_i::inspectIntegrator(
	const MarkerIntegrator_prt& 		integrator)
{
	pgl_assert(integrator != nullptr);
	this->inspectIntegrator(integrator.get());
	return;
}; //ENd: inspect integrator on managed type


void
egd_MarkerSetUp_i::inspectRehology(
		MarkerRehology_ptr 		rehology)
{
		pgl_assert(rehology != nullptr);
	this->inspectRehology(rehology.get());
	return;
}; //End: inspect managed rehology


/*
 * ===========================
 * Inspecting functions
 */

void
egd_MarkerSetUp_i::inspectSolver(
	StokesSolver_i* const 		solver)
{
	pgl_assert(solver != nullptr);

	if(this->m_mechGrid != eGridType::INVALID)
	{
		throw PGL_EXCEPT_LOGIC("Tried to set the grid type of the Stokes solver twice.");
	};

	//Load the grid type
	this->m_mechGrid = solver->getGridType();

	if(this->wasStokesSolverInspected() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The Stokes solver had an invalid grid type.");
	};

	//Update the property maps
	m_solverGridProperties.saveAdding(solver->mkNeededProperty() );
		pgl_assert(m_solverGridProperties.isValid());

	//store if inertia solver
	m_inertiaSolver = solver->isInertiaSolver();

	return;
}; //End: inspect mech solver default


void
egd_MarkerSetUp_i::inspectSolver(
	TemperatureSolver_i* const 	solver)
{
	pgl_assert(solver != nullptr);

	if(this->m_tempGrid != eGridType::INVALID)
	{
		throw PGL_EXCEPT_LOGIC("Tried to set the grid type of the temperature solver twice.");
	};

	//Load the grid type
	this->m_tempGrid = solver->getGridType();

	if(this->wasTempSolverInspected() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The temperature solver had an invalid grid type.");
	};


	//Update the property maps
	m_solverGridProperties.saveAdding(solver->mkNeededProperty() );
		pgl_assert(m_solverGridProperties.isValid());

	return;
};//End: inspect temp solver default


void
egd_MarkerSetUp_i::inspectIntegrator(
	const MarkerIntegrator_i* const 	integrator)
{
	pgl_assert(integrator != nullptr);

	//store if the feel velocity is used
	this->m_needsFeelVel = integrator->computeFeelVel();

	//Store if the integrator was inspected
	this->m_wasIntegratorInstepcted = true;

	return;
};//End: inspect the integrator


void
egd_MarkerSetUp_i::inspectRehology(
		MarkerRehology_i* const 		rehology)
{
		pgl_assert(rehology != nullptr);

	this->m_wasRehologyInspected = true;	//store that the rehology was inspected.

	return;
	PGL_UNUSED(rehology);
}; //End: inspect rehology


void
egd_MarkerSetUp_i::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	return;
	(void)dumper;
};



/*
 * =====================
 */


eGridType
egd_MarkerSetUp_i::getTempGrid()
 const
{
	if(this->wasTempSolverInspected() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to query the temperature solver grid type, but it is invalid.");
	};

	return m_tempGrid;
}; //End: get temperature solver grid


eGridType
egd_MarkerSetUp_i::getStokeGrid()
 const
{
	if(this->wasStokesSolverInspected() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to query the Stokes solver grid type, but it is invalid.");
	};

	return m_mechGrid;
}; //End: get mechanical solver grid


bool
egd_MarkerSetUp_i::isStokeOnExtGrid()
 const
{
	const eGridType gType = this->getStokeGrid();
		pgl_assert(::egd::isValidGridType(gType));

	return ::egd::isExtendedGrid(gType);
}; //End: test if the Stokes solver grid is extended


bool
egd_MarkerSetUp_i::isTempOnExtGrid()
 const
{
	const eGridType gType = this->getTempGrid();
		pgl_assert(::egd::isValidGridType(gType));

	return ::egd::isExtendedGrid(gType);
}; //End: test if temperature solver is extended


const egd_MarkerSetUp_i::PropToGridMap_t&
egd_MarkerSetUp_i::getSolverPropMap()
 const
{
	pgl_assert(this->wasTempSolverInspected()   ||	 //At least one was inspected
		   this->wasStokesSolverInspected()   );
	return m_solverGridProperties;
};




bool
egd_MarkerSetUp_i::isFellVelPresent()
 const
{
	if(this->wasIntegratorInspected() == false)
	{
		throw PGL_EXCEPT_LOGIC("The integrator was not ionspected.");
	};
	return this->m_needsFeelVel;
};


bool
egd_MarkerSetUp_i::isIntertiaSolver()
 const
{
	if(this->wasStokesSolverInspected() == false)
	{
		throw PGL_EXCEPT_LOGIC("The stokes solver was not inspected.");
	};
	return this->m_inertiaSolver;
};


/*
 * ========================
 * Was inspected
 */
bool
egd_MarkerSetUp_i::wasStokesSolverInspected()
 const
 noexcept
{
	return ::egd::isValidGridType(m_mechGrid)
	       ? true
	       : false;
};


bool
egd_MarkerSetUp_i::wasTempSolverInspected()
 const
 noexcept
{
	return ::egd::isValidGridType(m_tempGrid)
	       ? true
	       : false;
};


bool
egd_MarkerSetUp_i::wasRehologyInspected()
 const
 noexcept
{
	return this->m_wasRehologyInspected;
};


bool
egd_MarkerSetUp_i::wasIntegratorInspected()
 const
 noexcept
{
	return this->m_wasIntegratorInstepcted;
};



PGL_NS_END(egd)


