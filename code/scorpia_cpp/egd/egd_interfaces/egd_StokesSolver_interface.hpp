#pragma once
/**
 * \brief	This file contains the interface for the mechanical solver.
 *
 * The mechanical solver will compute the solution of the Stokes equation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_util/egd_propertyToGridMap.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverArgument.hpp>
#include <egd_interfaces/egd_MechSolverSourceTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_StokesSolver_i
 * \brief	Interface for solving the Stokes equation.
 *
 * This class will compute a solution of the stokes equation.
 * As input it will operate on the grid container directly.
 * It will then construct the equations out of it and will then
 * compute a solution, which is returned as a result object.
 *
 */
class egd_StokesSolver_i
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using Matrix_t 	= egd_Matrix_t<Numeric_t>;	//!< This is the type of the reuslts.

	using SolverResult_t 	= egd_mechSolverResult_t;		 //!< This is the result type of the Stokes solver.
	using SolverResult_ptr 	= SolverResult_t::MechSolverResult_ptr;	 //!< Pointer to the result type.

	using SolverArg_t 	= egd_mechSolverArg_t;			 //!< This is the mechanical argument type.
	using SolverArg_ptr 	= SolverArg_t::MechSolverArg_ptr;	 //!< This is a managed pointer type.

	using StokesSolver_ptr 	= ::std::unique_ptr<egd_StokesSolver_i>; //!< This is the pointer for storing the Stokes solver

	using GridContainer_t 	= egd_gridContainer_t;			 //!< This is the grid container

	using SourceTerm_t 	= egd_mechSolverSourceTerm_t;		 //!< This is the source term.
	using SourceTerm_ptr 	= SourceTerm_t::MechSolverSourceTerm_ptr;//!< Managed source term.

	using PropToGridMap_t   = GridContainer_t::PropToGridMap_t;	 //!< This is the map that deperimes which times are pressent.



	/*
	 * ===========================
	 * Constructors
	 *
	 * The destructor is public and virtual.
	 * All other constructors are defaulted
	 * and protected.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is virtual and defaulted.
	 */
	virtual
	~egd_StokesSolver_i()
	 noexcept;

protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_StokesSolver_i()
	 noexcept;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_StokesSolver_i(
		const egd_StokesSolver_i&)
	 noexcept;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_StokesSolver_i&
	operator= (
		const egd_StokesSolver_i&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_StokesSolver_i(
		egd_StokesSolver_i&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_StokesSolver_i&
	operator= (
		egd_StokesSolver_i&&)
	 noexcept;


	/*
	 * ========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function compoutes the solution.
	 *
	 * It uses the data in the grid in order to compute
	 * it. The result is stored inside the result object.
	 * The result object can be reused.
	 * Note that the graviation is positive, because the y
	 * axsis is pointing downwards!
	 *
	 * This function is virtual and abstarct.
	 *
	 * \param  grid		The grid container that should be used.
	 * \param  solArg 	This is the argument that is apssed to the implementation.
	 * \param  solResult	This is the colver result.
	 *
	 * \throw	An error is raised if something happened.
	 *
	 * \note	This is the real interface function. There is a second one,
	 * 		 that operates on the managed pointers for convenience.
	 */
	virtual
	void
	solve(
		const GridContainer_t& 		grid,
		SolverArg_t* const 		solArg,
		SolverResult_t* const 		solResult)
	 = 0;


	/**
	 * \brief	This is also the solver interface. Howvever it operates
	 * 		 on the managed pointertypes.
	 *
	 * There is a default oimplementation that forwards the request.
	 * This fucntion can not be overwritten.
	 *
	 * \param  grid		The grid container that should be used.
	 * \param  solArg 	This is the argument that is apssed to the implementation.
	 * \param  solResult	This is the colver result.
	 *
	 * \throw	An error is raised if something happened.
	 */
	virtual
	void
	solve(
		const GridContainer_t& 		grid,
		SolverArg_ptr& 			solArg,
		SolverResult_ptr& 		solResult)
	 final;



	/**
	 * \brief	This fucntion returns an instance of the result object.
	 *
	 * This function is something like the builder function of the
	 * result class.
	 *
	 * This function has a default implementation and generates the
	 * standard result class.
	 */
	virtual
	SolverResult_ptr
	creatResultContainer()
	 const;


	/**
	 * \brief	This function returns an instance of the solver argument.
	 *
	 * Depending on the concrete implementation, this instance is already
	 * initalized partially. Note that a managed pointer is returned.
	 */
	virtual
	SolverArg_ptr
	createArgument()
	 const;


	/**
	 * \brief	This function returns a source term for the
	 * 		 mechanical solver.
	 *
	 * This function returns a managed pointer object. This
	 * function has a default implementation who will return an
	 * empty source term.
	 */
	virtual
	SourceTerm_ptr
	creatSourceTerm()
	 const;


	/**
	 * \brief	This function returns a property map that is needed by the solver.
	 *
	 * This function allows to solver independend create the needed property.
	 * The returned property map must then be incooperated by the setup object.
	 *
	 * There is not default implementation.
	 */
	virtual
	PropToGridMap_t
	mkNeededProperty()
	 const
	 = 0;


	/**
	 * \brief	This function returns true if *this is an inertial solver.
	 *
	 * This means it needs a past time step that is supplied by an argument.
	 */
	virtual
	bool
	isInertiaSolver()
	 const
	 = 0;


	/**
	 * \brief	This function is for configuring the boundary condition.
	 *
	 * This function is used by several inspecting function to set properties,
	 * that are not addressed by the boundary condition object.
	 *
	 * key is used to specify which paramter is passed, as value only numerics
	 * can be passed.
	 * True is returned if the key was recognized.
	 *
	 * \param  key		The key of the setting.
	 * \param  value	The option value.
	 */
	virtual
	bool
	setBoundaryOption(
		const std::string& 		key,
		const Numeric_t 		value)
	 = 0;


	/*
	 * ================================
	 * Status functions.
	 *
	 * These functions allows to quiery the status of *this.
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 *
	 * This is mostly used for debugging and output.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/**
	 * \brief	This function returns the grid type
	 * 		 on which the solver operates.
	 *
	 * This function allows to query on which grid the
	 * solver is operating.
	 */
	virtual
	eGridType
	getGridType()
	 const
	 = 0;


	/**
	 * \brief	This function return true if *this operates
	 * 		 on an extended grid.
	 *
	 * The default of this function is implemented by calling
	 * the getGridType() function.
	 */
	virtual
	bool
	onExtendedGrid()
	 const;


	/**
	 * \brief	This function returns true if *this operates
	 * 		 on a regular grid.
	 *
	 * The default of this function is reduced to getGridType().
	 */
	virtual
	bool
	onRegularGrid()
	 const;


	/*
	 * ==========================
	 * F A C T O R Y
	 */
public:
	/**
	 * \brief	This is the builder function.
	 *
	 * This function examines the configuration object, to determine
	 * which implementation of the solver sould be created.
	 * The configuration object is then passed to the constructor
	 * of the selected implementation.
	 *
	 * \param  confObj	Configuration object.
	 */
	static
	StokesSolver_ptr
	BUILD(
		const egd_confObj_t& 		confObj);

}; //End: class(egd_StokesSolver_i)



PGL_NS_END(egd)

