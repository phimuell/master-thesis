/**
 * \brief	This file implements functions of the interface for the boundary allplier.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


egd_applyBoundary_i::egd_applyBoundary_i()
 noexcept
 = default;


egd_applyBoundary_i::~egd_applyBoundary_i()
 noexcept
 = default;


egd_applyBoundary_i::egd_applyBoundary_i(
	const egd_applyBoundary_i&)
 noexcept
 = default;


egd_applyBoundary_i&
egd_applyBoundary_i::operator= (
	const egd_applyBoundary_i&)
 noexcept
 = default;


egd_applyBoundary_i::egd_applyBoundary_i(
	egd_applyBoundary_i&&)
 noexcept
 = default;


egd_applyBoundary_i&
egd_applyBoundary_i::operator= (
	egd_applyBoundary_i&&)
 noexcept
 = default;

PGL_NS_END(egd)

