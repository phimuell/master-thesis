# Info
This folder contains all the interfaces that we will need in this project.
An interface is an abstract class that that defines a set of functions that
performs a specific task, such as interpolation.
But the task is not implemented in the interface itself.

## Note
However some interfaces were carelessly extended during the project.
However they still work.
Also the interfaces connected to the solver would need an overhaul.




