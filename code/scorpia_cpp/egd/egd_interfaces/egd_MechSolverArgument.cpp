/**
 * \brief	This file contains teh functions for the mechanical solver argument.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_MechSolverArgument.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/*
 * ===========================
 * Source Term functions
 */
egd_mechSolverArg_t::MechSourceTerm_t&
egd_mechSolverArg_t::getSourceTerm()
{
	return *(static_cast<MechSourceTerm_t*>(this->prot_getGenSourceTerm()));
};


const egd_mechSolverArg_t::MechSourceTerm_t&
egd_mechSolverArg_t::getSourceTerm()
 const
{
	return *(static_cast<const MechSourceTerm_t*>(this->prot_getGenSourceTerm()));
};


const egd_mechSolverArg_t::MechSourceTerm_t&
egd_mechSolverArg_t::cgetSourceTerm()
 const
{
	return *(static_cast<const MechSourceTerm_t*>(this->prot_getGenSourceTerm()));
};


void
egd_mechSolverArg_t::loadSourceTerm(
	MechSourceTerm_ptr 	sourceTerm)
{
	this->prop_adoptSourceTerm(GenSourceTerm_ptr(std::move(sourceTerm)));
};


/*
 * ====================
 * Interface functions
 */

bool
egd_mechSolverArg_t::isFinite()
 const
{
	if(this->isDensityStabOn() == true)
	{
		if(::pgl::isValidFloat(m_rhoStabTime) == false || m_rhoStabTime < 0.0)
		{
			pgl_assert(false && "Time is invalid.");
			return false;
		};
	}
	else
	{
		if(::pgl::isValidFloat(m_rhoStabTime) == true)
		{
			pgl_assert(false && "Density stab is disabled but a valid value is there.");
			return false;
		};
	};
	if(m_grav.isFinite() == false)
	{
		pgl_assert(false && "Gravity is invalid.");
		return false;
	};

	if(this->hasSourceTerm() == true)
	{
		if(this->Base_t::prot_cgetGenSourceTerm()->isFinite() == false)
		{
			pgl_assert(false && "Source term is invalid.");
			return false;
		};
	};//End if: source term

	return true;
}; //End: isFinite


/*
 * =====================
 * Constructors
 */
egd_mechSolverArg_t::~egd_mechSolverArg_t()
 noexcept
 = default;


egd_mechSolverArg_t::egd_mechSolverArg_t()
= default;


egd_mechSolverArg_t::egd_mechSolverArg_t(
	const egd_mechSolverArg_t&)
 = default;


egd_mechSolverArg_t&
egd_mechSolverArg_t::operator= (
	const egd_mechSolverArg_t&)
 = default;


egd_mechSolverArg_t::egd_mechSolverArg_t(
	egd_mechSolverArg_t&&)
 noexcept
 = default;


egd_mechSolverArg_t&
egd_mechSolverArg_t::operator= (
	egd_mechSolverArg_t&&)
 = default;


/*
 * ========================
 * Gravity
 */
egd_mechSolverArg_t::Gravitation_t
egd_mechSolverArg_t::setGrav(
	const Gravitation_t& 		newGrav)
{
	if(newGrav.isFinite() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid gravity value");
	};
	const Gravitation_t o = m_grav;
	m_grav = newGrav;

	return o;			//return old value
}; //End: exchange gravity


egd_mechSolverArg_t::Numeric_t
egd_mechSolverArg_t::g_x()
 const
{
	const Numeric_t v = m_grav[0];
	if(::pgl::isValidFloat(v) == false)
	{
		throw PGL_EXCEPT_LOGIC("The x value of gravity was not set.");
	};

	return v;
};


egd_mechSolverArg_t::Numeric_t
egd_mechSolverArg_t::g_x(
	const Numeric_t newGx)
{
	if(::pgl::isValidFloat(newGx) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid value to the gravity x component.");
	};
	const Numeric_t o = m_grav[0];
	m_grav[0] = newGx;

	return o;
};


egd_mechSolverArg_t::Numeric_t
egd_mechSolverArg_t::g_y()
 const
{
	const Numeric_t v = m_grav[1];
	if(::pgl::isValidFloat(v) == false)
	{
		throw PGL_EXCEPT_LOGIC("The y value of gravity was not set.");
	};

	return v;
};


egd_mechSolverArg_t::Numeric_t
egd_mechSolverArg_t::g_y(
	const Numeric_t newGy)
{
	if(::pgl::isValidFloat(newGy) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid value to the gravity y component.");
	};
	const Numeric_t o = m_grav[1];
	m_grav[1] = newGy;

	return o;
};


/*
 * =====================
 * Density stabilization
 */
egd_mechSolverArg_t&
egd_mechSolverArg_t::enableDensityStab()
{
	if(this->isDensityStabOn() == true)
	{
		throw PGL_EXCEPT_LOGIC("The density stabilization is already active.");
	};
	if(::pgl::isValidFloat(m_rhoStabTime) == true)
	{
		throw PGL_EXCEPT_LOGIC("A valid time was given.");
	};

	//Now we set the flag to true and the time to zero
	m_rhoStabTime = 0.0;
	m_useRhoStab  = true;

	//return ref to *this
	return *this;
};//End: enable the density stab


egd_mechSolverArg_t&
egd_mechSolverArg_t::disableDensityStab()
{
	if(this->isDensityStabOn() == false)
	{
		throw PGL_EXCEPT_LOGIC("The densoity stabilization is already disabled.");
	};
	if(this->m_rhoStabTime != 0.0)
	{
		throw PGL_EXCEPT_LOGIC("The density stabilization time is not zero, instead " + std::to_string(m_rhoStabTime));
	};

	//Now we set the time to NAN and flag to flase
	m_useRhoStab  = false;
	m_rhoStabTime = NAN;

	return *this;
};//End: disable density stab


bool
egd_mechSolverArg_t::isDensityStabOn()
 const
{
	pgl_assert(::pgl::isValidFloat(m_rhoStabTime) == this->m_useRhoStab);
	return this->m_useRhoStab;
}; //ENd: test if enabled


egd_mechSolverArg_t::Numeric_t
egd_mechSolverArg_t::getDensityStabTime()
 const
{
	if(this->isDensityStabOn() == false)
	{
		throw PGL_EXCEPT_LOGIC("The density stabilization is deactivated.");
	};
		pgl_assert(::pgl::isValidFloat(m_rhoStabTime));

	return m_rhoStabTime;
};//End: get stab time.


egd_mechSolverArg_t&
egd_mechSolverArg_t::setDensityStabTime(
	const Numeric_t newStabTime)
{
	if(this->isDensityStabOn() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not set the stabilization time, when stabilization is disabled.");
	};
	if(::pgl::isValidFloat(newStabTime) == false || newStabTime < 0.0)
	{
		throw PGL_EXCEPT_InvArg("The passed new stabilization time is invalid, it was "
				+ std::to_string(newStabTime) + ", the old was "
				+ std::to_string(m_rhoStabTime) );
	};

	//Set the time
	m_rhoStabTime = newStabTime;

	return *this;
};//End: set stab time



/*
 * ======================
 * Previous Timestep
 * Note the difference between, the timestep used for
 * density stabilization, which is an estimate of the
 * next timestep, and the previous, this, which is the
 * last timestep.
 */
egd_mechSolverArg_t&
egd_mechSolverArg_t::setPrevDT(
	const Numeric_t 	newPrevDT,
	Numeric_t* const 	oldPrevDT)
{
	if(::pgl::isValidFloat(newPrevDT) == false ||
	   newPrevDT <= 0.0                          )	//Zero is also prohibited
	{
		throw PGL_EXCEPT_InvArg("The given new previous timestep is invalid it was " + std::to_string(newPrevDT));
	};//ENd if: check input

	//Test if we should write the old timestep back
	if(oldPrevDT != nullptr)
	{
		*oldPrevDT = m_prevTimeStep;
	};//End write back

	//Install teh new timestep
	m_prevTimeStep = newPrevDT;

	return *this;
}; //End: setPrevDT


egd_mechSolverArg_t::Numeric_t
egd_mechSolverArg_t::getPrevDT()
 const
{
	if(this->hasPrevDT() == false)
	{
		throw PGL_EXCEPT_LOGIC("No previous timestep was installed.");
	};

	return m_prevTimeStep;
};


bool
egd_mechSolverArg_t::hasPrevDT()
 const
{
	if(::pgl::isValidFloat(m_prevTimeStep) == true)
	{
		pgl_assert(m_prevTimeStep > 0.0);	//Shall not be negative NOR ZERO
		return true;
	};//End if:

	return false;
};//End: hasPrevDT


void
egd_mechSolverArg_t::setTimeStep(
	const Numeric_t 	newTimeStep,
	Numeric_t* const 	oldTimeStep)
{
	this->setPrevDT(newTimeStep, oldTimeStep);
	return;
};


egd_mechSolverArg_t::Numeric_t
egd_mechSolverArg_t::getTimeStep()
 const
{
	return this->getPrevDT();
};


bool
egd_mechSolverArg_t::hasTimeStep()
 const
{
	return this->hasPrevDT();
};


PGL_NS_END(egd)

