#pragma once
/**
 *  \brief	This is a base class for all solver result types.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridProperty.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_generalSolverResult_i
 * \brief	This class stores the result of a solver.
 *
 * This class is the base of all result container.
 * It allows a general access to the container.
 * It also allows an abstarct interface to the different
 * solution fields.
 *
 * This class also can store a time step size. This value is
 * used to record the time step that was used to generate the
 * solution. It may be noted that depending on the underlying
 * solver, can have different interpretation.
 */
class egd_generalSolverResult_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Size_t 	 = ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
							//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	 = ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	 = ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t  = ::pgl::Numeric_t;		//!< This is the float type that is used
	using GridType_e = eGridType;			//!< This is the type of the grid where the result is interpreted.
	using SolField_t = egd_gridProperty_t;		//!< This is the type of the reuslts.
	using PropIdx_t  = SolField_t::PropIdx_t;	//!< The property index.
	using Matrix_t 	 = egd_Matrix_t<Numeric_t>;	//!< A matrix type.

	using GeneralSolverResult_ptr 	= ::std::unique_ptr<egd_generalSolverResult_i>;	//!< This is the pointer type.

	/*
	 * =========================
	 * Constructors
	 *
	 * Only destructor is public.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_generalSolverResult_i()
	 noexcept;


	/*
	 * =====================
	 * Interface functions
	 */
public:
	/**
	 * \brief	Returns the number of different solution fields.
	 */
	virtual
	Index_t
	nSolFields()
	 const
	 noexcept
	 = 0;


	/**
	 * \brief	This function returns a mutable reference
	 * 		 to the ith solution field.
	 *
	 * Counting starts with 0 and goes up to nSolFields() - 1.
	 * The ordering must be the same as the ordering is in the solver.
	 *
	 * \param  i 	The index of the solutuion field.
	 */
	virtual
	SolField_t&
	getSolField(
		const Index_t 	i)
	 = 0;


	/**
	 * \brief	This function returns a constant reference
	 * 		 to the ith solution.
	 *
	 * Counting starts with 0 and goes up to nSolFields() - 1.
	 * The ordering must be the same as the ordering is in the solver.
	 *
	 * \param  i 	The index of the solutuion field.
	 */
	virtual
	const SolField_t&
	getSolField(
		const Index_t 	i)
	 const
	 = 0;


	/**
	 * \brief	This function allows to load a property,
	 * 		 by passing the property index.
	 *
	 * This function has a default impelementation that iterates
	 * over all stored fields and returns a reference to the one
	 * that matches the property, the first one is returned.
	 * If not found an error is generated.
	 *
	 * \param  pIdx		The property to look for.
	 */
	virtual
	SolField_t&
	getSolField(
		const PropIdx_t 	pIdx);


	/**
	 * \brief	This function allows to load a property,
	 * 		 by passing the property index.
	 *
	 * This returns a constant reference.
	 * This function has a default impelementation that iterates
	 * over all stored fields and returns a reference to the one
	 * that matches the property, the first one is returned.
	 * If not found an error is generated.
	 *
	 * \param  pIdx		The property to look for.
	 */
	virtual
	const SolField_t&
	getSolField(
		const PropIdx_t 	pIdx)
	 const;


	/**
	 * \brief	This function returns true if *this is valid.
	 *
	 * This function provides a default that is reduced to the
	 * interface function. The default allo asserts if an error
	 * happens.
	 */
	virtual
	bool
	isFinite()
	 const;


	/**
	 * \brief	This function sets the solution fields to zero.
	 * 		 and deactivates the time step.
	 *
	 * This function will iterate over all solution fields and call
	 * the setConstantAll(0.0) function on them.
	 * It will also deactivate the time step if enabled.
	 *
	 * Note this function is virtual and final, this ensuring that
	 * all cleaning code is run. If deriving classes needs aditional
	 * cleaning implement the hook_setZero() function which is run
	 * first.
	 */
	virtual
	void
	setZero()
	 final;


	/**
	 * \brief	This function returns true if *this is allocated.
	 *
	 * This function will call the underling isAllocated() fucntion
	 * of the grid properties.
	 */
	virtual
	bool
	isAllocated()
	 const;


	/**
	 * \brief	This function returns the number of x grid points
	 * 		 on the basic grid.
	 *
	 * If *his is not allocated an error is generated.
	 */
	virtual
	Index_t
	Nx()
	 const;


	/**
	 * \brief	This function returns the number of y grid points
	 * 		 on the basic grid.
	 *
	 * If *this is not allocated an error is generated.
	 */
	virtual
	Index_t
	Ny()
	 const;


	/**
	 * \brief	This function returns true if *this is an
	 * 		 extended solution.
	 *
	 * This means that it is checked if the solutions are
	 * defined on an extended grid or not.
	 *
	 * The default implementation will examine the first
	 * solution field.
	 */
	virtual
	bool
	onExtendedGrid()
	 const;


	/**
	 * \brief	This function returns true if *this operates
	 * 		 on a regular grid.
	 *
	 * The default implementation will return the inverse of
	 * the extended function.
	 */
	virtual
	bool
	onRegularGrid()
	 const;


	/**
	 * \brief	This function returns true if *this is a
	 * 		 time dependend solution.
	 *
	 * This function is currently an alias of hasTimeStep(), but this
	 * could change in the future.
	 */
	virtual
	bool
	isTimeDependendSol()
	 const;


	/**
	 * \brief	This function returns true if *this has a time step.
	 *
	 * *this has a time step if the value is not NAN. A non NAN value
	 * that is not larger than zero is ilegal.
	 */
	virtual
	bool
	hasTimeStep()
	 const
	 noexcept;


	/**
	 * \brief	This function returns the time step value.
	 *
	 * It is an error if *this does not have a time step value.
	 */
	virtual
	Numeric_t
	getTimeStep()
	 const;


	/**
	 * \brief	This function allows to set the time step value.
	 *
	 * Note that the new value must be non NAN and positive.
	 * It is possible to request the old time step value.
	 * Note that this value can become NAN, the first time
	 * this fucntion is called.
	 *
	 * \param  newTimeStep		Value of the new time step.
	 * \param  oldTimeStep 		Value of the old time step.
	 *
	 * \note 	It is not possible to deactivate the time again.
	 */
	virtual
	void
	setTimeStep(
		const Numeric_t 	newTimeStep,
		Numeric_t* const 	oldTimeStep = nullptr);



	/*
	 * ========================
	 * Protected Functions
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * defaulted
	 */
	egd_generalSolverResult_i()
	 noexcept;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverResult_i(
		const egd_generalSolverResult_i&)
	 noexcept;


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverResult_i&
	operator= (
		const egd_generalSolverResult_i&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_generalSolverResult_i(
		egd_generalSolverResult_i&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverResult_i&
	operator= (
		egd_generalSolverResult_i&&)
	 noexcept;


	/**
	 * \brief	Thsi function allows to deactivate the time step.
	 *
	 * This function will set the time step value to NAN, and deactivate it.
	 * This function is only allowed if the time step was present.
	 * Also the old value is returned.
	 */
	virtual
	Numeric_t
	prot_disableTimeStep()
	 final;


	/**
	 * \brief	This function is a hook that allows deriving classes
	 * 		 to set additional memebers to zero.
	 *
	 * This function is called at the beginning of the setZero() function
	 * of *this. It allows sub classes to clean up their additional
	 * member.
	 * Note that the fields are cleaned by *this.
	 */
	virtual
	void
	hook_setZero()
	 = 0;


	/*
	 * ==========================
	 * Private Memeber
	 */
private:
	Numeric_t 	m_timeStep = NAN;	//!< The time step that is used.
}; //End: class(egd_generalSolverResult_i)

PGL_NS_END(egd)


