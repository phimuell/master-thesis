#pragma once
/**
 * \brief	This file declares the argument type of the mechanical solver.
 *
 * It is used to pass information from the calling code to the concrete implementation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_generalSolverArgument.hpp>
#include <egd_interfaces/egd_MechSolverSourceTerm.hpp>

#include <egd_phys/egd_gravitation.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_mechSolverArg_t
 * \brief	This class is used as arguments for the mechanical solvers.
 *
 * It caries all informations that are needed for the solver.
 * Note this class manages two time step, both are different.
 * The first one is the so called density stabilization time.
 * This time is used by the solver to stabilize the solution,
 * it is an _estimate_ of the new time step that will be done
 * in this iteration to advance the computation. Note that
 * the density stabilization time is allowed to be zero.
 *
 * The second time step is the previous time step, this is
 * used by a special type of somver that considered also
 * intertial problem, it is exactly the last time step.
 * Note that this value must be strictly positive.
 *
 * This class also implements the time step interface of the
 * argument base, it synchronizes the previous time
 * step to this functions.
 */
class egd_mechSolverArg_t : public egd_generalSolverArg_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 	 = egd_generalSolverArg_i;	//!< This is the base type of *this.
	using PropIdx_t  = egd_propertyIndex_t;		//!< This is the property index class.
	using Size_t 	 = ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
							//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	 = ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	 = ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t  = ::pgl::Numeric_t;		//!< This is the float type that is used

	using MechSolverArg_ptr 	= ::std::unique_ptr<egd_mechSolverArg_t>;	//!< This is the pointer type.

	using Gravitation_t = egd_gravForce_t;	//This type is used to represent a graviation.

	using MechSourceTerm_t 		= egd_mechSolverSourceTerm_t;		//!< This is the mechanical source term.
	using Source_t 			= MechSourceTerm_t::Source_t;		//!< This is the type that represents a source.
	using MechSourceTerm_ptr 	= MechSourceTerm_t::MechSolverSourceTerm_ptr;	//!< This is the managed pointer type for the source term.
	using GenSourceTerm_t 		= Base_t::GenSourceTerm_t;		//!< This is the general source term.
	using GenSourceTerm_ptr 	= Base_t::GenSourceTerm_ptr;		//!< This is the managed pointer instance of the general source term.


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_mechSolverArg_t()
 	 noexcept;


	/**
	 * \brief	Default constructor.
	 */
	egd_mechSolverArg_t();



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_mechSolverArg_t(
		const egd_mechSolverArg_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_mechSolverArg_t&
	operator= (
		const egd_mechSolverArg_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_mechSolverArg_t(
		egd_mechSolverArg_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_mechSolverArg_t&
	operator= (
		egd_mechSolverArg_t&&);


	/*
	 * ========================
	 * ========================
	 * Getter and setter.
	 */

	/*
	 * ======================
	 * Density stabilization
	 */
public:
	/**
	 * \brief	This function returns true, if density
	 * 		 stabilization is enabled.
	 *
	 * Note when density stabilization is deactivated, then
	 * still density is considered but no active version is.
	 */
	bool
	isDensityStabOn()
	 const;


	/**
	 * \brief	This function allows to enable the
	 * 		 density stabilization.
	 *
	 * This function will set the value of the density
	 * stabilization time to 0.
	 */
	egd_mechSolverArg_t&
	enableDensityStab();


	/**
	 * \brief	This function disables the density
	 * 	 	 stabilization.
	 *
	 * This is only possible if the current time is set to zero.
	 */
	egd_mechSolverArg_t&
	disableDensityStab();


	/**
	 * \brief	This function returns the timestep that is
	 * 		 used for the density stabilization.
	 *
	 * Note that the stabilization must be enabled.
	 */
	Numeric_t
	getDensityStabTime()
	 const;


	/**
	 * \brief	This fucntion allows to change the time for the
	 * 		 density stabilization.
	 *
	 * Note that the stabilization must be enabled for that.
	 *
	 * \param  newStabTime 	This is the new value for the density stabilization
	 */
	egd_mechSolverArg_t&
	setDensityStabTime(
		const Numeric_t newStabTime);


	/*
	 * =======================
	 * Previous time step
	 */
public:
	/**
	 * \brief	This function tests if *this has value of the
	 * 		 previous time step.
	 *
	 * This timestep is used for the inertia calculation.It is
	 * different from the time step that is used to stabilize
	 * the density, which is an estimation of the current
	 * time step.
	 */
	bool
	hasPrevDT()
	 const;


	/**
	 * \brief	This function returns the currently storred
	 * 		 previous time step.
	 *
	 * See description of hasPrevDT() for more information. An
	 * excpetion is generated if *This does not have a time step.
	 */
	Numeric_t
	getPrevDT()
	 const;


	/**
	 * \brief	This function allows to change the previous
	 * 		 time step.
	 *
	 * See description of hasPrevDT() for more information. An
	 * invalid value of the timestep will generate an exception.
	 *
	 * \param  newPrevDT	The new value of the timestep.
	 * \param  oldPrvDT	If provided writes the old value of the timestep.
	 * 			 this can become NAN.
	 */
	egd_mechSolverArg_t&
	setPrevDT(
		const Numeric_t 	newPrevDT,
		Numeric_t* const 	oldPrevDT = nullptr);


	/**
	 * \brief	Returns true if *this has an associated time step.
	 *
	 * This function is an alias of hasPrevDT().
	 *
	 */
	virtual
	bool
	hasTimeStep()
	 const
	 override;


	/**
	 * \brief	This function returns the currently set time step.
	 *
	 * This function is an alias of setPrevDT().
	 */
	virtual
	Numeric_t
	getTimeStep()
	 const
	 override;


	/**
	 * \brief	This function is able to change the time step of *this.
	 *
	 * This function is an alias of setPrevDT().
	 *
	 * \param  newTimeStep		The new time step size.
	 * \param  oldTimeStep 		The previously installed time step value.
	 */
	virtual
	void
	setTimeStep(
		const Numeric_t 	newTimeStep,
		Numeric_t* const 	oldTimeStep = nullptr)
	 override;


	/*
	 * ==========================
	 * Gravity
	 */
public:
	/**
	 * \brief	This function returns a constant reference to
	 * 		 the graviational type.
	 */
	const Gravitation_t&
	getGrav()
	 const;


	/**
	 * \brief	This function allows to exchange the gravitational
	 * 		 value with the new one.
	 *
	 * \param  newGrav	The new graviational value.
	 *
	 * \returns	The old one is returned.
	 */
	Gravitation_t
	setGrav(
		const Gravitation_t& 		newGrav);


	/**
	 * \brief	returns the first component of the gravitaional force.
	 *
	 * This is the one in x direction.
	 */
	Numeric_t
	g_x()
	 const;


	/**
	 * \brief	With this function you can set the x component of the
	 * 		 gravity to a certain value.
	 *
	 * \param  newGx	New gravity force in x direction.
	 *
	 * \return 	The old value.
	 */
	Numeric_t
	g_x(
		const Numeric_t newGx);


	/**
	 * \brief	returns the first component of the gravitaional force.
	 *
	 * This is the one in y direction.
	 */
	Numeric_t
	g_y()
	 const;


	/**
	 * \brief	With this function you can set the y component of the
	 * 		 gravity to a certain value.
	 *
	 * \param  newGy	New gravity force in y direction.
	 *
	 * \return 	The old value.
	 */
	Numeric_t
	g_y(
		const Numeric_t newGy);


	/*
	 * ===========================
	 * Source Term Functions
	 */
public:
	using Base_t::hasSourceTerm;
	using Base_t::hasSourceFor;
	using Base_t::getSourceFor;
	using Base_t::cgetSourceFor;
	using Base_t::getStateIdx;
	using Base_t::setStateIdx;
	using Base_t::getCurrentTime;
	using Base_t::setCurrentTime;


	/**
	 * \brief	This function returns a reference to the stored source.
	 *
	 * This function throws if *this does not have a source.
	 */
	MechSourceTerm_t&
	getSourceTerm();


	/**
	 * \brief	This function returns a constant reference to the stored source.
	 *
	 * This function throws if *this does not have a source.
	 */
	const MechSourceTerm_t&
	getSourceTerm()
	 const;


	/**
	 * \brief	This function returns a const reference to the stored source.
	 *
	 * This function throws if *this does not have a source.
	 * It is the explicit version.
	 */
	const MechSourceTerm_t&
	cgetSourceTerm()
	 const;


	/**
	 * \brief	This function is used to aquirte a source term.
	 *
	 * The source term must be moved into *this. It is an error
	 * if *this already has one.
	 *
	 * \param  sourceTerm 	The source term to load.
	 */
	void
	loadSourceTerm(
		MechSourceTerm_ptr 	sourceTerm);





	/*
	 * ==========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function tests if all enteries of *this are finite.
	 *
	 * Can also be used to test if *this is valid.
	 */
	virtual
	bool
	isFinite()
	 const
	 override;


	/*
	 * =============================
	 * Protected functions
	 */
protected:
	using Base_t::prot_getGenSourceTerm;
	using Base_t::prot_cgetGenSourceTerm;
	using Base_t::prop_adoptSourceTerm;


	/*
	 * ===========================
	 * Private members
	 */
private:
	Gravitation_t 		m_grav = {0.0, 0.0};	//!< This is the variable for storing gravitation.
	bool 			m_useRhoStab = false;	//!< This bool indicates if the density stabilization is needed.
	Numeric_t 		m_rhoStabTime = NAN;	//!< This is the time that is used for the density stabilization.
	Numeric_t 		m_prevTimeStep = NAN;	//!< This is the time that is used for the solver step.
							//!<  It is different form the time for the density stabilization.
}; //End: class(egd_mechSolverArg_t)


PGL_NS_END(egd)



