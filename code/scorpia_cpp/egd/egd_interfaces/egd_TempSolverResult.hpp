#pragma once
/**
 * \brief	This file defines a class that is used to store the result of the temperature solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_generalSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_tempSolverResult_t
 * \brief	This class stores the result of the temperature solver.
 *
 * The result of the temperature solver is quite special.
 * Temperature is expressed on the normal basic nodal grid.
 *
 * The solution to the temperature equation is thus the temperature distribution.
 * In the descrete case, it is the temperature at each grid point.
 *
 * However this is not what *this stores.
 * First of all, it does not hold the temperature at the current time step,
 * which is known, but it stores the temperature distribution of the next
 * time step.
 * Second, and more important, it will not contain the absolute temperature,
 * that was computed. Instead it will store the temperature difference between
 * the current and the next timestep.
 *
 * To make things clear, this class stores \DeltaT, and the following relation
 * holds:
 * 	T_curr + \DeltaT == T_next
 *
 * Since the temperature solution is found by an implicit scheme, the timestep
 * is always set. And its interpretion is, that it is the time step that is
 * used to evolve the system to its next stage.
 */
class egd_tempSolverResult_t : public egd_generalSolverResult_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Size_t 	 = ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
							//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	 = ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	 = ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t  = ::pgl::Numeric_t;		//!< This is the float type that is used
	using GridType_e = eGridType;			//!< This is the type of the grid where the result is interpreted.
	using SolField_t = egd_gridProperty_t;		//!< This is the type of the reuslts.
	using Matrix_t 	 = egd_Matrix_t<Numeric_t>;	//!< A matrix type.
	using Base_t 	 = egd_generalSolverResult_i; 	//!< This is the base type.

	using TempSolverResult_ptr 	= ::std::unique_ptr<egd_tempSolverResult_t>;	//!< This is the pointer type.

	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is the building constructor. This function creates
	 * an empty object. To set the size and other important
	 * settings use the
	 *
	 * See %egd_mechSolverResult_t for a full explanation.
	 */
	egd_tempSolverResult_t();


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_tempSolverResult_t();


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_tempSolverResult_t(
		const egd_tempSolverResult_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_tempSolverResult_t&
	operator= (
		const egd_tempSolverResult_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_tempSolverResult_t(
		egd_tempSolverResult_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_tempSolverResult_t&
	operator= (
		egd_tempSolverResult_t&&);


	/*
	 * =====================
	 * Interface functions
	 */
public:
	/**
	 * \brief	Returns the number of different solution fields.
	 *
	 * This function returns constant 1.
	 */
	virtual
	Index_t
	nSolFields()
	 const
	 noexcept
	 override;


	/**
	 * \brief	This function returns a mutable reference
	 * 		 to the ith solution field.
	 *
	 * If 0 is passed a reference to the temperature difference is returned.
	 *
	 * \param  i 	The index of the solutuion field.
	 */
	virtual
	SolField_t&
	getSolField(
		const Index_t 	i)
	 override;


	/**
	 * \brief	This function returns a constant reference
	 * 		 to the ith solution.
	 *
	 * Counting starts with 0 and goes up to nSolFields() - 1.
	 * The ordering must be the same as the ordering is in the solver.
	 *
	 * \param  i 	The index of the solutuion field.
	 */
	virtual
	const SolField_t&
	getSolField(
		const Index_t 	i)
	 const
	 override;


	//Will load the versions that operates on pIdx
	using Base_t::getSolField;


	/*
	 * The timestep has the meaning of the how far we will
	 * evolve the system next.
	 */
	using Base_t::hasTimeStep;
	using Base_t::getTimeStep;
	using Base_t::setTimeStep;
	using Base_t::isTimeDependendSol;


	/*
	 * ===================
	 * Member access functions
	 */
public:
	/**
	 * \brief	Return a reference to the temperature difference.
	 */
	SolField_t&
	getTempDiff()
	 noexcept;


	/**
	 * \brief	Return a constant reference to the temperature
	 * 		 difference.
	 */
	const SolField_t&
	getTempDiff()
	 const
	 noexcept;


	/**
	 * \brief	Return a constant reference to the temperature
	 * 		 difference. Explicit version.
	 */
	const SolField_t&
	cgetTempDiff()
	 const
	 noexcept;


	/**
	 * \brief	This function will return the value of the
	 * 		 largest temperature change.
	 */
	Numeric_t
	getLargestChange()
	 const
	 noexcept;


	/*
	 * ==================
	 * Managing.
	 *
	 * Here are some easy to use managing functions.
	 */
public:
	/**
	 * \brief	This function allocates *this.
	 *
	 * It will internaly create a property that will
	 * hold the temperature difference. On which
	 * grid this is stored can be selected by the
	 * its argument. This function has to be called
	 * after the default constructor has run.
	 *
	 * If this function is called several times,
	 * its argument must be the same as in the frist
	 * call, subsequent, valid, calls will set *this
	 * to zero.
	 *
	 * This function returns true, if the request was executed.
	 *
	 * \param  Ny 		The number of basic grid points in y direction.
	 * \param  Nx		The number of basic grid points in x direction.
	 * \param  isExt	Indicates if the the solver operates on the extended grid or not.
	 */
	virtual
	bool
	allocateSize(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const bool 			isExt);


	/**
	 * \brief	This function is a compatibility function.
	 *
	 * It allows to pass a grid type instead of a bool to indicate
	 * on whioch grid the solution sould be created.
	 * This function is a wrapper arround teh other allocation function.
	 *
	 * \param  Ny 		The number of basic grid points in y direction.
	 * \param  Nx		The number of basic grid points in x direction.
	 * \param  gType	The grid type that should be used.
	 */
	virtual
	bool
	allocateSize(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const eGridType 		gType)
	 final;




	/**
	 * \brief	This function tests if all enteries of *this are finite.
	 *
	 * In debug mode an assert will be triggered if a test fails.
	 */
	virtual
	bool
	isFinite()
	 const
	 override;


	/**
	 * \brief	Return the type of the solution grid.
	 */
	GridType_e
	getGridType()
	 const;


	/**
	 * \brief	Return the property of the solution field.
	 */
	PropIdx
	getPropIdx()
	 const;


	using Base_t::onRegularGrid;
	using Base_t::onExtendedGrid;


	/*
	 * =================
	 * Hooks
	 */
protected:
	/**
	 * \brief	This is the cleaning hook.
	 *
	 * Since beside the solution fields *this does not
	 * have any other memeber the function does nothing.
	 */
	virtual
	void
	hook_setZero()
	 override;


	/*
	 * ================
	 * Private Members
	 */
private:
	SolField_t 	m_tempDiff;		//!< This is the temperature difference between now and the future.
}; //End: class(egd_tempSolverResult_t)



PGL_NS_END(egd)











