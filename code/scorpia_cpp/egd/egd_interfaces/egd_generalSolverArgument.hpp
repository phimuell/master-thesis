#pragma once
/**
 * \brief	This file delcalres a generall argument type.
 *
 * This allows that all variables that a solver needs can be passed.
 * Note that thsi class is empty and is only used as base class.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_interfaces/egd_generalSolverSourceTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_generalSolverArg_i
 * \brief	This class is used as a base class for all argument types for solvers.
 *
 * A solver must create its own specializationof *this.
 * It will then contain all the arguments that the solver needs.
 *
 * The general argument also holds an instance of a source term.
 * Note that teh existence of this source term is optional.
 * Also *this does not realy implament managing and accessing of
 * the source term, this is left to the concrete implementation.
 *
 * Note this class provided functions for interfacing with an
 * assoictaed time step. However this functionality must be
 * implemented by the deriving classes.
 */
class egd_generalSolverArg_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Size_t 	 = ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
							//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	 = ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	 = ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t  = ::pgl::Numeric_t;		//!< This is the float type that is used

	using generalSolverArg_ptr 	= ::std::unique_ptr<egd_generalSolverArg_i>;	//!< This is the pointer type.
	using GenSourceTerm_t		= egd_generalSolverSourceTerm_i;		//!< This is the type of the source term.
	using PropIdx_t 		= GenSourceTerm_t::PropIdx_t;
	using Source_t 			= GenSourceTerm_t::Source_t;
	using GenSourceTerm_ptr 	= GenSourceTerm_t::GeneralSolverSourceTerm_ptr;	//!< This is a managed pointer of the source term.


	/*
	 * =========================
	 * Constructors
	 *
	 * Only destructor is public.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_generalSolverArg_i()
 	 noexcept;


	/*
	 * ==========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function tests if all enteries of *this are finite.
	 *
	 * Can also be used to test if *this is valid.
	 */
	virtual
	bool
	isFinite()
	 const
	 = 0;


	/**
	 * \brief	Returns true if *this has an associated time step.
	 *
	 * Note that depending on the concrete class, the interpretation
	 * of the value can be different.
	 * But is always associated with the evolution dynamic.
	 *
	 * The default of this function returns false.
	 */
	virtual
	bool
	hasTimeStep()
	 const;


	/**
	 * \brief	This function returns the currently set time step.
	 *
	 * See note about interpretation in teh description of the
	 * hasTimeStep() function.
	 *
	 * This function will generate an implementation error.
	 */
	virtual
	Numeric_t
	getTimeStep()
	 const;


	/**
	 * \brief	This function is able to change the time step of *this.
	 *
	 * For an interpretation of the time step see the description of
	 * hasTimeStep() function.
	 * It is also possible to return the currently installed time step.
	 * Note that if not set yet the value will be NAN.
	 *
	 * It is an error to give values that are NAN or not positive as
	 * new time step sizes.
	 *
	 * This function will generate an implementation error.
	 *
	 * \param  newTimeStep		The new time step size.
	 * \param  oldTimeStep 		The previously installed time step value.
	 */
	virtual
	void
	setTimeStep(
		const Numeric_t 	newTimeStep,
		Numeric_t* const 	oldTimeStep = nullptr);



	/*
	 * ==========================
	 * Public Source Term Functions
	 */
public:
	/**
	 * \brief	This function returns true if *this has a source term.
	 */
	bool
	hasSourceTerm()
	 const
	 noexcept;

	/**
	 * \brief	This function returns true if *this has a source term for
	 * 		 the property pIdx.
	 *
	 * \param  pIdx
	 */
	bool
	hasSourceFor(
		const PropIdx_t 	pIdx)
	 const;


	/**
	 * \brief	This function returns the grid type of the property pIdx.
	 *
	 * This function will throw if *this does not have the proeprty.
	 *
	 * \param  pIdx
	 */
	eGridType
	getGridType(
		const PropIdx_t 	pIdx)
	 const;


	/**
	 * \brief	This function returns the source term for the given property.
	 *
	 * This function throws if *this does not have that property.
	 *
	 * \param  pIdx		The property to search for.
	 */
	Source_t&
	getSourceFor(
		const PropIdx_t 	pIdx);


	/**
	 * \brief	Returns a constant reference to the source term
	 * 		 for property pIdx.
	 *
	 * This function throws if *this does not have that property.
	 *
	 * \param  pIdx		The property to search for.
	 */
	const Source_t&
	getSourceFor(
		const PropIdx_t 	pIdx)
	 const;


	/**
	 * \brief	Returns a constant reference to the source term
	 * 		 for property pIdx.
	 *
	 * This function throws if *this does not have that property.
	 * It is the explicit version.
	 *
	 * \param  pIdx		The property to search for.
	 */
	const Source_t&
	cgetSourceFor(
		const PropIdx_t 	pIdx)
	 const;


	/**
	 * \brief		This function returns the current timestep function.
	 *
	 * This is the step ID that is currently used.
	 * Note that it is not monotonic, since the provbing step and technically resolving
	 * the system is allowed.
	 */
	Size_t
	getStateIdx()
	 const;


	/**
	 * \brief	This function is used to set the state index of the simulation.
	 *
	 * The function will return the old stete index.
	 * Note that it can be negative.
	 *
	 * \param  stateIdx		The new state index.
	 */
	Size_t
	setStateIdx(
		const Size_t 		stateIdx);


	/**
	 * \brief	This function returns the current time of the simulation.
	 *
	 * The time is not monitonic increaded, since a system can be solved twice
	 * in a spetp.
	 */
	Numeric_t
	getCurrentTime()
	 const;


	/**
	 * \brief	This function allows to set the current time.
	 *
	 * The old time is returned, or minus infinity if not installed.
	 *
	 * \param  currentTime 		The new current time.
	 */
	Numeric_t
	setCurrentTime(
		const Numeric_t 	currentTime);



	/*
	 * ===========================
	 * Protected constructors.
	 */
protected:
	/**
	 * \brief	Default constructor.
	 */
	egd_generalSolverArg_i()
	 noexcept;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverArg_i(
		const egd_generalSolverArg_i&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverArg_i&
	operator= (
		const egd_generalSolverArg_i&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_generalSolverArg_i(
		egd_generalSolverArg_i&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverArg_i&
	operator= (
		egd_generalSolverArg_i&&);


	/*
	 * ==========================
	 * Protected Source Term Function
	 */
protected:
	/**
	 * \brief	This functin returns a pointer to the source term.
	 *
	 * Note that the pointer is a raw pointer. If *this does not have
	 * a source, then an exception is generated.
	 */
	GenSourceTerm_t*
	prot_getGenSourceTerm();


	/**
	 * \brief	This function returns a pointer to the constant
	 * 		 source term.
	 *
	 * Note that the pointer is a raw pointer. If *this does not have
	 * a source, then an exception is generated.
	 */
	const GenSourceTerm_t*
	prot_getGenSourceTerm()
	 const;


	/**
	 * \brief	This function returns a pointer to the constant
	 * 		 source term.
	 *
	 * This is the explicit version.
	 * Note that the pointer is a raw pointer. If *this does not have
	 * a source, then an exception is generated.
	 */
	const GenSourceTerm_t*
	prot_cgetGenSourceTerm()
	 const;


	/**
	 * \brief	This function allows to load a source term.
	 *
	 * The source term has to be moved into *this, and *this will
	 * take ownership. Note that it is not possible to exchange
	 * the source term after its initial setting. The term can be
	 * set only once.
	 *
	 * \param  sourceTerm	The term that should be adopted.
	 */
	void
	prop_adoptSourceTerm(
		GenSourceTerm_ptr 	sourceTerm);



	/*
	 * =====================
	 * Private Members
	 */
private:
	GenSourceTerm_ptr 		m_source;	//!< This is the instance of the source term.

	//Time stepping
	Size_t 				m_stateIdx = -1;	//!< The current state index
	Numeric_t 			m_currTime = -INFINITY;	//!< The current time.
}; //End: class(egd_generalSolverArg_i)

PGL_NS_END(egd)


