/**
 * \brief	This file contains the implementation of the solver interface.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridType.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_StokesSolver_i::~egd_StokesSolver_i()
 noexcept
 = default;

egd_StokesSolver_i::egd_StokesSolver_i()
 noexcept
 = default;


egd_StokesSolver_i::egd_StokesSolver_i(
	const egd_StokesSolver_i&)
 noexcept
 = default;


egd_StokesSolver_i&
egd_StokesSolver_i::operator= (
	const egd_StokesSolver_i&)
 noexcept
 = default;


egd_StokesSolver_i::egd_StokesSolver_i(
	egd_StokesSolver_i&&)
 noexcept
 = default;


egd_StokesSolver_i&
egd_StokesSolver_i::operator= (
	egd_StokesSolver_i&&)
 noexcept
 = default;


egd_StokesSolver_i::SolverResult_ptr
egd_StokesSolver_i::creatResultContainer()
 const
{
	return std::make_unique<SolverResult_t>();
};


egd_StokesSolver_i::SolverArg_ptr
egd_StokesSolver_i::createArgument()
 const
{
	return std::make_unique<SolverArg_t>();
};

egd_StokesSolver_i::SourceTerm_ptr
egd_StokesSolver_i::creatSourceTerm()
 const
{
	return std::make_unique<SourceTerm_t>();
};


void
egd_StokesSolver_i::solve(
	const GridContainer_t& 		grid,
	SolverArg_ptr& 			solArg,
	SolverResult_ptr& 		solResult)
{
	if(solArg == nullptr)
	{
		throw PGL_EXCEPT_NULL("Passed the nullptr as argument.");
	};
	if(solResult == nullptr)
	{
		throw PGL_EXCEPT_NULL("Passed the nullptr as result container.");
	};
	if(grid.getGeometry().isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set.");
	};

	this->solve(grid, solArg.get(), solResult.get());

	return;
}; //End: call the solver


bool
egd_StokesSolver_i::onExtendedGrid()
 const
{
	const eGridType gType = this->getGridType();
	if(egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_RUNTIME("The returned gridtype is wrong.");
	};

	return egd::isExtendedGrid(gType);
}; //End: onExtendedGrid


bool
egd_StokesSolver_i::onRegularGrid()
 const
{
	const eGridType gType = this->getGridType();
	if(egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_RUNTIME("The returned gridtype is wrong.");
	};

	return egd::isRegularGrid(gType);
}; //End: onRegularGrid

PGL_NS_END(egd)


