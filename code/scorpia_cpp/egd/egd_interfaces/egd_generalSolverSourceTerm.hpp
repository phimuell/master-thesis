#pragma once
/**
 *  \brief	This is a base class for all source terms.
 *
 *  See the discussion below to see that a sourcterm in egd is
 *  slightly different than it is in mathematics.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridProperty.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \brief	This class implements the base functionality of sourceterms.
 * \class 	egd_generalSolverSourceTerm_i
 *
 * A source term is used to modify the right hand side of equiations that being solved.
 * Note that this source term is quite general and not everything that is supposed to be
 * inside it is realy included in it, see the concrete source term for more information.
 *
 * This class provides a base and includes general functions for accessing the different
 * sources. This class is designed to solve system of PDEs rather than one PDE.
 * So this class is able to store and manage many different source terms.
 *
 * Internaly this class keeps a vector that stores the different sources.
 */
class egd_generalSolverSourceTerm_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Size_t 	 = ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
							//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	 = ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	 = ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t  = ::pgl::Numeric_t;		//!< This is the float type that is used
	using GridType_e = eGridType;			//!< This is the type of the grid where the result is interpreted.
	using Source_t 	 = egd_gridProperty_t;		//!< This is the type of the reuslts.
	using PropIdx_t  = Source_t::PropIdx_t;		//!< This is a property index.
	using Matrix_t 	 = egd_Matrix_t<Numeric_t>;	//!< A matrix type.

	using GeneralSolverSourceTerm_ptr 	= ::std::unique_ptr<egd_generalSolverSourceTerm_i>;	//!< This is the pointer type.


	/*
	 * =============================
	 * Protected types
	 */
protected:
	using SourceList_t 	= ::pgl::pgl_vector_t<Source_t>;	//!< This is the type that is used to store the different solution fields.
	using iterator 		= SourceList_t::iterator;		//!< Iterator to iterate over the list.
	using const_iterator 	= SourceList_t::const_iterator;		//!< Constant iterator to iterate over the list.


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_generalSolverSourceTerm_i()
	 noexcept;


	/**
	 * \brief	Default constructor.
	 *
	 * defaulted
	 */
	egd_generalSolverSourceTerm_i()
	 noexcept;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverSourceTerm_i(
		const egd_generalSolverSourceTerm_i&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverSourceTerm_i&
	operator= (
		const egd_generalSolverSourceTerm_i&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_generalSolverSourceTerm_i(
		egd_generalSolverSourceTerm_i&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverSourceTerm_i&
	operator= (
		egd_generalSolverSourceTerm_i&&);

	/*
	 * ======================
	 * Status Function
	 */
public:
	/**
	 * \brief	Returns the number of different source terms.
	 *
	 * This class is made to solve system of equations.
	 */
	Index_t
	nSources()
	 const
	 noexcept;


	/**
	 * \brief	This function checks if *this has a source of the specified property.
	 *
	 * This function throws if *this has zero property.
	 *
	 * \param  pIdx		The property to look for.
	 */
	bool
	hasProperty(
		const PropIdx_t& 	pIdx)
	 const;


	/**
	 * \brief	This function returns true if *this has a source term for that property.
	 *
	 * This function is an alias of the hasProperty() function, but is better named.
	 *
	 * \param  pIdx		The property to look for.
	 */
	bool
	hasSourceFor(
		const PropIdx_t& 	pIdx)
	 const;


	/**
	 * \brief	This function returns the property index of the ith source.
	 *
	 * \param  i 	The index of the source filed to query.
	 */
	PropIdx_t
	getPropIdx(
		const Index_t 	i)
	 const;


	/**
	 * \brief	This function returns the grid type of the ith source.
	 *
	 * \param  i 	Index of the source that should be loaded.
	 */
	eGridType
	getGridType(
		const Index_t 	i)
	 const;


	/**
	 * \brief	This function returns the grid type of the source that has
	 * 		 property index pIdx.
	 *
	 * \param  pIdx		The property index to look for.
	 */
	eGridType
	getGridType(
		const PropIdx_t 	pIdx)
	 const;



	/*
	 * =====================
	 * Accessing Functions
	 */
public:
	/**
	 * \brief	This function returns a reference to the ith solution field.
	 *
	 * \param  i 	Index of the solution field to access.
	 */
	Source_t&
	getSource(
		const Index_t 	i);


	/**
	 * \brief	This function returns a reference to the source
	 * 		 fileld of property prop.
	 *
	 * This function will look for a source with the appropriate property.
	 *
	 * \param  pIdx 	The property field.
	 */
	Source_t&
	getSource(
		const PropIdx_t& 	pIdx);



	/**
	 * \brief	This function returns a constant reference to the ith
	 * 		 source field.
	 *
	 * \param  i  	Index of the field to accept.
	 */
	const Source_t&
	getSource(
		const Index_t 	i)
	 const;


	/**
	 * \brief	This function returns a constant reference to the solution field
	 * 		 that has property pIdx.
	 *
	 * \param  pIdx		Property of the source that we should access.
	 */
	const Source_t&
	getSource(
		const PropIdx_t& 	pIdx)
	 const;


	/**
	 * \brief	This function returns a constant reference to the ith
	 * 		 source field.
	 *
	 * Explicit constant version.
	 *
	 * \param  i  	Index of the field to accept.
	 */
	const Source_t&
	cgetSource(
		const Index_t 	i)
	 const;


	/**
	 * \brief	This function returns a constant reference to the solution field
	 * 		 that has property pIdx.
	 *
	 * Explicit constant version.
	 *
	 * \param  pIdx		Property of the source that we should access.
	 */
	const Source_t&
	cgetSource(
		const PropIdx_t& 	pIdx)
	 const;



	/**
	 * \brief	This function returns true if *this is valid.
	 *
	 * This function provides a default that is reduced to the
	 * interface function. The default allo asserts if an error
	 * happens.
	 */
	virtual
	bool
	isFinite()
	 const;


	/*
	 * ==========================
	 * Iteration Functions
	 */
public:
	/**
	 * \brief	Returns an iterator to the first source term.
	 */
	iterator
	begin();


	/**
	 * \brief	Returns a constant iterator to the first source term.
	 */
	const_iterator
	begin()
	 const;


	/**
	 * \brief	Returns a constant iterator to the first source term.
	 *
	 * This is the explicit version.
	 */
	const_iterator
	cbegin()
	 const;


	/**
	 * \brief	Returns an iterator to the past the end source term.
	 */
	iterator
	end();


	/**
	 * \brief	Returns a constant iterator to the past the end source term.
	 */
	const_iterator
	end()
	 const;


	/**
	 * \brief	Returns a consatnt iterator to the past the end source term.
	 *
	 * Explicit version.
	 */
	const_iterator
	cend()
	 const;


	/*
	 * ======================
	 * Protected functions
	 */
protected:
	/**
	 * \brief	This function allows to create a new source term.
	 *
	 * The source term is appended at the end and its index is one larger
	 * than the previous one. In the case of teh first term, it is zero.
	 * This function returns the index at which location the source term
	 * was created. Note that this function may invalidate references,
	 * iterators and pointers to source terms.
	 *
	 * \param  Ny		The numebr of basic grid points in y direction.
	 * \param  Nx 		The number of basic grid points in x direction.
	 * \param  pIdx		The property index the source term shoudl have.
	 * \param  gType 	The grid type the source term should have.
	 */
	Index_t
	prot_addNewSource(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const PropIdx_t 	pIdx,
		const eGridType 	gType);


	/*
	 * ========================
	 * Protected Constructors
	 */
protected:



	/*
	 * ==================================
	 * Private Members
	 */
private:
	SourceList_t 		m_sources;	//!< The list of sources that are stored inside *this.
}; //End: class(egd_generalSolverSourceTerm_i)

PGL_NS_END(egd)


