/**
 * \brief	This file contains the code for the sourceterm used in the stokes equation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_MechSolverSourceTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)



/*
 * ==========================
 * Allocating Functions
 */

void
egd_mechSolverSourceTerm_t::allocateTerm(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const bool 		isExt)
{
	if(nSources() != 0)
	{
		throw PGL_EXCEPT_RUNTIME("Can not allocate the term twice, it already has " + std::to_string(nSources()) + ", many terms.");
	};

#define AL(p, g) this->prot_addNewSource(Ny, Nx, p.getRepresentant(), (isExt ? mkExt(g) : mkReg(g)))
	AL(PropIdx_t::VelX()    , eGridType::StVx      );
	AL(PropIdx_t::VelY()    , eGridType::StVy      );
	AL(PropIdx_t::Pressure(), eGridType::StPressure);
#undef AL

	return;
}; //End: allocating term



/*
 * ===========================
 * Constructor
 */

egd_mechSolverSourceTerm_t::egd_mechSolverSourceTerm_t()
 noexcept
 = default;


 egd_mechSolverSourceTerm_t::egd_mechSolverSourceTerm_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const bool 		isExt)
 :
  egd_mechSolverSourceTerm_t()
{
	this->allocateTerm(Ny, Nx, isExt);	//Is not a viurtual function, so no problem to call it in the constructor.
};



egd_mechSolverSourceTerm_t::~egd_mechSolverSourceTerm_t()
 noexcept
 = default;


egd_mechSolverSourceTerm_t::egd_mechSolverSourceTerm_t(
	const egd_mechSolverSourceTerm_t&)
 = default;


egd_mechSolverSourceTerm_t&
egd_mechSolverSourceTerm_t::operator= (
	const egd_mechSolverSourceTerm_t&)
 = default;


egd_mechSolverSourceTerm_t::egd_mechSolverSourceTerm_t(
	egd_mechSolverSourceTerm_t&&)
 noexcept
 = default;


egd_mechSolverSourceTerm_t&
egd_mechSolverSourceTerm_t::operator= (
	egd_mechSolverSourceTerm_t&&)
 = default;



bool
egd_mechSolverSourceTerm_t::isFinite()
 const
{
	return this->Base_t::isFinite();
}; //End: isFinite

PGL_NS_END(egd)


