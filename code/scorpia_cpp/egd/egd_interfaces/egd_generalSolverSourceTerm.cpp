/**
 * \brief	This file contains the code for the general source term interface implementation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_generalSolverSourceTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

/*
 * =========================
 * Protected function
 */
egd_generalSolverSourceTerm_i::Index_t
egd_generalSolverSourceTerm_i::prot_addNewSource(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const PropIdx_t 	pIdx,
	const eGridType 	gType)
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};

	//Test if there is already an instance only needed
	//if no sources are present
	if(this->nSources() != 0)
	{
		if(this->hasProperty(pIdx) == true)
		{
			throw PGL_EXCEPT_LOGIC("Tried to add the property " + pIdx + " twice.");
		};
	}; //End: no sources pressent

	//Now add it at the end
	m_sources.emplace_back(pIdx);	//This will create an empty one

	if(m_sources.back().getPropIdx() != pIdx)
	{
		throw PGL_EXCEPT_RUNTIME("Error in creating.");
	};

	//Now call the allocation function
	m_sources.back().allocateGrid(Ny, Nx, gType);

	//Set the property to zero
	m_sources.back().setConstantAll(0.0);

	//Return the index
	return Index_t(m_sources.size() - 1);
}; //End: add new index


/*
 * =======================
 * Status function
 */
egd_generalSolverSourceTerm_i::Index_t
egd_generalSolverSourceTerm_i::nSources()
 const
 noexcept
{
	return m_sources.size();
};


bool
egd_generalSolverSourceTerm_i::hasProperty(
	const PropIdx_t& 	pIdx)
 const
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid argument.");
	};
	if(this->nSources() <= 0)
	{
		throw PGL_EXCEPT_RUNTIME("*this does not have any sources.");
	};

	if(this->m_sources.cend() != ::std::find_if(m_sources.cbegin(), m_sources.cend(),
		[pIdx](const Source_t& x) -> bool
		{ return x.getPropIdx() == pIdx; }) )
	{
		return true;
	};

	return false;
}; //End: hasProperty


bool
egd_generalSolverSourceTerm_i::hasSourceFor(
	const PropIdx_t& 	pIdx)
 const
{
	return this->hasProperty(pIdx);
};


egd_generalSolverSourceTerm_i::PropIdx_t
egd_generalSolverSourceTerm_i::getPropIdx(
	const Index_t 	i)
 const
{
	return this->getSource(i).getPropIdx();
};


eGridType
egd_generalSolverSourceTerm_i::getGridType(
	const Index_t 	i)
 const
{
	return this->getSource(i).getType();
};


eGridType
egd_generalSolverSourceTerm_i::getGridType(
	const PropIdx_t 	pIdx)
 const
{
	return this->getSource(pIdx).getType();
};




/*
 * =========================
 * Accessing
 */
egd_generalSolverSourceTerm_i::Source_t&
egd_generalSolverSourceTerm_i::getSource(
	const Index_t 	i)
{
	return m_sources.at(i);
};


const egd_generalSolverSourceTerm_i::Source_t&
egd_generalSolverSourceTerm_i::getSource(
	const Index_t 	i)
 const
{
	return m_sources.at(i);
};


const egd_generalSolverSourceTerm_i::Source_t&
egd_generalSolverSourceTerm_i::cgetSource(
	const Index_t 	i)
 const
{
	return m_sources.at(i);
};


egd_generalSolverSourceTerm_i::Source_t&
egd_generalSolverSourceTerm_i::getSource(
 	const PropIdx_t& 	pIdx)
{
	if(this->m_sources.size() == 0)
	{
		throw PGL_EXCEPT_OutOfBound("The source term is empty.");
	};
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index is invalid.");
	};

	const auto it = ::std::find_if(m_sources.begin(), m_sources.end(),
			[&pIdx](const Source_t& x) -> bool { return (x.getPropIdx() == pIdx); });

	if(it == m_sources.end())
	{
		throw PGL_EXCEPT_OutOfBound("The requested property " + pIdx + " was not found.");
	};

	return *it;
}; //End: getSource


const egd_generalSolverSourceTerm_i::Source_t&
egd_generalSolverSourceTerm_i::getSource(
 	const PropIdx_t& 	pIdx)
 const
{
	return this->cgetSource(pIdx);
}; //End: getSource(const)


const egd_generalSolverSourceTerm_i::Source_t&
egd_generalSolverSourceTerm_i::cgetSource(
 	const PropIdx_t& 	pIdx)
 const
{
	if(this->m_sources.size() == 0)
	{
		throw PGL_EXCEPT_OutOfBound("The source term is empty.");
	};
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index is invalid.");
	};

	const auto it = ::std::find_if(m_sources.begin(), m_sources.end(),
			[&pIdx](const Source_t& x) -> bool { return (x.getPropIdx() == pIdx); });

	if(it == m_sources.end())
	{
		throw PGL_EXCEPT_OutOfBound("The requested property " + pIdx + " was not found.");
	};

	return *it;
}; //End: cgetSource


/*
 * =============================
 * Iterator functions
 */

egd_generalSolverSourceTerm_i::iterator
egd_generalSolverSourceTerm_i::begin()
{
	if(m_sources.empty())
	{
		throw PGL_EXCEPT_LOGIC("The source term is empty.");
	};

	return m_sources.begin();
};


egd_generalSolverSourceTerm_i::const_iterator
egd_generalSolverSourceTerm_i::cbegin()
 const
{
	if(m_sources.empty())
	{
		throw PGL_EXCEPT_LOGIC("The source term is empty.");
	};

	return m_sources.cbegin();
};


egd_generalSolverSourceTerm_i::const_iterator
egd_generalSolverSourceTerm_i::begin()
 const
{
	return this->cbegin();
};


egd_generalSolverSourceTerm_i::iterator
egd_generalSolverSourceTerm_i::end()
{
	if(m_sources.empty())
	{
		throw PGL_EXCEPT_LOGIC("The source term is empty.");
	};

	return m_sources.end();
};


egd_generalSolverSourceTerm_i::const_iterator
egd_generalSolverSourceTerm_i::end()
 const
{
	if(m_sources.empty())
	{
		throw PGL_EXCEPT_LOGIC("The source term is empty.");
	};

	return m_sources.cend();
};


egd_generalSolverSourceTerm_i::const_iterator
egd_generalSolverSourceTerm_i::cend()
 const
{
	if(m_sources.empty())
	{
		throw PGL_EXCEPT_LOGIC("The source term is empty.");
	};

	return m_sources.cend();
};



/*
 * ===========================
 * Constructor
 */

egd_generalSolverSourceTerm_i::egd_generalSolverSourceTerm_i()
 noexcept
 = default;


egd_generalSolverSourceTerm_i::~egd_generalSolverSourceTerm_i()
 noexcept
 = default;


egd_generalSolverSourceTerm_i::egd_generalSolverSourceTerm_i(
	const egd_generalSolverSourceTerm_i&)
 = default;


egd_generalSolverSourceTerm_i&
egd_generalSolverSourceTerm_i::operator= (
	const egd_generalSolverSourceTerm_i&)
 = default;


egd_generalSolverSourceTerm_i::egd_generalSolverSourceTerm_i(
	egd_generalSolverSourceTerm_i&&)
 noexcept
 = default;


egd_generalSolverSourceTerm_i&
egd_generalSolverSourceTerm_i::operator= (
	egd_generalSolverSourceTerm_i&&)
 = default;





bool
egd_generalSolverSourceTerm_i::isFinite()
 const
{
	for(const Source_t& s : m_sources)
	{
		if(s.isFinite() == false)
		{
			pgl_assert(false && "Found a solution field that is not finite.");
			return false;
		}; //End if: finite
	};//End for(s): iterating

	return true;
}; //End: isFinite



PGL_NS_END(egd)







