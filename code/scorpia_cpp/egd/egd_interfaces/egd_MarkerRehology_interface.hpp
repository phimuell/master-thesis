#pragma once
/**
 * \brief	This is the rehology interface.
 *
 * This interfaces is responsible for adapting the marker property.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

//FWD of teh dumper.
class egd_dumperFile_t;


/**
 * \class 	egd_MarkerRehology_i
 * \brief	This interfaces modifies the marker properties.
 *
 * This interface is used after all the marker properties has been
 * interpolated back, but _before_ the markers have been moved to
 * their new locations.
 *
 * Note that this interface is highly problem dependend.
 */
class egd_MarkerRehology_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using PropIdx_t 		= MarkerCollection_t::PropIdx_t;
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using TempSolverResult_t 	= egd_tempSolverResult_t;	//!< This is the result container for the temperature solver.
	using MechSolverResult_t 	= egd_mechSolverResult_t;	//!< This is the result container for the mechanical solver.
	using DeviatoricStress_t 	= egd_deviatoricStress_t;	//!< This is the container for the deviatoric stress.
	using DeviatoricStrainRate_t 	= egd_deviatoricStrainRate_t;	//!< This is the container for the deviatoric strain rate.

	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;		//!< Type of a marker property.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;		//!< This is the grid property.


	using MarkerRehology_ptr	= ::std::shared_ptr<egd_MarkerRehology_i>; //!< This is the pointer that stores an integrator instance.


	/*
	 * ======================
	 * Public Constructors
	 *
	 * Only the destructor is accessable from
	 * the outside.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_MarkerRehology_i();


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function performs the material change.
	 *
	 * This function is called before the marker are moved.
	 * All properties have already been interpolated back,
	 * to the current location of the markers.
	 *
	 * This function is now allowed to modify them.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  nodeStress 	This is the stress at the "nodes".
	 * \param  nodeStrain 	This is the strain at the "nodes".
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	modifyMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const DeviatoricStress_t& 	nodeStress,
		const DeviatoricStrainRate_t&	nodeStrain,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 = 0;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/**
	 * \brief	This function allows to inspect the dumper.
	 *
	 * This function can be used to enforce a certain dumper
	 * configuration.
	 * The default implementation of thei function does nothing.
	 * It is not guaranteed that thsi function is called.
	 *
	 * \param  dumper	The dumper object.
	 */
	virtual
	void
	inspectDumper(
		egd_dumperFile_t& 	dumper)
	 const;


	/**
	 * \brief	This is the building function.
	 *
	 * It takes the confighuration object and determines the
	 * concrete implementation and will call the appropriate
	 * constructor with the object and return the implementation.
	 *
	 * \param  confObj	The configuration object.
	 */
	static
	MarkerRehology_ptr
	BUILD(
		const egd_confObj_t& 	confObj);


	/*
	 * ====================
	 * Querring
	 * These functions can be used to further configure the
	 * setup object.
	 */
public:
	/**
	 * \brief	This function returns true if *this has an initial value
	 * 		 for the requested property.
	 *
	 * This function can be used by other objects to get some more detailed
	 * informations about initial values. Note that this function returns a
	 * scalar that is associated to markers.
	 * The default of this function returns false.
	 *
	 * \param  pIdx		The property in question.
	 * \param  mType	Material index.
	 */
	virtual
	bool
	hasInitialValue(
		const PropIdx_t& 	pIdx,
		const Int_t 		mType)
	 const;


	/**
	 * \brief	This function returns the initial value for the marker
	 * 		 property pIdx of material type mType.
	 *
	 * Note that the function is a scalar and the value applies to all markers
	 * of a special type.
	 * The default of this function throws.
	 *
	 * \param  pIdx		The property in question.
	 * \param  mType	Material index.
	 */
	virtual
	Numeric_t
	getInitialValue(
		const PropIdx_t& 	pIdx,
		const Int_t 		mType)
	 const;


	/*
	 * =====================
	 * Protected Constructors
	 *
	 * All other constructors are protected and defaulted.
	 * They are also marked as noexcept
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehology_i()
	 noexcept;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehology_i(
		const egd_MarkerRehology_i&)
	 noexcept;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehology_i&
	operator= (
		const egd_MarkerRehology_i&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehology_i(
		egd_MarkerRehology_i&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehology_i&
	operator= (
		egd_MarkerRehology_i&&)
	 noexcept;

}; //End class(egd_MarkerRehology_i)

PGL_NS_END(egd)


