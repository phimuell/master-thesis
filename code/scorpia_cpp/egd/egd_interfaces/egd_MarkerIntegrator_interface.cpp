/**
 * \brief	This file contains the implementation of the helper functions of the grid to marker interpolation interface.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

/*
 * Ignoring management
 */
bool
egd_MarkerIntegrator_i::isIgnoredType(
	const Index_t 		mType)
 const
{
	return (this->m_ignTypes.contains(mType) == 0)
		? false
		: true;
};//end: is ignored


bool
egd_MarkerIntegrator_i::addIgnoredType(
	const Index_t 		mType)
{
	const bool retVal = this->isIgnoredType(mType);
	this->m_ignTypes.insert(mType);
	return retVal;
}; //End: addIgnoredType


bool
egd_MarkerIntegrator_i::noIgnoredTypes()
 const
{
	return this->m_ignTypes.empty();
};


bool
egd_MarkerIntegrator_i::computeFeelVel()
 const
{
	return false;
};


bool
egd_MarkerIntegrator_i::storeFeelVel(
	MarkerCollection_t* const 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	if(this->computeFeelVel() == false)
	{
		//We do not compute feel velocity anyway
		//so we will simply return false
		return false;
	}; //No feel velocity


	/* If we are here, then we compute feel velocities but this
	 * function is not implemented, this is most likely and
	 * error so we throw. */
	throw PGL_EXCEPT_illMethod("Compute feel velocity but not use it.");
	return false;
		PGL_UNUSED(mColl, grid,
			   mSol, tSol,
			   thisDt, tStepIdx, currTime);
};//End: update feel velocities


const egd_MarkerIntegrator_i::IgnoredTypeList_t&
egd_MarkerIntegrator_i::getIgnoredTypeList()
 const
{
	return this->m_ignTypes;
};

/*
 * ============================
 * Constructor
 */

egd_MarkerIntegrator_i::~egd_MarkerIntegrator_i()
 = default;


egd_MarkerIntegrator_i::egd_MarkerIntegrator_i()
 noexcept
 = default;


egd_MarkerIntegrator_i::egd_MarkerIntegrator_i(
	const egd_MarkerIntegrator_i&)
 = default;


egd_MarkerIntegrator_i&
egd_MarkerIntegrator_i::operator= (
	const egd_MarkerIntegrator_i&)
 = default;


egd_MarkerIntegrator_i::egd_MarkerIntegrator_i(
	egd_MarkerIntegrator_i&&)
 noexcept
 = default;


egd_MarkerIntegrator_i&
egd_MarkerIntegrator_i::operator= (
	egd_MarkerIntegrator_i&&)
 = default;


PGL_NS_END(egd)


