/**
 * \brief	This file contains the implementation of the helper functions of the grid to marker interpolation interface.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_GridToMarkerInterpol_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_GridToMarkers_i::~egd_GridToMarkers_i()
 = default;


egd_GridToMarkers_i::egd_GridToMarkers_i()
 noexcept
 = default;


egd_GridToMarkers_i::egd_GridToMarkers_i(
	const egd_GridToMarkers_i&)
 noexcept
 = default;


egd_GridToMarkers_i&
egd_GridToMarkers_i::operator= (
	const egd_GridToMarkers_i&)
 noexcept
 = default;


egd_GridToMarkers_i::egd_GridToMarkers_i(
	egd_GridToMarkers_i&&)
 noexcept
 = default;


egd_GridToMarkers_i&
egd_GridToMarkers_i::operator= (
	egd_GridToMarkers_i&&)
 noexcept
 = default;


PGL_NS_END(egd)











