#pragma once
/**
 * \brief	This interface defines the process of interpolating
 * 		 the properties that where computed on the grid,
 * 		 back to the marker.
 *
 * This class performs all necessary operations.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_GridToMarkers_i
 * \brief	Interploation scheme for mapping the markers to the grid.
 *
 * This function maps the computed properties to the markers.
 * For that it is similar to the egd_MarkersToGrid_i inteerface.
 *
 * However *this also operates on the solution of the computation.
 * It is requiered that the user selects implementation that can work together.
 */
class egd_GridToMarkers_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using TempSolverResult_t 	= egd_tempSolverResult_t;	//!< This is the result container for the temperature solver.
	using MechSolverResult_t 	= egd_mechSolverResult_t;	//!< This is the result container for the mechanical solver.

	using GridToMarkers_ptr		= ::std::shared_ptr<egd_GridToMarkers_i>; //!< This is the pointer that stores an interpolation instance.



	/*
	 * ======================
	 * Public Constructors
	 *
	 * Only the destructor is accessable from
	 * the outside.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_GridToMarkers_i();


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function maps grid properties to markers.
	 *
	 * For that many different schemes are allowed to be implemented.
	 * This interface is this abstract task of mapping.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	mapBackToMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 = 0;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/**
	 * \brief	This is the building function.
	 *
	 * It takes a configuration object.
	 *
	 * \param  confObj 	The configuration object.
	 *
	 * This function is implemented in the factory folder.
	 */
	static
	GridToMarkers_ptr
	BUILD(
		const egd_confObj_t& 	confObj);


	/*
	 * =====================
	 * Protected Constructors
	 *
	 * All other constructors are protected and defaulted.
	 * They are also marked as noexcept
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Is defaulted and noexcept.
	 */
	egd_GridToMarkers_i()
	 noexcept;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_GridToMarkers_i(
		const egd_GridToMarkers_i&)
	 noexcept;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_GridToMarkers_i&
	operator= (
		const egd_GridToMarkers_i&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_GridToMarkers_i(
		egd_GridToMarkers_i&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_GridToMarkers_i&
	operator= (
		egd_GridToMarkers_i&&)
	 noexcept;

}; //End class(egd_GridToMarkers_i)




PGL_NS_END(egd)











