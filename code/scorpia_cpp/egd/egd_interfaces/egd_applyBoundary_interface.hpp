#pragma once
/**
 * \brief	This file defined the interface for a boundary applier.
 *
 * Its job is to apply a boundary condition to a grid property.
 * It is used by many different objects, for example the interpolation
 * routine needs it, and also the integrator needs it.
 *
 * It is a late object that was introduced after the first iteration
 * done. So it is located inside the grid container, which makes kind
 * of sense since it stores all grid properties.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

//FWD of the condition for all
class egd_boundaryConditions_t;

//FWD of grid property
class egd_gridProperty_t;

//FWD of the condfig object
class egd_confObj_t;

//FWD of the geometry
class egd_gridGeometry_t;


/**
 * \class 	egd_applyBoundary_i
 * \brief	This class is able to apply a boundary condition.
 *
 * Note that It will apply the boundary condition to a grid.
 * However *this does not host the boundary conditions thay are
 * passed to it if needed.
 *
 * The job of this class is to act as a generall interface of the
 * facilities to apply the conditions, hence the name.
 *
 * Note that it is also the job of concrete implementations to
 * handle the corners.
 */
class egd_applyBoundary_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using BoundCond_t 	= egd_boundaryConditions_t;	//!< This is the class that stores the boundary conditions.
	using GridProperty_t 	= egd_gridProperty_t;		//!< This is the grid property we operate on.
	using GridGeometry_t 	= egd_gridGeometry_t;		//!< The grid geometry.

	using ApplyBC_ptr 	= ::std::unique_ptr<egd_applyBoundary_i>;	//!< This is the pointer type.

	/*
	 * =========================
	 * Constructors
	 *
	 * Only destructor is public.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_applyBoundary_i()
 	 noexcept;


	/*
	 * ==========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function applies the conditions cond
	 * 		 to the grid property gProp.
	 *
	 * This function is used to enforce the conditions that are
	 * concoded in cond to gProp.
	 *
	 * Note that *this must also handle the corner points.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	apply(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 = 0;


	/**
	 * \brief	This function prints out a small
	 * 		 identification of *this.
	 *
	 * This is useful for debugging and outputing.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/*
	 * ==========================
	 * F A C T O R Y
	 */
public:
	/**
	 * \brief	This is the builder function.
	 *
	 * This function will create the boundary object.
	 * This function will only choce the appropriate
	 * type. It will not read in the condtions.
	 *
	 * \param  confObj	Configuration object.
	 */
	static
	ApplyBC_ptr
	BUILD(
		const egd_confObj_t& 		confObj);


	/*
	 * ===========================
	 * Protected constructors.
	 */
protected:
	/**
	 * \brief	Default constructor.
	 */
	egd_applyBoundary_i()
	 noexcept;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundary_i(
		const egd_applyBoundary_i&)
	 noexcept;


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundary_i&
	operator= (
		const egd_applyBoundary_i&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_applyBoundary_i(
		egd_applyBoundary_i&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundary_i&
	operator= (
		egd_applyBoundary_i&&)
	 noexcept;

}; //End: class(egd_applyBoundary_i)

PGL_NS_END(egd)


