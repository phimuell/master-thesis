#pragma once
/**
 * \brief	This file defines an interface that allows to map markers to the grid.
 *
 * This is an abstract interface that performs the mapping form the markers to the grid points.
 * How the result is interpreted is entierly left to the other functions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_MarkersToGrid_i
 * \brief	Interploation scheme for mapping the markers to the grid.
 *
 * This function maps the marker properties to properties on the grid.
 * The interpolation, and its format is entierly left up to the implementation.
 * The only requierement is that other code can handle it.
 *
 * The implementation also has to respect boundary condition if this is needed.
 */
class egd_MarkersToGrid_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.

	using MarkersToGrid_ptr	= ::std::shared_ptr<egd_MarkersToGrid_i>; //!< This is the pointer that stores an interpolation instance.



	/*
	 * ======================
	 * Public Constructors
	 *
	 * Only the destructor is accessable from
	 * the outside.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_MarkersToGrid_i();


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function maps the markers to the grid.
	 *
	 * This function performs the actual mapping.
	 * How the mapping is done and interpreted is completly
	 * left to the implementation.
	 *
	 * The grid is guaranteed to be allocated and the grid is
	 * set up, an error should be raised if this gurantee
	 * is not meet.
	 * However the implementation is free to change the sizes
	 * of the properties if needed, such an other gird is used.
	 * However the other parts of the code must be aware of
	 * this change.
	 * Note that it is not posslible to addpat the grid point
	 * locations that are stored inside the container.
	 *
	 * NOTE: It is very important to realize that this function
	 * does not apply boundary conditions to the interpolated
	 * values.
	 *
	 * \param  mColl	The marker collection that should be used.
	 * \param  gridCont	Pointer to the grid container.
	 *
	 * \throw 	If an error is detected.
	 */
	virtual
	void
	mapToGrid(
		const MarkerCollection_t& 	mColl,
		GridContainer_t* const 		gridCont)
	 = 0;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/**
	 * \brief	This is the building function.
	 *
	 * It will use the passed configuration object to determine
	 * which implementation must be loaded. It will then call
	 * the appropriate constructor and forward the configuration object.
	 *
	 * \param   confObj	The configuration object.
	 */
	static
	MarkersToGrid_ptr
	BUILD(
		const egd_confObj_t& 	confObj);


	/*
	 * =====================
	 * Protected Constructors
	 *
	 * All other constructors are protected and defaulted.
	 * They are also marked as noexcept
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkersToGrid_i()
	 noexcept;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkersToGrid_i(
		const egd_MarkersToGrid_i&)
	 noexcept;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkersToGrid_i&
	operator= (
		const egd_MarkersToGrid_i&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkersToGrid_i(
		egd_MarkersToGrid_i&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkersToGrid_i&
	operator= (
		egd_MarkersToGrid_i&&)
	 noexcept;


}; //End class(egd_MarkersToGrid_i)




PGL_NS_END(egd)











