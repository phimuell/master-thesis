#pragma once
/**
 * \brief	This file implements the source term for the temperature equation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_generalSolverSourceTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \brief	This class implements a thermal source term.
 * \class 	egd_tempSolverSourceTerm_t
 *
 * The termal source term is used inside the temperature equation
 * solver to influence the solution. It is very important that this
 * class does not includes the heating term which is handled differently.
 *
 * This class has one grid property that is used as heating term.
 * The property has the index Temperature for easy identifing
 * and for compability with the mechanical source term.
 */
class egd_tempSolverSourceTerm_t : public egd_generalSolverSourceTerm_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 	 = egd_generalSolverSourceTerm_i;	//!< This is the base type
	using Source_t 	 = Base_t::Source_t;			//!< This is the type of the reuslts.
	using PropIdx_t  = Base_t::PropIdx_t;			//!< This is a property index.
	using Matrix_t 	 = Base_t::Matrix_t;			//!< A matrix type.

	using TempSolverSourceTerm_ptr 	= ::std::unique_ptr<egd_tempSolverSourceTerm_t>;	//!< Type for a mechanical source term


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_tempSolverSourceTerm_t()
	 noexcept;


	/**
	 * \brief	Default constructor.
	 *
	 * This will construct an empty object. It has no
	 * sources. One has to call the allocateTerm()
	 * before *this can be used.
	 */
	egd_tempSolverSourceTerm_t()
	 noexcept;


	/**
	 * \brief	Building constructor.
	 *
	 * This constructor is equivalent to first calling
	 * the default constructor and then the allocateTerm()
	 * function.
	 *
	 * \param  Ny		Basic grid points in y direction.
	 * \param  Nx 		Basic grid points in x direction.
	 * \param  isExt 	Indicates if the solver is on the extended grid.
	 */
	egd_tempSolverSourceTerm_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const bool 		isExt);



	/*
	 * ======================
	 * Status Function
	 */
public:
	using Base_t::nSources;
	using Base_t::hasProperty;
	using Base_t::getPropIdx;
	using Base_t::getGridType;


	/*
	 * =====================
	 * Accessing Functions
	 */
public:
	using Base_t::getSource;
	using Base_t::cgetSource;


	/**
	 * \brief	This function returns true if *this is valid.
	 *
	 * The base implementation is called. No non negativ test is
	 * performed, even on the pressure.
	 */
	virtual
	bool
	isFinite()
	 const
	 override;


	/*
	 * ==========================
	 * Allocating function.
	 */
public:
	/**
	 * \brief	This function allocates *this.
	 *
	 * It will add the three different source term.
	 * Note that this function needs the information
	 * if the terms are extended or not and the
	 * size of the basic nodal grid.
	 *
	 * \param  Ny		Basic nodal points in y direction.
	 * \param  Nx 		Basic nodal points in x direction.
	 * \param  isExt 	Is the grid extended.
	 */
	void
	allocateTerm(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const bool 		isExt);



	/*
	 * ==========================
	 * Iteration Functions
	 */
public:
	using Base_t::begin;
	using Base_t::cbegin;
	using Base_t::end;
	using Base_t::cend;


	/*
	 * ======================
	 * Protected functions
	 */
protected:
	using Base_t::prot_addNewSource;


	/*
	 * ========================
	 * Protected Constructors
	 */
protected:
	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_tempSolverSourceTerm_t(
		const egd_tempSolverSourceTerm_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_tempSolverSourceTerm_t&
	operator= (
		const egd_tempSolverSourceTerm_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_tempSolverSourceTerm_t(
		egd_tempSolverSourceTerm_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_tempSolverSourceTerm_t&
	operator= (
		egd_tempSolverSourceTerm_t&&);
}; //End: class(egd_tempSolverSourceTerm_t)

PGL_NS_END(egd)


