#pragma once
/**
 * \brief	This file implements the argument class of the temperture solver interface.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_generalSolverArgument.hpp>
#include <egd_interfaces/egd_generalSolverSourceTerm.hpp>
#include <egd_interfaces/egd_TempSolverSourceTerm.hpp>

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_tempSolverArg_t
 * \brief	This class is used to represent the arguments that are passed to
 * 		 temperature solvers.
 *
 * This interface is the bare minimum that is needed.
 * It contains the time that is needed for the implict
 * discretization of the temperature equation in time.
 *
 * This class contains also a heating term, this is
 * needed for the uniformity of the interface.
 * The temperature solvver will manage it.
 *
 * This class implements the time step managing function of the
 * base class. It will sychronize them with with the already
 * present methods.
 */
class egd_tempSolverArg_t : public egd_generalSolverArg_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 	 	= egd_generalSolverArg_i;	//!< This is the base type of *this.
	using Size_t 	 	= ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
								//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	 	= ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t  	= ::pgl::Numeric_t;		//!< This is the float type that is used

	using HeatingTerm_t	= ::egd::egd_heatingTerm_t;	//!< This is the heating term type.
	using HeatingTerm_ptr	= HeatingTerm_t*; 		//!< This is a pointer to a heating term.

	using TempSolverArg_ptr 	= ::std::unique_ptr<egd_tempSolverArg_t>;	//!< This is the pointer type.

	using TempSourceTerm_t 		= egd_tempSolverSourceTerm_t;		//!< This is the mechanical source term.
	using Source_t 			= TempSourceTerm_t::Source_t;		//!< This is the type that represents a source.
	using TempSourceTerm_ptr 	= TempSourceTerm_t::TempSolverSourceTerm_ptr;	//!< This is the managed pointer type for the source term.
	using GenSourceTerm_t 		= Base_t::GenSourceTerm_t;		//!< This is the general source term.
	using GenSourceTerm_ptr 	= Base_t::GenSourceTerm_ptr;		//!< This is the managed pointer instance of the general source term.


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_tempSolverArg_t()
 	 noexcept;


 	/**
 	 * \brief	This constructor allows to set the timestep to the passed value.
 	 *
 	 * \param  dt 	The timestep that should be used.
 	 */
 	egd_tempSolverArg_t(
 		const Numeric_t 	dt);


	/**
	 * \brief	Default constructor.
	 */
	egd_tempSolverArg_t()
	 noexcept;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is deleted.
	 */
	egd_tempSolverArg_t(
		const egd_tempSolverArg_t&)
	 = delete;


	/**
	 * \brief	Copy assigment.
	 *
	 * Is deleted.
	 */
	egd_tempSolverArg_t&
	operator= (
		const egd_tempSolverArg_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_tempSolverArg_t(
		egd_tempSolverArg_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_tempSolverArg_t&
	operator= (
		egd_tempSolverArg_t&&);


	/*
	 * ==========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function tests if all enteries of *this are finite.
	 *
	 * Can also be used to test if *this is valid.
	 * Thsi will test the time step and the heat ands source term if they exist.
	 */
	virtual
	bool
	isFinite()
	 const
	 override;



	/*
	 * ============================
	 * Heating Term Functions
	 *
	 * This function is absically a very obscure hack to
	 * pipe the heating term through the base solver
	 * which has no concept of the heating term.
	 * Do not use them.
	 *
	 * Also note that althought source term and heating term
	 * are the same thing, EGD treats them a bit differently.
	 */
public:
	/**
	 * \brief	This function returns true if *this has a heating term.
	 *
	 * Note that a true does not mean that the heating is still allocated
	 * or at the smae place.
	 */
	bool
	hasHeatingTerm()
	 const
	 noexcept;


	/**
	 * \brief	This function resets the heating term.
	 *
	 * Note that it is an error if *this has not a heating term.
	 */
	void
	resetHeatingTerm();


	/**
	 * \brief	This function sets the heating term of
	 * 		 *this to the new heating term.
	 *
	 * Note that this function only works if *this does not have a heating term.
	 *
	 * \param  newHeat	The new heating term.
	 */
	void
	setHeatingTerm(
		const HeatingTerm_t& 		newHeat);


	/**
	 * \brief	This function returns a constant reference to the heating term.
	 */
	const HeatingTerm_t&
	getHeatingTerm()
	 const;



	/*
	 * ===================================
	 * ===================================
	 * Accesser Functions
	 */

	/*
	 * ==================================
	 * Time Step Managing
	 */
public:
	/**
	 * \brief	Returns true if *this has an associated time step.
	 *
	 * This function will check if a valid time step is installed.
	 */
	virtual
	bool
	hasTimeStep()
	 const
	 override;


	/**
	 * \brief	This function returns the currently set time step.
	 *
	 * This function internally calls the getDT() function.
	 */
	virtual
	Numeric_t
	getTimeStep()
	 const
	 override;


	/**
	 * \brief	This function is able to change the time step of *this.
	 *
	 * This function class internaly, the setDT() method.
	 *
	 * \param  newTimeStep		The new time step size.
	 * \param  oldTimeStep 		The previously installed time step value.
	 */
	virtual
	void
	setTimeStep(
		const Numeric_t 	newTimeStep,
		Numeric_t* const 	oldTimeStep = nullptr)
	 override;


	/**
	 * \brief	Returns the time step that should be used.
	 */
	Numeric_t
	getDT()
	 const;


	/**
	 * \brief	This function allows to exchange the timestep value.
	 *
	 * This function returns the old one, that could be invalid.
	 *
	 * \param  newDT	The new timestep value.
	 */
	Numeric_t
	setDT(
		const Numeric_t 	newDT);


	/*
	 * =========================
	 * Source Term Functions
	 *
	 * These functions deals with teh including of the source term.
	 * Note that the majority of thsi functionality is handled
	 * in the general solver argument.
	 */
public:
	using Base_t::hasSourceTerm;
	using Base_t::hasSourceFor;
	using Base_t::getSourceFor;
	using Base_t::cgetSourceFor;
	using Base_t::getStateIdx;
	using Base_t::setStateIdx;
	using Base_t::getCurrentTime;
	using Base_t::setCurrentTime;

	/**
	 * \brief	This function is an overload for the base function.
	 * 		 It returns a mutable reference to the source term of temperature.
	 *
	 * This function is equivalent in calling the function that is provided by
	 * base, with Temperature as argument.
	 */
	Source_t&
	getSourceFor();


	/**
	 * \brief	This function is an overload for the base function.
	 * 		 It returns a const reference to the source term of temperature.
	 *
	 * This function is equivalent in calling the function that is provided by
	 * base, with Temperature as argument.
	 */
	const Source_t&
	getSourceFor()
 	 const;


 	/**
 	 * \brief	This is the explicit constant referece fucntion for
 	 * 		 the temperature field.
 	 */
 	const Source_t&
 	cgetSourceFor()
 	 const;


	/**
	 * \brief	This function returns a reference to the stored source.
	 *
	 * This function throws if *this does not have a source.
	 */
	TempSourceTerm_t&
	getSourceTerm();


	/**
	 * \brief	This function returns a constant reference to the stored source.
	 *
	 * This function throws if *this does not have a source.
	 */
	const TempSourceTerm_t&
	getSourceTerm()
	 const;


	/**
	 * \brief	This function returns a const reference to the stored source.
	 *
	 * This function throws if *this does not have a source.
	 * It is the explicit version.
	 */
	const TempSourceTerm_t&
	cgetSourceTerm()
	 const;


	/**
	 * \brief	This function is used to aquirte a source term.
	 *
	 * The source term must be moved into *this. It is an error
	 * if *this already has one.
	 *
	 * \param  sourceTerm 	The source term to load.
	 */
	void
	loadSourceTerm(
		TempSourceTerm_ptr 	sourceTerm);



	/*
	 * =========================
	 * Private members
	 */
private:
	Numeric_t 		m_dt  	 = NAN;		//!< This is the time step size, tha is performed.
	const HeatingTerm_t* 	m_heat 	 = nullptr;	//!< This is a reference to the heating term.
}; //End: class(egd_tempSolverArg_t)

PGL_NS_END(egd)


