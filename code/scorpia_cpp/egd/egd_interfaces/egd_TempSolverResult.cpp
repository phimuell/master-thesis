/**
 * \brief	This file contains the result of the temperature solver.
 *
 * This file contains the implementation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


egd_tempSolverResult_t::egd_tempSolverResult_t()
 :
  m_tempDiff(PropIdx::TempDiff())	//this is grid agnostic
{};


egd_tempSolverResult_t::~egd_tempSolverResult_t()
 = default;


egd_tempSolverResult_t::egd_tempSolverResult_t(
	const egd_tempSolverResult_t&)
 = default;


egd_tempSolverResult_t&
egd_tempSolverResult_t::operator= (
	const egd_tempSolverResult_t&)
 = default;


egd_tempSolverResult_t::egd_tempSolverResult_t(
	egd_tempSolverResult_t&&)
 noexcept
 = default;


egd_tempSolverResult_t&
egd_tempSolverResult_t::operator= (
	egd_tempSolverResult_t&&)
 = default;


egd_tempSolverResult_t::SolField_t&
egd_tempSolverResult_t::getTempDiff()
	noexcept
{
	pgl_assert(m_tempDiff.hasValidGridType());
	return m_tempDiff;
};


const egd_tempSolverResult_t::SolField_t&
egd_tempSolverResult_t::getTempDiff()
 const
 noexcept
{
	pgl_assert(m_tempDiff.hasValidGridType());
	return m_tempDiff;
};


const egd_tempSolverResult_t::SolField_t&
egd_tempSolverResult_t::cgetTempDiff()
 const
 noexcept
{
	pgl_assert(m_tempDiff.hasValidGridType());
	return m_tempDiff;
};


Numeric_t
egd_tempSolverResult_t::getLargestChange()
 const
 noexcept
{
	pgl_assert(m_tempDiff.hasValidGridType());
	pgl_assert(this->isFinite());
	return m_tempDiff.getAbsMaxCoeff();
};


bool
egd_tempSolverResult_t::isFinite()
 const
{
	pgl_assert(m_tempDiff.hasValidGridType());

	if(m_tempDiff.isFinite() == false)
	{
		pgl_assert(false && "The temperatuire difference is not finite.");
		return false;
	}; //End if: detected non finite value

	return true;
}; //End: isFinite


bool
egd_tempSolverResult_t::allocateSize(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t	Nx,
	const bool 		isExt)
{
	if(Ny < Index_t(1) || Nx < Index_t(1))
	{
		throw PGL_EXCEPT_InvArg("Size is too small.");
	};

	//Restore the grid type
	const eGridType gType = isExt ? mkExt(eGridType::CellCenter) : mkReg(eGridType::BasicNode);

	/*
	 * We only have to do something if we are not allocated.
	 */
	if(m_tempDiff.isAllocated() == false)
	{
		//
		//We are here, so we have to allocate.


		if(egd::isBasicNodePoint(gType) == true)
		{
			//Basic nodal point.
			pgl_assert(::egd::isRegularGrid(gType));
			m_tempDiff.allocateGrid(Ny, Nx, gType, PropIdx::TempDiff());
		}
		else if(egd::isCellCentrePoint(gType) == true)
		{
			pgl_assert(::egd::isExtendedGrid(gType));	//This is the only option that makes sense

			/*
			 * Noe the default constructor will already have created
			 * a grid property, but this has the wrong property,
			 * so we have to create a new one, first.
			 */

			m_tempDiff = SolField_t(PropIdx::TempDiffCC() );
			m_tempDiff.allocateGrid(Ny, Nx, gType, PropIdx::TempDiffCC());
		}
		else
		{
			throw PGL_EXCEPT_InvArg("The grid type was invalid.");
			return false;
		};


		//We are here so we can stop
		return true;
	}; //End if: not allocated

	/*
	 * If we are here we have already allocated the grid.
	 * So we will now check if the arguemtns are the same.
	 * If so, we will set *this to zero.
	 */
	if(!((Nx == m_tempDiff.Nx()) && (Ny == m_tempDiff.Ny()) && (gType == m_tempDiff.getType())))
	{
		//We have not the same
		throw PGL_EXCEPT_InvArg("Tried to reallocate the temperature solution, with different arguments.");
	}; //End: not the same arguments.


	//Now we call the set zero function.
	this->setZero();

	return true;
}; //End: allocate Size


bool
egd_tempSolverResult_t::allocateSize(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t	Nx,
	const eGridType		gType)
{
	pgl_assert(::egd::isValidGridType(gType));

	return this->allocateSize(
			Ny, Nx,
			egd::isExtendedGrid(gType) );
}; //End: allocate wrapper


egd_tempSolverResult_t::GridType_e
egd_tempSolverResult_t::getGridType()
 const
{
	pgl_assert(m_tempDiff.hasValidGridType());
	return m_tempDiff.getType();
}; //End: getType


PropIdx
egd_tempSolverResult_t::getPropIdx()
 const
{
	pgl_assert(m_tempDiff.hasValidGridType());
	return m_tempDiff.getPropIdx();
};//End: getPropIdx


/*
 * ==========================
 * Interface functions
 */

Index_t
egd_tempSolverResult_t::nSolFields()
 const
 noexcept
{
	return 1;
};

egd_tempSolverResult_t::SolField_t&
egd_tempSolverResult_t::getSolField(
		const Index_t 	i)
{
	if(i == 0)
	{
		return this->getTempDiff();
	};

	throw PGL_EXCEPT_OutOfBound("Accessed the non existing " + std::to_string(i) + "th member.");
}; //End: get ith field


const egd_tempSolverResult_t::SolField_t&
egd_tempSolverResult_t::getSolField(
	const Index_t 	i)
 const
{
	if(i == 0)
	{
		return this->getTempDiff();
	};

	throw PGL_EXCEPT_OutOfBound("Accessed the non existing " + std::to_string(i) + "th member.");
}; //End: get ith field


void
egd_tempSolverResult_t::hook_setZero()
{
	pgl_assert(m_tempDiff.hasValidGridType());
	return;
}; //End: hook_setZero

PGL_NS_END(egd)

