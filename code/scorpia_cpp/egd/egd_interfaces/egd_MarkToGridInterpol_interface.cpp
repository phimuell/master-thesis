/**
 * \brief	This file contains the functions for the marker to gird interpolation interface.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_MarkersToGrid_i::~egd_MarkersToGrid_i()
 = default;


egd_MarkersToGrid_i::egd_MarkersToGrid_i()
 noexcept
 = default;


egd_MarkersToGrid_i::egd_MarkersToGrid_i(
	const egd_MarkersToGrid_i&)
 noexcept
 = default;


egd_MarkersToGrid_i&
egd_MarkersToGrid_i::operator= (
	const egd_MarkersToGrid_i&)
 noexcept
 = default;


egd_MarkersToGrid_i::egd_MarkersToGrid_i(
	egd_MarkersToGrid_i&&)
 noexcept
 = default;


egd_MarkersToGrid_i&
egd_MarkersToGrid_i::operator= (
	egd_MarkersToGrid_i&&)
 noexcept
 = default;


PGL_NS_END(egd)











