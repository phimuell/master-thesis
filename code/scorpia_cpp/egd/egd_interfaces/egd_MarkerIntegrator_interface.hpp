#pragma once
/**
 * \brief	This is the interface for the integrator, that advects markers.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_set.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_MarkerIntegrator_i
 * \brief	This interface defines an integrator
 * 		 that is able to advect the marker.
 *
 * This interface allows to move the markers into their new location.
 * From thsi interface you should inherent virtual.
 */
class egd_MarkerIntegrator_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using TempSolverResult_t 	= egd_tempSolverResult_t;	//!< This is the result container for the temperature solver.
	using MechSolverResult_t 	= egd_mechSolverResult_t;	//!< This is the result container for the mechanical solver.

	using IgnoredTypeList_t		= ::pgl::pgl_set_t<Index_t>;		//!< This is the list of marker types that should be ignored.

	using MarkerIntegrator_ptr		= ::std::shared_ptr<egd_MarkerIntegrator_i>; //!< This is the pointer that stores an integrator instance.


	/*
	 * ======================
	 * Public Constructors
	 *
	 * Only the destructor is accessable from
	 * the outside.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_MarkerIntegrator_i();


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function performs the interpolation.
	 *
	 * This function will advect the marker. It is allowed to use the velocities that
	 * where computed in the feel velocity function.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	moveMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 = 0;


	/**
	 * \brief	This fununction can be used to set feel velocities and
	 * 		 store them. This will NOT move the markers.
	 *
	 * This function will simuly compute the feel velocities and store them
	 * in the marker collection. It is allowed to store them in the object.
	 * and then reuse them upon the real moving.
	 *
	 * The function will return true, if the feel velocities where computed.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	bool
	storeFeelVel(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime);


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/**
	 * \brief	This function returns true if the given type is
	 * 		 inside the ignore list or not.
	 *
	 * This function provides a base implementation.
	 *
	 * \param  mType	The type that should be tested.
	 */
	virtual
	bool
	isIgnoredType(
		const Index_t 		mType)
	 const;


	/**
	 * \brief	This function is able to add a type to the ignore list.
	 *
	 * This functin will return true if the type was added and it was not known.
	 * It will return false if the type was already inside the list.
	 * If an error happens an exception will be generated.
	 * This function provides a base implementation.
	 *
	 * \param  mType 	The type to add to the ignore list.
	 */
	virtual
	bool
	addIgnoredType(
		const Index_t 		mType);


	/**
	 * \brief	This is a deleted version of the add type function.
	 */
	virtual
	bool
	addIgnoredType(
		const Numeric_t 	mType)
	 final
	 = delete;


	/**
	 * \brief	Returns true if the no marker types are ignored.
	 *
	 * This tests in essence if the list of marker types that are
	 * ignored is empty and if so returns true.
	 */
	virtual
	bool
	noIgnoredTypes()
	 const;


	/**
	 * \brief	This function returns true if the integrator will
	 * 		 compute the feel velocities.
	 *
	 * Thie function can be used to determine, if the the feel velocity
	 * property should be added to the list of marker properties.
	 *
	 * The default implementation of this function returns false.
	 */
	virtual
	bool
	computeFeelVel()
	 const;



	/**
	 * \brief	This is the building function.
	 *
	 * It takes a configuration object and sets this up.
	 *
	 * \param  confObj	This is the configuration object that is quried.
	 */
	static
	MarkerIntegrator_ptr
	BUILD(
		const egd_confObj_t& 	confObj);


	/*
	 * =====================
	 * Protected Constructors
	 *
	 * All other constructors are protected and defaulted.
	 * They are also marked as noexcept
	 */
protected:
	/**
	 * \brief	Default constructor
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerIntegrator_i()
	 noexcept;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerIntegrator_i(
		const egd_MarkerIntegrator_i&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerIntegrator_i&
	operator= (
		const egd_MarkerIntegrator_i&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerIntegrator_i(
		egd_MarkerIntegrator_i&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerIntegrator_i&
	operator= (
		egd_MarkerIntegrator_i&&);


	/*
	 * =========================
	 * Protected functions
	 */
protected:
	/**
	 * \brief	This function returns a reference to the set of ignored types.
	 *
	 * Note that the reference is only constant. To modify the the list, use the
	 * public interface functions.
	 */
	virtual
	const IgnoredTypeList_t&
	getIgnoredTypeList()
	 const
	 final;


	/*
	 * =====================
	 * Private members
	 */
private:
	IgnoredTypeList_t 		m_ignTypes;
}; //End class(egd_MarkerIntegrator_i)


PGL_NS_END(egd)


