/**
 * \brief	This file contains the code for the functions of the general argument type.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_generalSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


egd_generalSolverResult_i::egd_generalSolverResult_i()
 noexcept
 = default;


egd_generalSolverResult_i::~egd_generalSolverResult_i()
 noexcept
 = default;


egd_generalSolverResult_i::egd_generalSolverResult_i(
	const egd_generalSolverResult_i&)
 noexcept
 = default;


egd_generalSolverResult_i&
egd_generalSolverResult_i::operator= (
	const egd_generalSolverResult_i&)
 noexcept
 = default;


egd_generalSolverResult_i::egd_generalSolverResult_i(
	egd_generalSolverResult_i&&)
 noexcept
 = default;


egd_generalSolverResult_i&
egd_generalSolverResult_i::operator= (
	egd_generalSolverResult_i&&)
 noexcept
 = default;


bool
egd_generalSolverResult_i::isFinite()
 const
{
	const Index_t N = this->nSolFields();
		pgl_assert(N > 0);

	for(Index_t i = 0; i != N; ++i)
	{
		if(this->getSolField(i).isFinite() == false)
		{
			pgl_assert(false && "Found a solution field that is not finite.");
			return false;
		}; //End if: finite
	};//End for(i): iterating

	return true;
};


bool
egd_generalSolverResult_i::isAllocated()
 const
{
	if(this->nSolFields() == 0)
	{
		throw PGL_EXCEPT_LOGIC("There are no solution fields.");
	};
	pgl_assert([this](void) -> bool
			{  const Index_t  N          = this->nSolFields();
			   const bool     firstState = this->getSolField(0).isAllocated();
			   for(Index_t i = 1; i != N; ++i)
			   { if(this->getSolField(i).isAllocated() != firstState) {return false;};};
			   return true;
			}());

	return this->getSolField(0).isAllocated();
};//ENd: isAllocated


void
egd_generalSolverResult_i::setZero()
{
	//Call the hook to deactivate the subclasses
	this->hook_setZero();

	//Set all the fields to zero
	const Index_t N = this->nSolFields();
	for(Index_t i = 0; i != N; ++i)
	{
		this->getSolField(i).setConstantAll(0.0);
	}; //ENd for(i):

	//Deactivate the time step if needed
	if(this->hasTimeStep() == true)
	{
		(void)this->prot_disableTimeStep();
	};//End: disable

	return;
}; //End: setZero


bool
egd_generalSolverResult_i::onExtendedGrid()
 const
{
	if(this->nSolFields() == 0)
	{
		throw PGL_EXCEPT_LOGIC("The solution does not have any fields.");
	};
	if(this->isAllocated() == false)
	{
		throw PGL_EXCEPT_LOGIC("A non allocated grid, does not have an extendion.");
	};
	pgl_assert([this](void) -> bool
			{  const Index_t N  = this->nSolFields();
			   const Index_t NN = this->getSolField(0).isExtendedGrid();
			   for(Index_t i = 1; i != N; ++i)
			   { if(this->getSolField(i).isExtendedGrid() != NN) {return false;};};
			   return true;
			}());

	return this->getSolField(0).isExtendedGrid();
};//End: onExtendedGrid


bool
egd_generalSolverResult_i::onRegularGrid()
 const
{
	return this->onExtendedGrid()
	       ? false
	       : true;
};//End: onRegularGrid


egd_generalSolverResult_i::Index_t
egd_generalSolverResult_i::Nx()
 const
{
	if(this->nSolFields() == 0)
	{
		throw PGL_EXCEPT_LOGIC("There are no solution fields.");
	};
	if(this->isAllocated() == false)
	{
		throw PGL_EXCEPT_LOGIC("The result object is not allocated.");
	};
	pgl_assert([this](void) -> bool
			{  const Index_t N  = this->nSolFields();
			   const Index_t NN = this->getSolField(0).Nx();
			   for(Index_t i = 1; i != N; ++i)
			   { if(this->getSolField(i).Nx() != NN) {return false;};};
			   return true;
			}());

	return this->getSolField(0).Nx();
};//End: Nx


egd_generalSolverResult_i::Index_t
egd_generalSolverResult_i::Ny()
 const
{
	if(this->nSolFields() == 0)
	{
		throw PGL_EXCEPT_LOGIC("There are no solution fields.");
	};
	if(this->isAllocated() == false)
	{
		throw PGL_EXCEPT_LOGIC("The result object is not allocated.");
	};
	pgl_assert([this](void) -> bool
			{  const Index_t N  = this->nSolFields();
			   const Index_t NN = this->getSolField(0).Ny();
			   for(Index_t i = 1; i != N; ++i)
			   { if(this->getSolField(i).Ny() != NN) {return false;};};
			   return true;
			}());

	return this->getSolField(0).Ny();
};//End: Ny


egd_generalSolverResult_i::SolField_t&
egd_generalSolverResult_i::getSolField(
	const PropIdx_t 	pIdx)
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};

	//Load the number of fields
	const Index_t N = this->nSolFields();
		pgl_assert(N > 0);

	/*
	 * Going through the fields and check them
	 */
	for(Index_t i = 0; i != N; ++i)
	{
		SolField_t& f = this->getSolField(i);

		//Test if property match
		if(f.getPropIdx() == pIdx)
		{
			return f;
		}; //End if: found field
	}; //End for(i): going through all fields

	throw PGL_EXCEPT_OutOfBound("No solution field matched the passed property of " + pIdx);
};//End: getSolField by pIdx


const egd_generalSolverResult_i::SolField_t&
egd_generalSolverResult_i::getSolField(
	const PropIdx_t 	pIdx)
 const
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};

	//Load the number of fields
	const Index_t N = this->nSolFields();
		pgl_assert(N > 0);

	/*
	 * Going through the fields and check them
	 */
	for(Index_t i = 0; i != N; ++i)
	{
		const SolField_t& f = this->getSolField(i);

		//Test if property match
		if(f.getPropIdx() == pIdx)
		{
			return f;
		}; //End if: found field
	}; //End for(i): going through all fields

	throw PGL_EXCEPT_OutOfBound("No solution field matched the passed property of " + pIdx);
};//End: getSolField by pIdx


bool
egd_generalSolverResult_i::isTimeDependendSol()
 const
{
	return this->hasTimeStep();
};


bool
egd_generalSolverResult_i::hasTimeStep()
 const
 noexcept
{
	if(::pgl::isValidFloat(m_timeStep) == true)
	{
		pgl_assert(m_timeStep > 0.0);
		return true;
	};

	return false;
}; //End: hasDT


egd_generalSolverResult_i::Numeric_t
egd_generalSolverResult_i::getTimeStep()
 const
{
	if(this->hasTimeStep() == false)
	{
		throw PGL_EXCEPT_LOGIC("*this does not have a time step.");
	};

	return m_timeStep;
}; //ENd: getDT


void
egd_generalSolverResult_i::setTimeStep(
	const Numeric_t 	newTimeStep,
	Numeric_t* const 	oldTimeStep)
{
	if(::pgl::isValidFloat(newTimeStep) == false ||
	   newTimeStep <= 0.0	  		       )
	{
		throw PGL_EXCEPT_InvArg("The passed time step " + std::to_string(newTimeStep) + " was illegal.");
	};//End if: check time step

	if(oldTimeStep != nullptr)
	{
		*oldTimeStep = m_timeStep;
	};

	m_timeStep = newTimeStep;

	return;
}; //ENd: setDT

egd_generalSolverResult_i::Numeric_t
egd_generalSolverResult_i::prot_disableTimeStep()
{
	if(this->hasTimeStep() == false)
	{
		throw PGL_EXCEPT_LOGIC("The time step is not set, so can not deactivate it.");
	};

	//Save current time step
	const Numeric_t oldDT = m_timeStep;

	//Reset the memeber
	m_timeStep = NAN;

	return oldDT;
};//End reset


PGL_NS_END(egd)

