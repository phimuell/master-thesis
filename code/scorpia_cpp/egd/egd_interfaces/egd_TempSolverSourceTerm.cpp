/**
 * \brief	This file contains the code for the source term for the temperature equation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_TempSolverSourceTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)



/*
 * ==========================
 * Allocating Functions
 */

void
egd_tempSolverSourceTerm_t::allocateTerm(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const bool 		isExt)
{
	if(nSources() != 0)
	{
		throw PGL_EXCEPT_RUNTIME("Can not allocate the term twice, it already has " + std::to_string(nSources()) + ", many terms.");
	};

	this->prot_addNewSource(
			Ny, Nx,
			PropIdx_t::Temperature().getRepresentant(),
			(isExt ? mkExt(eGridType::CellCenter) : mkReg(eGridType::BasicNode)) );
	return;
}; //End: allocating term



/*
 * ===========================
 * Constructor
 */

egd_tempSolverSourceTerm_t::egd_tempSolverSourceTerm_t()
 noexcept
 = default;


 egd_tempSolverSourceTerm_t::egd_tempSolverSourceTerm_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const bool 		isExt)
 :
  egd_tempSolverSourceTerm_t()
{
	this->allocateTerm(Ny, Nx, isExt);	//Is not a viurtual function, so no problem to call it in the constructor.
};



egd_tempSolverSourceTerm_t::~egd_tempSolverSourceTerm_t()
 noexcept
 = default;


egd_tempSolverSourceTerm_t::egd_tempSolverSourceTerm_t(
	const egd_tempSolverSourceTerm_t&)
 = default;


egd_tempSolverSourceTerm_t&
egd_tempSolverSourceTerm_t::operator= (
	const egd_tempSolverSourceTerm_t&)
 = default;


egd_tempSolverSourceTerm_t::egd_tempSolverSourceTerm_t(
	egd_tempSolverSourceTerm_t&&)
 noexcept
 = default;


egd_tempSolverSourceTerm_t&
egd_tempSolverSourceTerm_t::operator= (
	egd_tempSolverSourceTerm_t&&)
 = default;



bool
egd_tempSolverSourceTerm_t::isFinite()
 const
{
	return this->Base_t::isFinite();
}; //End: isFinite

PGL_NS_END(egd)


