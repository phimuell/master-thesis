# Info
This folder contains the high level code of EGD.
It implements a framework that can be used later to simulate the processes.


## Documentation
Some documentation, especially concerning the design can be found in the report.
The code was documented using Doxygen, which will generate a searchable webpage.

## Special Behaviour
Note that EGD has for some cases a very special behaviour.
This is documented in the file "SPECIAL_BEHAVIOUR.md".
Please consult it, before doing anything.

## namespace
EGD is fully implemented inside the "egd" namespace.
It also makes use of PGL.


## Folders
Here a small summary of the folders are given.
The folders are created to separate different aspects of EGD into similar parts.
Many folder contains themself a README, that explains their content in more detail.

### EGD_CORE
This file contains the core files.
It is mainly intended to contains configuration and other things that are used everywhere.

## EGD_GRID
It contains the implementation of the grid property class, a very important class, that
stores a property on the grid. However it also implements the both containers, the marker
collection and the grid container.
However it also hosts the property index and the grid type as well.
And last but not least the interpolation routines.

## EGD_INTERFACES
This folder contains all the interfaces that are needed inside the project.
With interface we mean an abstract class that defines some functions.
Since we want to explore different schemes writing interfaces is the best
way to achieve that. An interface is for example the interface that maps
marker to the grid and backwards, although this are two different interfaces.

## EGD_IMPL
This folder contains the concrete implementation of interfaces.
In this folder a hierarchy is established to separate the different kind of interfaces.

## EGD_PHYS
This folder contains implementation of code, that calculates some physical properties.
Once can see is as a user/consumer of the values that are produced by the solvers and
grid functions.

## EGD_DUMPER
This file includes the code that is responsible for writing the simulation state into a file.
For that HDF5 is used.
This also contains a very easy HDF5 wrapper.

## EGD_FACTORY
This folder contains the code that is needed for the abstract building process.
For example it contains all the implementations of the BUILD() functions that are defined for
the interfaces. In this folder you have to modify, yes I am sorry, the code to install/
incorporate a new class.

## EGD_UTIL
This folder contains utility functions. Basically this folder was introduced a bit late.
It should have been made sooner. It will contain some helper code, that could be
independent, such as the code in EGD_PHYS, but without the physical background. Or
it is code that does not really belong to any other folder.
Some components currently located in EGD_GRID should also be placed here.


