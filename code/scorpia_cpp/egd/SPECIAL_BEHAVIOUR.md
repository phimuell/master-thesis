# Info
This file contains a list of all special or unexpected behaviour.
This behaviour deviates for some way in the expected behaviour.
The reasons could be many.
Please read this file, and the functions that are mentioned carefully.

## Subgrid Diffusion of Velocity
Subgrid diffusion of velocity is described in chapter 14 of Taras' book,
to be exact in step 11. There the time scale for the momentum diffusion
is calculated by
\begin{align}
	t_{M(m)} = \frac{\rho_m}{\eta_{vp(m)nodal} (2 / dx^2 + 2 / dy^2)}
\end{align}
In the above $\eta_{vp(m)nodal}$ is the visco-plastic viscosity that is
pull backed, from the grid to the marker, using a special scheme, that
is described in equation (13.56).
However this viscosity like parameter is not available in EGD.
Instead the viscosity that is defined for the marker is used.



