# Info
This folder contains the core part of EGD.

## EGD_CORE.HPP
This is the core file of EGD.
It is required that it is included first, before any other header.

## EGD_EIGEN.HPP
This file must be included before any Eigen header is included.
It will configure the library, it also provides some aliases.


