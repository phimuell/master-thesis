#pragma once
/**
 * \brief	This is the core file of EGD.
 *
 * The core file is the file that hosts all the configuration file.
 * We requiere that all EGD code includes this file first.
 * We use this file to include PGL and all other libraries that we need to custimize.
 *
 * Currently thsi file does not do much yet.
 */

/*
 * ====================
 * Here we would configure PGL.
 *
 */


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_strongUsing.hpp>


/*
 * END of PGL configuration
 * =========================
 */


/*
 * ========================
 * Here we would configure Eigen.
 * (Notice that we use Eigen through PGL)
 */

/*
 * We instruct Eigen to use long long int as indexing type.
 * This is fine since Eigen wants a signed index type, So
 * we give it a signed one. Using long long also assures
 * us that we can represent "the range we need".
 *
 * This settingis done for HDF5 dumper. We must be sure
 * about the type of index.
 */
#if defined(EIGEN_DEFAULT_DENSE_INDEX_TYPE)	//Undefine the macro
#	error "The Eigen macro was already defined. This indicated an error."
#endif
#define EIGEN_DEFAULT_DENSE_INDEX_TYPE signed long long int


/*
 * This macro will instruct Eigen to initialize every Coefficient array,
 * that is allocated to inizailaize with zeros. This is not done by default.
 *
 * Note that this will lead to some double setting to zero. Hoeever it is much
 * safer that way. In the future this option should be deactivated. because it
 * is done by hand, in the right places.
 *
 * Maybe acttivate the NAN macro
 */
#define EIGEN_INITIALIZE_MATRICES_BY_ZERO 1



//Include the headers
#include <pgl_linalg/pgl_EigenCore.hpp>
#include <Eigen/Core>	//To be sure we load the Core header of Eigen explicitly

/*
 * END of Eigen configuration
 * =======================
 */



/*
 * Here we import the type definition of PGL into our namespace.
 * Thus avoiding writting always ::pgl::...
 * We even extend them a little bit.
 */
PGL_NS_START(egd)

using Int_t 		= ::pgl::Int_t;
using uInt_t		= ::pgl::uInt_t;

using Int8_t 		= ::pgl::Int8_t;
using uInt8_t 		= ::pgl::uInt8_t;

using Int16_t		= ::pgl::Int16_t;
using uInt16_t 		= ::pgl::uInt16_t;

using Int32_t		= ::pgl::Int32_t;
using uInt32_t		= ::pgl::uInt32_t;

using Int64_t 		= ::pgl::Int64_t;
using uInt64_t		= ::pgl::uInt64_t;

using Size_t		= ::pgl::Size_t;
using sSize_t		= ::pgl::sSize_t;

using Numeric_t 	= ::pgl::Numeric_t;
using Real_t		= ::pgl::Real_t;
using Float_t		= ::pgl::Float_t;

using Index_t 		= ::pgl::Index_t;


/**
 * \brief	This is Explicit std::size_t aliaed.
 * \typedef 	uSize_t
 *
 * This is explicit the largest representable unsigned int that is.
 * It is basically size_t.
 */
using uSize_t 		= ::std::size_t;

/**
 * \brief	This is a strong alias of the number of
 * 		 basic nodal point in x direction.
 *
 * As a convention, the y index is allways the first one,
 * when accessing a matrix/array entry, but some constructors,
 * and functions are different in taking the number of points
 * in x direction first. This is a helper class that is used
 * to utilize the compiler for checking this.
 *
 * This using is used to encode the number of basic nodal points
 * in x direction, there is also one for the y direction.
 *
 * It is recomended that new code uses them for constructors and
 * functions.
 *
 * Note that this type is only intended to be used as a triger
 * for function overloading resolution and not more.
 */
PGL_STRONG_USING(xNodeIdx_t, Index_t);



/**
 * \brief	This is a strong alsias for the number of
 * 		 basic nodal points in the y direction.
 *
 * See the description of the x direction for more
 * information.
 */
PGL_STRONG_USING(yNodeIdx_t, Index_t);


/**
 * \brief	This macro instruct EGD to assume that the velocity field
 * 		 is incompressible.
 * \define	EGD_INCOMPRESSIBLE_MODE
 *
 * In order for this macro to take effect, it must be set to one, defining
 * it is not enough. directly this macro does not have any effect, it is used
 * by the code base to detect if compressibility is needed or not.
 *
 * By default this macro is set to zero.
 */
//Onyl set if not already defined
//TODO: add a doxygen switch
#ifndef EGD_INCOMPRESSIBLE_MODE
#	define EGD_INCOMPRESSIBLE_MODE 0
#endif



PGL_NS_END(egd)


