# This CMake file will add all the files in the current folder
# to the EGD library.

target_sources(
	egd
  PRIVATE
	# Outputing conmpile informations
  	"${CMAKE_CURRENT_LIST_DIR}/egd_core.cpp"
  PUBLIC
  	"${CMAKE_CURRENT_LIST_DIR}/egd_core.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_eigen.hpp"
  	"${CMAKE_CURRENT_LIST_DIR}/egd_eigenSparse.hpp"
)
