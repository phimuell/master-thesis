#pragma once
/**
 * \brief	This is the Eigen file EGD.
 *
 * This file also includes the sparse eigen Code.
 * Note that it only includes the basic modules.
 * And that the configurtation is done in the core file.
 *
 * This header also contains forward declarion of the
 * the Sparse LU solver of Eigen.
 */

//Include the egd_core file.
//there Eigen is configurated.
#include <egd_core/egd_core.hpp>
#include <egd_core/egd_eigen.hpp>	//load the dense Eigen code


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>


//Include the headers
#include <pgl_linalg/pgl_EigenCore.hpp>
#include <Eigen/SparseCore>		//Load the sparse core of Eigen


//Include STD
#include <type_traits>



PGL_NS_START(egd)

/**
 * \brief	This is the Sparse matrix that is used inside EGD.
 *
 * It is a column major ordering matrix, that is the default of Eigen.
 *
 * \tparam  T		The numeric type.
 * \tparam  Ops		Options, currently ignored.
 */
template<
	typename	T,
	int 		Ops = 0
>
using egd_SparseMatrix_t = ::Eigen::SparseMatrix<T, ::Eigen::ColMajor, ::egd::Index_t>;

	//Eigen wants that this is signed.
	static_assert(::std::is_signed<egd_SparseMatrix_t<Numeric_t>::StorageIndex>::value, "Must be signed.");


PGL_NS_END(egd)


/*
 * FWD Declarion of the LU solver offered by Eigen.
 */
PGL_NS_START(Eigen)

template <
	typename 	_MatrixType,
	typename 	_OrderingType
>
class SparseLU;

template<
	typename 	StorageIndex
>
class COLAMDOrdering;


template<
	typename 	Scalar
>
class DiagonalPreconditioner;


template<
	typename 	_MatrixType,
        typename 	_Preconditioner
>
class BiCGSTAB;


template<
	typename 	_Scalar,
	typename 	_StorageIndex
>
class IncompleteLUT;


template<
	typename 	_MatrixType
>
class IterScaling;



PGL_NS_END(Eigen)




