#pragma once
/**
 * \brief	This is the Eigen file EGD.
 *
 * The eigen file includes the Eigen Core headers and
 * defines some type alias.
 * This is done to have a central point of configuration.
 * This file should be included before any Eigen file,
 * but there is not obligation, since only type aliases
 * are done here.
 *
 * The configuration of eigen is done in the core file.
 *
 */

//Include the egd_core file.
//there Eigen is configurated.
#include <egd_core/egd_core.hpp>


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>


//Include the headers
#include <pgl_linalg/pgl_EigenCore.hpp>
#include <Eigen/Core>	//To be sure we load the Core header of Eigen explicitly


//Include STD
#include <string>



PGL_NS_START(egd)


/**
 * \brief	This is the matrix type that should be used inside EGD.
 *
 * It is an type alias of ::Eigen::Matrix, with suitable parameters.
 *
 * \tparam 	T 	The numberic type of the matrix
 * \tparam   	Rows	The number of rowsi (int); Defaulted to Dynamic.
 * \tparam  	Cols	The number of columns (int): Defaulted to Dynamic.
 * \tparam  	Ops 	Options, is currently ignored.
 */
template<
	typename 	T,
	int 		Rows = ::Eigen::Dynamic,
	int 		Cols = ::Eigen::Dynamic,
	int 		Ips  = 0
>
using egd_Matrix_t = ::Eigen::Matrix<T, Rows, Cols>;


/**
 * \brief	This is a vector type that is used inside EGD.
 *
 * This is also a type alias of an Eigen type.
 *
 * \tparam 	T	The numeric type that is used.
 * \tparam      N	The number of elements in the vector, defaulted to Dynamic.
 * \tparam 	Ops	Options, is currently ignored.
 */
template<
	typename 	T,
	int 		N   = ::Eigen::Dynamic,
	int 		Ops = 0
>
using egd_Vector_t = ::Eigen::Matrix<T, N, 1>;


/**
 * \brief	This is an alias of an Eigen Array.
 *
 * This is a convenient typedef of the Array class of Eigen.
 * It is similar to a matrix, with the exception that it is
 * purley componentwise.
 *
 * \tparam  T 		This is the numeric type for the calculations.
 * \tparam  Rows	The number of rows of the Array; defaults to Dynamic.
 * \tparam  Cols 	The number of cols of the Array; defaults to Dynamic
 * \tparam  Ops		Options are currently ignored.
 */
template<
	typename 	T,
	int 		Rows = ::Eigen::Dynamic,
	int 		Cols = ::Eigen::Dynamic,
	int 		Ops  = 0
>
using egd_Array_t = ::Eigen::Array<T, Rows, Cols>;


/**
 * \brief	This function prints the size of a matrix as a string.
 */
template<
	typename 	T
>
std::string
egd_printMatrixSize(
	const T& 	m)
{
	return std::string(std::to_string(m.rows()) + "x" + std::to_string(m.cols()));
}; //End: egd_printMatrixSize

#define MASI(m) ::egd::egd_printMatrixSize(m)





PGL_NS_END(egd)



