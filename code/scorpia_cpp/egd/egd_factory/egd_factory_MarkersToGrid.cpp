/**
 * \brief	This file contains the building function
 * 		 for the marker to grid interpolation process.
 */


//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

//Include the interface
#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>

//Include the implementations
#include <egd_impl/marker_to_grid/egd_MarkToGridInterpol_standard.hpp>
#include <egd_impl/marker_to_grid/egd_MarkToGridInterpol_full.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_MarkersToGrid_i::MarkersToGrid_ptr
egd_MarkersToGrid_i::BUILD(
	const egd_confObj_t& 	confObj)
{
	//Extracts the type
	const std::string type = confObj.getMarkerToGridImp();
		pgl_assert(false == type.empty()              ,
			   true  == confObj.getINI().hasKeys() );

	if(type == "std" || type == "org")
	{
		return std::make_unique<egd_markersToGridStandard_t>(confObj);
	}
	if(type == "full" || type == "ext")
	{
		return std::make_unique<egd_markersToGridFull_t>(confObj);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The type for the markers to grid interpolation object \"" + type + "\" was not known.");
	};
}; //End: build

PGL_NS_END(egd)

