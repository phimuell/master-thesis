/**
 * \brief 	This file contains the code that deals with instanziating a grid to marker implementation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

//Include the interface
#include <egd_interfaces/egd_GridToMarkerInterpol_interface.hpp>

//Include the implementations
#include <egd_impl/grid_to_marker/egd_GridToMarkInterpol_standard.hpp>
#include <egd_impl/grid_to_marker/egd_GridToMarkInterpol_velSGOnly.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_GridToMarkers_i::GridToMarkers_ptr
egd_GridToMarkers_i::BUILD(
	const egd_confObj_t& 	confObj)
{
	//load the type or instance string
	const std::string type = confObj.getGridToMarkerImpl();
		pgl_assert(true  == confObj.getINI().hasKeys(),
			   false == type.empty()               );


	if(type == "std" || type == "s" || type == "temponly" || type == "tonly")
	{
		return std::make_unique<egd_gridToMarkersStandard_t>(confObj);
	}
	else if(type == "velo")
	{
		return std::make_unique<egd_gridToMarkersVelOnlySG_t>(confObj);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The grid to marker implementation \"" + type + "\" is unkown.");
	};

	//We will never be here
}; //END: BUILD



PGL_NS_END(egd)


