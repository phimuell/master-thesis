# Info
This folder contains the code that deals with the building of the interfaces.
It basically contains the implementations of the different BUILD() functions
that are defined in the interfaces.

Each file implements one build functions. They are basically one large if
statement that checks the requested implementation and returns a pointer.



