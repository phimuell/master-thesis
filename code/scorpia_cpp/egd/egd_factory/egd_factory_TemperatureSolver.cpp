/**
 * \brief 	This file contains the factory code for the temperature solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

//Include the interface
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>

//Include the implementations
#include <egd_impl/temp_solver/egd_stdTempSolverRegularGrid.hpp>
#include <egd_impl/temp_solver/egd_stdTempSolverExtendedGrid.hpp>
#include <egd_impl/temp_solver/egd_stdTempSolver_NULL.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_TempSolver_i::TempSolver_ptr
egd_TempSolver_i::BUILD(
	const egd_confObj_t& 	confObj)
{
	//Extracts the type
	const std::string type = confObj.getTemperatureSolverImpl();
		pgl_assert(false == type.empty()              ,
			   true  == confObj.getINI().hasKeys() );

	if(type == "std" || type == "org" || type == "reg")
	{
		return std::make_unique<egd_StdTempSolverRegGrid_t>(confObj);
	}
	if(type == "std2" || type == "ext")
	{
		return std::make_unique<egd_StdTempSolverExtGrid_t>(confObj);
	}
	if(type == "nulle" || type == "nullext" || type == "extnull")
	{
		return std::make_unique<egd_NullTempSolverExt_t>(confObj);
	}
	if(type == "nullr" || type == "nullreg" || type == "regnull")
	{
		return std::make_unique<egd_NullTempSolverReg_t>(confObj);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The type for the temperature solver object \"" + type + "\" was not known.");
	};

	return egd_TempSolver_i::TempSolver_ptr(nullptr);
}; //End: build

PGL_NS_END(egd)

