/**
 * \brief	This file contains the code for the factory for the rheology.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

//Include the interface
#include <egd_interfaces/egd_MarkerRehology_interface.hpp>

//Include the implementations
#include <egd_impl/rheology/egd_MarkerRheology_orgProject.hpp>
#include <egd_impl/rheology/egd_MarkerRheologyNULL.hpp>
#include <egd_impl/rheology/egd_MarkerRheology_pulledBallSin.hpp>
#include <egd_impl/rheology/egd_MarkerRheology_constBall.hpp>
#include <egd_impl/rheology/egd_MarkerRheology_rotDisc.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_MarkerRehology_i::MarkerRehology_ptr
egd_MarkerRehology_i::BUILD(
	const egd_confObj_t& 	confObj)
{
	//Extracts the type
	const std::string type = confObj.getRheologyImpl();
		pgl_assert(false == type.empty()              ,
			   true  == confObj.getINI().hasKeys() );

	if(type == "std" || type == "org")
	{
		return std::make_unique<egd_markerRheologyOrgProj_t>(confObj);
	}
	if(type == "null")
	{
		return std::make_unique<egd_MarkerRehologyNull_t>(confObj);
	}
	if(type == "pballsin" || type == "pulledballsin")
	{
		return std::make_unique<egd_MarkerRehologyPulledBallSin_t>(confObj);
	}
	if(type == "pballconst" || type == "constball" || type == "cball")
	{
		return std::make_unique<egd_MarkerRehologyConstBall_t>(confObj);
	}
	if(type == "rottc" || type == "rotconst")
	{
		return std::make_unique<egd_MarkerRehologyRotDiscTC_t>(confObj);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The type for the rheology object \"" + type + "\" was not known.");
	};
}; //End: build



PGL_NS_END(egd)


