/**
 * \brief	This file contains the functions to build an instance of the boundary applier.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

//Include the interface
#include <egd_interfaces/egd_applyBoundary_interface.hpp>

//Include the implementations
#include <egd_impl/apply_boundary/pipeCond/egd_applyBoundary_pipe.hpp>
#include <egd_impl/apply_boundary/shearCond/egd_applyBoundary_shearing.hpp>
#include <egd_impl/apply_boundary/egd_applyBoundary_NULL.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_applyBoundary_i::ApplyBC_ptr
egd_applyBoundary_i::BUILD(
	const egd_confObj_t& 	confObj)
{
	//load the type or instance string
	const std::string type = confObj.getApplyBC();
		pgl_assert(true  == confObj.getINI().hasKeys(),
			   false == type.empty()               );


	if(type == "std" || type == "pipe" || type == "pipebc" || type == "pipecond")
	{
		return std::make_unique<egd_applyBoundaryPipe_t>(confObj);
	}
	else if(type == "null" || type == "false" || type == "no")
	{
		return std::make_unique<egd_applyBoundaryNULL_t>(confObj);
	}
	else if(type == "shear" || type == "spipe" || type == "mdivt")
	{
		return std::make_unique<egd_applyBoundaryShear_t>(confObj);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The applyBC implementation \"" + type + "\" is unkown.");
	};

	//We will never be here
}; //END: BUILD

PGL_NS_END(egd)

