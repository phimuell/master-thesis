/**
 * \brief	This file contains the factory code for the set up process.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

//Include the interface
#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

//Include the implementations
#include <egd_impl/setup/egd_MarkerSetUp_orgProject.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_simpleBall.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_simpleBallHighEta.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_simpleBallFalling.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_simpleBallZeroEta.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_rotatingDisk.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_rotatingDiskZeroEta.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_rotatingDiskHighEta.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_rotatingDiskHeavy.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_rotatingDiskTC.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_rotatingDiskTCLEta.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_shearTest.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_pulledBall.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_pulledBallRe3.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_pulledBallOrg.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_linMomentum.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_linMomentum2.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_linMomentum2ZEta.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_constBall.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_constBallTurb.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_constBallHighEta.hpp>
#include <egd_impl/setup/egd_MarkerSetUp_constBallHighEta2.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_MarkerSetUp_i::MarkerSetUp_ptr
egd_MarkerSetUp_i::BUILD(
	const egd_confObj_t& 	confObj)
{
	//Extracts the type
	const std::string type = confObj.getSetUpImpl();
		pgl_assert(true  == confObj.getINI().hasKeys(),
			   false == type.empty()               );

	if(type == "std" || type == "org")
	{
		return std::make_unique<egd_markerSetUpOrgProj_t>(confObj);
	}
	else if(type == "ball1")
	{
		return std::make_unique<egd_markerSetUpBallOne_t>(confObj);
	}
	else if(type == "ball1zero" || type == "ballzero" || type == "ballz")
	{
		return std::make_unique<egd_markerSetUpBallZeroEta_t>(confObj);
	}
	else if(type == "ball1he")
	{
		return std::make_unique<egd_markerSetUpBallTwoHighEta_t>(confObj);
	}
	else if(type == "ball2" || type == "falling" || type == "fall")
	{
		return std::make_unique<egd_markerSetUpBallTwo_t>(confObj);
	}
	else if(type == "rotdisc" || type == "rotball" || type == "rot")
	{
		return std::make_unique<egd_markerSetUpRotDisc_t>(confObj);
	}
	else if(type == "rotheavy" )
	{
		return std::make_unique<egd_markerSetUpRotDiscHeavy_t>(confObj);
	}
	else if(type == "rotdisczero" || type == "rotballzero" || type == "rotzero" || type == "rotdiscz")
	{
		return std::make_unique<egd_markerSetUpRotDiscZeroEta_t>(confObj);
	}
	else if(type == "rotdischigh" || type == "rotballhigh" || type == "rothigh")
	{
		return std::make_unique<egd_markerSetUpRotDiscHighEta_t>(confObj);
	}
	else if(type == "rotisctc" || type == "rottc")
	{
		return std::make_unique<egd_markerSetUpRotDiscTC_t>(confObj);
	}
	else if(type == "rotisctcleta" || type == "rottcle")
	{
		return std::make_unique<egd_markerSetUpRotDiscTCLowEta_t>(confObj);
	}
	else if(type == "shear" || type == "mdiv" || type == "wedge")
	{
		return std::make_unique<egd_markerSetUpShearSetting_t>(confObj);
	}
	else if(type == "pulledball" || type == "pball")
	{
		return std::make_unique<egd_markerSetUpPulledBall_t>(confObj);
	}
	else if(type == "pulledballre3" || type == "pballre3")
	{
		return std::make_unique<egd_markerSetUpPulledBallRe3_t>(confObj);
	}
	else if(type == "pulledballorg" || type == "pballorg")
	{
		return std::make_unique<egd_markerSetUpPulledBallOrg_t>(confObj);
	}
	else if(type == "linmom" || type == "linimp" || type == "linmomentum")
	{
		return std::make_unique<egd_markerSetUpLinMomentum_t>(confObj);
	}
	else if(type == "linmom2" || type == "linimp2" || type == "linmomentum2")
	{
		return std::make_unique<egd_markerSetUpLinMomentum2_t>(confObj);
	}
	else if(type == "linmom2ze" || type == "linimp2ze" || type == "linmomentum2zeroeta")
	{
		return std::make_unique<egd_markerSetUpLinMomentum2ZeroEta_t>(confObj);
	}
	else if(type == "constball" || type == "cball" || type == "dragball")
	{
		return std::make_unique<egd_markerSetUpConstBall_t>(confObj);
	}
	else if(type == "constballhe" || type == "cballhe" || type == "dragballhe")
	{
		return std::make_unique<egd_markerSetUpConstBallHighEta_t>(confObj);
	}
	else if(type == "constballhe2" || type == "cballhe2" || type == "dragballhe2")
	{
		return std::make_unique<egd_markerSetUpConstBallHighEta2_t>(confObj);
	}
	else if(type == "constballturb" || type == "cballturb" || type == "dragballt")
	{
		return std::make_unique<egd_markerSetUpConstBallTurb_t>(confObj);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The type for the setup object \"" + type + "\" was not known.");
	};
}; //End: build



PGL_NS_END(egd)


