/**
 * \brief 	This file contains the factory code for the Stokes solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

//Include the interface
#include <egd_interfaces/egd_StokesSolver_interface.hpp>

//Include the implementations
#include <egd_impl/mech_solver/egd_MechSolverImpl.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_StokesSolver_i::StokesSolver_ptr
egd_StokesSolver_i::BUILD(
	const egd_confObj_t& 	confObj)
{
	//Extracts the type
	const std::string type = confObj.getStokesSolverImpl();
		pgl_assert(false == type.empty()              ,
			   true  == confObj.getINI().hasKeys() );

	if(type == "std" || type == "org" || type == "reg")
	{
		return std::make_unique<egd_StokesSolverStdReg_t>(confObj);
	}
	else if(type == "std2" || type == "ext")
	{
		return std::make_unique<egd_StokesSolverStdExt_t>(confObj);
	}
	else if(type == "nsreg")
	{
		return std::make_unique<egd_NavierStokesSolverStdReg_t>(confObj);
	}
	else if(type == "nsext")
	{
		return std::make_unique<egd_NavierStokesSolverStdExt_t>(confObj);
	}
	else if(type == "sshear")
	{
		//For some reasons I do not fully understand YCM complains about no viable conversion
		return std::make_unique<egd_StokesSolverShearExt_t>(confObj);
	}
	else if(type == "nsshear")
	{
		//For some reasons I do not fully understand YCM complains about no viable conversion
		return std::make_unique<egd_NavierStokesSolverShearExt_t>(confObj);
	}
	else if(type == "nsextsin")
	{
		return std::make_unique<egd_NavierStokesSolverPulledBallSinExt_t>(confObj);
	}
	else if(type == "nsextconst")
	{
		return std::make_unique<egd_NavierStokesSolverPulledBallConstExt_t>(confObj);
	}
	else if(type == "stextsin")
	{
		return std::make_unique<egd_StokesSolverPulledBallSinExt_t>(confObj);
	}
	else if(type == "stextconst")
	{
		return std::make_unique<egd_StokesSolverPulledBallConstExt_t>(confObj);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The type for the Stokes solver object \"" + type + "\" was not known.");
	};
}; //End: build



PGL_NS_END(egd)


