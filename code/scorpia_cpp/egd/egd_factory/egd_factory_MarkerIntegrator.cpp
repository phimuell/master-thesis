/**
 * \brief	This file contains the factory function of the marker integrator.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

//Include interface
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>

//Include implementation
#include <egd_impl/integrator/egd_MarkerIntegrator_standard.hpp>
#include <egd_impl/integrator/egd_MarkerIntegrator_jennyMeyer_linMinModBC.hpp>
#include <egd_impl/integrator/egd_MarkerIntegrator_RK4LinP.hpp>
#include <egd_impl/integrator/egd_MarkerIntegrator_RK4Test64.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_MarkerIntegrator_i::MarkerIntegrator_ptr
egd_MarkerIntegrator_i::BUILD(
	const egd_confObj_t& 	confObj)
{
	//Load the type of the integrator
	const std::string type = confObj.getIntegratorImpl();
		pgl_assert(false == type.empty()                 ,
			   true  == confObj.getINI().hasKeys() );

	if(type == "std")
	{
		return std::make_unique<egd_markerIntegratorStd_t>(confObj);
	}
	else if(type == "jmmod" || type == "jennyminmod" || type == "Linminmod")
	{
		return std::make_unique<egd_markerIntegrator_jmILinMinMod_t>(confObj);
	}
	else if(type == "linp" || type == "1323")
	{
		return std::make_unique<egd_markerIntegratorRK4LinP_t>(confObj);
	}
	else if(type == "test64" || type == "t64")
	{
		return std::make_unique<egd_markerIntegratorRK4Test64_t>(confObj);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The integrator implementation \"" + type + "\" is not known.");
	};
};



PGL_NS_END(egd)











