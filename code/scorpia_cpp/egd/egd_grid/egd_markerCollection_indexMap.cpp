/**
 * \brief	This file implements the functions that are related to the index map.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl_linalg/pgl_EigenCore.hpp>			//Include the main LINALG header


//Include the Eigen Headers


//Include STD
#include <functional>



PGL_NS_START(egd)


/*
 * This function is nopt so efficient.
 * But we only run it once, so no problem.
 */
void
egd_markerCollection_t::setUpIndexesMap()
{
	if(m_idxMap.size() > 0)
	{
		throw PGL_EXCEPT_LOGIC("Can only set up the index map, if it is not set yet.");
	};
	if(this->hasProperty(PropIdx_t::Type()) == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not set up the index map automatically. The map does not have the type property.");
	};

	//Get a reference to the type property
	const auto& typeProp = this->cgetMarkerProperty(PropIdx_t::Type());

	//Test if the property has the correct size
	if(typeProp.size() != m_nMarkers)
	{
		throw PGL_EXCEPT_RUNTIME("The number of markers is inconsistent.");
	};


	/*
	 * Going through the markers and find the type
	 */
	for(Index_t m = 0; m != m_nMarkers; ++m)
	{
		const Numeric_t mType_num = typeProp[m];	//Load the type of marker m, but as a float
		const   Index_t mType     = Index_t(mType_num);	//Convert the type into an int

		//Load the index vector and insert it
		m_idxMap[mType].push_back(m);	//Store index of marker
	}; //End for(m):

	return;
}; //End: setUp index maps

PGL_NS_END(egd)







