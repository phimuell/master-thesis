/**
 * \brief	This function contains basic interpolation routines.
 *
 * These functions are provided as building blocks.
 * Note that this fucntion uses some templates to keep code dublication small.
 * I hope that the compiler will remove all the inner most ifs.
 * If not the branch predictor will work as a charm.
 *
 * Note this file contains only the functions to do non local interpolation.
 * This was the default and only one prior.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridType.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/dynamic_bitset.hpp>


//Include STD


PGL_NS_START(egd)



/**
 * \brief	This is the internal function that performs the mapping.
 *
 * It has only one loop and a lot of ifs in it. Howeverr some of the
 * if conditions are templated variables. The compiler should/must
 * be able to figuring that out. So the ifs will be removed completely.
 * At least it will enables the branch predictor to predict very accurate.
 *
 * \param  gProperty	This is the target that will be filled.
 * \param  mProperty	This is the marker property, the source.
 * \param  mWeights	This are the individual marker weights.
 * \param  gWeights 	This is the grid weights, is ignored when addapting is passed.
 * \param  modWeights	This is a modification for the weight, must be a vector of the same length as markers.
 *
 * \tparam  isExtended		Bool indicates if the grid is an extended grid.
 * \tparam  isModified 		Bool to indicate if the weights are modified.
 *
 */
template<
	bool 		isExtended,
	bool 		isModifiedWeights
>
extern
void
egdInternal_basicGridToMarker(
	egd_gridProperty_t* const 		gProperty_,
	const egd_markerProperty_t& 		mProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_gridWeight_t& 		gWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				reapplyDisableGhostNode);



// Thius is the real interface function
void
egd_mapToGrid(
	egd_gridProperty_t* const 		gProperty_,
	const egd_markerProperty_t& 		mProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_gridWeight_t& 		gWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				reapplyDisableGhostNode)
{
	if(gProperty_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The property argument was the null pointer.");
	};
	egd_gridProperty_t& gProperty = *gProperty_;	//Alias it

	if(gProperty.rows() < 1 ||
	   gProperty.cols() < 1   )
	{
		throw PGL_EXCEPT_InvArg("The passed property is too small " + MASI(gProperty));
	};
	if(mProperty.size() < 1)
	{
		throw PGL_EXCEPT_InvArg("Amount of markers is too small, it was " + std::to_string(mProperty.size()));
	};
	if(::egd::isValidGridType(gProperty.getType()) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid proeprty is on an invalid grid.");
	};

	//
	//Get the decisifness properties
	const bool isExtended = gProperty.isExtendedGrid();
	const bool isModifiedWeights = (modWeights == nullptr) ? false : true;

#define CALL(extended, modWe) if((isExtended == extended) && (isModifiedWeights == modWe)) {egdInternal_basicGridToMarker<extended, modWe>(&gProperty, mProperty, mWeights, gWeights, idxI, idxJ, modWeights, reapplyDisableGhostNode);}
	CALL(true, true);
	CALL(true, false);
	CALL(false, true);
	CALL(false, false);
#undef CALL

	return;
}; //End: mapToGrid switch





/*
 * ================================
 * Impl
 */
template<
	bool 		isExtended,
	bool 		isModifiedWeights
>
void
egdInternal_basicGridToMarker(
	egd_gridProperty_t* const 		gProperty_,
	const egd_markerProperty_t& 		mProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_gridWeight_t& 		gWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				reapplyDisableGhostNode)
{
	pgl_assert(gProperty_ != nullptr);	//Alias the gProperty
	egd_gridProperty_t& gProperty = *gProperty_;

	if(isExtended != gProperty.isExtendedGrid())
	{
		throw PGL_EXCEPT_RUNTIME("Passed a grid that is not extended to the version of extended interpolation.");
		//Do I understand it? probably not.
	};
	if(isModifiedWeights != (modWeights != nullptr))
	{
		throw PGL_EXCEPT_RUNTIME("Selected modified weight interpolator, but does not passed one.");
	};
	if(isModifiedWeights == false)	//We ignore this if we compute the weights directly.
	{
		if(gProperty.rows() != gWeights.rows() ||
		   gProperty.cols() != gWeights.cols()   )
		{
			throw PGL_EXCEPT_InvArg("The grid property and the grid weight disagree in their sizes."
					" The grid property has size " + MASI(gProperty) + ", but the grid weight has size " + MASI(gWeights));
		};
	};

	//Get the canonical marker count
	const Index_t nMarkers = mProperty.size();
		pgl_assert(nMarkers > 0);

	//Get the number of markers from the weight
	const Index_t nMarkersWeights = egd_getNMarkers(mWeights);

	if(nMarkersWeights != nMarkers)
	{
		throw PGL_EXCEPT_InvArg("Inconsistent marker weights."
				" Found " + std::to_string(nMarkersWeights) + ", but expected " + std::to_string(nMarkers));
	};

	//
	//Prepare the modified gridweight if needed.

	//This is the varoiable that will hold the modified grid weights
	egd_gridWeight_t modifiedGWeights;
	if(isModifiedWeights == true)
	{
		if(modWeights->size() != nMarkers)
		{
			throw PGL_EXCEPT_InvArg("Inconsistent marker weight modifications."
					" Found " + std::to_string(modWeights->size()) + ", but expected " + std::to_string(nMarkers));
		}; //End if: check if okay

		//Now we have to resize the temporary grid weight
		modifiedGWeights.resizeLike(gProperty.array());
		modifiedGWeights.setConstant(0.0);
	}; //End: creating a temporary grid weights

	//Get the size of the grid property
	const Index_t gpNX = gProperty.cols();	//Here we operate on the matrix
	const Index_t gpNY = gProperty.rows();
		pgl_assert(gpNX > 1, gpNX == (isModifiedWeights == false ? (gWeights.cols()) : (modifiedGWeights.cols())),
			   gpNY > 1, gpNY == (isModifiedWeights == false ? (gWeights.rows()) : (modifiedGWeights.rows())) );

	//If an index is smaller than this one, we have no problem
	//with neighbours. '-1' is the last valid entry in the matrix.
	//but we want that a right one is existing, so if we are smaller
	//than that there is no problem.
	//This will also interpolate to ghost nodes.
	const Index_t xIdxBound = gpNX - 1;	//this is the last index that exists.
	const Index_t yIdxBound = gpNY - 1;

	//
	//Set the grid property to zero
	gProperty.setZero();


	/*
	 * Now we iterate through the markers.
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load the values we need
		const Numeric_t mVal       = mProperty[m];
			pgl_assert(pgl::isValidFloat(mVal));
		const Numeric_t modMarkVal = (isModifiedWeights == false) ? (NAN) : ((*modWeights)[m]);		//This is teh marker property that _could_ influence the weight.

		//This are the weights that are needed
		const Numeric_t omega_ij   = (isModifiedWeights == false) ? (mWeights[0][0][m]) : (mWeights[0][0][m] * modMarkVal);
		const Numeric_t omega_i1j  = (isModifiedWeights == false) ? (mWeights[1][0][m]) : (mWeights[1][0][m] * modMarkVal);
		const Numeric_t omega_ij1  = (isModifiedWeights == false) ? (mWeights[0][1][m]) : (mWeights[0][1][m] * modMarkVal);
		const Numeric_t omega_i1j1 = (isModifiedWeights == false) ? (mWeights[1][1][m]) : (mWeights[1][1][m] * modMarkVal);
			pgl_assert(isModifiedWeights ? egd_isValidModifiedWeight(omega_ij  ) : egd_isValidWeight(omega_ij  ),
				   isModifiedWeights ? egd_isValidModifiedWeight(omega_i1j ) : egd_isValidWeight(omega_i1j ),
				   isModifiedWeights ? egd_isValidModifiedWeight(omega_ij1 ) : egd_isValidWeight(omega_ij1 ),
				   isModifiedWeights ? egd_isValidModifiedWeight(omega_i1j1) : egd_isValidWeight(omega_i1j1) );

		//Load the two indexes
		const Index_t i = idxI[m];
		const Index_t j = idxJ[m];
			pgl_assert(0 <= i, i < gpNY,
				   0 <= j, j < gpNX );

		//Precompute if we are save
		const bool iIsSave = (isExtended ? true : ((i < yIdxBound) ? true : false) );
		const bool jIsSave = (isExtended ? true : ((j < xIdxBound) ? true : false) );


		/*
		 * Now interpolating the marker values to the grid
		 * Note that if we are in the save range, then also [1][1]
		 * exists and only exists then. note that this concerns are
		 * only needed on regular grids.
		 */
		if(isExtended || (iIsSave && jIsSave))
		{
			/*
			 * We are here, this means all is save and right with the world.
			 */
			pgl_assert((i + 1) < gProperty.rows(),
				   (j + 1) < gProperty.cols() );

			//Update the property, we have to use the raw access,
			//for bypassing the checl of valid index
			gProperty.rawAccess(i    , j    ) += mVal * omega_ij  ;
			gProperty.rawAccess(i + 1, j    ) += mVal * omega_i1j ;
			gProperty.rawAccess(i    , j + 1) += mVal * omega_ij1 ;
			gProperty.rawAccess(i + 1, j + 1) += mVal * omega_i1j1;

			//Test if we have to sum up the additional weight
			if(isModifiedWeights == true)
			{
				pgl_assert((i + 1) < modifiedGWeights.rows(),
					   (j + 1) < modifiedGWeights.cols() );

				modifiedGWeights(i    , j    ) += omega_ij  ;
				modifiedGWeights(i + 1, j    ) += omega_i1j ;
				modifiedGWeights(i    , j + 1) += omega_ij1 ;
				modifiedGWeights(i + 1, j + 1) += omega_i1j1;

					pgl_assert(egd::egd_isValidModifiedWeight(modifiedGWeights(i    , j    ) ),
					           egd::egd_isValidModifiedWeight(modifiedGWeights(i    , j + 1) ),
					           egd::egd_isValidModifiedWeight(modifiedGWeights(i + 1, j    ) ),
					           egd::egd_isValidModifiedWeight(modifiedGWeights(i + 1, j + 1) ) );
			}; //End if: have to recompute the weights

		//End if: all entries exists
		}
		else
		{
			/*
			 * If we are here, then some nodes may not exists and we have to be
			 * caution to which one we write.
			 * The nice thing is, that we know for sure that the [1][1] node
			 * does not exist. Because for its existance the other two have to
			 * exists, so we can just check which one exists.
			 * It is important that depending on the grid, it could be that
			 * only the [0][0] exists and no other. For example in the regular
			 * pressure grid can this happen.
			 */

			//First of all the associated node, always exists
			{
				gProperty.rawAccess(i, j) += mVal * omega_ij;

				if(isModifiedWeights == true)
				{
					modifiedGWeights(i, j) += omega_ij;

					pgl_assert(egd::egd_isValidModifiedWeight(modifiedGWeights(i    , j    ) ));
				}; //End if: modified weights
			}; //End scope: accociated node



			if(iIsSave == true)
			{
				/*
				 * The first index is save. This means that the [1][0] exist
				 * and we can write to it, but beside [0][0] no other
				 * node will exist.
				 */
				pgl_assert((i + 1) <  gProperty.rows(),
					   (j + 1) == gProperty.cols() );	//the +1 is to compensate for zero indexing
				gProperty.rawAccess(i + 1, j) += mVal * omega_i1j;

				if(isModifiedWeights == true)
				{
					pgl_assert(gProperty.rows() == modifiedGWeights.rows(),
						   gProperty.cols() == modifiedGWeights.cols() );

					modifiedGWeights(i + 1, j) += omega_i1j;

					pgl_assert(egd::egd_isValidModifiedWeight(modifiedGWeights(i + 1, j    ) ));
				}; //End if: update grid weights
			}; //End if: i is save

			if(jIsSave == true)
			{
				/*
				 * Now the second index is save, this means that bedide [0][0]
				 * only [0][1] exists.
				 */
				pgl_assert((i + 1) == gProperty.rows(),		//+1 compensating zero indexing.
					   (j + 1) <  gProperty.cols() );

				gProperty.rawAccess(i, j + 1) += mVal * omega_ij1;

				if(isModifiedWeights == true)
				{
					pgl_assert(gProperty.rows() == modifiedGWeights.rows(),
						   gProperty.cols() == modifiedGWeights.cols() );

					modifiedGWeights(i, j + 1) += omega_ij1;

					pgl_assert(egd::egd_isValidModifiedWeight(modifiedGWeights(i    , j + 1) ));
				}; //End if: modifing weights
			}; //End else: j is safe
		}; //End else: one index is not save
	}; //End for(m): going through the markers

	/*
	 * Now we have distributed the marker's property onto the
	 * different locations. Now we have to normalize the grid
	 * property.
	 *
	 * Note that this function assumes that the weights are
	 * already processed, such that only a multiplication is
	 * needed.
	 */

	//Test if the grid weights have to be computed
	if(isModifiedWeights == true)
	{
		egd_processGridWeights(&modifiedGWeights);

		//Test if we have to reapply the disabling
		if(reapplyDisableGhostNode == true)
		{
			::egd::egd_disableGhostNodes(&modifiedGWeights, gProperty.getType() );
		}; //End if: reapply
	}; //End normalize grid weights.


	//
	//Apply the normalization
	//Since we assume that the preprocess process and disable
	//of ghost node functions was called, this will set all
	//entries that coresponds to such nodes to zero, in the grid.
	gProperty.applyNormalization(
			  isModifiedWeights == true
			   ? (modifiedGWeights)
			   : (gWeights)
			 );

	return;
}; //End: internal basic mapper




PGL_NS_END(egd)



