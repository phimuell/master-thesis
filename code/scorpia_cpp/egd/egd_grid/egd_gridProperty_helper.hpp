#pragma once
/**
 * \brief	This file defines several functions that operates on the grid property.
 *
 * This fuunctions are helper for the grid properties. That should not be implemented
 * as memeber functions, but should operate on the property itself.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_gridProperty.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD


PGL_NS_START(egd)


/**
 * \brief	This function sets all nodes to zero that corresponds to
 * 		 a node that is a ghost node.
 *
 * This function is similar to the one egd_disableGhostNodes() function,
 * but operates on grid property objects.
 *
 * \param  gProp	The grid property that should be manipulate.
 *
 * \note 	This function is currently redirected to the ghost node
 * 		 function, but this is an internal detail.
 *
 * \note	This function is a friend of the grid property class.
 */
void
egd_setGhostNodesToZero(
	egd_gridProperty_t* const 	gProp);





PGL_NS_END(egd)






