/**
 * \brief	This file implements the functiosn that performs naive averiging.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include BOOST


//Include STD


PGL_NS_START(egd)


/**
 * \brief	This is teh implementation that maps, basic grid points to other nodes.
 *
 * Note that this function works also on grids that are non constant.
 * This has to do with the special location of the basic nodal grid.
 *
 * \param  destPorp_		The destination.
 * \param  srcProp 		The source.
 * \param  doHarmonic		Should use harmonic.
 */
static
void
egdInternal_naiveInterpolFromBasic(
	egd_gridProperty_t* const 	destProp_,
	const egd_gridProperty_t& 	srcProp,
	const egd_gridGeometry_t& 	gridGeo,
	const bool 			useHarmonic);


/**
 * \brief	This is an interpolation function that operates on VX
 * 		 grids as source grids.
 *
 * This function may not fill all values.
 *
 * \param  destPorp_		The destination.
 * \param  srcProp 		The source.
 * \param  doHarmonic		Should use harmonic.
 */
static
void
egdInternal_naiveInterpolFromVX(
	egd_gridProperty_t* const 	destProp_,
	const egd_gridProperty_t& 	srcProp,
	const egd_gridGeometry_t& 	gridGeo,
	const bool 			useHarmonic);


/**
 * \brief	This is an interpolation function that operates on VY
 * 		 grids as source grids.
 *
 * This function may not fill all values.
 *
 * \param  destPorp_		The destination.
 * \param  srcProp 		The source.
 * \param  doHarmonic		Should use harmonic.
 */
static
void
egdInternal_naiveInterpolFromVY(
	egd_gridProperty_t* const 	destProp_,
	const egd_gridProperty_t& 	srcProp,
	const egd_gridGeometry_t& 	gridGeo,
	const bool 			useHarmonic);



/*
 * ==================================
 */





void
egd_performNaiveTransform(
	egd_gridProperty_t* const 	destProp_,
	const egd_gridProperty_t& 	srcProp,
	const egd_gridGeometry_t& 	gridGeo,
	const bool 			useHarmonic)
{
	if(destProp_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("Passed the nullpointer.");
	};
	egd_gridProperty_t& destProp = *destProp_;

	if(destProp.getPropIdx().getRepresentant() !=
	    srcProp.getPropIdx().getRepresentant()    )
	{
		throw PGL_EXCEPT_LOGIC("You can only interpolate properties that are the same.");
	}; //ENd if: are not the same

	if(destProp.Nx() != srcProp.Nx() ||
	   destProp.Ny() != srcProp.Ny()   )
	{
		throw PGL_EXCEPT_InvArg("The sizes are different.");
	}; //End if different sizes

	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid geometry is not set.");
	};

	if(gridGeo.isConstantGrid() == false)	//A firendly reminder
	{
		throw PGL_EXCEPT_illMethod("Naïve interpolation is only supported for constant grids.");
	};


	const eGridType  srcGType =  srcProp.getType();
	const eGridType destGType = destProp.getType();


	if(mkCase(srcGType) == mkCase(destGType))
	{
		if(isRegularGrid(srcGType) == false || isRegularGrid(destGType) == false)
		{
			throw PGL_EXCEPT_LOGIC("Currently only naive interpolation on regular grids:");
		}; //End if: not on rgular grid

		//They are the same, so only coping
		destProp.maxView() = srcProp.maxView();

		return;
	}; //End iuf: nothing to do


	/*
	 * Now we select from where we start.
	 */
	switch(mkCase(srcGType))
	{
	  case eGridType::BasicNode:
	  	egdInternal_naiveInterpolFromBasic(&destProp, srcProp, gridGeo, useHarmonic);
	  break;

	  case eGridType::StVx:
		egdInternal_naiveInterpolFromVX(&destProp, srcProp, gridGeo, useHarmonic);
	  break;

	  case eGridType::StVy:
	  	egdInternal_naiveInterpolFromVY(&destProp, srcProp, gridGeo, useHarmonic);
	  break;

	  default:
	  	throw PGL_EXCEPT_illMethod("The selected interpolation is not implemented.");
	}; //End switch(rscGType):

	return;
}; //End naive interpol


/*
 * ============================================
 * Worker implementations
 */
void
egdInternal_naiveInterpolFromBasic(
	egd_gridProperty_t* const 	destProp_,
	const egd_gridProperty_t& 	srcProp,
	const egd_gridGeometry_t& 	gridGeo,
	const bool 			useHarmonic)
{
	pgl_assert(destProp_ != nullptr);
	pgl_assert(isBasicNodePoint(srcProp.getType()));
	egd_gridProperty_t& destProp = *destProp_;	//Alias

	pgl_assert(destProp.isFinite(),
		    srcProp.isFinite() );
	pgl_assert(srcProp.isExtendedGrid() == false);

	if(destProp.isRegularGrid() == false)
	{
		throw PGL_EXCEPT_illMethod("This function onyl supports regular target grids.");
	};

	/*
	 * Switch over the grid type of the destrination.
	 */
	switch(mkCase(destProp.getType()))
	{
	  case eGridType::StPressure:
	  case eGridType::CellCenter:
	  {
	  	//This is the length of the window we are used, it is that because
	  	//we know that we are on a basic grid node
	  	const Index_t nWinRows = srcProp.Ny() - 1;
	  	const Index_t nWinCols = srcProp.Nx() - 1;

	  	if(useHarmonic == false)
		{
			/*
		  	 * Normal mean
		  	 */
			destProp.maxView() =
		  	  	  0.25 * (  srcProp.block(0, 0, nWinRows, nWinCols)
		  	  	  	  + srcProp.block(1, 0, nWinRows, nWinCols)
		  	  	  	  + srcProp.block(0, 1, nWinRows, nWinCols)
		  	  	  	  + srcProp.block(1, 1, nWinRows, nWinCols)
		  	  	  	 );
			pgl_assert(destProp.isFinite());
		//End if: normal averagin.
		}
		else
		{
			/*
			 * Harmonic averaging
			 */
			destProp.maxView() =
		  	  	  4.0 / (   srcProp.block(0, 0, nWinRows, nWinCols).cwiseInverse()
		  	  	  	  + srcProp.block(1, 0, nWinRows, nWinCols).cwiseInverse()
		  	  	  	  + srcProp.block(0, 1, nWinRows, nWinCols).cwiseInverse()
		  	  	  	  + srcProp.block(1, 1, nWinRows, nWinCols).cwiseInverse()
		  	  	  	 );
			pgl_assert(destProp.isFinite());
		}; //End else: harmonic averaging
	  }; //End scope: cell centre
	  break;

	  case eGridType::StVx:
	  {
	  	//This is the length of the window we are used, it is that because
	  	//we know that we are on a basic grid node
	  	//This time we have the full width (cols)
	  	const Index_t nWinRows = srcProp.Ny() - 1;
	  	const Index_t nWinCols = srcProp.Nx();

		if(useHarmonic == false)
		{
			/*
			 * Normal averaging
			 */
			destProp.maxView() =
				0.5 * (   srcProp.block(0, 0, nWinRows, nWinCols)
					+ srcProp.block(1, 0, nWinRows, nWinCols)
				      );
		//End if: normal averaging
		}
		else
		{
			/*
			 * Harmonic averaging
			 */
			destProp.maxView() =
				2 /  (  srcProp.block(0, 0, nWinRows, nWinCols).cwiseInverse()
				      + srcProp.block(1, 0, nWinRows, nWinCols).cwiseInverse()
				     );
		}; //End else: harmonic averagin
	  };//End scope: VX
	  break;

	  case eGridType::StVy:
	  {
	  	//This is the length of the window we are used, it is that because
	  	//we know that we are on a basic grid node
	  	//This time we have the full hight (rows)
	  	const Index_t nWinRows = srcProp.Ny();
	  	const Index_t nWinCols = srcProp.Nx() - 1;

		if(useHarmonic == false)
		{
			/*
			 * Normal averaging
			 */
			destProp.maxView() =
				0.5 * (   srcProp.block(0, 0, nWinRows, nWinCols)
					+ srcProp.block(0, 1, nWinRows, nWinCols)
				      );
		//End if: normal averaging
		}
		else
		{
			/*
			 * Harmonic averaging
			 */
			destProp.maxView() =
				2 /  (  srcProp.block(0, 0, nWinRows, nWinCols).cwiseInverse()
				      + srcProp.block(0, 1, nWinRows, nWinCols).cwiseInverse()
				     );
		}; //End else: harmonic averagin

	  }; //End scope: VY
	  break;

	  default:
	  	throw PGL_EXCEPT_InvArg("The interpolation was unknown.");
	}; //End switch(destProp):

	pgl_assert(destProp.isFinite());

	return;

	(void)gridGeo;
}; //End: naive interpol from basic


void
egdInternal_naiveInterpolFromVX(
	egd_gridProperty_t* const 	destProp_,
	const egd_gridProperty_t& 	srcProp,
	const egd_gridGeometry_t& 	gridGeo,
	const bool 			useHarmonic)
{
	pgl_assert(destProp_ != nullptr);
	pgl_assert(isVxPoint(srcProp.getType()));
	egd_gridProperty_t& destProp = *destProp_;	//Alias

	pgl_assert(destProp.isFinite(),
		    srcProp.isFinite() );

	if(useHarmonic != false)
	{
		throw PGL_EXCEPT_illMethod("Harmonic interpolation is not suporrted.");
	};
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid geometrx was not set.");
	};


	switch(mkCase(destProp.getType()))
	{
	  case eGridType::CellCenter:
	  case eGridType::StPressure:
		{
			//The grid geometry is not important.
			destProp.setZero();

			const Index_t nRows = destProp.rows();
			const Index_t Ny    = destProp.Ny();
			const Index_t nCols = destProp.cols();
			const Index_t Nx    = destProp.Nx();
				pgl_assert(nRows > 0, nCols > 0);

			/*
			 * In this loop we handle only the inner nodes
			 * these nodes are guaranteed to exist
			 */
			for(Index_t j = 1; j != Nx; ++j)
			{
				for(Index_t i = 1; i != Ny; ++i)
				{
					const Numeric_t V1 = srcProp(i, j    );		//Load value
					const Numeric_t V2 = srcProp(i, j - 1);
					const Numeric_t newV = 0.5 * (V1 + V2);		//Write new value

					destProp(i, j) = newV;				//Store new value
				}; //End for(i):
			}; //End for(j):

			/*
			 * We only have to handle more, if the destination is extended.
			 * And if the source is also extended.
			 * If not everything was set to zero
			 */
			if(destProp.isExtendedGrid() == true && srcProp.isExtendedGrid() == true)
			{
				//We han now handle the top and bottom row
				//Note that the corner pointrs are not contained here
				{
					const Index_t i_top = 0;
					const Index_t i_bot = nRows - 1;

					for(Index_t j = 1; j != Nx; ++j)
					{
						const Numeric_t V1_t = srcProp(i_top, j    );	//Load
						const Numeric_t V2_t = srcProp(i_top, j - 1);
						const Numeric_t V1_b = srcProp(i_bot, j    );
						const Numeric_t V2_b = srcProp(i_bot, j - 1);

						const Numeric_t newV_t = 0.5 * (V1_t + V2_t);	//Compute
						const Numeric_t newV_b = 0.5 * (V1_b + V2_b);

						destProp(i_top, j) = newV_t;			//Store
						destProp(i_bot, j) = newV_b;
					}; //End for(j):
				}; //End scope: handle top/bot row


				//The new values will be simply copyies from the nerest values
				//they also contain the corner nodes
				{
					const Index_t j_lef_dest = 0;
					const Index_t j_rig_dest = nCols - 1;
					const Index_t j_lef_src  = 0;
					const Index_t j_rig_src  = nCols - 2;	//-1 is a ghost node

					for(Index_t i = 0; i != nRows; ++i)
					{
						const Numeric_t V1_l = srcProp(i, j_lef_src);
						const Numeric_t V1_r = srcProp(i, j_rig_src);

						destProp(i, j_lef_dest) = V1_l;
						destProp(i, j_rig_dest) = V1_r;
					}; //End for(i):
				}; //End scope: handle the sides
			}; //End if: we can handle the boundary
		}; //End: handling cell centre points
	  break;

	  default:
	  	throw PGL_EXCEPT_illMethod("The requested interpolation is not implemented.");
	  break;
	}; //End swotch(destProp):

	pgl_assert(destProp.isFinite());


	return;
}; //End: naive interpol from VX


void
egdInternal_naiveInterpolFromVY(
	egd_gridProperty_t* const 	destProp_,
	const egd_gridProperty_t& 	srcProp,
	const egd_gridGeometry_t& 	gridGeo,
	const bool 			useHarmonic)
{
	pgl_assert(destProp_ != nullptr);
	pgl_assert(isVyPoint(srcProp.getType()));
	egd_gridProperty_t& destProp = *destProp_;	//Alias

	pgl_assert(destProp.isFinite(),
		    srcProp.isFinite() );

	if(useHarmonic != false)
	{
		throw PGL_EXCEPT_illMethod("Harmonic interpolation is not suporrted.");
	};
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid geometrx was not set.");
	};


	switch(mkCase(destProp.getType()))
	{
	  case eGridType::CellCenter:
	  case eGridType::StPressure:
		{
			//The grid geometry is not important.
			destProp.setZero();

			const Index_t nRows = destProp.rows();
			const Index_t Ny    = destProp.Ny();
			const Index_t nCols = destProp.cols();
			const Index_t Nx    = destProp.Nx();
				pgl_assert(nRows > 0, nCols > 0);

			/*
			 * In this loop we handle only the inner nodes
			 * these nodes are guaranteed to exist
			 */
			for(Index_t j = 1; j != Nx; ++j)
			{
				for(Index_t i = 1; i != Ny; ++i)
				{
					const Numeric_t V1 = srcProp(i - 1, j);		//Load value
					const Numeric_t V2 = srcProp(i    , j);
					const Numeric_t newV = 0.5 * (V1 + V2);		//Write new value

					destProp(i, j) = newV;				//Store new value
				}; //End for(i):
			}; //End for(j):

			/*
			 * We only have to handle more, if the destination is extended.
			 * And if the source is also extended.
			 * If not everything was set to zero
			 */
			if(destProp.isExtendedGrid() == true && srcProp.isExtendedGrid() == true)
			{
				//Right and left cols, this does not involves the corner nodes
				{
					const Index_t j_lef = 0;
					const Index_t j_rig = nCols - 1;

					for(Index_t i = 1; i != Ny; ++i)
					{
						const Numeric_t V1_l = srcProp(i    , j_lef);	//Load
						const Numeric_t V2_l = srcProp(i - 1, j_lef);
						const Numeric_t V1_r = srcProp(i    , j_rig);
						const Numeric_t V2_r = srcProp(i - 1, j_rig);

						const Numeric_t newV_l = 0.5 * (V1_l + V2_l);	//Compute
						const Numeric_t newV_r = 0.5 * (V1_r + V2_r);

						destProp(i, j_rig) = newV_r;			//Store
						destProp(i, j_lef) = newV_l;
					}; //End for(j):
				}; //End scope: handle left/right cols


				//The new values will be simply copyies from the nerest values
				//they also contain the corner nodes
				{
					const Index_t i_top_dest = 0;
					const Index_t i_bot_dest = nRows - 1;
					const Index_t i_top_src  = 0;
					const Index_t i_bot_src  = nRows - 2;	//-1 is a ghost node

					for(Index_t j = 0; j != nCols; ++j)
					{
						const Numeric_t V1_t = srcProp(i_top_src, j);
						const Numeric_t V1_b = srcProp(i_bot_src, j);

						destProp(i_top_dest, j) = V1_t;
						destProp(i_bot_dest, j) = V1_b;
					}; //End for(i):
				}; //End scope: handle the sides
			}; //End if: we can handle the boundary
		}; //End: handling cell centre points
	  break;

	  default:
	  	throw PGL_EXCEPT_illMethod("The requested interpolation is not implemented.");
	  break;
	}; //End swotch(destProp):

	pgl_assert(destProp.isFinite());

	return;
}; //End: naive interpolation from VY












PGL_NS_END(egd)


