#pragma once
/**
 * \brief	This file declares a marker collection.
 *
 * A marker collection is a class that contains markers for all the different types.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_util.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>

#include <pgl_linalg/pgl_EigenCore.hpp>			//Include the main LINALG header
#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD
#include <functional>



PGL_NS_START(egd)


/**
 * \brief	This class is a collection of the markers.
 * \class 	egd_markerCollection_t
 *
 * One could implement the markers as an array of structs.
 * However it is better to implement them as an struct of array.
 * Each array contains one properties for all markers.
 * This allows the efficient processing of the marker.
 * The main task of this class is to emulate the convenience that
 * an array of structs would give us, at least partially.
 *
 * The class also supports the iteration over the properties
 * and thus the markers. The iterators are forward iterators.
 * If accessed they will return a pair.
 * First is a constant property index, that is the index of the
 * current property, second is the vector that holds the marker.
 * The order in which the merkers are accessed is unspecific.
 *
 * The type of a marker is stored as a double. This is because
 * of the uniformity of iteration over properties. However if
 * used it is casted as an integer.
 * The collection also supports an index map.
 * This is a map, that maps properties from integers to
 * index vectors. An index vector is an vector of type Index_t.
 * That contains all index of markers that have the key marker type.
 * Note that Eigen technically support slicing, but it is only in the
 * development branch.
 * The index map has to be filled by the initiator.
 *
 * Note that only the represetations can be added.
 *
 */
class egd_markerCollection_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	/**
	 * \brief	This is the class that is used for indexing the properties.
	 * \typedef 	PropIdx
	 *
	 * This is the type that is used to address all the properties inside *this.
	 */
	using PropIdx_t 	= egd_propertyIndex_t;
	using PropList_t 	= ::pgl::pgl_vector_t<PropIdx_t>;	//!< This is a list of properties.


	/**
	 * \brief	This is the type for a marker.
	 * \typedef 	MarkerProperty_t
	 *
	 * This is the type of a single marker property.
	 * It is an Eigen vector. It is only used for
	 * physical properties, such as density.
	 *
	 * It is important to not confuses this type alias.
	 * with the MarkerProp class that is used to index
	 * the different properties.
	 *
	 * Note that due to our design, the marker type,
	 * which should be an integer, is also a double.
	 */
	using MarkerProperty_t 		= egd_markerProperty_t;
	using MarkerProperty_ref	= MarkerProperty_t&;		//!< This is a mutable reference to a merker property.
	using MarkerProperty_cref	= const MarkerProperty_t&;	//!< This is a constant reference to a marker property.


	/**
	 * \brief	This is the type that is used to store the propertires.
	 * \typedef	MarkerMap_t
	 *
	 * This is an associative map.
	 * It is indexed with the property index class provided by EGD.
	 * Using this designe we are able to iterate over all markes
	 * and special subsets, thus we can write code in loop that
	 * operates on markers in an easy way.
	 *
	 * Note that the usage of this type has an interesting consequences.
	 * The type of the mnarker is not in integer, but a double.
	 * I have thought about that and no better solution came to my mind.
	 */
	using MarkerMap_t 	= ::pgl::pgl_hashMap_t<PropIdx_t, MarkerProperty_t>;

	using iterator 		= MarkerMap_t::iterator;			//!< This is the iterator type (forward).
	using const_iterator	= MarkerMap_t::const_iterator;			//!< This is the constant iterator type (forward)

	using IndexVector_t 	= egd_indexVector_t;				//!< This is a vector of index
	using IndexMap_t 	= ::pgl::pgl_hashMap_t<Index_t, IndexVector_t>;	//!< This is a map of index vectors.
	using IdxIterator_t 	= IndexMap_t::const_iterator;			//!< Iterator to iterate over all index vectors.
	using IdxRange_it 	= ::pgl::pgl_rangeBasedLoppAdaptor_t<IdxIterator_t, true>;	//!< Iterator range to do range based loops.


	/**
	 * \brief	This is a property map.
	 * \typedef	PropToGridMap_t
	 *
	 * This class maps properties to grid types.
	 * It is needed for constructing the grid
	 * container.
	 *
	 * Note that this is just a redeclaration from the
	 * grid container. It can not be aliased, because
	 * the gric container can not be included.
	 */
	using PropToGridMap_t   = ::pgl::pgl_hashMap_t<PropIdx_t, eGridType>;


	/*
	 * Test for the types
	 */
	static_assert(std::is_same<MarkerProperty_t::Scalar, ::pgl::Numeric_t>::value, "The marker property must be a pgl::Numeric_t type.");

	/*
	 * =======================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is the building constructor. It will allocate
	 * the memory for the markers. It is guaranteed that
	 * the value of the markers are set to zero.
	 *
	 * This constructor will first call the default
	 * constructor and after wards the initializing function
	 * of *this.
	 *
	 * The number of marker must be bassed to this.
	 *
	 * The Representation function will be called for
	 * each property in the propList.
	 *
	 * \param  nMarkers	This is the number of markers.
	 * \param  propList	This is a list of properties that should be created.
	 */
	egd_markerCollection_t(
		Size_t 			nMarkers,
		const PropList_t&	propList)
	 :
	  egd_markerCollection_t()
	{
		//Allocate all the space.
		this->allocateMarkers(nMarkers, propList);

		//We are done now
	}; //End: building constructor



	/**
	 * \brief	Default constructor.
	 *
	 * The constructor will not allocate any data.
	 * It is provided such that a later binding can
	 * be done.
	 */
	explicit
	egd_markerCollection_t()
	 = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * It is defaulted and marked as explicit.
	 */
	explicit
	egd_markerCollection_t(
		const egd_markerCollection_t&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 * Eigen sould support it.
	 * Marked as noexcept.
	 */
	egd_markerCollection_t(
		egd_markerCollection_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignemnt.
	 *
	 * Is defaulted.
	 */
	egd_markerCollection_t&
	operator= (
		const egd_markerCollection_t&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 * Eigen should define that.
	 */
	egd_markerCollection_t&
	operator= (
		egd_markerCollection_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_markerCollection_t()
	 = default;



	/*
	 * ======================
	 * Properties.
	 *
	 * Here are small functions, that are more about
	 * a property than anything else.
	 */
public:
	/**
	 * \brief	This fucntion returns the number of markers of *this.
	 *
	 * This function quiries the marker type member for its size and returns it.
	 * It returns a Size_t obbecjt.
	 */
	egd_markerCollection_t::Size_t
	nMarkers()
	 const
	 noexcept
	{
		return m_nMarkers;
	}; //End: nMarkers


	/**
	 * \brief	Return the number of different properties *this has.
	 *
	 * Note that tzhe returned number includes, position in x and y dimension
	 * and the type of a marker.
	 */
	uSize_t
	nProperties()
	 const
	 noexcept
	{
		return this->getProperties().size();	//The getProperties() function will do the tests
	}; //End: nProperties


	/**
	 * \brief	This function returns true, if *this has the property prop.
	 */
	bool
	hasProperty(
		const PropIdx_t& 	prop)
	 const
	 noexcept
	{
			pgl_assert(prop.isValid());
		return (this->m_markerMap.count(prop) == 1);
	};


	/**
	 * \brief	Return true if *this has an index map.
	 *
	 * *this has an index map, if it has at least one mapping
	 * aviable. Note that there is no guarantee that all markers
	 * are inside the map.
	 */
	bool
	hasIndexMap()
	 const
	 noexcept
	{
		return (this->m_idxMap.size() > 0);
	};


	/**
	 * \brief	This function returns true if *this has feel velocities.
	 *
	 * This function tests in essence if the container has feel velocity.
	 * It is just nicer, cleaner and a bit faster to use this function.
	 */
	bool
	hasFeelVel()
	 const
	 noexcept
	{
		if((this->m_markerMap.count(PropIdx::FeelVelX()) == 1) &&
		   (this->m_markerMap.count(PropIdx::FeelVelY()) == 1)   )
		{
			return true;
		};
		return false;
	}; //End: has feel velocity



	/*
	 * ======================
	 * Version Controll
	 */
public:
	/**
	 * \brief	This function returns true if *this was modified.
	 *
	 * This function is intended if the markers are reseeded or the
	 * type has changed.
	 * See also the endOfIteration function, to learn how you can change
	 * this state back to unmodified.
	 *
	 * This functionality is not implemented yet. and the function will
	 * allways return false.
	 */
	bool
	isNewVersion()
	 const
	{
		/*
		 * NOTE:
		 * This functionality is not implemented in a great part
		 * EGD, this must be done before.
		 */
		return false;
	}; //ENd: isNewVersion


	/**
	 * \brief	This function is used to inform the collection, that
	 * 		 the iteration has ended.
	 *
	 * It is intended to let the container know that a new iteration
	 * will begin. The intend of this function is to clear the isNewVersion
	 * state, back to normal.
	 *
	 * This function must be called by the driver.
	 */
	void
	endOfIteration()
	{
		//Nothing to do for us.
		return;
	};//End: endOfIteration






	/*
	 * ====================
	 * Accessing Properties
	 */
public:
	/**
	 * \brief	This function returns a reference of all properties that are stored inside *this.
	 *
	 * This functions makes light wight tests, with an assert.
	 */
	const PropList_t&
	getProperties()
	 const
	 noexcept
	{
		pgl_assert(m_propList.size() == m_markerMap.size());
		pgl_assert(::std::all_of(m_propList.cbegin(), m_propList.cend(), [this](const PropIdx_t& i) -> bool
					{return (this->m_markerMap.count(i) == 1);}));

		return m_propList; 	//Returns a reference
	}; //End: getProperties


	/**
	 * \brief	This function returns a default PropToGridtype map.
	 *
	 * This function returns a map, that is accepted by the constructor of the
	 * grid to construct itself. The map consists of all properties that are
	 * registered inside *this, with the exception of the position.
	 *
	 * Note that the grid type is set to the vbalue that is returned by
	 * the default function.
	 *
	 * Note this function is implemented in the constructor file.
	 * All grids are set to the regular grid size.
	 */
	PropToGridMap_t
	getDefaultPropToGridMap()
	 const;


	/**
	 * \brief	This function returns a reference to the marker of the given property.
	 *
	 * An exception is only generated if the property does not exists inside *this.
	 * A check if the idx is valid is only done, by an assert.
	 *
	 * \param  idx		The property index of the property we want to access.
	 *
	 * \throw	If the propery does not exists indie *this.
	 */
	MarkerProperty_ref
	getMarkerProperty(
		const PropIdx_t& 		idx)
	{
		pgl_assert(idx.isValid());	//Check the validy of the index

		//test if the index exists
		const auto e = m_markerMap.find(idx);
		if(e == m_markerMap.end())
		{
			throw PGL_EXCEPT_InvArg("The passed idx \"" + idx.print() + "\" does not exist.");
		};

		//return the index
		return e->second;
	}; //End: getMarkerProperty


	/**
	 * \brief	This function returns a const reference to the marker of the given property.
	 *
	 * An exception is only generated if the property does not exists inside *this.
	 * A check if the idx is valid is only done, by an assert.
	 * This is the constant version.
	 *
	 * \param  idx		The property index of the property we want to access.
	 *
	 * \throw	If the propery does not exists indie *this.
	 */
	MarkerProperty_cref
	getMarkerProperty(
		const PropIdx_t& 		idx)
	 const
	{
		pgl_assert(idx.isValid());	//Check the validy of the index

		//test if the index exists
		const auto e = m_markerMap.find(idx);
		if(e == m_markerMap.end())
		{
			throw PGL_EXCEPT_InvArg("The passed idx \"" + idx.print() + "\" does not exist.");
		};

		//return the index
		return e->second;
	}; //End: getMarkerProperty


	/**
	 * \brief	This function returns a const reference to the marker of the given property.
	 *
	 * An exception is only generated if the property does not exists inside *this.
	 * A check if the idx is valid is only done, by an assert.
	 * This is the explicit constant version.
	 *
	 * \param  idx		The property index of the property we want to access.
	 *
	 * \throw	If the propery does not exists indie *this.
	 */
	MarkerProperty_cref
	cgetMarkerProperty(
		const PropIdx_t& 		idx)
	 const
	{
		pgl_assert(idx.isValid());	//Check the validy of the index

		//test if the index exists
		const auto e = m_markerMap.find(idx);
		if(e == m_markerMap.end())
		{
			throw PGL_EXCEPT_InvArg("The passed idx \"" + idx.print() + "\" does not exist.");
		};

		//return the index
		return e->second;
	}; //End: getMarkerProperty


	/**
	 * \brief	This function returns a reference to
	 * 		 the x position of the markers.
	 */
	MarkerProperty_ref
	getXPos()
	{
		return this->getMarkerProperty(PropIdx_t::PosX());
	};


	/**
	 * \brief	This function returns a constant reference to
	 * 		 the x position of the markers
	 */
	MarkerProperty_cref
	getXPos()
	 const
	{
		return this->cgetMarkerProperty(PropIdx_t::PosX());
	};


	/**
	 * \brief	This function returns a constant reference to
	 * 		 the x position of the markers
	 * 		 This is the explicit version.
	 */
	MarkerProperty_cref
	cgetXPos()
	 const
	{
		return this->cgetMarkerProperty(PropIdx_t::PosX());
	};


	/**
	 * \brief	This function returns a reference to
	 * 		 the y position of the markers.
	 */
	MarkerProperty_ref
	getYPos()
	{
		return this->getMarkerProperty(PropIdx_t::PosY());
	};


	/**
	 * \brief	This function returns a constant reference to
	 * 		 the y position of the markers
	 */
	MarkerProperty_cref
	getYPos()
	 const
	{
		return this->cgetMarkerProperty(PropIdx_t::PosY());
	};


	/**
	 * \brief	This function returns a constant reference to
	 * 		 the y position of the markers
	 * 		 This is the explicit version.
	 */
	MarkerProperty_cref
	cgetYPos()
	 const
	{
		return this->cgetMarkerProperty(PropIdx_t::PosY());
	};


	/*
	 * ========================
	 * Accessing Index
	 */
public:
	/**
	 * \brief	This function returns a reference to the index
	 * 		 vector of type t.
	 *
	 * \param  t 	The type that should be quiried.
	 *
	 * \throw 	If the type does not have a map.
	 */
	const IndexVector_t&
	getIndexes(
		const Index_t 		t)
	 const
	{
		pgl_assert(this->hasIndexMap() );
		return this->m_idxMap.at(t);
	};


	/**
	 * \brief	Explicit constant version of getIndexes().
	 *
	 * \param  t 	The type that should be queried.
	 *
	 * \throw	If the type does not exists
	 */
	const IndexVector_t&
	cgetIndexes(
		const Index_t 		t)
	 const
	{
		pgl_assert(this->hasIndexMap() );
		return this->m_idxMap.at(t);
	};


	/**
	 * \brief	This function returns the first type
	 *
	 * Order of types is unspecific.
	 */
	IdxIterator_t
	indexesBegin()
	 const
	 noexcept
	{
		pgl_assert(this->hasIndexMap() );
		return m_idxMap.cbegin();
	};


	/**
	 * \brief	Return the past the end iterator of the type indexes.
	 *
	 */
	IdxIterator_t
	indexesEnd()
	 const
	 noexcept
	{
		pgl_assert(this->hasIndexMap() );
		return m_idxMap.cend();
	};


	/**
	 * \brief	Returna a proxy object that allows range based for loop iteration
	 * 		 over the index map.
	 *
	 * The iterator is a pair with first beeing the marker type and second being an
	 * index vector with all indexes of markers of that specific type.
	 */
	IdxRange_it
	indexes()
	 const
	 noexcept
	{
		pgl_assert(this->hasIndexMap() );
		return IdxRange_it(m_idxMap.cbegin(), m_idxMap.cend());
	};


	/**
	 * \brief	This function returns a mutable reference to the index map.
	 * 		 Use with care.
	 *
	 * This function is only intendet to set up.
	 * There is also a function to automatically set up everything.
	 *
	 * This function only workls if the map is empty.
	 */
	IndexMap_t&
	getMutableIndexesMap()
	{
		if(m_idxMap.size() > 0)
		{
			throw PGL_EXCEPT_LOGIC("A mutable reference to the index map can only be optained, if the map was empty.");
		};

		return m_idxMap;
	};


	/**
	 * \brief	This function sets the internal map.
	 *
	 * This function will iterate over the type property
	 * and integrate it into *this.
	 *
	 * \throw	If the map contains values.
	 */
	void
	setUpIndexesMap();





	/*
	 * =====================
	 * Verification, testing and consistency cheks.
	 *
	 * These functions performs some basic test to ensure
	 * a consistent object. They varry between their consts
	 * and the degree of inconsistency that can be
	 * detected.
	 */
public:
	/**
	 * \brief	This function performs a simple consistency check.
	 *
	 * It is very basic and does only check if the sizes of the
	 * marker arrays are all the same.
	 *
	 * A false means that the test has failed.
	 *
	 * This function does not throw on its own.
	 */
	bool
	checkConsistency()
	 const
	 noexcept;


	/**
	 * \brief	This function tests if the values are valid.
	 *
	 * This function tests performs a checkConsistency() test.
	 * If passed it will tests of any of the marker properties is
	 * valid.
	 *
	 * This means it will test for NaN and Infinity.
	 * It will also test if they are negative, with the exception
	 * of the temperature. note that value zero is accepted.
	 *
	 * A false means that the test is not passed.
	 * This function does not throw on its own.
	 */
	bool
	checkValues()
	 const
	 noexcept;


	/*
	 * ====================
	 * Iterator functions
	 *
	 * These functions gives access to the stored markers
	 */
public:
	/**
	 * \brief	Returns an iterator to the first property.
	 */
	iterator
	begin()
	 noexcept
	{
		pgl_assert(m_nMarkers > 0, !m_markerMap.empty());	//This is for handling unallocated markers
		return m_markerMap.begin();
	};


	/**
	 * \brief	Returns a constant iterator to the first property.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		pgl_assert(m_nMarkers > 0, !m_markerMap.empty());	//This is for handling unallocated markers
		return m_markerMap.cbegin();
	};


	/**
	 * \brief	Returns a constant iterator to the first property,
	 * 		 Explicit version.
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		pgl_assert(m_nMarkers > 0, !m_markerMap.empty());	//This is for handling unallocated markers
		return m_markerMap.cbegin();
	};


	/**
	 * \brief	Returns the past the end iterator of the property.
	 */
	iterator
	end()
	 noexcept
	{
		pgl_assert(m_nMarkers > 0, !m_markerMap.empty());	//This is for handling unallocated markers
		return m_markerMap.end();
	};


	/**
	 * \brief	Returns the constant past the end iterator of the property.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		pgl_assert(m_nMarkers > 0, !m_markerMap.empty());	//This is for handling unallocated markers
		return m_markerMap.cend();
	};


	/**
	 * \brief	Returns the constant past the end iterator of the property.
	 * 		 Explicit version.
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		pgl_assert(m_nMarkers > 0, !m_markerMap.empty());	//This is for handling unallocated markers
		return m_markerMap.cend();
	};


	/*
	 * =====================
	 * Initalization functions
	 *
	 * Here are all initialization functions located.
	 * Some are private.
	 */
public:
	/**
	 * \brief	This function initalizes *this.
	 *
	 * This function will allocate all the space and set
	 * set all the values to zero, including the the type property.
	 *
	 * The Representation function will be called for
	 * each property in the propList.
	 *
	 * \param  nMarkers	The number of markers that should be allocated.
	 * \param  propList	This is a list of properties that should be created.
	 *
	 * \throw	If *this is already allocated or if nMarkers is invalid.
	 */
	void
	allocateMarkers(
		const uSize_t 		nMarkers,
		const PropList_t&	propList);





	/*
	 * ======================
	 * Private Member
	 */
private:
	Size_t 		m_nMarkers = 0;		//!< The number of markers; We use a dedicated variable, such that we can access it fast.
	MarkerMap_t 	m_markerMap;		//!< This is a map that contains all markers.
	PropList_t 	m_propList;		//!< This is a list of all properties inside "this, no ordering is guaranteed.
						//!<  It could be generated on the fly, but we will cache it.
	IndexMap_t 	m_idxMap;		//!< This is an index map.
}; //End: class(egd_markerCollection_t)





PGL_NS_END(egd)







