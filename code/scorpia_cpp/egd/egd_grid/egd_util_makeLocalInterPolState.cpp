/**
 * \brief	This file contains the function to make the non local interpolation state, local.
 *
 * Note that this function
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl/pgl_math.hpp>

#include <pgl_linalg/pgl_EigenCore.hpp>			//Include the main LINALG header
#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD
#include <limits>



PGL_NS_START(egd)

void
egd_makeLocalInterpolState(
	const egd_markerProperty_t& 		mPosY,
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_localAssocVector_t* const 		locAssoc_,
	const egd_markerWeight_t& 		mWeight,
	egd_gridWeight_t* const 		gWeightL_)
{
	using std::to_string;
	using pgl::isValidFloat;

	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid is not set up.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};
	if(mPosX.size() != mPosY.size())
	{
		throw PGL_EXCEPT_InvArg("Passed invalid marker sizes of len(mPosX) = " + std::to_string(mPosX.size())
				+ ", len(mPosY) = " + std::to_string(mPosY.size()));
	};
	pgl_assert(gWeightL_ != nullptr, 		//Aliasing
		   locAssoc_ != nullptr );
	egd_gridWeight_t&   		gWeightL = *gWeightL_;
	egd_localAssocVector_t& 	locAssoc = *locAssoc_;

	const Index_t nMarkers = mPosX.size();			//Load the number of markers, this is the reference
	const Index_t nMarkers_mW = egd_getNMarkers(mWeight);	//load the number of markers form the weights

	if(nMarkers <= 0)
	{
		throw PGL_EXCEPT_RUNTIME("Not enough markers, got " + to_string(nMarkers));
	};
	if(nMarkers != nMarkers_mW)
	{
		throw PGL_EXCEPT_InvArg("The number of markers in the collection was " + std::to_string(nMarkers) + ", but " + std::to_string(nMarkers_mW) + " many in the weights.");
	};
	if(Index_t(idxI.size()) != nMarkers)
	{
		throw PGL_EXCEPT_InvArg("The idxI vector has the wrong size, expected length " + to_string(nMarkers) + ", but got " + to_string(idxI.size()));
	};
	if(Index_t(idxJ.size()) != nMarkers)
	{
		throw PGL_EXCEPT_InvArg("The idxJ vector has the wrong size, expected length " + to_string(nMarkers) + ", but got " + to_string(idxJ.size()));
	};

	//Resize the local association vector
	locAssoc.assign(nMarkers, uInt8_t(0));	pgl_assert((Index_t)locAssoc.size() == nMarkers);


	/*
	 * Load the grid geometry
	 */
	egd_gridGeometry_t::NodePosition_t x_tmp, y_tmp;
	std::tie(x_tmp, y_tmp) = gridGeo.getGridPoints(gType);
	const egd_gridGeometry_t::NodePosition_t gridX = std::move(x_tmp);
	const egd_gridGeometry_t::NodePosition_t gridY = std::move(y_tmp);

	const Index_t lastNodeX = gridX.size() - 1;	//This is the last grid node in X direction, that can have an associated node.
	const Index_t lastNodeY = gridY.size() - 1;

	/*
	 * Now we load the lositions
	 */
	const egd_markerCollection_t::MarkerProperty_t& markerX = mPosX;	pgl_assert(markerX.size() == nMarkers);
	const egd_markerCollection_t::MarkerProperty_t& markerY = mPosY;	pgl_assert(markerY.size() == nMarkers);

	/*
	 * Set the grid weights to zero
	 */
	gWeightL.setZero(gridY.size(), gridX.size());
		pgl_assert(gWeightL.rows() == gridY.size(),
			   gWeightL.cols() == gridX.size() );


	/*
	 * Now we loop through the markers and checks which one is narer
	 *
	 * TODO: precompute the anser (demarcations line) and then simply compare
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load the association
		const Index_t i = idxI[m];		//Association in Y
		const Index_t j = idxJ[m];		//Association in X

		pgl_assert(0 <= i, i <= lastNodeY);	//<= because of -1 in definition
		pgl_assert(0 <= j, j <= lastNodeX);

		//Thses variables are here for the association in the end
		//0 means association is correct, 1 means the next one is the nearest
		Index_t nearestI = -1, nearestJ = -1;

		/*
		 * Handle Y
		 */
		if(PGL_UNLIKELY(i == lastNodeY))
		{
			//In this case, the association is trivial, because there is
			//no other candidate
			nearestI = 0;
		}
		else
		{
			//In this case we have to giguring out which one is the nearest
			//so we load the two points from the array and calculate the differences

			const Numeric_t y_m   = markerY[m    ];		//load the markler
			const Numeric_t y_gi  =   gridY[i    ];		//This is the position we are associated too
			const Numeric_t y_gip =   gridY[i + 1];		//this is the next node
				pgl_assert(isValidFloat(y_m), isValidFloat(y_gi), isValidFloat(y_gip),
					        y_gi <= y_m ,                           y_m <= y_gip  );

			//Compute the difference between the two
			const Numeric_t diffTo_gi  = y_m   - y_gi;
			const Numeric_t diffTo_gip = y_gip - y_m ;

			if(diffTo_gi < diffTo_gip)
			{
				//the associated node is nearer
				nearestI = 0;
			}
			else
			{
				//The node ahead is nearer
				nearestI = 1;
			};
		} //End if: handle the I case


		/*
		 * Handle X
		 */
		if(PGL_UNLIKELY(j == lastNodeX))
		{
			//There is no other node that could be checked to the test is trivial
			nearestJ = 0;
		}
		else
		{
			//Here we have a node to check so we must compute it

			const Numeric_t x_m   = markerX[m    ];		//Load the marker position
			const Numeric_t x_gi  =   gridX[j    ];		//Load the grid node that we are associated to
			const Numeric_t x_gip =   gridX[j + 1];		//Load the next grid point
				pgl_assert(isValidFloat(x_m), isValidFloat(x_gi), isValidFloat(x_gip),
					        x_gi <= x_m ,                           x_m <= x_gip  );

			//Compute the differences
			const Numeric_t diffTo_gi  = x_m   - x_gi;
			const Numeric_t diffTo_gip = x_gip - x_m ;

			if(diffTo_gi < diffTo_gip)
			{
				//We are nearer to the associated grid point
				nearestJ = 0;
			}
			else
			{
				nearestJ = 1;
			};
		}; //End if: handle the J case
			pgl_assert(nearestI == 0 || nearestI == 1);
			pgl_assert(nearestJ == 0 || nearestJ == 1);


		/*
		 * Now we will update the the markers
		 * notice that here all grid positions exists
		 */

		//This is the only weight that will be considered
		const Numeric_t onlyNonZeroWeight = mWeight[nearestI][nearestJ][m];

		//Store the association into the association vector
		locAssoc[m] = egd_makeLocalAssoc(nearestI, nearestJ);

		//Update the grid weights
			pgl_assert((i + nearestI) < gWeightL.rows());
			pgl_assert((j + nearestJ) < gWeightL.cols());
		gWeightL(i + nearestI, j + nearestJ) += onlyNonZeroWeight;
	}; //Endn for(m):

	return;
}; //End: makeLocalInterpolState


void
egd_makeLocalInterpolState(
	const egd_markerCollection_t& 		mColl,
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_localAssocVector_t* const 		locAssoc,
	const egd_markerWeight_t& 		mWeight,
	egd_gridWeight_t* const 		gWeightL)
{
	egd_makeLocalInterpolState(
		mColl.cgetYPos(), mColl.cgetXPos(),
		gridGeo, gType,
		idxI, idxJ,
		locAssoc, mWeight, gWeightL);
	return;
}; //End: convinent interface

PGL_NS_END(egd)

