/**
 * \brief	This function contains the code for the interpolation when doing local intepolation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridType.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/dynamic_bitset.hpp>


//Include STD


PGL_NS_START(egd)


/*
 * ===========================
 * Forwarddeclaring of internal functions
 */


/**
 * \brief	This is an internal function that is used to perform the interpolation.
 *
 * This function has a template parameter that controlles if modified weights are used
 * or not. This is similar in the spirit as the other interpolation routines.
 *
 * Note this function does not perform consitency tests on its own. It expects that
 * the interface does this, so this function should not be used directly.
 *
 */
template<
	bool 	useModWeights
>
void
egdInternal_basicGridToMarkerLocal(
	egd_gridProperty_t* const 		gProperty_,
	const egd_markerProperty_t& 		mProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_gridWeight_t& 		gWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const egd_localAssocVector_t& 		locAssoc,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				reapplyDisableGhostNode);


/*
 * =======================
 * Actuall Interface
 */


void
egd_mapToGridLocal(
	egd_gridProperty_t* const 		gProperty_,
	const egd_markerProperty_t& 		mProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_gridWeight_t& 		gWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const egd_localAssocVector_t& 		locAssoc,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				reapplyDisableGhostNode)
{
	if(gProperty_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The property argument was the null pointer.");
	};
	egd_gridProperty_t& gProperty = *gProperty_;	//Alias it

	if(gProperty.rows() < 1 ||
	   gProperty.cols() < 1   )
	{
		throw PGL_EXCEPT_InvArg("The passed property is too small " + MASI(gProperty));
	};
	if(mProperty.size() < 1)
	{
		throw PGL_EXCEPT_InvArg("Amount of markers is too small, it was " + std::to_string(mProperty.size()));
	};
	if(::egd::isValidGridType(gProperty.getType()) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid proeprty is on an invalid grid.");
	};

	const Index_t nMarkers = mProperty.size();
	if(nMarkers != egd_getNMarkers(mWeights))
	{
		throw PGL_EXCEPT_InvArg("Wrong number of markler weights.");
	};
	if(idxI.size() != idxJ.size() || idxI.size() != locAssoc.size())
	{
		throw PGL_EXCEPT_InvArg("The association indexes have different length.");
	};
	if(Index_t(idxI.size()) != nMarkers)
	{
		throw PGL_EXCEPT_InvArg("Association has not the correct length.");
	};


	if(modWeights == nullptr)
	{
		pgl_assert(reapplyDisableGhostNode == false);
		egdInternal_basicGridToMarkerLocal<false>(
			&gProperty, mProperty,
			mWeights, gWeights,
			idxI, idxJ, locAssoc,
			modWeights,
			reapplyDisableGhostNode);
	}
	else
	{
		egdInternal_basicGridToMarkerLocal<true>(
			&gProperty, mProperty,
			mWeights, gWeights,
			idxI, idxJ, locAssoc,
			modWeights,
			reapplyDisableGhostNode);
	};

	return;
}; //End: interface to local interpolation



/*
 * =================================
 * Work Implementation
 */
template<
	bool 	useModWeights
>
void
egdInternal_basicGridToMarkerLocal(
	egd_gridProperty_t* const 		gProperty_,
	const egd_markerProperty_t& 		mProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_gridWeight_t& 		gWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const egd_localAssocVector_t& 		locAssoc,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				reapplyDisableGhostNode)
{
	/*
	 * We assume that consistency tests are done in the calling function
	 */
	pgl_assert(gProperty_ != nullptr);	//Alias the gProperty
	egd_gridProperty_t& gProperty = *gProperty_;

	if(useModWeights != (modWeights != nullptr))
	{
		throw PGL_EXCEPT_RUNTIME("Selected modified weight interpolator, but does not passed one.");
	};
	if(useModWeights == false)	//We ignore this if we compute the weights directly.
	{
		if(gProperty.rows() != gWeights.rows() ||
		   gProperty.cols() != gWeights.cols()   )
		{
			throw PGL_EXCEPT_InvArg("The grid property and the grid weight disagree in their sizes."
					" The grid property has size " + MASI(gProperty) + ", but the grid weight has size " + MASI(gWeights));
		};
	};

	//Get the canonical marker count
	const Index_t nMarkers = mProperty.size();
		pgl_assert(nMarkers > 0);

	//Get the number of markers from the weight
	const Index_t nMarkersWeights = egd_getNMarkers(mWeights);

	if(nMarkersWeights != nMarkers)
	{
		throw PGL_EXCEPT_InvArg("Inconsistent marker weights."
				" Found " + std::to_string(nMarkersWeights) + ", but expected " + std::to_string(nMarkers));
	};
	if(nMarkers != Index_t(locAssoc.size()))
	{
		throw PGL_EXCEPT_InvArg("Inconsistent local marker association."
				" Found " + std::to_string(nMarkersWeights) + ", but expected " + std::to_string(nMarkers));
	};

	//
	//Prepare the modified gridweight if needed.

	//This is the varoiable that will hold the modified grid weights
	egd_gridWeight_t modifiedGWeights;
	if(useModWeights == true)
	{
		if(modWeights->size() != nMarkers)
		{
			throw PGL_EXCEPT_InvArg("Inconsistent marker weight modifications."
					" Found " + std::to_string(modWeights->size()) + ", but expected " + std::to_string(nMarkers));
		}; //End if: check if okay

		//Now we have to resize the temporary grid weight
		modifiedGWeights.resizeLike(gProperty.array());
		modifiedGWeights.setConstant(0.0);
	}; //End: creating a temporary grid weights

	//Get the size of the grid property
	const Index_t gpNX = gProperty.cols();	//Here we operate on the matrix
	const Index_t gpNY = gProperty.rows();
		pgl_assert(gpNX > 1, gpNX == (useModWeights == false ? (gWeights.cols()) : (modifiedGWeights.cols())),
			   gpNY > 1, gpNY == (useModWeights == false ? (gWeights.rows()) : (modifiedGWeights.rows())) );

	//
	//Set the grid property to zero
	gProperty.setZero();


	/*
	 * Now we iterate through the markers.
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load the two indexes of the association (global)
		const Index_t iAssoc = idxI[m]; 	pgl_assert(0 <= iAssoc, iAssoc < gpNY);
		const Index_t jAssoc = idxJ[m];		pgl_assert(0 <= jAssoc, jAssoc < gpNX);

		//Load the local modification
		const uInt8_t locAssocModi = locAssoc[m];

		//Unpack the association
		const Index_t iLocModi     = egd_getLocalAssocY(locAssocModi);	pgl_assert(iLocModi == 0 || iLocModi == 1);
		const Index_t jLocModi 	   = egd_getLocalAssocX(locAssocModi);	pgl_assert(jLocModi == 0 || jLocModi == 1);

		//This are the final grid indexing that is needed.
		const Index_t i = iAssoc + iLocModi; 	pgl_assert(0 <= i, i < gpNY);
		const Index_t j = jAssoc + jLocModi;	pgl_assert(0 <= j, j < gpNX);


		//
		//Load the values we need

		//Load the marker property
		const Numeric_t mVal       = mProperty[m]; 			pgl_assert(pgl::isValidFloat(mVal));


		//Load the single marker weight we are neededing
		const Numeric_t omega_ = mWeights[iLocModi][jLocModi][m];	pgl_assert(egd_isValidWeight(omega_));

		//Load the modification weight
		const Numeric_t modMarkVal = (useModWeights == false) ? (NAN) : ((*modWeights)[m]);		//This is teh marker property that _could_ influence the weight.

		//Create the final marker weight that is used
		const Numeric_t omega = (useModWeights == false) ? (omega_) : (omega_ * modMarkVal);
			pgl_assert((!useModWeights) || egd_isValidModifiedWeight(omega));


		/*
		 * Now add the contribution to the grid element
		 * We only have one element that hgas to be used.
		 */

		gProperty.rawAccess(i    , j    ) += mVal * omega;

		//Test if we have to sum up the additional weight
		if(useModWeights == true)
		{
			//This also involves just one position
			modifiedGWeights(i, j) += omega;
				pgl_assert(egd::egd_isValidModifiedWeight(modifiedGWeights(i, j)));
		}; //End if: have to recompute the weights
	}; //End for(m): going through the markers

	/*
	 * Now we have distributed the marker's property onto the
	 * different locations. Now we have to normalize the grid
	 * property.
	 *
	 * Note that this function assumes that the weights are
	 * already processed, such that only a multiplication is
	 * needed.
	 */

	//Test if the grid weights have to be computed
	if(useModWeights == true)
	{
		egd_processGridWeights(&modifiedGWeights);

		//Test if we have to reapply the disabling
		if(reapplyDisableGhostNode == true)
		{
			::egd::egd_disableGhostNodes(&modifiedGWeights, gProperty.getType() );
		}; //End if: reapply
	}; //End normalize grid weights.


	//
	//Apply the normalization
	//Since we assume that the preprocess process and disable
	//of ghost node functions was called, this will set all
	//entries that coresponds to such nodes to zero, in the grid.
	gProperty.applyNormalization(
			  useModWeights == true
			   ? (modifiedGWeights)
			   : (gWeights)
			 );

	return;
};//End: workhosre interpolation









PGL_NS_END(egd)



