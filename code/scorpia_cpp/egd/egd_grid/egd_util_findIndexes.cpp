/**
 * \brief	This file implements the find index routines.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridType.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/dynamic_bitset.hpp>


//Include STD


PGL_NS_START(egd)


//This function performs the operation on a constant grid
//This function does not check if all nodes have associated markers
static
void
egd_findAssociatedNodeIndex_constGrid(
	const egd_markerProperty_t& 		mPosY,	//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridCont,
	const eGridType 			gType,
	egd_indexVector_t* const 		idxI,
	egd_indexVector_t* const 		idxJ);


bool
egd_findAssociatedNodeIndex(
	const egd_markerProperty_t& 		mPosY,	//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridCont,
	const eGridType 			gType,
	egd_indexVector_t* const 		idxI,
	egd_indexVector_t* const 		idxJ)
{
	pgl_assert(idxI != nullptr, idxJ != nullptr);
	pgl_assert(mPosY.size() > 0, mPosX.size() == mPosY.size());
	pgl_assert(gridCont.xNPoints() >= 2, gridCont.yNPoints() >= 2);

	if(gridCont.isGridSet() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid is not set up.");
	};
	if(isExtendedGrid(gType) && isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_InvArg("The extended grid does not exists.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};

	//make the switch
	if(gridCont.isConstantGrid() == true)
	{
		//Call the constant grid
		(void)(egd_findAssociatedNodeIndex_constGrid(mPosY, mPosX, gridCont, gType, idxI, idxJ));
	}
	else
	{
		throw PGL_EXCEPT_illMethod("Currently only constant grids are supported.");
	};

	return egd_noEmptyCell(gridCont, gType, *idxI, *idxJ);
}; //End: find associated index


bool
egd_findAssociatedNodeIndex(
	const egd_markerCollection_t& 		mColl,
	const egd_gridGeometry_t& 		grid,
	const eGridType 			gType,
	egd_indexVector_t* const 		idxI,
	egd_indexVector_t* const 		idxJ)
{
	pgl_assert(idxI != nullptr, idxJ != nullptr);
	return egd_findAssociatedNodeIndex(
		mColl.cgetYPos(), mColl.cgetXPos(),	//get the position
		grid, gType, idxI, idxJ);
}; //End: conveninet interface



/*
 * =======================================
 * Internal helper function
 */
void
egd_findAssociatedNodeIndex_constGrid(
	const egd_markerProperty_t& 		yPos,	//mind the order
	const egd_markerProperty_t& 		xPos,
	const egd_gridGeometry_t& 		gridCont,
	const eGridType 			gType,
	egd_indexVector_t* const 		idxI_,
	egd_indexVector_t* const 		idxJ_)
{
	using IndexVector_t = egd_indexVector_t;

	pgl_assert(idxI_ != nullptr, idxJ_ != nullptr);
	pgl_assert(yPos.size() > 0, xPos.size() == yPos.size());
	pgl_assert(gridCont.xNPoints() >= 2, gridCont.yNPoints() >= 2);

	if(gridCont.isGridSet() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid is not set up.");
	};
	if(gridCont.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("Called the find index functions for constant grids with a non constant grid.");
	};
	if(isExtendedGrid(gType) && isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_InvArg("The extended grid does not exists.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};

	//
	//Load the variables

	IndexVector_t& idxI = *idxI_;	//Make references, such that we can work better
	IndexVector_t& idxJ = *idxJ_;

	//using MarkerPos_cref = egd_markerCollection_t::MarkerProperty_cref;		//Constant reference to marker
	//MarkerPos_cref xPos  = mColl.cgetXPos();
	//MarkerPos_cref yPos  = mColl.cgetYPos();
		pgl_assert(xPos.size() == yPos.size());

	//This is the number of grid points, but it can not be used as bounds
	//he reason is simply, because for example on the regular grid, the
	//Also there is the small divergence if the grid is a cell centre grid.
	//Where we have a larger matrix than grid points.
	const Size_t xNPoints = gridCont.getNGridPointsX(gType);	//Numbers of grid points in x direction
	const Size_t yNPoints = gridCont.getNGridPointsY(gType);

	const uSize_t nMarkers = xPos.size();
	//const uSize_t nMarkers = mColl.nMarkers();	//Load the number of markers
		pgl_assert(nMarkers > 0);

	//
	//Load the properties of the grid, these is not the
	//computational domain, but the grid!
	//The value loaded here is the coordinate of the top left node.
	//Note the special case for the cell centre point.
		Numeric_t tmp_xStart, tmp_yStart;
		std::tie(tmp_xStart, tmp_yStart) = gridCont.getTopNode(gType);
	const Numeric_t xStart = tmp_xStart;
	const Numeric_t xDelta = gridCont.getXSpacing();
	const Numeric_t yStart = tmp_yStart;
	const Numeric_t yDelta = gridCont.getYSpacing();

	//
	//Now we load data about the computational domain.
	//This is different and does not depend on the grid
	//type that is used. These data is needed for the checking.
	const Numeric_t xCompDomStart = gridCont.xDomainStart();
	const Numeric_t xCompDomEnd   = gridCont.xDomainEnd();
	const Numeric_t yCompDomStart = gridCont.yDomainStart();
	const Numeric_t yCompDomEnd   = gridCont.yDomainEnd();

	//
	//Get the bound for the associated nodes.
	//Note that the bounds are including.
	const Index_t xFirstNodeID = 0;
	const Index_t yFirstNodeID = 0;

	/*
	 * Here we use the functions to get the last valid index ids.
	 * Since here we have to account also for the physical structure of the grid.
	 */
	const Index_t xLastNodeID  = gridCont.getLastAssoNodeX(gType);
	const Index_t yLastNodeID  = gridCont.getLastAssoNodeY(gType);


	//
	//Prepare the variables

	if(idxI.size() != nMarkers)	//Resize I
	{
		idxI.resize(nMarkers);
	};
	if(idxJ.size() != nMarkers)	//Resize J
	{
		idxJ.resize(nMarkers);
	};
	pgl_assert(idxI.size() == nMarkers,
		   idxJ.size() == nMarkers );


	//
	//Finding the locations
	using ::pgl::isValidFloat;

	/*
	 * We iterate over all markers and determine the left upper node.
	 * This is the cell we assigne them to.
	 * In the MATLAB code, we had added one to the index, this was
	 * needed because we have an one indexing in MATLAB.
	 * This is not needed in C.
	 *
	 * Also notice that I, the first index, corresponds to Y and
	 * J, the second index, corresponds to X.
	 */
	for(uSize_t m = 0; m != nMarkers; ++m)
	{
		const Numeric_t x_m = xPos[m];		//x position of teh marker
		const Numeric_t y_m = yPos[m];
			pgl_assert(isValidFloat(x_m), isValidFloat(y_m));

		//
		//Test if the marker is outside the domain.
		if(!( (xCompDomStart <= x_m) && (x_m < xCompDomEnd) ))
		{
			throw PGL_EXCEPT_RUNTIME("Found a marker that is oputside the domain in x direction."
					" It's position is " + std::to_string(x_m) + ", the domains extensions are: "
					+ std::to_string(xCompDomStart) + ", " + std::to_string(xCompDomEnd));
		};
		if(!( (yCompDomStart <= y_m) && (y_m < yCompDomEnd) ))
		{
			throw PGL_EXCEPT_RUNTIME("Found a marker that is oputside the domain in y direction."
					" It's position is " + std::to_string(y_m) + ", the domains extensions are: "
					+ std::to_string(yCompDomStart) + ", " + std::to_string(yCompDomEnd));
		};


		const Numeric_t x_m1 = x_m - xStart;	//Adjust the offset
		const Numeric_t y_m1 = y_m - yStart;
			pgl_assert(x_m1 >= 0.0, y_m1 >= 0.0);

		//TODO: Make the / into a *
		const Numeric_t x_mc = x_m1 / xDelta;	//Caluclate the fractional index
		const Numeric_t y_mc = y_m1 / yDelta;

		//To get the index we must remove the fractional part.
		//We can do that by casting it into an ingeger.
		//see "https://en.cppreference.com/w/cpp/language/implicit_conversion#Integral_conversions"

		Size_t i = Size_t(y_mc);	//Starge swapping
		Size_t j = Size_t(x_mc);
			pgl_assert(i < yNPoints);	//This is a general test
			pgl_assert(j < xNPoints);

		/*
		 * Here we test if the associated bounds are correct.
		 */
		if(!((yFirstNodeID <= i) && ((Index_t)i <= yLastNodeID)))
		{
			throw PGL_EXCEPT_RUNTIME("Found a marker that is associated to an invalid node in y direction."
					" It was associated to " + std::to_string(i) + "."
					" The bounds were [" + std::to_string(yFirstNodeID) + ", " + std::to_string(yLastNodeID) + "].");
		};
		if(!((xFirstNodeID <= j) && ((Index_t)j <= xLastNodeID)))
		{
			throw PGL_EXCEPT_RUNTIME("Found a marker that is associated to an invalid node in x direction."
					" It was associated to " + std::to_string(j) + "."
					" The bounds were [" + std::to_string(xFirstNodeID) + ", " + std::to_string(xLastNodeID) + "].");
		};

		idxI[m] = i;		//Save it
		idxJ[m] = j;
	}; //End for(m): going through the marker

	//
	//If we are here, we are finisched
	return;
}; //End: find index on constant grid



bool
egd_noEmptyCell(
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ)
{
	if(::egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};
	if(idxI.size() <= 0 	      ||
	   idxI.size() != idxJ.size()	)
	{
		throw PGL_EXCEPT_InvArg("The index vectors are problematic.");
	};


	/*
	 * Here we use the functions to get the last valid index ids.
	 * Since here we have to account also for the physical structure of the grid.
	 */
	const Index_t xLastNodeID  = gridGeo.getLastAssoNodeX(gType);
	const Index_t yLastNodeID  = gridGeo.getLastAssoNodeY(gType);


	/*
	 * This is a lambda function that implements it
	 */
	auto f = [](const Size_t lvIdx, const egd_indexVector_t& idx) -> bool
	{
		//lvIdx is the last valid index, since 0 is the frist
		//valid index, we have to add one to get the full index
		::boost::dynamic_bitset<> saw(lvIdx + 1);
			pgl_assert(saw.any() == false);

		for(const auto& i : idx)
		{
			pgl_assert(i <  (Index_t)saw.size(),
				   i >= 0          );
			saw.set(i, true);
		}; //End for(i):

		return saw.all();
	}; //End lambda(f):


	/*
	 * Now test
	 */
	if(f(xLastNodeID, idxJ) == false)
	{
		return false;
	};

	return f(yLastNodeID, idxI);
}; //End: find if empty cell


PGL_NS_END(egd)


