#pragma once

/**
 * \brief	This file defines the grid container.
 *
 * A grid containr is a container for the different physical properties
 * on the grid. As such it can be seen as the marker collection but
 * transformed to the grid.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include <egd_util/egd_propertyToGridMap.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <tuple>
#include <memory>


PGL_NS_START(egd)


/**
 * \brief	This macro controles the enforcement of the grid size
 * \define 	EGD_GRIDCONT_ENFORCE_PROP_SIZE
 *
 * Setting this macro to a value different from zero, just defining it is not
 * enough, will instruct the egd_gridContainer_t to enforce that all
 * properties have the same size.
 */
#define EGD_GRIDCONT_ENFORCE_PROP_SIZE 1


/**
 * \brief	This class contains the properties that are defined on the grid.
 * \class 	egd_gridContainer_t
 *
 * It can be seen as the result of an interpolation process, that maps the marker
 * properties to the basic nodal grid. For the second iteration of EGD this class
 * was redesigned. Previously a grid container only stored properties that where
 * expressed at the basic nodal points themself. But this has changed.
 * Since the grid properties can store at which nodes they are stored, this restriction
 * does not make any sense, so it was removed.
 *
 * The domain is such that x points towards the right side and y
 * points downwards (the depth).
 * The first index of a matrix is associated with the y axis and
 * the second index with the x axis.
 * Generally the grid properties that are stored here are interpredted
 * as basic grid points. This class is also able to calculate the difference
 * to the orther four supported grids, but only the basic nodal points
 * are stored.
 *
 * *this also supports iteration over its properties, this excludes the position
 * property. The iterator is a forward iterator. That dereference to a pair.
 * first is the property index of that grid and second is the grid property,
 * that can be modified.
 *
 * Previously the container was created from the marker collections.
 * Since we now need to know also the different grids, this is not possiblle anymore.
 * So a new constructor that operates on a property map, a map that maps properties to
 * grid types, is used.
 *
 * For backwards compability, the grid will also hold a geometry instance.
 *
 * The grid container also stores a instance of the object for setting the boundary
 * condition. Note that this is purly staless object and the conditions has to
 * be passed to it, when they are applied.
 * It is worth noting, that *This may not have such an object.
 *
 *
 *
 * \note 	In previous versions the constructor and allocation functions
 * 		 hadd a different ordering than used normaly (fist x then y).
 * 		 This was changed.
 *
 * \note 	In previous versions this class was also responsible for managing
 * 		 and controlling the grid geometry, meaning the location of the
 * 		 nodes and so on. In the beginning this seamed to be a good idea,
 * 		 but it was latter learned that this was a bad idea. So the
 * 		 functionality of the geometry managment was removed from this
 * 		 class and added to a different one, the so called gridGeometry_t
 * 		 class.
 */
class egd_gridContainer_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;		//!< This is the marker collection.
	using PropIdx_t 		= MarkerCollection_t::PropIdx_t;	//!< This is the idex type for the properties
	using PropList_t 		= ::pgl::pgl_vector_t<PropIdx_t>;	//!< This is a list of properties.

	/**
	 * \brief	This is the grid property
	 * \typedef 	GridProperty_t
	 *
	 * This is the object that stores the actual property.
	 * It is a wrapper around an Eigen type, that behaves roughly
	 * like an array.
	 *
	 * \note	In earlier iterations this was just a typedef, but
	 * 		 is now its own class.
	 */
	using GridProperty_t	= egd_gridProperty_t;

	using GridProperty_ref  = GridProperty_t&;		//!< Reference to the grid property.
	using GridProperty_cref = const GridProperty_t&;	//!< Constant reference to the grid property.

	using GridMap_t 	= ::pgl::pgl_hashMap_t<PropIdx_t, GridProperty_t>;	//!< This is the map that stores the grid property matrices.
	using iterator 		= GridMap_t::iterator;					//!< This is the itertaor for iterating over the properties.
	using const_iterator	= GridMap_t::const_iterator;				//!< This is the constant iterator for iterating over the properties.

	using GridPosition_t 	= egd_Vector_t<Numeric_t>;	//!< This is the type for storing the positions.
	using GridSpacing_t 	= egd_Vector_t<Numeric_t>;	//!< This is the type for expressing many grid spacings.
	using GridPosition_ref 	= GridPosition_t&;		//!< Reference to the grid positions.
	using GridPosition_cref = const GridPosition_t&;	//!< Constant reference to the grid positions.

	using GridType_e 	= GridProperty_t::GridType_t;	//!< This is the type of the grid, that *this encodes.


	/**
	 * \brief	This is a property map.
	 * \typedef	PropToGridMap_t
	 *
	 * This is a class that is a simple map container.
	 * It allows to map a property to a special grid.
	 *
	 * This type was previously a typedef and this class
	 * declared some functions to perform some task.
	 * These functions were removed.
	 */
	using PropToGridMap_t   = egd_propToGridMap_t;


	/**
	 * \brief	This is hr grid geometry.
	 * \typedef	GridGeometry_t
	 *
	 * This is teh geometry of the grid. In previous version
	 * of the code, the grid container was also the grid
	 * geometry, but this was splitted.
	 * For backwards comnpability, the grid contains also
	 * _the_ instance of the grid.
	 */
	using GridGeometry_t 	= egd_gridGeometry_t;


	/**
	 * \brief	This is the boundary applier.
	 *
	 * This is an object that can apply a boundary
	 * to a property. It is an interface that is
	 * stored inside *this.
	 */
	using ApplyBC_t 	= egd_applyBoundary_i;


	/**
	 * \brief	This is a pointer for storring
	 * 	  	 the boundary applier.
	 *
	 * The grid container was designated to store
	 * such an object. This object is passed at
	 * construction to *this and this is the managed
	 * type.
	 */
	using ApplyBC_ptr  	= ApplyBC_t::ApplyBC_ptr;


	/*
	 * ====================
	 * Constructors
	 *
	 * The construction can happens directly, by calling
	 * a building constructor, or later and constzructing
	 * *this by calling the default constructor.
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function allocates the grid porperties if *this.
	 * It also stores the number of basic grid nodes.
	 * This constructo also takes a managed pointer to the
	 * apply bc object.
	 *
	 * \param  Ny 		The number of grid points in y direction (ROWS).
	 * \param  Nx		The number of grid points in x direction (COLS).
	 * \param  gGeometry 	The grid geometry.
	 * \param  applyBC	This is teh applied bc object.
	 * \param  propMap	The map that stores which properties on which grid have to be created.
	 *
	 * \throw 	If incompability arrises.
	 *
	 * \note	This function calls internaly the default constructor
	 * 		 and then the set up function allocateGrids().
	 *
	 * \note	The order of the grid point arguments was swapped,
	 * 		 compared to previous releases.
	 */
	egd_gridContainer_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const GridGeometry_t& 		gGeometry,
		ApplyBC_ptr 			applyBC,
		const PropToGridMap_t& 		propMap);


	/**
	 * \brief	This is the building constructor.
	 *
	 * This function allocates the grid porperties if *this.
	 * It also stores the number of basic grid nodes.
	 * Here *this does not have a apply bc object.
	 *
	 * \param  Ny 		The number of grid points in y direction (ROWS).
	 * \param  Nx		The number of grid points in x direction (COLS).
	 * \param  gGeometry 	The grid geometry.
	 * \param  propMap	The map that stores which properties on which grid have to be created.
	 *
	 * \throw 	If incompability arrises.
	 *
	 * \note	This function calls internaly the default constructor
	 * 		 and then the set up function allocateGrids().
	 *
	 * \note	The order of the grid point arguments was swapped,
	 * 		 compared to previous releases.
	 */
	egd_gridContainer_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const GridGeometry_t& 		gGeometry,
		const PropToGridMap_t& 		propMap);


	/**
	 * \brief	Default constructor.
	 *
	 * Such a grid container must first be allocated,
	 * then the grid must be configured and then the
	 * the container need to be finalized.
	 */
	egd_gridContainer_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_gridContainer_t(
		const egd_gridContainer_t&)
	 = delete;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is deleted.
	 */
	egd_gridContainer_t&
	operator= (
		const egd_gridContainer_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_gridContainer_t(
		egd_gridContainer_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_gridContainer_t&
	operator= (
		egd_gridContainer_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_gridContainer_t()
	 = default;


	/*
	 * =======================
	 * Version controll
	 */
public:
	/**
	 * \brief	This functon returns true if *this
	 * 		 was modified.
	 *
	 * Since the grid container is tightly coupled with
	 * the geometry this function is basically a wrapper
	 * arround the one of the geometry.
	 *
	 * Note since currently nothing is implemented,
	 * this function will generate an error if true
	 * is ever returned.
	 */
	bool
	isNewVersion()
	 const
	{
		if(this->m_gGeo.isNewVersion() == true)
		{
			throw PGL_EXCEPT_illMethod("The geometry has changed but nothing is impleemtended.");
		};

		return false;
	};


	/**
	 * \brief	This function is used to inform the container,
	 * 		 that the current iteration has ended.
	 *
	 * The main idea of this function is to clear the new version
	 * state of *this.
	 * This function will also call the grid geometry function.
	 */
	void
	endOfIteration()
	{
		// DOING OUR STUFF
		//

		// Forward to geometry
		//
		this->m_gGeo.endOfIteration();

		return;
	};



	/*
	 * ==========================
	 * Domain Functions
	 *
	 * These functions operates on the computational domain.
	 * These functions does not operates on the different grids.
	 */
public:
	/**
	 * \brief	Returns the numbers of grid points in x direction.
	 */
	Size_t
	basicNodalPointsX()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		return m_xPoints;
	};


	/**
	 * \brief	Returns the numbers of grid points in y direction.
	 */
	Size_t
	basicNodalPointsY()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		return m_yPoints;
	};


	/**
	 * \brief	Alias for basicNodalPointsY()
	 */
	Size_t
	BasicNodalPointsY()
	 const
	 noexcept
	{
		return this->basicNodalPointsY();
	};



	/**
	 * \brief	This function returns the grid geometry.
	 */
	const GridGeometry_t&
	getGeometry()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation
		pgl_assert(m_gGeo.isGridSet());

		return m_gGeo;
	};



	/*
	 * ===================
	 * Query Functions
	 */
public:
	/**
	 * \brief	Returns the number of properties inside *this.
	 *
	 * Note that the position property is not included here.
	 */
	uSize_t
	nProperties()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		return m_grids.size();
	};


	/**
	 * \brief	This function returns the type of the requested property.
	 *
	 * This function calls the grid type function of the passed property.
	 * It will also test if the property exists and generate an error if not.
	 *
	 * \param  propIdx		The property that is requested.
	 *
	 * \note	In previous iteration the grid container only stored
	 * 		 one type of grid, but with time this changed.
	 * 		 Back then, this function did not taked an argument.
	 */
	GridType_e
	getGridType(
		const PropIdx_t 	propIdx)
	 const
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		if(this->hasProperty(propIdx) == false)
		{
			throw PGL_EXCEPT_InvArg("The grid container does not have the requested property \"" + propIdx.print() + "\".");
		};

		return m_grids.at(propIdx).getType();
	};

	/*
	 * ===========================
	 * Apply BC functions.
	 *
	 * These functions allows to interact with the
	 * applyBC object that may be storred inside *this.
	 */
public:
	/**
	 * \brief	Returns true if *this has an applyBC object.
	 */
	bool
	hasApplyBC()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		return (m_applyBC == nullptr)
			? false
			: true;
	}; //End: hasBC


	/**
	 * \brief	This function returns a reference to BC object.
	 *
	 * If *this does not have a apply BC object, than an exception
	 * is generated.
	 * Also note that the returned reference is mutable.
	 */
	ApplyBC_t&
	getApplyBC()
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		if(this->hasApplyBC() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The grid does not have an apply BC object.");
		};

		return (*m_applyBC);
	}; //End: getBC


	/**
	 * \brief	This function returns a constant reference to the BC object.
	 *
	 * This function throws if *this does not have one.
	 */
	const ApplyBC_t&
	getApplyBC()
	 const
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		if(this->hasApplyBC() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The grid does not have an apply BC object.");
		};

		return (*m_applyBC);
	}; //End: get apply bc (const)


	/**
	 * \brief	This function returns a constant reference to the BC object.
	 *
	 * This function throws if *this does not have one.
	 * This is the explicit function.
	 */
	const ApplyBC_t&
	cgetApplyBC()
	 const
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		if(this->hasApplyBC() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The grid does not have an apply BC object.");
		};

		return (*m_applyBC);
	};//End: get apply BC (explicit const)


	/**
	 * \brief	This function allows to load a applyBC object
	 * 		 after the creation of this.
	 *
	 * Note that this function is not so nice, and the constructor
	 * should be used to equip a grid container with a BC.
	 *
	 * It is only possible to load it, so no overwritting is possible.
	 *
	 * \param  applyBC	The object to load
	 */
	void
	loadApplyBC(
		ApplyBC_ptr 	applyBC)	//Has to be moved
	{
		if(this->hasApplyBC() == true)
		{
			throw PGL_EXCEPT_LOGIC("This allready has a boundary object.");
		};
		if(applyBC == nullptr)
		{
			throw PGL_EXCEPT_InvArg("Passed the nullpointer to it.");
		};

		//Load the bc object
		m_applyBC = std::move(applyBC);

		return;
	}; //End: loadApplyBC




	/*
	 * =====================
	 * Property functions
	 *
	 * These functions operates on properties.
	 * This encludes the position.
	 */
public:
	/**
	 * \brief	Returns a list of all properties of *this,
	 * 		 without the positions.
	 *
	 * It is important that the list does not conatins the
	 * position porperties.
	 *
	 * This function does not requier *this to be set up.
	 */
	const PropList_t&
	getPropList()
	 const
	 noexcept
	{

		return m_propList;
	};


	/**
	 * \brief	This function returns true if the property
	 * 		 prop is managed by this.
	 *
	 * Note that this function also returns true in the case of
	 * positions, but they are not inside the property.
	 * Note that it is not checked if prop is valid.
	 *
	 * \param  prop		The property to search for.
	 */
	bool
	hasProperty(
		const PropIdx_t& 	prop)
	 const
	 noexcept
	{
		pgl_assert(prop.isValid());
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		//Handle the position spüecial case
		if(prop.isPosition() == true)
		{
			return true;
		};

		//Handle the other case
		return (m_grids.count(prop) == 1);
	}; //End: hasProperty


	/**
	 * \brief	This function returns a reference to the grid
	 * 		 property denoted by prop.
	 *
	 * This function can not access the position property.
	 * Depending on the configuration the class enforces the size
	 * of the properties.
	 *
	 * \param  prop		The property that should be accessed.
	 *
	 * \throw 	If the propery is not present, or prop is invalid.
	 */
	GridProperty_ref
	getProperty(
		const PropIdx_t&	prop)
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		if(prop.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
		};

		if(prop.isPosition() == true)
		{
			throw PGL_EXCEPT_InvArg("Passed a position property to the grid map.");
		};

		if(m_xPoints <= 1 || m_yPoints <= 1)
		{
			throw PGL_EXCEPT_LOGIC("The grid conatiner was not yet initalized.");
		};

		//Search for the property
		const auto it = m_grids.find(prop);

		//Test if we have found it
		if(m_grids.end() == it)
		{
			//Not found
			throw PGL_EXCEPT_InvArg("The property " + prop.print() + " is not inside the grid map.");
		};

#if defined(EGD_GRIDCONT_ENFORCE_PROP_SIZE) && EGD_GRIDCONT_ENFORCE_PROP_SIZE != 0
		if(it->second.Ny() != m_yPoints || it->second.Nx() != m_xPoints)
		{
			throw PGL_EXCEPT_RUNTIME("The grid property " + prop.print() + " has the wrong size."
					" It should have " + std::to_string(m_yPoints) + "x" + std::to_string(m_xPoints) +
					" but it has instead " + std::to_string(it->second.Ny()) + "x" + std::to_string(it->second.Nx())
					);
		}
		if(it->second.checkSize() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The grid property " + prop.print() + " has a size error.");
		};
#endif

		return it->second;
	}; //End: getProperty


	/**
	 * \brief	This function returns a constant reference
	 * 		 to the grid property denoted by prop.
	 * 		 This is the explicit version
	 *
	 * This function can not access the position property.
	 * Depending on the configuration the class enforces the size
	 * of the properties.
	 *
	 * \param  prop		The property that should be accessed.
	 *
	 * \throw 	If the propery is not present, or prop is invalid.
	 */
	GridProperty_cref
	cgetProperty(
		const PropIdx_t&	prop)
	 const
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		if(prop.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
		};

		if(prop.isPosition() == true)
		{
			throw PGL_EXCEPT_InvArg("Passed a position property to the grid map.");
		};

		if(m_xPoints <= 1 || m_yPoints <= 1)
		{
			throw PGL_EXCEPT_LOGIC("The grid conatiner was not yet initalized.");
		};

		//Search for the property
		const auto it = m_grids.find(prop);

		//Test if we have found it
		if(m_grids.end() == it)
		{
			//Not found
			throw PGL_EXCEPT_InvArg("The property " + prop.print() + " is not inside the grid map.");
		};

#if defined(EGD_GRIDCONT_ENFORCE_PROP_SIZE) && EGD_GRIDCONT_ENFORCE_PROP_SIZE != 0
		if(it->second.Ny() != m_yPoints || it->second.Nx() != m_xPoints)
		{
			throw PGL_EXCEPT_RUNTIME("The grid property " + prop.print() + " has the wrong size."
					" It should have " + std::to_string(m_yPoints) + "x" + std::to_string(m_xPoints) +
					" but it has instead " + std::to_string(it->second.Ny()) + "x" + std::to_string(it->second.Nx())
					);
		}
		if(it->second.checkSize() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The grid property " + prop.print() + " has a size error.");
		};
#endif

		return it->second;
	}; //End: cgetProperty


	/**
	 * \brief	This function returns a constant reference
	 * 		 to the grid property denoted by prop.
	 *
	 * This function can not access the position property.
	 * Depending on the configuration the class enforces the size
	 * of the properties.
	 *
	 * \param  prop		The property that should be accessed.
	 *
	 * \throw 	If the propery is not present, or prop is invalid.
	 */
	GridProperty_cref
	getProperty(
		const PropIdx_t&	prop)
	 const
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation

		return this->cgetProperty(prop);
	}; //End: getProperty




	/*
	 * ==========================
	 * Iterator functions
	 *
	 * These are the iterator functions.
	 * It allows iteration over the properties.
	 * Note that the position is not included.
	 */
public:
	/**
	 * \brief	Return an iterator to the begining of the property range.
	 */
	iterator
	begin()
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation
		return m_grids.begin();
	};


	/**
	 * \brief	Return a constant iterator to the begining of the property range.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation
		return m_grids.cbegin();
	};


	/**
	 * \brief	Return a constant iterator to the begining of the property range.
	 * 		 Explicit version.
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation
		return m_grids.cbegin();
	};


	/**
	 * \brief	Returns the past the end iterator of the property range.
	 */
	iterator
	end()
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation
		return m_grids.end();
	};


	/**
	 * \brief	Returns the constant past the end iterator of the property range.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation
		return m_grids.cend();
	};


	/**
	 * \brief	Returns the constant past the end iterator of the property range.
	 * 		 Explicit version.
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints > 1, m_yPoints > 1, !m_grids.empty());	//test for allocation
		return m_grids.cend();
	};


	/*
	 * =============================
	 * Binding functions
	 *
	 * This functions finalize and allocate *this.
	 */
public:
	/**
	 * \brief	This function allocate all the grid.
	 *
	 * This function sets the internal space up for this.
	 * This function takes the map that is passed to this
	 * function and creates the properties that are listed there.
	 *
	 * \param  Ny 		The number of grid points in y direction (ROWS).
	 * \param  Nx		The number of grid points in x direction (COLS).
	 * \param  gridGeo	The grid geometry.
	 * \param  propMap	The map that describes which and how the properties should be created.
	 *
	 * \throw 	If incompability arrises.
	 *
	 * \note	The ordering of the number of grid points was changed, such that they
	 * 		 match the default, that is used.
	 * \note	Previously this function operated on a marker collection, but this
	 * 		 was changed. Now it operates on a map, that maps properties to grid types.
	 */
	void
	allocateGrids(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const GridGeometry_t& 		gridGeo,
		const PropToGridMap_t& 		propMap);


	/*
	 * ===================
	 * Private memeber
	 */
private:
	Size_t 			m_xPoints = 0;		//!< Numbers of basic grid points in the x direction (COLS).
	Size_t 			m_yPoints = 0;		//!< Numbers of basic grid points in the y direction (ROWS).

	PropList_t 		m_propList;		//!< A list of all properties stored inside *this, except of the positions.
	GridMap_t 		m_grids;		//!< This variable stores all the grids (indexed by the propery index).

	GridGeometry_t 		m_gGeo;			//!< This is the grid geometry.

	ApplyBC_ptr 		m_applyBC = nullptr;	//!< This is the pointer that holds the object for appling the boundaries.
							//!<  It is optional that a grid container holds such a thing.
}; //End: class(egd_gridContainer_t)

PGL_NS_END(egd)

