#pragma once
/**
 * \brief	This file declares the grid geometry class.
 *
 * This is a class that manages the geometry of the grid.
 * It stores the positions of the grid nodes and allows to
 * access the different grids.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)


/**
 * \brief	This class manages the grid geometry.
 * \class 	egd_gridGeometry_t
 *
 * This class stores the geometry of the grid. This involves the
 * geometrical properties computational domain and the grid points.
 * This class manages all these and allows to access it.
 *
 * This class stores the location of the basic nodal points. All
 * other grids are then derived from them.
 *
 * See also the explanation of the eGridType enum in the "egd_grid/egd_gridType.hpp"
 * file, about which grids are handled specially.
 * This class will respect them.
 *
 * In previous versions this class was also responsible for managing and controlling
 * the grid geometry, meaning the location of the nodes and so on. In the beginning
 * this seamed to be a good idea, but it was latter learned that this was a bad idea.
 * So the functionality of the geometry managment was removed from this class and added
 * to a different one, the so called gridGeometry_t class.
 *
 * The work flow with this class is.
 * - constructing a grid geometry.
 * - either with the building constructor or the default constructor.
 * - If constructed with the default constructor the allocateGrids() function must be called.
 *    This will allocate all grids, if the building constructor is used, it was called by it.
 * - The grid point must be set up, so the two vectors that can be accessed, have to be set up.
 *    Alternatively a constant grid can be created, see makeConstantGrid() function.
 * - After this is done finalizeGrids() must be called.
 *
 */
class egd_gridGeometry_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used


	using NodePosition_t 	= egd_Vector_t<Numeric_t>;	//!< This is the type for storing the positions.
	using GridSpacing_t 	= egd_Vector_t<Numeric_t>;	//!< This is the type for expressing many grid spacings.
	using NodePosition_ref 	= NodePosition_t&;		//!< Reference to the grid positions.
	using NodePosition_cref = const NodePosition_t&;	//!< Constant reference to the grid positions.

	using GridType_t 	= eGridType;			//!< This is the type of the grid, that *this encodes.


	/*
	 * ====================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function will allocate the internal structure of the grid.
	 * But it will not fill them with values, thus *this is not set
	 * after this function returns.
	 *
	 * This function is implemented by calling the allocate function.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 *
	 * \note	Compared to the grid container, the order of the
	 * 		 grid points here is different. First comes the
	 * 		 number in y direction and then number in x
	 * 		 direction.
	 *
	 * \throw	If incompabilities arrise.
	 */
	egd_gridGeometry_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx);


	/**
	 * \brief	Default constructor.
	 *
	 * Note this function constructs an invalid and unusable
	 * object. Before usage the allocation function has to be
	 * called and the grid must be buildied.
	 *
	 * It is provided for steperating instatiation and construction.
	 */
	egd_gridGeometry_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_gridGeometry_t(
		const egd_gridGeometry_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_gridGeometry_t&
	operator= (
		const egd_gridGeometry_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_gridGeometry_t(
		egd_gridGeometry_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_gridGeometry_t&
	operator= (
		egd_gridGeometry_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_gridGeometry_t()
	 = default;

	/*
	 * ======================
	 * Version Controll
	 */
public:
	/**
	 * \brief	This function returns true if *this was modified.
	 *
	 * This means that a remeshing has been performed and all grid
	 * nodes nodes has been changed and must be redone.
	 *
	 * See the endOfIteration() function to see how to clear this stage.
	 *
	 * Also note that this functionality is most likely handled through
	 * the grid container object.
	 *
	 * This functionality is not implemented yet. and the function will
	 * allways return false.
	 */
	bool
	isNewVersion()
	 const
	{
		/*
		 * NOTE:
		 * This functionality is not implemented in a great part
		 * EGD, this must be done before.
		 */
		return false;
	}; //ENd: isNewVersion


	/**
	 * \brief	This function is used to inform the geometry,
	 * 		 that the end of the current iteration is reached.
	 *
	 * This function is intended to clear the new version state.
	 * This function is called by the driver code.
	 */
	void
	endOfIteration()
	{
		//Nothing to do for us.
		return;
	};//End: endOfIteration



	/*
	 * ==========================
	 * Computational Domain & Basic Grid
	 *
	 * These functions operates on the computational domain
	 * and the basic nodal grid, it is not possible to access
	 * the properties of the orther grids by this function.
	 *
	 * If you which to work with functions that allows to access
	 * and wotrk with the different grid, see bellow for the
	 * positional functions.
	 *
	 * Note this fucntion requeres that the grid was set up,
	 * but not enforece this. So depending on the function
	 * the returned result may be wrong.
	 */
public:
	/**
	 * \brief	Returns the numbers of grid points in x direction.
	 *
	 * The number relates to the basic nodal grid.
	 */
	Size_t
	xNPoints()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		return m_xPoints;
	};


	/**
	 * \brief	Returns the numbers of grid points in y direction.
	 *
	 * The number relates to the basic nodal grid.
	 */
	Size_t
	yNPoints()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		return m_yPoints;
	};




	/**
	 * \brief	Returns the number of grid cells inside *this.
	 *
	 * Note that this function will use xNPoints() and yNPoints()
	 * to compute it. This function also operates on the basic
	 * nodal grid only.
	 */
	Size_t
	nCells()
	 const
	 noexcept
	{
		const auto yPoints = this->yNPoints();	//Get the numbers of points
		const auto xPoints = this->xNPoints();

		pgl_assert(yPoints == m_yGrid.size(), yPoints >= 2);
		pgl_assert(xPoints == m_xGrid.size(), xPoints >= 2);

		return Size_t((yPoints - 1) * (xPoints - 1));
	};


	/**
	 * \brief	This function returns the start point of the
	 * 		 computational domain in x direction.
	 *
	 * To get the location of the left top node, the one
	 * that is designated (0, 0), see the getTopNode() function.
	 */
	Numeric_t
	xDomainStart()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		return m_xGrid[0];
	};


	/**
	 * \brief	This returns the x coordinate of end of the
	 * 		 computational domnain.
	 */
	Numeric_t
	xDomainEnd()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		return m_xGrid[m_xPoints - 1];
	};


	/**
	 * \brief	This function returns the length in x
	 * 		 of the computational domain.
	 *
	 * In essence this function compute xDomainEnde() - xDomainStart().
	 */
	Numeric_t
	xDomainLength()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		const Numeric_t xEnd    = m_xGrid[m_xPoints - 1];
		const Numeric_t xStart  = m_xGrid[0];
		const Numeric_t xLength = xEnd - xStart;
			pgl_assert(pgl::isValidFloat(xLength), xLength > 0.0);

		return xLength;
	};



	/**
	 * \brief	This function returns the start point of the
	 * 		 computational domain in y direction.
	 *
	 * To get the location of the left top node, the one
	 * that is designated (0, 0), see the getTopNode() function.
	 */
	Numeric_t
	yDomainStart()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		return m_yGrid[0];
	};


	/**
	 * \brief	This function returns the end point of the
	 * 		 computational domain in y direction.
	 */
	Numeric_t
	yDomainEnd()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		return m_yGrid[m_yPoints - 1];
	};


	/**
	 * \brief	This function returns the length of the
	 * 		 computational domain in y direction.
	 */
	Numeric_t
	yDomainLength()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		const Numeric_t yEnd    = m_yGrid[m_yPoints - 1];
		const Numeric_t yStart  = m_yGrid[0];
		const Numeric_t yLength = yEnd - yStart;
			pgl_assert(pgl::isValidFloat(yLength), yLength > 0.0);

		return yLength;
	};


	/**
	 * \brief	This function returns the start (frist) and the
	 * 		 length of a block view in X direction.
	 *
	 * Second index.
	 *
	 * \param  gType	The grid type.
	 */
	std::pair<Index_t, Index_t>
	getBlockSizeX(
		const eGridType 	gType)
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		return getBlockSizeX(gType, xNodeIdx_t(m_xPoints));
	};


	/**
	 * \brief	This function returns the start (frist) and the
	 * 		 length of a block view in Y direction.
	 *
	 * First index.
	 *
	 * \param  gType	The grid type.
	 */
	std::pair<Index_t, Index_t>
	getBlockSizeY(
		const eGridType 	gType)
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);

		return getBlockSizeY(gType, yNodeIdx_t(m_yPoints));
	};



	/*
	 * ===================
	 * Query Functions
	 */
public:
	/**
	 * \brief	This function returns true if the grid points were set.
	 *
	 * It basically means that *This is fully set up.
	 */
	bool
	isGridSet()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(),		//Has to hold no matter what
			   pgl_implies(m_isGridSet, m_yPoints > 1));
		pgl_assert(m_xPoints == m_xGrid.size(),		//Has to hold no matter what
			   pgl_implies(m_isGridSet, m_xPoints > 1));

		return m_isGridSet;
	};


	/**
	 * \brief	This function returns true if *this has a constant grid.
	 *
	 * This function does performes also some consistency check in the debug mode.
	 * To optain the spacing use getDSpacing, where D is {X, Y}.
	 */
	bool
	isConstantGrid()
	 const
	 noexcept
	{
		//It only makes sense to call this function if *this is set, which is indirectly tested.
		pgl_assert(pgl::isValidFloat(m_xSpacing) == m_isConstGrid,
		  	   m_xPoints == m_xGrid.size(), m_xPoints > 1);
		pgl_assert(pgl::isValidFloat(m_ySpacing) == m_isConstGrid,
			   m_yPoints == m_yGrid.size(), m_yPoints > 1);

		return m_isConstGrid;
	};




	/*
	 * ======================
	 * Position functions
	 *
	 * These functions deals with the different grids.
	 * They allow accessing them.
	 *
	 * Note that only the basic nodal grid is stored.
	 * Other grids are build upon request.
	 *
	 * All these function requeres that *This was set
	 * up. This is checked by an assert.
	 */
public:
	/**
	 * \brief	Returns a referecne to the x coordinates
	 * 		 of the basic grid points.
	 *
	 * This function returns the positions of the basic nodal grid.
	 * This means they are Nx many points in that vector.
	 *
	 * This function returns a constant reference, since we consider them
	 * as non modifiable. This function requieres that the grid points
	 * were set. This is only checked with an assert.
	 */
	NodePosition_cref
	getXBasicPos()
	 const
	 noexcept
	{
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);
		pgl_assert(m_isGridSet == true);
		return m_xGrid;
	};


	/**
	 * \brief	Returns a reference to the y coordinates
	 * 		 of the basic grid poinst.
	 *
	 * This function returns the y coordinates of the basic nodal grid.
	 * This means that they are Ny many points.
	 *
	 * This function returns a constant reference, since we consider them
	 * as non modifiable. This function requieres that the grid points
	 * were set. This is only checked with an assert.
	 */
	NodePosition_cref
	getYBasicPos()
	 const
	 noexcept
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_isGridSet == true);
		return m_yGrid;
	};


	/**
	 * \brief	This function returns the spacing in the x direction,
	 * 		 if *this is constant.
	 *
	 * This function only operates on constant *this. It is enforced that
	 * *this is constant
	 *
	 * Note in the case of a constant grid the spacing is the same on all
	 * grids, so in a constant setting, this is the spacing on all grids.
	 *
	 * \throw	If not constant.
	 */
	Numeric_t
	getXSpacing()
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		if(this->isConstantGrid() == false)	//The quiery functions does some safty checks
		{
			throw PGL_EXCEPT_LOGIC("Only regular grids have a spacing.");
		};
		pgl_assert(::pgl::isValidFloat(m_xSpacing), m_xSpacing >= 0.0);

		return m_xSpacing;
	}; //End: getXSpacing


	/**
	 * \brief	This function returns the spacing in the y direction,
	 * 		 if *this is constant.
	 *
	 * See description of the getXSpacing() function for more information.
	 *
	 * \throw	If not constant.
	 */
	Numeric_t
	getYSpacing()
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		if(this->isConstantGrid() == false)	//The quiery functions does some safty checks
		{
			throw PGL_EXCEPT_LOGIC("Only regular grids have a spacing.");
		};
		pgl_assert(::pgl::isValidFloat(m_ySpacing), m_ySpacing >= 0.0);

		return m_ySpacing;
	}; //End: getXSpacing


	/**
	 * \brief	This function generates the different grids.
	 *
	 * This function is the work horse and it computes the
	 * locations of the different grid, starting from the
	 * basic nodal points. The grid must be specified by
	 * a grid type enum.
	 * Also note that the ghost nodes are also returned.
	 *
	 * Both the x and y argument is returned, in that order.
	 * This is different from the usual indexing used here
	 * where first y is specified and then x.
	 *
	 * \param  gType	Which grid you want.
	 */
	std::pair<NodePosition_t, NodePosition_t>
	getGridPoints(
		const eGridType 	gType)
	 const;


	/**
	 * \brief	This function returns the number of grid points
	 * 		 on the given grid, in x direction.
	 *
	 * This function calls the static function with the
	 * appropriate arguments.
	 *
	 * \param  gType	The grid that is considered.
	 */
	Index_t
	getNGridPointsX(
		const eGridType 	gType)
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);
		pgl_assert(isValidGridType(gType));

		return getNGridPointsX(xNodeIdx_t(m_xPoints), gType);
	}; //End:


	/**
	 * \brief	This function returns the number of grid points
	 * 		 on the given grid, in y direction.
	 *
	 * This functions calls the static version of the function
	 * with the appropriate arguments.
	 */
	Index_t
	getNGridPointsY(
		const eGridType 	gType)
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);
		pgl_assert(isValidGridType(gType));

		return getNGridPointsY(yNodeIdx_t(m_yPoints), gType);
	}; //End:


	/**
	 * \brief	This function returns the index of the last grid
	 * 		 node a marker can associated to, in x direction.
	 *
	 * This function calls the static version of this functin.
	 * With the right arguments.
	 *
	 * \param  gType 	The grid type that is under considereation.
	 */
	Index_t
	getLastAssoNodeX(
		const eGridType 	gType)
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);
		pgl_assert(isValidGridType(gType));

		return getLastAssoNodeX(xNodeIdx_t(m_xPoints), gType);
	};




	/**
	 * \brief	This function returns the index of the last grid
	 * 		 node a marker can associated to, in y direction.
	 *
	 * This function calls the static version of this functin.
	 * With the right arguments.
	 *
	 * \param  gType 	The grid type that is under considereation.
	 */
	Index_t
	getLastAssoNodeY(
		const eGridType 	gType)
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);
		pgl_assert(isValidGridType(gType));

		return getLastAssoNodeY(yNodeIdx_t(m_yPoints), gType);
	};


	/**
	 * \brief	This function returns ture if the node (i, j)
	 * 		 on the grid gType is a ghost node.
	 *
	 * If assetrs are enabled invalid accesses, for any reasons,
	 * will cause an error. If they are deactivated, invalid
	 * accessed (out of bound, onconsitent arguments) will
	 * be detected as ghost nodes.
	 *
	 * \param  i 		The index of the row.
	 * \param  j 		The index of the column.
	 * \param  gType	The grid type.
	 */
	bool
	isGhostNode(
		const Index_t 		i,
		const Index_t 		j,
		const eGridType 	gType)
	 const
	{
		pgl_assert(m_yPoints == m_yGrid.size(), m_yPoints > 1);
		pgl_assert(m_xPoints == m_xGrid.size(), m_xPoints > 1);
		pgl_assert(isValidGridType(gType));

		return isGhostNode(i, j, gType, yNodeIdx_t(m_yPoints), xNodeIdx_t(m_xPoints));
	}; //End: isGhost node memeber function.


	/**
	 * \brief	This function returns the grid spacing on
	 * 		 different grids.
	 *
	 * This function returns the spcaing between two grid ponts.
	 * It is thus one entry smaller than the number of grid poinst.
	 * See the note for the getGridPoints() function about the cell
	 * centre grid points. The returned distances also involves the
	 * ghost nodes if pressent, with teh exception of the regular cell
	 * centre points.
	 *
	 * These function calculates the spacing in x and y direction,
	 * they are returned in that order.
	 *
	 * The i-th entry allways describes the distance between node
	 * i and i+1, it is also always positive.
	 *
	 * \param  gType	Which grid you want.
	 */
	std::pair<GridSpacing_t, GridSpacing_t>
	getGridSpacings(
		const eGridType 	gType)
	 const;


	/**
	 * \brief	This function returns the coordinates of the node
	 * 		 that is located top left in the grid.
	 *
	 * This function returns the coordinate of the top left node.
	 * That has the index (0, 0). This is important for the
	 * interpolation routine to get the relative location.
	 *
	 * Note that this function has a long history of change, but
	 * should now be stable. It will always return the location
	 * of the (0, 0) index, that could also be optained
	 * by first calculate the node location and then access the
	 * first entry.
	 *
	 * \param  gType	The grid type.
	 *
	 * \note	This function is very internal, you should
	 * 		 not use it.
	 */
	std::pair<Numeric_t, Numeric_t>
	getTopNode(
		const eGridType 	gType)
	 const;


	/*
	 * =============================
	 * Static Functions
	 *
	 * Here are all the static functions that are used
	 * to provide generall support to other code sections.
	 */
public:
	/**
	 * \brief	This function returns true if the index j on grid gType
	 * 		 with nBasicX basic nodal points in x direction is a
	 * 		 ghost node.
	 *
	 * This function is only able to operate in x direction. There is a dedicated
	 * function for the y direction. This function checks if the index is a ghost
	 * node on that grid. For that only the number of basic nodal point in x
	 * direction is needed.
	 *
	 * Note that this function essentially checks if a coulum consists
	 * entierly of ghost nodes.
	 *
	 * This function operates on the second index.
	 *
	 * If asserts are enabled an out of bound access, as well as errorious
	 * input arguments are reported by them. If asserts are not enabled,
	 * this function will translate them into ghost nodes.
	 * The hope/idea behind this is, that this will cause an error in some
	 * other parts of the code, that will be noticed.
	 *
	 * \param  j		The j (second) index of the entry.
	 * \param  nBasicX	The number of basic nodal points in x direction.
	 * \param  gType	The grid type.
	 */
	static
	bool
	isGhostNodeColIdx(
		const Index_t 		j,
		const xNodeIdx_t	nBasicX,
		const eGridType 	gType)
	{
		// Input tests, if violated, turn it into a ghost node.
		if(!(Index_t(1) < nBasicX))
		{
			pgl_assert(false && "The number of grid points is invalid.");
			return true;
		};
		if(j < 0)
		{
			pgl_assert(false && "j value is negative.");
			return true;	//If assertts are disabled, condiert it as a ghost node.
		}; //End: lower bound check
		if(isExtendedGrid(gType) && isBasicNodePoint(gType))
		{
			pgl_assert(false && "An extended basic grid, does not exist.");
			return true;
		};
		if(isValidGridType(gType) == false)
		{
			pgl_assert(false && "The grid type is not valid.");
			return true;
		};

		if(isRegularGrid(gType) == true)
		{
			if(nBasicX <= j)		//== pgl_assert(j < nBasicX)
			{
				pgl_assert(false && "j is too large.");
				return true;	//In the case of an out of bound, we handle it as ghost node.
			}; //End if: upper bound check

			switch(mkCase(gType))
			{
			  case eGridType::StVx:		//On thge regular StVx grid all colums are technically valid
				return false;
			  break;

			  case eGridType::StVy:
				return ((j == 0)	//Here the first colum is ghost column
					? true
					: false );
			  break;

			  case eGridType::StPressure:	//Here also the first one are ghost node
			  case eGridType::CellCenter:	//Technically the same as pressure
				return ((j == 0)
					? true
					: false );
			  break;

			  case eGridType::BasicNode:	//The basic grid does not have any ghost nodes.
					return false;
			  break;

			  default:
				pgl_assert(false && "Unknown grid.");
				return true;	//Turn non exiosting grid type into an error
			  break;
			}; //End switch(gType):
		// End if: regular grid size
		}
		else
		{
			/*
			 * Here we are on the extended grid, this means we have more
			 * ghost nodes, and they are a bit different.
			 * It is important to realize something, in each direction we have
			 * nBasicX + 1 many grid nodes, but since we have zero based indexing
			 * we start at 0 and the last valid index is nBasicX.
			 * We also that the cell centre and pressure grid are differe, concerning
			 * ghost nodes.
			 */
			if(nBasicX < j)		//== pgl_assert(j <= nBasicX)
			{
				pgl_assert(false && "j is too large.");
				return true;	//In the case of an out of bound, we handle it as ghost node.
			}; //End if: upper bound check

			switch(mkCase(gType))
			{
			  case eGridType::StVx:		//The last column is composed of ghost nodes
				return ((j == nBasicX)
					? true
					: false       );
			  break;

			  case eGridType::StVy:
				return false;	//Here last row would be composed out of ghost nodes; but now column
			  break;

			  case eGridType::StPressure:	//Here the first and last column are ghost nodes
				return (((j == 0) || (j == nBasicX))
					? true
					: false                    );
			  break;

			  case eGridType::CellCenter:	//No ghost nodes
				return false;
			  break;

			  case eGridType::BasicNode:	//Forbidden and checked above
			  default:
				pgl_assert(false && "Unkown grid.");
				return true;	//Convert non existing grid into a ghost node
			}; //End switch:
		}; //End else: extended grid

		/*
		 * This code is unreacable, beacause we will certainly go through the default case
		 * which returns. However I do not like functions that does not have a return statement
		 * at the end, so I add it, I return true, to follow the philosophy of turning an
		 * illegal access into a ghost node.
		 */
		return true;
	}; //End: is ghost column


	/**
	 * \brief	This function tests if i corresponds to a row that
	 * 		 is completly filled with ghost nodes.
	 *
	 * For an indeep discussion, see the egd_isGhostNodeColIdx() function.
	 *
	 * This function operates on the first index. It essentially checks
	 * if a given row is completly composed of ghost nodes.
	 *
	 * In the case that asserts are enabled, they will report inconsistent
	 * arguments and out of bound accesses. If however they are deactivated
	 * then the function will return true, which indicates that the
	 * node is a ghost node.
	 *
	 * \param  i		The row index.
	 * \param  nBasicY	The number of basic nodal points in Y direction.
	 * \param  gType 	The grid type.
	 */
	static
	bool
	isGhostNodeRowIdx(
		const Index_t 		i,
		const yNodeIdx_t 	nBasicY,
		const eGridType 	gType)
	{
		if(!(Index_t(1) < nBasicY))
		{
			pgl_assert(false && "The number of grid points is invalid.");
			return true;
		};
		if(i < 0)	//== pgl_assert(0 <= i)
		{
			pgl_assert(false && "i is too small.");
			return true;	//If asserts are disabled, then consider an out of bound as a ghost node.
		}; //End if: lower bound checking
		if(isExtendedGrid(gType) && isBasicNodePoint(gType))
		{
			pgl_assert(false && "An extended basic grid, does not exist.");
			return true;
		};
		if(isValidGridType(gType) == false)
		{
			pgl_assert(false && "The grid type is not valid.");
			return true;
		};


		if(isRegularGrid(gType))
		{
			if(nBasicY <= i)	//==  pgl_assert(i < nBasicY)
			{
				pgl_assert(false && "i is too large.");
				return true; //If asserts disabled, consider out of bound as a ghost node.
			}; //End if: handle upper out of bound

			switch(mkCase(gType))
			{
			  case eGridType::StVx:		//The first row is composed of ghost nodes
				return ((i == 0)
					? true
					: false );
			  break;

			  case eGridType::StVy:		//No row in this grid is made of ghost nodes
				return false;
			  break;

			  case eGridType::StPressure:	//First row is composed of ghost nodes
			  case eGridType::CellCenter:	//Technically the same as pressure
				return ((i == 0)
					? true
					: false );
			  break;

			  case eGridType::BasicNode:	//The basic nodal grid does not have ghost nodes
			  	return false;
			  break;

			  default:
				pgl_assert(false && "Unkown grid.");
				return true;	//Turing an non existing grid into a ghost node.
			  break;
			}; //End switch(gType):
		//End if: regular grid
		}
		else
		{
			/*
			 * As in the X case, we know that we have nBasicY
			 * many basic nodal point and in an extended grid
			 * we have nBasicY + 1 many nodes, but because of
			 * the zero based indexing, the last node has index
			 * nBasicY.
			 */
			if(nBasicY < i)		//==  pgl_assert(i <= nBasicY)
			{
				pgl_assert(false && "i is too large.");
				return true; //If asserts disabled, consider out of bound as a ghost node.
			}; //End if: handle upper out of bound

			switch(mkCase(gType))
			{
			  case eGridType::CellCenter:		//No ghost nodes on this grid
				return false;
			  break;

			  case eGridType::StVx:			//No row is composed completly out of ghost node
				return false;
			  break;

			  case eGridType::StVy:			//The last row is composed of grid nodes
				return ((i == nBasicY)
					? true
					: false       );
			  break;

			  case eGridType::StPressure:		//Here the first and last row are ghost nodes
				return (((i == nBasicY) || (i == 0))
					? true
					: false                     );
			  break;

			  case eGridType::BasicNode:	//This does not exists
			  default:
				pgl_assert(false && "Passed an unknown grid.");
				return true;	//Unkown grid to a ghost node
			  break;
			}; //End switch(gType):
		}; //End else: extended grid


		/*
		 * I do not like functions thatdoes not have a return statement at the end
		 */
		return true;
	}; //End: is ghost row



	/**
	 * \brief	This function checks if the passed index (i, j)
	 * 		 is a ghost node or not on the grid.
	 *
	 * This function needs the number of basic gird points and the
	 * grid type for accessing. Note theat the order of the grid
	 * sizes are such that they will agree with the matrix notation.
	 * This function currently reduces the call to a call to
	 * the function for checking the row and colum seperatly.
	 * The result is ored.
	 * The the description of the implementing functions for
	 * more informations.
	 *
	 * If asserts are enabled, invalid arguments will result in
	 * an error. If they are deactivated invalid argumensts, also
	 * out of bound accesses, will turn the node into a ghost
	 * node.
	 *
	 * \param  i		The first matrix index; row, y.
	 * \param  j		The second matrix index; colum, x.
	 * \param  gType	The grid type.
	 * \param  nBasicY	The number of basic nodal points in y direction.
	 * 			 Corresponds to first index.
	 * \param  nBasicX	The number of basic nodal points in x direction.
	 * 			 Corresponds to the second index.
	 */
	static
	bool
	isGhostNode(
		const Index_t 		i,
		const Index_t 		j,
		const eGridType 	gType,
		const yNodeIdx_t 	nBasicY,
		const xNodeIdx_t 	nBasicX)
	{
		pgl_assert(0 <= i, Index_t(0) < nBasicY, i <= nBasicY, 	//General out of bound tests
			   0 <= j, Index_t(0) < nBasicX, j <= nBasicX,
			   isValidGridType(gType)            );

		return (isGhostNodeRowIdx(i, nBasicY, gType) ||
			isGhostNodeColIdx(j, nBasicX, gType)   );
	}; //End: test for ghost node


	/**
	 * \brief	This function returns the number of grid points
	 * 		 on the given grid, in x direction.
	 *
	 * This function is a static one, that is used for extenral code.
	 * This allows a uniform handling.
	 *
	 * The returned number is the number of grid nodes on the grid
	 * and also the number of entries in a property.
	 *
	 * \param  nBasicX	Number of grid Points in x direction.
	 * \param  gType	The grid we operate on.
	 */
	static
	Index_t
	getNGridPointsX(
		const xNodeIdx_t 	nBasicX,
		const eGridType 	gType)
	{
		pgl_assert(nBasicX > Index_t(1));
		pgl_assert(isValidGridType(gType));

		if(isRegularGrid(gType) == true)
		{
			return nBasicX;
		}
		else
		{
			pgl_assert(isExtendedGrid(gType));
			return (nBasicX + 1);
		};
	}; //End: static x Nodes computing

	/**
	 * \brief	This function returns the number of grid points
	 * 		 on the given grid, in y direction.
	 *
	 * See description of the getNGridPointsX() static function for more
	 * details.
	 *
	 * \param  nBasicY	The number of grid points in y direction.
	 * \param  gType	The grid we operate on.
	 */
	static
	Index_t
	getNGridPointsY(
		const yNodeIdx_t 	nBasicY,
		const eGridType 	gType)
	{
		pgl_assert(nBasicY > Index_t(1));
		pgl_assert(isValidGridType(gType));

		if(isRegularGrid(gType) == true)
		{
			return nBasicY;
		}
		else
		{
			pgl_assert(isExtendedGrid(gType));
			return (nBasicY + 1);
		};
	}; //End: static y Nodes computing


	/**
	 * \brief	This function returns the last grid node index that
	 * 		 can own a marker, for the x direction.
	 *
	 * The (0, 0) node is the first node a marker can be associated with.
	 * This is even true for regular cell centre grids, because of the
	 * conventions that they start at (1, 1).
	 * But what is the last one? This function answers this question.
	 *
	 * Depending on the grid the last node index, a marker can be
	 * associated with, is computed. This involves geometrical and
	 * physical considerations and is not necessaraly the last index.
	 * Note that the returned index does not map in the grid positions.
	 * but into the index. This is only an issue for the regular
	 * cell centre grid.
	 *
	 * Note that the neighbouring nodes of the returned one must
	 * not necessaraly exists, but the returned index is VALID, in
	 * the grid property.
	 * This node can be seen as the most right node that is still
	 * inside the computational domain and some markers could
	 * be at its right side.
	 *
	 * See getLastAssoNodeY() for the function for the y direction.
	 *
	 * \param  nBasicX	The number of grid points in the x direction.
	 * \param  gType	The grid type.
	 *
	 * \note 	This function is extremly internal and the user will
	 * 		 should never use it.
	 *
	 * \note 	This is a static function, there is a member function
	 * 		 that calls this one with the right arguments.
	 */
	static
	Index_t
	getLastAssoNodeX(
		const xNodeIdx_t 	nBasicX,
		const eGridType 	gType);


	/**
	 * \brief	This function computes the last valid node index
	 * 		 a marker can assossicated with in y direction.
	 *
	 * See getLastAssoNodeX() function for a description.
	 * But now it concernes the y direction, so it is not right, but
	 * bottom.
	 *
	 * \param  nBasicY	Number of grid points in y direction.
	 * \param  gType 	The grid type.
	 *
	 * \note	This is the statioc implementation. As for the X
	 * 		 direction there is a memeber function that
	 * 		 calls it with the right arguments.
	 */
	static
	Index_t
	getLastAssoNodeY(
		const yNodeIdx_t 	nBasicY,
		const eGridType 	gType);


	/**
	 * \brief	This function returns true if the grid of type
	 * 		 gType does have any ghost nodes points.
	 *
	 * According to the new convention, all grids except the basic grid
	 * and the extended cell centre grid have ghost nodes.
	 *
	 * Note this function checks the validity of its input with asserts.
	 * If they are disabled the function will interprete invalid arguments
	 * as grids that have no ghost node. This is done to create a disalignment
	 * with teh ghost node check functions.
	 *
	 * \param  gType	The grid Type that is used.
	 */
	static
	bool
	hasGridGhostNodes(
		const eGridType 	gType)
	{
		if(isValidGridType(gType) == false)
		{
			pgl_assert(false && "Passed an invalid grid type.");
			return false;
		}; //Enf if: handle invalid grid types


		/*
		 * Check if we are ghost node free
		 */
		if(isRegularGrid(gType) == true)
		{
			/*
			 * We are on the regular grid, so only the
			 * basic nodal grid does not have ghost nodes
			 */
			if(isBasicNodePoint(gType) == true)
			{
				return false;
			}; //End if: is basic nodal point

			/*
			 * If we are here, then we have ghost nodes
			 */
			return true;

		//End if: regular grid
		}
		else
		{
			/*
			 * On the exteneded grid, only the cell centre grid does not have any
			 * ghost nodes.
			 */
			if(isCellCentrePoint(gType) == true)
			{
				return false;
			}; //End if: CC

			//This is an inconsistency check
			if(isBasicNodePoint(gType) == true)
			{
				//This grid does not exists
				pgl_assert(false && "Passed a non existing grid.");
				return false; //By convention illegal grids have no ghost nodes
			}; //End if: special case of non exitsting grid

			/*
			 * If we are here we have ghost nodes
			 */
			return true;
		}; //End else: extended grid


		/*
		 * This section is unreachable, but I do not like
		 * functions without a return statement.
		 * So I add one here.
		 */
		pgl_assert(false && "Unreachable code.");
		return false;
	}; //End: hasGhostNodes


	/**
	 * \brief	This function returns a pair, that describes
	 * 		 a block view in x direction.
	 *
	 * first is the start index of the block in x direction.
	 * One can also see it as the first col index, that is
	 * not a ghost node.
	 * second is the length of the block view, meaning the
	 * number of nodes in that directinns, that are not ghost
	 * nodes, remember the zero based index.
	 *
	 * \param  gType	The grid type.
	 * \param  nBaseX	Number of basic grid points in x direction.
	 */
	static
	std::pair<Index_t, Index_t>
	getBlockSizeX(
		const eGridType 	gType,
		const xNodeIdx_t 	nBaseX)
	{
		if(isValidGridType(gType) == false)
		{
			pgl_assert(false && "Invalid grid type.");
			return std::make_pair(-1, -1);
		};
		if(nBaseX <= Index_t(0))
		{
			pgl_assert(false && "Too few nodal points.");
			return std::make_pair(-1, -1);
		};

		if(isRegularGrid(gType) == true)
		{
			//This is the number of cols in the grid property
			const Index_t N = getNGridPointsX(nBaseX, gType);
			switch(mkCase(gType))
			{
			  case eGridType::BasicNode:
			  	return std::make_pair(0, N);
			  break;

			  case eGridType::StPressure:
			  case eGridType::CellCenter:
			  	return std::make_pair(1, N - 1);
			  break;

			  case eGridType::StVx:
			  	return std::make_pair(0, N);
			  break;

			  case eGridType::StVy:
			  	return std::make_pair(1, N - 1);
			  break;

			  default:
			  	pgl_assert(false && "Unkown grid in reg case.");
			  	//Will be handled at the end
			}; //End switch(gType):
		//End if: regular grid
		}
		else
		{
			const Index_t N = getNGridPointsX(nBaseX, gType);
			switch(mkCase(gType))
			{
			  case eGridType::BasicNode:
			  	return std::make_pair(-1, -1);
			  break;

			  case eGridType::StPressure:
			  	return std::make_pair(1, N - 2);
			  break;

			  case eGridType::CellCenter:
			  	return std::make_pair(0, N);
			  break;

			  case eGridType::StVx:
			  	return std::make_pair(0, N - 1);
			  break;

			  case eGridType::StVy:
			  	return std::make_pair(0, N);
			  break;

			  default:
			  	pgl_assert(false && "Unkown grid in ext case.");
			  	//Handled at end
			}; //End switch(gType):
		}; //End else: extended grid

		//In the very unlikely case that we are here return an invalid value
		pgl_assert(false && "Reached unreachable code.");
		return std::make_pair(-1, -1);
	}; //End: get Block parameter in x


	/**
	 * \brief	This function returns a pair, that describes
	 * 		 a block view in y direction.
	 *
	 * This is the same as the getBlockSizeX() function, but now for
	 * the y direction.
	 *
	 * \param  gType	The grid type.
	 * \param  nBaseY	Number of basic grid points in y direction.
	 */
	static
	std::pair<Index_t, Index_t>
	getBlockSizeY(
		const eGridType 	gType,
		const yNodeIdx_t 	nBaseY)
	{
		if(isValidGridType(gType) == false)
		{
			pgl_assert(false && "Invalid grid type.");
			return std::make_pair(-1, -1);
		};
		if(nBaseY <= Index_t(0))
		{
			pgl_assert(false && "Too few nodal points.");
			return std::make_pair(-1, -1);
		};

		if(isRegularGrid(gType) == true)
		{
			//This is the number of cols in the grid property
			const Index_t N = getNGridPointsY(nBaseY, gType);

			switch(mkCase(gType))
			{
			  case eGridType::BasicNode:
			  	return std::make_pair(0, N);
			  break;

			  case eGridType::StPressure:
			  case eGridType::CellCenter:
			  	return std::make_pair(1, N - 1);
			  break;

			  case eGridType::StVx:
			  	return std::make_pair(1, N - 1);
			  break;

			  case eGridType::StVy:
			  	return std::make_pair(0, N);
			  break;

			  default:
			  	pgl_assert(false && "Unkown grid in reg case.");
			  	//Will be handled at the end
			}; //End switch(gType):
		//End if: regular grid
		}
		else
		{
			const Index_t N = getNGridPointsY(nBaseY, gType);
			switch(mkCase(gType))
			{
			  case eGridType::BasicNode:
			  	return std::make_pair(-1, -1);
			  break;

			  case eGridType::StPressure:
			  	return std::make_pair(1, N - 2);
			  break;

			  case eGridType::CellCenter:
			  	return std::make_pair(0, N);
			  break;

			  case eGridType::StVx:
			  	return std::make_pair(0, N);
			  break;

			  case eGridType::StVy:
			  	return std::make_pair(0, N - 1);
			  break;

			  default:
			  	pgl_assert(false && "Unkown grid in ext case.");
			  	//Handled at end
			}; //End switch(gType):
		}; //End else: extended grid

		//In the very unlikely case that we are here return an invalid value
		pgl_assert(false && "Reached unreachable code.");
		return std::make_pair(-1, -1);
	}; //End: get Block parameter in y


	/*
	 * ===========================
	 * Boundary functions
	 */
public:
	/**
	 * \brief	This function returns true id the grid type
	 * 		 has a boundary.
	 *
	 * In the case that the input data is invalid, the function
	 * returns false, to indicate that there is no boundary.
	 *
	 * \param  gType 	The grid type that is used.
	 */
	bool
	hasGridBoundary(
		const eGridType 	gType)
	{
		if(isValidGridType(gType) == false)
		{
			pgl_assert(false && "Grid type is not valid.");
			return false;	//no boundary in case of an error.
		}; //End if: is invalid node
			pgl_assert(!(isExtendedGrid(gType) && isBasicNodePoint(gType)));

		switch(mkCase(gType))
		{
		  case eGridType::CellCenter:
		  	if(isRegularGrid(gType) == true)
			{
				return false;	//This is debate, maybe
			}
			else
			{
				return true;	//The sole reason for its existance
			};
		  break;

		  case eGridType::StPressure:
		  	return false;	//Have not
		  break;

		  case eGridType::BasicNode:
		  	if(isExtendedGrid(gType) == true)
			{
				pgl_assert(false && "Extended basic grid does not exist.");
				return false;
			};
		  	return true;
		  break;

		  case eGridType::StVx:
		  	return true;
		  break;

		  case eGridType::StVy:
		  	return true;
		  break;

		  default:
		  	pgl_assert(false && "Unknown grid.");
		  	return false;
		  break;
		}; //End switch(gType):

		pgl_assert(false && "Unreachable code.");
		return false;
	}; //End: check if grid has a potzential boundary.



	/*
	 * =============================
	 * Binding functions
	 *
	 * This functions finalize and allocate *this.
	 */
public:
	/**
	 * \brief	This function allocate all the grid.
	 *
	 * This function allocates the internal arraies for *this.
	 * Without setting them to values. This function is
	 * calld by the building constructor or has to called
	 * after the default constructor manually.
	 *
	 * Note that comparing to the grid container, the order
	 * of the grid poinst is different, but the same as the
	 * buildiong constructor of *this. The first argument
	 * defines the number of x points, the nu
	 *
	 *
	 * \param  Ny 		The number of grid points in y direction (ROWS).
	 * \param  Nx		The number of grid points in x direction (COLS).
	 *
	 * \throw 	If incompability arrises.
	 *
	 * \note	Also consider the different ordering of the number of grid points.
	 * \note	Previously this function operated on a marker collection, but this
	 * 		 was changed. Now it operates on a map, that maps properties to grid types.
	 */
	void
	allocateGrids(
		const yNodeIdx_t		Ny,
		const xNodeIdx_t		Nx);


	/**
	 * \brief	This fucntion will create a constant grid.
	 *
	 * This function is for creating a constant grid.
	 * It only needs the start and end point of the doamin.
	 * It will then create the grid.
	 *
	 * Calling this function has similar effects than calling
	 * finalizeGrids(), in the sense that it will block
	 * access to mutable references of the grid points.
	 * However it is still requiered to call the finalize function.
	 *
	 * \param  xStart	The point where the domain starts in the x direction.
	 * \param  xEnd		The point where the domain ends in the x direction.
	 * \param  yStart	The point where the domain starts in the y direction.
	 * \param  yEnd		The point where the domain ends in the y direction.
	 *
	 * \throw 	If inconsistencies are detected.
	 */
	void
	makeConstantGrid(
		const Numeric_t 	xStart,
		const Numeric_t 	xEnd,
		const Numeric_t 	yStart,
		const Numeric_t 	yEnd);


	/**
	 * \brief	This function marks the grid of *this as set.
	 *
	 * This function must be called after all tzhe grid points
	 * were initalized and set.
	 * After this function was called no mutable reference to the
	 * grid positions can be optained.
	 *
	 * \throw	If compability conditions are violated.
	 */
	void
	finalizeGrids();


	/**
	 * \brief	This function returns a mutable reference
	 * 		 to the x coordinate of the grid points.
	 *
	 * This function is intended to set the sizes.
	 * They are allready set to the right size, but the
	 * values they contains are unspecific.
	 *
	 * \throw	This function throws if *this was already set.
	 */
	NodePosition_ref
	getMutableXPoints()
	{
		if(this->m_isGridSet == true)	//test if set
		{
			throw PGL_EXCEPT_LOGIC("Called the fucntion for optaining a mutable reference to the x coordinate of the grid points, but they were already set.");
		};
		if(this->m_isConstGrid == true)
		{
			throw PGL_EXCEPT_LOGIC("The x grid points can not be changed, since the grid is constant.");
		};
		if(m_xPoints <= 1)
		{
			throw PGL_EXCEPT_LOGIC("Called the fucntion for optaining a mutable reference to the x coordinate of the grid points, but the vector has length zero.");
		};
		pgl_assert(m_xGrid.size() == m_xPoints);

		//Return the reference
		return m_xGrid;
	}; //End: getMutableXPoints


	/**
	 * \brief	This function returns a mutable reference
	 * 		 to the y coordinate of the grid points.
	 *
	 * See getMutableXPoints() function for more information.
	 *
	 * \throw	If the coordinates where already set.
	 */
	NodePosition_ref
	getMutableYPoints()
	{
		if(this->m_isGridSet == true)	//test if set
		{
			throw PGL_EXCEPT_LOGIC("Called the fucntion for optaining a mutable reference to the y coordinate of the grid points, but they were already set.");
		};
		if(this->m_isConstGrid == true)
		{
			throw PGL_EXCEPT_LOGIC("The y grid points can not be changed, since the grid is constant.");
		};
		if(m_yPoints <= 1)
		{
			throw PGL_EXCEPT_LOGIC("Called the fucntion for optaining a mutable reference to the y coordinate of the grid points, but the vector has length zero.");
		};
		pgl_assert(m_yGrid.size() == m_yPoints);

		//Return the reference
		return m_yGrid;
	}; //End: getMutableYPoints



	/*
	 * ===================
	 * Private memeber
	 */
private:
	Size_t 			m_xPoints = 0;		//!< Numbers of basic grid points in the x direction (COLS).
	Size_t 			m_yPoints = 0;		//!< Numbers of basic grid points in the y direction (ROWS).

	NodePosition_t 		m_xGrid;		//!< All the x coordinates of the grid points.
	NodePosition_t 		m_yGrid;		//!< All the y coordinates of the grid points.
	Numeric_t 		m_xSpacing = NAN;	//!< Grid spacing in a regular grid in x direction. Initialized as NAN.
	Numeric_t 		m_ySpacing = NAN;	//!< Grid spacing in a regular grid in y direction. Initialized as NAN.
	bool 			m_isGridSet = false;	//!< Stores if the grid was set. Initialized as false.
	bool 			m_isConstGrid = false;	//!< Indicates if the grid spacing is regular in all dimensions.
}; //End: class(egd_gridGeometry_t)



PGL_NS_END(egd)



