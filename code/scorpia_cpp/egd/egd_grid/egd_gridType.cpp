/**
 * \brief	This file defines functions to support enumaration for the grid type.
 *
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_propertyIndex.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)

eGridType
getDefaultGrid(
	const egd_propertyIndex_t& 	propIdx,
	const bool 			isExtended)
{
	if(propIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The invalid property does not have a default grid type.");
	};

	/*
	 * =======================
	 * Begin: Handling of special cases
	 */

	/*
	 * This is a macro that can help ease programming.
	 */
	#define CASE(p, g) case eMarkerProp:: p : {const auto G = (isExtended ? mkExt(g) : mkReg(g)); pgl_assert(isValidGridType(G)); return G;}; break;
	switch(propIdx.getProp())
	{
		CASE(DensityVX, eGridType::StVx);
		CASE(DensityVY, eGridType::StVy);
		CASE(DensityP,  eGridType::StPressure);
		CASE(DensityCC, eGridType::CellCenter);
		CASE(Density,   eGridType::BasicNode);

		case eMarkerProp::TempDiff:
		case eMarkerProp::Temperature:
		if(isExtended == true)
		{
			return eGridType::CellCenter;
		}
		else
		{
			return eGridType::BasicNode;
		};
		CASE(TemperatureCC, eGridType::CellCenter);
		CASE(TempDiffCC, eGridType::CellCenter);

		CASE(VelX, eGridType::StVx);
		CASE(VelXCC, eGridType::CellCenter);

		CASE(VelY, eGridType::StVy);
		CASE(VelYCC, eGridType::CellCenter);

		CASE(ThermConduct, eGridType::BasicNode);
		CASE(ThermConductVX, eGridType::StVx);
		CASE(ThermConductVY, eGridType::StVy);

		CASE(Viscosity, eGridType::BasicNode);
		CASE(ViscosityP, eGridType::StPressure);
		CASE(ViscosityCC, eGridType::CellCenter);
		CASE(ViscosityVX, eGridType::StVx);
		CASE(ViscosityVY, eGridType::StVy);

		CASE(Pressure, eGridType::StPressure);

		CASE(RhoCp, eGridType::BasicNode);
		CASE(RhoCpCC, eGridType::CellCenter);

	  default:
	  	/* No special implementation, so call the default implementation. */
	  break;
	}; //End switch(propIdx):

	#undef CASE
	/*
	 * End: Handling properties special
	 * =====================
	 */


	/*
	 * If we are here, we have no special case and handling
	 * the default case
	 */
	return getDefaultGrid();
}; //End: get default grid for different properties.


std::string
printGridType(
	const eGridType 	gType)
{
	if(isValidGridType(gType) == false)
	{
		return std::string("INVALID grid type.");
	};

	//This is the return value
	std::string ret = (isRegularGrid(gType)
			   ? "Regular "
			   : "Extended ");

	switch(mkCase(gType))
	{
	  case eGridType::BasicNode:
	  	ret += "basic grid";
	  break;

	  case eGridType::StPressure:
	  	ret += "staggered pressure grid";
	  break;

	  case eGridType::StVx:
	  	ret += "staggered x velocity grid";
	  break;

	  case eGridType::StVy:
	  	ret += "staggered y velocity grid";
	  break;

	  case eGridType::CellCenter:
	  	ret += "cell centre grid";
	  break;

	  case eGridType::StGrid:
	  	ret += "fully staggered grid";
	  break;

	  default:
	  	throw PGL_EXCEPT_InvArg("An invalid argument was detected.");
	  break;
	}; //End switch(gType):

	return ret;
}; //End: printGridType




PGL_NS_END(egd)



