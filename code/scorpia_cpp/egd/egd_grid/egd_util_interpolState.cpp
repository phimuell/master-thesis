/**
 * \brief	This file defines some functions that operates on interpolation state.
 * 		 But also some member functions are here.
 *
 * The functions here are high level, they operate on simpler functions.
 * Such that they besically consist of calling functions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/dynamic_bitset.hpp>


//Include STD


PGL_NS_START(egd)

/*
 * ==============================
 * Free Functions
 *
 * These functins operates on the interpolation state
 * but are not member functions.
 */

bool
egd_initInterpolation(
	const egd_markerProperty_t& 		mPosY,
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		grid,
	const eGridType 			gType,
	egd_interpolState_t* const 		interpolRes_,
	const bool 				doProcessing,
	const bool 				doDisabeling,
	const bool 				withLocal)
{
	if(interpolRes_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The interpolation result container was invalid.");
	};
	if(isExtendedGrid(gType) && isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_InvArg("An extended basic nodal grid does not exists.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The grid type is invalid.");
	};

	egd_interpolState_t& interpolRes = *interpolRes_; //Alias

	//clear it, such that it is zero
	interpolRes.clear();


	/*
	 * Set internal storages
	 */
	interpolRes.gType 	  = gType;	//Gridtype
	interpolRes.m_isProcessed = false;	//They are not yet processed


	/*
	 * Finding the associated nodes
	 */
	const bool noCellEmpty = egd_findAssociatedNodeIndex(
					mPosY, mPosX, grid, gType, 			//Forwarding
					&interpolRes.idxI, &interpolRes.idxJ);	//Storing the node idx

	/*
	 * Finding the weights
	 */
	egd_findAssociatedMarkerWeigts(
				mPosY, mPosX, grid, gType, 				//Forwarding
				interpolRes.idxI, interpolRes.idxJ,		//Forwaring of the indexes
				&interpolRes.mWeights, &interpolRes.gWeights);	//Storing the weights
		pgl_assert(interpolRes.testForConsistency() );

	/*
	 * Test if local information is requested.
	 */
	if(withLocal == true)
	{
		egd_makeLocalInterpolState(
				mPosY, mPosX, grid, gType,				//Formward the general information
				interpolRes.idxI, interpolRes.idxJ,		//Forward the indexing
				&interpolRes.locAssoc, 				//Local association
				interpolRes.mWeights, 				//Marker weights
				&interpolRes.locGridWeights);			//Local grid weights.

			pgl_assert(interpolRes.hasLocalAssoc());
			pgl_assert(interpolRes.testForConsistency() );

	}; //End if: generate local information too


	/*
	 * Perform the disableing of the ghost nodes
	 * if requested
	 */
	if(doDisabeling == true)
	{
		const bool wasDis = egd_disableGhostNodes(&interpolRes);	//This function performs already checks
		(void)wasDis;
	}; //End if: do disableing


	/*
	 * Apply the processing to the weights
	 * If requested.
	 */
	if(doProcessing == true)
	{
		const bool wasProcessed = egd_processWeights(&interpolRes);	//This must be true

		if(wasProcessed == false)
		{
			throw PGL_EXCEPT_RUNTIME("The processing failed.");
		}; //End if: processing failed
			pgl_assert(interpolRes.isProcessed());
	}; //End if: doProcessing

	/*
	 * Test if the result was okay
	 */
	if(egd_isValidWeight(interpolRes) == false)
	{
		//Use asserts to find the error
		throw PGL_EXCEPT_RUNTIME("Error during the creation of the interpolation state.");
	};//End if:


	/*
	 * Return the result of the index function
	 */
	return noCellEmpty;
}; //End: init the result


bool
egd_initInterpolation(
	const egd_markerCollection_t&		mColl,
	const egd_gridGeometry_t& 		grid,
	const eGridType 			gType,
	egd_interpolState_t* const 		interpolState,
	const bool 				doProcessing,
	const bool 				doDisabeling,
	const bool 				withLocal)
{
	pgl_assert(interpolState != nullptr);
	return egd_initInterpolation(
		mColl.cgetYPos(), mColl.cgetXPos(),
		grid, gType, interpolState,
		doProcessing, doDisabeling, withLocal);
}; //End: convenient init interpol state


bool
egd_isValidWeight(
	const egd_interpolState_t& 	interpolRes)
{
	return interpolRes.isValid();
}; //End: isValid


bool
egd_processWeights(
	egd_interpolState_t* const 	interpolRes_)
{
	if(interpolRes_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The interpolation result container was invalid.");
	};
	egd_interpolState_t& interpolRes = *interpolRes_; //Alias

	if(interpolRes.isValidGridType() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid type is invalid.");
	};
	if(interpolRes.testForConsistency() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The interpolation state was inconsistent.");
	};

	//Tets if it is processed
	if(interpolRes.isProcessed() == true)
	{
		return false;
	}; //End if: is already processed


	//
	//Call the processing function on the non local grids
	egd_processGridWeights(&interpolRes.gWeights);

	//
	//Call the processing function on the local grids if they are present
	if(interpolRes.hasLocalAssoc() )
	{
		egd_processGridWeights(&interpolRes.locGridWeights);
	}; //End if: has local state


	//Mark the result as processed
	interpolRes.m_isProcessed = true;

	//
	//Return true to indicate that everything workerd
	return true;
}; //End: process weight of interpol container


bool
egd_disableGhostNodes(
	egd_interpolState_t* const 	interpolRes_)
{
	if(interpolRes_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The interpolation result container was invalid.");
	};
	egd_interpolState_t& interpolRes = *interpolRes_; //Alias

	pgl_assert(interpolRes.isFinite());

	//Get the grid type
	const eGridType gType = interpolRes.gType;

	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The grid type is invalid.");
	};
	if(interpolRes.testForConsistency() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The interpolation state was inconsistent.");
	};

	//Apply the restriction to the grid nodes
	const bool res = egd_disableGhostNodes(&interpolRes.gWeights, gType);

	//make a test for consistency
	if(res != egd_gridGeometry_t::hasGridGhostNodes(gType))
	{
		throw PGL_EXCEPT_RUNTIME("The grid type should have ghost node, but nothing was changed.");
	}; //End inconsistency with the grid type

	//Perform the disavbeling of the local associatioins
	if(interpolRes.hasLocalAssoc() )
	{
		const bool locDis = egd_disableGhostNodes(&interpolRes.locGridWeights, gType);

		if(locDis != res)
		{
			throw PGL_EXCEPT_RUNTIME("The processing of the local grid weights leaded to a different result, than the non-local ones.");
		};
	}; //End if: has local information

	return res;
}; //End: interpol kill ghost


void
egd_mapToGrid(
	const egd_interpolState_t& 		interpolRes,
	const egd_markerProperty_t& 		mPorp,
	egd_gridProperty_t* const 		gProp_,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				disableGhostNodes,
	const bool 				doLocalInterpol)
{
	if(gProp_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The passed grid property is null.");
	};
	egd_gridProperty_t& gProp = *gProp_;

	if(interpolRes.gType != gProp.getType())
	{
		throw PGL_EXCEPT_InvArg("The grid type between the interpolation result and the grid propery differs. Interpolation is for " + interpolRes.gType + ", nut grid was " + gProp.getGridType() );
	};
	if(interpolRes.isValidGridType() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid type is invalid.");
	};
	if(interpolRes.isProcessed() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not use a interpolation result that is not processed.");
	};


	/*
	 * Test on which branch we are
	 */
	if(doLocalInterpol == true)
	{
		//Test if the state has local information
		if(interpolRes.hasLocalAssoc() == false)
		{
			throw PGL_EXCEPT_InvArg("Requested local interpolation, but does not provided a state for it.");
		};

		//
		//Call the interpolation function
		egd_mapToGridLocal(
			&gProp, mPorp,				//Pass the properties
			interpolRes.mWeights, 			//Pass the marker weights
			interpolRes.locGridWeights, 		//Pass the local grid weights
			interpolRes.idxI, interpolRes.idxJ, 	//Pass the global association index
			interpolRes.locAssoc, 			//Pass the local association information
			modWeights, 				//Pass the weight modifier
			disableGhostNodes);			//Pass if ghost nodes should be disabled
	}
	else
	{
		//
		//Call the interpolation function for non local interpolation.
		egd_mapToGrid(
			&gProp,						//Destination
			mPorp,						//Sources
			interpolRes.mWeights, interpolRes.gWeights,	//Weights
			interpolRes.idxI, interpolRes.idxJ,		//Indexes
			modWeights, disableGhostNodes);			//Alternative/extended weights
	};

	pgl_assert(gProp.isFinite());

	return;
}; //End: interpolate


void
egd_pullBackToMarker(
	egd_markerProperty_t* const 		mProperty_,
	const egd_gridProperty_t& 		gProperty,
	const egd_interpolState_t& 		interpolState,
	const bool 				doUpdate)
{
	if(mProperty_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The marker property is the nullptr.");
	};
	egd_markerProperty_t& mProperty = *mProperty_;	//Aliasing

	if(gProperty.getType() != interpolState.gType)
	{
		throw PGL_EXCEPT_InvArg("The interpolation state and the source grid property have a different grid.");
	};

	egd_pullBackToMarker(&mProperty, gProperty,
			interpolState.mWeights,
			interpolState.idxI, interpolState.idxJ,
			doUpdate);

	return;
}; //End: pull back to marker


/*
 * =======================================
 * Memeber Functions
 */


bool
egd_interpolState_t::testForConsistency()
 const
 noexcept
{
	if(gType == eGridType::INVALID)
	{
		pgl_assert(false && "gType is invalid.");
		return false;
	};
	if(gWeights.rows() < 1 || gWeights.cols() < 1)
	{
		pgl_assert(false && "gWeights is too small.");
		return false;
	};

	//get the number of stored nodes
	const Index_t nMarkers = idxI.size();

	if(nMarkers <= 0)
	{
		pgl_assert(false && "idxI has too few markers.");
		return false;
	};

	if((Index_t)idxJ.size() != nMarkers)
	{
		pgl_assert(false && "idxJ has the wrong size");
		return false;
	};
	if(this->hasLocalAssoc() == true)
	{
		if((Index_t)locAssoc.size() != nMarkers)
		{
			pgl_assert(false && "Different numbers of markers in local associations.");
			return false;
		};
		if(locGridWeights.rows() != gWeights.rows())
		{
			pgl_assert(false && "Different number of rows in the two grid weights.");
			return false;
		};
		if(locGridWeights.cols() != gWeights.cols())
		{
			pgl_assert(false && "Different number of cols in the two grid weights.");
			return false;
		};
	}; //End if: check

	for(int i = 0; i != 2; ++i)
	{
		for(int j = 0; j != 2; ++j)
		{
			if(mWeights[i][j].size() != nMarkers)
			{
				pgl_assert(false && "A marker weight has the wrong size.");
				return false;
			}
		};
	};

	return true;
}; //End: check for consistency


bool
egd_interpolState_t::isFinite()
 const
{
	//
	//Test if Consistent
	if(this->testForConsistency() == false)
	{
		pgl_assert(false && "Is not consistent");
		return false;
	};


	//
	//Test if grid weight is finite
	if(gWeights.isFinite().all() == false)
	{
		pgl_assert(false && "Non finite value in grid weights.");
		return false;
	}; //End if: non finite value

	//
	//Test if local grid sizes are finite, if pressent
	if(this->hasLocalAssoc() == true)
	{
		if(locGridWeights.isFinite().all() == false)
		{
			pgl_assert(false && "Non finite local grid weights.");
			return false;
		};
	}; //End if: local grid weights


	//
	//Test if marker weights are finite
	for(int i = 0; i != 2; ++i)
	{
		for(int j = 0; j != 2; ++j)
		{
			if(mWeights[i][j].array().isFinite().all() == false)
			{
				pgl_assert(false && "A marker has an illegal weight.");
				return false;
			}
		}; //End for(j):
	}; //End for(i):


	//
	//Found nothing.
	return true;
}; //End: isFinite

bool
egd_interpolState_t::isValid()
 const
{
	if(this->isFinite() == false)	//Will also do the consistency check
	{
		pgl_assert(false && "Is not finite");
		return false;
	};


	//Test if the gWeigts are valid
	{
		Index_t rowIdx = -1, colIdx = -1;	//For debuging, such that we knwo where they were

		//Test for the minimum
		const Numeric_t minGValue = gWeights.minCoeff(&rowIdx, &colIdx);

		if(minGValue < 0.0)
		{
			pgl_assert(false && "A grid weight is smaller than zero.");
			return false;
		}; //End if: too small value
	}; //End scope: check gWeights

	//Test if the local gWeigts are valid
	if(this->hasLocalAssoc() == true)
	{
		Index_t rowIdx = -1, colIdx = -1;	//For debuging, such that we knwo where they were

		//Test for the minimum
		const Numeric_t minGValue = locGridWeights.minCoeff(&rowIdx, &colIdx);

		if(minGValue < 0.0)
		{
			pgl_assert(false && "A local grid weight is smaller than zero.");
			return false;
		}; //End if: too small value
	}; //End scope: check gWeights

	/*
	 * Test the marker weights
	 */
	for(int i = 0; i != 2; ++i)
	{
		for(int j = 0; j != 2; ++j)
		{
			Index_t miCoeff, maCoeff;	//Index, needed for debugging

			if(mWeights[i][j].maxCoeff(&maCoeff) > 1.0)
			{
				pgl_assert(false && "A marker has a too large weight.");
				return false;
			}
			if(mWeights[i][j].minCoeff(&miCoeff) < 0.0)
			{
				pgl_assert(false && "A marker has a too small weight.");
				return false;
			}
		}; //End for(j):
	}; //End for(i):

	/*
	 * Test the indexes
	 * Note it is only possible to do a small check, since we do
	 * not known the number of basic grid points.
	 */
	const Index_t nRows    = gWeights.rows();
	const Index_t nCols    = gWeights.cols();
	const Index_t nMarkers = mWeights[0][0].size();
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		const Index_t gI = idxI[m];
		const Index_t gJ = idxJ[m];

		if(!((0 <= gI) && (gI < nRows)))
		{
			pgl_assert(false && "Found a not valid first index.");
			return false;
		};
		if(!((0 <= gJ) && (gJ < nCols)))
		{
			pgl_assert(false && "Found a not valid second index.");
			return false;
		};
	}; //End for(m):

	/*
	 * Test the association.
	 * This is a very exotic test
	 */
	if(this->hasLocalAssoc() == true)
	{
		const uInt8_t e = ~(uInt8_t(3));	//1111 1100
		for(Index_t m = 0; m != nMarkers; ++m)
		{
			const uInt8_t u = locAssoc[m];

			if((u & e) != 0)
			{
				pgl_assert(false && "More than two dimensions detected.");
				return false;
			};
		}; //End for(m):
	}; //End if: test for dimension

	//
	//FOund nothing
	return true;
}; //End: isValid



void
egd_interpolState_t::clear()
{
	this->gType = eGridType::INVALID;

	//Clear the index vectors
	egd_indexVector_t().swap(this->idxI);
	egd_indexVector_t().swap(this->idxJ);
	egd_localAssocVector_t().swap(this->locAssoc);

	//Clear the grid weights
	this->gWeights.setZero(0, 0);
		pgl_assert(gWeights.size() == 0,
			   gWeights.rows() == 0,
			   gWeights.cols() == 0 );
	this->locGridWeights.setZero();
		pgl_assert(locGridWeights.size() == 0);

	//Clear the marker weights
	for(int i = 0; i != 2; ++i)
	{
		for(int j = 0; j != 2; ++j)
		{
			this->mWeights[i][j].setZero(0);
		}; //End for(j):
	}; //End for(i):

	return;
}; //End: clear state


void
egd_interpolState_t::changeGridType(
	const eGridType 		newTargetGrid)
{
	if(egd::isValidGridType(this->gType) == false)
	{
		throw PGL_EXCEPT_LOGIC("The interpolation state does not have a valid grid type.");
	};
	if(this->testForConsistency() == false)
	{
		throw PGL_EXCEPT_LOGIC("The interpolation state is not consistent.");
	};
	if(egd::isValidGridType(newTargetGrid) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid type is not valid.");
	};
	if(::egd::getSizeBit(this->gType) != ::egd::getSizeBit(newTargetGrid))
	{
		throw PGL_EXCEPT_InvArg("The change function can not change the grid extension.");
	}; //End if: different sizes

	//We can always go to ourself
	if(this->gType == newTargetGrid)
	{
		return;
	}; //End: handle the trivial case

	if(this->hasLocalAssoc() == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not convert a interpolation state with local geometry.");
	};

#define FAILURE throw PGL_EXCEPT_LOGIC("No known conversion for " + this->gType + " -> " + newTargetGrid)


	/*
	 * Depending on the grid we act diffferently
	 */
	if(::egd::isCellCentrePoint(this->gType) == true)
	{
		/*
		 * From a CC grid we can always go to a Pressure grid.
		 * Because here we can be sure that no disabled nodes
		 * are pressent, becuase it does not hase ones.
		 * It is also important, that this is the only grid
		 * we can reach.
		 */
		if(::egd::getSizeBit(this->gType) != ::egd::getSizeBit(newTargetGrid))
		{
			FAILURE;
		}; //End if: different sizes

		//We knwo how to go to pressure
		if(::egd::isPressurePoint(newTargetGrid) == true)
		{
			/*
			 * We can go to pressure, note that we do not disable the nodes.
			 */
			this->gType = newTargetGrid;

			return;
		}; //End if: to pressure


		/*
		 * If we are here, then we have a failure
		 */
		FAILURE;
	//END IF: From CC to
	}
	else if(::egd::isPressurePoint(this->gType) == true)
	{
		/*
		 * From pressre we can only go to the CC, and this is also only possible
		 * on the regular grid size. The reason is that the pressure grid may have
		 * deactivated nodes, that the CC grid does not have had.
		 */

		if((::egd::isRegularGrid(    this->gType  ) == true) &&
		   (::egd::isRegularGrid(    newTargetGrid) == true) &&
		   (::egd::isCellCentrePoint(newTargetGrid) == true) 	)
		{
			pgl_assert(::egd::isRegularGrid(newTargetGrid));

			this->gType = mkReg(eGridType::CellCenter);

			return;
		}; //End if: can go


		FAILURE;
	// END IF: From PRESSURE to
	}
	else
	{
		/*
		 * We did not knnow the conversation.
		 */
		FAILURE;
	}; //End: else


	/*
	 * If we are here, then the conversion is not supported.
	 */
	FAILURE;
	return;
#undef FAILURE
}; //End: change grid type

PGL_NS_END(egd)

