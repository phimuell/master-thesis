/**
 * \brief	This file implements constructor and allocation functions for the grid property class.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD


PGL_NS_START(egd)


void
egd_gridProperty_t::resize(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx)
{
	/*
	 * Test the imput
	 */
	pgl_assert(m_Ny > 0, m_Nx > 0);
	if(Ny != m_Ny)
	{
		throw PGL_EXCEPT_InvArg("Can not actually resize the size of the matrix. Number of gridpoints in y direction are "
				+ std::to_string(m_Ny) + ", but " + std::to_string(Ny) + " were passed.");
	};
	if(Nx != m_Nx)
	{
		throw PGL_EXCEPT_InvArg("Can not actually resize the size of the matrix. Number of gridpoints in x direction are "
				+ std::to_string(m_Nx) + ", but " + std::to_string(Nx) + " were passed.");
	};
	if(m_pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("The property index is invalid and was not set.");
	};
	if(::egd::isValidGridType(m_type) == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid type was not set.");
	};


	//Now we calculate the size that we need
	const Index_t gNx = GridGeometry_t::getNGridPointsX(xNodeIdx_t(Nx), m_type);
	const Index_t gNy = GridGeometry_t::getNGridPointsY(yNodeIdx_t(Ny), m_type);

	//Now we alloctae the property and set its size too zero.
	m_grid.setZero(gNy, gNx);
		pgl_assert(this->checkSize()  ,
			   this->isAllocated() );

	return;
}; //End: resize


void
egd_gridProperty_t::allocateGrid(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const GridType_t 	gType)
{
	/*
	 * Test the state of *this
	 */
	if(m_Nx != 0)
	{
		throw PGL_EXCEPT_LOGIC("The x size was already set.");
	};
	if(m_Ny != 0)
	{
		throw PGL_EXCEPT_LOGIC("The y size was walready set.");
	};
	if(isValidGridType(m_type) == true)
	{
		throw PGL_EXCEPT_LOGIC("The grid type was set.");
	};
	if(m_grid.size() != 0)
	{
		throw PGL_EXCEPT_LOGIC("The grid was already allocated.");
	};
	if(m_pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("The property index is invalid and was not set.");
	};


	/*
	 * Test the imput data
	 */
	if(Ny <= Index_t(0))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in y direction is invalid, it was " + std::to_string(Ny));
	};
	if(Nx <= Index_t(0))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in x direction is invalid, it was " + std::to_string(Nx));
	};
	if(isValidGridType(gType) == false)	//This is only an incomplete test
	{
		throw PGL_EXCEPT_InvArg("The passed grid type is invalid.");
	};


	/*
	 * Store some internal parameters
	 */
	m_Ny   = Ny;
	m_Nx   = Nx;
	m_type = gType;


	/*
	 * Allocate the storage.
	 */
	this->resize(Ny, Nx);
		pgl_assert(this->checkSize());
	return;
}; //End: allocateSize


void
egd_gridProperty_t::allocateGrid(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const GridType_t 	gType,
	const PropIdx_t 	prop)
{
	if(this->m_pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid property of *this is invalid.");
	};
	if(prop.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid property is invalid.");
	};
	if(this->m_pIdx != prop)
	{
		throw PGL_EXCEPT_InvArg("Passed a wrong property index."
				" *this is \"" + this->m_pIdx.print() +"\" but passed a \""
				+ prop.print() + "\".");
	};

	/*
	 * Forward the call
	 */
	this->allocateGrid(Ny, Nx, gType);

	return;
}; //End: allocating



egd_gridProperty_t::egd_gridProperty_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const GridType_t 	gType,
	const PropIdx_t 	prop,
	const bool 		extended)
 :
  egd_gridProperty_t(Ny, Nx,	//Use the build constructor
  		  mkCase(gType) | (extended == true ? GridType_t::Extended : GridType_t::BasicNode),
  		  prop)
{
	//Checks are already done.
};




egd_gridProperty_t::egd_gridProperty_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const GridType_t 	gType,
	const PropIdx_t 	prop)
:
  egd_gridProperty_t(prop)			//Will set the property
{
	/*
	 * Test the imput data
	 */
	if(Ny <= Index_t(0))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in y direction is invalid, it was " + std::to_string(Ny));
	};
	if(Nx <= Index_t(0))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in x direction is invalid, it was " + std::to_string(Nx));
	};
	if(isValidGridType(gType) == false)	//This is only an incomplete test
	{
		throw PGL_EXCEPT_InvArg("The passed grid type is invalid.");
	};
	if(prop.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property.");
	};


	/*
	 * Allocate the grid and set the internal states
	 */
	this->allocateGrid(Ny, Nx, gType);
}; //End: building constructor

egd_gridProperty_t::egd_gridProperty_t(
	const PropIdx_t 	prop)
 :
  m_grid(),
  m_Ny(0),
  m_Nx(0),
  m_type(eGridType::INVALID),
  m_pIdx(prop)
{
	if(m_pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property was invalid.");
	};
}; //End: postponed building constructor



egd_gridProperty_t
egd_gridProperty_t::copyToExtendedGrid()
 const
{
	if(hasValidGridType() == false)
	{
		throw PGL_EXCEPT_RUNTIME("Can not copy an invalid grid.");
	};
	if(onBasicNodalPoints() == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not extend a basic nodal grid.");
	};
		pgl_assert(m_Nx > 0, m_Ny > 0, m_pIdx.isValid());

	//Test if *this is extended, if so then return a copy of *this.
	if(this->isExtendedGrid() == true)
	{
		return *this;
	}; //End if: is already extended


	// Create the return type
	egd_gridProperty_t ret(
		yNodeIdx_t(this->m_Ny), xNodeIdx_t(this->m_Nx), 	//Size of the property
		mkExt(mkCase(this->m_type)),				//The new grid type
		this->m_pIdx);						//The grid property.

	// Now we copy the data arround
	ret.m_grid.block(0, 0, this->m_grid.rows(), this->m_grid.cols()) = this->m_grid;


	return ret;
}; //End: copy to extended class


egd_gridProperty_t&
egd_gridProperty_t::EXTEND_INPLACE(
	egd_gridProperty_t* const 	gProp_)
{
	pgl_assert(gProp_ != nullptr);
	egd_gridProperty_t& gProp = *gProp_;	//Alias the argument

	if(gProp.hasValidGridType() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not extend a grid property that is not valid.");
	};
	if(gProp.onBasicNodalPoints() == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not extend a basic nodal grid.");
	};
		pgl_assert(gProp.m_Nx > 0, gProp.m_Ny > 0, gProp.m_pIdx.isValid());

	//test if this is already extended
	if(gProp.isExtendedGrid() == true)
	{
		return gProp;
	}; //End if: already extended.


	//Update the grid type
	gProp.m_type = mkExt(mkCase(gProp.m_type));

	//Now we calculate the size that we need
	const Index_t gNx = GridGeometry_t::getNGridPointsX(xNodeIdx_t(gProp.m_Nx), gProp.m_type);
	const Index_t gNy = GridGeometry_t::getNGridPointsY(yNodeIdx_t(gProp.m_Ny), gProp.m_type);

	//Use teh conservative resize function that will copy them relative to the left top node
	//This is exactly what we want.
	gProp.m_grid.conservativeResize(gNy, gNx);

	return gProp;
}; //End: extend inplace



PGL_NS_END(egd)



