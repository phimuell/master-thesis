#pragma once
/**
 * \brief	This file defines the grid property class.
 *
 * Previously this was basically a typedef of an Eigen type.
 * It was later learned that it is better to make a dedicated
 * class for that.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_util.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD


PGL_NS_START(egd)

//FWD of a friend function
extern
void
egd_setGhostNodesToZero(
	egd_gridProperty_t* const 	gProp);


//FWD OF THE STANDARD SOLVER
class egd_generalSolverImpl_t;

//FWD of the class itself
class egd_gridProperty_t;


/**
 * \brief	This macro allows to make the property grid imulatbel.
 * \define 	EGD_GRIDPROP_ALLOW_MUTABLE
 *
 * If this macro is defined to a value other than zero.
 * The class allows to directly access the underling grid.
 * Such that it can it be resized.
 *
 * It is not recomended to allow this.
 */
#define EGD_GRIDPROP_ALLOW_MUTABLE 0


/**
 * \brief	This macro will instruct the grid property to enforce safe accessing.
 * \define 	EGD_GRIDPROP_FORCE_ACCESS_CHECK
 *
 * if this macro is set to a value other than zero, it will instruct the
 * raw access functions to enforce checks with throws, the asserts are
 * then ignored and only throws are in place.
 */
#define EGD_GRIDPROP_FORCE_ACCESS_CHECK 1


/**
 * \brief	Specialization of the egd_printMatrixSize() function for gris properties.
 *
 * Implementation is below.
 */
inline
std::string
egd_printMatrixSize(
	const egd_gridProperty_t& 	gp);



/**
 * \brief	This class is a grid property.
 * \class	egd_gridProperty_t
 *
 * This class is a grid property.
 * Previously the grid property was implemented as a typedef
 * of an Eigen type. It was then learned that this is not that
 * since now we have different grids. So this class was created.
 * It is basically a wrapper arround an Eigen type, that knwos
 * on which grid it is defined and also knwos its size.
 *
 * It was also learned that for properties Eigen Arrays are more
 * suitable.
 *
 * This class mimiks to some extend an Eigen type.
 *
 * See also the description of the eGridType enum, that is defined
 * in "egd_grid/egd_gridType.hpp" file for more information on
 * the grid sizes.
 *
 * If a row, column or single entry is requested, the class checks
 * if they corresponds to a ghost node or not. In the case of rows
 * and columns it is checked if all nodes in that row/col are ghost
 * nodes. If this is true, then an error is generated. Meaning that
 * accessing ghost nodes is not possible.
 * For certain functions raw functions are provided that bypasses
 * these checks, but they are private.
 *
 */
class egd_gridProperty_t
{
	/*
	 * ====================
	 * Typedef
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;		//!< This is the marker collection.
	using PropIdx_t 		= MarkerCollection_t::PropIdx_t;	//!< This is the idex type for the properties
	using PropList_t 		= ::pgl::pgl_vector_t<PropIdx_t>;	//!< This is a list of properties.
	using GridType_t 		= eGridType;				//!< The type used for encoding which grid we have.
	using BaseGridProperty_t	= egd_Array_t<Numeric_t>;		//!< This is the underlying type of the property.
	using GridGeometry_t 		= egd_gridGeometry_t;			//!< This is the grid geometry class.

	/*
	 * These are Eigen types.
	 */
	//They are needed for block operations.
	using      ColXpr_t		= BaseGridProperty_t::ColXpr;
	using ConstColXpr_t 		= BaseGridProperty_t::ConstColXpr;
	using      RowXpr_t 		= BaseGridProperty_t::RowXpr;
	using ConstRowXpr_t 		= BaseGridProperty_t::ConstRowXpr;
	using      BlockXpr_t 		= BaseGridProperty_t::BlockXpr;
	using ConstBlockXpr_t		= BaseGridProperty_t::ConstBlockXpr;

	//Absolute value
	using AbsReturnType_t		= BaseGridProperty_t::AbsReturnType;

	//Matrix wrapper
	using MatrixWrapper_t 		= Eigen::MatrixWrapper<BaseGridProperty_t>;
	using constMatrixWrapper_t 	= const Eigen::MatrixWrapper<const BaseGridProperty_t>;




	/*
	 * ==============================
	 * Building Constructors
	 */
public:
	/**
	 * \brief	This is the constructor that constructs a property.
	 *
	 * The user has to base the size of of the basic nodal grid.
	 * This is the number of grid points not the one you have in the
	 * calculation, and you have to give the grid type of *this.
	 *
	 * Note that you should only pass the basic property to this function.
	 * Since some helper properties are defined at other locations.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  gType	The type of the grid that we mimik.
	 * \param  prop		This is the property, that isstorred on the grid.
	 */
	egd_gridProperty_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const GridType_t 	gType,
		const PropIdx_t 	prop);


	/**
	 * \brief	This is a convenient building constructor.
	 *
	 * It only requeres the node type to be passed if the grid
	 * is extended or not is selected by a bool.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  gType	The type of the grid that we mimic (only location).
	 * \param  prop		This is the property, that isstorred on the grid.
	 * \param  extend	If the grid should be extended or not.
	 */
	egd_gridProperty_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const GridType_t 	gType,
		const PropIdx_t 	prop,
		const bool 		extended);


	/*
	 * \brief	This the the postponed building constructor.
	 *
	 * This constructor takes the grid property which *this should
	 * hold and nothing else. Before this object can be used, you
	 * have to call the allocate function of *this.
	 *
	 * \param  prop		The property to allocate.
	 */
	egd_gridProperty_t(
		const PropIdx_t 	prop);


	/*
	 * =====================
	 * Special Operators
	 *
	 * Here are some special operators.
	 */
public:
	/**
	 * \brief	This function allows to assigne *this from an
	 * 		 Eigen extression.
	 *
	 * This is function allows to assigne *this form an Eigen
	 * Expression. This is quite a powerfull operator, so it
	 * is restricted. For example no resizing will be done, so
	 * *this must have the right size.
	 * Further all nodes, including the ghost nodes are included
	 * in the assignment.
	 *
	 * \param  other	The eigen expression we want.
	 *
	 * \tparam  Derived	The derived type of other.
	 */
	template<
		typename 	Derived
	>
	egd_gridProperty_t&
	operator= (
		const ::Eigen::DenseBase<Derived>& 	other)
	{
		/*
		 * Test if *this is okay.
		 */
		if(this->isAllocated() == false)
		{
			throw PGL_EXCEPT_LOGIC("*this is not allocated.");
		};
		if(this->hasValidGridType() == false)
		{
			throw PGL_EXCEPT_LOGIC("*this does not have a valid grid type.");
		};
		if(this->getPropIdx().isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("*this does not have a valid griod type.");
		};
		if(this->checkSize() == false)
		{
			throw PGL_EXCEPT_LOGIC("Dedtected wrong sizes.");
		};


		/*
		 * Test if other is compatible with *this
		 */
		if(this->rows() != other.rows() ||
		   this->cols() != other.cols()   )
		{
			throw PGL_EXCEPT_InvArg("The assignment source is not compatible with *this."
					" other is a " + MASI(other) + " matrix, but this is " + MASI(*this));
		};

		/*
		 * Now we perform the actual assignment
		 */
		m_grid = other;


		/*
		 * Now we check if we had an error.
		 */
		if(this->checkSize() == false)
		{
			throw PGL_EXCEPT_LOGIC("The assignement went wrong, *this has the wrong size.");
		};


		/*
		 * Return *this
		 */
		return *this;
	}; //End: assignment from Eigen extression


	/*
	 * ======================
	 * Defaulted Constructors
	 */
public:
	/**
	 * \brief	Copy constructor.
	 *
	 * Defaulted.
	 */
	egd_gridProperty_t(
		const egd_gridProperty_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Defaulted and no except
	 */
	egd_gridProperty_t(
		egd_gridProperty_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Defaulted.
	 */
	~egd_gridProperty_t()
	 = default;


	/**
	 * \brief	Copy Assignment.
	 *
	 * Defaulted.
	 */
	egd_gridProperty_t&
	operator= (
		const egd_gridProperty_t&)
 	 = default;


 	/**
 	 * \brief	Move assignment.
 	 *
 	 * Defaulted.
 	 */
 	egd_gridProperty_t&
 	operator= (
 		egd_gridProperty_t&&)
 	 = default;


	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted, produces a useless object.
	 * So do not use it. It is provided for
	 * some internal operations.
	 *
	 * Using it will most likely result in an
	 * exception.
	 */
	egd_gridProperty_t()
	 = default;


	/*
	 * ========================
	 * Private Functions I
	 *
	 * These functions must be declared at the beginning.
	 * The reason is that they can be used only after
	 * they are defined, the reason is the automatic return
	 * type deduction that is needed because of Eigen's
	 * typepolicy.
	 */
private:
	/**
	 * \brief	This function allows to access any row.
	 *
	 * This function is similar to the row(i) function.
	 * But it will not generate an error if a ghost entry
	 * is accessed.
	 *
	 * Note that this function allows the modification of
	 * the row.
	 *
	 * \param  i 		The row that should be accessed.
	 */
	RowXpr_t
	rawRowAccess(
		const Index_t 		i)
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
#if defined(EGD_GRIDPROP_FORCE_ACCESS_CHECK) && EGD_GRIDPROP_FORCE_ACCESS_CHECK != 0
		if((i < 0) || (m_grid.rows() <= i))
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access an invalid row."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed row was " + std::to_string(i) + " but *This only has "
					+ std::to_string(m_grid.rows()) + " many rows.");
		};//End if: check if the size
#else
		pgl_assert(0 <= i, i < m_grid.rows());
#endif

		return m_grid.row(i);
	}; //End: rawRowAccess


	/**
	 * \brief	This function allows to access any row.
	 * 		 Returns a constant view.
	 *
	 * See the non constant version for more informations.
	 *
	 * \param  i 		The row that should be accessed.
	 */
	ConstRowXpr_t
	rawRowAccess(
		const Index_t 		i)
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
#if defined(EGD_GRIDPROP_FORCE_ACCESS_CHECK) && EGD_GRIDPROP_FORCE_ACCESS_CHECK != 0
		if((i < 0) || (m_grid.rows() <= i))
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access an invalid row."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed row was " + std::to_string(i) + " but *This only has "
					+ std::to_string(m_grid.rows()) + " many rows.");
		};//End if: check if the size
#else
		pgl_assert(0 <= i, i < m_grid.rows());
#endif

		return m_grid.row(i);
	}; //End: rawRowAccess


	/**
	 * \brief	This function allows to access any column.
	 *
	 * This function is similar to the col(j) function, but
	 * does not check if the accerssed column is a composed
	 * of ghost nodes.
	 * Out of bound checks are performed by asserts.
	 *
	 * Note the returned object allows the modification
	 * of the requested column.
	 *
	 * \param  j 		The column that should be accessed.
	 */
	ColXpr_t
	rawColAccess(
		const Index_t 		j)
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
#if defined(EGD_GRIDPROP_FORCE_ACCESS_CHECK) && EGD_GRIDPROP_FORCE_ACCESS_CHECK != 0
		if((j < 0) || (m_grid.cols() <= j))
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access an invalid column."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed column was " + std::to_string(j) + " but *This only has "
					+ std::to_string(m_grid.cols()) + " many columns.");
		};//End if: check if the size
#else
		pgl_assert(0 <= j, j < m_grid.cols());
#endif

		return m_grid.col(j);
	}; //End: rawColAccess


	/**
	 * \brief	This function allows read access to any column.
	 *
	 * See the description of the non constant version for more
	 * details on this function.
	 *
	 * \param  j	The index of the column that should be accessed.
	 */
	ConstColXpr_t
	rawColAccess(
		const Index_t 		j)
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
#if defined(EGD_GRIDPROP_FORCE_ACCESS_CHECK) && EGD_GRIDPROP_FORCE_ACCESS_CHECK != 0
		if((j < 0) || (m_grid.cols() <= j))
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access an invalid column."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed column was " + std::to_string(j) + " but *This only has "
					+ std::to_string(m_grid.cols()) + " many columns.");
		};//End if: check if the size
#else
		pgl_assert(0 <= j, j < m_grid.cols());
#endif

		return m_grid.col(j);
	}; //End: rawColAccess


	/**
	 * \brief	This function performs a raw block access.
	 *
	 * A raw block access returns a block expression that has a
	 * certain size, it is like a view of a part of the matrix.
	 * Only out of bounds checkjs are done.
	 *
	 * \param  iStart	Start point; first index.
	 * \param  jStart 	Start point; second index.
	 * \param  nRows	The number of rows.
	 * \param  nCols	The number of cols.
	 */
	BlockXpr_t
	rawBlockAccess(
		const Index_t 		iStart,
		const Index_t 		jStart,
		const Index_t 		nRows,
		const Index_t 		nCols)
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
#if defined(EGD_GRIDPROP_FORCE_ACCESS_CHECK) && EGD_GRIDPROP_FORCE_ACCESS_CHECK != 0
		if( ((nRows <= 0) || (iStart < 0) || ((iStart + nRows) > m_grid.rows()) ) ||
		    ((nCols <= 0) || (jStart < 0) || ((jStart + nCols) > m_grid.cols()) )   )
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access an invalid block."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed block started at (" + std::to_string(iStart) + ", " + std::to_string(jStart) + ")."
					" With length (" + std::to_string(nRows) + ", " + std::to_string(nCols) + ")."
					" But *this is only a " + MASI(m_grid) + " property.");
		};//End if: check if the size
#else
		pgl_assert(0 < nCols, 0 <= jStart, (jStart + nCols) <= m_grid.cols(),
			   0 < nRows, 0 <= iStart, (iStart + nRows) <= m_grid.rows() );
#endif

		return m_grid.block(iStart, jStart, nRows, nCols);
	}; //End: rawBlockAccess


	/**
	 * \brief	This function performs a raw block access.
	 *
	 * This is teh constant version.
	 *
	 * \param  iStart	Start point; first index.
	 * \param  jStart 	Start point; second index.
	 * \param  nRows	The number of rows.
	 * \param  nCols	The number of cols.
	 */
	ConstBlockXpr_t
	rawBlockAccess(
		const Index_t 		iStart,
		const Index_t 		jStart,
		const Index_t 		nRows,
		const Index_t 		nCols)
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
#if defined(EGD_GRIDPROP_FORCE_ACCESS_CHECK) && EGD_GRIDPROP_FORCE_ACCESS_CHECK != 0
		if( ((nRows <= 0) || (iStart < 0) || ((iStart + nRows) > m_grid.rows()) ) ||
		    ((nCols <= 0) || (jStart < 0) || ((jStart + nCols) > m_grid.cols()) )   )
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access an invalid block."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed block started at (" + std::to_string(iStart) + ", " + std::to_string(jStart) + ")."
					" With length (" + std::to_string(nRows) + ", " + std::to_string(nCols) + ")."
					" But *this is only a " + MASI(m_grid) + " property.");
		};//End if: check if the size
#else
		pgl_assert(0 < nCols, 0 <= jStart, (jStart + nCols) <= m_grid.cols(),
			   0 < nRows, 0 <= iStart, (iStart + nRows) <= m_grid.rows() );
#endif

		return m_grid.block(iStart, jStart, nRows, nCols);
	}; //End: rawBlockAccess




	/*
	 * =======================
	 * Functions
	 */
public:
	/**
	 * \brief	This function applys the normalization to *this.
	 *
	 * This function is used to normalize *this. This function should
	 * only be used by interpolating code. It assumes that the grid
	 * weight was processed by egd_processGridWeights().
	 * Some basic checks are performed, but it is the users responsibility,
	 * to ensure consistency.
	 *
	 * \param  gWeight	The processed grid weights.
	 */
	void
	applyNormalization(
		const egd_gridWeight_t& 	gWeight);



	/*
	 * =======================
	 * Status Functions
	 *
	 * See also the "Eigen Adaptors" below.
	 */
public:
	/**
	 * \brief	This function returns the number of basic grig points
	 * 		 in x direction.
	 *
	 * This must not necessaraly be the same then the number of columns,
	 * as reported by cols(). This depends on the grid.
	 */
	Index_t
	Nx()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_Nx;
	};


	/**
	 * \brief	This function returns the number of basic nodal
	 * 		 grid points in y direction.
	 *
	 * This number is not necessaraly the same as the number of rows
	 * that is returned by rows(). The difference depends on the
	 * actual grid.
	 */
	Index_t
	Ny()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_Ny;
	};


	/**
	 * \brief	This function returns the grid type of *this.
	 */
	GridType_t
	getType()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(isValidGridType(m_type));
		return m_type;
	};


	/**
	 * \brief	This function is an alias of the getType() function.
	 *
	 * This alias was made, because this name is much more clearer.
	 */
	GridType_t
	getGridType()
	 const
	 noexcept
	{
		return this->getType();
	};


	/**
	 * \brief	This function returns true if *this has a valid
	 * 		 grid type.
	 *
	 * Note that this function is quite special, because it does not
	 * asserts anything! This means it is safe to call this function
	 * on any instance of the grid property, regardless if it is valid
	 * or not.
	 *
	 * This function has a different name than the function that operates
	 * directly on grid types, the reason is that this would confuse
	 * the compiler.
	 */
	bool
	hasValidGridType()
	 const
	 noexcept
	{
		return egd::isValidGridType(this->m_type);
	};


	/**
	 * \brief	This function returns true if *this
	 * 	   	 is allocated or not.
	 *
	 * This function checks if the sizes are set or not.
	 * Note that it requieres that *this is has a valid
	 * property index.
	 */
	bool
	isAllocated()
	 const
	 noexcept
	{
		pgl_assert(m_pIdx.isValid(),
			   pgl_implies( m_Nx <= 0, m_Nx == 0),
			   pgl_implies( m_Ny <= 0, m_Ny == 0),
			   pgl_ifteTest(m_Nx == 0, m_grid.cols() == 0, m_grid.cols() > 0),
			   pgl_ifteTest(m_Ny == 0, m_grid.rows() == 0, m_grid.rows() > 0),
			   pgl_ifteTest(m_Ny == 0, isValidGridType(m_type) == false, isValidGridType(m_type)) );

		return (m_grid.size() == 0)
			? false
			: true;
	}; //End: is allocated



	/**
	 * \brief	This function allows the conversion of *this into
	 * 		 a grid type.
	 *
	 * This allows for nicer expression.
	 */
	operator GridType_t()
	 const
	 noexcept
	{
		return this->getType();
	};


	/**
	 * \brief	This function returns the property.
	 *
	 * Note, *this can be implicitly converted to a grid type enum.
	 * But It does not support the implicit conversion of *this
	 * into a property index.
	 */
	PropIdx_t
	getPropIdx()
	 const
	 noexcept
	{
		pgl_assert(m_pIdx.isValid()                 ,
			   pgl_implies(m_Nx <= 0, m_Nx == 0),
			   pgl_implies(m_Ny <= 0, m_Ny == 0),
			   pgl_ifteTest(m_Nx == 0, m_grid.cols() == 0, m_grid.cols() > 0),
			   pgl_ifteTest(m_Ny == 0, m_grid.rows() == 0, m_grid.rows() > 0),
			   pgl_ifteTest(m_Ny == 0, isValidGridType(m_type) == false, isValidGridType(m_type)) );

		return m_pIdx;
	};


	/**
	 * \brief	This function returns the representant of *this.
	 *
	 * This function returns the representant of the underliyng grid propery.
	 * This function is provided for easy operations.
	 */
	PropIdx_t
	getRepresentant()
	 const
	 noexcept
	{
		pgl_assert(m_pIdx.isValid()                 ,
			   pgl_implies(m_Nx <= 0, m_Nx == 0),
			   pgl_implies(m_Ny <= 0, m_Ny == 0),
			   pgl_ifteTest(m_Nx == 0, m_grid.cols() == 0, m_grid.cols() > 0),
			   pgl_ifteTest(m_Ny == 0, m_grid.rows() == 0, m_grid.rows() > 0),
			   pgl_ifteTest(m_Ny == 0, isValidGridType(m_type) == false, isValidGridType(m_type)) );

		return m_pIdx.getRepresentant();
	};


	/**
	 * \brief	This function returns true if *this is a represntant.
	 *
	 * This function checks if the underling property index is a preresentant.
	 * Meaning that the property of *this is the same as its representant.
	 */
	bool
	isRepresentant()
	 const
	 noexcept
	{
		pgl_assert(m_pIdx.isValid()                 ,
			   pgl_implies(m_Nx <= 0, m_Nx == 0),
			   pgl_implies(m_Ny <= 0, m_Ny == 0),
			   pgl_ifteTest(m_Nx == 0, m_grid.cols() == 0, m_grid.cols() > 0),
			   pgl_ifteTest(m_Ny == 0, m_grid.rows() == 0, m_grid.rows() > 0),
			   pgl_ifteTest(m_Ny == 0, isValidGridType(m_type) == false, isValidGridType(m_type)) );

		return m_pIdx.isRepresentant();
	};


	/**
	 * \brief	This function returns true if *this and other
	 * 		 have the same representant.
	 *
	 * Note this function is reduced to call the base implementation.
	 *
	 * \param  other	The grid property that we want to examine.
	 */
	bool
	isRepresentedBy(
		const egd_gridProperty_t& 	other)
	 const
	 noexcept
	{
		return this->getPropIdx().isRepresentedBy(other);
	};


	/**
	 * \brief	This function returns true if *this and other
	 * 		 have the same representant.
	 *
	 * This function operates on a property index as argument.
	 *
	 * \param  other	The grid property that we want to examine.
	 */
	bool
	isRepresentedBy(
		const egd_propertyIndex_t& 	other)
	 const
	 noexcept
	{
		return this->getPropIdx().isRepresentedBy(other);
	};


	/**
	 * \brief	This function returns true if *this is a
	 * 		 normal grid.
	 *
	 * A normal grid is, like the original grid.
	 */
	bool
	isRegularGrid()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(isValidGridType     (m_type)         ,
			   isFullyStaggeredGrid(m_type) == false ); //Can not happen, because this is a single property.
		return ::egd::isRegularGrid(m_type);
	};


	/**
	 * \brief	This function returns true if *this is an extended grid.
	 *
	 * Extended grids have more grid points than the original.
	 * They are Ny + 1 times Nx + 1 sized grids.
	 */
	bool
	isExtendedGrid()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(isValidGridType     (m_type)         ,
			   isFullyStaggeredGrid(m_type) == false ); //Can not happen, because this is a single property.
		return ::egd::isExtendedGrid(m_type);
	};


	/**
	 * \brief	Returns true if *this grid is at the
	 * 		 staggered pressure points.
	 *
	 * These are the cell centeres, note that this are not
	 * the true cell centers, becuase it also involves
	 * some ghost nodes. Depending on the grid they could be
	 * at different locations.
	 */
	bool
	onStPresseurePoints()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(isValidGridType     (m_type)         ,
			   isFullyStaggeredGrid(m_type) == false ); //Can not happen, because this is a single property.
		return ::egd::isPressurePoint(m_type);
	};


	/**
	 * \brief	This function returns true if *this grid
	 * 		 is located at the staggered Vx points.
	 *
	 * See also comment about the pressure points.
	 */
	bool
	onStVxPoints()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(isValidGridType     (m_type)         ,
			   isFullyStaggeredGrid(m_type) == false ); //Can not happen, because this is a single property.
		return ::egd::isVxPoint(m_type);
	};


	/**
	 * \brief	Returns true if *this grid is located
	 * 		 at the staggered Vy points.
	 *
	 * See comment about the pressure points.
	 */
	bool
	onStVyPoints()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(isValidGridType     (m_type)         ,
			   isFullyStaggeredGrid(m_type) == false ); //Can not happen, because this is a single property.
		return ::egd::isVyPoint(m_type);
	};


	/**
	 * \brief	This function returns true if *this grid
	 * 		 is located at the cell center points.
	 *
	 * Note that such a grid is only usefull for regular grid.
	 * They are provided for compability. They have the size
	 * of the basic nodal grid, but only indexes that are greater
	 * than zeros are usefull. Thus the first usefull entry
	 * in this property is (1, 1), which referes to the top
	 * left cell center, that is inside the domain.
	 */
	bool
	onCellCentrePoints()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(isValidGridType     (m_type)         ,
			   isFullyStaggeredGrid(m_type) == false ); //Can not happen, because this is a single property.
		return ::egd::isCellCentrePoint(m_type);
	};


	/**
	 * \brief	This function retuurns true if *this is a
	 * 		 property defined on the basic nodal grid points.
	 *
	 * This is lie the normal case.
	 */
	bool
	onBasicNodalPoints()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(isValidGridType     (m_type)         ,
			   isFullyStaggeredGrid(m_type) == false ); //Can not happen, because this is a single property.
		return ::egd::isBasicNodePoint(m_type);
	};


	/**
	 * \brief	This function checks if *this is valid.
	 *
	 * This function only checks if the size of *this is okay.
	 * And does agree with what it should be. It does not
	 * perform a ceck of value, meaning *this can only hold
	 * NANs and this function does not care.
	 */
	bool
	checkSize()
	 const
	 noexcept;


	/**
	 * \brief	This function checks if *this and other are defined
	 * 		 on the _same_ grid.
	 *
	 * This chack involves the grid type but also the size of the basic grid.
	 * Note if the checkSize() function on either grid returns false then
	 * this function returns false.
	 *
	 * \param  other	The other grid.
	 */
	bool
	checkOnSameGrid(
		const egd_gridProperty_t& 	other)
	 const
	 noexcept;



	/*
	 * ========================
	 * Eigen Adaptor
	 *
	 * These functions are provided to mimic the behaviour of Eigen types.
	 * They basically forward the request to the wrapped property.
	 *
	 */
public:
	/**
	 * \brief	This function returns the number of rows.
	 *
	 * This is not necessaraly the number that was used for construction.
	 * It is the real thing.
	 */
	Index_t
	rows()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid.rows();
	}; //End: rows


	/**
	 * \brief	Return the number of columns of the property.
	 *
	 * This is the real size of the matrix and not the number of basic
	 * nodal poionst in x direction.
	 */
	Index_t
	cols()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid.cols();
	};


	/**
	 * \brief	This returns the full size of *this.
	 *
	 * This is the product of rows() and cols(), it is not
	 * the number of the underlying basic grid, but the number
	 * ob entries in teh underlying matrix.
	 *
	 * Also note that this function can return zero, it is the
	 * only size related function that does it. In that case
	 * it means that the property is not allocated.
	 */
	Index_t
	size()
	 const
	 noexcept
	{
		pgl_assert(this->m_pIdx.isValid()           ,
			   pgl_implies(m_Nx <= 0, m_Nx == 0),
			   pgl_implies(m_Ny <= 0, m_Ny == 0),
			   pgl_ifteTest(m_Nx == 0, m_grid.cols() == 0, m_grid.cols() > 0),
			   pgl_ifteTest(m_Ny == 0, m_grid.rows() == 0, m_grid.rows() > 0),
			   pgl_ifteTest(m_Ny == 0, isValidGridType(m_type) == false, isValidGridType(m_type)) );

		return m_grid.size();		//Call directly to avoid an assertion
	};


	/**
	 * \brief	This function returns an expression for the ith row.
	 *
	 * This function checks if the requested ro is composed entierly
	 * out of ghost nodes. In that case an error is generated.
	 *
	 * Note that the returned object allows modifications of the row.
	 *
	 * \param  i 	The row that should be returned.
	 */
	RowXpr_t
	row(
		const Index_t 		i)
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 <= i, i < m_grid.rows());

		if(GridGeometry_t::isGhostNodeRowIdx(i, yNodeIdx_t(m_Ny), m_type) == true)
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access a ghost row."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed row was " + std::to_string(i) + ".");
		}; //End if: check if ghost row

		return this->rawRowAccess(i);
	}; //End: row accessing


	/**
	 * \brief	This function returns a constant expression
	 * 		 of the ith row.
	 *
	 * This function checks if the requested ro is composed entierly
	 * out of ghost nodes. In that case an error is generated.
	 *
	 * \param  i 	The row that should be returned.
	 */
	ConstRowXpr_t
	row(
		const Index_t 		i)
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 <= i, i < m_grid.rows());

		if(GridGeometry_t::isGhostNodeRowIdx(i, yNodeIdx_t(m_Ny), m_type) == true)
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access a ghost row."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed row was " + std::to_string(i) + ".");
		}; //End if: check if ghost row

		return rawRowAccess(i);
	}; //End: Accessing row


	/**
	 * \brief	This function returns an expression of the ith column.
	 *
	 * This function checks if the requested column consists entierly out of
	 * ghost nodes, if so an error is generated.
	 *
	 * Note that the returned object allows the modification of the
	 * returned column.
	 *
	 * \param  j 	The column that should be returned.
	 */
	ColXpr_t
	col(
		const Index_t 		j)
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 <= j, j < m_grid.cols());

		if(GridGeometry_t::isGhostNodeColIdx(j, xNodeIdx_t(m_Nx), m_type) == true)
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access a ghost column."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed column was " + std::to_string(j) + ".");
		}; //End if: check if ghost row

		return this->rawColAccess(j);
	};//End: accessing col


	/**
	 * \brief	This function returns a constant expression
	 * 		 of the ith column.
	 *
	 * This function checks if the requested column consists entierly out of
	 * ghost nodes, if so an error is generated.
	 *
	 * \param  j 	The column that should be returned.
	 */
	ConstColXpr_t
	col(
		const Index_t 		j)
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 <= j, j < m_grid.cols());

		if(GridGeometry_t::isGhostNodeColIdx(j, xNodeIdx_t(m_Nx), m_type) == true)
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access a ghost column."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed column was " + std::to_string(j) + ".");
		}; //End if: check if ghost row

		return rawColAccess(j);
	}; //End: accessing col


	/**
	 * \brief	This function returns a pointer to the
	 * 		 begin of the memeory range of *this.
	 *
	 * This essentially allows access the internal memeory.
	 * Handle with care. This function returns a constant
	 * pointer.
	 */
	const Numeric_t*
	data()
	 const
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols(),
			   this->hasValidGridType()            ,
			   m_grid.data() != nullptr             );

		return m_grid.data();
	};//End: get pointer to memory


	/**
	 * \brief	This function allows to access a block.
	 *
	 * This function does not allow to accesses blocks that
	 * contains ghost rows
	 *
	 * \param  iStart	Start point; first index.
	 * \param  jStart 	Start point; second index.
	 * \param  nRows	The number of rows.
	 * \param  nCols	The number of cols.
	 */
	BlockXpr_t
	block(
		const Index_t 		iStart,
		const Index_t 		jStart,
		const Index_t 		nRows,
		const Index_t 		nCols)
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 < nCols, 0 <= jStart, (jStart + nCols) <= m_grid.cols(),
			   0 < nRows, 0 <= iStart, (iStart + nRows) <= m_grid.rows() );

		//Calculate the bottom right index that will be returned by the block
		const Index_t iLast = (iStart + nRows) - 1;
		const Index_t jLast = (jStart + nCols) - 1;

		if(GridGeometry_t::isGhostNode(iStart, jStart, this->m_type, yNodeIdx_t(this->m_Ny), xNodeIdx_t(this->m_Nx)) ||
		   GridGeometry_t::isGhostNode(iLast , jLast , this->m_type, yNodeIdx_t(this->m_Ny), xNodeIdx_t(this->m_Nx))   )
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access a ghost node.");
		};

		return rawBlockAccess(iStart, jStart, nRows, nCols);
	}; //End: block access


	/**
	 * \brief	This function allows to access a block.
	 *
	 * This function does not allow to accesses blocks that
	 * contains ghost rows.
	 * This is the constant version.
	 *
	 * \param  iStart	Start point; first index.
	 * \param  jStart 	Start point; second index.
	 * \param  nRows	The number of rows.
	 * \param  nCols	The number of cols.
	 */
	ConstBlockXpr_t
	block(
		const Index_t 		iStart,
		const Index_t 		jStart,
		const Index_t 		nRows,
		const Index_t 		nCols)
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 < nCols, 0 <= jStart, (jStart + nCols) <= m_grid.cols(),
			   0 < nRows, 0 <= iStart, (iStart + nRows) <= m_grid.rows() );

		//Calculate the bottom right index that will be returned by the block
		const Index_t iLast = (iStart + nRows) - 1;
		const Index_t jLast = (jStart + nCols) - 1;

		if(GridGeometry_t::isGhostNode(iStart, jStart, this->m_type, yNodeIdx_t(this->m_Ny), xNodeIdx_t(this->m_Nx)) ||
		   GridGeometry_t::isGhostNode(iLast , jLast , this->m_type, yNodeIdx_t(this->m_Ny), xNodeIdx_t(this->m_Nx))   )
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access a ghost node.");
		};

		return rawBlockAccess(iStart, jStart, nRows, nCols);
	}; //End: block access


	/**
	 * \brief	This function returns the maximum block view
	 * 		 That can be accessed by *this.
	 */
	BlockXpr_t
	maxView()
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );

		const auto rowView = egd_gridGeometry_t::getBlockSizeY(m_type, yNodeIdx_t(m_Ny));
		const auto colView = egd_gridGeometry_t::getBlockSizeX(m_type, xNodeIdx_t(m_Nx));

		return block(rowView.first, colView.first, rowView.second, colView.second);
	}; //End: maxView


	/**
	 * \brief	This function returns the maximum block view
	 * 		 That can be accessed by *this.
	 *
	 * This is the constant version.
	 */
	ConstBlockXpr_t
	maxView()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );

		const auto rowView = egd_gridGeometry_t::getBlockSizeY(m_type, yNodeIdx_t(m_Ny));
		const auto colView = egd_gridGeometry_t::getBlockSizeX(m_type, xNodeIdx_t(m_Nx));

		return block(rowView.first, colView.first, rowView.second, colView.second);
	}; //End: maxView


	/**
	 * \brief	This function allows write access to
	 * 		 the underling the property array.
	 *
	 * This function returns a mutable reference.
	 *
	 * In the case the requested entry corresponds to a
	 * ghost an error is generated.
	 * If you need to access also these entries use the
	 * rawAccess() function, but these functions are
	 * private, so you have to make the code that uses
	 * them a firend of thsi class.
	 *
	 * \param  i	The first index; Y direction.
	 * \param  j 	The second index; X direction.
	 */
	Numeric_t&
	operator() (
		const Index_t 		i,
		const Index_t 		j)
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 <= i, i < m_grid.rows());
		pgl_assert(0 <= j, j < m_grid.cols());

		if(GridGeometry_t::isGhostNode(i, j, this->m_type, yNodeIdx_t(this->m_Ny), xNodeIdx_t(this->m_Nx)) == true)
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access a ghost node."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed index was (" + std::to_string(i) + ", " + std::to_string(j) + ").");
		}; //End if: check if ghost node was accessed.

		//Use the internal access function
		return this->rawAccess(i, j);
	}; //End: access entry, mutable


	/**
	 * \brief	This function allows read access to the
	 * 		 property array.
	 *
	 * This is the constant version. it returns a copy
	 * of the value in the requested entry, this is the
	 * same that Eigen does.
	 *
	 * In the case the requested entry corresponds to a
	 * ghost an error is generated.
	 * If you need to access also these entries use the
	 * rawAccess() function, but these functions are
	 * private, so you have to make the code that uses
	 * them a firend of thsi class.
	 *
	 * \param  i	The first index; Y direction.
	 * \param  j 	The second index; X direction.
	 */
	Numeric_t
	operator() (
		const Index_t 		i,
		const Index_t 		j)
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 <= i, i < m_grid.rows());
		pgl_assert(0 <= j, j < m_grid.cols());

		if(GridGeometry_t::isGhostNode(i, j, this->m_type, yNodeIdx_t(this->m_Ny), xNodeIdx_t(this->m_Nx)) == true)
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access a ghost node."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed index was (" + std::to_string(i) + ", " + std::to_string(j) + ").");
		}; //End if: check if ghost node was accessed.

		//Use the internal access function
		return this->rawAccess(i, j);
	}; //End: access entry, constant


	/**
	 * \breief	Returns the maximal coef in the property.
	 *
	 * Note that this function simply call maxCoeff() of the
	 * full stored properties, including the ghost nodes.
	 * Also no absolute value is applied.
	 */
	Numeric_t
	maxCoeff()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid.maxCoeff();
	};


	/**
	 * \brief	This function returns the maximum of the absolute values of *this.
	 *
	 * First the absolute of the *thi is generated, then the largest value is searched.
	 * This is a common task in EGD so a dedicated function is finaly added.
	 *
	 * Note that this function also considers the ghost nodes.
	 */
	Numeric_t
	getAbsMaxCoeff()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols(),
			   m_pIdx.isValid(),
			   ::egd::isValidGridType(m_type)       );

		return m_grid.abs().maxCoeff();
	};


	/**
	 * \brief	Returns the minimal coefficient in the property.
	 *
	 * Note that this function simply call minCoeff() of the
	 * full stored properties, including the ghost nodes.
	 * Also no absolute value is applied.
	 */
	Numeric_t
	minCoeff()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid.minCoeff();
	};


	/**
	 * \brief	This function returns the minimum of the absolute values of *this.
	 *
	 * First the absolute of the *thi is generated, then the smallest value is searched.
	 * This is a common task in EGD so a dedicated function is finaly added.
	 *
	 * Note that this function also considers the ghost nodes.
	 */
	Numeric_t
	getAbsMinCoeff()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols(),
			   m_pIdx.isValid(),
			   ::egd::isValidGridType(m_type)       );

		return m_grid.abs().minCoeff();
	};


	/**
	 * \brief	Return an Eigen expression that encodes the
	 * 		 absolute value of the property.
	 *
	 * This includeds the ghost nodes.
	 *
	 */
	const AbsReturnType_t
	abs()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid.abs();
	};


	/**
	 * \brief	Returns a constant reference to the array.
	 *
	 * This function is named array() like teh one in Eigen.
	 * If you need a mutable reference then use getBaseArray() function.
	 * Remember that this function is private.
	 */
	const BaseGridProperty_t&
	array()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid;
	};


	/**
	 * \brief	This function allows the implicit convertion in a
	 * 		 constant reference to an array.
	 */
	operator const BaseGridProperty_t&()
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid;
	};


#if defined(EGD_GRIDPROP_ALLOW_MUTABLE) && EGD_GRIDPROP_ALLOW_MUTABLE != 0
	/**
	 * \brief	This function allowes the explicit convertion into
	 * 		 a reference to the underling propety.
	 *
	 * In contrast to the constant reference convertion, this constructor
	 * is markerd explicit.
	 */
	explicit
	operator BaseGridProperty_t&()
	 noexcept
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid;
	};
#endif


#if defined(EGD_GRIDPROP_ALLOW_MUTABLE) && EGD_GRIDPROP_ALLOW_MUTABLE != 0
	/**
	 * \brief	This returns the property array, as a matrix wrapper.
	 */
	MatrixWrapper_t
	matrix()
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid.matrix();
	};
#endif


	/**
	 * \brief	This returns a constant matric wrapper.
	 */
	constMatrixWrapper_t
	matrix()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid.matrix();
	};


	/**
	 * \brief	This function tests if all coefficient inside *this
	 * 		 are finite, not infinity and not nan.
	 *
	 * Note that this function is a little bitt different from the
	 * Eigen version. Where a coefficientwise test is done. This
	 * function will return a bool that indicates if all are finite.
	 * Because this is the only way it is used anyway.
	 *
	 * This function will also tests the ghost nodes.
	 */
	bool
	isFinite()
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid.isFinite().all();
	};


	/**
	 * \brief	This function allows to set all coefficients of *this
	 * 		 to zero, this includes the ghost nodes.
	 *
	 * Note that unlike the Eigen function, thus function does not accepts
	 * size arguments
	 */
	egd_gridProperty_t&
	setZero()
	{
		return this->setConstantAll(0.0);
	};


	/**
	 * \brief	This function sets all non ghost nodes
	 * 		 to the specified value.
	 *
	 * This function sets the accessable values of this to
	 * the given value val, note that this does not include
	 * the ghost nodes. This function is like calling the
	 * setConstant() function on the returned block of the
	 * maxView() function.
	 *
	 * If you want to set all coefficients to a certain value,
	 * including the ghost nodes than use the setConstantAll()
	 * function.
	 *
	 * \param  val	The value that should be used.
	 */
	egd_gridProperty_t&
	setConstant(
		const Numeric_t 	val)
	{
		pgl_assert(this->hasValidGridType(),
			   this->m_pIdx.isValid(),
			   this->m_Nx > 0, this->m_Ny > 0);

		if(::pgl::isValidFloat(val) == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid argument.");
		};

		//Set the value
		this->maxView().setConstant(val);

		return *this;
	};//End: setConstant


	/**
	 * \brief	This function sets all coeffient to a
	 * 		 certain value, including the ghost nodes.
	 *
	 * This function operates on the whole internal array, this
	 * is in contrast to the setConstant() function that only
	 * operates on the non ghost nodes.
	 *
	 * \param  val		The value that should be used.
	 */
	egd_gridProperty_t&
	setConstantAll(
		const Numeric_t 	val)
	{
		pgl_assert(this->hasValidGridType(),
			   this->m_pIdx.isValid(),
			   this->m_Nx > 0, this->m_Ny > 0);

		if(::pgl::isValidFloat(val) == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid argument.");
		};

		//Set the value
		this->m_grid.setConstant(val);

		return *this;
	};//End: setConstantAll






	/*
	 * ==========================
	 * Size Managing Functions
	 */
public:
	/**
	 * \brief	This function resizes *this.
	 *
	 * Despite its name it is impossible to change the size
	 * of *this with this function. As argument the function needs
	 * the number of basic nodal grid points. If this number is not
	 * the correct ones an error is generated.
	 * It will then calculate the sizes it should have and allocate
	 * the memory.
	 *
	 * Note that the order is swaped to comply with the matrix notation.
	 *
	 * \param  Ny		The number of basic grid points in y direction.
	 * \param  Nx 		The number of basic grid points in x direction.
	 */
	void
	resize(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx);


	/**
	 * \brief	This function performs the binding operation.
	 *
	 * If *this is not yet allocated, meaning it was constructed,
	 * with the default constructor, this function allows to
	 * do the actuall construction. Note that this function
	 * only works if *this is not yet allocated, and no size
	 * was set.
	 *
	 * Note that this function has a very internal touch.
	 * If you are using it, you are most likely using EGD
	 * wrong.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  gType	The type of the grid that we mimic (only location).
	 */
	void
	allocateGrid(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const GridType_t 	gType);


	/**
	 * \brief	This function performs the allocation of the grid.
	 *
	 * This function checks if the griven property is valid and the
	 * same as *this. if so the arguments are forwarded to the
	 * allocation function with three arguments.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  gType	The type of the grid that we mimic (only location).
	 * \param  prop		The property *this has.
	 *
	 * \note	This is a compatibility feature.
	 */
	void
	allocateGrid(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const GridType_t 	gType,
		const PropIdx_t 	prop);


	/**
	 * \brief	Thsi function performs creates an extended
	 * 		 version of *this.
	 *
	 * *thsi is defined on a regular sized grid, so this function
	 * will create an grid property that is on the same grid type
	 * than *this, but on an extended grid. The content is copied
	 * from *this. Since the top left point remains stable, through
	 * all the different size, this is used as anker, also note
	 * that this function does not care about ghost nodes.
	 *
	 * Note that this function returns the new object and *this
	 * is not modified.
	 *
	 * This function is implemented in teh same file as the constructors
	 * are.
	 *
	 * In the case *this is an extended grid already *this is copied.
	 */
	egd_gridProperty_t
	copyToExtendedGrid()
	 const;


	/**
	 * \brief	This function extends *this to its extended
	 * 		 version of itself.
	 *
	 * This function is basically the same as the copyToExtendedGrid()
	 * function, but operates inplace, but a memory allocation is needed.
	 * If *This is already extended nothing will be done.
	 *
	 * The function returns a reference to the manipulated argument.
	 *
	 * \param  gProp	The property that should be extended.
	 */
	static
	egd_gridProperty_t&
	EXTEND_INPLACE(
		egd_gridProperty_t* const 	gProp);




	/*
	 * =======================
	 * Private Functions
	 */
private:
	/**
	 * \brief	This function returns the underlying array.
	 *
	 * This fucntion returns a reference to the underlying array.
	 * It is not named array(), to enforce some consideration form
	 * the user. Do not mess up with this function.
	 */
	BaseGridProperty_t&
	getBaseArray()
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		return m_grid;
	};


	/**
	 * \brief	Access the underling array, without a checking if
	 * 		 for the cell central case.
	 *
	 * This function allows for accessing the underling array directly.
	 * This means it allows to access any entry without restricting
	 * the access to ghost nodes.
	 * Asserts are used to check if an out of bound accesses happens.
	 * If they are deactivated no checks are done.
	 *
	 * \param  i 		First index.
	 * \param  j 		Second index.
	 */
	Numeric_t
	rawAccess(
		const Index_t 	i,
		const Index_t 	j)
	 const
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
#if defined(EGD_GRIDPROP_FORCE_ACCESS_CHECK) && EGD_GRIDPROP_FORCE_ACCESS_CHECK != 0
		if(((i < 0) || (m_grid.rows() <= i)) ||		//Check rows
		   ((j < 0) || (m_grid.cols() <= j))   )	//Check columns
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access an invalid entry."
					" The accessed property was " + m_pIdx.print() + "."
					" The grid type was " + std::to_string(m_type) + "."
					" And the accessed index was (" + std::to_string(i) + ", " + std::to_string(j) + ").");
		};
#else
		pgl_assert(0 <= i, i < m_grid.rows(),
			   0 <= j, j < m_grid.cols() );
#endif
		return m_grid(i, j);
	}; //End: raw access


	/**
	 * \brief	This is the non constant version of the rawAccess function.
	 *
	 * For more information see the constant version of this function.
	 *
	 * \param  i 	First index.
	 * \param  j  	Second index.
	 */
	Numeric_t&
	rawAccess(
		const Index_t 	i,
		const Index_t 	j)
	{
		pgl_assert(0 < m_Ny         , 0 < m_Nx         ,
			   0 < m_grid.rows(), 0 < m_grid.cols() );
		pgl_assert(0 <= i, i < m_grid.rows(),
			   0 <= j, j < m_grid.cols() );

		return m_grid(i, j);
	}; //End: rawAccess




	/*
	 * ========================
	 * Friends
	 */
private:
	friend
	void
	egd_setGhostNodesToZero(
		egd_gridProperty_t* const 	gProp);


	template<
		bool 		isExtended,
		bool 		isModifiedWeights
	>
	friend
	void
	egdInternal_basicGridToMarker(
		egd_gridProperty_t* const 		gProperty_,
		const egd_markerProperty_t& 		mProperty,
		const egd_markerWeight_t& 		mWeights,
		const egd_gridWeight_t& 		gWeights,
		const egd_indexVector_t& 		idxI,
		const egd_indexVector_t& 		idxJ,
		const egd_markerProperty_t* const 	modWeights,
		const bool 				reapplyDisableGhostNode);


	template<
		bool 	useModWeights
	>
	friend
	void
	egdInternal_basicGridToMarkerLocal(
		egd_gridProperty_t* const 		gProperty_,
		const egd_markerProperty_t& 		mProperty,
		const egd_markerWeight_t& 		mWeights,
		const egd_gridWeight_t& 		gWeights,
		const egd_indexVector_t& 		idxI,
		const egd_indexVector_t& 		idxJ,
		const egd_localAssocVector_t& 		locAssoc,
		const egd_markerProperty_t* const 	modWeights,
		const bool 				reapplyDisableGhostNode);


	template<
		bool 	isExtended,
		bool 	updateMarker
	>
	friend
	void
	egdInternal_pullMarkerBackToGrid(
		egd_markerProperty_t* const 		mProperty_,
		const egd_gridProperty_t& 		gProperty,
		const egd_markerWeight_t& 		mWeights,
		const egd_indexVector_t& 		idxI,
		const egd_indexVector_t& 		idxJ,
		const bool 				updateMarkerArg);


	//It is not so nice to make the general solver a friend, but it
	//is the easiest thing
	friend
	class egd_generalSolverImpl_t;



	/*
	 * ========================
	 * Private Member
	 */
private:
	BaseGridProperty_t 		m_grid;		//!< This is the storage of the property.
	Index_t 			m_Ny = 0;	//!< This is the number of real basic nodal points, in y direction.
	Index_t 			m_Nx = 0;	//!< This is the number of real basic nodal points, in x direction.
	GridType_t 			m_type = GridType_t::INVALID;	//!< This is the type of the grid that this mimiks.
	PropIdx_t 			m_pIdx = eMarkerProp::NONE;	//!< This is the property, also what is stored.
}; //End class(egd_gridProperty_t)



inline
std::string
egd_printMatrixSize(
	const egd_gridProperty_t& 	gp)
{
	if(gp.isAllocated() == false)
	{
		return std::string("0x0");
	};
	return ::egd::egd_printMatrixSize(gp.array());
}; //End: print size



PGL_NS_END(egd)






