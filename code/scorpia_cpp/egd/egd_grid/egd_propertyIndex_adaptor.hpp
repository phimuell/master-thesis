#pragma once
/**
 * \brief	This file implements a grir index adaptor.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include BOOST
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/stringize.hpp>


//Include STD
#include <iterator>
#include <string>
#include <iostream>
#include <functional>

PGL_NS_START(egd)


/**
 * \brief	This class is an adaptor that wraps a grid property index.
 * \class	egd_gridpropertyIndexAdaptor_t
 *
 * This class wraps a grid property, its main feature is that it is able to be
 * default constructable. Which leads to an invalid object. It supports the
 * conversions to the index property.
 *
 * You should only use them inside containers when a default constructible
 * index is needed.
 *
 */
class egd_propertyIndexAdaptor_t
{
	/*
	 * ====================
	 * Type defs
	 */
public:
	using PropIdx_t 	= egd_propertyIndex_t;			//!< The wrapped type
	/*
	 * ===============
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This constructor creats an egd_propertyIndexAdaptor_t object out of an enum.
	 * This function checks if the vlue passed was correct.
	 *
	 * \param  markerProp		The marker property.
	 *
	 * \throw 	If *this is invalid.
	 */
	egd_propertyIndexAdaptor_t(
		const eMarkerProp& 	markerProp)
	 :
	  m_pIdx(markerProp)
	{
		//Perform the checking
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed marker property enum was invalid.");
		};
	}; //End: builing constructor to user


	/**
	 * \brief	Default constructor.
	 *
	 * Construct an invalid object
	 */
	egd_propertyIndexAdaptor_t()
	 noexcept
	 :
	  m_pIdx(eMarkerProp::NONE, 1)
	{
		pgl_assert(this->isValid() == false);
	}; //End: default


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_propertyIndexAdaptor_t(
		const egd_propertyIndexAdaptor_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_propertyIndexAdaptor_t&
	operator= (
		const egd_propertyIndexAdaptor_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	constexpr
	egd_propertyIndexAdaptor_t(
		egd_propertyIndexAdaptor_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_propertyIndexAdaptor_t&
	operator= (
		egd_propertyIndexAdaptor_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_propertyIndexAdaptor_t()
	 noexcept
	 = default;


	/*
	 * =========================
	 * Building from a property index.
	 *
	 * These functions allows *this to be buildied
	 * directly from a property index.
	 */
public:
	/**
	 * \brief	Copy constructor from a index
	 */
	egd_propertyIndexAdaptor_t(
		const egd_propertyIndex_t& o)
	 :
	  m_pIdx(o)
	{
		if(o.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Can not construct from an invalid index.");
		};
	};


	/**
	 * \brief	Copy assignment from an index
	 */
	egd_propertyIndexAdaptor_t&
	operator= (
		const egd_propertyIndex_t& o)
	{
		if(o.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Can not construct from an invalid index.");
		};
		this->m_pIdx = o;
		return *this;
	};


	/**
	 * \brief	Move constructor from an index
	 */
	egd_propertyIndexAdaptor_t(
		const egd_propertyIndex_t&& o)
	 :
	  m_pIdx(o)
	{
		if(o.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Can not construct from an invalid index.");
		};
	};


	/**
	 * \brief	Move assignment from an index
	 */
	egd_propertyIndexAdaptor_t&
	operator= (
		const egd_propertyIndex_t&& o)
	{
		if(o.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Can not construct from an invalid index.");
		};
		this->m_pIdx = o;
		return *this;
	};



	/*
	 * =========================
	 * Functions
	 *
	 * Here are some functions that mimik the property index
	 */
public:
	/**
	 * \brief	Returns true if *this is valid.
	 *
	 * This forwards the result of the wrapped index.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		return this->m_pIdx.isValid();
	}; //End: isValid


	/**
	 * \brief	This function calls the print function.
	 *
	 * This will call the print function of the wrapped index.
	 * Note that it is save to call this function even on invalid objects.
	 */
	std::string
	print()
	 const
	{
		return m_pIdx.print();
	};


	/**
	 * \brief	This function returns the underling enum.
	 *
	 * Note that this function throws if *this is invalid.
	 * Also note that this function does not return the wraped index
	 * but the hard core low level enum.
	 * If you want the index use the conversion functions.
	 */
	eMarkerProp
	getProp()
	 const
	{
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("The addaptor still wraps an invalid object.");
		};

		return m_pIdx.getProp();
	}; // End: getProp


	/**
	 * \brief	This function returns the representant of *this.
	 *
	 * Note this function throws if *this is invalid.
	 */
	egd_propertyIndex_t
	getRepresentant()
	 const
	{
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("The addaptor still wraps an invalid object.");
		};

		return m_pIdx.getRepresentant();
	}; //End: getRep


	/**
	 * \brief	This function returns ture if *this represents itself.
	 *
	 * Thsi function will call the isRepresent() function of the wrapped
	 * property index. If *This is invalid an exception is enerated.
	 */
	bool
	isRepresentant()
	 const
	{
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("The addaptor still wraps an invalid object.");
		};

		return m_pIdx.isRepresentant();
	}; //End: getRep


	/*
	 * ===========================
	 * Conversions functions
	 *
	 * This function allows the transformation of *this
	 * into its wrapped index. Note that no mutable reference
	 * is supported, use the assignment interface if you want
	 * to change its value.
	 */
public:
	/**
	 * \brief	Returns a constant reference to the wrapped type.
	 */
	const PropIdx_t&
	get()
	 const
	{
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("The addaptor still wraps an invalid object.");
		};

		return m_pIdx;
	}; //ENd: get const ref



	/*
	 * ==========================
	 * Operators
	 *
	 * For safty and convinience we only allow equal tests.
	 * Note that compaing addaptors with each other is save
	 * but not with raw index
	 */
public:
	/**
	 * \brief	Test if *this and rhs is the same.
	 *
	 * No exceotion is generated.
	 */
	bool
	operator== (
		const egd_propertyIndexAdaptor_t& 	rhs)
	 const
	 noexcept
	{
		return (this->m_pIdx == rhs.m_pIdx);
	};


	/**
	 * \brief	Test if *This and rhs are not the same.
	 *
	 * No exception is generated.
	 */
	bool
	operator!= (
		const egd_propertyIndexAdaptor_t&	rhs)
	 const
	 noexcept
	{
		return (this->m_pIdx != rhs.m_pIdx);
	};



	/*
	 * =========================
	 * Private Member
	 */
private:
	PropIdx_t 		m_pIdx;		//!< This is the wrapped index, can be invalid.
}; //End class(egd_propertyIndexAdaptor_t)


/**
 * \brief 	This prints the egd_propertyIndex_t to a stream.
 *
 * \param  o	This is the stream
 * \param  p	This is the property
 */
inline
std::ostream&
operator<< (
	std::ostream&					o,
	const ::egd::egd_propertyIndexAdaptor_t&	p)
{
	o << p.print();

	return o;
}; //End: outstream operator


/*
 * Free functions compare functions
 *
 * All equal/unequal compare functions are reduced to the
 * one provided by the real index function, so a get is used
 * to aquire them. Thus makes it not possible to compare agains
 * a non initailazed adaptor.
 */

inline
bool
operator== (
	const egd_propertyIndex_t& 		lhs,
	const egd_propertyIndexAdaptor_t&	rhs)
{
	return (lhs == rhs.get());
};


inline
bool
operator== (
	const egd_propertyIndexAdaptor_t& 		lhs,
	const egd_propertyIndex_t&	rhs)
{
	return (lhs.get() == rhs);
};


inline
bool
operator!= (
	const egd_propertyIndex_t& 		lhs,
	const egd_propertyIndexAdaptor_t&	rhs)
{
	return (lhs != rhs.get());
};


inline
bool
operator!= (
	const egd_propertyIndexAdaptor_t& 		lhs,
	const egd_propertyIndex_t&	rhs)
{
	return (lhs.get() != rhs);
};


/*
 * Less operators
 * They are deleted.
 */
inline
bool
operator< (
	const egd_propertyIndex_t&,
	const egd_propertyIndexAdaptor_t&)
 = delete;


inline
bool
operator< (
	const egd_propertyIndexAdaptor_t&,
	const egd_propertyIndex_t&)
 = delete;

PGL_NS_END(egd)




PGL_NS_START(std)

/**
 * \brief	This is a specializattion of the hash object for the index adaptor type.
 *
 * This is an empyt struct, so it is not possible to hash it.
 */
template<>
struct hash<::egd::egd_propertyIndexAdaptor_t>
{
}; //End struct(hash)

PGL_NS_END(std)

