/**
 * \brief	This file contains some miuscelious functions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD


PGL_NS_START(egd)


bool
egd_gridProperty_t::checkSize()
 const
 noexcept
{
	if(m_Nx <= 0)
	{
		pgl_assert(false && "Nx too small.");
		return false;
	};
	if(m_Ny <= 0)
	{
		pgl_assert(false && "Ny too small.");
		return false;
	};
	if(egd::isValidGridType(m_type) == false)
	{
		pgl_assert(false && "type is invalid.");
		return false;
	};

	const Index_t sollRow = GridGeometry_t::getNGridPointsY(yNodeIdx_t(m_Ny), m_type);
	const Index_t sollCol = GridGeometry_t::getNGridPointsX(xNodeIdx_t(m_Nx), m_type);
		pgl_assert(sollCol > 0,
			   sollRow > 0 );

	if(sollRow != m_grid.rows())
	{
		pgl_assert(false && "Wrong row size.");
		return false;
	};
	if(sollCol != m_grid.cols())
	{
		pgl_assert(false && "Wrong col size.");
		return false;
	};

	return true;
}; //End: checksize


void
egd_gridProperty_t::applyNormalization(
	const egd_gridWeight_t& 	gWeight)
{
	if(this->checkSize() == false)
	{
		throw PGL_EXCEPT_RUNTIME("A size error was detected.");
	}
	if(gWeight.rows() != this->rows() ||
	   gWeight.cols() != this->cols()   )
	{
		throw PGL_EXCEPT_InvArg("The grid weight parameter that was passed to the normalizer function"
				" has the wrong dimensions, it is " + MASI(gWeight) + ", but *this is " + MASI(*this) + ".");
	}; //End if: check sizes

	//Make some optional tests
	pgl_assert(this->isFinite(),
		   gWeight.isFinite().all());

	//
	//Apply the normalization
	this->m_grid *= gWeight;

	//Test again
	pgl_assert(this->isFinite());

	return;
}; //End: applyNormalization



bool
egd_gridProperty_t::checkOnSameGrid(
	const egd_gridProperty_t&	other)
 const
 noexcept
{
	if(this->checkSize() == false ||
	   other.checkSize() == false   )
	{
		return false;
	};

	if(this->hasValidGridType() == false ||
	   other.hasValidGridType() == false   )
	{
		return false;
	};

	if(this->getType() != other.getType())
	{
		return false;
	};

	if(this->m_Nx != other.m_Nx ||
	   this->m_Ny != other.m_Ny   )
	{
		return false;
	};

	return true;
}; //End: check on same grid







PGL_NS_END(egd)


