/**
 * \brief	Thisd file contains the utility fuunction for computing the weights.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl/pgl_math.hpp>

#include <pgl_linalg/pgl_EigenCore.hpp>			//Include the main LINALG header
#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD
#include <limits>



PGL_NS_START(egd)

using pgl::isVerySmall;
using pgl::approxTheSame;
using pgl::isValidFloat;


/**
 * \brief	This function is used to test if the argument is very small
 *
 * This function compares its argument with the machine presicsion.
 */
static
bool
egdInternal_isVerySmall(
	const Numeric_t 	x)
{
	return ::pgl::isVerySmall(x);
}; //End: is very small number


/*
 * This function is the implementation of the process for finding
 * the associated weights for constant grids.
 */
static
void
egdInternal_findAssociatedMarkerWeigts_constGrid(
	const egd_markerProperty_t& 		mPosY,		//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridCont,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_markerWeight_t* const 		mWeight_,
	egd_gridWeight_t* const 		gWeight_);



void
egd_findAssociatedMarkerWeigts(
	const egd_markerProperty_t& 		mPosY,		//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridCont,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_markerWeight_t* const 		mWeight_,
	egd_gridWeight_t* const 		gWeight_)
{
	pgl_assert(mWeight_ != nullptr, gWeight_ != nullptr);
	pgl_assert(mPosX.size() > 0, mPosX.size() == mPosY.size());
	pgl_assert((Size_t)mPosX.size() == idxI.size(), (Size_t)mPosX.size() == idxJ.size());
	pgl_assert(gridCont.xNPoints() >= 2, gridCont.yNPoints() >= 2);
	pgl_assert(isValidGridType(gType));

	if(gridCont.isGridSet() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid is not set up.");
	};
	if(isExtendedGrid(gType) && isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_InvArg("The extended grid does not exists.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};

	if(gridCont.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_illMethod("Calculating the weights is only supported on constant grids.");
	}
	else
	{
		egdInternal_findAssociatedMarkerWeigts_constGrid(mPosY, mPosX, gridCont, gType, idxI, idxJ, mWeight_, gWeight_);
	};

	return;
}; //End: Weight switch


void
egd_findAssociatedMarkerWeigts(
		const egd_markerCollection_t&		mColl,
		const egd_gridGeometry_t& 		grid,
		const eGridType 			gType,
		const egd_indexVector_t& 		idxI,
		const egd_indexVector_t& 		idxJ,
		egd_markerWeight_t* const 		mWeight,
		egd_gridWeight_t* const 		gWeight)
{
	pgl_assert(mWeight != nullptr, gWeight != nullptr);
	egd_findAssociatedMarkerWeigts(
			mColl.cgetYPos(), mColl.cgetXPos(),
			grid, gType,
			idxI, idxJ,
			mWeight, gWeight);
	return;
}; //end: find associated wieghts convenients

void
egd_processGridWeights(
	egd_gridWeight_t* const 	gWeight_)
{
	pgl_assert(gWeight_ != nullptr);	//Alias the pointer argument
	egd_gridWeight_t& gWeight = *gWeight_;

	const Index_t nJ = gWeight.cols();
	const Index_t nI = gWeight.rows();

	for(Index_t j = 0; j != nJ; ++j)
	{
		for(Index_t i = 0; i != nI; ++i)
		{
			const Numeric_t gW = gWeight(i, j);	//Load the value
				pgl_assert(::pgl::isValidFloat(gW), gW >= 0.0);

			/*
			 * Now we decide, notice that we check
			 * exactly for zero, this is a bit dubious,
			 * but is actually what we should expect.
			 */
			if(PGL_UNLIKELY(egdInternal_isVerySmall(gW)) )
			{
				//We found a zero value, so we do nothing.
				//Since the value is already zero.
			}
			else
			{
				//We found a not zero value, so we invert it
				//and store it back
				const Numeric_t iGW = 1.0 / gW;
					pgl_assert(::pgl::isValidFloat(iGW), iGW > 0.0);

				//write back
				gWeight(i, j) = iGW;
			}; //End else:
		}; //End for(i):
	}; //End for(j):

	return;
}; //End: process grid weights





Index_t
egd_getNMarkers(
	const egd_markerWeight_t& 	mWeights)
{
	using std::to_string;

	//Get the mase value
	const Index_t nMarkers = mWeights[0][0].size();

	bool foundError = false;	//This bool indicates if an error was found.

	for(int i = 0; i != 2; ++i)
	{
		for(int j = 0; j != 2; ++j)
		{
			if(mWeights[i][j].size() != nMarkers)	//Test if the sizes are different
			{
				foundError = true;
			};
		}; //End for(j):
	}; //End for(i):


	if(foundError == false)	//No error found
	{
		return nMarkers;
	};

	//
	//We found an error, so we will raise an error
	std::string errStr = "The passed marker sizes has size problems.";

	errStr += "The sizes are: {";

	for(int i = 0; i != 2; ++i)
	{
		if(i == 1)
		{
			errStr += ", ";
		};

		for(int j = 0; j != 2; ++j)
		{
			const Index_t N_ij = mWeights[i][j].size();

			if(j == 1)
			{
				errStr += ", ";
			};

			errStr += "[" + to_string(i) + "][" + to_string(j) + "] -> " + to_string(N_ij);
		}; //End for(j):
	}; //End for(i):

	throw PGL_EXCEPT_RUNTIME(errStr);
}; //End: get nMarkers from marker weights



/*
 * ================================
 * Worker implementation
 */


/**
 * \brief	This function is the implementation for the constant grid.
 *
 * It sounds very strange to define a worker function for a worker function.
 * But it makes actually a lot of sense, in some way. It will help us, to
 * Reduce the coding complexity a bit.
 * It is the same as the constant grid, but has templates that sets some
 * things from the outside.
 *
 * \tparam  isExtenedGrid	Indicates if it is an extended grid.
 */
template<
	bool isExtendedGrid
>
extern
void
egdInternal_findAssociatedMarkerWeigts_constGrid_worker(
	const egd_markerProperty_t& 		mPosY,		//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridCont,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_markerWeight_t* const 		mWeight_,
	egd_gridWeight_t* const 		gWeight_);



void
egdInternal_findAssociatedMarkerWeigts_constGrid(
	const egd_markerProperty_t& 		mPosY,		//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridCont,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_markerWeight_t* const 		mWeight_,
	egd_gridWeight_t* const 		gWeight_)
{
	using pgl::isValidFloat;

	if(gridCont.isGridSet() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid is not set up.");
	};
	if(gridCont.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_LOGIC("Called the constant weight fuinction for an grid that is not constant.");
	};
	if(isExtendedGrid(gType) && isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_InvArg("The extended grid does not exists.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};
	if(mWeight_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The passed marker weight object is NULL.");
	};
	if(gWeight_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The passed grid weight object is zero.");
	};
	if(mPosX.size() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The passed marker collection is empty.");
	};
	if(mPosX.size() != mPosY.size())
	{
		throw PGL_EXCEPT_InvArg("different size for marker positions.");
	};
	pgl_assert((uSize_t)mPosX.size() == idxI.size(), (uSize_t)mPosX.size() == idxJ.size());
	pgl_assert(gridCont.xNPoints() >= 2, gridCont.yNPoints() >= 2);

	egd_gridWeight_t&   gWeight = *gWeight_;
	egd_markerWeight_t& mWeight = *mWeight_;

	if(isExtendedGrid(gType) == true)
	{
		egdInternal_findAssociatedMarkerWeigts_constGrid_worker<true>(
			mPosY, mPosX, gridCont, gType,
			idxI, idxJ,
			&mWeight, &gWeight);
	}
	else
	{
		pgl_assert(isRegularGrid(gType));	//Obscure situation
		egdInternal_findAssociatedMarkerWeigts_constGrid_worker<false>(
			mPosY, mPosX, gridCont, gType,
			idxI, idxJ,
			&mWeight, &gWeight);
	};

	return;
}; //End: constant grid




template<
	bool isExtendedGrid
>
extern
void
egdInternal_findAssociatedMarkerWeigts_constGrid_worker(
	const egd_markerProperty_t& 		mPosY,		//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridCont,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_markerWeight_t* const 		mWeight_,
	egd_gridWeight_t* const 		gWeight_)
{

	using pgl::isValidFloat;

	pgl_assert(mWeight_ != nullptr, gWeight_ != nullptr);
	pgl_assert(mPosX.size() > 0, mPosX.size() == mPosY.size());
	pgl_assert((Size_t)mPosX.size() == idxI.size(), (Size_t)mPosX.size() == idxJ.size());
	pgl_assert(gridCont.xNPoints() >= 2, gridCont.yNPoints() >= 2);

	if(gridCont.isGridSet() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid is not set up.");
	};
	if(gridCont.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_LOGIC("Called the constant weight fuinction for an grid that is not constant.");
	};
	if(::egd::isExtendedGrid(gType) && isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_InvArg("The extended grid does not exists.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};
	if(isExtendedGrid != ::egd::isExtendedGrid(gType))
	{
		throw PGL_EXCEPT_illMethod("Called the instantion for extended grids on a non extended grid.");
	};

	//
	//Load the variables

	egd_markerWeight_t& mWeight = *mWeight_;	//For convenience
	egd_gridWeight_t&   gWeight = *gWeight_;

	//
	//Load the positions of the markers
	using MarkerPos_cref = egd_markerCollection_t::MarkerProperty_cref;		//Constant reference to marker
	MarkerPos_cref xPos  = mPosX;
	MarkerPos_cref yPos  = mPosY;
		pgl_assert(xPos.size() == yPos.size());

	//
	//Load the number of grid points
	const Index_t nXPoints     = gridCont.getNGridPointsX( gType);
	const Index_t nYPoints     = gridCont.getNGridPointsY( gType);
	const Index_t lastValidIDx = nXPoints - 1;	//We do not use the "lastValidIndex"-function here, the reason is
	const Index_t lastValidIDy = nYPoints - 1;	// that the valid indexing has to do with the location on the domain
							// of the different grid, and thus considered ghost nodes, but we
							// also interpolate to ghost node and must thus be based on the real thing.
		pgl_assert(1 <  nXPoints,
			   1 <  nYPoints );

	//
	//Load marker information
	const Index_t nMarkers = mPosX.size();
	const Index_t nCells   = gridCont.nCells();
		pgl_assert(nMarkers > 0, nCells > 0);

	//
	//load information about the grid
	//Note that in case of regular cell centre points, not the location
	//of the first grid node is returned.
	Numeric_t xStart, yStart;
	std::tie(xStart, yStart) = gridCont.getTopNode(gType);
		pgl_assert(isValidFloat(xStart),
			   isValidFloat(yStart) );

	const Numeric_t xDelta  = gridCont.getXSpacing();
		pgl_assert(isValidFloat(xDelta), xDelta > 0.0);
	const Numeric_t yDelta  = gridCont.getYSpacing();
		pgl_assert(isValidFloat(yDelta), yDelta > 0.0);

	//
	//Prepare the variables

	for(Size_t i = 0; i != 2; ++i)
	{
		for(Size_t j = 0; j != 2; ++j)
		{
			if(mWeight[i][j].size() != nMarkers)
			{
				mWeight[i][j].resize(nMarkers);
			};

			//Set the marker weights to zero
			mWeight[i][j].setZero();
				pgl_assert(mWeight[i][j].size() == nMarkers);
		}; //End for(j):
	}; //End for(i):

	//
	//Resize the weight
	gWeight.resize(nYPoints, nXPoints);	//Swapping x and y

	//
	//Set the weights to zero
	gWeight.setZero();
		pgl_assert(gWeight.rows() == nYPoints, gWeight.cols() == nXPoints);

	/*
	 * We can not be sure, if the neighbours exists or not.
	 * It sio very complicated and depends on the grid to detect this.
	 * For this we use the templates. We know that if we have an extended grid,
	 * all nodes exist.
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load Variables
		const Numeric_t x_m = xPos[m];	//Load marker pos
		const Numeric_t y_m = yPos[m];
		const Index_t 	gI  = idxI[m];	//first index (y) of grid point
		const Index_t   gJ  = idxJ[m];	//Second index (x) of associated grid point
		pgl_assert(0 <= gJ, gJ <= lastValidIDx,
				0 <= gI, gI <= lastValidIDy );

		//TODO: Optimize this calculation

		//Const Calculate the distance between the marker and the grid point
		const Numeric_t x_diff = (x_m - xStart) - gJ * xDelta;
		const Numeric_t y_diff = (y_m - yStart) - gI * yDelta;
		//pgl_assert(x_diff >= 0.0, y_diff >= 0.0);	THE NOPRMALIZER FUNCTION WILL TAKE CARE OF THAT

		//This are the component of the weights
		const Numeric_t compX 	      = egd_normalizeWeightComp(x_diff / xDelta);
		const Numeric_t compY 	      = egd_normalizeWeightComp(y_diff / yDelta);
		const Numeric_t compYOneMinus = 1.0 - compY;
		const Numeric_t compXOneMinus = 1.0 - compX;


		/*
		 * X-----------------------------> x / j (second index)
		 * |  omega_ij        omega_ij1
		 * |    o----------------o
		 * |    |       	 |
		 * |    |	^   	 |
		 * |    |		 |
		 * |    |		 |	^ ~ Marker
		 * |    o----------------o
		 * |  omega_i1j       omega_i1j1
		 * |
		 * V  y / i (frist index)
		 *
		 */

		//Caluclate the different weights
		const Numeric_t omega_ij   = compXOneMinus * compYOneMinus;
		const Numeric_t omega_ij1  = compX         * compYOneMinus;
		const Numeric_t omega_i1j  = compXOneMinus * compY        ;
		const Numeric_t omega_i1j1 = compX 	   * compY        ;
			pgl_assert(egd_isValidWeight(omega_ij  ),
				   egd_isValidWeight(omega_i1j ),
				   egd_isValidWeight(omega_ij1 ),
				   egd_isValidWeight(omega_i1j1) );

		//Test now which one is is save
		//If an index is save, one can access +1
		const bool iIsSave = (isExtendedGrid ? true : ((gI < lastValidIDy) ? true : false) );
		const bool jIsSave = (isExtendedGrid ? true : ((gJ < lastValidIDx) ? true : false) );

		if( isExtendedGrid == true ||
		   (iIsSave && jIsSave)      )
		{
			//This is an inner case, so the nodes do exist
			pgl_assert((gI + 1) < gWeight.rows(),
				   (gJ + 1) < gWeight.cols() );

			//Store the values in the weight vector
			//This is just writting it.
			mWeight[0][0][m] = omega_ij  ;
			mWeight[0][1][m] = omega_ij1 ;
			mWeight[1][0][m] = omega_i1j ;
			mWeight[1][1][m] = omega_i1j1;

			//Update the grid weights.
			gWeight(gI    , gJ    ) += omega_ij  ;
			gWeight(gI + 1, gJ    ) += omega_i1j ;
			gWeight(gI    , gJ + 1) += omega_ij1 ;
			gWeight(gI + 1, gJ + 1) += omega_i1j1;
			//End if: no problem
		}
		else
		{
			/*
			 * In this section we know that not all nodes exists.
			 * So we handle them by simply checking if the node
			 * exists or not. Note that an analytical solution
			 * would be possible, but is very complictaed to implement.
			 *
			 * However we know that the [1][1] node will surly not exist.
			 */

			//Despite all, we know the the associated node does exists
			{
				pgl_assert(gI < gWeight.rows(),
					   gJ < gWeight.cols() );

				mWeight[0][0][m] = omega_ij;
				gWeight(gI, gJ) += omega_ij;
			}; //End scope: first index


			if(iIsSave)
			{
				pgl_assert((gI + 1) < gWeight.rows());

				mWeight[1][0][m]     = omega_i1j;
				gWeight(gI + 1, gJ) += omega_i1j;
			}; //End if: i save

			if(jIsSave)
			{
				pgl_assert((gJ + 1) < gWeight.cols());

				mWeight[0][1][m]     = omega_ij1;
				gWeight(gI, gJ + 1) += omega_ij1;
			}; //End if: j save
		}; //End else: find special nodes
	}; //End for(m): iterating over the markers

	return;
}; //End: find weights constant

PGL_NS_END(egd)

