/**
 * \brief	This function contains the implementation that for the utility function of the grid.
 *
 * These functions are functions that does not belong to any other file and are rather small and
 * unrelated.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST


//Include STD


PGL_NS_START(egd)


bool
egd_isValidWeight(
	const egd_gridWeight_t& 	gWeight,
	const bool 			wasProcessed)
{
	pgl_assert(gWeight.size() > 0);
	const Index_t rows = gWeight.rows();
	const Index_t cols = gWeight.cols();

	if(wasProcessed == true)
	{
		for(Index_t j = 0; j != rows; ++j)
		{
			for(Index_t i = 0; i != cols; ++i)
			{
				if(PGL_UNLIKELY(egd_isValidWeight(gWeight(i, j)) == false))
				{
					pgl_assert(false && "Detected an invalid grid weight.");
					return false;
				}; //End if: is invalid
			}; //End for(i):
		};//End for(j):
	} //End if: was processed
	else
	{
		for(Index_t j = 0; j != rows; ++j)
		{
			for(Index_t i = 0; i != cols; ++i)
			{
				const Numeric_t v = gWeight(i, j);
				const bool r1 = pgl::isValidFloat(v);
				const bool r2 = (v >= 0.0);
				if(PGL_UNLIKELY(r1 && r2))
				{
					pgl_assert(false && "Detected an invalid grid weight.");
					return false;
				}; //End if: is invalid
			}; //End for(i):
		};//End for(j):
	}; //End else: was not processed

	return true;	//Found nothing so it sould be okay.
}; //End: isValidWeight for grids.


bool
egd_isValidWeight(
	const egd_markerWeight_t& 	mWeight)
{
	//Get the number of markers
	const Index_t nMarkers = egd_getNMarkers(mWeight);
		pgl_assert(nMarkers > 0);

	for(int i = 0; i != 2; ++i)
	{
		for(int j = 0; j != 2; ++j)
		{
			const auto& W = mWeight[i][j];	//Get a reference to the current markers

			//Checking all the markers
			for(Index_t m = 0; m != nMarkers; ++m)
			{
				if(PGL_UNLIKELY(egd_isValidWeight(W[m]) == false))
				{
					pgl_assert(false && "Detected an invalid grid weight.");
					return false;
				}; //End if: test the marker
			}; //End for(m): going through the markers
		}; //End for(j):
	}; //End for(i):

	return true;	//Found no evidence of corruption
}; //End: check weights for markers


std::pair<egd_Array_t<Numeric_t>, egd_Array_t<Numeric_t> >
egd_meshgrid(
	const ::std::pair<egd_Vector_t<Numeric_t>, egd_Vector_t<Numeric_t> >& 		gridPointPair)
{
	return egd_meshgrid(gridPointPair.first, gridPointPair.second);
};


std::pair<egd_Array_t<Numeric_t>, egd_Array_t<Numeric_t> >
egd_meshgrid(
	const egd_Vector_t<Numeric_t>& 		xPoints,
	const egd_Vector_t<Numeric_t>& 		yPoints)
{
	const Index_t Nx = xPoints.size();
	const Index_t Ny = yPoints.size();

	return ::std::make_pair(
		xPoints.transpose().colwise().replicate(Ny).array().eval(),
		yPoints            .rowwise().replicate(Nx).array().eval() );
}; //End: egd_meshgrid

PGL_NS_END(egd)


