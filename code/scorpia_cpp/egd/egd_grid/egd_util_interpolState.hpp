#pragma once
/**
 * \brief	This file defines a class that can be used as the result of
 * 		 Several interpolation routines.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridType.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include BOOST
#include <boost/dynamic_bitset.hpp>


//Include STD


PGL_NS_START(egd)

/**
 * \brief	This struct is a simple container for a complete interpolation result.
 * \class 	egd_interpolState_t
 *
 * This class stores all the information about an interpolation.
 * This is a very open class, since it has not private memebers.
 *
 * Note this class was previously named egd_interpolResult_t.
 * But it was learned, that this name is not appropriate, so it
 * was changed to egd_interpolState_t, which reflects its purpose
 * better.
 */
struct egd_interpolState_t
{
	/*
	 * ======================
	 * The member
	 */
public:
	eGridType 		gType = eGridType::INVALID;	//!< This is the grid type.
	egd_indexVector_t 	idxI;				//!< Indexvectors for the first index.
	egd_indexVector_t 	idxJ;				//!< Indexvectors for the second index.
	egd_markerWeight_t 	mWeights;			//!< These are the marker weights.
	egd_gridWeight_t 	gWeights;			//!< This is the grid node weights.
	bool 			m_isProcessed = false;		//!< Indicates if *this was processed or not.

	egd_localAssocVector_t  locAssoc;			//!< This is the vector for local association.
	egd_gridWeight_t 	locGridWeights;			//!< This is the grid weights for local ones.
								//!< 	Theier process state is coupled to the one of the non local grid weights.


	/*
	 * =========================
	 * Constructors
	 *
	 * We do not define any constructor so all are
	 * implicitly defined and present.
	 */
public:


	/*
	 * =========================
	 * Query Functions
	 */
public:
	/**
	 * \brief	This function returns true if *this is processed.
	 *
	 * *this is processed, if *this was passed to the process functions,
	 * that adjusted the grid weights, such that they can be used by the
	 * interpolator as expected.
	 */
	bool
	isProcessed()
	 const
	 noexcept
	{
		return m_isProcessed;
	}; //End: isProcessed


	/**
	 * \brief	This function returns true if *this has a valid grid type.
	 */
	bool
	isValidGridType()
	 const
	 noexcept
	{
		return ::egd::isValidGridType(this->gType);
	}; //End: isValidGridType


	/**
	 * \brief	This function returns true if *this has a local addition.
	 *
	 * *this basically checks if there are local associations present in *this.
	 */
	bool
	hasLocalAssoc()
	 const
	 noexcept
	{
		return (locAssoc.size() == 0)
			? false
			: true;
	}; //End: locAssoc


	/*
	 * ============================
	 * Managing Functions
	 */
public:
	/**
	 * \brief	This function will reset *this.
	 *
	 * It will try to deallocate all memory.
	 * Never the less it will invalidate *This by setting
	 * the grid type to NONE.
	 */
	void
	clear();


	/**
	 * \brief	Under some conditions, this function allows to change the
	 * 		 grid type of *this without recomputing the state.
	 *
	 * This function can be used to quickly change the size or the grid
	 * type, note that only a certain set of operations are possible.
	 * As argument this function takes the new target grid type.
	 *
	 * Note that this function does not know if the weights where
	 * disabled or not, it is the users responsibility to do that.
	 *
	 * If the operation fails an exception is thrown.
	 *
	 * \param  newTargetGrid 	The type of the new grid
	 */
	void
	changeGridType(
		const eGridType 		newTargetGrid);


	/*
	 * =============================
	 * Test Functions
	 */
public:
	/**
	 * \brief	This function returns true if *this is valid.
	 *
	 * *this is valid if there are no contradictions.
	 * If this function returns true, it only means that *this
	 * could be valid.
	 * No values of the stored properties are inspected by this function.
	 */
	bool
	testForConsistency()
	 const
	 noexcept;


	/**
	 * \brief	This function checks if the weights of *this are finite.
	 *
	 * This first includes a consistency check and then
	 * a test if a ono finite value was found.
	 *
	 * \note	This function previously did something similar to
	 * 		 the isValid() function, but it was learned that the
	 * 		 it did too much, so the isValid() fuinction was
	 * 		 introduced.
	 */
	bool
	isFinite()
	 const;


	/**
	 * \brief	This function performs an indeep test of *this.
	 *
	 * It will first apply the consistency check.
	 * It will then check if the grid and marker weights are valid.
	 * This includes finite and if they are in the righgt range.
	 * Then all associating node indexes are checked.
	 */
	bool
	isValid()
	 const;

}; //End struct(egd_interpolState_t):

PGL_NS_END(egd)



