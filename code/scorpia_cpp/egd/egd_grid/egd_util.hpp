#pragma once
/**
 * \brief	This file implements utility functions for the grid.
 *
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>


//Include BOOST


//Include STD
#if PGL_DEBUG_ON
#	include <iomanip>
#endif

PGL_NS_START(egd)

//FWD of the grid container
class egd_gridContainer_t;

//Fwd of the marker collection
class egd_markerCollection_t;

//FWD of the grid property
class egd_gridProperty_t;

//FWD of the grid geometry
class egd_gridGeometry_t;

//FWD of interpolat result
struct egd_interpolState_t;


/**
 * \brief	This is the index vector, it is basically a vector of ints,
 * 		 that is used for indexing.
 * \typedef 	egd_indexVector_t
 *
 * It is used at different locations inside the code base.
 * It is used to store the associatation between markers and nodes
 * during interpolation or stores which markers are of whitch type.
 *
 * Note that the Eigen documation clamed that a vector that conforms
 * to the C++ standard can be used for slicing, since pgl_vector_t
 * does this, it should work.
 */
using egd_indexVector_t = ::pgl::pgl_vector_t<Index_t>;


/**
 * \brief	This is a special indexing vector, that is used to deduce
 * 		 association in a local interpolation setting.
 *
 * This index is only usefull if used with a full interpolation set,
 * meaning association index (full) and marker weights. It basically
 * wich of the sourounding nodes is closest to the markers, for that
 * a very space representation is used. Each bit encodes if the
 * marker in that dimension is nearest or not.
 * Note that the ordering is is very strange in the way that first bit,
 * least significant one, is the x dimension, and then the y dimension
 * and so on.
 *
 * Use the provided macros to extract the selected association.
 */
using egd_localAssocVector_t = ::pgl::pgl_vector_t<uInt8_t>;


/**
 * \brief	This macro is able if to decode the association in x direction.
 * \define 	egd_getLocalAssocX
 *
 * This function returns 0, if the closest node to the marker the one
 * it is associated to, or one, if it is the next one.
 *
 * \param  locA		The value of local association
 */
#define egd_getLocalAssocX(locA) ( ( uInt8_t(locA) & uInt8_t(1)) ? Index_t(1) : Index_t(0))


/**
 * \brief	This macro will decode the local association in y direction.
 * \define	egd_getLocalAssocY
 *
 * This function is the same as the X version, but operates on the Y dimension.
 *
 * \param  locA 	The value of the local association.
 */
#define egd_getLocalAssocY(locA) ( ( ((uInt8_t(locA)) >> 1) & uInt8_t(1)) ? Index_t(1) : Index_t(0))


/**
 * \brief	This macro can encode the local association.
 * \brioef	egd_makeLocalAssoc
 *
 * This function performs some bit magic, not that this macro
 * assumes that the value that is passed to it, is either
 * 0 or 1. Also note that the order for this macro is the
 * usual way it is in EGD, meaning first comes the Y then X.
 *
 * \param  laY 		Local association  in Y, must be 0 or 1.
 * \param  laX		Local aassociation in X, must be 0 or 1.
 */
#define egd_makeLocalAssoc(laY, laX)   (  ((uInt8_t(laX)) & 1) |   (((uInt8_t(laY)) & 1) << 1) )


/**
 * \brief	This is the type that is used for the marker property.
 * \typedef 	egd_markerProperty_t
 *
 * This is basically a vector that stores the value of a certain property for a marker.
 *
 * Note it was changed to an Eigen Array type. This is more logical, since the
 * marker properties are no matematical vectors, they are just a bunch of
 * values.
 */
using egd_markerProperty_t = ::egd::egd_Array_t<Numeric_t, Eigen::Dynamic, 1>;


/**
 * \brief	This is the grid weight.
 *
 * The gread weight is the weight sum of all weights of the
 * markers that are associated to that node.
 */
using egd_gridWeight_t = egd_Array_t<Numeric_t>;


/**
 * \brief	This is the marker weight.
 *
 * The marker weight is the contribution of one marker
 * to its four surrounding nodes.
 * Note that the this is a [2][2] matrix of Eigen
 * vectors.
 * Assume marker m is associated to node (i,j) and A
 * ist the marker weight, then
 *
 *  - A[0][0][m] -> 	Weight of marker m towards node (i  , j  )
 *  - A[0][1][m] ->	Weight of marker m towards node (i  , j+1)
 *  - A[1][0][m] -> 	Weight of marker m towards node (i+1, j  )
 *  - A[1][1][m] -> 	Weight of marker m towards node (i+1, j+1)
 *
 */
using egd_markerWeight_t = egd_Vector_t<Numeric_t>[2][2];





/**
 * \brief	This function runs the preprocessing for the interpolation
 * 		 process, it is absically a wrapper that uses several functions.
 *
 * This function combines several other functions and stores the output,
 * the associations and the weights into an interpolation result object.
 * This reduces the number of argumenst a function takes.
 *
 * This function returns the values that is returned by the find index function,
 * i.e. if all cells have markers in it, and no empty cells. Depending on the grid,
 * this can indicate propblems.
 *
 * The function also has two optional falgs. The first one will request
 * that the computed grid weights are processed. The second flag, will
 * request that the grid weights are disabled.
 * Both flags are set to true, which enables the task.
 *
 * The last flag instructs the function to generate a local association map
 * in the state. This is optional information that is used to perform local
 * interpolation. The default value of this falg is false.
 *
 * The process of this function is:
 * 	- Finding the accociated nodes.
 * 	- Finding the weights of the markers and one of the grid nodes.
 * 	- Run the disableing process of the grid weights, if requested.
 * 	- Run the post processing of the grid weights, if requested.
 *
 * Its parameter are.
 * \param  mColl		The marker collection.
 * \param  grid			This is the grid that is used.
 * \param  gType 		The grid object that it used.
 * \param  interpolState	This is the state of the interpolation.
 * \param  doProcessing		Should the processing function be applied; default true.
 * \param  doDisableing 	Should the disableing functin be caalled.
 * \param  withLocal		Activate the local interpolation mode too.
 *
 * \note 	This function calls some low-level functions directly, but
 * 		 also other high level functions. Note that this function
 * 		 forwardss to the specific version, but you should regard
 * 		 this function as the work horse.
 */
bool
egd_initInterpolation(
	const egd_markerCollection_t&		mColl,
	const egd_gridGeometry_t& 		grid,
	const eGridType 			gType,
	egd_interpolState_t* const 		interpolState,
	const bool 				doProcessing = true,
	const bool 				doDisabeling = true,
	const bool 				withLocal    = false);


/**
 * \brief	This function calls the diable ghost node function on the
 * 		 interpolation result container.
 *
 * This function calls the low level disable function on the grid nodes of the
 * passed interpolation result. This function ignores the processing result
 * of the passed container, since this does not make any differents.
 *
 * This function returns true, if the grid weights were manipulated.
 * A false indicates that the grid does not have any ghost nodes
 * that could be manipulated.
 *
 * Note that if the passed state has local association informations,
 * this function will also disable the ghost noes on them.
 *
 * \param  interpolState		The interpolation result that should be manipulated.
 *
 * \note	This function is called by the init function.
 */
bool
egd_disableGhostNodes(
	egd_interpolState_t* const 	interpolState);


/**
 * \brief	This function applies teh processing on the passed
 * 		 interpolation result object.
 *
 * This function basically inverts the grid weights such that they
 * can be used by the interpolation routine. Note if the argument
 * was already processed this function is considered a noops.
 *
 * Note this function returns true if the processing was applied
 * and false if not, also in case of an error false will be returned.
 *
 * Note that if the it has local addition they will be processed as well.
 *
 *
 * \param  interpolState		This is teh interpolation result.
 *
 * \note	This this fnction is called by the init function
 * 		 if a processing was requested.
 */
bool
egd_processWeights(
	egd_interpolState_t* const 	interpolState);


/**
 * \brief	This function tests if the interpolation result is valid.
 *
 * This function calls the memeber function isValid() of the interpolation result.
 *
 * \param  interpolState		This is the interpolation result to check.
 */
bool
egd_isValidWeight(
	const egd_interpolState_t& 	interpolState);


/**
 * \brief	This function performs interpolation.
 *
 * This function maps the markers to the grid. The interpolation
 * result object is used for that.
 *
 * Note that all nodes, also the ghost nodes will participate in
 * the interpolation prcess. When the result is normalized the
 * ghost nodes, under the condition that the disable function
 * was appled to the interpolation container, will be removed,
 * since theier weight was zero. If wou want to remove other
 * nodes from the interpolation, then you must manipulate
 * the grid weights directly, which is not recommended.
 *
 * Also note that the marker weights can not be manipulated.
 *
 * For conservative interpolation, it is also possible to modify
 * the weigths. For that a second grid property, of the same
 * size and order as the marker property has to be passed.
 * The weights of a marker to a node are then multiplied
 * by this factor. The grid weight is also recomputed on the
 * fly for that. Before the grid weights are processed,
 * it is possible to request a disableing operation
 * on them, which is the default.
 *
 * This function can also be used to perform local interpolation,
 * by setting the respective flag to true. Note also modified
 * weights is accepted.
 *
 * \param  interpolState	The interpolation result.
 * \param  mProp		The marker property that should be interpolated.
 * \param  gProp		The destination grid property.
 * \param  modWeights		Modifcation for the weights.
 * \param  disableGhostNodes	Disable ghost node on newly computed weights.
 * \param  doLocalInterpol 	Only use local interpolation.
 */
void
egd_mapToGrid(
	const egd_interpolState_t& 		interpolState,
	const egd_markerProperty_t& 		mPorp,
	egd_gridProperty_t* const 		gProp,
	const egd_markerProperty_t* const 	modWeights         = nullptr,
	const bool 				disableGhostNodes  = true,
	const bool 				doLocalInterpol    = false);


/**
 * \brief	This function allows to interpolate grid property to
 * 		 marker property object.
 *
 * This function is able to pull back a property that is defined on the grid
 * back to the markers. This function is a convinient interface of the
 * low level interface of the same name.
 *
 * Normaly the function will overwrite the marker property stored in the target
 * object, it will also resize it if needed. However it is possible to select
 * an update of the property, in that case the update happens with "+=".
 * Note that in this case the target marker property object must have the
 * requiered size and no resizing is done.
 *
 * An imporrtant remark is, that this function does not consider ghost nodes
 * any special. If you which to diable all contribution comming from ghost
 * nodes, then you have to set all nodes corresponding to ghost nodes in the
 * grid property to zero.
 *
 * \param  mProperty		The marker property, the destinnation.
 * \param  gProperty		The grid proptrerty, the source.
 * \param  interpolState	The interpolation state.
 * \param  dpoUpdate		Should the marker be updated.
 *
 * \note that the gWeights memeber of the interpolation state is ignored.
 */
void
egd_pullBackToMarker(
	egd_markerProperty_t* const 		mProperty,
	const egd_gridProperty_t& 		gProperty,
	const egd_interpolState_t& 		interpolState,
	const bool 				doUpdate = false);



/**
 * \brief	This function performs a naïve interpolation.
 *
 * This function is able to perform a very simple minded interpolation,
 * between grids. For that it averages the value of the surrounding nodes,
 * or so. It is not very efficient or so, or precise.
 * Some transformation might not be implemented.
 *
 * It suppors the normal arethmetic average and it supports a harmonic average.
 *
 * Note when harmonic interpolation is selected, the user must guarantee that
 * the non ghost node are non zero in value.
 *
 * \note 	Note that this function assumes that the grid is spacied constant.
 *
 * \param  destProp	The destination property.
 * \param  srcProp	The source property.
 * \param  gridGeo	The grid geometry.
 * \param  useHarmonic	Should harmonic average be used.
 */
void
egd_performNaiveTransform(
	egd_gridProperty_t* const 	destProp,
	const egd_gridProperty_t& 	srcProp,
	const egd_gridGeometry_t& 	gridGeo,
	const bool 			useHarmonic);






/*
 * ================================
 * LOW LEVEL FUNCTION
 *
 * Here low level functions are declared.
 * You should not use them, but read the description.
 * use the functions above.
 */


/**
 * \brief	This function allows to setup an interpolation state without having
 * 		 a marker collection at hand.
 *
 * This function operates on pure position array, mind thge order.
 *
 * \param  mPosY		The y coordinate of the position.
 * \param  mPosX		The x component of the position.
 * \param  grid			This is the grid that is used.
 * \param  gType 		The grid object that it used.
 * \param  interpolState	This is the state of the interpolation.
 * \param  doProcessing		Should the processing function be applied; default true.
 * \param  doDisableing 	Should the disableing functin be caalled.
 * \param  withLocal		Activate the local interpolation mode too.
 *
 * \note 	This function calls some low-level functions directly, but
 * 		 also other high level functions. Note that this function
 * 		 forwardss to the specific version, but you should regard
 * 		 this function as the work horse.
 */
bool
egd_initInterpolation(
	const egd_markerProperty_t& 		mPosY,		//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		grid,
	const eGridType 			gType,
	egd_interpolState_t* const 		interpolState,
	const bool 				doProcessing = true,
	const bool 				doDisabeling = true,
	const bool 				withLocal    = false);



/**
 * \brief	This function finds the associated node index of the markers.
 *
 * Generally the associated node to a marker is the node that is top left
 * to the marker. This information is stored in the passed index vectors.
 * the first index vectors have the same length as the number of markers.
 * The first vector (idxI) contains the first node index and the second
 * vector contains the second index.
 *
 * The grid on which the operation has to be performed must be specified,
 * by the gType argument.
 * Note thet the three neighbours of an associated node may not exists.
 *
 * If a cell is found that has no markers in it false is returned.
 *
 * This function returns true if all cell contained a marker.
 * It is not considered an error if this is not the case.
 *
 * \param  mPosY	The y coordinates of the marker positions.
 * \param  mPosX	The x coordinates of the marker positions.
 * \param  grid		This is the grid that is used.
 * \param  gType 	The grid object that it used.
 * \param  idxI		This is a list of the first indexes (y coordinate).
 * \param  idxJ		This is a list of the second indexes (x coordinate).
 *
 * \note 	On the long run this function and the find weight
 * 		 function will be merged.
 */
bool
egd_findAssociatedNodeIndex(
	const egd_markerCollection_t& 		mColl,
	const egd_gridGeometry_t& 		grid,
	const eGridType 			gType,
	egd_indexVector_t* const 		idxI,
	egd_indexVector_t* const 		idxJ);


/**
 * \brief	This function is a convenient wrapper arround the low level interface.
 *
 * It will compute the assoication of the markers, the markers possitions will be
 * unpacked from the container directly.
 *
 * \param  mColl	The marker collection.
 * \param  grid		This is the grid that is used.
 * \param  gType 	The grid object that it used.
 * \param  idxI		This is a list of the first indexes (y coordinate).
 * \param  idxJ		This is a list of the second indexes (x coordinate).
 */
bool
egd_findAssociatedNodeIndex(
	const egd_markerProperty_t& 		mPosY,	//mind the order
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		grid,
	const eGridType 			gType,
	egd_indexVector_t* const 		idxI,
	egd_indexVector_t* const 		idxJ);


/**
 * \brief	This function examines the index vectors if a
 * 		 cell is empty.
 *
 * This function will test if some cells may be empty or not.
 * Note this function takes into account the some ghost nodes
 * may not have associated markers. thsy will be ignored.
 *
 * \param  gridGeo	The grid geometry.
 * \param  gType	The grid type.
 * \param  idxI 	The index vector for the first index.
 * \param  idxJ		The index vector for the second index.
 */
bool
egd_noEmptyCell(
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ);



/**
 * \brief	This function is used to calculate the weights
 * 		 for the interpolation.
 *
 * This function operates on the associations, that are computed
 * by the index functions, the correctness is not checked.
 *
 * This function will then populate the marker weight structure
 * and the grid weight variable for such that it can be used.
 * There is some kind of debate what this means.
 *
 * Depending on the grid, it can happenbs that the neighbouring
 * nodes of the associated node does not exist, for example
 * on the pressure grid. In that case the value will be set
 * to zero.
 *
 * Note that weights are computed for all existing grid nodes.
 * This also involves the ghost nodes, which can be desirable
 * or not. If you whant to change that you have to manipulate
 * the computed weight further.
 *
 * This function does not apply any further processing on its own.
 *
 * \param  mPosY	The y coordinate of the marker positions.
 * \param  mPosX	The x coordinate of the marker positions.
 * \param  gridCont	The grid container.
 * \param  gType	The grid type that is used.
 * \param  idxI 	The first matrix indexes.
 * \param  idxJ 	The second matrix indexes.
 * \param  mWeight	The weight for the merkers (vector).
 * \param  gWeight 	The weight for the grid point (matrix).
 *
 * \throw	If a problem is detected.
 */
void
egd_findAssociatedMarkerWeigts(
		const egd_markerProperty_t& 		mPosY,		//mind the order
		const egd_markerProperty_t& 		mPosX,
		const egd_gridGeometry_t& 		grid,
		const eGridType 			gType,
		const egd_indexVector_t& 		idxI,
		const egd_indexVector_t& 		idxJ,
		egd_markerWeight_t* const 		mWeight,
		egd_gridWeight_t* const 		gWeight);


/**
 * \brief	This function is a convenient wrapper for the the associated weight function.
 *
 * This function will unpack the marker collection by itself.
 *
 * \param  mPosY	The y coordinate of the marker positions.
 * \param  mPosX	The x coordinate of the marker positions.
 * \param  gridCont	The grid container.
 * \param  gType	The grid type that is used.
 * \param  idxI 	The first matrix indexes.
 * \param  idxJ 	The second matrix indexes.
 * \param  mWeight	The weight for the merkers (vector).
 * \param  gWeight 	The weight for the grid point (matrix).
 */
void
egd_findAssociatedMarkerWeigts(
		const egd_markerCollection_t&		mColl,
		const egd_gridGeometry_t& 		grid,
		const eGridType 			gType,
		const egd_indexVector_t& 		idxI,
		const egd_indexVector_t& 		idxJ,
		egd_markerWeight_t* const 		mWeight,
		egd_gridWeight_t* const 		gWeight);


/**
 * \brief	This function computes a helper structure which allows
 * 		 to perform local interpolation.
 *
 * Normaly a marker is surrounded by four nodes, at least in the
 * interiour part of the domain. This function will determine which
 * is the closest node to a marker.
 * This will be stored inside a local association structure that
 * can be used to perform local interpolation.
 *
 * It will also compute an additionally grid weight matrix, which
 * contains the grid weights that are needed. Note it will not
 * process them in any way. It is recomended to pass a new
 * grid weight and not reuse the one of the non-local state.
 *
 * Note that this function extends a non-local interpolation state,
 * such that it can be sued for local interpolation as well.
 *
 * \param  mPosY	The y coordinate of the marker positions.
 * \param  mPosX	The x coordinate of the marker positions.
 * \param  gridGeo	The grid geometry.
 * \param  gType	The used grid type.
 * \param  idxI 	The first matrix indexes.
 * \param  idxJ 	The second matrix indexes.
 * \param  mWeight	The weight for the merkers (vector).
 * \param  gWeightL 	The weight for the grid point (matrix), in local sense
 */
void
egd_makeLocalInterpolState(
	const egd_markerProperty_t& 		mPosY,
	const egd_markerProperty_t& 		mPosX,
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_localAssocVector_t* const 		locAssoc,
	const egd_markerWeight_t& 		mWeight,
	egd_gridWeight_t* const 		gWeightL);


/**
 * \brief	This is a conveninet interface that will
 * 		 unpack the marker collection and forward
 * 		 the request to the base implementation.
 *
 * \param  mColl	The marker collection.
 * \param  gridGeo	The grid geometry.
 * \param  gType	The used grid type.
 * \param  idxI 	The first matrix indexes.
 * \param  idxJ 	The second matrix indexes.
 * \param  mWeight	The weight for the merkers (vector).
 * \param  gWeightL 	The weight for the grid point (matrix), in local sense
 */
void
egd_makeLocalInterpolState(
	const egd_markerCollection_t&		mColl,
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	egd_localAssocVector_t* const 		locAssoc,
	const egd_markerWeight_t& 		mWeight,
	egd_gridWeight_t* const 		gWeightL);



/**
 * \brief	This function prepares the grid node weight, that is computed
 * 		 by the findAssociatedMarkerWeights() function.
 *
 * This function processes the marker weights, such that they can be used, by
 * the interpolator routine. It will invert the weights such that no division
 * is needed, so a multiplication is needed to apply the normalization.
 *
 * However if the grid weight is zero, this function simply set the value to zero.
 * This function is completly grid agnostic, thus this function also does not
 * treat ghost nodes spacial.
 * Note the test if the grid weight is zero is not (anylonger) implemented as
 * a equal test against zero, but a comparisson. If the value in smaller
 * than the mashine prescision, it is considered as zero. Note that the test
 * is done by first taking the absolute value, this is done to preserve the
 * signe and trigger an error.
 *
 * In short this function computes:
 * 	g_{ij} := g_{ij} != 0 ? (1.0 / g_{ij}) : 0.0
 *
 * \param  gWeight		The grid weight container to process.
 */
void
egd_processGridWeights(
	egd_gridWeight_t* const 	gWeight);


/**
 * \brief	This function disables the gridweights
 * 		 that are belonging to ghost nodes.
 *
 * This function sets all nodes in the grid weight object to zero.
 * This means they will then be removed and if the property is
 * normalized and have no effect.
 * Note that only ghost nodes are targeted, boundary nodes are not.
 *
 * This function returns ture if the grid weight was manipulated.
 * False indicates that the grid does not have any ghost nodes.
 *
 * \param  gWeight	The grid weight to process.
 * \param  gType	The grid type that is involved.
 *
 * \note	It is not important if this operation is applied before
 * 		 or after the grid weights are processed.
 */
bool
egd_disableGhostNodes(
	egd_gridWeight_t* const 	gWeight,
	const eGridType 		gType);


/**
 * \brief	This function interpolates the marker properties to the grid.
 *
 * Note that this function operates on previously computed data.
 * It is, and can not, be checked, if the weights and indexes were
 * computed on the right grid. This function is provided as a building block.
 *
 * The function can also handle the case, were the weights have to be modified,
 * to allow conservative interpolation. In that case a second marker property
 * has to be passed. The weight of a marker, meaning all four of them, are
 * then multiplied by the passed marker value.
 * The marker weight is not recomputed but the grid weitghts are recomputed.
 * If the grid weights are recomputed this function will call the processesing
 * function for the grid weights on the modified grid weights. If requested
 * also the disableing function will be called, to remove the ghost nodes.
 *
 * Note that this function consider all nodes for interpolation.
 * Meaning that all nodes could have a non zero property value after
 * interpolation, this also involves the ghost nodes, which could also
 * be target for interpolation. If you do not want this, then you have
 * to set the grid weight of these node to zero, before you call this
 * function, for example, by calling the disable function.
 * In the case of modified marker weights, you can request this
 * by the disableing function.
 *
 * \param  gProperty	This is the target that will be filled.
 * \param  mProperty	This is the marker property, the source.
 * \param  mWeights	This are the individual marker weights.
 * \param  gWeights 	This is the grid weights, is ignored when addapting is passed.
 * \param  idxI 	The first matrix indexes.
 * \param  idxJ 	The second matrix indexes.
 * \param  modWeights	This is a modification for the weight, must be a vector of the same length as markers.
 * \param  DisGhostNode	This bool has only an effect the grid weights are recomputed.
 * 			 Setting this to true, the default, will call the disable function on the mmodified
 * 			 grid weights.
 *
 * \note	This function internaly uses templates to achieve its vertability.
 */
void
egd_mapToGrid(
	egd_gridProperty_t* const 		gProperty,
	const egd_markerProperty_t& 		mProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_gridWeight_t& 		gWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				reapplyDisableGhostNode = true);


/**
 * \brief	This function performs local interpolation from the markers to the grid.
 *
 * This function assumes that the local association wqas properly set up.
 * It also allows to use a weighted interpolation sheme. It is also possible
 * to reapply the ghost node prcessing to it.
 * It also assumes that its argument are processed, if needed.
 *
 * \param  gProperty	This is the target that will be filled.
 * \param  mProperty	This is the marker property, the source.
 * \param  mWeights	This are the individual marker weights.
 * \param  gWeights 	This is the local grid weights, is ignored when addapting is passed.
 * \param  idxI 	The first matrix indexes.
 * \param  idxJ 	The second matrix indexes.
 * \param  locAssoc	The local association of the markers.
 * \param  modWeights	This is a modification for the weight, must be a vector of the same length as markers.
 * \param  DisGhostNode	This bool has only an effect the grid weights are recomputed.
 * 			 Setting this to true, the default, will call the disable function on the mmodified
 * 			 grid weights.
 */
void
egd_mapToGridLocal(
	egd_gridProperty_t* const 		gProperty,
	const egd_markerProperty_t& 		mProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_gridWeight_t& 		gWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const egd_localAssocVector_t& 		locAssoc,
	const egd_markerProperty_t* const 	modWeights,
	const bool 				reapplyDisableGhostNode = true);





/**
 * \brief	This function is the low level function to pull the grid
 * 		 values back to a marker.
 *
 * This function multiplies the marker weight of the four sourounding
 * grid points with the respective grid property and adds the result
 * together to get the new marker value. The old marker property is
 * overwritten and lost. The target marker property is also resized
 * if needed. However this function allows to update the marker
 * property, in this case the gProperty could be seen as a change that
 * is interpolated back.
 *
 * This function does not care if a node is a ghost node or not.
 * If you want that ghost node have no effect, set them zo zero
 * in the grid property.
 *
 * This function however consider cases there the grid property
 * is too small, meaning some nodes mey not exists, then a
 * spocky node is created, such that it has no contribution.
 *
 * The grid type is extracted from the grid property.
 *
 * \param  mProperty	The marker property that should be condidered.
 * \param  gProperty	This is the target that will be filled.
 * \param  mWeights	This are the individual marker weights.
 * \param  idxI 	The first matrix indexes.
 * \param  idxJ 	The second matrix indexes.
 * \param  updateMarker	If set to true, markers are updated rather than overwritten.
 */
void
egd_pullBackToMarker(
	egd_markerProperty_t* const 		mProperty,
	const egd_gridProperty_t& 		gProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const bool 				updateMarker = false);




/**
 * \brief	This function finds the number of markers that are
 * 		 encoded inside the marker weights.
 *
 * Thsi function will query the [0][0] weights and then check
 * if then comare this values iwth the others, if they are not
 * the same an exception is raised.
 *
 * \param  mWeights	The weights to check.
 */
Index_t
egd_getNMarkers(
	const egd_markerWeight_t& 	mWeights);


/**
 * \brief	This macro tests if a weight is valid.
 *
 * This function tests if a weight is valid or not.
 * For that pgl::isValidFloat is used and it is tested
 * if the value is inside the range [0, 1].
 *
 * \param  w	The weight to check.
 *
 * \note 	This function was previoulsy a macro.
 */
inline
bool
egd_isValidWeight(
	const Numeric_t 	w)
{
	 return (::pgl::isValidFloat(w) && (0.0 <= (w)) && ((w) <= 1.0));
};


/**
 * \brief	This function tests if \e w is a valid modified weight.
 *
 * A valid modified weight is a valid number that is zero or positive.
 *
 * \param  w	The weight to test.
 */
inline
bool
egd_isValidModifiedWeight(
	const Numeric_t		w)
{
	return (::pgl::isValidFloat(w) && (0.0 <= (w)));
};


/**
 * \brief	This function returns true if gWeight is a
 * 		 valid grid weight.
 *
 * In the case the grid weight weight was processed this function
 * applies the egd_isValidWeight(Numeric_t) function to the grid weights.
 * If this was not processed, is is only tested for a valid float
 * and if the weight is non negative.
 *
 * \param  gWeight		The grid weight to check.
 * \param  wasProcessed 	Indicates if processed or not.
 */
bool
egd_isValidWeight(
	const egd_gridWeight_t& 	gWeight,
	const bool 			wasProcessed);


/**
 * \brief	This function returns true if mWeight
 * 		 is a valid marker weight.
 *
 * \param  mWeight	The marker weights.
 */
bool
egd_isValidWeight(
	const egd_markerWeight_t& 	mWeight);


/**
 * \brief	This is a function that can be used to test and
 * 		 normalize a component.
 *
 * Componets are encountered when interpolation is performed. It
 * is the distance between the marker and nbext left/top grid node
 * divided by the length of the cell where the marker is.
 * This function will test if the value is approximatly correct
 * and check this by asserts. It will also normalize it, meaning
 * values that are just a bit above 1 will be set to one and
 * the same happens with zero.
 *
 * \param  comp 	This is the component to normalize.
 *
 * \note 	If asserts are disabled and the value is too large
 * 		 (error large) then NAN is returned.
 */
inline
Numeric_t
egd_normalizeWeightComp(
	const Numeric_t& 	comp)
{
	pgl_assert(::pgl::isValidFloat(comp));

	const bool tooLow   = (comp < 0.0) ? true : false;
	const bool tooLarge = (comp > 1.0) ? true : false;

	if(!tooLarge && !tooLow)    	//We are right so we can return
	{
		return comp;
	}
	else if(tooLarge) 		//We are too large
	{
		if(pgl::approxTheSame(comp, 1.0, 100000) == true)
		{
			return 1.0;
		}
#			if PGL_DEBUG_ON
#			pragma message "Extended debugging in normalize weights."
			std::cout
				<< "==============================\n"
				<< "\t\tExtended Debugging Output\n"
				<< "Too large component was detected it was!\n"
				<< "The weight part was: " << std::setprecision(18) << std::fixed << comp << "\n"
				<< "==============================\n"
				<< std::endl;
#			endif
			pgl_assert(false && "Detected a very large component.");

		return NAN;
	//End: too large component
	}
	else				//We are too small
	{
		if(::pgl::isVerySmall(comp, 100000) == true)
		{
			return 0.0;
		};
#			if PGL_DEBUG_ON
#			pragma message "Extended debugging in normalize weights."
			std::cout
				<< "==============================\n"
				<< "\t\tExtended Debugging Output\n"
				<< "Too small component was detected it was!\n"
				<< "The weight part was: " << std::setprecision(18) << std::fixed << comp << "\n"
				<< "==============================\n"
				<< std::endl;
#			endif
			pgl_assert(false && "Detected a very small component.");

		return NAN;
	}; //End: too small components

	//unreacable code
		pgl_assert(false && "Reached unreachable code");
	return NAN;
}; //End: normalize




/**
 * \brief	This function is used to generate a meshgrid from
 * 		 the coordinate vector.
 *
 * This function is similarly to the function that is provided by
 * matlab and numpy. Assuming we have Nx grid points in x direction
 * and Ny in y direction, then this function returns TWO $ Ny \times Nx $
 * matrices. Note that the first matrix that is returned is the
 * X matric and second is the Y matrix.
 * Also note that the imput argument have the same order.
 *
 * \param  xPoints	This is the vector with the x positions.
 * \param  yPoints 	This is the vector with the y positions.
 */
std::pair<egd_Array_t<Numeric_t>, egd_Array_t<Numeric_t> >
egd_meshgrid(
	const egd_Vector_t<Numeric_t>& 		xPoints,
	const egd_Vector_t<Numeric_t>& 		yPoints);


/**
 * \brief	This function accepts a pair as argument of
 * 		 of the meshgrid operation.
 *
 * This allows interpolerability with the grid geometry.
 *
 * \param  gridPointPair 	First x then y.
 */
std::pair<egd_Array_t<Numeric_t>, egd_Array_t<Numeric_t> >
egd_meshgrid(
	const ::std::pair<egd_Vector_t<Numeric_t>, egd_Vector_t<Numeric_t> >& 		gridPointPair);


PGL_NS_END(egd)


