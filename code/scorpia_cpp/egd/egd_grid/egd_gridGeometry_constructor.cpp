/**
 * \brief	This file includes the construction code of the grid geometry object.
 *
 * This involves not only the constructor but also the finalize and the set up code.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_gridGeometry.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_linalg/pgl_EigenCore.hpp>			//Include the main LINALG header


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)


egd_gridGeometry_t::egd_gridGeometry_t()
 = default;


egd_gridGeometry_t::egd_gridGeometry_t(
	const yNodeIdx_t 			Ny,
	const xNodeIdx_t 			Nx)
 :
  egd_gridGeometry_t()		//call the default constructor
{
	//Allocate the space
	this->allocateGrids(Ny, Nx);
}; //End: building constructor


/*
 * ======================
 * Functions
 *
 * These functions are not constructors, but serves
 * a simillar need, thus they are implemented here.
 */

void
egd_gridGeometry_t::allocateGrids(
	const yNodeIdx_t 			Ny_,
	const xNodeIdx_t 			Nx_)
{
	const Index_t Nx = Nx_;	//Convert the argument in the explicit base type.
	const Index_t Ny = Ny_;

	//Consistency checks
	pgl_assert(m_xPoints == m_yPoints, m_xPoints == 0);	//This comes from the initialization

	//Check input arguments
	if(Nx <= 1)	//We need at least two grid points
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in x direction is invalid, it was " + std::to_string(Nx));
	};
	if(Ny <= 1)
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in y direction is invlid, it was " + std::to_string(Ny));
	};

	//Check state
	if(m_isGridSet == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate a grid which was already set.");
	};
	if(m_xPoints != 0) 	//This is our test for allready allocated
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate an already allocated grid.");
	};

	/*
	 * Setting the values
	 */
	m_xPoints   = Nx;
	m_yPoints   = Ny;
	m_isGridSet = false;	//is already the case, but I like it
	//propery list is loaded when the grids are loded (because of the positions).


	/*
	 * Allocatre the grid points
	 */
	m_xGrid.setZero(Nx);
	m_yGrid.setZero(Ny);

	return;
}; //End: allocateGrid


void
egd_gridGeometry_t::makeConstantGrid(
	const Numeric_t 	xStart,
	const Numeric_t 	xEnd,
	const Numeric_t 	yStart,
	const Numeric_t 	yEnd)
{
	using ::pgl::isValidFloat;

	//Test consistency
	if(this->isGridSet() == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not make a constant grid, the grid is already set.");
	};
	if(this->m_isConstGrid == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not make a constant grid, it is already a constant grid.");
	};
	if(this->m_xPoints == 0)		//0 indicates not allocated.
	{
		throw PGL_EXCEPT_LOGIC("The grid is not allocated.");
	};
	pgl_assert(m_yPoints > 0); //Only for safty

	//Test the input
#define TEST(x) if(isValidFloat(x) == false) {throw PGL_EXCEPT_InvArg(#x "is an invalid number.");}
	TEST(xStart);
	TEST(xEnd);
	TEST(yStart);
	TEST(yEnd);
#undef TEST

	if(!(xStart < xEnd))
	{
		throw PGL_EXCEPT_InvArg("xStart (" + std::to_string(xStart) + ") is larger than xEnd (" + std::to_string(xEnd) + ")");
	};
	if(!(yStart < yEnd))
	{
		throw PGL_EXCEPT_InvArg("yStart (" + std::to_string(yStart) + ") is larger than yEnd (" + std::to_string(yEnd) + ")");
	};


	//
	//Creating the grid
	m_xGrid.setLinSpaced(m_xPoints, xStart, xEnd);
	m_xSpacing = m_xGrid[1] - m_xGrid[0];

	m_yGrid.setLinSpaced(m_yPoints, yStart, yEnd);
	m_ySpacing = m_yGrid[1] - m_yGrid[0];

	//Mark the grid as constant
	m_isConstGrid = true;
		pgl_assert(this->isConstantGrid());	//Will also perform consistency checks.

	return;
}; //End: makeConstantGrid


void
egd_gridGeometry_t::finalizeGrids()
{
	if(m_isGridSet == true)
	{
		throw PGL_EXCEPT_RUNTIME("Finalizing of the grid points failed. The points where already finalized.");
	};
	if(m_xPoints != m_xGrid.size())
	{
		throw PGL_EXCEPT_RUNTIME("Finalizing the grid points failed in x coordinates. The vector has " + std::to_string(m_xGrid.size()) + " poinst, but it should have " + std::to_string(m_xPoints));
	};
	if(m_yPoints != m_yGrid.size())
	{
		throw PGL_EXCEPT_RUNTIME("Finalizing the grid points failed in y coordinates. The vector has " + std::to_string(m_yGrid.size()) + " poinst, but it should have " + std::to_string(m_yPoints));
	};

	//This will performs some consistency checks
	(void)(this->isConstantGrid());

	//Now set it
	m_isGridSet = true;

	return;
}; //End: finalizing grid


PGL_NS_END(egd)

