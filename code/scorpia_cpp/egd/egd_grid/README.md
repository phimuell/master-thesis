# Info
This folder provides some very basic classes.
These classes allows a nice and compact handling of the grid and the marker.
It is very important to realize that this folder also contains the marker.

For example a marker has many properties, thus passing a marker involves passing many arguments, one for each maker property.
Thus a class is provided that collects all the marker into one class and allows accessing them easily.

It will also contains code that deals with handling the grid.


## Note
Some components stored in this grid should actually be located in the EGD_UTIL folder.


# The grid
We operate on a grid, to make things more exciting, we have not one grid but
many grids and depending on the situation we need different ones.

Fortunately we need the physical properties only at the basic nodal
point which is quite good, because we need just one interpolation code to get the values from the markers to the grid.
However some need special treatment, such as the temperature.

## The Grid Layout
Now we will discuss the layout of the grid.
If we have $N$ grid points then we have $N – 2$ inner grid points, the two other grid points are located at the edges of the domain.
The domain, or to be specific, the basic nodal points are mapped into a matrix.

 	o------------------------------------------>  X / J
 	|
 	|
 	|
 	|
 	|
 	|
	|
	|
	|
	|
	V
	  Y / I

The orientation of the grid is quite special and, at first unintuitive.
The x axis points to the right, meaning going to the right increases the x coordinate.
The y axis points downwards, meaning doing down will increase the y coordinate.
The idea behind y is that is should be interpreted as depth, and depth is a positive
quantity and here we are interested in depth and not height.


### The Matrix Layout
Now we can also map this into a matrix.
The prototype (code from lecture) is written in MATLAB and thus indexing starts with 1.
Scorpia/egd is written in C++ thus indexing starts at 0.

Lets assume that the Matrix RHO(:, :) stores the density at the basic nodal points.
The matrix entry RHO(i, j), now designates the density at a certain basic nodal point.
It is the ith in y direction and the jth in x direction.
This means that the Y index comes first and the X index second.
For the sake of compatibility we will also use that scheme.

	A(Y_IDX, X_IDX)


#### Matrix Order
Since we use Eigen we will use column major order, which is the default ordering in Eigen.
This means that iterating over the first index (Y) is better than iterating over the second index.
However in certain situations we will use matrices that uses row major ordering.


