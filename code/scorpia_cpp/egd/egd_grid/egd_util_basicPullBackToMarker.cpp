/**
 * \brief	This function performs performs the mapping from the grid back to markers.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridType.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/dynamic_bitset.hpp>


//Include STD


PGL_NS_START(egd)


/**
 * \brief	This funnction is the worker function.
 *
 * This function is the real implementation, since it again handles
 * some special cases. In this case we only have to care about
 * non existing nodes, they can only apear in the case of non
 * extended grids, so we template for that.
 * Also there is only the case allowed where we have one difference
 * in the zize of the property.
 *
 * \param  mProperty	The marker property that should be condidered.
 * \param  gProperty	This is the target that will be filled.
 * \param  mWeights	This are the individual marker weights.
 * \param  idxI 	The first matrix indexes.
 * \param  idxJ 	The second matrix indexes.
 * \param  updateMarker	Update the marker instead of over writing.
 *
 * \tparam 	isExtended	bool to indicate if we are in an extended setting.
 * \tparam 	updateMarker	Should the markers be updated.
 *
 * This function does not perform consitency checks which are assumed have
 * been done by the calling function.
 */
template<
	bool 	isExtended,
	bool 	updateMarker
>
void
egdInternal_pullMarkerBackToGrid(
	egd_markerProperty_t* const 		mProperty_,
	const egd_gridProperty_t& 		gProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const bool 				updateMarkerArg);	//Only for consistency



// This is the front implementation, that is called by the user fucntion
void
egd_pullBackToMarker(
	egd_markerProperty_t* const 		mProperty_,
	const egd_gridProperty_t& 		gProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const bool 				updateMarker)
{
	pgl_assert(mProperty_ != nullptr);
	egd_markerProperty_t& mProperty = *mProperty_;	//Alias the marker property.

	//Make some tests
	const Index_t nMarkers = egd_getNMarkers(mWeights);

	if(nMarkers <= 0)
	{
		throw PGL_EXCEPT_InvArg("Too few markers.");
	};

	//Test if we have to resize the marker
	if(nMarkers != mProperty.size())
	{
		if(updateMarker == false)
		{
			mProperty.resize(nMarkers);
			mProperty.setConstant(0.0);
		}
		else
		{
			//In the case we want to update the markers
			//we should not allow resize, since we have to
			//update something, this something has to exists
			throw PGL_EXCEPT_InvArg("Requested an update of markers but only "
					+ std::to_string(mProperty.size()) + " are given, but needs " + std::to_string(nMarkers));
		};
	}; //End if: resize the marker property

	if(gProperty.hasValidGridType() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid property has no grid type.");
	};

	if(nMarkers != (Index_t)idxI.size())
	{
		throw PGL_EXCEPT_InvArg("The number of indexes in i is wrong.");
	};
	if(nMarkers != (Index_t)idxJ.size())
	{
		throw PGL_EXCEPT_InvArg("The number of indexes in j is wrong.");
	};

	const bool isExt = ::egd::isExtendedGrid(gProperty.getType());

#define CALL(ext, up) 	if(((ext) == isExt) && ((up) == updateMarker)) 			\
			{								\
				egdInternal_pullMarkerBackToGrid<ext, up>(		\
				&mProperty,						\
				gProperty,						\
				mWeights,						\
				idxI, idxJ,						\
				updateMarker);						\
			}
	CALL(true , true );
	CALL(true , false);
	CALL(false, true );
	CALL(false, false);
#undef CALL

	return;
}; //End: pullBackToMarker face implementation.


/*
 * ==============================================
 */
template<
	bool 	isExtended,
	bool 	updateMarker
>
void
egdInternal_pullMarkerBackToGrid(
	egd_markerProperty_t* const 		mProperty_,
	const egd_gridProperty_t& 		gProperty,
	const egd_markerWeight_t& 		mWeights,
	const egd_indexVector_t& 		idxI,
	const egd_indexVector_t& 		idxJ,
	const bool 				updateMarkerArg)
{
	using pgl::isValidFloat;

	pgl_assert(mProperty_ != nullptr);
	egd_markerProperty_t& mProperty = *mProperty_;	//Alias the marker property.

	if(updateMarkerArg != updateMarker)
	{
		throw PGL_EXCEPT_RUNTIME("Wrong template parameter.");
	};

	if(::egd::isExtendedGrid(gProperty.getType()) != isExtended)
	{
		throw PGL_EXCEPT_RUNTIME("Called a wrong method.");
	};

	const Index_t nMarkers = mProperty.size();	//Load the number of markers

	//load the size of the grid property
	const Index_t gRows = gProperty.rows();
	const Index_t gCols = gProperty.cols();
		pgl_assert(gRows >= 1, gCols >= 1);

	//These are the last accessable indexes
	const Index_t iLastIdx = gRows - 1;
	const Index_t jLastIdx = gCols - 1;


	/*
	 * Going through the markers and map them back
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//We load now the weights of the markers
		const Numeric_t omega_ij   = mWeights[0][0][m];
		const Numeric_t omega_i1j  = mWeights[1][0][m];
		const Numeric_t omega_ij1  = mWeights[0][1][m];
		const Numeric_t omega_i1j1 = mWeights[1][1][m];
			pgl_assert(egd_isValidWeight(omega_ij  ), egd_isValidWeight(omega_i1j ),
				   egd_isValidWeight(omega_ij1 ), egd_isValidWeight(omega_i1j1) );

		//Load the values
		const Index_t i = idxI[m];
		const Index_t j = idxJ[m];
			pgl_assert(0 <= i, i <= iLastIdx,
				   0 <= j, j <= jLastIdx );


		if(isExtended == true)
		{
			/*
			 * This is the extended case, so we can simply load all values that we need
			 * since we can be sure that they will extsis
			 */
			const Numeric_t gProp_ij   = gProperty.rawAccess(i    , j    );
			const Numeric_t gProp_ij1  = gProperty.rawAccess(i    , j + 1);
			const Numeric_t gProp_i1j  = gProperty.rawAccess(i + 1, j    );
			const Numeric_t gProp_i1j1 = gProperty.rawAccess(i + 1, j + 1);
				pgl_assert(isValidFloat(gProp_ij  ), isValidFloat(gProp_i1j ),
					   isValidFloat(gProp_ij1 ), isValidFloat(gProp_i1j1) );


			//Now perform the pull back
			const Numeric_t newM =   (  omega_ij   * gProp_ij
				                  + omega_i1j  * gProp_i1j)
				               + (  omega_ij1  * gProp_ij1
				                  + omega_i1j1 * gProp_i1j1);
				pgl_assert(isValidFloat(newM));


			if(updateMarker == false)
			{
				//Write the result back
				mProperty[m]  = newM;
			}
			else
			{
				//update the result
				mProperty[m] += newM;
			};

		//End if: extended case
		}
		else
		{
			/*
			 * We are not in the extended case, so we must check
			 * if we have a problem. The node does not exists
			 * if i or j is equal the last valid node.
			 */
			const bool saveI = (i != iLastIdx) ? true : false;
			const bool saveJ = (j != jLastIdx) ? true : false;

			//Load the grid value values
			const Numeric_t gProp_ij   =                  gProperty.rawAccess(i    , j    )      ;	//Always exists
			const Numeric_t gProp_i1j  = saveI          ? gProperty.rawAccess(i + 1, j    ) : 0.0;
			const Numeric_t gProp_ij1  =          saveJ ? gProperty.rawAccess(i    , j + 1) : 0.0;
			const Numeric_t gProp_i1j1 = saveI && saveJ ? gProperty.rawAccess(i + 1, j + 1) : 0.0;
				pgl_assert(isValidFloat(gProp_ij  ), isValidFloat(gProp_i1j ),
					   isValidFloat(gProp_ij1 ), isValidFloat(gProp_i1j1) );

			//Now perform the pull back
			const Numeric_t newM =   (  omega_ij   * gProp_ij
				                  + omega_i1j  * gProp_i1j)
				               + (  omega_ij1  * gProp_ij1
				                  + omega_i1j1 * gProp_i1j1);
				pgl_assert(isValidFloat(newM));

			if(updateMarker == false)
			{
				//Write the result back
				mProperty[m]  = newM;
			}
			else
			{
				mProperty[m] += newM;
			};

		}; //End else: handle regular case
	}; //End for(m):

	return;
}; //End: worker implementation






PGL_NS_END(egd)



