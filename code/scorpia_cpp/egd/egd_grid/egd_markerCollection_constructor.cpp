/**
 * \brief	This file implements the constructing process of the marker collection.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl_linalg/pgl_EigenCore.hpp>			//Include the main LINALG header
#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD



PGL_NS_START(egd)

/*
 * This is technically also a constructor
 */
void
egd_markerCollection_t::allocateMarkers(
	const uSize_t 		nMarkers,
	const PropList_t&	propList)
{
	if(nMarkers <= 0)
	{
		throw PGL_EXCEPT_InvArg("Passed a negative or zero number as, " + std::to_string(nMarkers) + ", as nMarkers.");
	};
	if(propList.size() == 0)
	{
		throw PGL_EXCEPT_InvArg("The passed propList is empty.");
	};

	//It is only allowed to allocate markers if the size is zero
	//This would mean a reallocation
	if(m_nMarkers != 0)
	{
		throw PGL_EXCEPT_LOGIC("Tried to reallocate the marker collection.");
	};

	//Make some consistency checks
	pgl_assert(m_propList.empty());
	pgl_assert(m_markerMap.empty());


	/*
	 * Set the variable
	 */
	m_nMarkers = nMarkers;
	m_propList = propList;

	/*
	 * Allocate the markers
	 */
	for(const PropIdx_t propRaw : m_propList)
	{
		if(propRaw.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid property.");
		};
		if(propRaw.isRepresentant() == false)
		{
			throw PGL_EXCEPT_InvArg("Passed a not representant to the construcotr of the marker collection."
					" The raw index was " + propRaw + ", its representant is " + propRaw.getRepresentant() );
		};

		//Now call the prepresentation
		const PropIdx_t prop = propRaw.getRepresentant();

		// test if the prop was already there
		if(m_markerMap.count(prop) != 0)
		{
			throw PGL_EXCEPT_InvArg("The property \"" + prop.print()
					+ "\" was given twice, the raw property was \""
					+ propRaw.print() + "\".");
		};

		//Create it
		(void)m_markerMap[prop];

		//Set it to zero
		m_markerMap.at(prop).setZero(m_nMarkers);
	}; //End for(prop): allocate the markers


	if(m_markerMap.count(PropIdx::PosX()) != 1)
	{
		throw PGL_EXCEPT_InvArg("No x position was given, when the marker collection was constructed.");
	};
	if(m_markerMap.count(PropIdx::PosY()) != 1)
	{
		throw PGL_EXCEPT_InvArg("No y position was given, when the marker collection was constructed.");
	};

	pgl_assert(m_markerMap.size() == m_propList.size(),
		   m_propList.size() == propList.size());

	return;
}; //End: allocateMarker



egd_markerCollection_t::PropToGridMap_t
egd_markerCollection_t::getDefaultPropToGridMap()
 const
{
	PropToGridMap_t ret;	//This we will return

	// Going through the markers to check
	for(const auto& it : m_markerMap)
	{
		const auto prop = it.first;	//Load the property

		//Exclude positions
		if(prop.isPosition() == true)
		{
			continue;
		}; //End if: positions

		//Add it and set the groid type to the default value
		ret[prop] = getDefaultGrid(prop, false);
			pgl_assert(::egd::isValidGridType(ret.at(prop)));
	}; //End for(it):

	if(ret.empty() == true)	//Check if something is found
	{
		throw PGL_EXCEPT_LOGIC("Did not fuind any grids to add to the map.");
	};

	return ret;
}; //End: getDefaultPropToGridMap()







PGL_NS_END(egd)







