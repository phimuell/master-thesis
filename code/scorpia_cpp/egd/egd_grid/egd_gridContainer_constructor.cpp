/**
 * \brief	This file includes the construction code of the grid container object.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl_linalg/pgl_EigenCore.hpp>			//Include the main LINALG header
#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <tuple>


PGL_NS_START(egd)


egd_gridContainer_t::egd_gridContainer_t()
 = default;


egd_gridContainer_t::egd_gridContainer_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const GridGeometry_t& 		gridGeo,
	ApplyBC_ptr 			applyBC,
 	const PropToGridMap_t& 		pMap)
 :
  egd_gridContainer_t()		//call the default constructor
{
	//If a applyBC was passed, load it
	if(applyBC != nullptr)
	{
		m_applyBC = std::move(applyBC);
	}; //End if: load BC
		pgl_assert(applyBC == nullptr);

	//Allocate the space
	this->allocateGrids(Ny, Nx, gridGeo, pMap);
}; //End: building constructor


egd_gridContainer_t::egd_gridContainer_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const GridGeometry_t& 		gridGeo,
 	const PropToGridMap_t& 		pMap)
 :
  egd_gridContainer_t(		//Calling the full constructor
  	Ny, Nx,			//The size
  	gridGeo,		//Geometry
  	ApplyBC_ptr(nullptr),	//No BC appllier
  	pMap)			//The map
{
}; //End: building constructor



/*
 * ======================
 * Functions
 *
 * These functions are not constructors, but serves
 * a simillar need, thus they are implemented here.
 */

void
egd_gridContainer_t::allocateGrids(
	const yNodeIdx_t 		Ny_,
	const xNodeIdx_t 		Nx_,
	const GridGeometry_t& 		gridGeo,
	const PropToGridMap_t& 		propMap)
{
	//Consistency checks
	pgl_assert(m_xPoints == m_yPoints, m_xPoints == 0);	//This comes from the initialization

	//Undo the alias, we only need them for the dispatch
	const Index_t Ny = Ny_;
	const Index_t Nx = Nx_;

	//Check input arguments
	if(Nx <= 1)	//We need at least two grid points
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in x direction is invalid, it was " + std::to_string(Nx));
	};
	if(Ny <= 1)
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in y direction is invlid, it was " + std::to_string(Ny));
	};
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid geometry is not set.");
	};
	if(propMap.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property map.");
	};
	if(propMap.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("Passed an ampty property map.");
	};


	//Check state
	if(m_grids.empty() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate a grid which was already set.");
	};
	if(m_xPoints != 0) 	//This is our test for allready allocated
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate an already allocated grid.");
	};
	if(m_gGeo.isGridSet() == true)
	{
		throw PGL_EXCEPT_LOGIC("The grid geometry of the container, was already set.");
	};

	/*
	 * Setting the values
	 */
	m_xPoints   = Nx;
	m_yPoints   = Ny;
	m_gGeo      = gridGeo;



	/*
	 * Allocate the property grids
	 */

	//Iterate through all the properties and insering them
	for(const auto& it : propMap)
	{
		const PropIdx_t prop = it.first;	//Load the property

		if(prop.isValid() == false)	//Test if the property is valid
		{
			throw PGL_EXCEPT_InvArg("One of the passed properties is invlid.");
		};

		if(prop.isPosition() == true)	//Test if prop is a psoition
		{
			continue;	//Is a position; ignore it
		};

		//Test if the property is not inserted before.
		if(m_grids.count(prop) != 0 ||
		   std::count(m_propList.cbegin(), m_propList.cend(), prop) != 0)
		{
			throw PGL_EXCEPT_LOGIC("Tried to add the property \"" + prop.print() + "\" twice.");
		};

		//
		//Now we will create the grid and allocate it
		GridProperty_t thisProp(
				yNodeIdx_t(m_yPoints), xNodeIdx_t(m_xPoints),	//Here ordering is not swapped
				it.second, 		 			//The grid type
				prop);						//Load the of the property

		//
		//Inserting the propery into the map.
		//We can not use the [] operator, because this would need us,
		//to default construct a property which is not possible,
		//so we will use emplace.
		m_grids.emplace(::std::make_pair(prop, ::std::move(thisProp)));

		pgl_assert(m_grids.at(prop).Nx() == Nx,	//test if the allocation has happend
			   m_grids.at(prop).Ny() == Ny);
		pgl_assert(m_grids.at(prop).getType()    == it.second,
			   m_grids.at(prop).checkSize()  == true      );

		//
		//Insert the property index in the list of *this
		m_propList.push_back(prop);
	}; //End for(prop):

	if(m_grids.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No suitable properties where used.");
	};

	return;
}; //End: allocateGrid





PGL_NS_END(egd)


