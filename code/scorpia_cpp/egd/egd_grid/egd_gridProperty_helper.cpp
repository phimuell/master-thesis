/**
 * \brief	This function defines the implementation of the helper functions of the grid property.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_gridProperty.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD


PGL_NS_START(egd)


void
egd_setGhostNodesToZero(
	egd_gridProperty_t* const 	gProp_)
{
	if(gProp_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The passed grid property is zero.");
	};
	egd_gridProperty_t& gProp = *gProp_;

	if(gProp.checkSize() == false)
	{
		throw PGL_EXCEPT_InvArg("Detected a size error in the property.");
	};

	if(isValidGridType(gProp.getType()) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid type is too small.");
	};

	//load the grid type
	const egd_gridProperty_t::GridType_t gType = gProp.getType();

	//Test if extended or not
	const bool isExtended = isExtendedGrid(gType);

	switch(mkCase(gType))
	{
	  case eGridType::CellCenter:
	  	if(isExtended)
		{
			//Grids of this size do not have
			//ghost nodes.
			break;
		}; //End if: ghost node handling

		gProp.rawRowAccess(0) = 0.0;
		gProp.rawColAccess(0) = 0.0;
	  break;

	  case eGridType::StPressure:
		//This is always
		gProp.rawRowAccess(0) = 0.0;
		gProp.rawColAccess(0) = 0.0;

		if(isExtended == true)
		{
			gProp.rawRowAccess(gProp.rows() - 1) = 0.0;
			gProp.rawColAccess(gProp.cols() - 1) = 0.0;
		};
	  break;

	  case eGridType::StVx:
	  	if(isExtended == true)
		{
			gProp.rawColAccess(gProp.cols() - 1) = 0.0;
		}
		else
		{
			gProp.rawRowAccess(0) = 0.0;
		};
	  break;

	  case eGridType::StVy:
	  	if(isExtended == true)
		{
			gProp.rawRowAccess(gProp.rows() - 1) = 0.0;
		}
		else
		{
			gProp.rawColAccess(0) = 0.0;
		};
	  break;

	  case eGridType::BasicNode:
	  	if(isExtended == true)
		{
			throw PGL_EXCEPT_InvArg("A basic grid is always regular.");
		};

		/*
		 * Nothing to do here
		 */
	  break;

	  default:
	  	throw PGL_EXCEPT_InvArg("Passed unkown grid type to the function.");
	}; //End switch(gType):


	return;

	return;
}; //End: manipulate the property



PGL_NS_END(egd)

