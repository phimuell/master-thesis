/**
 * \brief	This file contains functions to disable the ghost nodes
 * 		 in the grid weights, by setting them to zero.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridType.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

//Include BOOST
#include <boost/dynamic_bitset.hpp>


//Include STD


PGL_NS_START(egd)

bool
egd_disableGhostNodes(
	egd_gridWeight_t* const 	gWeight_,
	const eGridType 		gType)
{
	if(gWeight_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The grid weight argument is the null ptr.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid type is too small.");
	};

	egd_gridWeight_t& gWeight = *gWeight_;	//Aliasng

	//Test if extended or not
	const bool isExtended = isExtendedGrid(gType);

	//this bool is used for checking, if ghost nodes where manipilated.
	bool hadGhostNodes = false;

	switch(mkCase(gType))
	{
	  case eGridType::CellCenter:
	  	if(isExtended)
		{
			//Grids of this size do not have ghost nodes.
			break;
		}; //End if: ghost node handling

		gWeight.row(0) = 0.0;
		gWeight.col(0) = 0.0;
		hadGhostNodes = true;
	  break;

	  case eGridType::StPressure:
		//This is always
		gWeight.row(0) = 0.0;
		gWeight.col(0) = 0.0;

		if(isExtended == true)
		{
			gWeight.row(gWeight.rows() - 1) = 0.0;
			gWeight.col(gWeight.cols() - 1) = 0.0;
		};

		hadGhostNodes = true;
	  break;

	  case eGridType::StVx:
	  	if(isExtended == true)
		{
			gWeight.col(gWeight.cols() - 1) = 0.0;
		}
		else
		{
			gWeight.row(0) = 0.0;
		};
		hadGhostNodes = true;
	  break;

	  case eGridType::StVy:
	  	if(isExtended == true)
		{
			gWeight.row(gWeight.rows() - 1) = 0.0;
		}
		else
		{
			gWeight.col(0) = 0.0;
		};
		hadGhostNodes = true;
	  break;

	  case eGridType::BasicNode:
	  	if(isExtended == true)
		{
			throw PGL_EXCEPT_InvArg("A basic grid is always regular.");
		};

		/*
		 * Nothing to do here
		 */
	  break;

	  default:
	  	throw PGL_EXCEPT_InvArg("Passed unkown grid type to the function.");
	}; //End switch(gType):

	//Check if we have made the changes we must, or not do them of no were there
	if(hadGhostNodes != egd_gridGeometry_t::hasGridGhostNodes(gType))
	{
		throw PGL_EXCEPT_RUNTIME("The grid should have ghost node, but no were set.");
	};

	//Return the result.
	return hadGhostNodes;
}; //End: disable ghost nodes on grid.



PGL_NS_END(egd)



