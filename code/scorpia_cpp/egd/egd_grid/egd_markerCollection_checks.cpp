/**
 * \brief	This file implements the testing process of the marker collection.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_hashmap.hpp>



//Include the Eigen Headers


//Include STD



PGL_NS_START(egd)

bool
egd_markerCollection_t::checkConsistency()
 const
 noexcept
{
	/*
	 * This is a simple test
	 */

	if(m_nMarkers < 0)
	{
		//this is a serious probelm
		pgl_assert(false && "negative number of markers.");
		return false;
	};

	//We allways requiere that
	if(m_markerMap.size() != m_propList.size())
	{
		pgl_assert(false && "The number of properties and marker vectors is not the same.");
		return false;
	};

	//in the case of zero markers, we are consitent
	//but requiere that no property is present
	if(m_nMarkers == 0)
	{
		if(m_markerMap.size() != 0)
		{
			pgl_assert(false && "No markers but property list is non empty.");
			return false;
		};

		//We are consistent now
		return true;
	}; //End if: case when zero markers are present


	//Test if the properties has the correct size
	for(const auto& it : m_markerMap)
	{
		if(it.first.isValid() == false)
		{
			pgl_assert(false && "An invalid property index was found.");
			return false;
		};

		if(it.second.size() != m_nMarkers)
		{
			pgl_assert(false && "The size of a marker array is not correct.");
			return false;
		};
	}; //End for(it):

	Size_t nIndexes = 0;	//The number of known indexes

	// Make a small consistency checks about the indexes
	for(const auto& it : m_idxMap)
	{
		nIndexes += it.second.size();
	}; //End for(it):

	if(!(nIndexes <= m_nMarkers))
	{
		pgl_assert(false && "Index map error detected.");
		return false;
	};

	//All is correct so return
	return true;
}; //End: checkConsistency


bool
egd_markerCollection_t::checkValues()
 const
 noexcept
{
	//Perform a simple consitency check
	if(this->checkConsistency() == false)	//Will call assert on its own
	{
		return false;
	};

	//Handle the case of zero markers, in that case
	//we have allready tested everything
	if(m_nMarkers == 0)
	{
		return true;
	}; //end if: hande zero marker case


	/*
	 * Testing all the values
	 */
	for(const auto& it : m_markerMap)
	{
		if(it.second.array().isFinite().all() == false)
		{
			pgl_assert(false && "Found a non finite value.");
			return false;
		};

		//Test the non negativity of the values
		if(it.first.isPhysical() == true && 	//Physical parameters need to be positive
		   !(					//but certain properties can be negative
		     it.first.isVelocity() ||		//like the velocity
		     it.first.isPosition()		//or the position
		    ))
		{
			if((it.second.array() >= 0.0).all() == false)
			{
				pgl_assert(false && "Non-Negativity constraint is violated.");
				return false;
			};
		};
	}; //End for(it): going through all the markers.

	//Tets if the index are consistent
	if(m_idxMap.size() > 0)
	{
		IndexVector_t fullIdxList;
		fullIdxList.reserve(m_nMarkers);
		const auto& typeProp = this->cgetMarkerProperty(PropIdx_t::Type());

		for(const auto& it_map : m_idxMap)
		{
			//Load the type
			const Index_t it_type = it_map.first;

			for(const Index_t& m : it_map.second)
			{
				//Test if the type is wrong
				if(Index_t(typeProp[m]) != it_type)
				{
					pgl_assert(false && "Type was indexed wrongly.");
					return false;
				}; //ENd if: wrong type

				//Add the current index to the full index list
				fullIdxList.push_back(m);
			}; //End for(m)
		}; //End for(it_map):

		/*
		 * We have now all known indexes, so we now check them
		 */
		std::sort(fullIdxList.begin(), fullIdxList.end());	//Sort such that the unique test can work.

		if(pgl::is_unique(fullIdxList.cbegin(), fullIdxList.cend()) == false)
		{
			pgl_assert(false && "Found one index multiple time.");
			return false;
		}; //ENd if: not unigue
	}; //End if: test if index are consistent

	//If we are here we have a valid object
	return true;
}; //End: checkValue


PGL_NS_END(egd)







