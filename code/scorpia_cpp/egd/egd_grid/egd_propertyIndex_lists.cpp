/**
 * \brief	This file implements property indexes for the markers.
 *
 * This file solves the problem of the easy accessing of different marker properties.
 * It basically allows to iterate over the different markers.
 * It is like an index, but not numbers but properties.
 *
 * It does this in the following way.
 * It first defines an enum that is used to name the properties.
 * Then a class is created with allows for some syntactic sugar.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridProperty.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl_set.hpp>
#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>
#include <pgl/pgl_algorithm.hpp>


//Include BOOST
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/stringize.hpp>


//Include STD
#include <iterator>
#include <string>
#include <iostream>
#include <functional>


PGL_NS_START(egd)

#if defined(EGD_PROPIDX_USE_ITERATOR) && (EGD_PROPIDX_USE_ITERATOR != 0)
#	pragma GCC warning "The property index does provide iterator functions, they are depricated."
#else
#	pragma message "The property index does not provide iterator functions."
#endif

bool
egd_propertyIndex_t::isRepresentedBy(
	const egd_gridProperty_t& 	other)
 const
 noexcept
{
	return this->isRepresentedBy(other.getPropIdx());
};  //End: rep by


#if defined(EGD_PROPIDX_USE_ITERATOR) && (EGD_PROPIDX_USE_ITERATOR != 0)
egd_propertyIndex_t::PropList_t
egd_propertyIndex_t::allList()
{
	PropList_t pl;
	for(const auto it : egd_propertyIndex_t::all())
	{
		pl.push_back(it);
	};
	return pl;
};


egd_propertyIndex_t::PropList_t
egd_propertyIndex_t::posList()
{
	PropList_t pl;
	for(const auto it : egd_propertyIndex_t::pos())
	{
		pl.push_back(it);
	};
	return pl;
};


egd_propertyIndex_t::PropList_t
egd_propertyIndex_t::physicalList()
{
	PropList_t pl;
	for(const auto it : egd_propertyIndex_t::physical())
	{
		pl.push_back(it);
	};
	return pl;
};
#endif


bool
egd_propertyIndex_t::ensureProperty(
	PropList_t* const 		pList_,
	const egd_propertyIndex_t& 	prop)
{
	pgl_assert(pList_ != nullptr);
	PropList_t& pList = *pList_;

	if(prop.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property.");
	};

	bool didWeFoundIt = false;

	//look through the list to find the property
	for(const auto& p : pList)
	{
		if(p.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed list contained an invalid property.");
		};

		if(p == prop)
		{
			//We found the property in the list, but we will not break,
			//because the function guarantees a full scann
			didWeFoundIt = true;
		}; //End if: we found the property
	}; //End for(o):


	//If we did not found it, then we have to add it
	if(didWeFoundIt == false)
	{
		pList.push_back(prop);
		return true;
	}; //End if: we had to modify the list

	return false;	//No modification needed
}; //End: ensure property


bool
egd_propertyIndex_t::isValidList(
	const PropList_t& 		pList,
	const bool 			useRep)
{
	if(pList.empty() == true)	//Trivial case
	{
		return true;
	}; //End if: list is empty

	/*
	 * Test if avalid and if they are representant
	 * if requested.
	 */
	for(const egd_propertyIndex_t& pIdx : pList)
	{
		if(pIdx.isValid() == false)
		{
			pgl_assert(false && "Detected an invalid property index.");
			return false;
		};
		if(useRep == true)
		{
			if(pIdx.isRepresentant() == false)
			{
				pgl_assert(false && "Detected a non marker property.");
				return false;
			};
		}; //End if: use rep
	};//End for(pIdx):


	/*
	 * Going through the list and check if unique
	 */
	::pgl::pgl_set_t<egd_propertyIndex_t> uniqueProps;	//unique by default
	for(const auto& p : pList)
	{
		if(uniqueProps.count(p) == 0)
		{
			//Not inside, so unique
			uniqueProps.insert(p);
		}
		else
		{
			//We have found them twiche
			pgl_assert(false && "Detected a property twice in a list.");
			return false;
		};
	}; //End for(p)

	/*
	 * We found nothing
	 */
	return true;
}; //End: is valid list


bool
egd_propertyIndex_t::isPropertyInList(
	const PropList_t&		pList,
	const egd_propertyIndex_t& 	pIdx)
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index is invalid.");
	};

	if(pList.empty() )
	{
		return false;
	};

	for(const egd_propertyIndex_t& p : pList)
	{
		if(p.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("WHile searching for property " + pIdx + ", encounted an invalid index.");
		};

		if(p == pIdx)
		{
			return true;
		};
	}; //End for(pIdx):

	return false;
};//ENd: isPropertyInList


PGL_NS_END(pgl)


