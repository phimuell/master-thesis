#pragma once
/**
 * \brief	This file implements property indexes for the markers.
 *
 * This file solves the problem of the easy accessing of different marker properties.
 * It basically allows to iterate over the different markers.
 * It is like an index, but not numbers but properties.
 *
 * It does this in the following way.
 * It first defines an enum that is used to name the properties.
 * Then a class is created with allows for some syntactic sugar.
 */

//Include the confg file
#include <egd_core.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>
#include <pgl_preprocessor.hpp>

#include <pgl_vector.hpp>
#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/stringize.hpp>


//Include STD
#include <iterator>
#include <string>
#include <iostream>
#include <functional>


/**
 * \brief	This macro allows to enable the iterator functions.
 * \define	EGD_PROPIDX_USE_ITERATOR
 *
 * If this macro is set to a value that is not zero the property
 * index will expose iterator like functions. These functions are
 * depricated, and should not be used any longer, so the default
 * is to deactivate them.
 */
#if !defined(EGD_PROPIDX_USE_ITERATOR)
#	define EGD_PROPIDX_USE_ITERATOR 0
#endif



/**
 * \define 	EGD_MARKER_PROPERTY_LIST
 * \brief	This is a list that names all the properties
 * 		 that are used.
 *
 * This is the list of the physical properties, beside position and type,
 * that the marker could have.
 * The names that are entered here are the same that are used in the index.
 * Position and type are inserted automatically. This is the only part where
 * it is needed to modify.
 *
 * Note that this relys heavaly on the preprocesser and it is not
 * expected that you understand the exact working.
 * But use only stream comments and end each line with a \ to continue on the
 * next line, also comma seperation is needed.
 *
 * All names/properties that are added here are designeted physical properties.
 * The name you choses here will enter the scoped enum eMarkerProp and the
 * index class as well.
 *
 * After each property you have to make a comma.
 * The comma is needed also after the last entry.
 *
 * Note that some quentities are defined at different points.
 * Also note that some quantities are defined at different points.
 * Use the representatnt to find out which is the over property.
 *
 * Also the grid type provides a function to get the standard grid type for a given
 * property, not that it does not work always.
 */
#define EGD_MARKER_PROPERTY_LIST 	\
	Density,		/* This is the density of the markers;  [kg / m^3] 					*/	\
	DensityVX,		/* Density at VX node points. 								*/      \
	DensityVY,		/* Density at VY node points.								*/	\
	DensityP, 		/* Density at the pressure points.							*/ 	\
	DensityCC,		/* Density at the Cell centre poinst.							*/	\
	Viscosity,		/* This is the viscosity;  [Pa s]							*/ 	\
	ViscosityVX,		/* Viscosity at the VX points.								*/	\
	ViscosityVY, 		/* Viscosity at VY points.								*/	\
	ViscosityP, 		/* Viscosity at the pressure points.							*/ 	\
	ViscosityCC,		/* Viscosity at the cell centre points.							*/	\
	Temperature,		/* This is the temperature;  [K]							*/	\
	TemperatureCC, 		/* Temperature at the extended cell centre points.					*/	\
	TemperaturePlotBG,	/* Temperature at the basic grid, is used for plotting only				*/	\
	TemperaturePlotCC,	/* Temperature at cell centre, only used for plotting. 					*/ 	\
	TempDiff,		/* This is a pseudo property for the temp solver, temperature difference on grid. 	*/	\
	TempDiffCC, 		/* This is the temperature difference at the cell centre grid.				*/	\
	Pressure, 		/* This is the pressure; [Pa]								*/	\
	RhoCp,			/* This is the volumetric heat capacity times density;  [J / (K m^3)]			*/	\
	RhoCpCC, 		/* This is the volumetric heat capacity in the cell centre points.			*/	\
	ThermConduct,		/* This is the thermal conductivity;  [W / (K m)]					*/	\
	ThermConductVX, 	/* This is the thermal conductivity on the VX grid.					*/	\
	ThermConductVY, 	/* Thermal conductivity at the VY points.						*/	\
	ThermExpansAlpha,	/* This is the thermal expansion, (\alpha := \frac{1}{\rho} \d{\rho}{T});  [1 / K]	*/	\
	CompressibililityBeta,	/* This is the compressibility, (\beta := \frac{1}{\rho} \d{\rho}{p});  [1 / Pa]	*/	\
	StrainRate,		/* This is the deviatoric strain rate                                                   */      \
	StrainRateXX, 		/* This is the deviatoric starin rate in XX						*/	\
	StrainRateYX, 		/* This is the deviatoric strain rate in YX						*/ 	\
	StrainRateYY, 		/* This is the deviatoric strain rate in YY 						*/ 	\
	Stress,			/* This is the deviatoric stress                                                        */	\
	StressXX, 		/* This is the deviatoric stress in XX 							*/ 	\
	StressYX, 		/* This is the deviatoric stress in YX 							*/	\
	StressYY, 		/* This is the deviatoric stress in YY							*/ 	\
	VelX, 			/* This is the velocity in x direction.  						*/ 	\
	VelXCC,			/* X velocity on the Cell centre points.						*/ 	\
	VelY, 			/* This is the velocity in y direction.							*/ 	\
	VelYCC,			/* This is the velocity in y direction on the cell centre point.			*/	\
	RadioEnergy,		/* The radiocative energy that is produced;  [W / m^3]					*/	\
	RadioEnergyCC,		/* The radiocative energy that is produced, on CC;  [W / m^3]				*/	\
	AdiabaticHeat,		/* This is the heat that is caused by adiabatic processes; [W / m^3]			*/	\
	ShearHeat, 		/* This is the heat aused by shear effects; [W / m^3]					*/ 	\
	FullHeatingTerm,	/* This is teh full heating term that is used in the temperature equation; [W / m^3]	*/


/**
 * \brief	This macro is used to encode all different forms of the density.
 * \define 	EGD_PROPIDX_DENSITY
 *
 * This enum stores a list, comma seperated list, of all properties that should map to
 * the master density.
 */
#define EGD_PROPIDX_DENSITY Density, DensityP, DensityVX, DensityVY, DensityCC


/**
 * \brief	This macro encodes all different viscosity types that should
 * 		 be represented by the viscosity.
 * \define	EGD_PROPIDX_VISCOSITY
 */
#define EGD_PROPIDX_VISCOSITY Viscosity, ViscosityP, ViscosityVX, ViscosityVY, ViscosityCC


/**
 * \brief	This is the List of all temperatures that should map
 * 		 to the same master temperature.
 * \define	EGD_PROPIDX_TEMP
 */
#define EGD_PROPIDX_TEMP Temperature, TempDiff, TemperatureCC, TempDiffCC, TemperaturePlotBG, TemperaturePlotCC


/**
 * \brief	This is the list of all thermal conductivity that should map
 * 		 to the master Conductivity.
 * \define 	EGD_PROPIDX_THERMCOND
 */
#define EGD_PROPIDX_THERMCOND ThermConduct, ThermConductVX, ThermConductVY

/**
 * \brief	This is the list of all volumetric heat capacities, that should
 * 		 map to the master one.
 * \define 	EGD_PROPIDX_RHOCP
 */
#define EGD_PROPIDX_RHOCP RhoCp, RhoCpCC


/**
* \brief	This is the list of all X Velocities.
* \define 	EGD_PROPIDX_VELX
*/
#define EGD_PROPIDX_VELX VelX, VelXCC


/**
* \brief	This is the list of all Y Velocities.
* \define 	EGD_PROPIDX_VELY
*/
#define EGD_PROPIDX_VELY VelY, VelYCC


/**
 * \brief	This is the list of all properties that are represented by
 * 		 the radiosactive hheating
 * \define 	EGD_PROPIDX_RADIOHEAT
 */
#define EGD_PROPIDX_RADIOHEAT RadioEnergy, RadioEnergyCC


/**
 * \brief	This is the list of all strain rates.
 * \define 	EGD_PROPIDX_DEVSTRAINRATE
 */
#define EGD_PROPIDX_DEVSTRAINRATE StrainRate, StrainRateXX, StrainRateYX, StrainRateYY


/**
 * \brief	This is the list of all stresses.
 * \define 	EGD_PROPIDX_DEVSTRSS
 */
#define EGD_PROPIDX_DEVSTRSS Stress, StressXX, StressYX, StressYY


/**
 * \brief	This is the list of all plot properties.
 * \define 	EGD_PROPIDX_PLOTPROP
 */
#define EGD_PROPIDX_PLOTPROP  TemperaturePlotBG, TemperaturePlotCC


/**
 * \brief	This is a list of all special properties.
 *
 * A special property is something that is not a real property,
 * but is used for some intermediate states and operations.
 * Note that plot properties are special properties too.
 */
#define EGD_PROPIDX_SPECIALPROP PGL_EXPAND(EGD_PROPIDX_PLOTPROP) , FeelVelX, FeelVelY



PGL_NS_START(egd)

/**
 * \brief	This is the enum that is used to specify
 * 		 the different marker properties.
 * \enum 	eMarkerProp
 *
 * This enum is the raw interface to the marker properties.
 * When adding new poperties do not give them a value.
 * And only add it in the designated range.
 * Also do not forget to also adapt the class below.
 *
 * The position and type property are hardcoded added to the
 * enum already. If you want to add a new property, which
 * is a physical property, then you have to add it in the
 * definition of the macro EGD_MARKER_PROPERTY_LIST that
 * is defined above.
 *
 * The feel velocity is a special marker property. It is the
 * velocity that is actually used to move the markers. It
 * is different than the intrinsic velocity speed. Note that
 * for several reason it is a represerntative but not a
 * velocity.
 *
 * The integration into the class will be done automatically.
 */
enum class eMarkerProp : Int_t
{
	NONE = -1, 	//!< This is the invalid property; its value is -1,
			//!<  so the next one will have value 0.

	//Position parameter
	PosX,		//!< This is the x position of the markers.
	PosY,		//!< This is the y position of the markers.

	//Feel velocity
	FeelVelX,	//!< This is the feel velocity in x direction.
	FeelVelY,	//!< This is the feel velocity in y direction.

	//Type has to be the last one here
	Type,		//!< This is the type of the marker.

	// Add the physical properties entered by the user to the list.
	EGD_MARKER_PROPERTY_LIST
			//The comma is included directly in the list

	//This is for the internal control flow
	//It markes the end.
	END 			//!< This is the past the end value.
}; //End enum(eMarkerProp)



/*
 * \brief	This is an internal macro.
 * \define 	EGD_MARKER_PROPERTY_LIST_FULL
 *
 * This is like EGD_MARKER_PROPERTY_LIST but also with the
 * other properties. This is an internal macro and the user should not use it.
 */
#define  EGD_MARKER_PROPERTY_LIST_FULL 	PosX, PosY, EGD_MARKER_PROPERTY_LIST  Type, FeelVelX, FeelVelY

//Fwd of the adaptor
class egd_propertyIndexAdaptor_t;

//FWD of the class itself
class egd_gridProperty_t;


/**
 * \brief	This class wrapps the eMarkerProp enum into a class.
 * \class 	egd_propertyIndex_t
 *
 * This class is a wrapper for the marker property enum.
 * It is provided for allowing to have an enum with a functions.
 *
 * This class is the main way to interact with the property enum.
 * It relies heavaly on the preprocessor, so you are not expected
 * to understand this.Make sure that all the properties that the
 * marker has, beside the position and the type, are also added
 * to the EGD_MARKER_PROPERTY_LIST macro list.
 *
 * This class also provides the ability to iterate over properties.
 * For that it mimics a bidirectional iterator.
 * The ranges can be accessed by calling certain static functions.
 * These functions returns a pgl_rangeBasedLoopAdaptor_t object.
 * That allows to use the convenient range based loop syntax.
 *
 * The supported ranges are:
 * 	- all:		Which returns all properties.
 * 	- pos:		Which returns all properties that deals
 * 			 with the position of markers.
 * 	- physical: 	Returns a range over all physical parameter,
 * 			 also no position and type.
 */
class egd_propertyIndex_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	//These typedefs are needed to be an iterator.
	using iterator_category 	= std::bidirectional_iterator_tag;	//!< The category of the iterator.
	using value_type		= eMarkerProp;				//!< The value of the iterator dereerencing.
	using difference_type 		= std::ptrdiff_t;			//!< Difference type.
	using pointer 			= value_type*;				//!< Type of a pointer.
	using reference 		= eMarkerProp;				//!< Reference, not a reference.

	/**
	 * \brief	This is a range of properties.
	 * \typedef 	PropRange_t
	 *
	 * This is a range based loopü adaptor that
	 * allows to create ranges, over special subset
	 * of the properties.
	 */
	using PropRange_t 		= ::pgl::pgl_rangeBasedLoppAdaptor_t<egd_propertyIndex_t, true>;	//!< is defined as constant
	using PropList_t 		= ::pgl::pgl_vector_t<egd_propertyIndex_t>;				//!< This is a list of properties.


	/*
	 * ===============
	 * Constructors
	 *
	 * You should use the generating functions.
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This constructor creats an egd_propertyIndex_t object out of an enum.
	 * This function checks if the vlue passed was correct.
	 *
	 * \param  markerProp		The marker property.
	 *
	 * \throw 	If *this is invalid.
	 */
	egd_propertyIndex_t(
		const eMarkerProp& 	markerProp)
	 :
	  egd_propertyIndex_t(markerProp, 1)	//This calls the non checking implementation
	{
		//Perform the checking
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed marker property enum was invalid.");
		};
	}; //End: builing constructor to user


	/**
	 * \brief	Default constructor.
	 *
	 * Is forbidden.
	 */
	constexpr
	egd_propertyIndex_t()
	 noexcept
	 = delete;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	constexpr
	egd_propertyIndex_t(
		const egd_propertyIndex_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_propertyIndex_t&
	operator= (
		const egd_propertyIndex_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	constexpr
	egd_propertyIndex_t(
		egd_propertyIndex_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_propertyIndex_t&
	operator= (
		egd_propertyIndex_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_propertyIndex_t()
	 noexcept
	 = default;


	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor transforms a eMarkerProp
	 * into a egd_propertyIndex_t object.
	 * This constructor is private, as such it can not be used.
	 *
	 * Instead use the static functions for that.
	 *
	 * \param  markerProp		This is the enum that should be converted.
	 * \param  Dummy		This is a dummy int, that is needed for the overloading.
	 *
	 */
private:
	constexpr
	egd_propertyIndex_t(
		const eMarkerProp& 	markerProp,
		int)
	 :
	  m_prop(markerProp)
	{
	}; //End: building constructor



	/*
	 * =====================
	 * Static building functions
	 *
	 * These functions allows to statically
	 * build an egd_propertyIndex_t object.
	 * For that the Preprocessor is used
	 */
public:
	/*
	 * This code will auto generate static functions that have
	 * the same name as the value of the enum.
	 * They will construct a egd_propertyIndex_t object from it.
	 * These is also constexpr.
	 */
#define StaticBuild(r, name, elem) static constexpr egd_propertyIndex_t elem () noexcept { return egd_propertyIndex_t(eMarkerProp :: elem, 1);};
	BOOST_PP_SEQ_FOR_EACH(StaticBuild, IG, BOOST_PP_VARIADIC_TO_SEQ(EGD_MARKER_PROPERTY_LIST_FULL));
#undef StaticBuild


	/**
	 * \brief	This function allows to create an invalid property index.
	 *
	 * This function is the only way to do this. It is highly recomended, that
	 * it is not used, only in cases, where it is really needed.
	 */
	static
	constexpr
	egd_propertyIndex_t
	MAKE_INVALID_INDEX()
	{
		return egd_propertyIndex_t(eMarkerProp::NONE, 1);
	};


	/*
	 * This is for convenience
	 */
	static constexpr egd_propertyIndex_t StrainRateXY() noexcept { return egd_propertyIndex_t(eMarkerProp::StrainRateYX, 1);};
	static constexpr egd_propertyIndex_t StressXY() noexcept { return egd_propertyIndex_t(eMarkerProp::StressYX, 1);};

	/**
	 * \brief	This fucntion will generate a string out of *this.
	 */
	std::string
	print()
	 const
	{
		//This macro is fro writting the code
#define CASE(r, name, elem) case eMarkerProp :: elem : return std::string(BOOST_PP_STRINGIZE(elem)); break;

		//Switching over the staus
		switch(this->m_prop)
		{
			//Apply the case
			BOOST_PP_SEQ_FOR_EACH(CASE, IG, BOOST_PP_VARIADIC_TO_SEQ(EGD_MARKER_PROPERTY_LIST_FULL));

			//Handling the other known cases
			case eMarkerProp::END:  return std::string("END"); break;
			case eMarkerProp::NONE:	return std::string("NONE"); break;

			default:
			throw PGL_EXCEPT_InvArg("Encountered an unknown eMarkerProp value.");
		}; //End switch
#undef CASE
	}; //End: print function.


	/*
	 * =====================
	 * Accessing fucntions
	 */
public:
	/**
	 * \brief	This fucntion returns *this as eMarkerProp.
	 *
	 * This is usefull for writting switch statements.
	 */
	eMarkerProp
	getProp()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return m_prop;
	}; //End: getProp


	/*
	 * ==========================
	 * Status functions
	 */
public:
	/**
	 * \brief	This function returns the reperesentant of *this.
	 *
	 * Since properties have to be unique, the density in the basic nodal point
	 * and the density in the pressure point are different values.
	 * But they are both densities, but only expressed differently.
	 * This function now represents the cannonic properties of *this back.
	 *
	 * Meaning that all densityies will return the density index when called.
	 *
	 * \note	There is no guarantee that it actually works.
	 */
	egd_propertyIndex_t
	getRepresentant()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());

#define IsRepre(r, MasterRep, elem) case eMarkerProp:: elem : return MasterRep; break;
		switch(m_prop)
		{
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::Density, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_DENSITY));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::Viscosity, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_VISCOSITY));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::RhoCp, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_RHOCP));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::Temperature, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_TEMP));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::ThermConduct, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_THERMCOND));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::VelX, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_VELX));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::VelY, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_VELY));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::Stress, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_DEVSTRSS));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::StrainRate, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_DEVSTRAINRATE));
			BOOST_PP_SEQ_FOR_EACH(IsRepre, eMarkerProp::RadioEnergy, BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_RADIOHEAT));

			default:
				//We do not hve specialized for that so we simply exit and handle this at the end
			break;
		}; //End switch
#undef IsRepre

		/*
		 * Everything that is not handled above, will simply map to itself.
		 */
		return *this;
	}; //End: representat


	/**
	 * \brief	This function returns true if *this is a prepresentant.
	 *
	 * In essence this function checks if (*this == this->getRepresentant())
	 * is true. Note that there is no guarantee that this function is faster
	 * than doing it just like that. The function is primarly supported
	 * to write nice expressions.
	 *
	 * It is asserted, if *this is valid. If asserts are disabled *this is never
	 * a representant of itslef.
	 */
	bool
	isRepresentant()
	 const
	 noexcept
	{
		if(this->isValid() == false)
		{
			pgl_assert(false && "Called isRepresentant on an invalid index.");
			return false;	//COnvention
		};

		return ((*this) == this->getRepresentant())
			? true
			: false;
	}; //End: isRepresentaqnt


	/**
	 * \brief	This function returns true if *this is represented by other.
	 *
	 * This function absically checks if the representant of *this and other
	 * is the same. This has the effect that the operation is commutative.
	 * Note if *this or other are invalid this function will always return false.
	 * This case is asserted.
	 *
	 * Note this function is not guaranteed to be faster than
	 * doing it explicitly, but much cleaner.
	 *
	 * \param  other	The other property.
	 */
	bool
	isRepresentedBy(
		const egd_propertyIndex_t& 	other)
	 const
	 noexcept
	{
		if(this->isValid() == false ||
		   other.isValid() == false   )
		{
			pgl_assert(false && "Detected invalid indexes.");
			return false;
		};

		return (this->getRepresentant() == other.getRepresentant())
			? true
			: false;
	}; //End: is represented by


	/**
	 * \brief	This function checks if *this is rettresentated,
	 * 		 by the grid property other.
	 *
	 * This function is the same as the other function of the same
	 * name, but other is a grid property.
	 *
	 * \param  other	A grid property to check.
	 */
	bool
	isRepresentedBy(
		const egd_gridProperty_t& 	other)
	 const
	 noexcept;


	/**
	 * \brief	This fucntion returns ture of *this is valid.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		if((m_prop < eMarkerProp::END) && (eMarkerProp::NONE < m_prop))
		{
			return true;
		};

		return false;	//It is not valid
	}; //End: isValid


	/**
	 * \brief	This function returns true if *this is a position.
	 *
	 * This function explicitly test for positions. However no
	 * representant is computed.
	 */
	bool
	isPosition()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return ((this->m_prop == eMarkerProp::PosX) ||
			(this->m_prop == eMarkerProp::PosY)   )
		        ? true
		        : false;
	}; //End: isPosition


	/**
	 * \brief	This fucntion returns true if *this is the type property.
	 *
	 * This function only checks if *this is valid in debug mode.
	 */
	bool
	isType()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return (this->m_prop == eMarkerProp::Type);
	}; //End: isType


	/**
	 * \brief	This function returns true if *this is a velocity.
	 *
	 * Note this is true if *this is any velocity, with the exception
	 * of the feel velocity which is not a velocity.
	 */
	bool
	isVelocity()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return ((this->getRepresentant() == eMarkerProp::VelX) ||
			(this->getRepresentant() == eMarkerProp::VelY)   );
	}; //End: isVelcoity


	/**
	 * \brief	This function returns true if *this is an x velocity.
	 *
	 * Note that this is not true for the feel velocity.
	 */
	bool
	isVelX()
	 const
	 noexcept
	{
		pgl_assert(this->isValid() );
		return (this->getRepresentant() == eMarkerProp::VelX);
	};//End: isVelX


	/**
	 * \brief	This function returns true if *this is an y velocity.
	 *
	 * Note that this is not true for the feel velocity.
	 */
	bool
	isVelY()
	 const
	 noexcept
	{
		pgl_assert(this->isValid() );
		return (this->getRepresentant() == eMarkerProp::VelY);
	};//End: isVelY


	/**
	 * \brief	Returns true if *this is a physical parameter.
	 *
	 * Only in debug mode a check for validty is performed.
	 */
	bool
	isPhysical()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return ((eMarkerProp::Type < this->m_prop    ) &&
			(this->m_prop      < eMarkerProp::END)   );
	}; //End isPhysical


	/**
	 * \brief	Returns true if *this is a temperature.
	 *
	 * Only in debug mode checks for validity.
	 */
	bool
	isTemperature()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return (this->getRepresentant() == eMarkerProp::Temperature);
	};


	/**
	 * \brief	Returns true if *this is pressure.
	 *
	 * This is done by comparing the represenatnat of *this with pressure.
	 */
	bool
	isPressure()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return (this->getRepresentant() == eMarkerProp::Pressure);
	};


	/**
	 * \brief	This function returns true if *this is the full heating term.
	 *
	 * The full heating term is basically the source term that apears in the
	 * temperature equation.
	 */
	bool
	isFullHeatingTerm()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return (eMarkerProp::FullHeatingTerm == this->m_prop);
	};



	/**
	 * \brief	This function returns true if *this is a plot property.
	 *
	 * A plot property is a special property, that is not needed for the computation,
	 * but for plotting. It is for convenience such that the interpolator is used
	 * to generate it and is already in the data base.
	 *
	 * Note you should not use them directly.
	 */
	bool
	isPlotProperty()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());

#		if defined(EGD_DUMP_PLOT_PROPERTY) && EGD_DUMP_PLOT_PROPERTY == 1
		/*
		 * Only plot properties if they are enabled
		 */
#			define plotProp(r, MasterRep, elem) case eMarkerProp:: elem : return true;
		switch(m_prop)
		{
			BOOST_PP_SEQ_FOR_EACH(plotProp, "", BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_PLOTPROP));

			default:
				//We do not hve specialized for that so we simply exit and handle this at the end
			break;
		}; //End switch
#			undef plotProp
#		endif

		/*
		 * *This is not a plot property
		 */
		return false;
	}; //End: is Plot propery


	/**
	 * \brief	This function returns true if *this is a special properties.
	 *
	 * Special properties are properties that have some artificial touch to them.
	 * They are not directly used for computation, but are nice to have in other
	 * parts. An example are the plot property, or the feel property.
	 */
	bool
	isSpecialProperty()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());

#		define specialProp(r, MasterRep, elem) case eMarkerProp:: elem : return true;
		switch(m_prop)
		{
			BOOST_PP_SEQ_FOR_EACH(specialProp, "", BOOST_PP_VARIADIC_TO_SEQ(EGD_PROPIDX_SPECIALPROP));

			default:
				//We do not hve specialized for that so we simply exit and handle this at the end
			break;
		}; //End switch
#		undef plotProp
		return false;
	};//End: is special


	/**
	 * \brief	This function returns true if *this is a feel velocity.
	 *
	 * A feel velocity is the velocity that is used to move the markers and
	 * not their intrinsic velocity. Note that the feel velocity is a velocity.
	 * It is not considered as a velocity nor a pysical property.
	 */
	bool
	isFeelVel()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());

		return ((this->m_prop == eMarkerProp::FeelVelX) ||
			(this->m_prop == eMarkerProp::FeelVelY)   )
			? true
			: false;
	}; //End: isFeel



	/*
	 * ==============================
	 * Operators
	 *
	 * Some of the operators are used to turn *this
	 * into an operator like object, however they are
	 * depricated and by default deactivated.
	 *
	 * However some comparisson operators are provided.
	 * Note that the operator< is provided to use
	 * the index inside an ordered container.
	 */
public:
	/**
	 * \brief	This function is an alias of getProp().
	 *
	 * It is usefull for iteration. The Type is able to
	 * model a bidirectional iterator.
	 */
	eMarkerProp
	operator* ()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return m_prop;
	}; //End: operator*


#if defined(EGD_PROPIDX_USE_ITERATOR) && (EGD_PROPIDX_USE_ITERATOR != 0)
	/**
	 * \brief	Prefix increment.
	 *
	 * This function increments this and returns a reference to
	 * itself.
	 */
	egd_propertyIndex_t&
	operator++ ()
	 noexcept
	{
		//Make a test if the
		pgl_assert(m_prop <= eMarkerProp::END);

		//incrementing it; casting to conferm with the strict typing
		m_prop = static_cast<eMarkerProp>(static_cast<Int_t>(m_prop) + 1);

		return *this;
	}; //End: prefix increment


	/**
	 * \brief	This is the postfix increment.
	 *
	 * It makes a copy of itself and then increments it.
	 * It will then return the unincremented copy.
	 */
	egd_propertyIndex_t
	operator++ (
		int)
	 noexcept
	{
		egd_propertyIndex_t dolly(*this);	//Make a copy.
		this->operator++();		//increment *this using the prefix
		return dolly;			//Return the unincremented copy
	}; //End postfix increment


	/**
	 * \brief	This is the prefix decrement.
	 *
	 * This functin decrements *this and returns a reference to itself.
	 */
	egd_propertyIndex_t&
	operator-- ()
	 noexcept
	{
		pgl_assert(eMarkerProp::NONE < m_prop);

		//decrementing it; casting to conferm with the strict typing
		m_prop = static_cast<eMarkerProp>(static_cast<Int_t>(m_prop) - 1);

		return *this;
	}; //End: prefix decrement


	/**
	 * \brief	Postfix decrement.
	 *
	 * This function copy *this and then decrements *this.
	 * The copy is returned.
	 */
	egd_propertyIndex_t
	operator-- (
		int)
	 noexcept
	{
		egd_propertyIndex_t dolly(*this);	//make a copy of *this
		this->operator--();		//decrement *this
		return dolly;
	}; //End: postfix decrement
#endif


	/**
	 * \brief	This function tests if *this and rhs are equal.
	 *
	 * \param  rhs	The other egd_propertyIndex_t object.
	 */
	bool
	operator== (
		const egd_propertyIndex_t& rhs)
	 const
	 noexcept
	{
		return this->m_prop == rhs.m_prop;
	}; //End: operator==


	/**
	 * \brief	Checks if *this is the same as rhs, where rhs is the
	 * 		 base enum.
	 */
	bool
	operator== (
		const eMarkerProp 	rhs)
	 const
	 noexcept
	{
		return (m_prop == rhs);
	};


	/**
	 * \brief	This function tests if *this is unequal to rhs.
	 *
	 * \param  rhs	The other egd_propertyIndex_t object.
	 */
	bool
	operator!= (
		const egd_propertyIndex_t& rhs)
	 const
	 noexcept
	{
		return this->m_prop != rhs.m_prop;
	};


	/**
	 * \brief	This function checks if *this is not the same as rhs,
	 * 		 where rhs is the basis enum.
	 */
	bool
	operator!= (
		const eMarkerProp 	rhs)
	 const
	 noexcept
	{
		return (m_prop != rhs);
	};


	/**
	 * \brief	Returns true if *this is less than rhs.
	 *
	 * Note that the order is unspecific, but all elements are
	 * well ordered.
	 *
	 * \param  rhs		The rhs object of the expression *this < rhs.
	 */
	bool
	operator< (
		const egd_propertyIndex_t& rhs)
	 const
	 noexcept
	{
		return (static_cast<Int_t>(this->m_prop) < static_cast<Int_t>(rhs.m_prop));
	};


#if defined(EGD_PROPIDX_USE_ITERATOR) && (EGD_PROPIDX_USE_ITERATOR != 0)
	/*
	 * ==========================
	 * Range generator functions.
	 *
	 * This functions here are used to generate a range
	 * out of *this. These are the ones that are used to
	 * iterate over the ranges.
	 *
	 * calling them on objects is quite unintuitive results.
	 */
public:
	/**
	 * \brief	This function returns an iterator to the first property.
	 */
	[[deprecated]]
	static
	constexpr
	egd_propertyIndex_t
	begin()
	 noexcept
	{
		return egd_propertyIndex_t::PosX();
	};


	/**
	 * \brief	This function returns an iterator to the first property.
	 *
	 * Explicit constant version.
	 */
	[[deprecated]]
	static
	constexpr
	egd_propertyIndex_t
	cbegin()
	 noexcept
	{
		return egd_propertyIndex_t::PosX();
	};


	/**
	 * \brief	This fucntion returns the past the end iterator of all properties.
	 *
	 * The returned iterator shall not be dereferenced.
	 */
	[[deprecated]]
	static
	constexpr
	egd_propertyIndex_t
	end()
	 noexcept
	{
		return egd_propertyIndex_t(eMarkerProp::END, 1);
	};


	/**
	 * \brief	This fucntion returns the past the end iterator of all properties.
	 *
	 * The returned iterator shall not be dereferenced.
	 */
	[[deprecated]]
	static
	constexpr
	egd_propertyIndex_t
	cend()
	 noexcept
	{
		return egd_propertyIndex_t(eMarkerProp::END, 1);
	};


	/*
	 * ====================
	 * Range accesses
	 *
	 * These functions allows to write range based loops
	 * over a certain subset of properties.
	 * These the returned types have a begin and an end function
	 * so it can also be used manually.
	 */
public:
	/**
	 * \brief	Returns a range over all properties.
	 *
	 * This includes possitions, type and physical properties.
	 */
	[[deprecated]]
	static
	PropRange_t
	all()
	 noexcept
	{
		return PropRange_t(begin(), end());
	};


	/**
	 * \brief	Returns a property list with all indexes that are alo in the all range.
	 */
	[[deprecated]]
	static
	PropList_t
	allList();


	/**
	 * \brief	Returns a range over all the positions inside the properties.
	 */
	[[deprecated]]
	static
	PropRange_t
	pos()
	 noexcept
	{
		return PropRange_t(
			egd_propertyIndex_t::PosX(), 	//By default the first pos and property
			egd_propertyIndex_t::Type()	//By default the first after the positions
		);
	}; //End: pos range


	/**
	 * \brief	Returns a property list containg all the indeces that are also contained in the pos range.
	 */
	[[deprecated]]
	static
	PropList_t
	posList();


	/**
	 * \brief	Returns a range over the physical properties.
	 *
	 * This are all properties except the type and positions.
	 */
	[[deprecated]]
	static
	PropRange_t
	physical()
	 noexcept
	{
		return PropRange_t(
			++egd_propertyIndex_t::Type(),	//By default Type is the last non physical property, so incrementing it, will get the first physical property
						// We have to use the pre increment, because we do not want to have Type
			end()			//The last physical one is also the last one globaly
		);
	}; //End physical Range


	/**
	 * \brief	Return a propery list containg all indexis that are alos contained in the physical range.
	 */
	[[deprecated]]
	static
	PropList_t
	physicalList();
#endif


	/*
	 * =========================
	 * List functions
	 */
public:
	/**
	 * \brief	This function checks if the passeld property
	 * 	  	 list contains a certain property or not.
	 *
	 * If the property is found the function has no effect, however it
	 * will iterate through all the properties and check if they are
	 * valid. If the property is not found it will be added to the list.
	 * A true is returned if the list was modified, i.e. the property
	 * was not present in the list.
	 *
	 * \param  pList	The property list to examine.
	 * \param  prop		The property to look for.
	 */
	static
	bool
	ensureProperty(
		PropList_t* const 		pList,
		const egd_propertyIndex_t& 	prop);


	/**
	 * \brief	This function checks if teh proeprty list is valid.
	 *
	 * A valid property list is a list that contains no invalid properties
	 * and each property is present only once.
	 * If the bool flag is set to true, then you instract the function
	 * that you only want to have representantts in it, this is for
	 * example needed wehen you want to test if the list contains valid
	 * marker properties.
	 *
	 * \param  pList	The list to check.
	 * \param  useRep	Expect marker properties in the list.
	 */
	static
	bool
	isValidList(
		const PropList_t&		pList,
		const bool 			useRep = false);


	/**
	 * \brief	This function returns true if pIdx was found in the list.
	 *
	 * This function will iterate through the list and check if the property
	 * pIdx is inside it.
	 *
	 * \param  pList 	The property list to search.
	 * \param  pIdx		The property to search.
	 */
	static
	bool
	isPropertyInList(
		const PropList_t&		pList,
		const egd_propertyIndex_t& 	pIdx);




	/*
	 * ======================
	 * Friends
	 */
private:
	friend
	class egd_propertyIndexAdaptor_t;


	/*
	 * ================
	 * Private Members
	 */
private:
	eMarkerProp 	m_prop;		//!< This is the property type of this
}; //End class(egd_propertyIndex_t)



/**
 * \brief 	This prints the egd_propertyIndex_t to a stream.
 *
 * \param  o	This is the stream
 * \param  p	This is the property
 */
inline
std::ostream&
operator<< (
	std::ostream&			o,
	const ::egd::egd_propertyIndex_t&	p)
{
	o << p.print();

	return o;
}; //End: outstream operator


inline
bool
operator== (
	const eMarkerProp 		lhs,
	const egd_propertyIndex_t&	rhs)
 noexcept
{
	return (rhs == lhs);
};


inline
bool
operator!= (
	const eMarkerProp 		lhs,
	const egd_propertyIndex_t&	rhs)
 noexcept
{
	return (rhs != lhs);
};


inline
std::string
operator+(
	const std::string& 		s,
	const egd_propertyIndex_t&	p)
{
	return s + p.print();
};


inline
std::string
operator+(
	const egd_propertyIndex_t&	p,
	const std::string& 		s)
{
	return p.print() + s;
};


/**
 * \brief	This function transforms pIdx into its representant.
 *
 * Note that this function is basically calling getRepresentatn()
 * but is a bit shorter.
 *
 * \param  pIdx		The property index to transform.
 */
inline
egd_propertyIndex_t
mkRep(
	const egd_propertyIndex_t& 	pIdx)
{
		pgl_assert(pIdx.isValid());
	return pIdx.getRepresentant();
};


/**
 * \brief	This function tests if pIdx is a representant.
 *
 * This function basically calls the isRepresentant() function
 * but is shorter to write.
 *
 * \param  pIdx		The property to check.
 */
inline
bool
isRep(
	const egd_propertyIndex_t& 	pIdx)
{
		pgl_assert(pIdx.isValid());
	return pIdx.isRepresentant();
};



/**
 * \brief	This is an alias of the egd_propertyIndex_t.
 * \typedef 	PropIdx
 *
 * This is an alias that alows to write a bit cleaner code.
 */
using PropIdx = egd_propertyIndex_t;

PGL_NS_END(egd)



PGL_NS_START(std)

/**
 * \brief	This is a specializattion of the hash object for the index type.
 *
 * We do not have to overload pgl_hash_t for the Index.
 * The reason is that all overloads aviable to std::hash are also aviable to pgl_hash_t.
 * Overloading the std version is more gneral.
 */
template<>
struct hash<::egd::egd_propertyIndex_t>
{
	using argument_type 	= ::egd::egd_propertyIndex_t;
	using result_type 	= ::std::size_t;

        result_type
        operator() (
        	argument_type const& idx)
         const
         noexcept
        {
        	return static_cast<typename ::std::underlying_type<::egd::eMarkerProp>::type>(idx.getProp());
	};
}; //End struct(hash)

PGL_NS_END(std)



//Disable the macro with the list
#undef EGD_MARKER_PROPERTY_LIST
#undef EGD_MARKER_PROPERTY_LIST_FULL
#undef EGD_PROPIDX_DENSITY
#undef EGD_PROPIDX_TEMP
#undef EGD_PROPIDX_RHOCP
#undef EGD_PROPIDX_THERMCOND
#undef EGD_PROPIDX_VISCOSITY
#undef EGD_PROPIDX_VELX
#undef EGD_PROPIDX_VELY
#undef EGD_PROPIDX_RADIOHEAT
#undef EGD_PROPIDX_SPECIALPROP

