/**
 * \brief	This function implements the functions that are needed to transform
 * 		 the basic nodal grid into other grids.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_linalg/pgl_EigenCore.hpp>			//Include the main LINALG header
#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <tuple>


PGL_NS_START(egd)

using pgl::isValidFloat;

/*
 * This is an anonymous namespace, that implements some core functionality.
 * If one thinks about the structure of the grid a bit than it is clear,
 * that they are very similar, so they can be reduced to two functions, to handle
 * the staggered grid on the regular size and in an extended form.
 * This is done by simply append a last grid point, but this is done on
 * the caller side.
 */
namespace
{

//This is the grid positions type that is used
using NodePosition_t = egd_gridGeometry_t::NodePosition_t;

/**
 * \brief	This is the node mover function.
 *
 * It interpretes the passed vectors as grid point locations.
 * It will then move them half a grid wide to the back.
 * For the first grid point, it is assumed that a ghost cell is there.
 * These ghost cell has the same width as the first cell.
 *
 * This is a core function.
 *
 * \param  pos_		This is the position
 */
void
egdInternal_moveGridPointsBack(
	NodePosition_t* const 	pos_)
{
	pgl_assert(pos_ != nullptr);
	NodePosition_t& pos = *pos_;	//Create an alias of the argument.

	const auto N 	  = pos.size();	//This is the number of points
		pgl_assert(N > 0);
	Numeric_t lastPos = NAN;	//This is the last psotion this is needed for the correct moving

	//
	//Now calculate the '-1' grid point, by coping the first cell.
	lastPos = pos[0] - (pos[1] - pos[0]);

	/*
	 * Now move all grid points
	 */
	for(Index_t i = 0; i != N; ++i)
	{
		//Get the location, current, of the grid point we want to move
		const Numeric_t gridPointToMove = pos[i];
			pgl_assert(isValidFloat(gridPointToMove)                           ,
				   isValidFloat(lastPos        ), lastPos < gridPointToMove );

		//Position of the gridpiont prior to this one
		const Numeric_t gridPointBefore = lastPos;

		//The new pressure grid point is located between this one and
		//the one from last time. so we have to calculate their distance
		//and then subtract half of it from the current point
		const Numeric_t distBetweenTheTwo = gridPointToMove - gridPointBefore;
			pgl_assert(isValidFloat(distBetweenTheTwo), distBetweenTheTwo > 0.0);

		//Now calculate the new position of this point
		const Numeric_t newPositionOfGridPoint = gridPointToMove - 0.5 * distBetweenTheTwo;

		//Now write the result back
		pos[i] = newPositionOfGridPoint;

		//Also propagate the position of the previous grid point
		lastPos = gridPointToMove;
	}; //End for(i):

	return;
}; //End: moveGridPoints


/**
 * \brief	This function extends the passed grid by
 * 		 appending a new one.
 *
 * This function assumes that the passed vector represents
 * the locations of the grid points. It will then extend
 * it by adding a new cell at the end, by dublicating the
 * last cell.
 *
 * \param  pos		The positions.
 */
NodePosition_t
egdInternal_extendGridAtBack(
	const NodePosition_t& 		pos)
{
	//Get the positions
	const Index_t N = pos.size();
		pgl_assert(N > 0);

	//This is the return value
	NodePosition_t extGrid(N + 1);	//It is larger by one.

	//Copy the original vector into the first part of the
	//return value
	extGrid.head(N) = pos;

	//Now Calculate the length of the last cell
	const Numeric_t lengthOfLastCell = pos[N - 1] - pos[N - 2];
		pgl_assert(isValidFloat(lengthOfLastCell), lengthOfLastCell > 0.0);

	//Write the last position
	extGrid[N] = pos[N - 1] + lengthOfLastCell;

	//Check if all is good
	pgl_assert([](const NodePosition_t& p) -> bool
			{
			  if(isValidFloat(p[0]) == false) return false;
			  const Index_t N = p.size();
			  	pgl_assert(N >= 2);		//this has to hold
			  for(Index_t i = 1; i != N; ++i)	//we have to start at 1, so that the previous exist
			  { if(  isValidFloat(p[i]) == false ) return false;
			    if(!(p[i - 1] < p[i]            )) return false; };
			  return true;}(extGrid));

	return extGrid;
}; //End: extend grid


/**
 * \brief	This function calculates the grid points for the pressure.
 *
 * This function essentially moves the x and y points back half a grid wide.
 *
 * \param  xPoints	The x coordinates of the nodal grid.
 * \param  yPoints	The y coordinates of the nodal grid.
 */
void
egdInternal_makePressureMove(
	NodePosition_t* const 		xPoints,
	NodePosition_t* const 		yPoints)
{
	pgl_assert(xPoints != nullptr,
		   yPoints != nullptr );
	NodePosition_t& xPoints_ = *xPoints;
	NodePosition_t& yPoints_ = *yPoints;

	//Move both grids point coordinates backwards
	egdInternal_moveGridPointsBack(&xPoints_);
	egdInternal_moveGridPointsBack(&yPoints_);

	return;
}; //End: make pressure move


/**
 * \brief	This function creates the Vx points.
 *
 * By observing picture 7.15 (p 96) in the book one sees,
 * that here the x coordinates of the node points are the
 * same as the basic points.
 * But the y coordinates are shifted to the top.
 *
 * So this function does this.
 */
void
egdInternal_makeVxMove(
	NodePosition_t* const 		xPoints,
	NodePosition_t* const 		yPoints)
{
	pgl_assert(xPoints != nullptr,
		   yPoints != nullptr );
	NodePosition_t& xPoints_ = *xPoints;
	NodePosition_t& yPoints_ = *yPoints;

	//Only shift the y coordinate
	(void)xPoints_;
	egdInternal_moveGridPointsBack(&yPoints_);

	return;
}; //End: make VX Points


/**
 * \brief	This function creates the Vx points.
 *
 * As it was for the VX points, one sees that
 * here the y coordinate are the same, but the
 * x coordinates are moved backwards.
 *
 * \param  xPoints	Pointer to the location of the x coordinates.
 * \param  yPoints	Pointer to the location of the y coordinates.
 */
void
egdInternal_makeVyMove(
	NodePosition_t* const 		xPoints,
	NodePosition_t* const 		yPoints)
{
	pgl_assert(xPoints != nullptr,
		   yPoints != nullptr );
	NodePosition_t& xPoints_ = *xPoints;
	NodePosition_t& yPoints_ = *yPoints;

	//Only shift the x Coordinate
	egdInternal_moveGridPointsBack(&xPoints_);
	(void)yPoints_;

	return;
}; //End: make VY Points


/**
 * \brief	This function calculates the grid spacing in the constant case.
 *
 * This function works on all grid, since the spacing is the same on all.
 * This function only handles the constant case. It also handles the case
 * of cell centre points.
 *
 * \param  NBasic	Number of basic nodal points.
 * \param  spacing	The spacing in the dimension.
 * \param  gType 	The grid type.
 */
NodePosition_t
egdInterlal_constantSpacing(
	const Index_t 		nBasic,
	const Numeric_t 	spacing,
	const eGridType 	gType)
{
	pgl_assert(nBasic > 0);
	pgl_assert(isValidFloat(spacing), spacing > 0.0);
	pgl_assert(isValidGridType(gType));

	//Get the paramter that are needed for detrming the number of sacings
	const bool regGrid    = egd::isRegularGrid(gType);

	if(::egd::isExtendedGrid(gType) && egd::isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_InvArg("The basic grid can not be extended.");
	};

	//Determine the number of spacings, so ONE less than we have grid points
	const Index_t N = (regGrid == false 		//Test if we are on the extended grid
			   ? (nBasic) 			// == ((nBasic + 1) - 1)
			   : (nBasic - 1)   );		//regular grid
	return NodePosition_t::Constant(N, spacing);
}; //End: const spacing

}; //End: anonymous namespace






std::pair<egd_gridGeometry_t::NodePosition_t, egd_gridGeometry_t::NodePosition_t>
egd_gridGeometry_t::getGridPoints(
	const eGridType 	gType)
 const
{
	pgl_assert(this->m_isGridSet == true,
		   isValidGridType(gType)    );
	if(this->isGridSet() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The grid is not set, so can not calculate any grid points.");
	};

	if(this->isConstantGrid() == false)	//Friendly reminder
	{
		throw PGL_EXCEPT_RUNTIME("It seams that now non constant grids are used. blease nesure that the routines works correctly.");
	};

	//This are the return values; how they are initialized depends
	//on the grid type.
	NodePosition_t X, Y;

	/*
	 * Fill the X and Y variable wioth sense
	 */
	if(isRegularGrid(gType) == true)
	{
		//In a regular setting we just have to copy
		//the original data
		X = m_xGrid;
		Y = m_yGrid;
	}
	else
	{
		if(isBasicNodePoint(gType) == true)	//Not possible
		{
			throw PGL_EXCEPT_LOGIC("An extended basic nodal grid, does not exist.");
		};

		//In an extended setting, we must extend the grid
		X = egdInternal_extendGridAtBack(m_xGrid);
		Y = egdInternal_extendGridAtBack(m_yGrid);
			pgl_assert(egd::isExtendedGrid(gType ));	//Obscure situation
			pgl_assert(X.size() == (m_xPoints + 1),
				   Y.size() == (m_yPoints + 1) );
	}; //End else:


	//Now we can handle the cases
	switch(mkCase(gType))
	{
	  case eGridType::StPressure:
	  case eGridType::CellCenter:			//By definition the same as pressure
	  	egdInternal_makePressureMove(&X, &Y);	//Differs only in later details.
	  break;

	  case eGridType::StVx:
	  	egdInternal_makeVxMove(&X, &Y);
	  break;

	  case eGridType::StVy:
	  	egdInternal_makeVyMove(&X, &Y);
	  break;

	  case eGridType::BasicNode:
	  	//Nothing to do, since we return X, Y; Test for
	  	//extended grid is done above
	  break;

	  default:
	  	throw PGL_EXCEPT_RUNTIME("Could not identify the grid type \"" + std::to_string(gType) + "\"");
	  break;
	}; //End switch(gType):


	/*
	 * Now we make a pair and return it.
	 * Again we maintain the X, Y order.
	 */
	return std::make_pair(std::move(X), std::move(Y));
}; //End: getGridPoints



std::pair<egd_gridGeometry_t::GridSpacing_t, egd_gridGeometry_t::GridSpacing_t>
egd_gridGeometry_t::getGridSpacings(
	const eGridType 	gType)
 const
{
	if(this->isGridSet() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The grid is not set, so can not calculate any spacing.");
	};
	if(::egd::isExtendedGrid(gType) && ::egd::isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_RUNTIME("The basic grid can not be extended.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed gird type is invalid.");
	};
	if(this->isConstantGrid() == false)	//Friendly reminder
	{
		throw PGL_EXCEPT_RUNTIME("It seams that now non constant grids are used. blease nesure that the routines works correctly.");
	};

	//
	//Handle spacing in the case of a constant grid spacing.
	if(this->isConstantGrid() == true)
	{
		/*
		 * We have a constant grid, so we have the spacing already.
		 * We use the internal function to get them.
		 */
		NodePosition_t dX = egdInterlal_constantSpacing(m_xPoints, m_xSpacing, gType);
		NodePosition_t dY = egdInterlal_constantSpacing(m_yPoints, m_ySpacing, gType);

		return std::make_pair(std::move(dX), std::move(dY));
	}; //End if: constant grid

	//
	//Handle the non constant case

	/*
	 * Note
	 * Spacing is not the same on the grids.
	 * It is the same on all grids, if the grid
	 * is a constant grid. But if this is not the
	 * case, then spacing differs.
	 */
	NodePosition_t X, Y;
	std::tie(X, Y) = this->getGridPoints(gType);

	//Now we can caluclate the distance
	auto D = [](const NodePosition_t& gp)
	  {
		const auto N = gp.size();
		return (gp.head(N - 1) - gp.tail(N - 1));	//You are a fool if you thing this returns a matrix
	  };

	GridSpacing_t dX = D(X);	//Assigning it to a Eigen storage type, will perform the calculations.
	GridSpacing_t dY = D(Y);

	pgl_assert((dX.array() > 0.0).all(),
		   (dY.array() > 0.0).all() );


	return std::make_pair(std::move(dX), std::move(dY));
}; //End: calculate grid spacing


std::pair<Numeric_t, Numeric_t>
egd_gridGeometry_t::getTopNode(
	const eGridType 	gType)
 const
{
	if(this->isGridSet() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The grid is not set, so can not calculate any spacing.");
	};
	if(::egd::isExtendedGrid(gType) && ::egd::isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_RUNTIME("The basic grid can not be extended.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed gird type is invalid.");
	};

	//We precalulate the grid space shifting
	//The ghost cells are copies of the first row/columns
	const Numeric_t gridShiftingX = (m_xGrid[1] - m_xGrid[0]) * 0.5;
	const Numeric_t gridShiftingY = (m_yGrid[1] - m_yGrid[0]) * 0.5;

	//load the basic grid points
	const Numeric_t X0 = m_xGrid[0];
	const Numeric_t Y0 = m_yGrid[0];

	/*
	 * For this function it is irelevant if we are
	 * on the regular grid or the extended grid.
	 */
	switch(mkCase(gType))
	{
	  case eGridType::BasicNode:
		return std::make_pair(X0               , Y0                );
	  break;

	  case eGridType::StVx:
	  	return std::make_pair(X0               , Y0 - gridShiftingY);
	  break;

	  case eGridType::StVy:
	  	return std::make_pair(X0 - gridShiftingX, Y0               );
	  break;

	  case eGridType::StPressure:
	  case eGridType::CellCenter:		//Is the same forom a geometrical pov, as the pressure
		return std::make_pair(X0 - gridShiftingX, Y0 - gridShiftingY);
	  break;

	  default:
	  	throw PGL_EXCEPT_RUNTIME("Could not identify the grid type \"" + std::to_string(gType) + "\"");
	  break;
	}; //End switch(gType):

	pgl_assert(false && "Reached unreacable code.");
	return std::make_pair(NAN, NAN);
}; //End: getTopNode



egd_gridGeometry_t::Index_t
egd_gridGeometry_t::getLastAssoNodeX(
	const xNodeIdx_t 	nBasicX,
	const eGridType 	gType)
{
	if(::egd::isExtendedGrid(gType) && ::egd::isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_RUNTIME("The basic grid can not be extended.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed gird type is invalid.");
	};

	//We are interested on the _index_ of the last node that a marker
	//can be associated to, so '-1' is there to account for that.
	const Index_t N = nBasicX - 1;

	/*
	 * It does not matter if we are on the extended grid or
	 * the regular grid. The reason is that the extended grid
	 * simply adds more nodes, and does not moves nodes.
	 * The nodes that are more, are outside the computational
	 * domain, so no marker can be right to them.
	 * So only the locations of the nodes on the grid is
	 * important.
	 */
	switch(mkCase(gType))
	{
	  case eGridType::StPressure:
	  case eGridType::CellCenter:	//Same as the pressure
		return N;	//Markers could be right from the last marker.
				// Also because the definition of the pressure grid.
	  break;

	  case eGridType::StVx:
		return (N - 1);	//Because the last one is exactly at the border,
				// thus no marker can be right to it.
	  break;

	  case eGridType::StVy:
		return N;	//Because it is in the middle of the cell.
	  break;

	  case eGridType::BasicNode:
	  	return (N - 1);	//Because the last one is right at the bottom right corner.
	  break;

	  default:
	  	throw PGL_EXCEPT_RUNTIME("Could not identify the grid type \"" + std::to_string(gType) + "\"");
	  break;
	}; //End switch(gType):

	pgl_assert(false && "Unreachable code.");
}; //End: get last valid index.


egd_gridGeometry_t::Index_t
egd_gridGeometry_t::getLastAssoNodeY(
	const yNodeIdx_t 	nBasicY,
	const eGridType 	gType)
{
	if(::egd::isExtendedGrid(gType) && ::egd::isBasicNodePoint(gType))
	{
		throw PGL_EXCEPT_RUNTIME("The basic grid can not be extended.");
	};
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed gird type is invalid.");
	};

	//We are interested on the _index_ of the last node that a marker
	//can be associated to, so '-1' is there to account for that.
	const Index_t N = nBasicY - 1;

	/*
	 * See explanation in x direction.
	 * Only some adjustments are nneded here.
	 */
	switch(mkCase(gType))
	{
	  case eGridType::StPressure:
	  case eGridType::CellCenter:	//Alias of pressure point.
		return N;	//Because markers can be right from the last node
				// And because of the definition of pressure grid.
	  break;

	  case eGridType::StVx:
		return N;	//Because is only half way down the last cell.
				// so markers can be below it
	  break;

	  case eGridType::StVy:
		return (N - 1);	//Beacause the last one is right at the boundary.
	  break;

	  case eGridType::BasicNode:
	  	return (N - 1);	//Because the last one is right at the bottom right corner.
	  break;

	  default:
	  	throw PGL_EXCEPT_RUNTIME("Could not identify the grid type \"" + std::to_string(gType) + "\"");
	  break;
	}; //End switch(gType):

	pgl_assert(false && "Unreachable code.");
}; //End: get last valid index.



PGL_NS_END(egd)


