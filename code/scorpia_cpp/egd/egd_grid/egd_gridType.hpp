#pragma once

/**
 * \brief	This file defines an enum that can be used for characterizing a grid.
 *
 */

//Include the confg file
#include <egd_core.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <string>



PGL_NS_START(egd)

//FWD of the property index class
class egd_propertyIndex_t;

/**
 * \brief	This enum names and defines a grid.
 * \enum 	eGridType
 *
 * In EGD there are four types of grids.
 *
 * - The basic nodal grid. In this grid the gridpoints are exactly what the
 *   	naïve approache would tell you, they are the points where two cels are coninside.
 * - The cell centre grid. In this grid the nodes are placed inside the cell, at the center.
 *   	The pressure in the staggered grid is located there too.
 * - The Vx grid. This are the locations where the velocities in x direction are located
 *   	on the staggered grid, not only the x component of the velocities are located there,
 *   	but also the heat flux for example. The name is generic, but not very precise.
 * - The Vy grid. Simillary to the Vx grid, but shifted in the x direction by half a cell.
 *
 * The classical stagered grid, is the same as it is in the original code.
 * The means the are a series of unphysical ghost nodes, on the left and/or
 * top boundary. But all grids were $N_y \times N_x$ point grids.
 *
 * The extended grid staggered grid is a bit different than before.
 * It can be seen in the book on page 96, in figure 7.15.
 * One can seen it as an enlargement of the original staggered grid,
 * by a new row and column at the end of the domain. Then the staggered
 * grid is placed on that grid it was normaly.
 * The old ghost nodes have now a meaning, the pressure points are still
 * ghost nodes. Instead the ghost nodes are now loacated at a different
 * place, see the book.
 *
 * The rest is very simmilar to how it was before, see the book, chaptor
 * seven. However some grids are handled very special and this is described
 * here.
 *
 * - regular cell centre point.
 * This grid was previously handled very special, but this has now changed.
 * Instead of being a seperate grid type, it is basically the same as the
 * regular pressure grid, this also involves tzhe grid points,
 * so the ghost nodes are no included.
 *
 * - extended basic gird.
 * This is the normal basic nodal grid, but in an extended form.
 * It does not exist.
 *
 * - extended cell centre and pressure grid.
 * In the regular incranation they are basically the same, this is also
 * mostly true for the extended case, but not completly. The difference
 * between the two is, that the pressure node only has ghost nodes.
 * But the cell centre grid does not have ghost nodes.
 * The additional nodes, that surround the computational domain,
 * are the boundary nodes. Note that there all boundary types also depends
 * on the inner nodes. So in the extended cell centre grid you can
 * accass all $(N_y + 1) \times (N_x + 1)$ nodes.
 *
 *
 * \note	This enum was previously, basically not used, because it was not yet ready
 * 		 for use. But now it is, so it is now modified.
 */
enum class eGridType : ::pgl::Int32_t
{
	INVALID  = 0,		//!< Markes an invalid gird.

	BasicNode  = 1 <<  0,	//!< This indicates a basic nodal point/grid, meaning the one defined by the grid.
	CellCenter = 1 <<  1,	//!< This is the point in the cell center, it is only usefull with a regular grid.
	StPressure = 1 <<  2,	//!< This indicates a pressure point in the staggered grid (gohst nodes included).
	StVx 	   = 1 <<  3,	//!< This indicates a x velocity node in the staggered grid (ghost nodes included).
	StVy 	   = 1 <<  4, 	//!< This indicates a y Velocity node in the staggered grid (ghost nodes included).

	StGrid     = StPressure | StVx | StVy,	// This is an alias for a fully stagered grid.

	RegGrid    = 1 << 20,	//!< This is a regular grid, where the grid nodes are in the intersections.
				//!<  You can also say that it is the original one. It is not extended.
				//!<  Note that a regular grid can also be staggered.

	Extended   = 1 << 21,	//!< This means that the grid is extended meaning that it is the new one.
				//!<  It is like the oposite of RegGrid.
}; //End: enum(eGridType)



/*
 * ===========================
 * Operators
 */

/**
 * \brief	This function performs the bitwise \e or operation.
 *
 * \param  lhs		The left hand side.
 * \param  rhs		The right hand side.
 */
inline
constexpr
eGridType
operator| (
	const eGridType 		lhs,
	const eGridType 		rhs)
{
	return static_cast<eGridType>(static_cast<Int32_t>(lhs) | static_cast<Int32_t>(rhs));
};


/**
 * \brief	This function performs the bitwise \e and operation.
 *
 * \param  lhs		The left hand side.
 * \param  rhs		The right hand side.
 */
inline
constexpr
eGridType
operator& (
	const eGridType 		lhs,
	const eGridType 		rhs)
{
	return static_cast<eGridType>(static_cast<Int32_t>(lhs) & static_cast<Int32_t>(rhs));
};


/*
 * ==================================
 * Functions
 */


/**
 * \brief	This function returns the default grid type.
 *
 * There is no guarantee what the default grid type is.
 * Currently it is the regular basic nodal grid.
 */
inline
constexpr
eGridType
getDefaultGrid()
 noexcept
{
	return (eGridType::BasicNode | eGridType::RegGrid);
};


/**
 * \brief	This function returns a default grid type for a specific property.
 *
 * This function can returns different grid types depending on the property
 * that is passed to it. If the property has no predefined grid type,
 * the default value, the same function but without any argument is returned.
 *
 * This function is implemented in the associated cpp file.
 *
 * Note that not all properties have a meaningfull implementation.
 * Before you hack them in somwhere else in the code, modify it here.
 *
 * You can also request an that the grid is extended.
 *
 * \param  Prop		The property that is juged.
 * \param  isExtended	The grid should be extended.
 */
extern
eGridType
getDefaultGrid(
	const egd_propertyIndex_t& 	propIdx,
	const bool 			isExtended);


/**
 * \brief	This function removes everything beside the location.
 *
 * This means this function, as indicates allows to switch over a grid type enum
 * that is set with te grid extensions
 *
 * \param  gType
 */
inline
constexpr
eGridType
mkCase(
	const eGridType 	gType)
{
	return (gType & static_cast<eGridType>( ~(static_cast<Int32_t>(eGridType::Extended | eGridType::RegGrid)) ));
};


/**
 * \brief	This function allows to make a regular grid from a given node point.
 *
 * This function computes the bitwise or of its argument point and the eGridType::RegGrid.
 *
 * \param  point	The point we want.
 */
inline
constexpr
eGridType
mkReg(
	const eGridType 	point)
 noexcept
{
	return (eGridType::RegGrid | mkCase(point));
};


/**
 * \brief	This function allows for the easy construction of an extended grid.
 *
 * This function computes the bitwise or with its argument point and
 * eGridType::ExtGrid.
 *
 * \param  point	The point we want.
 */
inline
constexpr
eGridType
mkExt(
	const eGridType 	point)
 noexcept
{
	return (eGridType::Extended | mkCase(point));
};



/**
 * \brief	This function returns the extension size of teh grid.
 *
 * With size of grid, we mean if it is defined on the regular grid or
 * on the extended grid.
 *
 * \param  gType	The grid type.
 */
inline
constexpr
eGridType
getSizeBit(
	const eGridType 	gType)
{
	return (gType & (eGridType::Extended | eGridType::RegGrid));
};


/**
 * \brief	This function returns true if gType has a grid size bit.
 *
 * \param  gType	The grid to examine.
 */
inline
constexpr
bool
hasSizeBit(
	const eGridType 	gType)
{
	return (static_cast<Int32_t>(gType & (eGridType::Extended | eGridType::RegGrid)) != 0);
};


/**
 * \brief	This function copyis the size, of the passed src and
 * 		 ors it with the passed grid points.
 *
 * \param  sizeSrc	What size we should use.
 * \param  nType	Which node type we sould use.
 */
inline
constexpr
eGridType
copySize(
	const eGridType 	sizeSrc,
	const eGridType 	nType)
{
	return (getSizeBit(sizeSrc) | mkCase(nType));
};


/**
 * \brief	Return true if grid is a pressure point grid.
 *
 * \param  grid		The grid type we want to check.
 */
inline
constexpr
bool
isPressurePoint(
	const eGridType 		grid)
{
	return (((grid & eGridType::StPressure) == eGridType::StPressure) ? true : false);
};


/**
 * \brief	Return true if grid is a Vx point.
 *
 * \param  grid		The grid type we want to check.
 */
inline
constexpr
bool
isVxPoint(
	const eGridType 		grid)
{
	return (((grid & eGridType::StVx) == eGridType::StVx) ? true : false);
};


/**
 * \brief	Return true if grid is a Vy point.
 *
 * \param  grid		The grid type we want to check.
 */
inline
constexpr
bool
isVyPoint(
	const eGridType 		grid)
{
	return (((grid & eGridType::StVy) == eGridType::StVy) ? true : false);
};



/**
 * \brief	This function returns true if type is an extended grid.
 *
 * \param  type		The grid to examine.
 */
inline
constexpr
bool
isExtendedGrid(
	const eGridType 		type)
{
	return (((type & eGridType::Extended) == eGridType::Extended) ? true : false);
};


/**
 * \brief	This function returns true if grid is a regular grid.
 *
 * \param  grid 	The grid we want to check
 */
inline
constexpr
bool
isRegularGrid(
	const eGridType 		grid)
{
	return (((grid & eGridType::RegGrid) == eGridType::RegGrid) ? true : false);
};


/**
 * \brief	This function returns true if grid is a fully staggered grid.
 *
 * A fully staggered grid is a grid that is composed out of the Vx, Vy and pressure nodes.
 *
 * \param  grid		This is teh grid we want to check.
 */
inline
constexpr
bool
isFullyStaggeredGrid(
	const eGridType 		grid)
{
	return (mkCase(grid) == (eGridType::StVx | eGridType::StVy | eGridType::StPressure));
};


/**
 * \brief	Returns true if grid is a basic nodal point.
 *
 * \param  grid		The grid we want to check.
 */
inline
constexpr
bool
isBasicNodePoint(
	const eGridType 		grid)
{
	return (((grid & eGridType::BasicNode) == eGridType::BasicNode) ? true : false);
};


/**
 * \brief	This function returns true if grid is a cell center grid.
 *
 * Note since thsi is only usefull with a regular grid, this condition is
 * also enforced.
 *
 * \param  grid		This is the grid, that should be checked.
 */
inline
constexpr
bool
isCellCentrePoint(
	const eGridType 		grid)
{
	return (((grid & eGridType::CellCenter) == eGridType::CellCenter) ? true : false);
};


/**
 * \brief	This function returns true if type is a valid grid type.
 *
 * In essence this function checks if type is not equal eGridType::INVALID,
 * and if the conditions are usefull.
 */
inline
constexpr
bool
isValidGridType(
	const eGridType 		type)
{
	return (
		(type != eGridType::INVALID) 		         		&&
		(isExtendedGrid(type) != isRegularGrid(type))    		&&
		(pgl_implies(isBasicNodePoint(type), isRegularGrid(type))) 	&&  //The extended basic grid does not exist.
		//Test if only one grid position bit is set, or if type is a fully staggered grid.
		(
		 (1 == __builtin_popcount(static_cast<Int32_t>(type & (eGridType::BasicNode | eGridType::CellCenter | eGridType::StPressure | eGridType::StVx | eGridType::StVy)))) ||
		 ((isFullyStaggeredGrid(type) == true) && (isCellCentrePoint(type) == false) && (isBasicNodePoint(type) == false))
		)//End: bit checking
	       );
}; //End: is valid grid


/**
 * \brief	This function returns a human redable string of the grid type.
 *
 * This fucntion is provoided for outputting the type in a nice manor.
 * There is aslo an overload of the to_string function.
 * Note if the grid type is invalid no error is raised.
 *
 * \param  gType	The grid type.
 */
extern
std::string
printGridType(
	const eGridType 	gType);


inline
std::string
operator+ (
	const std::string& 	s,
	const eGridType 	gType)
{
	return s + printGridType(gType);
};


inline
std::string
operator+ (
	const eGridType 	gType,
	const std::string& 	s)
{
	return printGridType(gType) + s;
};


inline
std::ostream&
operator<< (
	std::ostream& 		o,
	const eGridType 	gType)
{
	o << printGridType(gType);

	return o;
};




PGL_NS_END(egd)



PGL_NS_START(std)

/**
 * \brief	This function is provided for uniformal printing.
 *
 * This function redirects to the printGridType() function.
 *
 * \param gType		The grid type.
 */
inline
std::string
to_string(
	const egd::eGridType 	gType)
{
	return printGridType(gType);
};

PGL_NS_END(std)



