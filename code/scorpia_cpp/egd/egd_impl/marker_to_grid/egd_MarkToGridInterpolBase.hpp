#pragma once
/**
 * \brief	This file contains the standard implementtation of the marker to grid interpolator.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex_adaptor.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_util_interpolState.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_map.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_set.hpp>
#include <pgl_list.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_markersToGridBase_t
 * \brief	This class is a intended to be used as a base of all
 * 		 marker to grid interpolators.
 *
 * It is completly custumaze by uing hooks. This class implements
 * sensable default implementation of the majority of the hooks.
 * However it does not clreate a plan. This must be done by an
 * interpolation.
 *
 * Special handling is done for all the temperature (RhoCP)
 * and velocity (density).
 *
 * Postprocessing involves the mapping of all quantities that where not
 * yet interpolated my naive interpolation, for that the basic nodal
 * points are used.
 *
 * Boundary conditions are applied to stored conditions, that are read
 * from the configuration object.
 * This must meybe be extended in the future.
 */
class egd_markersToGridBase_t : public egd_MarkersToGrid_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using PropIdx_t 		= GridContainer_t::PropIdx_t;	//!< This is the property index type.
	using GridGeometry_t 		= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;	//!< This is the grid property.
	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;	//!< This is the marker property.
	using InterpolState_t		= egd_interpolState_t;		//!< This is the state of an interpolation.

	using GridWeight_t 		= egd_gridWeight_t;		//!< This is the type for storing the weights of grid points.
	using MarkerWeight_t 		= egd_markerWeight_t;		//!< This is the type for storing the weights of markers.


	using PropList_t 		= ::pgl::pgl_list_t<PropIdx_t>;	//!< List inside a plan.

	/**
	 * \brief	This is an interpolation plan.
	 * \typedef	InterpolPlan_t
	 *
	 * An interpolation plan encodes which properties are interpolated
	 * to which grid. However the storage is inverse, it maps grid types
	 * to properties, this is done, because it is better to process it
	 * that way.
	 *
	 * The plan will be iterate over and the interpolation happens.
	 * The target property is optained by passing the stored
	 * property to the grid container. The source property is
	 * optained by passing the represant of the property
	 * to the marker list.
	 */
	using InterpolPlan_t 		= ::pgl::pgl_map_t<eGridType, PropList_t>;	//!< This type stores all grids that are present.

	/**
	 * \brief	This is the map that encodes properties that needs
	 * 		 special handling.
	 * \typedef	SpecialProp_t
	 *
	 * This is used to encode properties that needs special handling.
	 * It maps a grid property that needs special handling to a
	 * marker property that is used to modify the weights.
	 *
	 * To determine if a grid property needs special interpolation,
	 * it is checked if the representant of its property is inside
	 * that list and if so the modification is obtained, by reqesting
	 * the mapped type from the marker collection, here no representation
	 * is applied.
	 *
	 * Note that the mapped type is not a property index, but its adaptor.
	 * This is because it needs to be default constructible.
	 */
	using SpecialProp_t 		= ::pgl::pgl_map_t<PropIdx_t, egd_propertyIndexAdaptor_t>;


	/**
	 * \brief	This type is used to store many boundary conditions.
	 * \typefdef 	BCMap_t
	 */
	using BCMap_t 			= ::egd::egd_BCMap_t;


	/**
	 * \brief	This type is used to store a boundary condition for a single
	 * 		 boundary condition.
	 * \typedef	BoundaryCondition_t
	 */
	using BoundaryCondition_t 	= BCMap_t::BoundaryCondition_t;
	using SideCondition_t		= BCMap_t::SideCondition_t;


	/**
	 * \brief	This is a plan for the boundary.
	 * \typedef	BoundaryPlan_t
	 *
	 * This is a list of all grid property that needs
	 * boundary conditions applied to them.
	 */
	using BoundaryPlan_t 		= ::pgl::pgl_vector_t<PropIdx_t>;




	/*
	 * ======================
	 * Public Constructors
	 */
public:
	/**
	 * \brief	Construct an interpolator with disabled boundary conditions.
	 *
	 * Thsi constructor builds a marker to grid interpolator, with no knwon
	 * boundary conditions.
	 *
	 * \note	The usage of this function is discuraged and only recommended for
	 * 		 testing purpose.
	 */
	egd_markersToGridBase_t();


	/**
	 * \brief	Construct a interpolator with enabled boundary conditions.
	 *
	 * The boundary conditions are loaded from the condition map.
	 * See also the constructors of the BC map.
	 *
	 * \param  bcMap	The condition map that we use.
	 *
	 */
	egd_markersToGridBase_t(
		const BCMap_t& 		bcMap);

	/**
	 * \brief	Construct an interpolator from an initalizer list.
	 *
	 * This allows to construct a map from a list.
	 *
	 * \param  iList	The list we want to use for construction of the map.
	 */
	egd_markersToGridBase_t(
		const std::initializer_list<BoundaryCondition_t>& 	iList);


	/**
	 * \brief	This function creates an interpolation object from
	 * 		 teh configuration object.
	 *
	 * \param  confObj	The configuiration object.
	 */
	egd_markersToGridBase_t(
		const egd_confObj_t& 	confObj);



	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markersToGridBase_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridBase_t(
		const egd_markersToGridBase_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridBase_t&
	operator= (
		const egd_markersToGridBase_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markersToGridBase_t(
		egd_markersToGridBase_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridBase_t&
	operator= (
		egd_markersToGridBase_t&&);


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function maps the markers to the grid.
	 *
	 * Bilinear interpolation is used for interpolating.
	 * The size of the grids is taken from the grid container.
	 * The grid size is addpated if needed.
	 *
	 * It will only process the properties that are located in
	 * the grid. Currently only constant grids are supported.
	 *
	 * \param  mColl	The marker collection that should be used.
	 * \param  gridCont	Pointer to the grid container.
	 *
	 * \throw 	If an error is detected.
	 */
	virtual
	void
	mapToGrid(
		const MarkerCollection_t& 	mColl,
		GridContainer_t* const 		gridCont)
	 final;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	virtual
	std::string
	print()
	 const
	 = 0;


	/*
	 * =========================
	 * Status function
	 *
	 * These functions are specific to the concrete function.
	 */
public:
	/**
	 * \brief	This function returns true if the temperature boundary is enabled.
	 *
	 * This function returns true if both temperatures are NAN.
	 */
	bool
	isTempBoundaryEnabled()
	 const;





	/*
	 * ========================
	 * Hooks
	 *
	 * These functions are the hooks, that can be overriden
	 * by deriving classes.
	 */
protected:
	/**
	 * \brief	This function is used to create the plan.
	 *
	 * This function is a hook function that determines
	 * whioch property is implemented to which grid and
	 * so on.
	 * See also the descrioption of the returned type.
	 *
	 * \param  gridContainer	The grid container.
	 */
	virtual
	InterpolPlan_t
	hook_mkInterpolPlan(
		const GridContainer_t& 		gridContainer)
	 const
	 = 0;


	/**
	 * \brief	This function generate the list of properties
	 * 		 that needs special handling.
	 *
	 * This is a map that maps properties, from the grid collection
	 * to other poroperties, that is used to manipulate the marker
	 * weights.
	 * The grid type and the prop list is returned.
	 * This function is called for each grid seperatly.
	 *
	 * The default implementation returns a list with:
	 * 	>  Temperature => Thermal conductivity
	 * 	>  Velocity    => Density
	 * It ignores its arguments.
	 *
	 * \param  gType	The grid type that is currently processed.
	 * \param  pList 	The property list that is currently processed.
	 */
	virtual
	SpecialProp_t
	hook_mkSpecialPropList(
		const eGridType 	gType,
		const PropList_t& 	pList)
	 const;


	/**
	 * \brief	This function generate a list of all properties that
	 * 		 needs boundary conditioon applied to them.
	 *
	 *
	 * After the interpolation is done, meaning mormal interpolation and
	 * posistprocessing is done, boundary conditions are applied to the
	 * grid property.
	 *
	 * This function is used by the driver to determine which properties
	 * are needed to be applied.
	 *
	 * The default implementation will add every property that a condition
	 * has to the list (or other, if we can apply something to it, then we
	 * do it).
	 *
	 * \param  interPlan	The interpolation plan that was used.
	 * \param  gridCont	The grid container that is used.
	 */
	virtual
	BoundaryPlan_t
	hook_mkBoundaryPlan(
		const InterpolPlan_t& 		interPlan,
		const GridContainer_t& 		gridCont)
	 const;


	/**
	 * \brief	This function returns true if the init function of the
	 * 		 interpolation set the disable flag to true.
	 *
	 * The default implementation of this function returns true. It is
	 * recomended that this function is not changed.
	 * It ignores its arguments.
	 *
	 * \param  gType	The grid type that is currently processed.
	 * \param  pList 	The property list that is currently processed.
	 */
	virtual
	bool
	hook_disableGhostNodes(
		const eGridType 	gType,
		const PropList_t& 	pList)
	 const;


	/**
	 * \brief	This function performs a second stage of interpolation.
	 *
	 * It is used to get properterties at locations where they were not
	 * interpolated to. For example the density, that was only interpolated
	 * to the basic nodal point could now be mapped to the CC points by
	 * averaging them.
	 *
	 * The default implementation, will do just that. It will map all points
	 * that are not interpolated, meaning have not the basic nodal grid as
	 * the target grid, to the nodes.
	 * For that averaging is used, but If the target is a viscosity harmonic
	 * averaging will be used.
	 * Note that if a property is found inside the plan, nothing is done.
	 * Also Temperature and velocities will cause a problem.
	 *
	 * \param  gridCont 		The grid property.
	 * \param  markerColl 		The marker collection.
	 * \param  interpolPlan		The interpolation plan.
	 *
	 * \note 	Previously the function that is now known as apply boundary
	 * 		 condition was named post processing.
	 */
	virtual
	void
	hook_postProcessing(
		GridContainer_t* const 		gridCont,
		const MarkerCollection_t& 	markerColl,
		const InterpolPlan_t& 		interpolPlan);


	/**
	 * \brief	This function is used as a preprocessing stage.
	 *
	 * This function is called before the interpolation plan is computed.
	 * This function is provided such that the container could be analized
	 * or so.
	 *
	 * The default implementation of this function sets all properties
	 * in the container to zero.
	 *
	 * \param  gridCont 		The grid property.
	 * \param  markerColl 		The marker collection.
	 *
	 * \note 	Is implemented in the base post processing file.
	 */
	virtual
	void
	hook_preProcessing(
		GridContainer_t* const 		gridCont,
		const MarkerCollection_t& 	markerColl);


	/**
	 * \brief	This function is responsible for applying
	 * 		 the boundary conditions.
	 *
	 * This function is called after the interpolation plan was
	 * handled and the post processing was done.
	 *
	 * The default implementation of this function will generate
	 * a boundary plan and execute it. For applying the boundaries
	 * the noundary object inside the grid container is used.
	 * The boundary condition is taken the member of this.
	 *
	 * \param  gridCont 		The grid property.
	 * \param  markerColl 		The marker collection.
	 * \param  interpolPlan		The interpolation plan.
	 */
	virtual
	void
	hook_applyBoundaryCondition(
		GridContainer_t* const 		gridCont,
		const MarkerCollection_t& 	markerColl,
		const InterpolPlan_t& 		interpolPlan)
	 const;


	/**
	 * \brief	This hook is called before a normal interpolation
	 * 		 is performed.
	 *
	 * The source (index and markers) as well as the target property
	 * is passed to the function.
	 * This function should allow inspecting the behaviour.
	 *
	 * The default of this function does nothing.
	 *
	 * \param  srcMIdx	Property of the markers.
	 * \param  srcMProp	Markers that are used for interpolation.
	 * \param  gProp	The grid property which is the target.
	 */
	virtual
	void
	hook_inspectNormalInterpol(
		const PropIdx_t& 		srcMIdx,
		const MarkerProperty_t& 	srcMProp,
		const GridProperty_t& 		gProp)
	 const;


	/**
	 * \brief	This hook is called before a weighted interpolation
	 * 		 is performed.
	 *
	 * The source (index and markers) as well as the target property
	 * is passed to the function.
	 * This function should allow inspecting the behaviour.
	 *
	 * The default of this function does nothing.
	 *
	 * \param  srcMIdx	Property of the markers.
	 * \param  srcMProp	Markers that are used for interpolation.
	 * \param  weightIdx	Property index of the marklers that ar used for weights.
	 * \param  weightMProp	The markers that are used for weights.
	 * \param  gProp	The grid property which is the target.
	 */
	virtual
	void
	hook_inspectWeightedInterpol(
		const PropIdx_t& 		srcMIdx,
		const MarkerProperty_t& 	srcMProp,
		const PropIdx_t& 		weightMIdx,
		const MarkerProperty_t& 	weightMProp,
		const GridProperty_t& 		gProp)
	 const;


	/*
	 * =========================
	 * Helper functions
	 *
	 * These functions are helper functions, they could be
	 * private, but we will make them protected. Also some
	 * of them are virtual, thus allowing a modification.
	 * They are not directly hooks.
	 */
protected:
	/**
	 * \brief	Checks if the passed plan is valid or not.
	 *
	 * If the plan is valid true is returned, false otherwise.
	 * The default implementation checks if no properties
	 * appiers twice.
	 *
	 * \param  interpolPlan		The computed interpolation plan.
	 * \param  gridCont 		The grid container.
	 * \param  markerCont		The marker container.
	 */
	virtual
	bool
	isValidPlan(
		const InterpolPlan_t& 		interpolPlan,
		const GridContainer_t& 		gridCont,
		const MarkerCollection_t& 	markerCont)
	 const;


	/**
	 * \brief	This function returns true, if the interpolation
	 * 		 plan does not contain the property prop.
	 *
	 * \param  interpolPlan		The interpolation plan.
	 * \param  prop			The property to check for.
	 */
	static
	bool
	isPropInsidePlan(
		const InterpolPlan_t& 	interpolPlan,
		const PropIdx_t 	prop);


	/**
	 * \brief	This function scanns the plan for a special
	 *		 a property that is defined on a grid.
	 *
	 * The intent of this function is to search for an already
	 * interpolated property that can be used for other kinds
	 * of operation, such as naive interpolation.
	 *
	 * If the grid type is valid the associated list will be
	 * scanned for a property that has the same representant
	 * as the passed property \e prop. Property is applied on
	 * both. If a suitable property is not found, the function
	 * will scann the full plan.
	 *
	 * If however the corresponding grid does not contain the
	 * requested property on the grid, this function will scann
	 * all grid types, in that case it is unspecific which one
	 * is returned, if the property appears multiple times.
	 * The same is true if the passed grid type is eGridType::INVALID.
	 *
	 * Note that the same effect can be acieved by calling this
	 * function with an invalid grid type.
	 *
	 * The function will return a pair, first will contain the grid
	 * on which the property was found and second will contain the
	 * property.
	 *
	 * \throw	If the property was not found.
	 *
	 * \param  interpolPlan		The interpolation plan.
	 * \param  prop 		The property we search for.
	 * \param  gType		The grid type we prefere.
	 */
	static
	std::pair<eGridType, PropIdx_t>
	lookForPropertyOn(
		const InterpolPlan_t& 		interpolPlan,
		const PropIdx_t& 		prop,
		const eGridType 		gType);


	/**
	 * \brief	This function tests if the special plan is valid.
	 *
	 * Thsi function examines the map and returns its result.
	 *
	 * \param  spProp	The list to be chaked.
	 */
	static
	bool
	isValidSpecialPropList(
		const SpecialProp_t& 		spList);


	/**
	 * \brief	This function returns a constant reference to the
	 * 		 stored bounday condition map.
	 *
	 * This function allows deriving class to access the property.
	 * Since its value is constant, we have the absolute controll.
	 */
	const BCMap_t&
	getBCMap()
	 const
	 noexcept;

	/*
	 * ===========================
	 * Private members
	 */
private:
	BCMap_t 		m_bcMap;		//!< This is a container for the grid collection.
}; //End class(egd_markersToGridBase_t)

PGL_NS_END(egd)


