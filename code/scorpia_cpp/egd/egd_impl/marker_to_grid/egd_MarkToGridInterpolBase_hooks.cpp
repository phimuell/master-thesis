/**
 * \briefe	This file contains the implementaqtion of the default hooks.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)



egd_markersToGridBase_t::SpecialProp_t
egd_markersToGridBase_t::hook_mkSpecialPropList(
	const eGridType 	gType,
	const PropList_t& 	pList)
 const
{
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};
	if(pList.empty())
	{
		throw PGL_EXCEPT_InvArg("Passed an empty property list.");
	};

	//This is is the special property index
	SpecialProp_t specialList;

	specialList[PropIdx_t::Temperature()] = PropIdx_t::RhoCp().getRepresentant();
	specialList[PropIdx_t::VelX()       ] = PropIdx_t::Density().getRepresentant();
	specialList[PropIdx_t::VelY()       ] = PropIdx_t::Density().getRepresentant();

	return specialList;
	(void)gType;
	(void)pList;
}; //End: get Special list


bool
egd_markersToGridBase_t::hook_disableGhostNodes(
	const eGridType 	gType,
	const PropList_t& 	pList)
 const
{
	if(isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};
	if(pList.empty())
	{
		throw PGL_EXCEPT_InvArg("Passed an empty property list.");
	};

	return true;
	(void)gType;
	(void)pList;
}; //End: diabable ghost nodes


void
egd_markersToGridBase_t::hook_inspectNormalInterpol(
	const PropIdx_t& 		srcMIdx,
	const MarkerProperty_t& 	srcMProp,
	const GridProperty_t& 		gProp)
 const
{
	pgl_assert(srcMIdx.isValid());
	pgl_assert(srcMProp.size() > 0);
	pgl_assert(gProp.hasValidGridType(), gProp.getPropIdx().getRepresentant() == srcMIdx);

	return;
	(void)srcMIdx;
	(void)srcMProp;
	(void)gProp;
}; //ENd: normal interpol


void
egd_markersToGridBase_t::hook_inspectWeightedInterpol(
	const PropIdx_t& 		srcMIdx,
	const MarkerProperty_t& 	srcMProp,
	const PropIdx_t& 		weightMIdx,
	const MarkerProperty_t& 	weightMProp,
	const GridProperty_t& 		gProp)
 const
{
	pgl_assert(srcMIdx.isValid(), weightMIdx.isValid());
	pgl_assert(srcMProp.size() == weightMProp.size(), srcMProp.size() > 0);
	pgl_assert(gProp.hasValidGridType(), gProp.getPropIdx().getRepresentant() == srcMIdx);

	return;
	(void)srcMIdx;
	(void)srcMProp;
	(void)weightMIdx;
	(void)weightMProp;
	(void)gProp;
}; //ENd: weighted interpol


PGL_NS_END(egd)


