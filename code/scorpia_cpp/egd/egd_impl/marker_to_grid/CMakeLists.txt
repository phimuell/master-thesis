# Add the concrerte implementation of the marker to grid interpolator
# to the EGD library.

target_sources(
	egd
  PRIVATE
  	# Base Impl
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpolBase_constructors.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpolBase.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpolBase_interpolation.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpolBase_priv.cpp"

	# Hooks of base
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpolBase_hooks_applyBC.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpolBase_hooks.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpolBase_hooks_postProcess.cpp"

	# Concrete Implementations
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpol_standard.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpol_full.cpp"
  PUBLIC
  	# Base implementation
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpolBase.hpp"

	# Concrete Implementations
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpol_standard.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpol_full.hpp"

	# The test implementation is not added to EGD
	#"${CMAKE_CURRENT_LIST_DIR}/egd_MarkToGridInterpol_TEST.hpp"
)


