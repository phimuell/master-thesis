/**
 * \brief	This function contains the functions for the full interpolation plan routine.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpol_full.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>

PGL_NS_START(egd)


egd_markersToGridFull_t::InterpolPlan_t
egd_markersToGridFull_t::hook_mkInterpolPlan(
	const GridContainer_t& 		gridContainer)
 const
{
	if(gridContainer.nProperties() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The grid container did not contain any properties.");
	};

	// This is the interpolation plan
	InterpolPlan_t interpolPlan;

	/*
	 * Go through the list of stored properties and add them.
	 */
	for(const auto& gPropIT : gridContainer)
	{
		const PropIdx_t  	pIdx 	= gPropIT.first;		//unpack
		const GridProperty_t& 	gProp 	= gPropIT.second;
		const eGridType 	gType 	= gProp.getType();

		//Now we add it to the list
		interpolPlan[gType].push_back(pIdx);
	}; //End for(gPropIT):

	if(interpolPlan.empty() == true)
	{
		throw PGL_EXCEPT_RUNTIME("Did not found any grid.");
	};

	return interpolPlan;
}; //End: interpol plan


std::string
egd_markersToGridFull_t::print()
 const
{
	//This is the begining
	std::string desc = "Full Marker to grid implementation \n";
		desc += "With boundaries:";

	const BCMap_t& bcMap = this->getBCMap();

	if(bcMap.noConditions() == true)	//If we do not have any conditions
	{
		desc += "\tNO CONDITIONS";

		return desc;
	}; //ENd if: no conditions

	/*
	 * Goiing through the list and add the different boudnaries
	 */
	Index_t i = 1;
	for(const auto& it : bcMap)
	{
		desc += "\n\t" + std::to_string(i) + it.print();
		i += 1;
	}; //End for(it):

	return desc;
}; //End printing.


void
egd_markersToGridFull_t::hook_postProcessing(
	GridContainer_t* const 		gridCont_,
	const MarkerCollection_t& 	markerColl,
	const InterpolPlan_t& 		interpolPlan)
{
		pgl_assert(gridCont_ != nullptr);
	return;
	(void)gridCont_;
	(void)markerColl;
	(void)interpolPlan;
};//ENd: post processing


/*
 * =============================
 * Constructors
 */
egd_markersToGridFull_t::egd_markersToGridFull_t(
	const BCMap_t& 		bcMap)
 :
  Base_t(bcMap)
{};


egd_markersToGridFull_t::egd_markersToGridFull_t(
	const std::initializer_list<BoundaryCondition_t>& 	iList)
 :
  Base_t(iList)
{};


egd_markersToGridFull_t::egd_markersToGridFull_t(
	const egd_confObj_t& 	confObj)
 :
  Base_t(confObj)
{}; //End: constructor with config object.


/*
 * =================================
 */
egd_markersToGridFull_t::egd_markersToGridFull_t()
 = default;


egd_markersToGridFull_t::~egd_markersToGridFull_t()
 = default;


egd_markersToGridFull_t::egd_markersToGridFull_t(
	const egd_markersToGridFull_t&)
 = default;


egd_markersToGridFull_t&
egd_markersToGridFull_t::operator= (
	const egd_markersToGridFull_t&)
 = default;


egd_markersToGridFull_t::egd_markersToGridFull_t(
	egd_markersToGridFull_t&&)
 noexcept
 = default;


egd_markersToGridFull_t&
egd_markersToGridFull_t::operator= (
	egd_markersToGridFull_t&&)
 = default;

PGL_NS_END(egd)

