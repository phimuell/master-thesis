/**
 * \brief	This file contains the standard implementtation of the marker to grid interpolator.
 *
 * This file contains the left over code.
 * Meaning the ocde that does not hanbdle interpolation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)



bool
egd_markersToGridBase_t::isTempBoundaryEnabled()
 const
{
	/*
	 * We simply check if a condition for temperature was given
	 * And if so if it is valid.
	 */
	if(m_bcMap.hasConditionFor(PropIdx::Temperature()) == false)	//Not even one
	{
		return false;
	};

	if(m_bcMap.getConditionFor(PropIdx::Temperature()).isValid() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The temperature condition is invbalid.");
	};

	return true;
}; //End: is enabled


const egd_markersToGridBase_t::BCMap_t&
egd_markersToGridBase_t::getBCMap()
 const
 noexcept
{
	return m_bcMap;
}; //End: get map




PGL_NS_END(egd)


