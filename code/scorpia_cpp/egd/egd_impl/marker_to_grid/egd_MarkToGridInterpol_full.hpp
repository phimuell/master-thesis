#pragma once
/**
 * \brief	This is the standard mapping procedure.
 *
 * This class is teh full version.
 * It will make the full plan interpolate every property found
 * in the grid container.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>

#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_markersToGridFull_t
 * \brief	This is the full interpolation plan.
 *
 * Every property will be interpolated cleanly to the grid that
 * it is defined on, so now naive interpolation, will be done.
 */
class egd_markersToGridFull_t : public egd_markersToGridBase_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t 			= egd_markersToGridBase_t;		//!< This is the base implementation.
	using PropList_t 		= Base_t::PropList_t;			//!< This is the property list.
	using InterpolPlan_t 		= Base_t::InterpolPlan_t;		//!< This is the object used for storing the plan.

	using BCMap_t 			= Base_t::BCMap_t;			//!< Type for stioring boundary conditions, many of them.
	using BoundaryCondition_t 	= Base_t::BoundaryCondition_t;
	using SideCondition_t		= Base_t::SideCondition_t;


	/*
	 * ======================
	 * Public Constructors
	 *
	 * Note that *this does not have a state.
	 * All constructor are forvarded to the base.
	 */
public:
	/**
	 * \brief	Construct an interpolator with disabled boundary conditions.
	 *
	 * Thsi constructor builds a marker to grid interpolator, with no knwon
	 * boundary conditions.
	 *
	 * \note	The usage of this function is discuraged and only recommended for
	 * 		 testing purpose.
	 */
	egd_markersToGridFull_t();


	/**
	 * \brief	Construct a interpolator with enabled boundary conditions.
	 *
	 * The boundary conditions are loaded from the condition map.
	 * See also the constructors of the BC map.
	 *
	 * \param  bcMap	The condition map that we use.
	 *
	 */
	egd_markersToGridFull_t(
		const BCMap_t& 		bcMap);

	/**
	 * \brief	Construct an interpolator from an initalizer list.
	 *
	 * This allows to construct a map from a list.
	 *
	 * \param  iList	The list we want to use for construction of the map.
	 */
	egd_markersToGridFull_t(
		const std::initializer_list<BoundaryCondition_t>& 	iList);


	/**
	 * \brief	This function creates an interpolation object from
	 * 		 teh configuration object.
	 *
	 * \param  confObj	The configuiration object.
	 */
	egd_markersToGridFull_t(
		const egd_confObj_t& 	confObj);



	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markersToGridFull_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridFull_t(
		const egd_markersToGridFull_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridFull_t&
	operator= (
		const egd_markersToGridFull_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markersToGridFull_t(
		egd_markersToGridFull_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridFull_t&
	operator= (
		egd_markersToGridFull_t&&);


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/*
	 * ========================
	 * Hooks
	 *
	 * These functions are the hooks, that can be overriden
	 * by deriving classes.
	 */
protected:
	/**
	 * \brief	This function is used to create the plan.
	 *
	 * This function will add every property found on the grid
	 * to the interpolation plan. Thus no property will be
	 * handled by the postprocessing routine.
	 *
	 * \param  gridContainer	The grid container.
	 */
	virtual
	InterpolPlan_t
	hook_mkInterpolPlan(
		const GridContainer_t& 		gridContainer)
	 const
	 override;


	/**
	 * \brief	This function is called upon postprocessing.
	 *
	 * Since everything is handled by the interpolation, there
	 * is noting to do for this routine. So this function will
	 * simply return.
	 *
	 * \param  gridCont 		The grid property.
	 * \param  markerColl 		The marker collection.
	 * \param  interpolPlan		The interpolation plan.
	 */
	virtual
	void
	hook_postProcessing(
		GridContainer_t* const 		gridCont_,
		const MarkerCollection_t& 	markerColl,
		const InterpolPlan_t& 		interpolPlan)
	 override;

	/*
	 * ===========================
	 * Private members
	 *
	 * No members
	 */
private:
}; //End class(egd_markersToGridFull_t)

PGL_NS_END(egd)


