/**
 * \brief	This file contains the standard implementtation of the marker to grid interpolator.
 *
 * This function contains the code that does the interpolation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)



void
egd_markersToGridBase_t::mapToGrid(
	const MarkerCollection_t& 	mColl,
	GridContainer_t* const 		gridCont_)
{
	if(gridCont_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The passed grid container is null.");
	};
	GridContainer_t& gridCont = *gridCont_;		//Aliasing

	const GridGeometry_t& gridGeo = gridCont.getGeometry();


	if(mColl.nMarkers() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The collection does not have any markers.");
	};
	if(gridGeo.xNPoints() < 2 || gridGeo.yNPoints() < 2)
	{
		throw PGL_EXCEPT_InvArg("The grid does not seam to be allocated.");
	};
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid is not set up.");
	};
	if(gridCont.nProperties() <= 0)
	{
		throw PGL_EXCEPT_InvArg("The grid does not have any properties.");
	};

	// ===== END: 	Checks

	//
	//Run the preprocessing stage
	this->hook_preProcessing(&gridCont, mColl);

	//
	//Compute an interpolation plan
	const InterpolPlan_t interpolPlan = this->hook_mkInterpolPlan(gridCont);

	// Check the interpolation plan
	if(this->isValidPlan(interpolPlan, gridCont, mColl) == false)
	{
		throw PGL_EXCEPT_RUNTIME("Computed an invalid interpolation plan.");
	}; //End if: is valid plan

	//
	//This is the interpolation state
	InterpolState_t interpolState;

	/*
	 * Now we handle the plan
	 */
	for(const auto& planIT : interpolPlan)
	{
		const eGridType   gType = planIT.first;	//Unpack the plan
		const PropList_t& pList = planIT.second;
			pgl_assert(pList.size() > 0);

		const bool disableGhostNodes = this->hook_disableGhostNodes(gType, pList);

		//Create the interpolation state
		const bool noEmptyCell = egd_initInterpolation(
				mColl, gridCont.getGeometry(), gType,
				&interpolState,
				true, 	//processing
				disableGhostNodes);
			pgl_assert(egd_isValidWeight(interpolState));

		if(noEmptyCell == false)
		{
			throw PGL_EXCEPT_RUNTIME("Detected an empty cell.");
		};

		//
		//Now we generate the special list
		const SpecialProp_t specialList = this->hook_mkSpecialPropList(gType, pList);

		if(isValidSpecialPropList(specialList) == false)
		{
			throw PGL_EXCEPT_RUNTIME("Generated an invalid special case list.");
		};

		/*
		 * Now we perform the iteration of the grid properties
		 * of the current list
		 */
		for(const PropIdx_t& pIdx : pList)
		{
			//
			//Load everything
			const PropIdx_t destIdx = pIdx;				//This is the target index
			const PropIdx_t srcIdx  = pIdx.getRepresentant();	//This is the source and also
										// for the special handling
			GridProperty_t&          gProp = gridCont.getProperty(destIdx);
			const MarkerProperty_t&  mProp = mColl.cgetMarkerProperty(srcIdx);

			/*
			 * Test if special handling is needed.
			 */
			if(specialList.count(srcIdx) == 0)
			{
				//Inspect
				this->hook_inspectNormalInterpol(srcIdx, mProp, gProp);

				//No special handling is needed
				egd_mapToGrid(interpolState,
					mProp,		//src
					&gProp);	//dest
			//End if: no special handling
			}
			else
			{
				//Load the property that should be used to modify the marker
				const PropIdx_t modIdx = specialList.at(srcIdx).get();

				//Load the marker properties as weights
				const MarkerProperty_t& modWeight = mColl.getMarkerProperty(modIdx);

				//Inspect
				this->hook_inspectWeightedInterpol(
						srcIdx, mProp,		//What we interpolate
						modIdx, modWeight,	//How we extend the weights
						gProp);			//Where we write to

				// Do special interpolation.
				egd_mapToGrid(interpolState,
					mProp,			//src
					&gProp,			//dest
					&modWeight, 		//Weights
					disableGhostNodes);	// In this case the grid weights are recomputed.
			}; //End else: special handling

			//Test if it is good
			pgl_assert(gProp.isFinite());
		}; //End for(pIdx): iterating through the list of current properties
	}; //End for(planIT): going through the plane

	/*
	 * Perform a post processing
	 */
	this->hook_postProcessing(&gridCont, mColl, interpolPlan);


	/*
	 * Now we have done everything, so we now apply the boundary
	 */
	this->hook_applyBoundaryCondition(&gridCont, mColl, interpolPlan);

	return;
}; //End: controll code for interpolation





PGL_NS_END(egd)











