/**
 * \biref	This function contains the code that deals with teh post and pre processing.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

void
egd_markersToGridBase_t::hook_postProcessing(
	GridContainer_t* const 		gridCont_,
	const MarkerCollection_t& 	markerColl,
	const InterpolPlan_t& 		interpolPlan)
{
	if(gridCont_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The passed grid container is null.");
	};
	GridContainer_t& gridCont = *gridCont_;		//Aliasing

	const GridGeometry_t& gridGeo = gridCont.getGeometry();

	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The standard implementation of the post processing only works on constant grids.");
	};

	/*
	 * We will now interpolate through all the grid properties.
	 * If we found one, that is not defined at the basic nodal grid,
	 * we will do some kind of naïve interpolation to get the correct value.
	 */
	for(auto& gPropIT : gridCont)
	{
		GridProperty_t& gProp     = gPropIT.second;	//Load some parts
		const eGridType gType = gProp.getType();

		//Test if the grid is not a basic nodal grid
		if(isBasicNodePoint(gType) == true)
		{
			//it is a basic nodal grid, so we can skip it
			continue;
		}; //End if: is basic nodal grid

		//Test if the property is inside the plan
		if(isPropInsidePlan(interpolPlan, gProp.getPropIdx()) == true)
		{
			//The property was allready handled, so we ignore it
			continue;
		}; //End if: was already handled.

		//load the destination property we want to fill
		//that grid property.
		const PropIdx destPropIdx = gProp.getPropIdx();	//This is the destination index

		//get the representant of the property
		const PropIdx destRepIdx  = destPropIdx.getRepresentant();

		//Check if the property is special
		if(destRepIdx.isTemperature() ||
		   destRepIdx.isVelocity()      )
		{
			throw PGL_EXCEPT_RUNTIME("There is a problem with the property. A special case should be implemented.");
		}; //End if: is problem


		/*
		 * Now we will look for a property that we cwan use for interpolation.
		 */
		const auto srcProp = lookForPropertyOn(
				interpolPlan,
				destPropIdx, 		//We want to interpolate to that thing.
				mkReg(eGridType::BasicNode)	//We prefere that grid
				);
			pgl_assert(srcProp.second.isValid(),
				   egd::isValidGridType(srcProp.first));
		const PropIdx_t srcPropIdx = srcProp.second;

		/*
		 * Load the grid property that we use for interpolation.
		 */
		const GridProperty_t& srcGridProp = gridCont.cgetProperty(srcPropIdx);

		/*
		 * Now we do the naïve interpolation
		 */
		egd_performNaiveTransform(
				&gProp, srcGridProp,	//SRC and dest
				gridGeo, 		//Use the grid geometry
			(	srcPropIdx.getRepresentant() == PropIdx::Viscosity()) ? true : false);	//Viscosity harmonic interpol
			pgl_assert(gProp.isFinite());
	}; //End for(gPropIT): going through the properties

	/*
	 * Now we return
	 */
	return;
	(void)markerColl;
}; //End: postprocessing


void
egd_markersToGridBase_t::hook_preProcessing(
	GridContainer_t* const 		gridCont_,
	const MarkerCollection_t& 	markerColl)
{
	if(gridCont_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The passed grid container is null.");
	};
	GridContainer_t& gridCont = *gridCont_;		//Aliasing


	/*
	 * Go through the grid container and set to zero
	 * Also test if we can interpolate it
	 */
	for(auto& it : gridCont)
	{
		const PropIdx_t pIdx    = it.first;	pgl_assert(pIdx.isValid() ); //load the grid property
		const eGridType gType   = it.second.getGridType();
		const PropIdx_t pIdxRep = pIdx.getRepresentant();

		//check if the grid property is also on the marker
		if(markerColl.hasProperty(pIdxRep) == false)
		{
			throw PGL_EXCEPT_RUNTIME("Detected the grid property " + pIdx
					+ ", on the gird " + gType + ", with representant " + pIdxRep
					+ ". But no compatible property found on marker.");
		};

		//set everything to zero
		it.second.setConstantAll(0.0);
	}; //End for(it):

	return;
}; //End: preprocessing


PGL_NS_END(egd)


