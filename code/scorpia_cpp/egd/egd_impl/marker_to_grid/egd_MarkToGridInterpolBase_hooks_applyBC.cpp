/**
 * \brief	This file contains all hooks that are related to the implementation of the boundary conditions.
 *
 * This includes the default hook interpolation to generate the default boudnary list and
 * the function that executes the plan, this is in this designe the hook that applyies them and no dedicated function.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_markersToGridBase_t::BoundaryPlan_t
egd_markersToGridBase_t::hook_mkBoundaryPlan(
	const InterpolPlan_t& 		interPlan,
	const GridContainer_t& 		gridCont)
 const
{
	BoundaryPlan_t bcPlan;	//This is the boundary plan

	const auto& bcMap = this->getBCMap();

	/*
	 * We will simply iterate through the grid
	 * propertyies and check if we have a condition
	 * for it.
	 */
	for(const auto& it : gridCont)
	{
		//Test if we have a condition
		if(bcMap.hasConditionFor(it.first) == true)
		{
			//We have a condition so we add it
			bcPlan.push_back(it.first);
		};//End if: Condition known
	}; //End for(it):

	return bcPlan;
	(void)interPlan;
}; //End: markers to grid


void
egd_markersToGridBase_t::hook_applyBoundaryCondition(
	GridContainer_t* const 		gridCont_,
	const MarkerCollection_t& 	markerColl,
	const InterpolPlan_t& 		interpolPlan)
 const
{
	pgl_assert(gridCont_ != nullptr);		//Alias it
	GridContainer_t& gridCont = *gridCont_;

	//load the geometry
	const GridGeometry_t& gridGeo = gridCont.getGeometry();

	//Generate the boundary plan
	const BoundaryPlan_t bPlan = this->hook_mkBoundaryPlan(interpolPlan, gridCont);
		pgl_assert(bPlan.empty() == false,
			   ::std::all_of(bPlan.begin(), bPlan.end(),
			   	   [](const PropIdx_t p) -> bool { return p.isValid(); }) );

	//Load the boundary object
	const GridContainer_t::ApplyBC_t& applyBC = gridCont.getApplyBC();

	/*
	 * Execute the plan by iteratuing over all properties
	 * that are inside the plan.
	 */
	for(const PropIdx_t& pIdx : bPlan)
	{
		pgl_assert(pIdx.isValid(), gridCont.hasProperty(pIdx));	//Test

		GridProperty_t& 	    gProp = gridCont.getProperty(pIdx);		//Load the grid property
		const BoundaryCondition_t&  BC    = m_bcMap.getConditionFor(gProp);	//Load the condition object

		//Apply the condition
		applyBC.apply(BC, gridGeo, &gProp);

		pgl_assert(gProp.isFinite());
	}; //End for(pIdx):

	return;
	(void)markerColl;
}; //End: apply boundary conditions


PGL_NS_END(egd)


