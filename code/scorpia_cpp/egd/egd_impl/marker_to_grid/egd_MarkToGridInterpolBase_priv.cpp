/**
 * \briefe	This file contains the implementation of the privbate helper functions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

bool
egd_markersToGridBase_t::isValidPlan(
	const InterpolPlan_t& 		interpolPlan,
	const GridContainer_t& 		gridCont,
	const MarkerCollection_t& 	markerCont)
 const
{
	if(interpolPlan.empty() == true)
	{
		pgl_assert(false && "The interpolation plan is empty.");
	};

	//This set is used to store all seen properties.
	::pgl::pgl_set_t<PropIdx_t> seenProps;

	/*
	 * Going through all of the properties
	 */
	for(const auto& it : interpolPlan)
	{
		const eGridType   gType = it.first;	//Load the grid.
		const PropList_t& pList = it.second;	//Get the list of properties on that grid.

		if(isValidGridType(gType) == false)
		{
			pgl_assert(false && "The grid type is invalid.");
			return false;
		};

		if(pList.empty() == true)
		{
			pgl_assert(false && "No properties on the grid.");
			return false;
		};


		/*
		 * Going through all the properties on the list
		 */
		for(const auto& prop : pList)
		{
			if(prop.isValid() == false)
			{
				pgl_assert(false && "Invalid property detected.");
				return false;
			};

			if(gridCont.cgetProperty(prop).getType() != gType)
			{
				pgl_assert(false && "Property is defined on a different grid.");
				return false;
			};

			if(markerCont.hasProperty(prop.getRepresentant()) == false)
			{
				pgl_assert(false && "The marker collection does not have the source property.");
				return false;
			};

			if(seenProps.count(prop) != 0)
			{
				pgl_assert(false && "Property is known.");
				return false;
			};

			//Add the property to the seen ones
			seenProps.emplace(prop);

		}; //End for(prop): going through the properties
	}; //End for(it): iterating through the plan


	//
	//We did not found an error, so it is probably okay
	return true;
}; //End: check plan


bool
egd_markersToGridBase_t::isPropInsidePlan(
	const InterpolPlan_t& 	interpolPlan,
	const PropIdx_t 	prop)
{
	if(prop.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The property is invalid.");
	};
	pgl_assert(interpolPlan.size() > 0);

	for(const auto& it : interpolPlan)
	{
		const PropList_t& pList = it.second;
			pgl_assert(pList.empty() == false);

		if(pgl::contains(pList.cbegin(), pList.cend(), prop) == true)
		{
			return true;
		}; //End if: the property is inside
	}; //End for(it): going through all grids

	//
	//We did not found the property, so we return false
	return false;
}; //End: is inside plan



std::pair<eGridType, egd_markersToGridBase_t::PropIdx_t>
egd_markersToGridBase_t::lookForPropertyOn(
	const InterpolPlan_t& 		interpolPlan,
	const PropIdx_t& 		prop_,
	const eGridType 		gType)
{
	if(interpolPlan.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("The interpolation plan is empty.");
	};
	if(prop_.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The property that we search for is not found.");
	};


	//We apply the get representant function to the passed property.
	const PropIdx_t prop = prop_.getRepresentant();

	/*
	 * Handle the specific case
	 */
	if(gType != eGridType::INVALID    ||	//We must pass a non invalid grid type
	   interpolPlan.count(gType) != 0   )	//We must have the grid type
	{
		//In some situation this error can happen
		if(::egd::isValidGridType(gType) == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid grid type that is not INVALID.");
		};

		//Get the list of all properties we have interpolqated
		const auto& pList = interpolPlan.at(gType);
			pgl_assert(pList.empty() != true);

		/*
		 * Go through the list of the property to
		 * search for one that is good.
		 */
		for(const PropIdx_t& p : pList)
		{
			if(p.isValid() == false)
			{
				throw PGL_EXCEPT_InvArg("The interpolation plan contained an invalid property.");
			};

			const PropIdx_t otherRep = p.getRepresentant();	//Get the representant

			//Test if we have found the property
			if(otherRep == prop)
			{
				return std::make_pair(gType, p);
			}; //End if: we found
		}; //End for(p):

		/*
		 * If we are here, then we have not found it.
		 * so we will now scann the full list.
		 */
	}; //End if: looked for a specifig grid


	/*
	 * Scann the full plan.
	 */
	for(const auto& it : interpolPlan)
	{
		eGridType it_gt   = it.first;	//Load the grid type
		const auto& pList = it.second; 	//load the assosicated list
			pgl_assert(pList.empty() != true);

		//In some situation this error can happen
		if(::egd::isValidGridType(it_gt) == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid grid type that is not INVALID.");
		};

		/*
		 * Go through the list of the property to
		 * search for one that is good.
		 */
		for(const PropIdx_t& p : pList)
		{
			if(p.isValid() == false)
			{
				throw PGL_EXCEPT_InvArg("The interpolation plan contained an invalid property.");
			};

			const PropIdx_t otherRep = p.getRepresentant();	//Get the representant

			//Test if we have found the property
			if(otherRep == prop)
			{
				return std::make_pair(gType, p);
			}; //End if: we found
		}; //End for(p):
	}; //End for(it): full plan


	/*
	 * If we are here, then nothing was found
	 */
	throw PGL_EXCEPT_RUNTIME("The list that was passed did not contain a suitable property.");
}; //End: look for plan


bool
egd_markersToGridBase_t::isValidSpecialPropList(
	const SpecialProp_t& 		spList)
{
	if(spList.empty() == true)
	{
		return true;	//EMpty lists are always valid
	};

	for(const auto& it : spList)
	{
		//ONly valid index are allowd
		if(it.first.isValid()  == false ||
		   it.second.isValid() == false   )
		{
			return false;
		};

		//ONly represnetants are allowed
		if(it.first.isRepresentant()  == false ||
		   it.second.isRepresentant() == false   )
		{
			return false;
		};
	}; //End for(it): inspecting the lists

	//Did not find an error
	return true;
}; //End: is valid special plan


PGL_NS_END(egd)



