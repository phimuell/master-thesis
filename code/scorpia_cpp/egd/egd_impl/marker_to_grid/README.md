# Info
This folder contains implementation for the marker to grid interpolation process.
There are two versions.

## STD
In these properties are interpolated to the basic nodal grid. Properties on different
grids are determine by averaging processes.

## FULL
All properties on _any_ grids are directly interpolated.



