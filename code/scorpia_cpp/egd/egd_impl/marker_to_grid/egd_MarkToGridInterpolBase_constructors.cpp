/**
 * \brief	This file contains the constructors of the marker to grid interpolator.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_markersToGridBase_t::egd_markersToGridBase_t(
	const BCMap_t& 		bcMap)
 :
  m_bcMap(bcMap)
{};


egd_markersToGridBase_t::egd_markersToGridBase_t(
	const std::initializer_list<BoundaryCondition_t>& 	iList)
 :
  m_bcMap(iList)
{
	pgl_assert(m_bcMap.isValid());
};


egd_markersToGridBase_t::egd_markersToGridBase_t(
	const egd_confObj_t& 	confObj)
 :
  egd_markersToGridBase_t()		//Use the default constructor
{
	/*
	 * Now we will extract all properties that the
	 * configuration knwos about. and addd the
	 * respective condition to the map.
	 */

	//load the list of all properties
	const egd_confObj_t::ePropList_t confPropBC = confObj.getAllProps();

	//Now we iterate over them
	for(const auto& eProp : confPropBC)
	{
		//Load the condition
		BoundaryCondition_t BC = confObj.getBC(eProp);
			pgl_assert(BC.isValid());

		//Add it to the map
		m_bcMap.addCond(BC);
	}; //End for(eProp):

	/*
	 * We are done
	 */
}; //End: constructor with config object.


egd_markersToGridBase_t::egd_markersToGridBase_t()
 = default;


/*
 * =================================
 */

egd_markersToGridBase_t::~egd_markersToGridBase_t()
 = default;


egd_markersToGridBase_t::egd_markersToGridBase_t(
	const egd_markersToGridBase_t&)
 = default;


egd_markersToGridBase_t&
egd_markersToGridBase_t::operator= (
	const egd_markersToGridBase_t&)
 = default;


egd_markersToGridBase_t::egd_markersToGridBase_t(
	egd_markersToGridBase_t&&)
 noexcept
 = default;


egd_markersToGridBase_t&
egd_markersToGridBase_t::operator= (
	egd_markersToGridBase_t&&)
 = default;


PGL_NS_END(egd)











