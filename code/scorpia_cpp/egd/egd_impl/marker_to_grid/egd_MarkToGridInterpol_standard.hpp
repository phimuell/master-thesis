#pragma once
/**
 * \brief	This is the standard mapping procedure.
 *
 * It was designed for woring with the regular grid.
 * It basically only cares about the basic grid properties.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>

#include "./egd_MarkToGridInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_markersToGridStandard_t
 * \brief	This is the standard interpolation plan.
 *
 * This class overrides the function that creates an iterpolation plan.
 * It will interpolate only the properties that are defined on the basic
 * nodal grid.
 * All other properties are handled by the base implementation, which applies
 * naive interpolation.
 */
class egd_markersToGridStandard_t : public egd_markersToGridBase_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t 			= egd_markersToGridBase_t;		//!< This is the base implementation.
	using PropList_t 		= Base_t::PropList_t;			//!< This is the property list.
	using InterpolPlan_t 		= Base_t::InterpolPlan_t;		//!< This is the object used for storing the plan.

	using BCMap_t 			= Base_t::BCMap_t;			//!< Type for stioring boundary conditions, many of them.
	using BoundaryCondition_t 	= Base_t::BoundaryCondition_t;
	using SideCondition_t		= Base_t::SideCondition_t;


	/*
	 * ======================
	 * Public Constructors
	 *
	 * Note that *this does not have a state.
	 * All constructor are forvarded to the base.
	 */
public:
	/**
	 * \brief	Construct an interpolator with disabled boundary conditions.
	 *
	 * Thsi constructor builds a marker to grid interpolator, with no knwon
	 * boundary conditions.
	 *
	 * \note	The usage of this function is discuraged and only recommended for
	 * 		 testing purpose.
	 */
	egd_markersToGridStandard_t();


	/**
	 * \brief	Construct a interpolator with enabled boundary conditions.
	 *
	 * The boundary conditions are loaded from the condition map.
	 * See also the constructors of the BC map.
	 *
	 * \param  bcMap	The condition map that we use.
	 *
	 */
	egd_markersToGridStandard_t(
		const BCMap_t& 		bcMap);

	/**
	 * \brief	Construct an interpolator from an initalizer list.
	 *
	 * This allows to construct a map from a list.
	 *
	 * \param  iList	The list we want to use for construction of the map.
	 */
	egd_markersToGridStandard_t(
		const std::initializer_list<BoundaryCondition_t>& 	iList);


	/**
	 * \brief	This function creates an interpolation object from
	 * 		 teh configuration object.
	 *
	 * \param  confObj	The configuiration object.
	 */
	egd_markersToGridStandard_t(
		const egd_confObj_t& 	confObj);



	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markersToGridStandard_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridStandard_t(
		const egd_markersToGridStandard_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridStandard_t&
	operator= (
		const egd_markersToGridStandard_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markersToGridStandard_t(
		egd_markersToGridStandard_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridStandard_t&
	operator= (
		egd_markersToGridStandard_t&&);


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/*
	 * ========================
	 * Hooks
	 *
	 * These functions are the hooks, that can be overriden
	 * by deriving classes.
	 */
protected:
	/**
	 * \brief	This function is used to create the plan.
	 *
	 * This function adds every property that is defined on
	 * the basic nodal grid to the plan.
	 * All other properties will be ignored.
	 * They will be handled by the postprocessing routine
	 * of base.
	 *
	 * \param  gridContainer	The grid container.
	 */
	virtual
	InterpolPlan_t
	hook_mkInterpolPlan(
		const GridContainer_t& 		gridContainer)
	 const
	 override;


	/*
	 * ===========================
	 * Private members
	 *
	 * No members
	 */
private:
}; //End class(egd_markersToGridStandard_t)

PGL_NS_END(egd)


