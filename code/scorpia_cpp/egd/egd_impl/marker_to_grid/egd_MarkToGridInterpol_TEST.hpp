#pragma once
/**
 * \brief	This function is a special case of the standard interpolation routine.
 *
 * It should be used only in testing cases.
 * What it does is doing nonesense.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex_adaptor.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_util_interpolState.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_MarkToGridInterpol_standard.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_set.hpp>
#include <pgl_list.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_markersToGridTestImpl_t
 * \brief	This class is a very simple implementation that performs a test.
 *
 * It should not be used for other things.
 */
class egd_markersToGridTestImpl_t final : public egd_markersToGridBase_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using PropIdx_t 		= GridContainer_t::PropIdx_t;	//!< This is the property index type.
	using GridGeometry_t 		= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;	//!< This is the grid property.
	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;	//!< This is the marker property.
	using InterpolState_t		= egd_interpolState_t;		//!< This is the state of an interpolation.

	using GridWeight_t 		= egd_gridWeight_t;		//!< This is the type for storing the weights of grid points.
	using MarkerWeight_t 		= egd_markerWeight_t;		//!< This is the type for storing the weights of markers.


	using PropList_t 		= ::pgl::pgl_list_t<PropIdx_t>;	//!< List inside a plan.

	using InterpolPlan_t 		= ::pgl::pgl_map_t<eGridType, PropList_t>;	//!< This type stores all grids that are present.

	using SpecialProp_t 		= ::pgl::pgl_map_t<PropIdx_t, egd_propertyIndexAdaptor_t>;



	/*
	 * ======================
	 * Public Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * The constructor will call the default constructor of the underlying
	 * implementation, which should disable anything, but the most important
	 * functions are overwritten anyway.
	 */
	egd_markersToGridTestImpl_t()
	 :
	  egd_markersToGridBase_t()
	{};


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	~egd_markersToGridTestImpl_t()
	 = default;



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridTestImpl_t(
		const egd_markersToGridTestImpl_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridTestImpl_t&
	operator= (
		const egd_markersToGridTestImpl_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markersToGridTestImpl_t(
		egd_markersToGridTestImpl_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted.
	 */
	egd_markersToGridTestImpl_t&
	operator= (
		egd_markersToGridTestImpl_t&&)
	 = default;


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	std::string
	print()
	 const
	 override
	{
		return std::string("Test interpolator.");
	}; //End: print


	/*
	 * =========================
	 * Status function
	 *
	 * These functions are specific to the concrete function.
	 */
public:
	/*
	 * ========================
	 * Hooks
	 *
	 * These functions are the hooks, that can be overriden
	 * by deriving classes.
	 */
protected:
	/**
	 * \brief	This function is used to create the plan.
	 *
	 * This function will add all properties that are inside
	 * the grid container and defiend on the basic nodal grid
	 * to the interpoation plan.
	 *
	 * This is in essence the same as the default, but will
	 * remain that way eve if the default is changed.
	 *
	 * \param  gridContainer	The grid container.
	 */
	virtual
	InterpolPlan_t
	hook_mkInterpolPlan(
		const GridContainer_t& 		gridContainer)
	 const
	 override
	{
		if(gridContainer.nProperties() <= 0)
		{
			throw PGL_EXCEPT_InvArg("The grid container did not contain any properties.");
		};

		// This is the interpolation plan
		InterpolPlan_t interpolPlan;


		/*
		 * We gho througt the grid properties and only
		 * consider the properties that are defined on the
		 * basic nodal grid. This ensures compatibility with
		 * the original matlab and C++ implementation.
		 */
		for(const auto& gPropIT : gridContainer)
		{
			const PropIdx_t  	pIdx 	= gPropIT.first;		//unpack
			const GridProperty_t& 	gProp 	= gPropIT.second;
			const eGridType 	gType 	= gProp.getType();

			//We only consider the quantities that are defined
			//in the basic nodal points
			if(isBasicNodePoint(gType) == false)
			{
				continue;
			}; //end if: consider only basic grids


			//Now we add it to the list
			interpolPlan[gType].push_back(pIdx);
		}; //End for(gPropIT):



		if(interpolPlan.empty() == true)
		{
			throw PGL_EXCEPT_RUNTIME("Did not found any grid.");
		};

		return interpolPlan;
	};//End: make interpolation plan


	/**
	 * \brief	This function generate the list of properties
	 * 		 that needs special handling.
	 *
	 * This function will instruct that density is interpolated
	 * using the viscosity. This is completly stupid but is only
	 * a test.
	 */
	virtual
	SpecialProp_t
	hook_mkSpecialPropList(
		const eGridType 	gType,
		const PropList_t& 	pList)
	 const
	 override
	{
		if(isValidGridType(gType) == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
		};
		if(pList.empty())
		{
			throw PGL_EXCEPT_InvArg("Passed an empty property list.");
		};

		//This is is the special property index
		SpecialProp_t specialList;

		specialList[PropIdx_t::Density()] = PropIdx_t::Viscosity().getRepresentant();

		return specialList;
	}; //End: handle special case


	/**
	 * \brief	This function performs a second stage of interpolation.
	 *
	 * This function calls the default. this is very logical since we want to test it.
	 */
	virtual
	void
	hook_postProcessing(
		GridContainer_t* const 		gridCont,
		const MarkerCollection_t& 	markerColl,
		const InterpolPlan_t& 		interpolPlan)
	 override
	{
		this->egd_markersToGridBase_t::hook_postProcessing(gridCont, markerColl, interpolPlan);
		return;
	}; //End: posutprcessing




	/**
	 * \brief	This function is responsible for applying
	 * 		 the boundary conditions.
	 *
	 * This function does nothing.
	 */
	virtual
	void
	hook_applyBoundaryCondition(
		GridContainer_t* const 		gridCont,
		const MarkerCollection_t& 	markerColl,
		const InterpolPlan_t& 		interpolPlan)
	 const
	 override
	{
		return;
		(void)gridCont;
		(void)markerColl;
		(void)interpolPlan;
	}; //End: boundary


	virtual
	void
	hook_inspectWeightedInterpol(
		const PropIdx_t& 		srcMIdx,
		const MarkerProperty_t& 	srcMProp,
		const PropIdx_t& 		weightMIdx,
		const MarkerProperty_t& 	weightMProp,
		const GridProperty_t& 		gProp)
	 const
	 override
	{
		std::cout
			<< "Perform weighthed interpolation."
			<< " Mapping " << srcMIdx << ", weighted with " << weightMIdx
			<< ", to " << gProp.getPropIdx() << " on " << printGridType(gProp.getType()) << "\n";
		this->egd_markersToGridBase_t::hook_inspectWeightedInterpol(srcMIdx, srcMProp, weightMIdx, weightMProp, gProp);

		return;
		(void)srcMProp;
		(void)weightMProp;
	}; //End weighted inspection


	virtual
	void
	hook_inspectNormalInterpol(
		const PropIdx_t& 		srcMIdx,
		const MarkerProperty_t& 	srcMProp,
		const GridProperty_t& 		gProp)
	 const
	 override
	{
		std::cout
			<< "Perform normal interpolation."
			<< " Mapping " << srcMIdx
			<< ", to " << gProp.getPropIdx() << " on " << printGridType(gProp.getType()) << "\n";
		this->egd_markersToGridBase_t::hook_inspectNormalInterpol(srcMIdx, srcMProp, gProp);

		return;
		(void)srcMProp;
	}; //End weighted inspection


	/*
	 * =========================
	 * Helper functions
	 *
	 * These functions are helper functions, they could be
	 * private, but we will make them protected. Also some
	 * of them are virtual, thus allowing a modification.
	 * They are not directly hooks.
	 */
protected:
	/*
	 * ===========================
	 * Private members
	 */
private:
}; //End class(egd_markersToGridTestImpl_t)




PGL_NS_END(egd)











