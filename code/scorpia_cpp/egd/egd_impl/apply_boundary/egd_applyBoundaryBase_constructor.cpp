/**
 * \brief	This file implements the constructors for the base implementation of the
 * 		 boundary applier.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundaryBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


egd_applyBoundaryBase_t::egd_applyBoundaryBase_t(
	const egd_confObj_t& 	confObj)
 noexcept
 :
  egd_applyBoundaryBase_t()	//Call the default and ignore argument
{
	(void)confObj;
}; //End: building process constructor.



egd_applyBoundaryBase_t::egd_applyBoundaryBase_t()
 noexcept
 = default;


egd_applyBoundaryBase_t::~egd_applyBoundaryBase_t()
 noexcept
 = default;


egd_applyBoundaryBase_t::egd_applyBoundaryBase_t(
	const egd_applyBoundaryBase_t&)
 noexcept
 = default;


egd_applyBoundaryBase_t&
egd_applyBoundaryBase_t::operator= (
	const egd_applyBoundaryBase_t&)
 noexcept
 = default;


egd_applyBoundaryBase_t::egd_applyBoundaryBase_t(
	egd_applyBoundaryBase_t&&)
 noexcept
 = default;


egd_applyBoundaryBase_t&
egd_applyBoundaryBase_t::operator= (
	egd_applyBoundaryBase_t&&)
 noexcept
 = default;

PGL_NS_END(egd)

