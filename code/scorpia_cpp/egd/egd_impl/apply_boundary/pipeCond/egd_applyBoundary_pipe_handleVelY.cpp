/**
 * \brief	This file contains the code for applying the condition to Velocity Y property.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundary_pipe.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

/**
 * \brief	This function applies the condition on theextended CC grid.
 *
 * \param  cond		The condition that are in place.
 * \param  gridGeo	The grid geometry.
 * \param  gProp	The grid that should be modified.
 */
static
void
egdInternal_velYOnExtCCGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_);


/**
 * \brief	This function applies the condition on the extended Vy grid.
 *
 * \param  cond		The condition that are in place.
 * \param  gridGeo	The grid geometry.
 * \param  gProp	The grid that should be modified.
 */
static
void
egdInternal_velYOnExtVyGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_);


void
egd_applyBoundaryPipe_t::applyPipeVelYBC(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	GridProperty_t& gProp = *gProp_;

	pgl_assert(cond.isFinal(), cond.isValid(),	//General tests
		   gridGeo.isGridSet(), gridGeo.isConstantGrid(),
		   gProp.hasValidGridType(), gProp.checkSize(),
		   cond.getPropIdx() == gProp.getRepresentant(),
		   cond.getPropIdx() == PropIdx_t::VelY());

	if(gProp.isExtendedGrid() == true)
	{
		/*
		 * We are on the extended grid
		 */
		switch(mkCase(gProp.getType()))
		{
		  case eGridType::CellCenter:
		  	egdInternal_velYOnExtCCGrid(cond, gridGeo, &gProp);
		  break;

		  case eGridType::StVy:
		  	egdInternal_velYOnExtVyGrid(cond, gridGeo, &gProp);
		  break;

		  default:
		  	throw PGL_EXCEPT_illMethod("VelY interpolation on " + gProp.getType() + " is not implemented.");
		  break;
		}; //End switch(gProp):

	//ENd if: extended grid
	}
	else
	{
		/*
		 * We are on the regular grid
		 */
			pgl_assert(gProp.isRegularGrid());

		switch(mkCase(gProp.getType()))
		{
		  default:
		  	throw PGL_EXCEPT_illMethod("VelY interpolation on " + gProp.getType() + " is not implemented.");
		  break;
		}; //End switch(gProp):

	}; //End else: regular grid

	return;
}; //ENd real apply


void
egd_applyBoundaryPipe_t::hook_handleVelY(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp_)
 const
{
	/*
	 * Thsi function is only to forward the request to a
	 * static function.
	 */
	egd_applyBoundaryPipe_t::applyPipeVelYBC(cond, gridGeo, gProp_);

	return;
}; //End: handle temperature


/*
 * ==========================================
 * Low Level impl
 */

void
egdInternal_velYOnExtCCGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	egd_applyBoundaryPipe_t::GridProperty_t& gProp = *gProp_;

	pgl_assert(gProp.onCellCentrePoints(),
		   gProp.isExtendedGrid(),
		   gProp.getPropIdx().getRepresentant() == PropIdx::VelY(),
		   cond.isFinal(),
		   cond.getPropIdx() == PropIdx::VelY());

	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_illMethod("Only supported for constant grids.");
	};


	/*
	 * For the extended grids, we have to use the rows and cols.
	 * But we use sligtly off variable names.
	 */
	const Index_t Nx = gProp.cols();
	const Index_t Ny = gProp.rows();
		pgl_assert(Ny > 0, Nx > 0);

	/*
	 * We do not have ghost nodes.
	 * Neuman conditions takes precedence over the Dirichlet
	 * condition, so Dirichlet is handled first.
	 */


	/*
	 * DIRICHELT
	 *
	 * Here we have to do linear interpolation.
	 * After some thinking we realize, that the
	 * condition becomes, that the mean of the two
	 * has to be the prescribed values. We can
	 * then rearange them, to get the final expression.
	 */
	{
		pgl_assert(cond.isDirichlet(eRandLoc::TOP   ),
			   cond.isDirichlet(eRandLoc::BOTTOM) );
		pgl_assert(cond.hasValue(eRandLoc::TOP   ),
			   cond.hasValue(eRandLoc::BOTTOM) );

		//Load the values
		const Numeric_t topVal = cond.getValue(eRandLoc::TOP   );
		const Numeric_t botVal = cond.getValue(eRandLoc::BOTTOM);

		//Load the indexes
		const Index_t outTopIdx = 0;			//Row at top, outside; to determine
		const Index_t  inTopIdx = outTopIdx + 1;	//Row at top, inside; given
		const Index_t outBotIdx = Ny - 1;		//Row at bottom, outside; to determine
		const Index_t  inBotIdx = outBotIdx - 1;	//Row at bottom, inside; given

		//Perform the setting
		gProp.row(outTopIdx) = 2.0 * topVal - gProp.row(inTopIdx);
		gProp.row(outBotIdx) = 2.0 * botVal - gProp.row(inBotIdx);
	}; //End scope: DIRICHLET


	/*
	 * NEUMAN
	 * On that grid the inner node and the outher node must
	 * have difference zero, so they have to be equal.
	 * We see that we can simly copy them.
	 */
	{
		pgl_assert(cond.isFreeSlip(eRandLoc::LEFT ),
			   cond.isFreeSlip(eRandLoc::RIGHT) );
		pgl_assert(cond.hasValue(eRandLoc::LEFT ) == false,
			   cond.hasValue(eRandLoc::RIGHT) == false );

		//Calculating the index
		const Index_t outLefIdx = 0;			//Left column, outside; to determine
		const Index_t  inLefIdx = outLefIdx + 1;	//Left column, inside; given
		const Index_t outRigIdx = Nx - 1;		//Right column, outside; to determine
		const Index_t  inRigIdx = outRigIdx - 1;	//Right column, inside; given

		//Setting the values
		gProp.col(outLefIdx) = gProp.col(inLefIdx);
		gProp.col(outRigIdx) = gProp.col(inRigIdx);
	}; //End scope: Neumann

	return;
}; //ENd: extended CC

void
egdInternal_velYOnExtVyGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	egd_applyBoundaryPipe_t::GridProperty_t& gProp = *gProp_;

	pgl_assert(gProp.onStVyPoints(),
		   gProp.isExtendedGrid(),
		   gProp.getPropIdx().getRepresentant() == PropIdx::VelY(),
		   cond.isFinal(),
		   cond.getPropIdx() == PropIdx::VelY());

	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_illMethod("Only supported for constant grids.");
	};


	/*
	 * For the extended grids, we have to use the rows and cols.
	 * But we use sligtly off variable names.
	 */
	const Index_t Cols = gProp.cols();
	const Index_t Rows = gProp.rows();
		pgl_assert(Cols > 0, Rows > 0);

	/*
	 * We have ghost nodes, the lowest row is
	 * not accessable. We will also handle the
	 * dirichlet condition first, so the Neumann
	 * will take precedence.
	 */

	/*
	 * DIRICHLET
	 * We can just set the value
	 */
	{
		pgl_assert(cond.isDirichlet(eRandLoc::TOP   ),
			   cond.isDirichlet(eRandLoc::BOTTOM) );
		pgl_assert(cond.hasValue(eRandLoc::TOP   ),
			   cond.hasValue(eRandLoc::BOTTOM) );

		//Load the values
		const Numeric_t topVal = cond.getValue(eRandLoc::TOP   );
		const Numeric_t botVal = cond.getValue(eRandLoc::BOTTOM);

		//load the index
		const Index_t topIdx   = 0;
		const Index_t botIdx   = Rows - 2;	//-1 becuase of teh 0 based index, -1 for the last one is the ghost row

		//Set the value
		gProp.row(topIdx) = topVal;
		gProp.row(botIdx) = botVal;
	}; //End scope(DIRICHLET):


	/*
	 * NEUMANN
	 * THe two sides are the same
	 */
	{
		pgl_assert(cond.isFreeSlip(eRandLoc::LEFT ),
			   cond.isFreeSlip(eRandLoc::RIGHT) );
		pgl_assert(cond.hasValue(eRandLoc::LEFT ) == false,
			   cond.hasValue(eRandLoc::RIGHT) == false );


		//load the indexes
		const Index_t outLefIdx = 0;			//To determine
		const Index_t  inLefIdx = outLefIdx + 1;
		const Index_t outRigIdx = Cols - 1;		//To determine, also for acounting
		const Index_t  inRigIdx = outRigIdx - 1;

		//Set the index
		gProp.col(outLefIdx) = gProp.col(inLefIdx);
		gProp.col(outRigIdx) = gProp.col(inRigIdx);
	}; //End scope(NEUMANN):

	return;
};//End: extended Vy

PGL_NS_END(egd)

