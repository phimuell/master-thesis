/**
 * \brief	This file contains the code that is able to handle the temperature conditions for the temperature.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundary_pipe.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

/*
 * \brief	This function applies the conditions on the basic grid.
 *
 * This function operates on the basic grid. It will apply the conditions.
 * This means it will manipulate the outer nodes.
 *
 * \param  cond		The condition that are in place.
 * \param  gridGeo	The grid geometry.
 * \param  gProp	The grid that should be modified.
 */
static
void
egdInternal_tempOnBasicGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_);


/**
 * \brief	This function applies the condition on theextended CC grid.
 *
 * \param  cond		The condition that are in place.
 * \param  gridGeo	The grid geometry.
 * \param  gProp	The grid that should be modified.
 */
static
void
egdInternal_tempOnExtCCGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_);




void
egd_applyBoundaryPipe_t::applyPipeTempBC(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	GridProperty_t& gProp = *gProp_;

	pgl_assert(cond.isFinal(), cond.isValid(),	//General tests
		   gridGeo.isGridSet(), gridGeo.isConstantGrid(),
		   gProp.hasValidGridType(), gProp.checkSize(),
		   cond.getPropIdx() == gProp.getRepresentant(),
		   cond.getPropIdx() == PropIdx_t::Temperature());

	if(gProp.isExtendedGrid() == true)
	{
		/*
		 * We are on the extended grid
		 */
		switch(mkCase(gProp.getType()))
		{
		  case eGridType::CellCenter:
		  	egdInternal_tempOnExtCCGrid(cond, gridGeo, &gProp);
		  break;

		  default:
		  	throw PGL_EXCEPT_illMethod("Temperature interpolation on " + gProp.getType() + " is not implemented.");
		  break;
		}; //End switch(gProp):

	//ENd if: extended grid
	}
	else
	{
		/*
		 * We are on the regular grid
		 */
			pgl_assert(gProp.isRegularGrid());

		switch(mkCase(gProp.getType()))
		{
		  case eGridType::BasicNode:
		  	egdInternal_tempOnBasicGrid(cond, gridGeo, &gProp);
		  break;

		  default:
		  	throw PGL_EXCEPT_illMethod("Temperature interpolation on " + gProp.getType() + " is not implemented.");
		  break;
		}; //End switch(gProp):

	}; //End else: regular grid

	return;
}; //ENd real apply


void
egd_applyBoundaryPipe_t::hook_handleTemperature(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp_)
 const
{
	/*
	 * Thsi function is only to forward the request to a
	 * static function.
	 */
	egd_applyBoundaryPipe_t::applyPipeTempBC(cond, gridGeo, gProp_);

	return;
}; //End: handle temperature


/*
 * ==========================================
 * Low Level impl
 */
void
egdInternal_tempOnBasicGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	egd_applyBoundaryPipe_t::GridProperty_t& gProp = *gProp_;

	pgl_assert(gProp.onBasicNodalPoints(),
		   gProp.isRegularGrid(),
		   gProp.getPropIdx().getRepresentant() == PropIdx::Temperature(),
		   cond.isFinal(),
		   cond.getPropIdx() == PropIdx::Temperature());

	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_illMethod("Only supported for constant grids.");
	};

	const Index_t Nx = gProp.Nx();
	const Index_t Ny = gProp.Ny();
		pgl_assert(Ny > 0, Ny == gridGeo.yNPoints(),
			   Nx > 0, Nx == gridGeo.xNPoints() );

	/*
	 * We do not have ghost nodes.
	 * The dirichlet condition takes precedence, so the
	 * Neuman conditions are handled first.
	 */


	/*
	 * NEUMAN
	 * On that grid the inner node and the outher node must
	 * have difference zero, so they have to be equal.
	 * We see that we can simly copy them.
	 */
	{
		pgl_assert(cond.hasValue(eRandLoc::LEFT ) == false,
			   cond.hasValue(eRandLoc::RIGHT) == false );
		pgl_assert(cond.isInsulation(eRandLoc::LEFT),
			   cond.isInsulation(eRandLoc::RIGHT) );

		gProp.col(     0) = gProp.col(     1);
		gProp.col(Nx - 1) = gProp.col(Nx - 2);	//Nx - 1 ~ Right most index
	}; //End scope: Neumann


	/*
	 * DIRICHELT
	 * We can just set it to the prescribed value
	 */
	{
		pgl_assert(cond.hasValue(eRandLoc::TOP   ),
			   cond.hasValue(eRandLoc::BOTTOM) );
		pgl_assert(cond.isDirichlet(eRandLoc::TOP   ),
			   cond.isDirichlet(eRandLoc::BOTTOM) );

		const Numeric_t topVal = cond.getValue(eRandLoc::TOP   );	//Load the values
		const Numeric_t botVal = cond.getValue(eRandLoc::BOTTOM);

		gProp.row(     0) = topVal;		//Set the top value
		gProp.row(Ny - 1) = botVal;	//Set bottom value; Ny - 1, lowest accessable row
	}; //End scope: DIRICHLET

	return;
}; //ENd: basic grid


void
egdInternal_tempOnExtCCGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	egd_applyBoundaryPipe_t::GridProperty_t& gProp = *gProp_;

	pgl_assert(gProp.onCellCentrePoints(),
		   gProp.isExtendedGrid(),
		   gProp.getPropIdx().getRepresentant() == PropIdx::Temperature(),
		   cond.isFinal(),
		   cond.getPropIdx() == PropIdx::Temperature());

	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_illMethod("Only supported for constant grids.");
	};


	/*
	 * For the extended grids, we have to use the rows and cols.
	 * But we use sligtly off variable names.
	 */
	const Index_t Nx = gProp.cols();
	const Index_t Ny = gProp.rows();
		pgl_assert(Ny > 0, Nx > 0);

	/*
	 * We do not have ghost nodes.
	 * The dirichlet condition takes precedence, so the
	 * Neuman conditions are handled first.
	 */


	/*
	 * NEUMAN
	 * On that grid the inner node and the outher node must
	 * have difference zero, so they have to be equal.
	 * We see that we can simly copy them.
	 */
	{
		pgl_assert(cond.hasValue(eRandLoc::LEFT ) == false,
			   cond.hasValue(eRandLoc::RIGHT) == false );
		pgl_assert(cond.isInsulation(eRandLoc::LEFT),
			   cond.isInsulation(eRandLoc::RIGHT) );

		gProp.col(     0) = gProp.col(     1);
		gProp.col(Nx - 1) = gProp.col(Nx - 2);	//Nx - 1 ~ Right most index
	}; //End scope: Neumann


	/*
	 * DIRICHELT
	 *
	 * Here we have to do linear interpolation.
	 * After some thinking we realize, that the
	 * condition becomes, that the mean of the two
	 * has to be the prescribed values. We can
	 * then rearange them, to get the final expression.
	 */
	{
		pgl_assert(cond.hasValue(eRandLoc::TOP   ),
			   cond.hasValue(eRandLoc::BOTTOM) );
		pgl_assert(cond.isDirichlet(eRandLoc::TOP   ),
			   cond.isDirichlet(eRandLoc::BOTTOM) );

		const Numeric_t topVal = cond.getValue(eRandLoc::TOP   );	//Load the values
		const Numeric_t botVal = cond.getValue(eRandLoc::BOTTOM);

		gProp.row(     0) = 2.0 * topVal - gProp.row(     1);
		gProp.row(Ny - 1) = 2.0 * botVal - gProp.row(Ny - 2);	//Set bottom value; Ny - 1, lowest accessable row
	}; //End scope: DIRICHLET

	return;
}; //ENd: extended CC


PGL_NS_END(egd)

