#pragma once
/**
 * \brief	This file is a concrete implementation of the pipe condition.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "../egd_applyBoundaryBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_applyBoundaryPipe_t
 * \brief	This class implements the boundary condition for pipes.
 *
 * A description of these condition is given in the README that is
 * located in teh corresponding source folder.
 */
class egd_applyBoundaryPipe_t : public egd_applyBoundaryBase_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using BaseImpl_t 	= egd_applyBoundaryBase_t;	//!< This is the base implementation
	using BaseInterface_t 	= BaseImpl_t::BaseInterface_t;	//!< This is the interface that is used

	using BoundCond_t 	= BaseImpl_t::BoundCond_t;	//!< This is the class for encoding the boundary condition.
	using GridProperty_t 	= BaseImpl_t::GridProperty_t;	//!< This is the class that represents a grid property.
	using GridGeometry_t 	= BaseImpl_t::GridGeometry_t;	//!< This is the class that represents a grid geometry.
	using SideCondition_t 	= BaseImpl_t::SideCondition_t;	//!< Condition at a single side.
	using PropIdx_t		= BaseImpl_t::PropIdx_t;	//!< The property index


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * This constructor also serves as building constructor.
	 * It is defaulted.
	 */
	egd_applyBoundaryPipe_t()
	 noexcept;


	/**
	 * \brief	This constructor operates on
	 * 		 the configuaration object.
	 *
	 * This cinstructor is needed for the building
	 * process of EGD. This constructor will pass
	 * the configuration object down to the base.
	 * Otherwhise it is ignored.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_applyBoundaryPipe_t(
		const egd_confObj_t& 		confObj)
	 noexcept;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryPipe_t(
		const egd_applyBoundaryPipe_t&)
	 noexcept;


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryPipe_t&
	operator= (
		const egd_applyBoundaryPipe_t&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_applyBoundaryPipe_t(
		egd_applyBoundaryPipe_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryPipe_t&
	operator= (
		egd_applyBoundaryPipe_t&&)
	 noexcept;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_applyBoundaryPipe_t()
 	 noexcept;


	/*
	 * ==========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function applies the conditions cond
	 * 		 to the grid property gProp.
	 *
	 * This function will call the base implementation.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	apply(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 override;


	/**
	 * \brief	This function prints out a small
	 * 		 identification of *this.
	 *
	 * Will output that this is a pipe condition.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/*
	 * ==========================
	 * BC Hooks
	 *
	 * Here are the BC hooks, they are called by the
	 * apply function of the base class.
	 */
protected:
	/**
	 * \brief	This function is called to handle the case of
	 * 		 a temperature property.
	 *
	 * This function applies dirichlet conditions at the top and
	 * an insulating condition at the bottom.
	 * The Dirichlet condition is also used in the corners.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleTemperature(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 override;


	/**
	 * \brief	This function is called to handle the case of
	 * 		 a VelX property.
	 *
	 * The left and right boundary are set to a dirichlet condition.
	 * At the top and bottom a Neumann condition is in place, that
	 * also applies to the corners.
	 *
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleVelX(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 override;


	/**
	 * \brief	This function is called to handle the case of
	 * 		 a VelY property.
	 *
	 * Top and bottom are set to a dirichelt condition. Left and
	 * right side are set to an free slip condition, that is also
	 * in place at the corners.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleVelY(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 override;


	/*
	 * ==========================
	 * LOGIC Hooks
	 */
protected:
	/**
	 * \brief	This function returns true if *this can
	 * 		 handle the passed condition.
	 *
	 * This function tests if the condition is such as it
	 * is outlined above. It will test the geomrtery, since
	 * only constant geometries are currently supported.
	 *
	 * \param  cond		The condition to test.
	 * \param  gridGeo	The grid geometry.
	 */
	virtual
	bool
	canHandleThat(
		const BoundCond_t& 	cond,
		const GridGeometry_t& 	gridGeo)
	 const
	 override;


	/*
	 * =============================
	 * S T A T I C     Functions
	 *
	 * All the concrete code is implemented in static functions.
	 * The virtual functions are only used as a wrapper.
	 * This way they can easaly be used outside if needed.
	 */
public:
	/**
	 * \brief	This function applies the boundary condition
	 * 		 to the temperature.
	 *
	 * This is a function that handles the condtions. It assumes that
	 * all checks are made. so no checking is performed. The conditions
	 * are applied as described above.
	 * Currently the extended CC and the Basic nodal grid are supported.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	static
	void
	applyPipeTempBC(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp);


	/**
	 * \brief	This function applies the boundary condition
	 * 		 to the Velocity X.
	 *
	 * This is a function that handles the condtions. It assumes that
	 * all checks are made. so no checking is performed. The conditions
	 * are applied as described above.
	 * Currently the extended CC is supported.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	static
	void
	applyPipeVelXBC(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp);


	/**
	 * \brief	This function applies the boundary condition
	 * 		 to the Velocity Y.
	 *
	 * This is a function that handles the condtions. It assumes that
	 * all checks are made. so no checking is performed. The conditions
	 * are applied as described above.
	 * Currently the extended CC is supported.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	static
	void
	applyPipeVelYBC(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp);


	/*
	 * ===========================
	 * Private Members
	 */
private:
	/*
	 * This class does not have any memebers
	 */
}; //End: class(egd_applyBoundaryPipe_t)

PGL_NS_END(egd)


