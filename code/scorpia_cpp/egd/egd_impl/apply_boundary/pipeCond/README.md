# Info
This folder contains the implementation of the so called pipe conditions.
That are conditions that are used in the original code.


# The Conditions
This are the conditions that are handled.

## TEMPERATURE
For the temperature we have insulation on the right and left side of the domain.
At the top and the bottom a Dirichlet condition is in place.
Note that the Dirichlet condition are also in place at the corners.

## VELX
The x velocity applies a Dirichlet condition at the left and right side of the domain.
This means the normal component at the boundary is prescribed.
The top and the bottom are handled by a Neumann condition.
This means that the velocity (x) derivative in tangential component (y) is set to a value, here zero.
The Neumann condition takes precedence over the Dirichlet condition, thus the corner points are handled by them.

## VELY
The y component of the velocity is handled similarly to the x component.
There where the velocity is normal, here at the top and bottom, a Dirichlet condition is in place.
Boundaries that are tangential to the velocity, here the left and right side, are governed by a Neumann condition.
As the X component the Neumann condition takes precedence over the Dirichlet, meaning that the corners are handled by the Neumann conditions.


# Impl Notes
Here are some notes for the implementation.
Currently only constant grids are supported.
Also not all grid combinations are supported.





