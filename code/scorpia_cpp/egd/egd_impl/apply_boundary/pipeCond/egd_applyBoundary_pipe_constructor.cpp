/**
 * \brief	This file implementents the constructor code for the pipe condition.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundary_pipe.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_applyBoundaryPipe_t::egd_applyBoundaryPipe_t(
	const egd_confObj_t& 		confObj)
 noexcept
 :
  egd_applyBoundaryBase_t(confObj)
{};


egd_applyBoundaryPipe_t::egd_applyBoundaryPipe_t()
 noexcept
 = default;


egd_applyBoundaryPipe_t::~egd_applyBoundaryPipe_t()
 noexcept
 = default;


egd_applyBoundaryPipe_t::egd_applyBoundaryPipe_t(
	const egd_applyBoundaryPipe_t&)
 noexcept
 = default;


egd_applyBoundaryPipe_t&
egd_applyBoundaryPipe_t::operator= (
	const egd_applyBoundaryPipe_t&)
 noexcept
 = default;


egd_applyBoundaryPipe_t::egd_applyBoundaryPipe_t(
	egd_applyBoundaryPipe_t&&)
 noexcept
 = default;


egd_applyBoundaryPipe_t&
egd_applyBoundaryPipe_t::operator= (
	egd_applyBoundaryPipe_t&&)
 noexcept
 = default;

PGL_NS_END(egd)

