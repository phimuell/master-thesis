/**
 * \brief	This file implements some general function for the pipe condition.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundary_pipe.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


bool
egd_applyBoundaryPipe_t::canHandleThat(
	const BoundCond_t& 	cond,
	const GridGeometry_t& 	gridGeo)
 const
{
	pgl_assert(cond.isFinal(), cond.isValid(),	//General tests
		   gridGeo.isGridSet());

	if(gridGeo.isConstantGrid() == false)	//We can only handle constant grids
	{
		return false;
	}; //End if: check grid consta


	/*
	 * Now we will check if the condition contains
	 * the information we expect.
	 */
	switch(cond.getPropIdx().getProp() )
	{

	  case eMarkerProp::Temperature:
	  {
	  	  if(cond.isDirichlet (eRandLoc::TOP   ) == false ||
	  	     cond.isInsulation(eRandLoc::RIGHT ) == false ||
	  	     cond.isDirichlet (eRandLoc::BOTTOM) == false ||
	  	     cond.isInsulation(eRandLoc::LEFT  ) == false   )
		  {
		  	  return false;
		  };
	  };
	  break; //End case(temperature):


	  case eMarkerProp::VelX:
	  {
	  	  if(cond.isFreeSlip (eRandLoc::TOP   ) == false ||
	  	     cond.isDirichlet(eRandLoc::RIGHT ) == false ||
	  	     cond.isFreeSlip (eRandLoc::BOTTOM) == false ||
	  	     cond.isDirichlet(eRandLoc::LEFT  ) == false   )
		  {
		  	  return false;
		  }
	  };
	  break; //End case(VelX):


	  case eMarkerProp::VelY:
	  {
	  	  if(cond.isDirichlet(eRandLoc::TOP   ) == false ||
	  	     cond.isFreeSlip (eRandLoc::LEFT  ) == false ||
	  	     cond.isDirichlet(eRandLoc::BOTTOM) == false ||
	  	     cond.isFreeSlip (eRandLoc::RIGHT ) == false   )
		  {
		  	  return false;
		  };
	  };
	  break; //End case(VelY):

	  default:
	  	return false;
	  break;
	}; //End switch(gProp):


	/*
	 * If we are here, then we can handle the condition.
	 * So we return true.
	 */
	return true;
}; //End: can handle this


std::string
egd_applyBoundaryPipe_t::print()
 const
{
	return std::string("STandard pipe condition.");
}; //End print:


void
egd_applyBoundaryPipe_t::apply(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp)
 const
{
	this->BaseImpl_t::apply(cond, gridGeo, gProp);
	return;
}; //End apply




PGL_NS_END(egd)

