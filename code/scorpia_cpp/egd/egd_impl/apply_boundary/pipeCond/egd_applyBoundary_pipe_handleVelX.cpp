/**
 * \brief	This file contains the code for applying the condition to Velocity X properties
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundary_pipe.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

/**
 * \brief	This function applies the condition on theextended CC grid.
 *
 * \param  cond		The condition that are in place.
 * \param  gridGeo	The grid geometry.
 * \param  gProp	The grid that should be modified.
 */
static
void
egdInternal_velXOnExtCCGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_);


/**
 * \brief	This function applies the condition on theextended Vx grid.
 *
 * \param  cond		The condition that are in place.
 * \param  gridGeo	The grid geometry.
 * \param  gProp	The grid that should be modified.
 */
static
void
egdInternal_velXOnExtVxGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_);


void
egd_applyBoundaryPipe_t::applyPipeVelXBC(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	GridProperty_t& gProp = *gProp_;

	pgl_assert(cond.isFinal(), cond.isValid(),	//General tests
		   gridGeo.isGridSet(), gridGeo.isConstantGrid(),
		   gProp.hasValidGridType(), gProp.checkSize(),
		   cond.getPropIdx() == gProp.getRepresentant(),
		   cond.getPropIdx() == PropIdx_t::VelX());

	if(gProp.isExtendedGrid() == true)
	{
		/*
		 * We are on the extended grid
		 */
		switch(mkCase(gProp.getType()))
		{
		  case eGridType::CellCenter:
		  	egdInternal_velXOnExtCCGrid(cond, gridGeo, &gProp);
		  break;

		  case eGridType::StVx:
		  	egdInternal_velXOnExtVxGrid(cond, gridGeo, &gProp);
		  break;

		  default:
		  	throw PGL_EXCEPT_illMethod("VelX interpolation on " + gProp.getType() + " is not implemented.");
		  break;
		}; //End switch(gProp):

	//ENd if: extended grid
	}
	else
	{
		/*
		 * We are on the regular grid
		 */
			pgl_assert(gProp.isRegularGrid());

		switch(mkCase(gProp.getType()))
		{
		  default:
		  	throw PGL_EXCEPT_illMethod("VelX interpolation on " + gProp.getType() + " is not implemented.");
		  break;
		}; //End switch(gProp):

	}; //End else: regular grid

	return;
}; //ENd real apply


void
egd_applyBoundaryPipe_t::hook_handleVelX(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp_)
 const
{
	/*
	 * Thsi function is only to forward the request to a
	 * static function.
	 */
	egd_applyBoundaryPipe_t::applyPipeVelXBC(cond, gridGeo, gProp_);

	return;
}; //End: handle temperature


/*
 * ==========================================
 * Low Level impl
 */

void
egdInternal_velXOnExtCCGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	egd_applyBoundaryPipe_t::GridProperty_t& gProp = *gProp_;

	pgl_assert(gProp.onCellCentrePoints(),
		   gProp.isExtendedGrid(),
		   gProp.getPropIdx().getRepresentant() == PropIdx::VelX(),
		   cond.isFinal(),
		   cond.getPropIdx() == PropIdx::VelX());

	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_illMethod("Only supported for constant grids.");
	};


	/*
	 * For the extended grids, we have to use the rows and cols.
	 * But we use sligtly off variable names.
	 */
	const Index_t Nx = gProp.cols();
	const Index_t Ny = gProp.rows();
		pgl_assert(Ny > 0, Nx > 0);

	/*
	 * We do not have ghost nodes.
	 * Neuman conditions takes precedence over the Dirichlet
	 * condition, so Dirichlet is handled first.
	 */


	/*
	 * DIRICHELT
	 *
	 * Here we have to do linear interpolation.
	 * After some thinking we realize, that the
	 * condition becomes, that the mean of the two
	 * has to be the prescribed values. We can
	 * then rearange them, to get the final expression.
	 */
	{
		pgl_assert(cond.isDirichlet(eRandLoc::LEFT ),
			   cond.isDirichlet(eRandLoc::RIGHT) );
		pgl_assert(cond.hasValue(eRandLoc::LEFT ),
			   cond.hasValue(eRandLoc::RIGHT) );

		//Load the values
		const Numeric_t  leftVal = cond.getValue(eRandLoc::LEFT );
		const Numeric_t rightVal = cond.getValue(eRandLoc::RIGHT);

		//Get the indexes
		const Index_t outLeftIdx  = 0;			//Column most left and outside; to determine
		const Index_t  inLeftIdx  = outLeftIdx + 1;	//Column left, but inside; given
		const Index_t outRightIdx = Nx - 1;		//Column most right, outside; to determine
		const Index_t  inRightIdx = outRightIdx - 1;	//Column right, but inside; given

		//Perfomr the interpolation
		gProp.col( outLeftIdx)    = 2.0 * leftVal  - gProp.col(inLeftIdx);
		gProp.col(outRightIdx)	  = 2.0 * rightVal - gProp.col(inRightIdx);
	}; //End scope: DIRICHLET


	/*
	 * NEUMAN
	 * On that grid the inner node and the outher node must
	 * have difference zero, so they have to be equal.
	 * We see that we can simly copy them.
	 */
	{
		pgl_assert(cond.isFreeSlip(eRandLoc::TOP   ),
			   cond.isFreeSlip(eRandLoc::BOTTOM) );
		pgl_assert(cond.hasValue(eRandLoc::TOP   ) == false,
			   cond.hasValue(eRandLoc::BOTTOM) == false );

		//Get the index that we need
		const Index_t outTopIdx = 0;			//Top row, outside; to determine
		const Index_t  inTopIdx = outTopIdx + 1;	//Row at top and inside; given
		const Index_t outBotIdx = Ny - 1;		//Bottom row, outside; to determine
		const Index_t  inBotIdx = outBotIdx - 1; 	//Row at bottom, but inside; given

		//Copy the values
		gProp.row(outTopIdx) = gProp.row(inTopIdx);
		gProp.row(outBotIdx) = gProp.row(inBotIdx);
	}; //End scope: Neumann

	return;
}; //ENd: extended CC



void
egdInternal_velXOnExtVxGrid(
	const egd_applyBoundaryBase_t::BoundCond_t& 		cond,
	const egd_applyBoundaryPipe_t::GridGeometry_t& 		gridGeo,
	egd_applyBoundaryPipe_t::GridProperty_t* const 		gProp_)
{
	pgl_assert(gProp_ != nullptr);	//Alias
	egd_applyBoundaryPipe_t::GridProperty_t& gProp = *gProp_;

	pgl_assert(gProp.onStVxPoints(),
		   gProp.isExtendedGrid(),
		   gProp.getPropIdx().getRepresentant() == PropIdx::VelX(),
		   cond.isFinal(),
		   cond.getPropIdx() == PropIdx::VelX());

	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_illMethod("Only supported for constant grids.");
	};


	/*
	 * For the extended grids, we have to use the rows and cols.
	 * But we use sligtly off variable names.
	 */
	const Index_t Cols = gProp.cols();
	const Index_t Rows = gProp.rows();
		pgl_assert(Cols > 0, Rows > 0);

	/*
	 * On this grid we do have ghost nodes
	 * So we will now adjust for them.
	 * Also Dirichlet are handled before Neuman are handled.
	 * So Neumann takes precedence
	 */

	/*
	 * DIRICHELT
	 * Here we can just set the node to the value we want.
	 */
	{
		pgl_assert(cond.isDirichlet(eRandLoc::LEFT ),
			   cond.isDirichlet(eRandLoc::RIGHT) );
		pgl_assert(cond.hasValue(eRandLoc::LEFT ),
			   cond.hasValue(eRandLoc::RIGHT) );

		//Load indexing
		const Index_t  leftMostIdx = 0;
		const Index_t rightMostIdx = Cols - 2;	//-1 for the 0 based indexing, and theright most is a ghost node.

		//Load the values
		const Numeric_t  leftVal = cond.getValue(eRandLoc::LEFT );
		const Numeric_t rightVal = cond.getValue(eRandLoc::RIGHT);

		gProp.col( leftMostIdx) = leftVal;
		gProp.col(rightMostIdx) = rightVal;
	}; //End scope(DIRICHLET):


	/*
	 * FREESLIP
	 * The two nodes have the dame value
	 */
	{
		pgl_assert(cond.isFreeSlip(eRandLoc::TOP   ),
			   cond.isFreeSlip(eRandLoc::BOTTOM) );
		pgl_assert(cond.hasValue(eRandLoc::TOP   ) == false,
			   cond.hasValue(eRandLoc::BOTTOM) == false );

		//Indexing
		const Index_t outTopIdx = 0;			//Outside to determine
		const Index_t  inTopIdx = outTopIdx + 1;	//Inside teh domain, will determine
		const Index_t outBotIdx = Rows - 1;		//Outside, to determine, -1 for accounting
		const Index_t  inBotIdx = outBotIdx - 1;	//Inside

		gProp.row(outTopIdx) = gProp.row(inTopIdx);
		gProp.row(outBotIdx) = gProp.row(inBotIdx);
	}; //End scope(FREESPLIP):

	return;
}; //End: handle VelXGhjostNode



PGL_NS_END(egd)

