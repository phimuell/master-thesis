/**
 * \brief	This file implements the apply function form the interface.
 *
 * This function will determine which concrete implementation should be called.
 * It will then forward the request to the function that handles it.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundaryBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


void
egd_applyBoundaryBase_t::apply(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp_)
 const
{
	pgl_assert(gProp_ != nullptr);
	GridProperty_t& gProp = *gProp_;

	if(cond.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid condition.");
	};
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid is not set.");
	};
	if(gProp.getPropIdx().isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid property has an invalid grid index.");
	};
	if(gProp.hasValidGridType() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid property has an invalid grid type.");
	};
	if(gProp.checkSize() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid property does not have consistent size.");
	};
	if(gProp.getRepresentant() != cond.getPropIdx())
	{
		throw PGL_EXCEPT_InvArg("The property indees are not correct, the grid has " + gProp.getPropIdx() + "("
				+ gProp.getRepresentant() + "), but the condition has " + cond.getPropIdx() + ".");
	};


	/*
	 * ======================================
	 */

	//Now we check if the impl can handle the conditions
	if(this->canHandleThat(cond, gridGeo) == false)
	{
		throw PGL_EXCEPT_RUNTIME("The implementation can not handle the passed condition for property " + cond.getPropIdx() + "!");
	};


	/*
	 * We knwo that the code can handle it, so we now
	 * implement the selcting process.
	 */
	switch(gProp.getRepresentant().getProp())
	{

	  case eMarkerProp::Temperature:
	  {
	  	//Handle the temperature
	  	this->hook_handleTemperature(cond, gridGeo, &gProp);
	  };
	  break; //End case(temperature):


	  case eMarkerProp::VelX:
	  {
	  	  this->hook_handleVelX(cond, gridGeo, &gProp);
	  };
	  break; //End case(VelX):


	  case eMarkerProp::VelY:
	  {
	  	  this->hook_handleVelY(cond, gridGeo, &gProp);
	  };
	  break; //End case(VelY):

	  default:
	  	throw PGL_EXCEPT_illMethod("Currently the base implementation is not able to handle a condition for " + gProp.getRepresentant());
	  break;
	}; //End switch(gProp):


	pgl_assert(gProp.checkSize(),
		   gProp.isFinite());


	return;
}; //End: apply condition interface.


PGL_NS_END(egd)

