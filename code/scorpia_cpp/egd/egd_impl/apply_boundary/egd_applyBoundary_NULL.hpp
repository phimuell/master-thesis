#pragma once
/**
 * \brief	This file implements the NULL boundary.
 *
 * This is an object that is able to do nothing.
 * It is just justed, such that an instance can be created.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundaryBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_applyBoundaryNULL_t
 * \brief	This is the NULL implementation of the boundary object.
 *
 * This interface is not able to perform any operations, all functions,
 * except some fews will throw when tzhey are called. This object is
 * provided, such that it exists.
 *
 */
class egd_applyBoundaryNULL_t : public egd_applyBoundaryBase_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using BaseImpl_t 	= egd_applyBoundaryBase_t;	//!< This is the base implementation
	using BaseInterface_t 	= BaseImpl_t::BaseInterface_t;	//!< This is the interface that is used

	using BoundCond_t 	= BaseImpl_t::BoundCond_t;	//!< This is the class for encoding the boundary condition.
	using GridProperty_t 	= BaseImpl_t::GridProperty_t;	//!< This is the class that represents a grid property.
	using GridGeometry_t 	= BaseImpl_t::GridGeometry_t;	//!< This is the class that represents a grid geometry.
	using SideCondition_t 	= BaseImpl_t::SideCondition_t;	//!< Condition at a single side.
	using PropIdx_t		= BaseImpl_t::PropIdx_t;	//!< The property index


	/*
	 * =========================
	 * Constructors
	 *
	 * Only destructor is public.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_applyBoundaryNULL_t()
 	 noexcept
 	 = default;


 	/**
 	 * \brief	This constructor is requiered for
 	 * 		 the building process.
 	 *
 	 * This constructor calls the default one. Its
 	 * argument is ignored.
 	 *
 	 * \param  confObj	The configuration object.
 	 */
 	egd_applyBoundaryNULL_t(
 		const egd_confObj_t& 	confObj)
 	 noexcept
 	 :
 	  BaseImpl_t(confObj)
 	{};


	/**
	 * \brief	Default constructor.
	 */
	egd_applyBoundaryNULL_t()
	 noexcept
	 = default;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryNULL_t(
		const egd_applyBoundaryNULL_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryNULL_t&
	operator= (
		const egd_applyBoundaryNULL_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_applyBoundaryNULL_t(
		egd_applyBoundaryNULL_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryNULL_t&
	operator= (
		egd_applyBoundaryNULL_t&&)
	 noexcept
	 = default;



	/*
	 * ==========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function prints out a small
	 * 		 identification of *this.
	 *
	 * This is useful for debugging and outputing.
	 */
	virtual
	std::string
	print()
	 const
	 override
	{
		return std::string("NULL Boundary implementation.");
	};


	/*
	 * ==========================
	 * BC Hooks
	 *
	 * Here are the hooks that are connected to
	 * the implementation of the applying of BC.
	 * They are implemented, but the implentation
	 * throws if they are called.
	 *
	 * Note that the function must accept the
	 * properties in a grid agnostic way.
	 *
	 * You have to seperate them from the logic hooks.
	 */
protected:
	/**
	 * \brief	This function is called to handle the case of
	 * 		 a temperature property.
	 *
	 * This function throws.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleTemperature(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 override
	{
		throw PGL_EXCEPT_illMethod("The NULL boundary does nothing.");
		(void)cond;
		(void)gridGeo;
		(void)gProp;
	};


	/**
	 * \brief	This function is called to handle the case of
	 * 		 a VelX property.
	 *
	 * This function throws.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleVelX(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
 	 const
	 override
	{
		throw PGL_EXCEPT_illMethod("The NULL boundary does nothing.");
		(void)cond;
		(void)gridGeo;
		(void)gProp;
	};


	/**
	 * \brief	This function is called to handle the case of
	 * 		 a VelY property.
	 *
	 * This function throws.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleVelY(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
 	 const
	 override
	{
		throw PGL_EXCEPT_illMethod("The NULL boundary does nothing.");
		(void)cond;
		(void)gridGeo;
		(void)gProp;
	};



	/*
	 * ==========================
	 * LOGIC Hooks
	 *
	 * The logic hooks are hooks that allows
	 * to influence the behaviour of the base
	 * from a deriving class. They have more
	 * a logical touch than implementing of
	 * the boundary.
	 */
protected:
	/**
	 * \brief	This function returns true if *this can
	 * 		 handle the passed condition.
	 *
	 * This function returns false.
	 *
	 * \param  cond		The condition to test.
	 * \param  gridGeo	The grid geometry.
	 */
	virtual
	bool
	canHandleThat(
		const BoundCond_t& 	cond,
		const GridGeometry_t& 	gridGeo)
	 const
	 override
	{
		return false;
		(void)cond;
		(void)gridGeo;
	};


	/*
	 * ===========================
	 * Protected constructors.
	 */
protected:


	/*
	 * ===========================
	 * Private Members
	 */
private:
	/*
	 * This class does not have any memebers
	 */
}; //End: class(egd_applyBoundaryNULL_t)

PGL_NS_END(egd)


