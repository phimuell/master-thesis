/**
 * \brief	This file contains the definition of default hooks.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundaryBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


void
egd_applyBoundaryBase_t::hook_handleTemperature(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp)
 const
{
	pgl_assert(gProp != nullptr);

	throw PGL_EXCEPT_illMethod("This method is not implemented.");

	return;
	(void)cond;
	(void)gridGeo;
	(void)gProp;
}; //End: handle temperature


void
egd_applyBoundaryBase_t::hook_handleVelX(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp)
 const
{
	pgl_assert(gProp != nullptr);

	throw PGL_EXCEPT_illMethod("This method is not implemented.");

	return;
	(void)cond;
	(void)gridGeo;
	(void)gProp;
}; //End: handle vel x


void
egd_applyBoundaryBase_t::hook_handleVelY(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp)
 const
{
	pgl_assert(gProp != nullptr);

	throw PGL_EXCEPT_illMethod("This method is not implemented.");

	return;
	(void)cond;
	(void)gridGeo;
	(void)gProp;
}; //End: handle vel y


PGL_NS_END(egd)

