/**
 * \brief	This file contains the code for applying the condition to Velocity Y properties
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundary_shearing.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)

void
egd_applyBoundaryShear_t::hook_handleVelY(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp)
 const
{
	if(gProp->Nx() != gProp->Ny())
	{
		throw PGL_EXCEPT_InvArg("Only sqare domains are supported.");
	};
	if(gProp->isExtendedGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("Only extended grids are supported.");
	};
		pgl_assert(gProp->getPropIdx().getRepresentant() == PropIdx_t::VelY());

	/*
	 * We will first apply the pipe condition
	 * for that we will call the base implementation
	 * We use the virtual function to ensure the correct
	 * calling
	 */
	this->BaseImpl_t::hook_handleVelY(cond, gridGeo, gProp);


	/*
	 * Now we will apply the internal shearing condition.
	 * We do this my simply calling the static function.
	 */
	if(gProp->onStVyPoints() == true)
	{
		egd_applyBoundaryShear_t::applyInnerShearingVelYBC(cond, gridGeo, gProp);
	};

	return;
}; //End: hook


/*
 * =======================
 * Static worker function
 */
void
egd_applyBoundaryShear_t::applyInnerShearingVelYBC(
	const BoundCond_t& 		cond,
	const GridGeometry_t& 		gridGeo,
	GridProperty_t* const 		gProp)
{
	pgl_assert(cond.isValid(), cond.hasInternalBC(),
		   gridGeo.isGridSet());

	if(cond.getPropIdx().getRepresentant() != PropIdx_t::VelY())
	{
		throw PGL_EXCEPT_InvArg("The passed grid condition is not a y velcocity, instead it is a " + cond.getPropIdx());
	};
	if(gProp->getRepresentant() != PropIdx_t::VelY())
	{
		throw PGL_EXCEPT_InvArg("The grid property is not a y velcoity, instead it is " + gProp->getPropIdx());
	};
	if(gProp->onStVyPoints() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid property is not defined on the VX grid.");
	};
	if(gProp->Nx() != gProp->Ny())
	{
		throw PGL_EXCEPT_InvArg("Only sqare domains are supported.");
	};
	if(gProp->isExtendedGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("Only extended grids are supported.");
	};

	/*
	 * Load the conditions
	 */
	const SideCondition_t lowerWedge = cond.getSideCond(eRandLoc::INT_LOWSHEAR);	pgl_assert(lowerWedge.isDirichlet());
	const SideCondition_t upperWedge = cond.getSideCond(eRandLoc::INT_UPSHEAR );	pgl_assert(upperWedge.isDirichlet());

	//Unpack the value
	const Numeric_t lowerSpeed = lowerWedge.getValue();
	const Numeric_t upperSpeed = upperWedge.getValue();

	/*
	 * We will now apply the condition
	 *
	 * Note that the code is tranlated from Taras' "Marker_divergence.m" script,
	 * with some additions to account for the different indexing used in MATLAB.
	 *
	 * We will iterate through the i range and apply.
	 */
	const Index_t Ny1 = gProp->rows();
	const Index_t Ny  = gProp->Ny();
	const Index_t Nx1 = gProp->cols();

	for(Index_t iM = 1; iM <= Ny1; ++iM)	//note *M means Matlab convention
	{
		//This is the test from mathlab simply copied
		//TODO: check me
		if((iM > 3) && (iM < (Ny - 2)) )	//note jew is Ny1 in the VX case.
		{
			//now we create the j values.
			const Index_t jM_lower = iM + 1;	//lower shear index
			const Index_t jM_upper = iM + 3;	//upper shear index

			//bow we will transfere them into C++ indexes
			//We jave to subtract one for that, because of the counting
			const Index_t i       = iM       - 1;
			const Index_t j_lower = jM_lower - 1;
			const Index_t j_upper = jM_upper - 1;

			//setting the value
			gProp->operator()(i, j_lower) = lowerSpeed;
			gProp->operator()(i, j_upper) = upperSpeed;
		}; //End if:
	}; //End for(iM):

	return;
	(void)Nx1;
}; //End static apply


PGL_NS_END(egd)

