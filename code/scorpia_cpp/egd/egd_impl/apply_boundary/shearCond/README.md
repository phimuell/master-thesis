# Info
This is the shear condition. This is a boundary condition that is
technically equivalent to the pipe condition, but is extended by
an internal boundary condition.

## Internal Condition
The internal condition is a shear condition. See Taras' book,
chapter 8 figure 8.12. There is a diagonal line, in which one
side goes down and the other remains the same.
Note that although there are only two diagonal line involved
in the internal boundary, they are not adjacent to each other.
There is a diagonal in between. This is needed to guarantee
that the continuity equation is respected in each cell.


## Special behavior
Depending on the grid type that is used the internal boundary
is not respected. The internal boundary condition is only respected,
on the staggered velocity points. This means that on the CC/StPressure
and the basic grid, the condition is not applied.


