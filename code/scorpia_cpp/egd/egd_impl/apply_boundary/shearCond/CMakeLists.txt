# This file will add the implementation for the shear condition to EGD.

target_sources(
	egd
  PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/egd_applyBoundary_shearing.cpp"

	# Concrete properties
	"${CMAKE_CURRENT_LIST_DIR}/egd_applyBoundary_handleVelX_shearing.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_applyBoundary_handleVelY_shearing.cpp"

  PUBLIC
	"${CMAKE_CURRENT_LIST_DIR}/egd_applyBoundary_shearing.hpp"
)

