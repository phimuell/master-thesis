/**
 * \brief	This file contains constructors and other functions of the shear class.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "./egd_applyBoundary_shearing.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


bool
egd_applyBoundaryShear_t::canHandleThat(
	const BoundCond_t& 	cond,
	const GridGeometry_t& 	gridGeo)
 const
{
	pgl_assert(cond.isFinal(), cond.isValid(),	//General tests
		   gridGeo.isGridSet());

	if(gridGeo.isConstantGrid() == false)	//We can only handle constant grids
	{
		return false;
	}; //End if: check grid consta

	if(cond.hasInternalBC() == false)
	{
		throw PGL_EXCEPT_LOGIC("The passed condition does not have a internal component. Condition was " + cond.print());
	};


	/*
	 * Now we will check if the condition contains
	 * the information we expect.
	 */
	switch(cond.getPropIdx().getProp() )
	{

	  case eMarkerProp::Temperature:
	  {
	  	  if(cond.isDirichlet (eRandLoc::TOP   ) == false ||
	  	     cond.isInsulation(eRandLoc::RIGHT ) == false ||
	  	     cond.isDirichlet (eRandLoc::BOTTOM) == false ||
	  	     cond.isInsulation(eRandLoc::LEFT  ) == false   )
		  {
		  	  return false;
		  };
	  };
	  break; //End case(temperature):


	  case eMarkerProp::VelX:
	  {
	  	  if(cond.isFreeSlip (eRandLoc::TOP         ) == false ||
	  	     cond.isDirichlet(eRandLoc::RIGHT       ) == false ||
	  	     cond.isFreeSlip (eRandLoc::BOTTOM      ) == false ||
	  	     cond.isDirichlet(eRandLoc::LEFT        ) == false ||
	  	     cond.isDirichlet(eRandLoc::INT_LOWSHEAR) == false ||	//Handle internal condition
		     cond.isDirichlet(eRandLoc::INT_UPSHEAR ) == false   )
		  {
		  	  return false;
		  }
	  };
	  break; //End case(VelX):


	  case eMarkerProp::VelY:
	  {
	  	  if(cond.isDirichlet(eRandLoc::TOP         ) == false ||
	  	     cond.isFreeSlip (eRandLoc::LEFT        ) == false ||
	  	     cond.isDirichlet(eRandLoc::BOTTOM      ) == false ||
	  	     cond.isFreeSlip (eRandLoc::RIGHT       ) == false ||
	  	     cond.isDirichlet(eRandLoc::INT_LOWSHEAR) == false ||	//Handle internal condition
		     cond.isDirichlet(eRandLoc::INT_UPSHEAR ) == false   )
		  {
		  	  return false;
		  };
	  };
	  break; //End case(VelY):

	  default:
	  	return false;
	  break;
	}; //End switch(gProp):


	/*
	 * If we are here, then we can handle the condition.
	 * So we return true.
	 */
	return true;
}; //End: can handle this


std::string
egd_applyBoundaryShear_t::print()
 const
{
	return std::string("Standard shear pipe condition.");
}; //End print:



/*
 * =======================
 * Constructors
 */

egd_applyBoundaryShear_t::egd_applyBoundaryShear_t(
	const egd_confObj_t& 		confObj)
 noexcept
 :
  BaseImpl_t(confObj)
{};


egd_applyBoundaryShear_t::egd_applyBoundaryShear_t()
 noexcept
 = default;


egd_applyBoundaryShear_t::~egd_applyBoundaryShear_t()
 noexcept
 = default;


egd_applyBoundaryShear_t::egd_applyBoundaryShear_t(
	const egd_applyBoundaryShear_t&)
 noexcept
 = default;


egd_applyBoundaryShear_t&
egd_applyBoundaryShear_t::operator= (
	const egd_applyBoundaryShear_t&)
 noexcept
 = default;


egd_applyBoundaryShear_t::egd_applyBoundaryShear_t(
	egd_applyBoundaryShear_t&&)
 noexcept
 = default;


egd_applyBoundaryShear_t&
egd_applyBoundaryShear_t::operator= (
	egd_applyBoundaryShear_t&&)
 noexcept
 = default;


PGL_NS_END(egd)

