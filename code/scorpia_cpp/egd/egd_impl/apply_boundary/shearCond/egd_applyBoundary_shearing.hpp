#pragma once
/**
 * \brief	This file contains the definition of the class that is used to implement the shear boundaries.
 *
 * This is an extension of the pipe conditions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include "../egd_applyBoundaryBase.hpp"
#include "../pipeCond/egd_applyBoundary_pipe.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_applyBoundaryShear_t
 * \brief	This class implements the boundary condition for shearing.
 *
 * This class extends the pipe boundary class in the way that it
 * introduces also an internal boundary condition. As it is described
 * in the README, the condition is only applied on the staggered
 * velocity grids.
 *
 * It is implemented in that way too, meaning that it will use the
 * implementation of the base, but will then apply its own boundary.
 */
class egd_applyBoundaryShear_t : public egd_applyBoundaryPipe_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using BaseImpl_t 	= egd_applyBoundaryPipe_t;	//!< This is the base implementation
	using BaseInterface_t 	= BaseImpl_t::BaseInterface_t;	//!< This is the interface that is used

	using BoundCond_t 	= BaseImpl_t::BoundCond_t;	//!< This is the class for encoding the boundary condition.
	using GridProperty_t 	= BaseImpl_t::GridProperty_t;	//!< This is the class that represents a grid property.
	using GridGeometry_t 	= BaseImpl_t::GridGeometry_t;	//!< This is the class that represents a grid geometry.
	using SideCondition_t 	= BaseImpl_t::SideCondition_t;	//!< Condition at a single side.
	using PropIdx_t		= BaseImpl_t::PropIdx_t;	//!< The property index


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * This constructor also serves as building constructor.
	 * It is defaulted.
	 */
	egd_applyBoundaryShear_t()
	 noexcept;


	/**
	 * \brief	This constructor operates on
	 * 		 the configuaration object.
	 *
	 * This cinstructor is needed for the building
	 * process of EGD. This constructor will pass
	 * the configuration object down to the base.
	 * Otherwhise it is ignored.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_applyBoundaryShear_t(
		const egd_confObj_t& 		confObj)
	 noexcept;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryShear_t(
		const egd_applyBoundaryShear_t&)
	 noexcept;


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryShear_t&
	operator= (
		const egd_applyBoundaryShear_t&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_applyBoundaryShear_t(
		egd_applyBoundaryShear_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryShear_t&
	operator= (
		egd_applyBoundaryShear_t&&)
	 noexcept;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_applyBoundaryShear_t()
 	 noexcept;


	/*
	 * ==========================
	 * Interface functions
	 */
public:
	using BaseImpl_t::apply;


	/**
	 * \brief	This function prints out a small
	 * 		 identification of *this.
	 *
	 * Will output that this is a pipe condition.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/*
	 * ==========================
	 * BC Hooks
	 *
	 * As described above, the hooks are implemented
	 * by first calling the base versions of them and
	 * then apply its own action.
	 *
	 * Note that a shear condition must be pressent
	 * regardless if it is applied or not.
	 */
protected:
	/*
	 * Temperature is not subject to the shear boundary,
	 * so it is left allown.
	 */
	using BaseImpl_t::hook_handleTemperature;


	/**
	 * \brief	This function is called to handle the
	 * 		 case of a VelX property.
	 *
	 * Will call the implementation of the pipe class.
	 * And then, if gProp is defined on the staggered
	 * velocity X grid if will apply the internal
	 * condition.
	 *
	 * Note that in either case, the passed condition
	 * needs to have an internal condition.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleVelX(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 override;


	/**
	 * \brief	This function is called to handle the case of
	 * 		 a VelY property.
	 *
	 * This function first applies the condition of the pipe condition.
	 * It will then apply the inner condition iff gProp is defined
	 * on the VY nodeal points.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleVelY(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 override;


	/*
	 * ==========================
	 * LOGIC Hooks
	 *
	 * The logic hooks are hooks that allows
	 * to influence the behaviour of the base
	 * from a deriving class. They have more
	 * a logical touch than implementing of
	 * the boundary.
	 */
protected:
	/**
	 * \brief	This function returns true if *this can
	 * 		 handle the passed condition.
	 *
	 * This function tests if the condition is such as it
	 * is outlined above. It will test the geomrtery, since
	 * only constant geometries are currently supported.
	 *
	 * \param  cond		The condition to test.
	 * \param  gridGeo	The grid geometry.
	 */
	virtual
	bool
	canHandleThat(
		const BoundCond_t& 	cond,
		const GridGeometry_t& 	gridGeo)
	 const
	 override;


	/*
	 * =============================
	 * S T A T I C     Functions
	 *
	 * Some of them are implemented by the base, but not all.
	 */
public:
	//These are functions that applies the pop boundary
	using BaseImpl_t::applyPipeTempBC;
	using BaseImpl_t::applyPipeVelXBC;
	using BaseImpl_t::applyPipeVelYBC;


	/**
	 * \brief	This function applies the internal shearing condition to the
	 * 		 x component of the velocity.
	 *
	 * Note that this function does only apply the inner shearing condition and
	 * no other boudnary condition is applied to the property. Also this function
	 * can only handle the VX grids.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	static
	void
	applyInnerShearingVelXBC(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp);


	/**
	 * \brief	This function applies the internal shearing condition to the
	 * 		 y component of the velocity.
	 *
	 * Note that this function does only apply the inner shearing condition and
	 * no other boudnary condition is applied to the property. Also this function
	 * can only handle the VY grids.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	static
	void
	applyInnerShearingVelYBC(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp);


	/*
	 * ===========================
	 * Private Members
	 */
private:
	/*
	 * This class does not have any memebers
	 */
}; //End: class(egd_applyBoundaryShear_t)

PGL_NS_END(egd)


