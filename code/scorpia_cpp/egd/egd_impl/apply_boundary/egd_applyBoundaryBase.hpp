#pragma once
/**
 * \brief	This file implements a generall base implementation for the boundary applier.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_util/egd_confObj.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include <egd_interfaces/egd_applyBoundary_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


/**
 * \class 	egd_applyBoundaryBase_t
 * \brief	This function applies a concrete base
 * 	 	 for implementing the boundary.
 *
 * This is a base implementation and all concrete implementation
 * should inherent from this class. It contains several hooks
 * and code that allows to select an appropriate implementation.
 *
 * For each property it contains a property handler hook. This
 * is the function that should be overwritten by the deriving
 * class. If the property is not there, then you have to implement
 * it. It also contains code to check if a condition can be
 * applied, meaning if teh function realy can handle it.
 */
class egd_applyBoundaryBase_t : public egd_applyBoundary_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using BaseInterface_t 	= egd_applyBoundary_i;			//!< This is the base interface.
	using BoundCond_t 	= BaseInterface_t::BoundCond_t;		//!< This is the class for encoding the boundary condition.
	using GridProperty_t 	= BaseInterface_t::GridProperty_t;	//!< This is the class that represents a grid property.
	using GridGeometry_t 	= BaseInterface_t::GridGeometry_t;	//!< This is the class that represents a grid geometry.

	using SideCondition_t 	= BoundCond_t::SideCondition_t;		//!< Condition at a single side.
	using PropIdx_t		= BoundCond_t::PropIdx_t;		//!< The property index


	/*
	 * =========================
	 * Constructors
	 *
	 * Only destructor is public.
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_applyBoundaryBase_t()
 	 noexcept;


 	/**
 	 * \brief	This constructor is requiered for
 	 * 		 the building process.
 	 *
 	 * This constructor calls the default one. Its
 	 * argument is ignored.
 	 *
 	 * \param  confObj	The configuration object.
 	 */
 	egd_applyBoundaryBase_t(
 		const egd_confObj_t& 	confObj)
 	 noexcept;


	/*
	 * ==========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function applies the conditions cond
	 * 		 to the grid property gProp.
	 *
	 * This function will examine the passed property
	 * and forward the request to the appropriate hook.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	apply(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const
	 override;


	/**
	 * \brief	This function prints out a small
	 * 		 identification of *this.
	 *
	 * This is useful for debugging and outputing.
	 */
	virtual
	std::string
	print()
	 const
	 override
	 = 0;


	/*
	 * ==========================
	 * BC Hooks
	 *
	 * Here are the hooks that are connected to
	 * the implementation of the applying of BC.
	 * They are implemented, but the implentation
	 * throws if they are called.
	 *
	 * Note that the function must accept the
	 * properties in a grid agnostic way.
	 *
	 * You have to seperate them from the logic hooks.
	 */
protected:
	/**
	 * \brief	This function is called to handle the case of
	 * 		 a temperature property.
	 *
	 * This function is called by the selector, after the tests are done
	 * and it was determined that the grid property is a temperature.
	 * This function must be able to handle all grids, if situations
	 * occure that can not be handle an exception should be raised.
	 *
	 * Note this function has a default impelemntation that thorws.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleTemperature(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const;


	/**
	 * \brief	This function is called to handle the case of
	 * 		 a VelX property.
	 *
	 * This function is called by the selector, after the tests are done
	 * and it was determined that the grid property is a VelY
	 * This function must be able to handle all grids, if situations
	 * occure that can not be handle an exception should be raised.
	 *
	 * Note this function has a default impelemntation that thorws.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleVelX(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const;


	/**
	 * \brief	This function is called to handle the case of
	 * 		 a VelY property.
	 *
	 * This function is called by the selector, after the tests are done
	 * and it was determined that the grid property is a VelY.
	 * This function must be able to handle all grids, if situations
	 * occure that can not be handle an exception should be raised.
	 *
	 * Note this function has a default impelemntation that thorws.
	 *
	 * \param  cond		The condition that are in place.
	 * \param  gridGeo	The grid geometry.
	 * \param  gProp	The grid that should be modified.
	 */
	virtual
	void
	hook_handleVelY(
		const BoundCond_t& 		cond,
		const GridGeometry_t& 		gridGeo,
		GridProperty_t* const 		gProp)
	 const;


	/*
	 * ==========================
	 * LOGIC Hooks
	 *
	 * The logic hooks are hooks that allows
	 * to influence the behaviour of the base
	 * from a deriving class. They have more
	 * a logical touch than implementing of
	 * the boundary.
	 */
protected:
	/**
	 * \brief	This function returns true if *this can
	 * 		 handle the passed condition.
	 *
	 * This function is used to check if the coincrete
	 * implementation of *this is able to handle a set of
	 * conditions.
	 * Note this function does not throw if this is not the
	 * case only if an error in teh condition itself is detected.
	 * The geometry can also be used to restrict the possibilities further.
	 *
	 * \param  cond		The condition to test.
	 * \param  gridGeo	The grid geometry.
	 */
	virtual
	bool
	canHandleThat(
		const BoundCond_t& 	cond,
		const GridGeometry_t& 	gridGeo)
	 const
	 = 0;









	/*
	 * ===========================
	 * Protected constructors.
	 */
protected:
	/**
	 * \brief	Default constructor.
	 */
	egd_applyBoundaryBase_t()
	 noexcept;


	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryBase_t(
		const egd_applyBoundaryBase_t&)
	 noexcept;


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryBase_t&
	operator= (
		const egd_applyBoundaryBase_t&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_applyBoundaryBase_t(
		egd_applyBoundaryBase_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_applyBoundaryBase_t&
	operator= (
		egd_applyBoundaryBase_t&&)
	 noexcept;


	/*
	 * ===========================
	 * Private Members
	 */
private:
	/*
	 * This class does not have any memebers
	 */
}; //End: class(egd_applyBoundaryBase_t)

PGL_NS_END(egd)


