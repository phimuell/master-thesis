# Info
This folder contains the implementation of the code that will apply a boundary condition to a property.
The code is split in different branches, each for one type of boundary conditions.

## Base Implementation
In this folder a base implementation is provided.
This implementation extends the interface and provides some
helper functionality, such that adding new boundary types is easy.

## Concrete Implementation
Here the concrete implementation.

### NULL
This is the NULL implementation.
This implementation can not handle a single condition,
meaning that all functions throws, except the print
function and the can handle function.
It is present just to have an implementation.

### PIPE_COND
This are the conditions that are present in the original project.
The reason why they are named "pipe condition" is lost.

### SHEAR_COND
This is a condition that is used for the shear experiment.
It is the same as the PIPE condition, but in addition has an internal boundary.
That is diagonal in the middle.
Note that this implementation handles the CC and V{X,Y} grid different.

### Internal Condition
Since the internal conditions are implemented in a special way, it is not needed
to apply them.



