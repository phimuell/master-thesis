#pragma once
/**
 * \brief	This is a test object for the standard mapper.
 *
 * It basically bypasses everything to the underlying implementation.
 * But it is possible to inject a compute plan from the outside.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_GridToMarkerInterpol_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include "./egd_GridToMarkInterpol_sgComputePlan.hpp"
#include "./egd_GridToMarkInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_gridToMarkersTestImpl_t
 * \brief	Class to test the standard implementation.
 *
 * This class basically bypasses everything to the parrent class.
 * But it allows that the compute job is injected into this.
 *
 * This class allows to select from which class it should derive
 *
 * \tparam  BaseClass_t		The base of the implementation.
 */
template<
	class 	BaseClass_t
>
class egd_gridToMarkersTestImpl_t : public BaseClass_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t 	= BaseClass_t;
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using PropIdx_t 		= GridContainer_t::PropIdx_t;	//!< This is the property index type.
	using TempSolverResult_t 	= egd_tempSolverResult_t;	//!< This is the result container for the temperature solver.
	using MechSolverResult_t 	= egd_mechSolverResult_t;	//!< This is the result container for the mechanical solver.
	using InterpolState_t 		= egd_interpolState_t;		//!< This is the interpolation state.

	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;	//!< Type for a property of a marker.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;	//!< Type for a porperty of the grid.
	using GridGeometry_t 		= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.


	/**
	 * \brief	This is the Compute plan.
	 * \typedef 	ComputePlan_t
	 *
	 * The compute plane describes which marker property should be updated
	 * and how this should be done.
	 */
	using ComputePlan_t 		= egd_sgComputePlan_t;
	using ComputeJob_t 		= ComputePlan_t::ComputeJob_t;		//!< This is a single compute job.
	using DiffConstant_t 		= ComputePlan_t::DiffConstant_t;	//!< This is the diffusion constant.


	/*
	 * ======================
	 * Public Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * One has to move in the compute plan.
	 *
	 * \param  plan		The compute plan that is moved into *this.
	 */
	egd_gridToMarkersTestImpl_t(
		ComputePlan_t&& 	plan)
	 :
	  Base_t(),
	  m_plan(std::move(plan))
	{};


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	~egd_gridToMarkersTestImpl_t()
	 = default;



	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted
	 */
	egd_gridToMarkersTestImpl_t(
		const egd_gridToMarkersTestImpl_t&)
	 = delete;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is deleted
	 */
	egd_gridToMarkersTestImpl_t&
	operator= (
		const egd_gridToMarkersTestImpl_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is deleted.
	 */
	egd_gridToMarkersTestImpl_t(
		egd_gridToMarkersTestImpl_t&&)
	 = delete;


	/**
	 * \brief	Move assigment.
	 *
	 * Is deleted
	 */
	egd_gridToMarkersTestImpl_t&
	operator= (
		egd_gridToMarkersTestImpl_t&&)
	 = delete;


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	std::string
	print()
	 const
	 override
	{
		return std::string("Test implementation of grid to marker mapper.");
	};


	/*
	 * \brief	Thsi function allows to execute the job of *this.
	 *
	 * Note that this function basically calls the execute plan function.
	 * Note that no postprocessing is done, since the arguemnts are not
	 * aviable.
	 *
	 * \param  gridCont 	The grid container.
	 * \param  mColl	The marker collection.
	 */
	virtual
	void
	testBase(
		const GridContainer_t& 		gridCont,
		MarkerCollection_t* const 	mColl)
	 final
	{
		/*
		 * Now we execute the compute plan
		 */
		this->Base_t::execurePlan(
				m_plan,
				gridCont,
				mColl);
		return;
	};


	/*
	 * =========================
	 * Status function
	 *
	 * These functions are specific to the concrete function.
	 */
public:

	/*
	 * ======================
	 * Hooks
	 *
	 * These are the hooks that are implemented.
	 */
protected:
	/**
	 * \brief	This function returns a compute plan.
	 *
	 * This function will return, by a move, the computre plan
	 * of *this, if non empty.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	ComputePlan_t
	hook_makeComputePlan(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override
	{
		if(m_plan.hasJobs() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The test imple does not have jobs.");
		};

		return std::move(m_plan);	//We have to move
		(void)mColl;
		(void)grid;
		(void)mSol;
		(void)tSol;
		(void)thisDt;
		(void)tStepIdx;
		(void)currTime;
	};


	/**
	 * \brief	This function is called before absolute
	 * 		 interpolation is performed.
	 *
	 * This function outputs an information and will call the
	 * default.
	 *
	 * \param  compJob	This is the compute job, that is about to be executed.
	 * \param  grid		This is the grid container object.
	 * \param  mColl	This is the marker collection.
	 */
	virtual
	bool
	hook_inspectAbsInterpol(
		const ComputeJob_t& 		compJob,
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl)
	 override
	{
		std::cerr
			<< "DEBUG: "
			<< "Perform absolute interpolation."
			<< std::endl;

		return this->Base_t::hook_inspectAbsInterpol(compJob, grid, mColl);
	};


	/**
	 * \brief	This function is called before pure change
	 * 		 interpolation is performed.
	 *
	 * This function will output an information and call the default.
	 *
	 * \param  compJob	This is the compute job, that is about to be executed.
	 * \param  grid		This is the grid container object.
	 * \param  mColl	This is the marker collection.
	 */
	virtual
	bool
	hook_inspectPureChangeInterpol(
		const ComputeJob_t& 		compJob,
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl)
	 override
	{
		std::cerr
			<< "DEBUG: "
			<< "Perform pure change interpolation."
			<< std::endl;

		return this->Base_t::hook_inspectPureChangeInterpol(compJob, grid, mColl);
	};


	/**
	 * \brief	This function is called before subgrid
	 * 		 diffusion interpolation is performed.
	 *
	 * \param  compJob	This is the compute job, that is about to be executed.
	 * \param  grid		This is the grid container object.
	 * \param  mColl	This is the marker collection.
	 */
	virtual
	bool
	hook_inspectSubgridDiffInterpol(
		const ComputeJob_t& 		compJob,
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl)
	 override
	{
		std::cerr
			<< "DEBUG: "
			<< "Perform subgrid diffusion interpolation."
			<< std::endl;

		return Base_t::hook_inspectSubgridDiffInterpol(compJob, grid, mColl);
	};



	/*
	 * ========================
	 * Dedicated Plan Functions
	 *
	 * These functions are provided to ease the composing
	 * of complicated scenarios.
	 */
protected:
	/**
	 * \brief	This function returns the scalling constant for the
	 * 		 temperature subgrid diffusion.
	 *
	 * This is a dimensionless scaling constant that allows to adjust the
	 * strength of the diffusion. It has to be in the range [0, 1].
	 * It is called by the prot_doTempInterpol() function
	 */
	virtual
	Numeric_t
	hook_getTempDiffScaling()
	 const
	 override
	{
		throw PGL_EXCEPT_illMethod("Called a forbidden method.");
	};




	/*
	 * ========================
	 * Access functions
	 */
public:

	/*
	 * ========================
	 * Execution Functions
	 *
	 * These function perform the real task.
	 * There is one driver that performs a task
	 * and for each of the three interpolation
	 * task a dedicated fucntion.
	 */
protected:

	/*
	 * ===========================
	 * Private members
	 */
private:
	ComputePlan_t 		m_plan;		//This is the compute plan.
}; //End class(egd_gridToMarkersTestImpl_t)


PGL_NS_END(egd)


