# Info
This folder contains code, that is able to map the grid
properties back to the markers.

There is a base class that basically contains all the
code that is needed to do the operations. The whole
class is implemented by hooks, and the base does not
specify a default. This means that concrete classes
must generate the job list.
Note that all job generating functions, should be
implemented in the base, such that they can be used
by all sub classes, implementing it once.

## Implementations
Here is a list of the provided implementations.
Which interpolation method is used is determined by
the configuration file. For this the key "JobType"
is read in.

### STANDARD
This is the one for the original. It supports only
temperature.

### VELO
This is a version that only handles velocity. It will
add _all_ velocities that it finds in the mapping
to the compute job.



