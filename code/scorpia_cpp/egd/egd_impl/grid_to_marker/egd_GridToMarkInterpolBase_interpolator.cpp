/**
 * \brief	This function contains the driving code for the ionterpolation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_GridToMarkInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


void
egd_gridToMarkersBase_t::mapBackToMarkers(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;
	const GridGeometry_t& gridGeo = grid.getGeometry();

	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set.");
	};
	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not constant.");
	};

	/*
	 * We clear the interpolation state
	 */
	this->m_interpolState.clear();


	/*
	 * First of all, we will create a compute plan.
	 */
	const ComputePlan_t compPlan = this->hook_makeComputePlan(
			mColl, grid,
			mSol, tSol,
			thisDt, tStepIdx, currTime);
		pgl_assert(compPlan.isValid(),
			   compPlan.hasJobs() );

	/*
	 * Now we execute the compute plan
	 */
	this->execurePlan(
		compPlan,
		grid,
		&mColl);

	/*
	 * Invalidate and free the interpolation state
	 */
	m_interpolState.clear();


	/*
	 * Now we do the post processing
	 */
	this->hook_doPostProcess(
		compPlan,			//The plan
		&mColl, grid,			//The information
		mSol, tSol,			//Solutions
		thisDt, tStepIdx, currTime);	//Timestepping informations

	// To be sure
	m_interpolState.clear();

	return;
}; //End driver.

PGL_NS_END(egd)


