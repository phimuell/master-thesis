/**
 * \brief	This file contains the implementation of the velocity job generation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_GridToMarkInterpolBase.hpp"
#include "./egd_GridToMarkInterpol_sgComputePlan.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD



PGL_NS_START(egd)

void
egd_gridToMarkersBase_t::prot_makeVelJob(
	ComputePlan_t* const 		compPlan_,
	const MarkerCollection_t& 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime,
	const eJobType 			jType,
	const PropIdx_t 		pIdx)
 const
{
	if(compPlan_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("Passed the nullptr as compute plan to the function.");
	};
	ComputePlan_t& compPlan = *compPlan_;	//Alias of compute plan.

	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(pIdx.isVelocity() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index, " + pIdx + ", is not a velocity.");
	};
	if(mColl.hasProperty(pIdx.getRepresentant()) == false)
	{
		throw PGL_EXCEPT_InvArg("The marker collection does not have a property for " + pIdx
				+ ", checked by searching for the representant " + pIdx.getRepresentant());
	};

	//These are the properties that we need
	const PropIdx_t markerProp = pIdx.getRepresentant();	//This is the target property that we update
	const PropIdx_t weightMod  = PropIdx_t::Density();	//This is the property that we use to modify the marker weights.
	DiffConstant_t  diffConst;				//This is the diffusion constant, currently diabled.

	//We call the diff constant function, regardless if we need it
	//or not, the side effect is that we also check the validity
	//of the component.
	const Numeric_t diffScalingValue = this->hook_getVelDiffScaling(pIdx);
		pgl_assert(pgl::isValidFloat(diffScalingValue),
			   0.0 <= diffScalingValue, diffScalingValue <= 1.0);

	switch(jType)
	{
	  case eJobType::Abs:
	  {
		/*
		 * Handling the ABS case
		 * We can just load the velocity from the solution.
		 */

		const auto& velField = mSol.getSolField(pIdx);	//load the solution field, which is also the new field.

		//Now create the job object
		ComputeJob_t job(
			markerProp, markerProp, 	//This will disable conservativive interpolation
			std::move(diffConst),		//Will disable subgrid diffusion
			std::cref(velField),		//New field to imprint on marker, not owning
			nullptr, 			//No old field
			true);				//Enable absolute interpolation
			pgl_assert(job.isValid()              == true ,
				   job.doSubGridDiffusion()   == false,
				   job.doAbsInterpolation()   == true ,
				   job.doPureChangeInterpol() == false );

		//Insert into joblist
		compPlan.addJob(std::move(job));

	  //End case(Abs):
	  };
	  break;

	  case eJobType::Change:
	  {
		/*
		 * Handling the CHANGE case
		 *
		 * In this case, teh grid must have the property.
		 * Also we must make the change woer self.
		 */

		if(grid.hasProperty(pIdx) == false)
		{
			throw PGL_EXCEPT_InvArg("No change interpolation possible!"
					" The grid container does not have the property " + pIdx);
		};

		//load the veelocities
		const auto& newVelField = mSol.getSolField(pIdx);	//Load the new filed
		const auto& oldVelField = grid.getProperty(pIdx);	//Load the old field
			if(newVelField.checkOnSameGrid(oldVelField) == false)
			{
				throw PGL_EXCEPT_LOGIC("No change interpolation possible!"
						" Old and new field on different grids."
						" Old filed on " + oldVelField.getType()
						+ "; new field on " + newVelField.getType());
			};

		//
		//Create the differences
		//We first have to make a property with with the new field.
		std::unique_ptr<GridProperty_t> velDiff = ::std::make_unique<GridProperty_t>(newVelField);

		//now comes the differences
		velDiff->maxView() -= oldVelField.maxView();


		//Now create the job object
		ComputeJob_t job(
			markerProp, markerProp, 	//This will disable conservativive interpolation
			std::move(diffConst),		//Will disable subgrid diffusion
			velDiff, 			//What we interpolate, difference, owning
			nullptr, 			//No old temp field
			false);				//Enable absolute interpolation
			pgl_assert(job.isValid()              == true ,
				   job.doSubGridDiffusion()   == false,
				   job.doAbsInterpolation()   == false,
				   job.doPureChangeInterpol() == true  );

		//Insert into joblist
		compPlan.addJob(std::move(job));

	  //End case(Chnage):
	  };
	  break;

	  case eJobType::SubGrid:
	  {

		/*
		 * Here we handle the special case of subgrid diffusion.
		 */

		  //We must first generate the velocity change field.
		if(grid.hasProperty(pIdx) == false)
		{
			throw PGL_EXCEPT_InvArg("No change interpolation possible!"
					" The grid container does not have the property " + pIdx);
		};

		//load the veelocities
		const auto& newVelField = mSol.getSolField(pIdx);	//Load the new filed
		const auto& oldVelField = grid.getProperty(pIdx);	//Load the old field
			if(newVelField.checkOnSameGrid(oldVelField) == false)
			{
				throw PGL_EXCEPT_LOGIC("No change interpolation possible!"
						" Old and new field on different grids."
						" Old filed on " + oldVelField.getType()
						+ "; new field on " + newVelField.getType());
			};

		//
		//Create the differences
		//We first have to make a property with with the new field.
		std::unique_ptr<GridProperty_t> velDiff = ::std::make_unique<GridProperty_t>(newVelField);

		//now comes the differences
		velDiff->maxView() -= oldVelField.maxView();


		const auto& gridGeo = grid.getGeometry();	//The grid geometry

		if(gridGeo.isGridSet() == false)		//Test the grid
		{
			throw PGL_EXCEPT_InvArg("Grid is not set.");
		};
		if(gridGeo.isConstantGrid() == false)
		{
			throw PGL_EXCEPT_InvArg("Grid must be constant.");
		};

		const Numeric_t dx  = gridGeo.getXSpacing();
		const Numeric_t dy  = gridGeo.getYSpacing();

		const Numeric_t d   = diffScalingValue;
		pgl_assert(::pgl::isValidFloat(     d), 0.0 <= d, d <= 1.0);
		pgl_assert(::pgl::isValidFloat(thisDt), thisDt > 0.0);


		  /*
		   * Now we compute the diffusion time scale that is given as
		   * 	t_M(m) := \frac{\rho)_m}{\eta_{m} \cdot (\frac{2}{\Delta X^2} + \frac{2}{\Delta Y^2}}
		   * Note that there is a critical difference in the above when
		   * compared to (14.35). There the visco-plastic viscosity is used, this
		   * is a grid property that is pulled back with a special scheme (13.59).
		   * We do not have this parameter here, so we use the viscoity that is
		   * defined on the marker anyway.
		   *
		   * The diffusion constant is then given as:
		   * 	diff := -d \cdot \frac{\Delta t}{t_M(m)}
		   *
		   * Where d is a scaling parameter in the range [0, 1] and $\Delta t$ is the
		   * time step that is/will be performed.
		   */

		//Load the properties we need of the markers
		const auto& RHO = mColl.getMarkerProperty(PropIdx_t::Density()   );
		const auto& ETA = mColl.getMarkerProperty(PropIdx_t::Viscosity() );

		//We now compute the scalar factor in the above
		const Numeric_t gridFactor = 2.0 / (dx * dx) + 2.0 / (dy * dy);

		//We now compute the diffusion constant
		diffConst = ((-d * thisDt * gridFactor) * ETA) / RHO;
			pgl_assert(diffConst.size() == mColl.nMarkers(),
		           	   diffConst.isFinite().all());

		//Now create the job object
		ComputeJob_t job(
				  markerProp, weightMod, 		//This will RHOCP for conservative interpol
				  std::move(diffConst),			//move sbbgrid constant into place
				  velDiff, 				//Changes to interpolate, acuire
				  &oldVelField,				//The old velocity field.
				  false);				//disable absolute interpolation.
			pgl_assert(job.isValid()              == true ,
			           job.doSubGridDiffusion()   == true ,
				   job.doAbsInterpolation()   == false,
				   job.doPureChangeInterpol() == false );

		/*
		 * Add the job to the job list
		 */
		compPlan.addJob(std::move(job));
	  //End case: subgrid
	  };
	  break;

	  default:
	  {
	  	throw PGL_EXCEPT_InvArg("The passed job enum is unkown.");
	  };
	}; //End switch(jType):

	/*
	 * We have nothing to do
	 */

	return;
	(void)tSol;
	(void)currTime;
	(void)tStepIdx;
}; //End: create vel job


Numeric_t
egd_gridToMarkersBase_t::hook_getVelDiffScaling(
	const PropIdx_t 		pIdx)
 const
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(pIdx.isVelocity() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index, " + pIdx + ", is not a velocity.");
	};

	if(PropIdx_t::VelX() == pIdx)
	{
		return 1.0;
	}
	else if(PropIdx_t::VelY() == pIdx)
	{
		return 1.0;
	}
	else
	{
		throw PGL_EXCEPT_LOGIC("The velocity " + pIdx + " is unkwon.");
	};
};//End: get Velocity scalling factor

PGL_NS_END(egd)


