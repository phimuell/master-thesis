/**
 * \brief	This file contains the function that will create a job for interpolating the temperature.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_GridToMarkInterpolBase.hpp"
#include "./egd_GridToMarkInterpol_sgComputePlan.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD



PGL_NS_START(egd)

void
egd_gridToMarkersBase_t::prot_makeTempJob(
	ComputePlan_t* const 		compPlan_,
	const MarkerCollection_t& 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime,
	const eJobType 			jType)
 const
{
	if(compPlan_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("Passed the nullptr as compute plan to the function.");
	};
	ComputePlan_t& compPlan = *compPlan_;	//Alias of compute plan.

	//These are the properties that we need
	const PropIdx_t markerProp = PropIdx_t::Temperature();		//This is the target property that we update
	const PropIdx_t weightMod  = PropIdx_t::RhoCp();		//This is the property that we use to modify the marker weights.
	DiffConstant_t  diffConst;					//This is the diffusion constant, currently diabled.

	switch(jType)
	{
	  case eJobType::Abs:
	  {
		/*
		 * Handling the ABS case
		 */

		//load the temperatures
		const auto& TEMP_DIFF = tSol.getTempDiff();
		const auto& TEMP      = grid.getProperty(PropIdx_t::Temperature());	//By convention, this one is correct
			if(TEMP_DIFF.getType() != TEMP.getType())
			{
				throw PGL_EXCEPT_LOGIC("Did not found the correct grid.");
			};

		//We first have to create the new property, initailize it with the temperature
		std::unique_ptr<GridProperty_t> TEMP_ABS = ::std::make_unique<GridProperty_t>(TEMP);

		//Now transform *this into an absolute temperature
		TEMP_ABS->maxView() += TEMP_DIFF.maxView();

		//Now create the job object
		ComputeJob_t job(
			markerProp, markerProp, 	//This will disable conservativive interpolation
			std::move(diffConst),		//Will disable subgrid diffusion
			TEMP_ABS, 			//What we interpolate
			nullptr, 			//No old field
			true);				//Enable absolute interpolation
			pgl_assert(job.isValid()              == true ,
				   job.doSubGridDiffusion()   == false,
				   job.doAbsInterpolation()   == true ,
				   job.doPureChangeInterpol() == false );

		//Insert into joblist
		compPlan.addJob(std::move(job));

	  //End case(Abs):
	  };
	  break;

	  case eJobType::Change:
	  {
		/*
		 * Handling the CHANGE case
		 */

		//load the temperatures
		const auto& TEMP_DIFF = tSol.getTempDiff();
		const auto& TEMP      = grid.getProperty(PropIdx_t::Temperature());	//By convention, this one is correct
			if(TEMP_DIFF.getType() != TEMP.getType())
			{
				throw PGL_EXCEPT_LOGIC("Did not found the correct grid.");
			};

		//Now create the job object
		ComputeJob_t job(
			markerProp, markerProp, 	//This will disable conservativive interpolation
			std::move(diffConst),		//Will disable subgrid diffusion
			std::cref(TEMP_DIFF), 		//What we interpolate, difference, only reference
			&TEMP, 				//Temperature old field
			false);				//Enable absolute interpolation
			pgl_assert(job.isValid()              == true ,
				   job.doSubGridDiffusion()   == false,
				   job.doAbsInterpolation()   == false,
				   job.doPureChangeInterpol() == true  );

		//Insert into joblist
		compPlan.addJob(std::move(job));

	  //End case(Chnage):
	  };
	  break;

	  case eJobType::SubGrid:
	  {
		  /*
		   * Here we handle the special case of subgrid diffusion.
		   */

		  //Load and chenck the parameters
		  const auto& RHOCP = mColl.cgetMarkerProperty(PropIdx_t::RhoCp()        );
		  const auto& K     = mColl.cgetMarkerProperty(PropIdx_t::ThermConduct() );
		  pgl_assert(RHOCP.isFinite().all(),
				  K.isFinite().all() );
		  //load the temperatures
		  const auto& TEMP_DIFF = tSol.getTempDiff();
		  const auto& TEMP      = grid.getProperty(PropIdx_t::Temperature());	//By convention, this one is correct
		  if(TEMP_DIFF.getType() != TEMP.getType())
		  {
			  throw PGL_EXCEPT_LOGIC("Did not found the correct grid.");
		  };

		  const auto& gridGeo = grid.getGeometry();	//The grid geometry

		  if(gridGeo.isGridSet() == false)		//Test the grid
		  {
			  throw PGL_EXCEPT_InvArg("Grid is not set.");
		  };
		  if(gridGeo.isConstantGrid() == false)
		  {
			  throw PGL_EXCEPT_InvArg("Grid must be constant.");
		  };

		  const Numeric_t dx  = gridGeo.getXSpacing();
		  const Numeric_t dy  = gridGeo.getYSpacing();

		  const Numeric_t d   = this->hook_getTempDiffScaling();
		  pgl_assert(::pgl::isValidFloat(     d), 0.0 <= d, d <= 1.0);
		  pgl_assert(::pgl::isValidFloat(thisDt), thisDt > 0.0);

		  /*
		   * Now we compute the diffusion time scale that is given as
		   * 	t_m := \frac{\rho C_p)_m}{k_m \cdot (\frac{2}{\Delta X^2} + \frac{2}{\Delta Y^2}}
		   *
		   * The diffusion constant is then given as:
		   * 	diff := -d \cdot \frac{\Delta t}{t_m}
		   *
		   * Where d is a scaling parameter in the range [0, 1].
		   */

		  //We now compute the scalar factor in the above
		  const Numeric_t gridFactor = 2.0 / (dx * dx) + 2.0 / (dy * dy);

		  //We now compute the diffusion constant
		  diffConst = ((-d * thisDt * gridFactor) * K) / RHOCP;
		  pgl_assert(diffConst.size() == mColl.nMarkers(),
				  diffConst.isFinite().all());


		  //Now create the job object
		  ComputeJob_t job(
				  markerProp, weightMod, 		//This will RHOCP for conservative interpol
				  std::move(diffConst),		//move sbbgrid constant into place
				  std::cref(TEMP_DIFF), 		//Interpolate the temp change, do not aquire
				  &TEMP,				//The old temperature field
				  false);				//disable absolute interpolation
		  pgl_assert(job.isValid()              == true ,
			     job.doSubGridDiffusion()   == true ,
			     job.doAbsInterpolation()   == false,
			     job.doPureChangeInterpol() == false );

		  /*
		   * Add the job to the job list
		   */
		  compPlan.addJob(std::move(job));
	  //End case: subgrid
	  };
	  break;

	  default:
	  {
	  	throw PGL_EXCEPT_InvArg("The passed job enum is unkown.");
	  };
	}; //End switch(jType):

	/*
	 * We have nothing to do
	 */

	return;
	(void)mSol;
	(void)currTime;
	(void)tStepIdx;
}; //End: create temperature job


Numeric_t
egd_gridToMarkersBase_t::hook_getTempDiffScaling()
 const
{
	return 1.0;
};

PGL_NS_END(egd)

