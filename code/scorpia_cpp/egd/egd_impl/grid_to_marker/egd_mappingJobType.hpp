#pragma once
/**
 * \brief	This file contains an enum and some helper function that allows to
 * 		 specify how the mapping works.
 */

//Include the confg file
#include <egd_core.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <string>


PGL_NS_START(egd)


/**
 * \brief	This enum is used to encode which kind of mapping should be done.
 * \enum 	eJobType
 *
 * Note that this was previously an enum that was defined inside the base of the
 * grid to marker mapper. Now it is defined outside, and it was also made into
 * a scoped enum.
 */
enum class eJobType
{
	Abs,		//!< Absolute interpolation.
	Change,		//!< Change ineterpolation.
	SubGrid,	//!< Subgrid interpolation.
}; //End enum(eJobType)



/**
 * \brief	Returns true if jType is a valid argument.
 */
inline
bool
isValidJobType(
	const eJobType& 	jType)
{
	switch(jType)
	{
	  case eJobType::Abs:
	  case eJobType::Change:
	  case eJobType::SubGrid:
	  	return true;

	  default:
	  	pgl_assert(false && "Detected a wrong mapping job.");
	  	return false;
	};//End switch(jType):
	return false;
};//End: is valid job type


/**
 * \brief	This function produces a textual representation
 * 		 of the job type.
 *
 * This function does basically stringify the name.
 *
 * \param  jType	This is the job type.
 */
std::string
printJobType(
	const eJobType& 	jType);


/**
 * \brief	This function will parse a string and
 * 		 transform it into a job enum.
 *
 * The function will do the processing in an case
 * insensitive fashion and also supports several
 * short hands.
 *
 * \param  s	The string to parse
 *
 * \throw	If the string was not recogniced.
 */
eJobType
parseJobType(
	const std::string& 	s);



/*
 * ======================
 * Convenience operator
 */
inline
std::string
operator+ (
	const std::string& 	s,
	const eJobType& 	jType)
{
	return (s + printJobType(jType));
};

inline
std::string
operator+ (
	const eJobType& 	jType,
	const std::string& 	s)
{
	return (printJobType(jType) + s);
};















PGL_NS_END(egd)


