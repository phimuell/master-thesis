/**
 * \brief	This file contains teh function that will executes jobs.
 *
 * This file contains the function for just one job or a plan.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_GridToMarkInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>


PGL_NS_START(egd)


void
egd_gridToMarkersBase_t::execurePlan(
	const ComputePlan_t& 		compPlan,
	const GridContainer_t& 		grid,
	MarkerCollection_t* const 	mColl_)
{
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;	//Alias

	//get the geometry
	const GridGeometry_t& gridGeo = grid.getGeometry();
		pgl_assert(gridGeo.isGridSet());

	//Reset the interpolation state
	m_interpolState.clear();


	/*
	 * Going through the job list and execute them
	 */
	for(const ComputeJob_t& job : compPlan)
	{
		//
		//First we test, if the interpolation state is on the correct grid
		if(job.getGridType() != this->m_interpolState.gType)
		{
			const bool noEmptyCell = egd_initInterpolation(
				mColl, gridGeo,		//Grid structure
				job.getGridType(),	//Target type
				&m_interpolState,	//Interpolation state
				true,			//Do Processing
				true);			//Do Disableing

			if(noEmptyCell == false)
			{
				throw PGL_EXCEPT_RUNTIME("Found an empty cell.");
			};
				pgl_assert(m_interpolState.isFinite(),
					   m_interpolState.isProcessed());
		}; //End if: update the interpolation state


		//
		//Now we execute the job
		this->executeJob(job, grid, &mColl);
	}; //End for(jobs): going through the job list

	//Reset the interpolation state
	m_interpolState.clear();

	//return
	return;
}; //End: executePlan


void
egd_gridToMarkersBase_t::executeJob(
	const ComputeJob_t& 		job,
	const GridContainer_t& 		grid,
	MarkerCollection_t* const 	mColl_)
{
	pgl_assert(mColl_ != nullptr);
	pgl_assert(job.isValid());
	pgl_assert(job.getGridType() == m_interpolState.gType);
	MarkerCollection_t& mColl = *mColl_;

	//Now we load the marker property that we have to update.
	MarkerProperty_t& mPropUpdate = mColl.getMarkerProperty(job.getMarkerProp());
		pgl_assert(mPropUpdate.size() > 0,
			   mPropUpdate.isFinite().all());

	/*
	 * Now we check if we have to perform subgrid diffusion or not
	 * Must be done before the absolute interpolation is performed.
	 */
	if(job.doSubGridDiffusion() == true)
	{
		//Inspect the job
		if(this->hook_inspectSubgridDiffInterpol(
				job, grid, mColl
			) == false)
		{
			throw PGL_EXCEPT_RUNTIME("The inspection function for a subgrid job returned false.");
		};

		//
		//We have to test if we have to adjust the conservative interpolation
		const MarkerProperty_t* const wUpdate =
			(job.hasWeightModifier() == true)
			? (&(mColl.cgetMarkerProperty(job.getWeightModifier())))
			: nullptr;

		//Unpack the job
		const GridProperty_t& oldField  = job.getOldField();
		const GridProperty_t& chaField  = job.getGridChange();
		const DiffConstant_t& diffConst = job.getDiffConstant();
			pgl_assert(diffConst.size() > 0,
				   diffConst.isFinite().all(),
				   diffConst.size() == mPropUpdate.size(),
				   oldField.getType() == m_interpolState.gType,
				   chaField.getType() == m_interpolState.gType,
				   ::egd::isValidGridType(chaField.getType())    );

		/*
		 * This is a very strange situation, but it is possible to perform
		 * absolute interpolation and subgrid diffusion. This is handled
		 * by setting the target property to zero and then performing
		 * subgrid diffusion.
		 */
		if(job.doAbsInterpolation() == true)
		{
			mPropUpdate.setZero();
				pgl_assert(mPropUpdate.size() == diffConst.size());
		}; //End if: absolute interpolation and subgrid diffusion.


		//Call the subgrid function
		this->prot_performSubgridDiff(&mPropUpdate,
				oldField, chaField,
				diffConst,
				wUpdate);
	//End if: do subgrid
	}
	else if(job.doAbsInterpolation() == true)
	{
		pgl_assert(job.doSubGridDiffusion() == false);

		//Inspect the job
		if(this->hook_inspectAbsInterpol(
				job, grid, mColl
			) == false)
		{
			throw PGL_EXCEPT_RUNTIME("The inspection function for a absolute interpolation returned false.");
		};

		//load everything
		const GridProperty_t& absField = job.getNewAbsField();

		//Call the interpolation function
		this->prot_performAbsInterpolation(&mPropUpdate, absField);

	//End if: absolute interpolation
	}
	else
	{
		//Inspect the job
		if(this->hook_inspectPureChangeInterpol(
				job, grid, mColl
			) == false)
		{
			throw PGL_EXCEPT_RUNTIME("The inspection function for a putre change job returned false.");
		};

		if(job.doPureChangeInterpol() == false)
		{
			throw PGL_EXCEPT_LOGIC("Expected a pure change job, but did not get one.");
		};

		//load everything
		const GridProperty_t& chaField = job.getGridChange();

		//Call the interpolation function
		this->prot_performChangeInterpol(&mPropUpdate, chaField);
	}; //End else: do change interpolation


	return;
}; //End: execute one job.



PGL_NS_END(egd)


