/**
 * \brief	This file contains some of the functions that delas with
 * 		 the interpolation from the grid to the marker.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_GridToMarkInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD



PGL_NS_START(egd)



void
egd_gridToMarkersBase_t::hook_doPostProcess(
	const ComputePlan_t& 		compPlan,
	MarkerCollection_t* const 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
 const
{
	pgl_assert(mColl != nullptr);
	pgl_assert(::pgl::isValidFloat(thisDt  ), thisDt > 0.0,
		   ::pgl::isValidFloat(currTime) 	       );
	pgl_assert(grid.getGeometry().isGridSet());

	return;
	(void)compPlan;
	(void)mColl;
	(void)grid;
	(void)mSol;
	(void)tSol;
	(void)thisDt;
	(void)tStepIdx;
	(void)currTime;
}; //End: dostprocess interpolation


bool
egd_gridToMarkersBase_t::hook_inspectAbsInterpol(
	const ComputeJob_t& 		compJob,
	const GridContainer_t& 		grid,
	const MarkerCollection_t& 	mColl)
{
	return true;
	(void)compJob;
	(void)grid;
	(void)mColl;
};


bool
egd_gridToMarkersBase_t::hook_inspectPureChangeInterpol(
	const ComputeJob_t& 		compJob,
	const GridContainer_t& 		grid,
	const MarkerCollection_t& 	mColl)
{
	return true;
	(void)compJob;
	(void)grid;
	(void)mColl;
};


bool
egd_gridToMarkersBase_t::hook_inspectSubgridDiffInterpol(
	const ComputeJob_t& 		compJob,
	const GridContainer_t& 		grid,
	const MarkerCollection_t& 	mColl)
{
	return true;
	(void)compJob;
	(void)grid;
	(void)mColl;
};


PGL_NS_END(egd)


