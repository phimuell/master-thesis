/**
 * \brief	This file contains the implementation of teh functions that are needed by the th
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_GridToMarkerInterpol_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include "./egd_GridToMarkInterpol_sgComputePlan.hpp"

#include "./egd_GridToMarkInterpol_standard.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


egd_gridToMarkersStandard_t::Numeric_t
egd_gridToMarkersStandard_t::hook_getTempDiffScaling()
 const
{
	return 1.0;
};


std::string
egd_gridToMarkersStandard_t::print()
 const
{
	return std::string("Standard (temp only) grid to marker interpolator");
};


egd_gridToMarkersStandard_t::ComputePlan_t
egd_gridToMarkersStandard_t::hook_makeComputePlan(
	const MarkerCollection_t& 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	//In the first step we will make an absolute job, otherwise a
	// subgrid diff job.
	pgl_assert(tStepIdx >= 0);
	const eJobType jType = (tStepIdx == 0)
				? eJobType::Abs
				: eJobType::SubGrid;

	//make an empty plan
	ComputePlan_t cPlan;

	//Make the temperature job
	this->prot_makeTempJob(
			&cPlan,
			mColl,
			grid,
			mSol,
			tSol,
			thisDt, tStepIdx, currTime,
			jType);
		pgl_assert(cPlan.isValid());

	return cPlan;
}; //End: make compute plan




/*
 * ======================
 * Constructors
 */
egd_gridToMarkersStandard_t::egd_gridToMarkersStandard_t(
	const egd_confObj_t& 	confObj)
 :
  Base_t(confObj)
{};

egd_gridToMarkersStandard_t::~egd_gridToMarkersStandard_t()
 = default;


egd_gridToMarkersStandard_t::egd_gridToMarkersStandard_t()
 = default;


egd_gridToMarkersStandard_t::egd_gridToMarkersStandard_t(
	const egd_gridToMarkersStandard_t&)
 = default;


egd_gridToMarkersStandard_t&
egd_gridToMarkersStandard_t::operator= (
	const egd_gridToMarkersStandard_t&)
 = default;


egd_gridToMarkersStandard_t::egd_gridToMarkersStandard_t(
	egd_gridToMarkersStandard_t&&)
 noexcept
 = default;


egd_gridToMarkersStandard_t&
egd_gridToMarkersStandard_t::operator= (
	egd_gridToMarkersStandard_t&&)
 = default;


PGL_NS_END(egd)


