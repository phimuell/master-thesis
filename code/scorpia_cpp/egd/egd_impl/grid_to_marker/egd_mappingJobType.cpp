/**
 * \brief	Code for the job enum.
 */

//Include the confg file
#include <egd_core.hpp>

#include "./egd_mappingJobType.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_string.hpp>


//Include the Eigen Headers


//Include STD
#include <string>


PGL_NS_START(egd)


std::string
printJobType(
	const eJobType& 	jType)
{
	if(isValidJobType(jType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed job type was invalid.");
	};
#	define CASE(x) case eJobType:: x : return std::string(#x); break

	switch(jType)
	{
	  CASE(Abs);
	  CASE(Change);
	  CASE(SubGrid);
	}; //End switch(jType):

	throw PGL_EXCEPT_RUNTIME("Reached unreachable code.");
	return std::string("");
}; //ENd printing


eJobType
parseJobType(
	const std::string& 	s_)
{
	if(s_.empty() )
	{
		throw PGL_EXCEPT_InvArg("The passed string is empty.");
	};

	const auto s = ::pgl::pgl_strToLower(s_);

	if(s == "s" || s == "subgrid" || s == "sg" || s == "sub")
	{
		return eJobType::SubGrid;
	}
	else if(s == "a" || s == "abs" || s == "absolute")
	{
		return eJobType::Abs;
	}
	else if(s == "p" || s == "c" || s == "ch" || s == "change")
	{
		return eJobType::Change;
	}

	throw PGL_EXCEPT_InvArg("Could not parse \"" + s_ + "\"");
	return static_cast<eJobType>(0);
};//End: parse jobType

















PGL_NS_END(egd)


