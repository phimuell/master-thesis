/**
 * \brief	This file contains some function to manipulate the behaviour of the plan.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_GridToMarkInterpol_sgComputePlan.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_list.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <functional>


PGL_NS_START(egd)

namespace internal
{
	/*
	 * This is ugly and we will burn in hell.
	 */
	static bool sgCompPlanReportDelete = false;
}; //End namespace(internal):



void
egd_sgComputeJob_t::printResourceInfo(
	const std::string& 		where,
	std::ostream& 			o)
 const
{
	//Print the where argument if given
	if(where.empty() == false)
	{
		o << where << "\n";
	}; //End if: non empty where argument

#define YESNO(x) ((x) ? std::string("Yes") : std::string("No"))

	//Mode of interpolation
	o
		<< "Subgrid:     " << YESNO(this->doSubGridDiffusion())   << "\n"
		<< "Absolut:     " << YESNO(this->doAbsInterpolation())   << "\n"
		<< "Pure change: " << YESNO(this->doPureChangeInterpol()) << "\n";

	//
	//Change field.
	if(m_gProp == nullptr)
	{
		o
			<< "m_gProp = nullptr" << "\n";
	}
	else
	{
		o
			<<         "m_gProp = " << m_gProp                           << "\n"	//Output address
			<< "\t" << "own:  "     << YESNO(m_ownProp) 		     << "\n"
			<< "\t" << "prop: "     << m_gProp->getPropIdx()             << "\n"	//Output property
			<< "\t" << "grid: "     << printGridType(m_gProp->getType()) << "\n";	//ouptut grid type
	}; //End else: print grid prop


	//
	//Weight modifier
	o
		<< "\t" << "Has weight modifier: ";

	if(this->hasWeightModifier() == true)
	{
		o
			<< "Yes => " << this->m_wUpdate.print();
	}
	else
	{
		o
			<< "No";
	};
	o
		<< "\n";




	//
	//Old filed
	if(m_oldField == nullptr)
	{
		o
			<< "m_oldField = nullptr" << "\n";
	}
	else
	{
		o
			<<         "m_oldField = " << m_oldField                           << "\n"	//Output address
			<< "\t" << "prop: "        << m_oldField->getPropIdx()             << "\n"	//Output property
			<< "\t" << "grid: "        << printGridType(m_oldField->getType()) << "\n";	//ouptut grid type
	}; //End else: print old field


	o << std::endl;	//Add a final new line

	return;
#undef YESNO
};//End print resource info



void
egd_sgComputeJob_t::BE_VERBOSE()
{
	if(internal::sgCompPlanReportDelete == true)
	{
		throw PGL_EXCEPT_LOGIC("Called the verbose function twice.");
	};

	internal::sgCompPlanReportDelete = true;

	return;
};//End: BE_VERBOSE


bool
egd_sgComputeJob_t::IS_VERBOSE()
{
	return internal::sgCompPlanReportDelete;
};


PGL_NS_END(egd)

