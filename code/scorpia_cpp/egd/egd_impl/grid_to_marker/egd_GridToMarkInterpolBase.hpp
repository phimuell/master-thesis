#pragma once
/**
 * \brief	This file contains the base grid to marker mapper.
 *
 * This is the file declares the base implementation.
 * From this implementation all other classes schould derive.
 *
 * However plan generation function should be implemented in
 * this class, thus violating the open close principle.
 * But it is logical to do that.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_GridToMarkerInterpol_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include "./egd_GridToMarkInterpol_sgComputePlan.hpp"
#include "./egd_mappingJobType.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


/**
 * \class 	egd_gridToMarkersBase_t
 * \brief	This class implements the base of the mapping process.
 *
 * This class provides different ways of mapping grid properties
 * to the markers. It is implemented by hooks, such that a concrete
 * class can be used to generate a plan.
 *
 * It supports absolute interpolation, change interpolation and
 * subgrid diffusion interpolation.
 * Note that functions to generate subgrid plans, should be
 * implemented in thsi class and not on deriving class.
 * This is done to keep everything together and allow accessing
 * them in a uniform way.
 *
 * Note that subgrid diffusion for velocity is handled differently,
 * than described in the book.
 *
 * ====================
 * Subgrid diffusion, explained by temperature
 * The whole process is quite complicated and is described in full
 * in Taras' book in chaptor 10.4, ehere only a small suumary is given.
 * that however explains the process, the equations can be found in the
 * book.
 *
 * The temperature solver computes the temperature differences, at the
 * nodal points, denoted as
 * 	DT_{i,j} := T_{i,j}^{+1} - T_{i,j}^{0}
 *
 * where
 * 	T_{i,j}^{+1} ~ Temperature at node (i,j) in the next time step, solver computes that.
 * 	T_{i,j}^{ 0} ~ Current temperature at node (i,j)
 *
 * Then we assume that we can split the temperature change in two parts,
 * as
 * 	DT_{i,j} = DT_{i,j}^{sg} + DT_{i,j}^{r}		(*)
 *
 * DT_{i,j}^{sg} is the temperature change that is caused by subgrid
 * diffusion and DT_{i,j}^{r} is the rest of the temperature change
 * that happened at _node_ (i,j).
 * We can not directly compute DT_{i,j}^{sg} at the node location.
 * But we can compute the DT_{m}^{sg} which is the subgrid diffusion
 * happens at the marker m, as
 * 	DT_{m}^{sg} := (T_{m;node}^{0} - T_{m}^{0}) * (1 - \Exp{A})
 * where
 * 	- T_{m;node}^{0} is the temperature of the surrounding 4 nodes
 * 	    is interpolated at location where marker m is located.
 * 	    Important is, that _standard_ interpolation is used, no conservative
 * 	    scheme.
 *	- T_{m}^{0} is the normal marker temperature.
 *	- A is an expression that depends on many things, see equation (10.16).
 *
 * DT_{ij}^{sg} is then optained by _conservative interpolating_ the DT_{m}^{sg}
 * values to the grid, using a conservative scheme.
 * This in turn allows us to calculate DT_{ij}^{r} by reorder equation (*)
 *
 * The new marker temperature can then be calculated as
 * 	T_{m}^{corr} := T_{m}^{0} + DT_{m}^{sg} + DT_{m}^{r}
 * where
 * 	- T_{m}^{0} is the current marker temperature
 * 	- DT_{m}^{sg} is the subgrid diffusion change at the marker m.
 * 	    Was previously calculated.
 * 	- DT_{m}^{r} is the rest diffusion at the marker m.
 */
class egd_gridToMarkersBase_t : public egd_GridToMarkers_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using PropIdx_t 		= GridContainer_t::PropIdx_t;	//!< This is the property index type.
	using TempSolverResult_t 	= egd_tempSolverResult_t;	//!< This is the result container for the temperature solver.
	using MechSolverResult_t 	= egd_mechSolverResult_t;	//!< This is the result container for the mechanical solver.
	using InterpolState_t 		= egd_interpolState_t;		//!< This is the interpolation state.

	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;	//!< Type for a property of a marker.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;	//!< Type for a porperty of the grid.
	using GridGeometry_t 		= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.


	/**
	 * \brief	This is the Compute plan.
	 * \typedef 	ComputePlan_t
	 *
	 * The compute plane describes which marker property should be updated
	 * and how this should be done.
	 */
	using ComputePlan_t 		= egd_sgComputePlan_t;
	using ComputeJob_t 		= ComputePlan_t::ComputeJob_t;		//!< This is a single compute job.
	using DiffConstant_t 		= ComputePlan_t::DiffConstant_t;	//!< This is the diffusion constant.

	using eJobType = ::egd::eJobType;	//Backwards compability.



	/*
	 * ======================
	 * Public Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function does nothing, since no building is
	 * needed.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_gridToMarkersBase_t(
		const egd_confObj_t& 		confObj);


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	~egd_gridToMarkersBase_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_gridToMarkersBase_t(
		const egd_gridToMarkersBase_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_gridToMarkersBase_t&
	operator= (
		const egd_gridToMarkersBase_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_gridToMarkersBase_t(
		egd_gridToMarkersBase_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted.
	 */
	egd_gridToMarkersBase_t&
	operator= (
		egd_gridToMarkersBase_t&&);


	/*
	 * ==========================
	 * Protected Constructor
	 */
public:
	/**
	 * \brief	This is the default constructor.
	 *
	 * This constructor will construct an invalid object.
	 * It is only used internaly.
	 */
	egd_gridToMarkersBase_t();


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function maps grid properties to markers.
	 *
	 * This function currently only maps the the temperature back to the
	 * markers. For that it uses subgrid diffusion.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	mapBackToMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 */
	std::string
	print()
	 const
	 override
	 = 0;


	/*
	 * =========================
	 * Status function
	 *
	 * These functions are specific to the concrete function.
	 */
public:

	/*
	 * ======================
	 * Hooks
	 *
	 * These are the hooks that are implemented.
	 */
protected:
	/**
	 * \brief	This function returns a compute plan.
	 *
	 * A compute plan describes what should be executed,
	 * This function computes one. It will then be executed.
	 *
	 * Note that implementations should implement a dedicated,
	 * protected function, such that other code can use them.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	ComputePlan_t
	hook_makeComputePlan(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 = 0;


	/**
	 * \brief	This function is called after all compute jobs were
	 * 		 executed, it is provided for further manipulation.
	 *
	 * This function is called after the interpolation state was cleared.
	 * It allows sub classes to postprocess the result.
	 *
	 * The default of this function does nothing. it is implemented in the
	 * cpp file.
	 *
	 * \param  compPlan	The compute plan that was used.
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	hook_doPostProcess(
		const ComputePlan_t& 		compPlan,
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 const;


	/**
	 * \brief	This function is called before absolute
	 * 		 interpolation is performed.
	 *
	 * This function allows sub classes to inspect the job.
	 * And adapt internal states.
	 *
	 * The default of this function does nothing.
	 *
	 * \param  compJob	This is the compute job, that is about to be executed.
	 * \param  grid		This is the grid container object.
	 * \param  mColl	This is the marker collection.
	 */
	virtual
	bool
	hook_inspectAbsInterpol(
		const ComputeJob_t& 		compJob,
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl);


	/**
	 * \brief	This function is called before pure change
	 * 		 interpolation is performed.
	 *
	 * This function allows sub classes to inspect the job.
	 * And adapt internal states.
	 *
	 * The default of this function does nothing.
	 *
	 * \param  compJob	This is the compute job, that is about to be executed.
	 * \param  grid		This is the grid container object.
	 * \param  mColl	This is the marker collection.
	 */
	virtual
	bool
	hook_inspectPureChangeInterpol(
		const ComputeJob_t& 		compJob,
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl);


	/**
	 * \brief	This function is called before subgrid
	 * 		 diffusion interpolation is performed.
	 *
	 * This function allows sub classes to inspect the job.
	 * And adapt internal states.
	 *
	 * The default of this function does nothing.
	 *
	 * \param  compJob	This is the compute job, that is about to be executed.
	 * \param  grid		This is the grid container object.
	 * \param  mColl	This is the marker collection.
	 */
	virtual
	bool
	hook_inspectSubgridDiffInterpol(
		const ComputeJob_t& 		compJob,
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl);



	/*
	 * ========================
	 * ========================
	 * Dedicated Plan Functions
	 *
	 * These functions are provided to ease the composing
	 * of complicated scenarios.
	 */

	/*
	 * =========================
	 * Temperature
	 */
protected:
	/**
	 * \brief	This function adds the temperature to the compute plan.
	 *
	 * This function is used to generate
	 *
	 * \param  compPlan	This is the compute plan.
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 * \param  jType	This parameter encodes which kind of interpolation should be generated.
	 */
	virtual
	void
	prot_makeTempJob(
		ComputePlan_t* const 		compPlan,
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime,
		const eJobType 			jType)
	 const
	 final;


	/**
	 * \brief	This function returns the scalling constant for the
	 * 		 temperature subgrid diffusion.
	 *
	 * This is a dimensionless scaling constant that allows to adjust the
	 * strength of the diffusion. It has to be in the range [0, 1].
	 * It is called by the prot_doTempInterpol() function
	 *
	 * The default of this function returns 1
	 */
	virtual
	Numeric_t
	hook_getTempDiffScaling()
	 const;


	/*
	 * ======================
	 * Velocity
	 * Note they are used for all component, select them
	 * by specifing the property.
	 */
protected:
	/**
	 * \brief	This function adds the velocity to the compute plan.
	 *
	 * To select which velocity is passed use the index argument.
	 * Note that this index argument, must be able to look the
	 * velocity in teh mechanical solver.
	 * The velocity if found in the grid and colver, by looking up,
	 * the passed property. The marker property is found
	 * by looking up the representant.
	 *
	 * Note that insead of the pulled back, visco elastic viscosity,
	 * the viscosity defined on the marker is used.
	 *
	 * \param  compPlan	This is the compute plan.
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 * \param  jType	This parameter encodes which kind of interpolation should be generated.
	 * \param  pIdx		Which velocity is loaded.
	 */
	virtual
	void
	prot_makeVelJob(
		ComputePlan_t* const 		compPlan,
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime,
		const eJobType 			jType,
		const PropIdx_t 		pIdx)
	 const
	 final;


	/**
	 * \brief	This function returns the scalling constant for the
	 * 		 velocity subgrid diffusion scaling.
	 *
	 * The returned value must be indise the range [0, 1].
	 * Note That this function uses a property index to distingusih
	 * which component is used.
	 * The default of this function returns one.
	 *
	 * \param  pIdx		The velocity component to access.
	 */
	virtual
	Numeric_t
	hook_getVelDiffScaling(
		const PropIdx_t 	pIdx)
	 const;




	/*
	 * ========================
	 * Access functions
	 */
public:

	/*
	 * ========================
	 * Execution Functions
	 *
	 * These function perform the real task.
	 * There is one driver that performs a task
	 * and for each of the three interpolation
	 * task a dedicated fucntion.
	 */
protected:
	/**
	 * \brief	This function performs the execution of
	 * 		 the compute plan.
	 *
	 * It is prinmarly provided for debugging. It should not
	 * be overriden. It is called by the mapping function
	 * after the test is created. It allows to execute a plan
	 * without having all the needed argument for the full
	 * function.
	 *
	 * \param  compPlan	The compution plan that should be executed.
	 * \param  gridCont	The grid container that is used.
	 * \param  mColl	The marker collection.
	 */
	virtual
	void
	execurePlan(
		const ComputePlan_t& 		compPlan,
		const GridContainer_t& 		gridCont,
		MarkerCollection_t* const 	mColl)
	 final;


	/**
	 * \brief	This function performs the interpolation.
	 *
	 * This function operates on a single job and executes it.
	 * This function also assumes that the interpolation state
	 * of *thsi was updated.
	 *
	 * This function will unpack the job and called the dedicated
	 * implementation functions. It will also calls the hooks
	 * for inspecting before it unpacks it.
	 *
	 * \param  job		The job that should be executed.
	 * \param  grid		This is the grid container.
	 * \param  mColl 	The marker collction that should be modified.
	 *
	 * \note 	This function performs some operation, such as the
	 * 		 absolute interpolation task (setting marker to zero),
	 * 		 but basically calls other functions.
	 */
	virtual
	void
	executeJob(
		const ComputeJob_t& 		job,
		const GridContainer_t& 		grid,
		MarkerCollection_t* const 	mColl)
	 final;


	/**
	 * \brief	This function performs subgrid diffusion.
	 *
	 * This function will allocate a temporary grid property
	 * for the computation. It will perform the subgrid diffusion
	 * process as it is described in the book.
	 *
	 * \param  mProp	The marker property that should be updated.
	 * \param  pFieldOld	The old property field.
	 * \param  pFieldChange	The change in the property field.
	 * \param  diffConst	This is the diffusion constant.
	 * \param  pModWeight	Marker property to do conservative interpolation.
	 */
	virtual
	void
	prot_performSubgridDiff(
		MarkerProperty_t* const		mProp,
		const GridProperty_t& 		pFieldOld,
		const GridProperty_t& 		pFieldChange,
		const DiffConstant_t& 		diffConst,
		const MarkerProperty_t* const 	pModWeight)
	 const
	 final;


	/**
	 * \brief	This function is able to perform the cahnge interpolation.
	 *
	 * It will update the marker property, for that the change field is pull
	 * back to the marker, with enabled update flag.
	 *
	 * Note this function assumes that the interpolation state of *this was set up.
	 *
	 * \param  mProp 	Markerproperty to update
	 * \param  pFieldChange	The grid property that encodes the change.
	 */
	virtual
	void
	prot_performChangeInterpol(
		MarkerProperty_t* const 	mProp,
		const GridProperty_t& 		pFieldChange)
	 const
	 final;


	/**
	 * \brief	This function performs absolute interpolation.
	 *
	 * This means that the markers are not updated as it is done
	 * in the chnage interpolation function, but overwritten.
	 * The grid property is pulled back to the markers and
	 * the old marker property is overwritten.
	 *
	 * \param  mProp	The marker property that we write to.
	 * \param  pField 	This is the property field that is interpolated.
	 *
	 * \note	This function is located in the file that also contains
	 * 		 the change interpolation.
	 */
	virtual
	void
	prot_performAbsInterpolation(
		MarkerProperty_t* const 	mProp,
		const GridProperty_t& 		pField)
	 const
	 final;


	/*
	 * ===========================
	 * Private members
	 */
private:
	InterpolState_t 		m_interpolState;		//!< This is the interpolation state.
}; //End class(egd_gridToMarkersBase_t)


PGL_NS_END(egd)


