/**
 * \brief	This function interpolated the changes to the marker.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_GridToMarkInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)

void
egd_gridToMarkersBase_t::prot_performChangeInterpol(
	MarkerProperty_t* const 	mProp_,
	const GridProperty_t& 		pFieldChange)
 const
{
	pgl_assert(mProp_ != nullptr);		//Alias
	MarkerProperty_t& mProp = *mProp_;

	if((pFieldChange.getType() != m_interpolState.gType)  ||
	   (isValidGridType(m_interpolState.gType) == false)    )
	{
		throw PGL_EXCEPT_LOGIC("The interpolation set is not compatible with the grid.");
	};


	/*
	 * We can simply call the bull back function.
	 */
	egd_pullBackToMarker(
		&mProp,
		pFieldChange, m_interpolState,
		true);				//Enable updating

	return;
}; //End: interpolate changes


void
egd_gridToMarkersBase_t::prot_performAbsInterpolation(
	MarkerProperty_t* const 	mProp_,
	const GridProperty_t& 		pField)
 const
{
	pgl_assert(mProp_ != nullptr);		//Alias
	MarkerProperty_t& mProp = *mProp_;

	if((pField.getType() != m_interpolState.gType)  ||
	   (isValidGridType(m_interpolState.gType) == false)    )
	{
		throw PGL_EXCEPT_LOGIC("The interpolation set is not compatible with the grid.");
	};


	/*
	 * We can simply call the bull back function.
	 */
	egd_pullBackToMarker(
		&mProp,
		pField, m_interpolState,
		false);				//Disableling update

	return;
}; //End: absolute interpolatin

PGL_NS_END(egd)


