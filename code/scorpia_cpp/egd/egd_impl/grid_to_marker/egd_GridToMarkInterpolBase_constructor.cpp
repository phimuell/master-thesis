/**
 * \brief	This file contains the implementation of teh constructors.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_GridToMarkInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD



PGL_NS_START(egd)

egd_gridToMarkersBase_t::egd_gridToMarkersBase_t(
	const egd_confObj_t& 	confObj)
 :
  egd_gridToMarkersBase_t()		//Calling the default constructor, noting is needed.
{
	//All checks are done in the other in other parts
	(void)confObj;
};

egd_gridToMarkersBase_t::~egd_gridToMarkersBase_t()
 = default;


egd_gridToMarkersBase_t::egd_gridToMarkersBase_t()
 = default;


egd_gridToMarkersBase_t::egd_gridToMarkersBase_t(
	const egd_gridToMarkersBase_t&)
 = default;


egd_gridToMarkersBase_t&
egd_gridToMarkersBase_t::operator= (
	const egd_gridToMarkersBase_t&)
 = default;


egd_gridToMarkersBase_t::egd_gridToMarkersBase_t(
	egd_gridToMarkersBase_t&&)
 noexcept
 = default;


egd_gridToMarkersBase_t&
egd_gridToMarkersBase_t::operator= (
	egd_gridToMarkersBase_t&&)
 = default;





PGL_NS_END(egd)











