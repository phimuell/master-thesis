/**
 * \brief	This file will contains the implementation of the velocity only subgrid diffusion.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_GridToMarkerInterpol_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include "./egd_GridToMarkInterpol_sgComputePlan.hpp"

#include "./egd_GridToMarkInterpol_velSGOnly.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)



egd_gridToMarkersVelOnlySG_t::Numeric_t
egd_gridToMarkersVelOnlySG_t::hook_getVelDiffScaling(
	const PropIdx_t 	pIdx)
 const
{
	return this->Base_t::hook_getVelDiffScaling(pIdx);
};


std::string
egd_gridToMarkersVelOnlySG_t::print()
 const
{
	return std::string("Standard (vel only) grid to marker interpolator, job type is: " + this->m_jobType);
};


egd_gridToMarkersVelOnlySG_t::ComputePlan_t
egd_gridToMarkersVelOnlySG_t::hook_makeComputePlan(
	const MarkerCollection_t& 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	pgl_assert(tStepIdx >= 0);

	//We only do subgrid diffusion
	const eJobType jType = this->m_jobType;

	//make an empty plan
	ComputePlan_t cPlan;

	/*
	 * Iterate over all solution fields
	 */
	const Index_t N = mSol.nSolFields();
	for(Index_t i = 0; i != N; ++i)
	{
		//Load the property index of the field
		const PropIdx_t iPropIdx = mSol.getSolField(i).getPropIdx();
			pgl_assert(iPropIdx.isValid());

		//Test if it is velocity
		if(iPropIdx.isVelocity() == true)
		{
			this->prot_makeVelJob(
					&cPlan,
					mColl, grid,
					mSol, tSol,
					thisDt, tStepIdx, currTime,
					jType,
					iPropIdx);
		}; //end if: is velocity

		/*
		 * If we are here, then we have nothing
		 * more to do
		 */
	}; //End for(i):

		pgl_assert(cPlan.isValid(),
			   cPlan.hasJobs() );
	return cPlan;
}; //End: make compute plan




/*
 * ======================
 * Constructors
 */
egd_gridToMarkersVelOnlySG_t::egd_gridToMarkersVelOnlySG_t(
	const egd_confObj_t& 	confObj)
 :
  Base_t(confObj)
{
	//load the value we have to do
	bool wasFound = false;
	const eJobType rJType = confObj.getGtoMJobType(eJobType::SubGrid, &wasFound);

	if(isValidJobType(rJType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed job type is not valid.");
	};

	this->m_jobType = rJType;
};


egd_gridToMarkersVelOnlySG_t::egd_gridToMarkersVelOnlySG_t(
	const eJobType 		jType)
 :
  egd_gridToMarkersVelOnlySG_t()
{
	if(isValidJobType(jType) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed job type is not valid.");
	};

	this->m_jobType = jType;
};


egd_gridToMarkersVelOnlySG_t::~egd_gridToMarkersVelOnlySG_t()
 = default;


egd_gridToMarkersVelOnlySG_t::egd_gridToMarkersVelOnlySG_t()
 = default;


egd_gridToMarkersVelOnlySG_t::egd_gridToMarkersVelOnlySG_t(
	const egd_gridToMarkersVelOnlySG_t&)
 = default;


egd_gridToMarkersVelOnlySG_t&
egd_gridToMarkersVelOnlySG_t::operator= (
	const egd_gridToMarkersVelOnlySG_t&)
 = default;


egd_gridToMarkersVelOnlySG_t::egd_gridToMarkersVelOnlySG_t(
	egd_gridToMarkersVelOnlySG_t&&)
 noexcept
 = default;


egd_gridToMarkersVelOnlySG_t&
egd_gridToMarkersVelOnlySG_t::operator= (
	egd_gridToMarkersVelOnlySG_t&&)
 = default;


PGL_NS_END(egd)


