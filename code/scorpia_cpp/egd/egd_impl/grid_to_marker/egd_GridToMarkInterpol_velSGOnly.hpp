#pragma once
/**
 * \brief	This file contains teh implementation of the mapping
 * 		 if only velocity is concerned.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_GridToMarkerInterpol_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include "./egd_GridToMarkInterpol_sgComputePlan.hpp"

#include "./egd_GridToMarkInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


/**
 * \class 	egd_gridToMarkersVelOnlySG_t
 * \brief	This class will interpolate velocity from the
 * 		 grid to marker using subgrid diffusion.
 *
 * This class will only map velocities back from grid the grid to
 * the markers.
 * Which mapping method is used, can be selected by the an enum.
 * The default is subgrid diffusion.
 */
class egd_gridToMarkersVelOnlySG_t : public egd_gridToMarkersBase_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t 			= egd_gridToMarkersBase_t;	//!< This is the base implementation

	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using Base_t::PropIdx_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using Base_t::InterpolState_t;

	using Base_t::MarkerProperty_t;
	using Base_t::GridProperty_t;
	using Base_t::GridGeometry_t;

	using Base_t::ComputePlan_t;
	using Base_t::ComputeJob_t;
	using Base_t::DiffConstant_t;

	using Base_t::eJobType;


	/*
	 * ======================
	 * Public Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function does nothing, since no building is
	 * needed.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_gridToMarkersVelOnlySG_t(
		const egd_confObj_t& 		confObj);


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	~egd_gridToMarkersVelOnlySG_t();


	/**
	 * \brief	This is the constructor that allows to select
	 * 		 the job type that sjould be used.
	 *
	 * \param  jType	The kind of interpolation that should be used.
	 */
	egd_gridToMarkersVelOnlySG_t(
		const eJobType 		jType);


	/**
	 * \brief	This is the default constructor.
	 *
	 * This constructs a mapper that uses subgrid diffusion.
	 */
	egd_gridToMarkersVelOnlySG_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_gridToMarkersVelOnlySG_t(
		const egd_gridToMarkersVelOnlySG_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_gridToMarkersVelOnlySG_t&
	operator= (
		const egd_gridToMarkersVelOnlySG_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_gridToMarkersVelOnlySG_t(
		egd_gridToMarkersVelOnlySG_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted.
	 */
	egd_gridToMarkersVelOnlySG_t&
	operator= (
		egd_gridToMarkersVelOnlySG_t&&);


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	using Base_t::mapBackToMarkers;

	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 */
	std::string
	print()
	 const
	 override;


	/*
	 * =========================
	 * Status function
	 *
	 * These functions are specific to the concrete function.
	 */
public:

	/*
	 * ======================
	 * Hooks
	 *
	 * These are the hooks that are implemented.
	 */
protected:
	/**
	 * \brief	This function returns a compute plan.
	 *
	 * This function will create a compute plan for velocity only.
	 * It will add _all_ velocities from the mechanical solution
	 * to it.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	ComputePlan_t
	hook_makeComputePlan(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	using Base_t::hook_doPostProcess;
	using Base_t::hook_inspectAbsInterpol;
	using Base_t::hook_inspectPureChangeInterpol;
	using Base_t::hook_inspectSubgridDiffInterpol;

	/*
	 * ========================
	 * ========================
	 * Dedicated Plan Functions
	 *
	 * These functions are provided to ease the composing
	 * of complicated scenarios.
	 */

	/*
	 * =========================
	 * Temperature
	 */
protected:
	using Base_t::prot_makeTempJob;
	using Base_t::hook_getTempDiffScaling;


	/*
	 * ========================
	 * Velocity
	 */
protected:
	/**
	 * \brief	This function returns the scalling constant for the
	 * 		 velocity subgrid diffusion scaling.
	 *
	 * This function will call the default implementaion.
	 *
	 * \param  pIdx		The velocity component to access.
	 */
	virtual
	Numeric_t
	hook_getVelDiffScaling(
		const PropIdx_t 	pIdx)
	 const
	 override;

	using Base_t::prot_makeVelJob;



	/*
	 * ========================
	 * Access functions
	 */
public:

	/*
	 * ========================
	 * Execution Functions
	 *
	 * These function perform the real task.
	 * There is one driver that performs a task
	 * and for each of the three interpolation
	 * task a dedicated fucntion.
	 */
protected:
	using Base_t::execurePlan;
	using Base_t::executeJob;
	using Base_t::prot_performSubgridDiff;
	using Base_t::prot_performChangeInterpol;
	using Base_t::prot_performAbsInterpolation;

	/*
	 * ===========================
	 * Private members
	 */
private:
	eJobType		m_jobType = eJobType::SubGrid;	//!< This is the job type that is being used.
}; //End class(egd_gridToMarkersVelOnlySG_t)

PGL_NS_END(egd)


