/**
 * \brief	This file contains the implementation of the
 * 		 functions that deals with the subgrid diffusion.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include "./egd_GridToMarkInterpolBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)

void
egd_gridToMarkersBase_t::prot_performSubgridDiff(
	MarkerProperty_t* const		mProp_,
	const GridProperty_t& 		pFieldOld,
	const GridProperty_t& 		pFieldChange,
	const DiffConstant_t& 		diffConst,
	const MarkerProperty_t* const 	pModWeight)	//May be null if not needed
 const
{
	/*
	 * Note that this function is a bit more memeory intense than the previous one.
	 * But it now operates completly with the high level functions.
	 */
	using pgl::isValidFloat;

	pgl_assert(mProp_ != nullptr);		//Alias the target property
	MarkerProperty_t& mProp = *mProp_;

	if(this->m_interpolState.isProcessed() == false)
	{
		throw PGL_EXCEPT_LOGIC("The interpolation state is not processed.");
	};

	/*
	 * Calculate the subgrid diffusion at the marker.
	 *
	 * We use the following formulat to compute it:
	 *
	 * 	DT_{m}^{s} := (T_{m;nodal}^{0} - T_{m}^{0})
	 * 			* (1 - \Exp{A}
	 *
	 * Where as $T_{m;nodal}^{0}$ is the temperature at the
	 * location of marker m, that is optained by interpolating
	 * the four node temperature to that location, with a non
	 * conservative scheme.
	 *
	 * A is also known as diffusion constant, that is stored by the job.
	 * it is composed as:
	 * 	A         := -d \cdot \frac{\Delta t}{t_{diff}}
	 * 	t_{diff}  := (RhoCp)_{m} / (k_{m} * ( 2 / h_x + 2 / h_y) )
	 */

	//compute the temperature of the virtaul nodes
	//This is done by pull back the grid property of old field to the marker
	MarkerProperty_t temp_virtualNodes;
	egd_pullBackToMarker(&temp_virtualNodes,
			pFieldOld, m_interpolState);
		pgl_assert(temp_virtualNodes.isFinite().all(),
			   temp_virtualNodes.size() == mProp.size());

	//Now we compute the virtual subgrid change
	const MarkerProperty_t DT_m_sg = (temp_virtualNodes - mProp) * (1.0 - diffConst.exp());


	/*
	 * Now we interpolate the subgrid diffusion from the markers
	 * to the grid
	 */
	GridProperty_t DT_node_sg(pFieldOld);	//Such that it has the same size as we want
	egd_mapToGrid(m_interpolState,
			DT_m_sg, 		//source
			&DT_node_sg, 		//target
			pModWeight, 		//Modified
			(pModWeight == nullptr ? false : true));	//Force a dissabeling of the ghost nodes


	/*
	 * Now we can compute the residual temperature difference at the
	 * nodes by reformulating the relation of the temperature difference.
	 *
	 *   	DT_{ij} == DT_{ij}^{s} + DT_{ij}^{r}
	 *   	  <=>
	 *   	DT_{ij}^{r} == DT_{ij} - DT_{ij}^{s}
	 */
	GridProperty_t DT_node_r(pFieldChange);
		DT_node_r.maxView() -= DT_node_sg.maxView();


	/*
	 * Now we compute DT_{m}^{r}, which is the rmaining temperature
	 * change at the marker.
	 * This value can be optained by interplolating DT_{ij}^{r}
	 * to the marker, for that the normal interpolation,
	 * no conservative, is used.
	 */
	MarkerProperty_t DT_m_r;
	egd_pullBackToMarker(&DT_m_r,
			DT_node_r, m_interpolState);
		pgl_assert(DT_m_r.isFinite().all(),
			   DT_m_r.size() == mProp.size());


	/*
	 * Now we update the marker temperature.
	 * We do this by the following new formula.
	 *
	 *  T_{m}^{corr} := T_{m}^{0} + DT_{m}^{s} + DT_{m}^{r}
	 */
	mProp += DT_m_sg + DT_m_r;	//T_{m}^}{0} is already stored in mProp
		pgl_assert(mProp.isFinite().all());

	return;
}; //End: prot_performSubgridDiff




PGL_NS_END(egd)


