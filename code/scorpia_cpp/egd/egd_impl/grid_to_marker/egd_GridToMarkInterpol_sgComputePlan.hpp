#pragma once
/**
 * \brief	This file contains the compute plan object for the subgrid diffusion object.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_list.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <functional>


PGL_NS_START(egd)

/**
 * \brief	This is a subgrid mapping job.
 * \class 	egd_sgComputeJob_t
 *
 * This class is used to encode how the grid property should be
 * interpolated back. This class allows quite powerfull operations.
 *
 * This class encodes how the marker property, mProp, should be
 * updated. Its primarly goal is to enable subgrid diffusion, but
 * it is able to perform other kinds of interpolations.
 *
 * In order to perform subgrid interpolation, *this needs to have
 * the diffConsatnts, this constant are stored into a vector
 * one for each marker. This value is the argument, "A",  of the
 * exponential in the description of the interpolator.
 *
 * *this also needs a change, defined on the grid, that should be
 * mapped to the markers. It is special, in the sense, that this
 * change can also be owned by *this, so it can be created just for
 * that, therwhise a pointer is stored.
 * If subgrid diffusion is requested, also the old property field,
 * defined on the same grid, must be passed to *this. However that
 * field must exist and can not be moved into *this.
 *
 * If the number of diffusion coefficient is zero, then pure change
 * interpolation is selected. In that case the changes are interpolated
 * back to the markers. This means the old field is ignored.
 *
 * The last mode that is supported, is the absolute interpolation.
 * In that mode the interpolation assumes that the change is no change
 * but absolute values that are interpolated back. In that case the
 * old property filed is ignored as well.
 * This is equivalent to the pure change interpolation, when the
 * property on the marker is set to zero, before the interpolatin
 * is done.
 *
 */
class egd_sgComputeJob_t
{
	/*
	 * ==========================
	 * Typedef
	 */
public:
	using GridContainer_t 		= egd_gridContainer_t;				//!< This is the grid container.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;		//!< This is a grid property.
	using MarkerCollection_t 	= GridContainer_t::MarkerCollection_t;		//!< This is the parker collection.
	using DiffConstant_t 		= egd_Array_t<Numeric_t, ::Eigen::Dynamic, 1>;	//!< This is the diffusion constant.
	using PropIdx_t 		= GridProperty_t::PropIdx_t;			//!< This is the property index.



	/*
	 * ==========================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is a building constructor that references the
	 * 		 the grid property.
	 *
	 * This means no ownership is taken. It is also important that the
	 * Diffusion constants is moved into *this. Note that you have to
	 * use the std::cref funnction.
	 *
	 * The passed grid property is interpreted as change that occured
	 * at the markers. For subgrid diffusion all non defaulted arguments
	 * need to be given. wUpdate is a marker property that is used in
	 * the conservative interpolation for the subgrid diffusion. The
	 * oldDield is the old property field and.
	 *
	 * The oldFiled and the wUpdate are ignored if no subgriddiffusion
	 * is given. PureChange interpolation is activated by passing and
	 * empty vector as diffusion constants. Also oldField is ignored
	 * in that case. Absolute interpolation is by additionally setting
	 * the absInterpol argument to true.
	 *
	 * The conservative scheme is disabled by, setting the weight
	 * modifier property to the same value as the marker property.
	 *
	 * \param  mProp	The marker property that should be updated.
	 * \param  wUpdate	Marker property to update the interpolation weights.
	 * \param  diffConst	The diffusion constant.
	 * \param  gProp	The grid property that should be used.
	 * \param  oldField	The old property field.
	 * \param  absInterpol	Perform absolute interpolation.
	 */
	egd_sgComputeJob_t(
		const PropIdx_t& 				mProp,
		const PropIdx_t& 				wUpdate,
		DiffConstant_t&& 				diffConst,
		std::reference_wrapper<const GridProperty_t> 	gProp,
		const GridProperty_t* 				oldField,
		const bool 					absInterpol = false)
	 :
	  egd_sgComputeJob_t(
	  	mProp, wUpdate,
	  	std::move(diffConst),
	  	std::addressof(gProp.get()), false,	//false ~ no ownership
	  	oldField,				//Never take ownership
	  	absInterpol)
	{
	};


	/**
	 * \brief	This is also a non aquiering constructor, but it takes a mutable
	 * 		 Chnage argument.
	 *
	 * This is provided for convenience. This function allows to use std::ref
	 *
	 * \param  mProp	The marker property that should be updated.
	 * \param  wUpdate	Marker property to update the interpolation weights.
	 * \param  diffConst	The diffusion constant.
	 * \param  gProp	The grid property that should be used.
	 * \param  oldField	The old property field.
	 * \param  absInterpol	Perform absolute interpolation.
	 */
	egd_sgComputeJob_t(
		const PropIdx_t& 				mProp,
		const PropIdx_t& 				wUpdate,
		DiffConstant_t&& 				diffConst,
		std::reference_wrapper<GridProperty_t> 		gProp,
		const GridProperty_t* 				oldField,
		const bool 					absInterpol = false)
	 :
	  egd_sgComputeJob_t(
	  	mProp, wUpdate,
	  	std::move(diffConst),
	  	std::cref(gProp.get()),
	  	oldField, absInterpol)
	{
	};


	/**
	 * \brief	This is the building constructor, that aquires ownership of the
	 * 		 passed grid property, that encodes the difference.
	 *
	 * See the description of the other constructor for information.
	 *
	 * \param  mProp	The marker property that should be updated.
	 * \param  wUpdate	Marker property to update the interpolation weights.
	 * \param  diffConst	The diffusion constant.
	 * \param  gProp	The grid property that should be used.
	 * \param  oldField	The old property field.
	 * \param  absInterpol	Perform absolute interpolation.
	 */
	egd_sgComputeJob_t(
		const PropIdx_t& 				mProp,
		const PropIdx_t& 				wUpdate,
		DiffConstant_t&& 				diffConst,
	 	std::unique_ptr<GridProperty_t>& 		gProp,
	 	const GridProperty_t* 				oldField,
	 	const bool 					absInterpol = false)
	 :
	  egd_sgComputeJob_t(
	  	mProp, wUpdate,
	  	std::move(diffConst),
	  	gProp.get(), false,	//We will not take ownership. yet
	  	oldField, 		//Never take ownership
	  	absInterpol)
	{
		//We have to release the object.
		gProp.release();

		/*
		 * Now nobody has ownersip over the resource. We will now
		 * aquire it. We do this here, in order to avoid a double
		 * free error if an exception is generated, because technically
		 * the unique pointer still have the ownership.
		 */
		this->m_ownProp = true;		//We are now in controll.
	}; //End: aquiering building constructor


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_sgComputeJob_t(
		const egd_sgComputeJob_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * other will be invalid.
	 */
	egd_sgComputeJob_t(
		egd_sgComputeJob_t&& other)
	 :
	  egd_sgComputeJob_t()		//Call the default constructor.
	{
		//Tets if other is valid, currently it is only copied
		if(other.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Tried to initialize *this from an invalid object.");
		};

		//Now move other into *this
		this->operator=(std::move(other));
			pgl_assert(this->isValid(),
				   other.isValid() == false);
	}; //End: move


	/**
	 * \brief	Move assignment.
	 *
	 * Is makes other invalid.
	 */
	egd_sgComputeJob_t&
	operator= (
		egd_sgComputeJob_t&& other)
	{
		/*
		 * Note the process is quite complicated and obscure.
		 * But it allws us to forget this function ifwe change
		 * the structure of this. The only thing that is not
		 * obvious is that we manualy transfer the diffusion
		 * constant. It is first swapped into a temporary,
		 * this is done, such that we can copy other without
		 * coping the large structure, we then swap it in
		 * into this.
		 */

		//Test if the source is okay
		if(other.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Tried to assigne an invalid job to *this.");
		};

		//Perform a print of this
		if(IS_VERBOSE() == true)
		{
			this->printResourceInfo(
				"Delete *this inside move assignment",
				std::cerr);
		}; //End if: should print

		//We first have to invalidate *this, to free resources
		this->invalidate();

		//We first temporary store the Diffusion constants, such that they are
		//not needed to be copied.
		DiffConstant_t tempConst;
		tempConst.swap(other.m_diffConst);
			pgl_assert(other.m_diffConst.size() == 0);

		//Now copy everything into *this
		this->operator=(other);

		//Now we have to swap in the temporary extracted diffusion constants
		this->m_diffConst.swap(tempConst);

		//Now *This owns the resource,s o we will set teh bool to false to indicate this
		other.m_ownProp = false;
		other.m_gProp   = nullptr;	//this will deactivbate the printing, because it is uninteresting

		//Now we will invalidate other. This will not realse the resource, because *this
		//owns it. Also not that it will not complain about an error.
		other.invalidate();

		pgl_assert(this->isValid(),
			   other.isValid() == false);

		//Return *this.
		return *this;
	};//End: move assignbement


	/**
	 * \brief	Destructor.
	 *
	 * If *this owns the difference type, then it will be deallocated.
	 * if not nothing will happen.
	 */
	~egd_sgComputeJob_t()
	{
		if(IS_VERBOSE() == true)
		{
			//Perform the printing only if we have a valid
			//grid property field, this is an internal behaviour
			if(m_gProp != nullptr)
			{
				try
				{
					this->printResourceInfo(
						"Destructor",
						std::cerr);
				}
				catch(const ::std::exception& e)	//Will also catch pgl exceptions
				{
					std::cerr
						<< "The resource printing function has thrown an exception."
						<< " The exception read as:\n"
						<< e.what()
						<< std::endl;

					::std::terminate();
				}; //End: catch
			}; //End if: print
		}; //End: should print

		//We call invalidate, this will free the resources
		this->invalidate();
	}; //End: destructor


	/*
	 * ======================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	This is the internal constructor.
	 *
	 * This constructor accepts the data, that the two different
	 * forms of the constructor passed to *this.
	 *
	 * \param  mProp	The property that should be updated.
	 * \param  wUpdate	This is the property that modifies the interpolation.
	 * \param  diffConst	The diffusion constant that should be used.
	 * \param  gProp	Pointer to the property that stores the DIFFERENCE.
	 * \param  ownsProp	True, then *This will assume ownership of the property.
	 * \param  oldField	The old property field.
	 * \param  absInterpol	Indicates if the absolute interpolation is needed.
	 */
	egd_sgComputeJob_t(
		const PropIdx_t 		mProp,
		const PropIdx_t 		wUpdate,
		DiffConstant_t&&   		diffConst,
		const GridProperty_t* const 	gProp,
		const bool 			ownProp,
		const GridProperty_t* const 	oldField,
		const bool 			absInterpol)
	 :
	  m_mProp(mProp),
	  m_wUpdate(wUpdate),
	  m_noSubgrid(false),
	  m_diffConst(std::move(diffConst)),
	  m_ownProp(ownProp),
	  m_gProp(gProp),
	  m_oldField(oldField),
	  m_absInterpol(absInterpol)
	{
		//Test if we have to perform subgrid diffusion
		if(m_diffConst.size() == 0)
		{
			m_noSubgrid = true; 	//No subgrid diffusion needed
		};

		if(m_gProp == nullptr)
		{
			throw PGL_EXCEPT_NULL("The passed property is a null pointer.");
		};
		if((this->doSubGridDiffusion())
		   ? (m_oldField == nullptr)		//Only in the case of subgrid diffusion we need to have a old field
		   : (false))
		{
			throw PGL_EXCEPT_InvArg("No old field passed.");
		};
		if(m_wUpdate.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed weight modifier is invalid.");
		};
		if(m_mProp.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed marker property is invalid.");
		};
		if(this->doSubGridDiffusion()
		   ? (m_diffConst.size() <= 0)
		   : (m_diffConst.size() != 0) )
		{
			throw PGL_EXCEPT_InvArg("The passed diffusion constant is invalid.");
		};
			pgl_assert(m_diffConst.isFinite().all());
		if(m_gProp->getPropIdx().getRepresentant() != m_mProp.getRepresentant())
		{
			throw PGL_EXCEPT_InvArg("The two properties are not the same.");
		};
		if((m_oldField != nullptr)
		   ? (m_gProp->getType() != m_oldField->getType())
		   : (false)                                       )
		{
			throw PGL_EXCEPT_InvArg("The passed grid and passed old grid, are not the same.");
		};

		//Depending if no subgrid diffusion is needed, deactivate everything
		if(this->doSubGridDiffusion() == false)
		{
			if(m_oldField != nullptr)
			{
				throw PGL_EXCEPT_InvArg("Subgrid diffusion was disabled, but passed the old field.");
			};
			if(m_wUpdate  != m_mProp)
			{
				throw PGL_EXCEPT_InvArg("Subgrid diffusion was disabled, but not deactivated the conservative sheme.");
			};
		}; //End: modify *this in case no supgrid is needed


		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("The valid test failed.");	//This may lead to a double free error in case of error
		};

		if(IS_VERBOSE() == true)
		{
			try
			{
				this->printResourceInfo(
						"Impl Building Contr.",
						std::cerr);
			}
			catch(const std::exception& e)
			{
				std::cerr
					<< "An exception occured in the building constructor while printing the resource informations."
					<< "\n" << "The error was:\n"
					<< e.what()
					<< std::endl;

				std::terminate();
			}; // End catch
		}; //End if verbose output
	}; //End: internal building


	/**
	 * \brief	Default constructor.
	 *
	 * Is needed for internal use.
	 */
	egd_sgComputeJob_t()
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * For internal use.
	 */
	egd_sgComputeJob_t&
	operator= (
		const egd_sgComputeJob_t&)
	 = default;



	/*
	 * ==============================
	 * Function.
	 */
public:
	/**
	 * \brief	This function returns true if *this is valid.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		if(m_gProp == nullptr)
		{
			//pgl_assert(false);	//this is like a valid invalid, so we do not assert here
			return false;
		};
		if(m_wUpdate.isValid() == false ||
		   m_mProp.isValid()   == false   )
		{
			pgl_assert(false);
			return false;
		};
		if(this->doSubGridDiffusion()
		   ? (m_diffConst.size() <= 0)
		   : (m_diffConst.size() != 0) )
		{
			pgl_assert(false);
			return false;
		};
		if(this->doSubGridDiffusion()
		   ? (m_oldField == nullptr)	//Only in subgrid case we need that.
		   : (false)                 )
		{
			pgl_assert(false);
			return false;
		};
		if(m_gProp->getPropIdx().getRepresentant() != m_mProp.getRepresentant())
		{
			pgl_assert(false);
			return false;
		};
		if((m_oldField != nullptr)
		   ? (m_gProp->getPropIdx().getRepresentant() != m_oldField->getPropIdx().getRepresentant())
		   : (false 										   ) )
		{
			pgl_assert(false);
			return false;
		};
		if(::egd::isValidGridType(m_gProp->getType()) == false)
		{
			pgl_assert(false);
			return false;
		};
		if((m_oldField != nullptr)
		   ? (m_gProp->getType() != m_oldField->getType())
		   : (false)                                       )
		{
			pgl_assert(false);
			return false;
		};

		return true;
	}; //End: isValid


	/**
	 * \brief	This function returns true if subgrid is enabled.
	 */
	bool
	doSubGridDiffusion()
	 const
	 noexcept
	{
		return (m_noSubgrid == false) ? true : false;
	}; //End if: do Subgrid diffusion.


	/**
	 * \brief	Returns true if absolute interpolation should be done.
	 *
	 * Absolute interpolation means that the property that is recoreded is
	 * not interpreted as a change that should be interpolated back,
	 * but as an absolute value that should be interpolated.
	 * This effectively sets the marker property to zero.
	 *
	 * Note that it is allowed to perfrom absolute interpolation and
	 * subgrid diffusion at the same time, it is just a bit strage to
	 * do that.
	 */
	bool
	doAbsInterpolation()
	 const
	 noexcept
	{
		return (m_absInterpol == true) ? true : false;
	};//End: do absolute interpol


	/**
	 * \brief	This function returns true if only the changes
	 * 		 should be interpolated.
	 *
	 * This means that no subgrid diffusion is done, and no absolute
	 * interpolation is done.
	 */
	bool
	doPureChangeInterpol()
	 const
	 noexcept
	{
		return (this->doSubGridDiffusion() == false && this->doAbsInterpolation() == false) ? true : false;
	}; //End: check if only absolute interpol is done.



	/**
	 * \brief	This function returns true if the marker weights have to be updated.
	 *
	 * The marker weights have to be updated, if the update property is different from
	 * the real marker property.
	 */
	bool
	hasWeightModifier()
	 const
	 noexcept
	{
		return (m_wUpdate != m_mProp)
			? true
			: false;
	};//ENd: check if we have a property to modify


	/**
	 * \brief	This function returns the marker property that is used for updating the weights.
	 */
	PropIdx_t
	getWeightModifier()
	 const
	{
		pgl_assert(this->isValid());
		if(this->hasWeightModifier() == false)
		{
			throw PGL_EXCEPT_LOGIC("No property for modifing aviable.");
		};

		return m_wUpdate;
	}; //End: get the property to modify


	/**
	 * \brief 	This function returns the marker property that we have to update.
	 */
	PropIdx_t
	getMarkerProp()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return m_mProp;
	};


	/**
	 * \brief	Returns the grid property that stores the change.
	 *
	 * Note for convenience this function will return the absolute value
	 * in case of absolute interpolation, this is done for uniformity.
	 */
	const GridProperty_t&
	getGridChange()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return (*m_gProp);
	};


	/**
	 * \brief	This function returns the absolute value that should
	 * 		 be interpolated back.
	 *
	 * Technically this function is the same as the getGridCahnge() function
	 * but checks if absolute interpolation is requested.
	 */
	const GridProperty_t&
	getNewAbsField()
	 const
	{
		if(this->doAbsInterpolation() == false)
		{
			throw PGL_EXCEPT_LOGIC("*this is not an absolute job.");
		};

		return this->getGridChange();
	}; //End: get new abs field


	/**
	 * \brief	This function returns the old field.
	 *
	 * Note that this function is only usefull if old field was
	 * used, this means that subgrid diffusion is performed.
	 */
	const GridProperty_t&
	getOldField()
	 const
	{
		if(this->doSubGridDiffusion() == false)
		{
			throw PGL_EXCEPT_LOGIC("The olf property field is only aviable, if subgrid is enabled.");
		};
		pgl_assert(m_oldField != nullptr);

		return (*m_oldField);
	}; //End: getOld filed


	/**
	 * \brief	Returns the diffusion constant.
	 */
	const DiffConstant_t&
	getDiffConstant()
	 const
	{
		pgl_assert(this->isValid());
		if(this->doSubGridDiffusion() == false)
		{
			throw PGL_EXCEPT_LOGIC("Requested the diffusion constants, but subgrid diffusion is disabled.");
		};
		return m_diffConst;
	};


	/**
	 * \brief	Returns the number of markers that are registered inside *this.
	 *
	 * The markers are queried from teh diff constant.
	 * Note that the value returned by this function is only usefull,
	 * if subgrid diffusion is enabled.
	 */
	Index_t
	nMarkers()
	 const
	 noexcept
	{
		pgl_assert(this->doSubGridDiffusion());
		return this->getDiffConstant().size();
	};


	/**
	 * \brief	This function returns the grid type of the grid property.
	 */
	eGridType
	getGridType()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return m_gProp->getType();
	};



	/*
	 * ===========================
	 * Operators
	 */
public:
	/**
	 * \brief	This fucntion compares *this with rhs.
	 *
	 * This function checks if *this is less than rhs, but
	 * here less has a very special meaning. Since we compare
	 * the grid type with each other.
	 *
	 * This is done, such that we can easaly group them together.
	 * And is rather useless otherwhise.
	 *
	 * \param  rhs		The right hand side.
	 */
	bool
	operator< (
		const egd_sgComputeJob_t& rhs)
	 const
	{
		pgl_assert(this->isValid(),
			     rhs.isValid() );
		return (this->getGridType() < rhs.getGridType())
			? true
			: false;
	};


	/**
	 * \brief	This function returns true if the marker
	 * 		 property of *this is the same as rhs.
	 *
	 * This function can be used to detect, if two job targets
	 * the same marker property.
	 *
	 * \param  rhs		The rhs of the comparisson.
	 */
	bool
	operator== (
		const egd_sgComputeJob_t& rhs)
	 const
	{
		pgl_assert(this->isValid(),
			     rhs.isValid() );

		return (this->m_mProp == rhs.m_mProp)
			? true
			: false;
	}; //End: check for same mColl



	/*
	 * =======================
	 * Resource printing functions
	 *
	 * These functions deals with the outputting
	 * of resource information of *this.
	 *
	 * Note it is an ugly hack that will send us
	 * straigth to hell.
	 */
public:
	/**
	 * \brief	This this function is called the
	 * 		 invalidate function is verbose.
	 *
	 * This is internaly for testing only.
	 */
	static
	void
	BE_VERBOSE();


	/**
	 * \brief	This function returns true if we should be verbose.
	 *
	 */
	static
	bool
	IS_VERBOSE();


	/**
	 * \brief	This function is a printing function.
	 *
	 * It should be used only for debugging. if you use it,
	 * you use EGD most likly incorrect.
	 *
	 * \param  where	A string that should indicate where the function is called.
	 * \param  sout		The stream we write to
	 */
	void
	printResourceInfo(
		const std::string&	where,
		std::ostream& 		sout)
	 const;



	/*
	 * =========================
	 * Private Function
	 */
private:
	/**
	 * \brief	This function deletes all associated resources.
	 *
	 * This function will dealocate the grid property of *this if it is owned.
	 * Note that also everything will be invalidated by this function.
	 */
	void
	invalidate()
	{
		if(this->m_ownProp == true)	//Test if we have to free
		{
			delete m_gProp;	//Free the memory
			m_gProp = nullptr;
			m_ownProp = false;
		}; //End: free memory

		//Invalidate *this
		this->m_mProp        = PropIdx_t::MAKE_INVALID_INDEX();
		this->m_wUpdate      = PropIdx_t::MAKE_INVALID_INDEX();
		this->m_gProp        = nullptr;
		this->m_ownProp      = false;
		this->m_absInterpol  = false;
		this->m_oldField     = nullptr;
		this->m_diffConst.resize(0);

		return;
	}; //End: invalidate




	/*
	 * ==========================
	 * Private Members
	 */
private:
	PropIdx_t 		m_mProp = PropIdx_t::MAKE_INVALID_INDEX();		//!< This is the property of the marker that we want to updtae.
	PropIdx_t 		m_wUpdate = PropIdx_t::MAKE_INVALID_INDEX();		//!< This is the property for modifing the weight of the interpolation, id the same as mProp then it has no effect.
	bool 			m_noSubgrid = false;	//!< This bool indicates if subgrid diffusion is required.
	DiffConstant_t 		m_diffConst;		//!< This is the diffusion constant.
	bool 			m_ownProp = false;	//!< Indicate if the property is owned by *this.
	const GridProperty_t* 	m_gProp = nullptr;	//!< This is the grid property, can be a reference or an owning instance.
	const GridProperty_t* 	m_oldField = nullptr;	//!< This is the old field.
	bool 			m_absInterpol = false;	//!< Indicates if absolute interpolation is needed.
}; //End class(egd_sgComputeJob_t):




/**
 * \brief	The compute plan is an object that records how
 * 		 the subgrid mapping should work.
 * \class 	egd_sgComputePlan_t
 *
 * A compute plan is a sequence of jobs that should be executed.
 * Each job describes the iteration of a single property.
 *
 * There is no guarantee about the order in which the jobs are inserted.
 */
class egd_sgComputePlan_t
{
	/*
	 * =====================
	 * Typedef
	 */
public:
	using GridContainer_t 		= egd_gridContainer_t;			//!< This is the grid container.
	using MarkerCollection_t 	= GridContainer_t::MarkerCollection_t;	//!< This is the parker collection.
	using ComputeJob_t 		= egd_sgComputeJob_t;			//!< Object that encodes one job.

	using DiffConstant_t 		= ComputeJob_t::DiffConstant_t;		//!< This is the diffusion constant.
	using GridProperty_t 		= ComputeJob_t::GridProperty_t;		//!< This is a grid property.
	using PropIdx_t 		= ComputeJob_t::PropIdx_t;		//!< This is the property index.

	using JobList_t 		= ::pgl::pgl_list_t<ComputeJob_t>;	//!< This is list of single jobs that is executed.
	using const_iterator 		= JobList_t::const_iterator;		//!< Iterator type.
	using iterator 			= const_iterator;			//!< Constant iterator.


	/*
	 * ========================
	 * Constructor
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted.
	 */
	egd_sgComputePlan_t()
	 noexcept
	 = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted
	 */
	egd_sgComputePlan_t(
		const egd_sgComputePlan_t&)
	 = delete;


	/**
	 * \brief	Copy assignment
	 *
	 * Is deleted
	 */
	egd_sgComputePlan_t&
	operator= (
		const egd_sgComputePlan_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_sgComputePlan_t(
		egd_sgComputePlan_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_sgComputePlan_t&
	operator= (
		egd_sgComputePlan_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_sgComputePlan_t()
	 = default;



	/*
	 * ======================
	 * Plan Managing Functions
	 */
public:
	/**
	 * \brief	This function adds the job to *this.
	 *
	 * This function adds the valid job to *this.
	 * Note that no guarantee about the order is done.
	 *
	 * \param  job 		The new job that should be added
	 */
	void
	addJob(
		ComputeJob_t&& 		job)
	{
		if(job.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Tried to add an invalid job to the list.");
		};

		//Test if the job is already there
		for(const auto& oldJob : m_jobs)
		{
			if(oldJob == job)
			{
				throw PGL_EXCEPT_InvArg("Tryed to add a job twice.");
			};
		}; //End for(job): test if already tehre

		//Add the job at the back of the current list.
		m_jobs.emplace_back(std::move(job));
			pgl_assert(m_jobs.back().isValid(),
				   job.isValid() == false);

		//Sort the list, this will make that all grids are together.
		m_jobs.sort();	//Uses <, which is overloaded to work as we want.

		return;
	}; //End: addJob


	/**
	 * \brief	This function directly constructs a compute
	 * 		 job and inserts it to *this.
	 *
	 * This function is the same as the respective constructor.
	 *
	 * \param  mProp	The marker property that should be updated.
	 * \param  wUpdate	Marker property to update the interpolation weights.
	 * \param  diffConst	The diffusion constant.
	 * \param  gProp	The grid property that should be used.
	 * \param  oldField	The old property field.
	 * \param  absInterpol	Perform absoplute interpolation.
	 */
	void
	addJob(
		const PropIdx_t& 				mProp,
		const PropIdx_t& 				wUpdate,
		DiffConstant_t&& 				diffConst,
		std::reference_wrapper<GridProperty_t> 		gProp,
		const GridProperty_t* 				oldField,
		const bool 					absInterpol = false)
	{
		this->addJob(ComputeJob_t(mProp, wUpdate, std::move(diffConst), gProp, oldField, absInterpol));
		return;
	}; //End: addJob directly construct


	/**
	 *
	 * \brief	This function directly constructs a compute
	 * 		 job and inserts it to *this.
	 *
	 * See the respective constructor for more.
	 * This function will take ownership of the change.
	 *
	 * \param  mProp	The marker property that should be updated.
	 * \param  wUpdate	Marker property to update the interpolation weights.
	 * \param  diffConst	The diffusion constant.
	 * \param  gProp	The grid property that should be used.
	 * \param  oldFiled	This is the old property field.
	 * \param  absInterpol	Perform absolute interpolation.
	 */
	void
	addJob(
		const PropIdx_t& 			mProp,
		const PropIdx_t& 			wUpdate,
		DiffConstant_t&& 			diffConst,
	 	std::unique_ptr<GridProperty_t>& 	gProp,
	 	const GridProperty_t* const 		oldField,
	 	const bool 				absInterpol = false)
	{
		this->addJob(ComputeJob_t(mProp, wUpdate, std::move(diffConst), gProp, oldField, absInterpol));
		return;
	}; //End: addJob for ownership aquire.



	/*
	 * =======================
	 * Plan Status Functions
	 */
public:
	/**
	 * \brief	Returns the number of jobs.
	 */
	Index_t
	nJobs()
	 const
	 noexcept
	{
		return m_jobs.size();
	};


	/**
	 * \brief	Returns true if *this has jobs, i.e. not empty.
	 */
	bool
	hasJobs()
	 const
	 noexcept
	{
		return (m_jobs.empty()
			? false
			: true        );
	};


	/**
	 * \brief	Checks if *this is valid.
	 *
	 * This is the case if all jobs are valid.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		//Test all jobs
		for(const auto& job : m_jobs)
		{
			if(job.isValid() == false)	//Job is not valid
			{
				return false;
			};
		}; //ENd for(job):

		return true;
	}; //End: isValid



	/*
	 * ========================
	 * Iterating functions
	 */
public:
	/**
	 * \brief	Returns the iterator to the first job of *this.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return this->cbegin();
	};


	/**
	 * \brief	Explicit constant version for the first job.
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
			pgl_assert(hasJobs());
		return this->m_jobs.cbegin();
	};


	/**
	 * \brief	Return the past the end iterator to the job sequence.
	 *
	 * Explicit version.
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
			pgl_assert(hasJobs());
		return this->m_jobs.cend();
	};


	/**
	 * \brief	Return the past the end iterator to the job sequence.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return this->cend();
	};



	/*
	 * ========================
	 * Private Members
	 */
private:
	JobList_t 		m_jobs;		//!< Stores the different jobs that should be executed.
}; //End class(egd_sgComputePlan_t):



PGL_NS_END(egd)

