/**
 * \brief	This file implements some functions of thew linear mometum test setup.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_linMomentum.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;


std::string
egd_markerSetUpLinMomentum_t::print()
 const
{
	return (std::string("Linear Momentum Test.")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " Vx = " + std::to_string(this->m_velXBall       ) + "m/s;"
			+ " Vy = " + std::to_string(this->m_velYBall       ) + "m/s;"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpLinMomentum_t::PropList_t
egd_markerSetUpLinMomentum_t::hook_creatMarkerProperties()
 const
{
	/*
	 * Create the default list.
	 * Do not include positions and type, this is done by the driver code.
	 */
	PropList_t props = { PropIdx::Density(), PropIdx::Viscosity(),
		             PropIdx::VelX(),    PropIdx::VelY()      };

	return props;
}; //End: make makrker property


void
egd_markerSetUpLinMomentum_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	dumper.addConstantMarkerProperty(PropIdx::Density()   )
	      .addConstantMarkerProperty(PropIdx::Viscosity() )
	      .addConstantMarkerProperty(PropIdx::Type()      );

	dumper.noTempSol();


	return;
}; //End: marker


void
egd_markerSetUpLinMomentum_t::inspectSolver(
		StokesSolver_i* const 		solver)
{
		pgl_assert(solver != nullptr);
	this->Base_t::inspectSolver(solver);

	//Load the property
	const Numeric_t xStart  = this->xDomStart();
	const Numeric_t xLength = this->xDomLength();
	const Numeric_t yStart  = this->yDomStart();
	const Numeric_t yLength = this->yDomLength();

	//the centere of the ball is in the middle
	const Numeric_t xCentre = xStart + xLength - (1.2 * m_radBall);
		pgl_assert((xCentre + m_radBall) <= (xLength + xStart),
			   xStart                <= (xCentre - m_radBall));

	//the y centree is above the lower level
	const Numeric_t yCentre = yStart + 0.5 * yLength;


	/*
	 * Configure the solver.
	 * Ignore the success state.
	 */
	(void)(solver->setBoundaryOption("x0", xCentre));
	(void)(solver->setBoundaryOption("y0", yCentre));

	return;
}; //End: inspect solver




bool
egd_markerSetUpLinMomentum_t::hool_setMarkerProperties(
	MarkerCollection_t& 		mColl)
 const
{
	using pgl::isValidFloat;

	if(isValidFloat(this->m_velXBall) == false)
	{
		throw PGL_EXCEPT_InvArg("The x velocity is invalid.");
	};
	if(isValidFloat(this->m_velYBall) == false)
	{
		throw PGL_EXCEPT_InvArg("The y velocity is invalid.");
	};


	/*
	 * Some helper functions/ claculations
	 */

	//Now we load all properties that we need
	      MarkerProperty_t& RHO   = mColl.getMarkerProperty(PropIdx::Density()   );
	      MarkerProperty_t& ETA   = mColl.getMarkerProperty(PropIdx::Viscosity() );
	      MarkerProperty_t& VELX  = mColl.getMarkerProperty(PropIdx::VelX()      );
	      MarkerProperty_t& VELY  = mColl.getMarkerProperty(PropIdx::VelY()      );
	const MarkerProperty_t& TYPE  = mColl.getMarkerProperty(PropIdx::Type()      );

	//Get the position of the markers
	const MarkerPositions_t& X = mColl.cgetXPos();
	const MarkerPositions_t& Y = mColl.cgetYPos();

	const Index_t nMarkers = X.size();

	/*
	 * Iterating through all the markers and set them
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load the positions
		const Numeric_t xm = X[m];
		const Numeric_t ym = Y[m];

		//Determine the type of the material
		const Index_t mType = Index_t(TYPE[m]);
			pgl_assert(Index_t(mType) == this->hook_findMaterial(xm, ym));

		//We will now set the velocity to zero
		VELX[m] = 0.0;
		VELY[m] = 0.0;

		//Set the specific stuff
		switch(mType)
		{
		  case TY_STICKY_AIR:
		  	RHO [m] = m_rhoAir;
		  	ETA [m] = m_etaAir;
		  	//Velocities are set to zero.
		  break;

		  case TY_BALL:
		  	RHO [m] = m_rhoBall;
		  	ETA [m] = m_etaBall;
		  	VELY[m] = m_velYBall;
		  	VELX[m] = m_velXBall;
		  break;

		  default:
		  	throw PGL_EXCEPT_InvArg("Computed an invalid material type.");
		}; //End switch(mType):
	}; //End for(m):

	return true;
}; //End: default for setting up the problem geometry


Index_t
egd_markerSetUpLinMomentum_t::hook_findMaterial(
	const Numeric_t 	x_m,
	const Numeric_t 	y_m)
 const
{
	if(x_m < this->xDomStart() )
	{
		throw PGL_EXCEPT_InvArg("X position is too small.");
	};
	if((this->xDomStart() + this->xDomLength() ) < x_m)
	{
		throw PGL_EXCEPT_InvArg("X position is too large.");
	};
	if(y_m < this->yDomStart() )
	{
		throw PGL_EXCEPT_InvArg("Y position is too small.");
	};
	if((this->yDomStart() + this->yDomLength() ) < y_m)
	{
		throw PGL_EXCEPT_InvArg("Y position is too large.");
	};

	//Load the property
	const Numeric_t xStart  = this->xDomStart();
	const Numeric_t xLength = this->xDomLength();
	const Numeric_t yStart  = this->yDomStart();
	const Numeric_t yLength = this->yDomLength();
		pgl_assert(isValidFloat(m_radBall), m_radBall > 0.0,
						    m_radBall < std::min(yLength, xLength));

	//the centere of the ball is in the middle
	const Numeric_t xCentre = xStart + xLength - (1.2 * m_radBall);
		pgl_assert((xCentre + m_radBall) <= (xLength + xStart),
			   xStart                <= (xCentre - m_radBall));

	//the y centree is above the lower level
	const Numeric_t yCentre = yStart + 0.5 * yLength;


	//calculate the square distance from the ball centre
	const Numeric_t dist2   = std::pow(xCentre - x_m, 2.0) + std::pow(yCentre - y_m, 2.0);
		pgl_assert(isValidFloat(dist2), dist2 >= 0.0);

	/* Test if it is a ball */
	if(dist2 < (m_radBall * m_radBall))
	{
		return TY_BALL;
	}

	return TY_STICKY_AIR;
	(void)yLength;
}; //End: find location


PGL_NS_END(egd)

