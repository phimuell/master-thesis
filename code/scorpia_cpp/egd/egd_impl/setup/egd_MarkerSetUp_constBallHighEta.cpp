/**
 * \brief	This file implements functions for the constant ball with high eta.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_constBallHighEta.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;


std::string
egd_markerSetUpConstBallHighEta_t::print()
 const
{
	return (std::string("Const Ball Test High Eta.")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " Vx = " + std::to_string(this->m_velXBall       ) + "m/s;"
			+ " Vy = " + std::to_string(this->m_velYBall       ) + "m/s;"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print

void
egd_markerSetUpConstBallHighEta_t::hook_setUpInternals()
{
	/* Call the base version */
	this->Base_t::hook_setUpInternals();

	/* Modify viscosity */
	this->m_etaAir *= 10.0;

	return;
}; //End: set initial values




/*
 * =======================
 * Constructors
 */

egd_markerSetUpConstBallHighEta_t::egd_markerSetUpConstBallHighEta_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		Vy,
	const Numeric_t 		Vx,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 Vy, Vx,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpConstBallHighEta_t::egd_markerSetUpConstBallHighEta_t()
 :
  Base_t()
{}; //End: default constructor


egd_markerSetUpConstBallHighEta_t::egd_markerSetUpConstBallHighEta_t(
	const egd_confObj_t& 		confObj)
 :
  Base_t(confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
}; // End: building constructor


egd_markerSetUpConstBallHighEta_t::~egd_markerSetUpConstBallHighEta_t()
 = default;


egd_markerSetUpConstBallHighEta_t::egd_markerSetUpConstBallHighEta_t(
	const egd_markerSetUpConstBallHighEta_t&)
 = default;


egd_markerSetUpConstBallHighEta_t&
egd_markerSetUpConstBallHighEta_t::operator= (
	const egd_markerSetUpConstBallHighEta_t&)
 = default;


egd_markerSetUpConstBallHighEta_t::egd_markerSetUpConstBallHighEta_t(
	egd_markerSetUpConstBallHighEta_t&&)
 noexcept
 = default;


egd_markerSetUpConstBallHighEta_t&
egd_markerSetUpConstBallHighEta_t::operator= (
	egd_markerSetUpConstBallHighEta_t&&)
 = default;


PGL_NS_END(egd)

