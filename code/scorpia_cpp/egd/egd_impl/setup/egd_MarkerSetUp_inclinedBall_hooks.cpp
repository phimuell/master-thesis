/**
 * \brief	This file implements the hooks of the inclined ball setting.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_inclinedBall.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;



void
egd_markerSetUpInclinedBall_t::hook_setUpInternals()
{
using ::pgl::isValidFloat;

	/*	 BALL	 */
	m_etaBall  = 		1000.0 ;		//~Lard
	m_rhoBall  = 		 865.64;		//https://www.aqua-calc.com/page/density-table/substance/lard
	m_radBall  = 		   0.20;		//In meters


	/*	AIR	*/
	m_etaAir  = 		   18.5e-4;		// 100 times the real viscosity Pa s
	m_rhoAir  = 		    1.2   ;		//Real density in kg / m^3

		pgl_assert(isValidFloat(m_angleBall),
			   isValidFloat(m_velBall  ) );
	return;
}; //End: set initial values


egd_markerSetUpInclinedBall_t::PropList_t
egd_markerSetUpInclinedBall_t::hook_creatMarkerProperties()
 const
{
	/*
	 * Create the default list.
	 * Do not include positions and type, this is done by the driver code.
	 */
	PropList_t props = { PropIdx::Density(), PropIdx::Viscosity(),
		             PropIdx::VelX(),    PropIdx::VelY()      };

	return props;
}; //End: make makrker property


void
egd_markerSetUpInclinedBall_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	//All makrer properties are assumed to be constant, note that
	// velocity is not.
	dumper.addConstantMarkerProperty(PropIdx::Density()   )
	      .addConstantMarkerProperty(PropIdx::Viscosity() )
	      .addConstantMarkerProperty(PropIdx::Type()      );

	//We do not consider a temperature.
	dumper.noTempSol();

	return;
}; //End: marker


bool
egd_markerSetUpInclinedBall_t::hool_setMarkerProperties(
	MarkerCollection_t& 		mColl)
 const
{
	using pgl::isValidFloat;

	/*
	 * Some helper functions/ claculations
	 */

	//Now we load all properties that we need
	      MarkerProperty_t& RHO   = mColl.getMarkerProperty(PropIdx::Density()   );
	      MarkerProperty_t& ETA   = mColl.getMarkerProperty(PropIdx::Viscosity() );
	      MarkerProperty_t& VELX  = mColl.getMarkerProperty(PropIdx::VelX()      );
	      MarkerProperty_t& VELY  = mColl.getMarkerProperty(PropIdx::VelY()      );
	const MarkerProperty_t& TYPE  = mColl.getMarkerProperty(PropIdx::Type()      );

	//Get the position of the markers
	const MarkerPositions_t& X = mColl.cgetXPos();
	const MarkerPositions_t& Y = mColl.cgetYPos();

	const Index_t nMarkers = X.size();

	/*
	 * Here we compute the velocity. Since the velocity
	 * is the same for all markers of the ball, be can
	 * do this.
	 * Note that the angle value was already modified
	 * upon construction, such that it can be feed
	 * to the trigonometric functions.
	 */
	const Numeric_t mVelBallY = m_velBall * std::sin(m_angleBall);		pgl_assert(isValidFloat(mVelBallY));
	const Numeric_t mVelBallX = m_velBall * std::cos(m_angleBall);		pgl_assert(isValidFloat(mVelBallX));

	/*
	 * Iterating through all the markers and set them
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load the positions
		const Numeric_t xm = X[m];
		const Numeric_t ym = Y[m];

		//Determine the type of the material
		const Index_t mType = Index_t(TYPE[m]);
			pgl_assert(Index_t(mType) == this->hook_findMaterial(xm, ym));

		//We will now set the velocity to zero

		//Set the specific stuff
		switch(mType)
		{
		  case TY_STICKY_AIR:
		  	RHO [m] = m_rhoAir;
		  	ETA [m] = m_etaAir;
			VELX[m] = 0.0;		//Fluid has no velocity
			VELY[m] = 0.0;
		  break;

		  case TY_BALL:
		  	RHO [m] = m_rhoBall;
		  	ETA [m] = m_etaBall;
		  	VELX[m] = mVelBallX;
		  	VELY[m] = mVelBallY;
		  break;

		  default:
		  	throw PGL_EXCEPT_InvArg("Computed an invalid material type.");
		}; //End switch(mType):
	}; //End for(m):

	return true;
}; //End: default for setting up the problem geometry


Index_t
egd_markerSetUpInclinedBall_t::hook_findMaterial(
	const Numeric_t 	x_m,
	const Numeric_t 	y_m)
 const
{
	if(x_m < this->xDomStart() )
	{
		throw PGL_EXCEPT_InvArg("X position is too small.");
	};
	if((this->xDomStart() + this->xDomLength() ) < x_m)
	{
		throw PGL_EXCEPT_InvArg("X position is too large.");
	};
	if(y_m < this->yDomStart() )
	{
		throw PGL_EXCEPT_InvArg("Y position is too small.");
	};
	if((this->yDomStart() + this->yDomLength() ) < y_m)
	{
		throw PGL_EXCEPT_InvArg("Y position is too large.");
	};
		pgl_assert(isValidFloat(m_radBall), m_radBall > 0.0);

	//Load the property
	const Numeric_t xStart  = this->xDomStart();
	const Numeric_t xLength = this->xDomLength();
	const Numeric_t yStart  = this->yDomStart();
	const Numeric_t yLength = this->yDomLength();

	//this is the safty factor that seperates the ball from the wall
	//despite its name it is a distance
	const Numeric_t saftyFactor = 0.4 * m_radBall;

	//This are the centre of the balls
	const Numeric_t xCentre =  xStart + (saftyFactor + m_radBall);
	const Numeric_t yCentre = -(yStart + yLength) + (saftyFactor + m_radBall);

	//calculate the square distance from the ball centre
	const Numeric_t dist2   = std::pow(xCentre - x_m, 2.0) + std::pow(yCentre - y_m, 2.0);
		pgl_assert(isValidFloat(dist2), dist2 >= 0.0);

	/* Test if it is a ball */
	if(dist2 < (m_radBall * m_radBall))
	{
		return TY_BALL;
	}

	return TY_STICKY_AIR;
	(void)yLength;
	(void)xLength;
}; //End: find location


PGL_NS_END(egd)

