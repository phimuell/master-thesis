#pragma once
/**
 * \brief	This class implements a rotating disc.
 *
 * The disc rotates in a medium with a very low viscosity.
 * The angular momentum can be comapred to an analytic value.
 *
 * WIth a compile switch it can be selected if some parts of the
 * fluid, that are in the vincinity of the disc, should also
 * rotate initially.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUpBase.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

/**
 * \brief	Define this macro to a non zero value, to enable a rotating fluid.
 * \define 	EGD_SETUP_ROT_NO_FLUID
 *
 * This will deactivate the rotating fluid. This means that the fluid will be totaly
 * at rest initially and not have a region, where it rotates.
 */
#if !defined(EGD_SETUP_ROT_NO_FLUID)
#	define EGD_SETUP_ROT_NO_FLUID 1
#endif


/**
 * \brief	This macro controles if the wall should be composed out of markers with high density
 * \define 	EGD_SETUP_ROT_REALWALL
 *
 * Seeting this macro to a non zero value, will generate a real wall.
 * This means that the markers with high densities and viscosity are placed at the boundary.
 *
 * If the value is zero, then the wall will be replaced by fluid, which realy exists.
 * However it will not rotate.
 *
 * The default is zero.
 */
#if !defined(EGD_SETUP_ROT_REALWALL)
#	define 	EGD_SETUP_ROT_REALWALL 0
#endif



/**
 * \class 	egd_markerSetUpRotDisc_t
 * \brief	This class creates the rotating disc scenario.
 */
class egd_markerSetUpRotDisc_t : public egd_markerSetUpBase_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_markerSetUpBase_t;			//!< This is the base implementation
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::PropIdx_t;
	using Base_t::PropList_t;
	using Base_t::PropToGridMap_t;

	using Base_t::MarkerCollection_t;
	using Base_t::MarkerPositions_t;
	using Base_t::MarkerProperty_t;
	using Base_t::CreationResult_t;

	/*
	 * Brief	This are the name of the different materials we have.
	 */
	const static Index_t TY_STICKY_AIR 	= 0;		//This is the air
	const static Index_t TY_DISC            = 1;		//This is the disc
	const static Index_t TY_WALL            = 2;		//This is the wall
	const static Index_t TY_N 		= 3;


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_markerSetUpRotDisc_t();


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * Note that it will place the centre of the disc at the
	 * origin of the coordiante system.
	 * The constructor allows to set the angular velocity of the ball.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  angularVel	The angular velocity.
	 * \param  markerRand	Displace the markers randomly.
	 */
	egd_markerSetUpRotDisc_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const Index_t 		nMarkerY,
		const Index_t 		nMarkerX,
		const Numeric_t 	angularVel,
		const bool 		markerRand,
		const egd_confObj_t* const 	confObj = nullptr);



	/**
	 * \brief	Building constructor with
	 * 		 config object.
	 *
	 * This constructor will examine the.
	 *
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_markerSetUpRotDisc_t(
		const egd_confObj_t&	confObj);



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDisc_t(
		const egd_markerSetUpRotDisc_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDisc_t&
	operator= (
		const egd_markerSetUpRotDisc_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_markerSetUpRotDisc_t(
		egd_markerSetUpRotDisc_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDisc_t&
	operator= (
		egd_markerSetUpRotDisc_t&&);


	/*
	 * =====================
	 * Protected Constructors
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is only used internaly. The result
	 * of this constructor, is an invalid
	 * object.
	 */
	egd_markerSetUpRotDisc_t();



	/*
	 * =========================
	 * Query Functions
	 */
public:



	/*
	 * =========================
	 * Interface Functions
	 */
public:
	using Base_t::creatProblem;


	/**
	 * \brief	This function outputs information about
	 * 		 the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/**
	 * \brief	This function will ignore Density and viscosity on
	 * 		 the markers.
	 *
	 * \param  dumper	This si is the dumper.
	 */
	virtual
	void
	inspectDumper(
		egd_dumperFile_t& 	dumper)
	 const
	 final;


	/*
	 * ============================
	 * Hooks
	 */
protected:
	using Base_t::hook_creatMarkerPositions;
	using Base_t::hook_creatGridGeometry;
	using Base_t::hook_creatGridPropertyList;
	using Base_t::hook_postProcessGridProperties;


	/*
	 * =============================
	 * Needed Hooks
	 */
protected:
	/**
	 * \brief	This function creates the property list of the markers.
	 *
	 * This will add all the needed properties.
	 *
	 */
	virtual
	PropList_t
	hook_creatMarkerProperties()
	 const
	 final;


	/**
	 * \brief	This function determines in which material the given point has.
	 *
	 * \param  x	The x coordinate of the position.
	 * \param  y 	The y coordinate of the position.
	 */
	virtual
	Index_t
	hook_findMaterial(
		const Numeric_t 	x,
		const Numeric_t 	y)
	 const
	 final;


	/**
	 * \brief	This hook is called to fill the marker
	 * 		 property collection.
	 *
	 * This function is called after the marker collection is
	 * build. It should set the markers properties to theier
	 * initial values.
	 *
	 * Note that the type property is allready set by the
	 * driver code. So the user can use them to simplyify
	 * implementation.
	 *
	 * \param  mColl 	The collection that should be set.
	 */
	virtual
	bool
	hool_setMarkerProperties(
		MarkerCollection_t& 		mColl)
	 const
	 final;





	/*
	 * ===============================
	 * Interface helper
	 *
	 * These are functions that are provided by the interface
	 * They are non virtual
	 */
protected:
	using Base_t::getTempGrid;
	using Base_t::wasTempSolverInspected;
	using Base_t::getStokeGrid;
	using Base_t::wasStokesSolverInspected;
	using Base_t::isTempOnExtGrid;
	using Base_t::isStokeOnExtGrid;
	using Base_t::getSolverPropMap;



	/*
	 * =======================
	 * Internal Access functions
	 *
	 * These functions allows to access some private
	 * variables of *this.
	 */
protected:
	using Base_t::xDomStart;
	using Base_t::yDomStart;
	using Base_t::xDomLength;
	using Base_t::yDomLength;
	using Base_t::gridPointsX;
	using Base_t::gridPointsY;
	using Base_t::markerDensityX;
	using Base_t::markerDensityY;
	using Base_t::randDistribution;


	/*
	 * ====================
	 * Material hook
	 */
protected:
	/**
	 * \brief	This function sets up the internal state of *this.
	 *
	 * This functin will set all internal parameters to their default value.
	 * With teh exception of the angular momentum.
	 * If this value is nan an error is generated.
	 */
	virtual
	void
	hook_setUpInternals()
	 override;



	/*
	 * ==========================
	 * Helper functions
	 */
protected:
	/**
	 * \brief	This function returns thr outer radious.
	 *
	 * There the wall begins.
	 */
	Numeric_t
	getOuterRadius()
	 const;



	/*
	 * ============================
	 * Protected Variable
	 *
	 * They are protected, such that deriving classes can access them.
	 */
protected:
	//Ball
	Numeric_t 		m_etaBall    = NAN;		//!< This is the viscosity of the ball.
	Numeric_t 		m_rhoBall    = NAN;		//!< Density of the ball.
	Numeric_t 		m_radBall    = NAN;		//!< Radius f the ball.
	Numeric_t 		m_angularVel = NAN;		//!< Angular momentum.

	//Air
	Numeric_t 		m_etaAir   = NAN;		//!< This is the viscosity of the ball.
	Numeric_t 		m_rhoAir   = NAN;		//!< This is teh density of the ball.
}; //End: class(egd_markerSetUpRotDisc_t)

PGL_NS_END(egd)

