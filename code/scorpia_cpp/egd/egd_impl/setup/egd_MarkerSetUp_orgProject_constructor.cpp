/**
 * \brief	This file contains the implementation of the code that
 * 		 Sets up the original problem.
 *
 * This file contains the constructors.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_orgProject.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0)
#	pragma message "Pressure scaling is disabled, this setting needs it."
#endif

namespace
{
/**
 * \brief	This function sets all rocks entries to the given value
 *
 * Since the air is special the value NAN can be used.
 * This will not write nan into the positioon, but zero.
 * This is done to indicate that the value of the AIR is not important.
 *
 * \param  pVectore	The parameter vector that should be maipulated.
 * \param  plateValue	The value that should be used, for the not mantle rocks.
 * \param  mantleValue	This is the value of the mantel.
 * \param  airValue 	Which value should be assigned to the air.
 * 			 If omitted zero will be written.
 */
void
setParamVec(
	egd_markerSetUpOrgProj_t::ParamVector_t* const 		pVector,
	egd_markerSetUpOrgProj_t::Numeric_t 			plateValue,
	egd_markerSetUpOrgProj_t::Numeric_t 			mantleValue,
	egd_markerSetUpOrgProj_t::Numeric_t 			airValue)
{
	pgl_assert(pVector != nullptr);
	pgl_assert(::pgl::isValidFloat(plateValue ),
		   ::pgl::isValidFloat(mantleValue) );
	pgl_assert(pVector->size() == egd_markerSetUpOrgProj_t::TY_N);

	for(Index_t it  = egd_markerSetUpOrgProj_t::TY_ROCK_START;
		    it != egd_markerSetUpOrgProj_t::TY_END       ;
		  ++it 						     )
	{
		//Do not handle the mantle here.
		if(it == egd_markerSetUpOrgProj_t::TY_ROCK_MANTLE)
		{
			continue;
		};

		//Set the value, with the exception it is the mantle
		(*pVector)[it] = plateValue;
	}; //End for(it):

	//Set the mantel
	(*pVector)[egd_markerSetUpOrgProj_t::TY_ROCK_MANTLE] = mantleValue;

	//Set the air value if needed
	if(::pgl::isValidFloat(airValue) == true)
	{
		(*pVector)[egd_markerSetUpOrgProj_t::TY_STICKY_AIR] = airValue;
	}
	else
	{
		(*pVector)[egd_markerSetUpOrgProj_t::TY_STICKY_AIR] = 0.0;
	};

	return;
}; //End: setParamVec
}; //End: anyonmus namespace


egd_markerSetUpOrgProj_t::egd_markerSetUpOrgProj_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const Numeric_t 	yLengthM,
	const Numeric_t 	xLengthM,
	const Index_t 		nMarkerY,
	const Index_t 		nMarkerX,
	const bool 		markerRand)
 :
  Base_t(Ny, Nx,
  	 yLengthM, xLengthM,
  	 nMarkerY, nMarkerX,
  	 markerRand)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpOrgProj_t::egd_markerSetUpOrgProj_t()
 :
  Base_t(yNodeIdx_t(81), xNodeIdx_t(101),
  	400.0 * 1000, 500.0 * 1000,
  	5, 5, false)
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpOrgProj_t::egd_markerSetUpOrgProj_t(
	const egd_confObj_t& 		confObj)
 :
  Base_t(confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpOrgProj_t::~egd_markerSetUpOrgProj_t()
 = default;


egd_markerSetUpOrgProj_t::egd_markerSetUpOrgProj_t(
	const egd_markerSetUpOrgProj_t&)
 = default;


egd_markerSetUpOrgProj_t&
egd_markerSetUpOrgProj_t::operator= (
	const egd_markerSetUpOrgProj_t&)
 = default;


egd_markerSetUpOrgProj_t::egd_markerSetUpOrgProj_t(
	egd_markerSetUpOrgProj_t&&)
 noexcept
 = default;


egd_markerSetUpOrgProj_t&
egd_markerSetUpOrgProj_t::operator= (
	egd_markerSetUpOrgProj_t&&)
 = default;


void
egd_markerSetUpOrgProj_t::hook_setUpInternals()
{
	/*
	 * Set the parameter vectors
	 */
	setParamVec(&m_initCp   , 1'000.0, 1'000.0, 3'300'000.0);
	setParamVec(&m_initRho  , 3'400.0, 3'250.0,         1.0);
	setParamVec(&m_initEta  ,   1e+23,   1e+20,       1e+18);
	setParamVec(&m_initAlpha,    3e-5,   3e-5 ,         NAN);
	setParamVec(&m_initBeta ,   1e-11,   1e-11,         NAN);


	/*
	 * Set the termal conductivity of teh air.
	 * This one is fix.
	 */
	m_initKAir = 3'000.0;


	/*
	 * Set the temperature
	 */
	m_initTemp_Air         =  273.0;
	m_initTemp_Mantle      = 1573.0;
	m_initTemp_plateTop    =  273.0;
	m_initTemp_plateBottom = 1573.0;

	return;
}; //End: set initial values

PGL_NS_END(egd)

