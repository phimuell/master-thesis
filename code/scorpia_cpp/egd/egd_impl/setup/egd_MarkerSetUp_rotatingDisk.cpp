/**
 * \brief	This file contains functions to set up the rotating disc functionality.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_rotatingDisk.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;


#if defined(EGD_SETUP_ROT_NO_FLUID) && (EGD_SETUP_ROT_NO_FLUID != 0)
#	pragma message "Rotaing fluid is enabled."
#else
#	pragma message "Rotaing fluid is disabled."
#endif

#if defined(EGD_SETUP_ROT_REALWALL) && (EGD_SETUP_ROT_REALWALL != 0)
#	pragma message "Will create a real wall in the rotating scenario."
#else
#	pragma message "No wall will be created in the rotating case."
#endif




std::string
egd_markerSetUpRotDisc_t::print()
 const
{
	return (
#			if defined(EGD_SETUP_ROT_NO_FLUID) && (EGD_SETUP_ROT_NO_FLUID != 0)
			std::string("Rotating ball (fluid also rotates)")
#			else
			std::string("Rotating ball (fluid doesn't rotate)")
#			endif
#			if defined(EGD_SETUP_ROT_REALWALL) && (EGD_SETUP_ROT_REALWALL != 0)
			+ std::string(", wall is enabled.")
#			else
			+ std::string(", wall is disabled.")
#			endif
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " aV = " + std::to_string(m_angularVel           ) + " 1/s;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpRotDisc_t::Numeric_t
egd_markerSetUpRotDisc_t::getOuterRadius()
 const
{
using std::fmin;

	//this is the outer radius
	const Numeric_t outerRadius = (0.5 * 0.90) * fmin(this->xDomLength(), this->yDomLength());
		pgl_assert(pgl::isValidFloat(outerRadius),
			   outerRadius > this->m_radBall);

	return outerRadius;
}; //End: outer radius


egd_markerSetUpRotDisc_t::PropList_t
egd_markerSetUpRotDisc_t::hook_creatMarkerProperties()
 const
{
	/*
	 * Create the default list.
	 * Do not include positions and type, this is done by the driver code.
	 */
	PropList_t props = { PropIdx::Density(), PropIdx::Viscosity(),
		             PropIdx::VelX(),    PropIdx::VelY()      };

	return props;
}; //End: make makrker property


void
egd_markerSetUpRotDisc_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	dumper.addConstantMarkerProperty(PropIdx::Density()   )
	      .addConstantMarkerProperty(PropIdx::Viscosity() )
	      .addConstantMarkerProperty(PropIdx::Type()      );

	dumper.noTempSol();


	return;
}; //End: marker


bool
egd_markerSetUpRotDisc_t::hool_setMarkerProperties(
	MarkerCollection_t& 		mColl)
 const
{
	using pgl::isValidFloat;

	/*
	 * Some helper functions/ claculations
	 */

	//Now we load all properties that we need
	      MarkerProperty_t& RHO   = mColl.getMarkerProperty(PropIdx::Density()   );
	      MarkerProperty_t& ETA   = mColl.getMarkerProperty(PropIdx::Viscosity() );
	      MarkerProperty_t& VELX  = mColl.getMarkerProperty(PropIdx::VelX()      );
	      MarkerProperty_t& VELY  = mColl.getMarkerProperty(PropIdx::VelY()      );
	const MarkerProperty_t& TYPE  = mColl.getMarkerProperty(PropIdx::Type()      );

	//Get the position of the markers
	const MarkerPositions_t& X = mColl.cgetXPos();
	const MarkerPositions_t& Y = mColl.cgetYPos();

	//this is the number of markers
	const Index_t   nMarkers = X.size();	//Numbers of markers

#	if defined(EGD_SETUP_ROT_NO_FLUID) && (EGD_SETUP_ROT_NO_FLUID != 0)
	const Numeric_t outerRadius = this->getOuterRadius();		//distance from centre to wall
	const Numeric_t innerRadius = this->m_radBall;			//inner radius (disc)

	/*
	 * See: https://en.wikipedia.org/wiki/Taylor%E2%80%93Couette_flow#Flow_description
	 *
	 * With
	 * 	R_1 ~ innerRadius
	 * 	R_2 ~ outerRadius
	 * 	\Omega_1 ~ angularVell
	 * 	\Omega_2 ~ 0
	 */
	const Numeric_t velETA  = innerRadius / outerRadius;
	const Numeric_t velETA2 = velETA * velETA;
	const Numeric_t velMU   = 0.0;		//outer radius is zero
	const Numeric_t velOM1  = this->m_angularVel;
	const Numeric_t velA    = velOM1 * ((velMU - velETA2) / (1.0 - velETA2));
	const Numeric_t velB    = velOM1 * (innerRadius * innerRadius) * ((1.0 - velMU) / (1 - velETA2));
# 	endif

	/*
	 * Iterating through all the markers and set them
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		const Numeric_t xm = X[m];		//Load the positions
		const Numeric_t ym = Y[m];

		const Numeric_t d2 = (xm * xm) + (ym * ym); 	//square distance from middle
		const Numeric_t d  = std::sqrt(d2);		//distance from the middle

		//Determine the type of the material
		const Index_t mType = Index_t(TYPE[m]);
			pgl_assert(Index_t(mType) == this->hook_findMaterial(xm, ym));

		/*
		 * This is teh magnitude of the rotation.
		 * it have to be assigned by the switch statement
		 *
		 * The component are computed with sine and cosine, see below.
		 */
		Numeric_t velMagnitude = NAN;

		//Set the specific stuff
		switch(mType)
		{
		  case TY_STICKY_AIR:
		  	RHO [m] = m_rhoAir;
		  	ETA [m] = m_etaAir;

#			if defined(EGD_SETUP_ROT_NO_FLUID) && (EGD_SETUP_ROT_NO_FLUID != 0)
			/*
			 * The magnitude of the velocity is given by a Taylor–Couette flow
			 */
			velMagnitude = d * velA + velB / d;

#				if !(defined(EGD_SETUP_ROT_REALWALL) && (EGD_SETUP_ROT_REALWALL != 0))
				/*
				 * The wall is disabled, this means we must ensure that the value
				 * fluid outside the wall is not rotating. For that we just set the
				 * magnitude to zero, if the marker is a potential wall marker.
				 */
				if(d >= outerRadius)
				{
					velMagnitude = 0.0;
				};
#				endif

#			else
			velMagnitude = 0.0;
#			endif

		  break;

		  case TY_WALL:
		  	RHO[m] = 3200.0;
		  	ETA[m] = 1e+9;
#			if defined(EGD_SETUP_ROT_REALWALL) && (EGD_SETUP_ROT_REALWALL != 0)
			velMagnitude = 0.0;
#			else
			throw PGL_EXCEPT_RUNTIME("Encountered a wall marker, that should not exists.");
#			endif
		  break;

		  case TY_DISC:
		  	RHO[m] = m_rhoBall;
		  	ETA[m] = m_etaBall;
		  	velMagnitude = d * (this->m_angularVel);
		  break;

		  default:
		  	throw PGL_EXCEPT_InvArg("Computed an invalid material type.");
		}; //End switch(mType):

		/*
		 * For the minus in front of the vx velocity.
		 * Imagine the first quadrant with \phi \approx 45Deg
		 * There the velocity vector, “\” has a positive y component, but an negative x component.
		 *
		 *  A
		 *  |
		 *  |xx
		 *  |   xxx  \
		 *  |      xx \
		 *  |         x\
		 *  |           x
		 *  |            x
		 *  O---------------->
		 *
		 * So we have to use the minus in from of the calculation.
		 */
		//compute the angles
		const Numeric_t phi    = std::atan2(ym, xm);	//can also handle both zero
		const Numeric_t sinPhi = std::sin(phi);
		const Numeric_t cosPhi = std::cos(phi);
			pgl_assert(pgl::isValidFloat(velMagnitude));

		VELX[m] = - velMagnitude * sinPhi;	//Minus is important and swapped cos sin
		VELY[m] =   velMagnitude * cosPhi;
	}; //End for(m):

	return true;
}; //End: default for setting up the problem geometry


Index_t
egd_markerSetUpRotDisc_t::hook_findMaterial(
	const Numeric_t 	x_m,
	const Numeric_t 	y_m)
 const
{

	if(x_m < this->xDomStart() )
	{
		throw PGL_EXCEPT_InvArg("X position is too small.");
	};
	if((this->xDomStart() + this->xDomLength() ) < x_m)
	{
		throw PGL_EXCEPT_InvArg("X position is too large.");
	};
	if(y_m < this->yDomStart() )
	{
		throw PGL_EXCEPT_InvArg("Y position is too small.");
	};
	if((this->yDomStart() + this->yDomLength() ) < y_m)
	{
		throw PGL_EXCEPT_InvArg("Y position is too large.");
	};
		pgl_assert(isValidFloat(m_radBall), m_radBall > 0.0);

#	if defined(EGD_SETUP_ROT_REALWALL) && (EGD_SETUP_ROT_REALWALL != 0)
	//this is the outer radius
	const Numeric_t outerRadius = this->getOuterRadius();
#	endif

	//calculate the square distance from the ball centre
	const Numeric_t dist2   = (x_m * x_m) + (y_m * y_m);
		pgl_assert(isValidFloat(dist2), dist2 >= 0.0);

	if(dist2 < (m_radBall * m_radBall))
	{
		//it is the rotating disc
		return TY_DISC;
	}
#	if defined(EGD_SETUP_ROT_REALWALL) && (EGD_SETUP_ROT_REALWALL != 0)
	else if(dist2 >= (outerRadius * outerRadius))
	{
		//We are in the outer region, also known as wall
		return TY_WALL;
	}
#	endif
	else
	{
		return TY_STICKY_AIR;
	};
	pgl_assert(false && "Reached unreacable code.");
}; //End: find location


PGL_NS_END(egd)

