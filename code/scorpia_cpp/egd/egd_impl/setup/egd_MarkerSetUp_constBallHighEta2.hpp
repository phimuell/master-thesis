#pragma once
/**
 * \brief	This file declares a class for the setup of the constant moving ball, with a high eta of the media.
 * 		 But also with a special density
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUpBase.hpp"
#include "./egd_MarkerSetUp_constBall.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)


/**
 * \class 	egd_markerSetUpConstBallHighEta2_t
 * \brief	ConstBall setting, but with larger viscosity and a lower density.
 *
 *
 * The normal high density version of the constant ball has a ten times
 * larger viscosity than the normal one, this means that the RE is also
 * ten times smaller than it is in the normal version.
 * This setting has also a 10 times larger viscosity, but also a 10 times
 * larger fluid density. This means that the Reynolds number is the same
 * as it is in the original setting.
 *
 * The density of the ball is also increased by the same factor.
 *
 * \note	When I set up this setting I was confused and I thought that
 * 		 I have to lowert the density înstead of increasiong it.
 * 		 I have changed this at all points I am aware off, but it is
 * 		 possible that I have missed some points.
 */
class egd_markerSetUpConstBallHighEta2_t : public egd_markerSetUpConstBall_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_markerSetUpConstBall_t;			//!< This is the base implementation
		static_assert(std::is_same<Base_t, egd_markerSetUpConstBall_t>::value, "Must be directly derived from the constPulledBall.");
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::PropIdx_t;
	using Base_t::PropList_t;
	using Base_t::PropToGridMap_t;

	using Base_t::MarkerCollection_t;
	using Base_t::MarkerPositions_t;
	using Base_t::MarkerProperty_t;
	using Base_t::CreationResult_t;

	/*
	 * Brief	This are the name of the different materials we have.
	 */
	const static Index_t TY_STICKY_AIR 	= Base_t::TY_STICKY_AIR;
	const static Index_t TY_BALL            = Base_t::TY_BALL;
	const static Index_t TY_N 		= Base_t::TY_N;


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_markerSetUpConstBallHighEta2_t();


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * This constructor will setup the base implementation.
	 * It also will also set up the velocity.
	 *
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  Vy		The velocity in y direction.
	 * \param  Vx 		The velocity in x direction.
	 * \param  markerRand	Displace the markers randomly.
	 * \param  confObj	Optional pointer to the configuration object.
	 */
	egd_markerSetUpConstBallHighEta2_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const Index_t 			nMarkerY,
		const Index_t 			nMarkerX,
		const Numeric_t 		Vy,
		const Numeric_t 		Vx,
		const bool 			markerRand,
		const egd_confObj_t* const  	confObj = nullptr);



	/**
	 * \brief	Building constructor with
	 * 		 config object.
	 *
	 * Will be forwarded to the base's constructor
	 * and set up the paramters of this.
	 *
	 * It will read in the velocity from file.
	 * If not found they will be read in from
	 * the rehology.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_markerSetUpConstBallHighEta2_t(
		const egd_confObj_t&	confObj);



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpConstBallHighEta2_t(
		const egd_markerSetUpConstBallHighEta2_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpConstBallHighEta2_t&
	operator= (
		const egd_markerSetUpConstBallHighEta2_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_markerSetUpConstBallHighEta2_t(
		egd_markerSetUpConstBallHighEta2_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpConstBallHighEta2_t&
	operator= (
		egd_markerSetUpConstBallHighEta2_t&&);


	/*
	 * =====================
	 * Protected Constructors
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is only used internaly. The result
	 * of this constructor, is an invalid
	 * object.
	 */
	egd_markerSetUpConstBallHighEta2_t();



	/*
	 * =========================
	 * Query Functions
	 */
public:



	/*
	 * =========================
	 * Interface Functions
	 */
public:
	using Base_t::creatProblem;


	/**
	 * \brief	This function outputs information about
	 * 		 the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	using Base_t::inspectDumper;
	using Base_t::inspectRehology;

	/*
	 * ============================
	 * Hooks
	 */
protected:
	using Base_t::hook_creatMarkerPositions;
	using Base_t::hook_creatGridGeometry;
	using Base_t::hook_creatGridPropertyList;
	using Base_t::hook_postProcessGridProperties;


	/*
	 * =============================
	 * Needed Hooks
	 */
protected:
	using Base_t::hook_creatMarkerProperties;
	using Base_t::hook_findMaterial;
	using Base_t::hool_setMarkerProperties;





	/*
	 * ===============================
	 * Interface helper
	 *
	 * These are functions that are provided by the interface
	 * They are non virtual
	 */
protected:
	using Base_t::getTempGrid;
	using Base_t::wasTempSolverInspected;
	using Base_t::getStokeGrid;
	using Base_t::wasStokesSolverInspected;
	using Base_t::isTempOnExtGrid;
	using Base_t::isStokeOnExtGrid;
	using Base_t::getSolverPropMap;



	/*
	 * =======================
	 * Internal Access functions
	 *
	 * These functions allows to access some private
	 * variables of *this.
	 */
protected:
	using Base_t::xDomStart;
	using Base_t::yDomStart;
	using Base_t::xDomLength;
	using Base_t::yDomLength;
	using Base_t::gridPointsX;
	using Base_t::gridPointsY;
	using Base_t::markerDensityX;
	using Base_t::markerDensityY;
	using Base_t::randDistribution;


	/*
	 * ====================
	 * Material hook
	 */
protected:
	/**
	 * \brief	This function sets up the internal state of *this.
	 *
	 * This function will call the base version and then
	 * multiply the viscosity of the fluid by 10.
	 */
	virtual
	void
	hook_setUpInternals()
	 override;



	/*
	 * ============================
	 * Protected Variable
	 *
	 * All variables that are needed are implemented in the lin momentum setup
	 */
protected:
}; //End: class(egd_markerSetUpConstBallHighEta2_t)

PGL_NS_END(egd)

