/**
 * \brief	This file contains the code for the default hooks.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUpBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

egd_markerSetUpBase_t::GridGeometry_t
egd_markerSetUpBase_t::hook_creatGridGeometry()
 const
{
	GridGeometry_t gridGeo((yNodeIdx_t(m_gridYNPoints)), xNodeIdx_t(m_gridXNPoints));	//Most vexing parsing

	gridGeo.makeConstantGrid(m_domainXStart, m_domainXStart + m_domainXLength,
			         m_domainYStart, m_domainYStart + m_domainYLength );

	//Now finalize the geometry
	gridGeo.finalizeGrids();

	return gridGeo;
}; //End: make grid geometry


egd_markerSetUpBase_t::PropToGridMap_t
egd_markerSetUpBase_t::hook_creatGridPropertyList(
	const PropList_t& 	mPropList)
 const
{
	if(mPropList.empty())
	{
		throw PGL_EXCEPT_InvArg("The passed list of marker properties was empty.");
	};


	/**
	 * THis is the grid property of the the solvers.
	 */
	const PropToGridMap_t& solProp = this->getSolverPropMap();


	/*
	 * We generate now the list of properties that will be added to the container.
	 * They will contain out of two parts all properties on basic nodal poinst.
	 * And some specialy placed ones.
	 */
	PropToGridMap_t gProps;	//This is the list of properties on the grid

	//Adding all the properties from the marker, but set them to basic nodal points
	for(const auto& it : mPropList)
	{
		if(it.isPosition() == true)	//Skip positions
		{
			continue;
		};

		//Test if a property is already inside the solver property, thus the solver
		//properties will take precedence.
		if(solProp.hasMappingFor(it) == true)
		{
			continue;		//!< Skip all solver positions
		};

		//Handle velocity different, map it to the correct velocity grid
		if(it.isVelocity() == true)
		{
			if(it.isVelX() )
			{
				gProps.ensureMapping(it, eGridType::StVx, this->isStokeOnExtGrid() );
			}
			else if(it.isVelY() )
			{
				gProps.ensureMapping(it, eGridType::StVy, this->isStokeOnExtGrid() );
			}
			else
			{
				throw PGL_EXCEPT_InvArg("An unknown velocity " + it + " was detected.");
			};

			continue;
		};//End if: is velocity

		// Add the property to the map and set the associated grid to the default grid.
		gProps.addMapping(it, mkReg(eGridType::BasicNode) );
	}; //End for(it): add all the default ones

	return gProps;
}; //End: create grid property list


void
egd_markerSetUpBase_t::hook_postProcessGridProperties(
	PropToGridMap_t* const 		gProps_)
 const
{
	pgl_assert(gProps_ != nullptr);
	PropToGridMap_t& gProps = *gProps_;

	if(gProps.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property map.");
	};

	return;
}; //End: creat grid types


std::pair<egd::egd_markerSetUpBase_t::MarkerPositions_t, egd_markerSetUpBase_t::MarkerPositions_t>
egd::egd_markerSetUpBase_t::hook_creatMarkerPositions(
		const bool 	doDisturbe)
 const
{
	using pgl::isValidFloat;

	pgl_assert(doDisturbe == m_markerRand);

	//Calculate the the number of markers in x and y direction
	const Index_t nMarkersX = (m_gridXNPoints - 1) * m_markerPerCellX;
	const Index_t nMarkersY = (m_gridYNPoints - 1) * m_markerPerCellY;
		pgl_assert(nMarkersX > 1, m_gridXNPoints > 2, m_markerPerCellX > 1,
			   nMarkersY > 1, m_gridYNPoints > 2, m_markerPerCellY > 1 );

	//Calculate the number of markers that are needed in total
	const Index_t nMarkersTot = nMarkersX * nMarkersY;
		pgl_assert(nMarkersTot > 0);

	//Allocate vectors for the result
	MarkerPositions_t xPos(nMarkersTot),
			  yPos(nMarkersTot);

	//Calculate the spacing
	const Numeric_t mDeltaX = m_domainXLength / nMarkersX;
	const Numeric_t mDeltaY = m_domainYLength / nMarkersY;
		pgl_assert(isValidFloat(mDeltaX), mDeltaX > 0.0,
			   isValidFloat(mDeltaY), mDeltaY > 0.0 );

	//This is the offset
	const Numeric_t mStartX = m_domainXStart + 0.5 * mDeltaX;
	const Numeric_t mStartY = m_domainYStart + 0.5 * mDeltaY;
		pgl_assert(isValidFloat(mStartX),
			   isValidFloat(mStartY) );

	/*
	 * Going to create the marker positions
	 */

	//This is an index to count the markers ID
	uSize_t m = 0;

	if(m_markerRand == false)
	{
		/*
		 * Distribute the markers regulary
		 */
		for(Index_t j = 0; j != nMarkersX; ++j)
		{
			for(Index_t i = 0; i != nMarkersY; ++i)
			{
				/*
				 * Calculate the position of the marker.
				 * Note that no random displacement yet is possible.
				 *
				 * TODO:
				 * Implement the random displacement. Use the PGL seed generator.
				 */
				pgl_assert(m < (uSize_t)xPos.size(), m < (uSize_t)yPos.size());

				//Compute the final position
				const Numeric_t xm = mStartX + mDeltaX * j;
				const Numeric_t ym = mStartY + mDeltaY * i;
					pgl_assert(m_domainXStart <= xm, xm <= (m_domainXStart + m_domainXLength),
						   m_domainYStart <= ym, ym <= (m_domainYStart + m_domainYLength) );

				xPos[m] = xm;	//Writing them back
				yPos[m] = ym;

				m += 1;		//Increment the index
			}; //End for(i):
		};//End for(j):
	//End if: regular placement
	}
	else
	{
		/*
		 * Distribute the markers randomly
		 */

		//Create the generator and the distribution that is needed
		const auto randSeed = ::pgl::pgl_seedRNG();	//Generate the random seed
		std::mt19937_64 geni(randSeed);			//Seed the ral generator

		// This is the distribution that is used or the displacement.
		// The range is not [0, 1), but a bit smaller, this is done to ensure
		// that the markers stays inside the domain
		std::uniform_real_distribution<Numeric_t> dist(0.01, 0.99);

		for(Index_t j = 0; j != nMarkersX; ++j)
		{
			for(Index_t i = 0; i != nMarkersY; ++i)
			{
				/*
				 * Calculate the position of the marker.
				 * Note that no random displacement yet is possible.
				 *
				 * TODO:
				 * Implement the random displacement. Use the PGL seed generator.
				 */
				pgl_assert(m < (uSize_t)xPos.size(), m < (uSize_t)yPos.size());

				//Generate the random displacement
				const Numeric_t xRand = 0.5 - dist(geni);
				const Numeric_t yRand = 0.5 - dist(geni);

				//Compute the final position
				const Numeric_t xm = mStartX + mDeltaX * (j + xRand);
				const Numeric_t ym = mStartY + mDeltaY * (i + yRand);
					pgl_assert(m_domainXStart <= xm, xm <= (m_domainXStart + m_domainXLength),
						   m_domainYStart <= ym, ym <= (m_domainYStart + m_domainYLength) );

				xPos[m] = xm; 	//Writing them back
				yPos[m] = ym;

				m += 1;		//Increment the index
			}; //End for(i):
		};//End for(j):
	}; //End else: randomly displacement

	//Test if all markers where visited
	pgl_assert(m == (uSize_t)nMarkersTot);


	/*
	 * Return them as a pair
	 */
	return std::make_pair(std::move(xPos), std::move(yPos));
}; //End: create marker positions


PGL_NS_END(egd)


