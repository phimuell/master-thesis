/**
 * \brief	This file implements the constructors of the the rotating disc scenario in TC Mode.
 * 		 But with low fluid viscosity.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_rotatingDiskTCLEta.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif

using pgl::isValidFloat;


std::string
egd_markerSetUpRotDiscTCLowEta_t::print()
 const
{
	return (
			std::string("Rotating ball TC; Low fluid viscosity\n")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " aV = " + std::to_string(m_angularVel           ) + " 1/s;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpRotDiscTCLowEta_t::egd_markerSetUpRotDiscTCLowEta_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		angularVel,
	const bool 			markerRand,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 angularVel,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpRotDiscTCLowEta_t::egd_markerSetUpRotDiscTCLowEta_t()
 :
  Base_t()
{
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpRotDiscTCLowEta_t::egd_markerSetUpRotDiscTCLowEta_t(
	const egd_confObj_t& 		confObj)
 :
  Base_t(confObj)
{
	this->hook_setUpInternals();
}; // End: building constructor

egd_markerSetUpRotDiscTCLowEta_t::~egd_markerSetUpRotDiscTCLowEta_t()
 = default;


egd_markerSetUpRotDiscTCLowEta_t::egd_markerSetUpRotDiscTCLowEta_t(
	const egd_markerSetUpRotDiscTCLowEta_t&)
 = default;


egd_markerSetUpRotDiscTCLowEta_t&
egd_markerSetUpRotDiscTCLowEta_t::operator= (
	const egd_markerSetUpRotDiscTCLowEta_t&)
 = default;


egd_markerSetUpRotDiscTCLowEta_t::egd_markerSetUpRotDiscTCLowEta_t(
	egd_markerSetUpRotDiscTCLowEta_t&&)
 noexcept
 = default;


egd_markerSetUpRotDiscTCLowEta_t&
egd_markerSetUpRotDiscTCLowEta_t::operator= (
	egd_markerSetUpRotDiscTCLowEta_t&&)
 = default;


void
egd_markerSetUpRotDiscTCLowEta_t::hook_setUpInternals()
{
	if((isValidFloat(this->m_angularVel) == false) or
	   (             this->m_angularVel  <= 0.0  )   )
	{
		throw PGL_EXCEPT_InvArg("The angular momentum is not set correctly, it is " + std::to_string(m_angularVel));
	};

	/*	AIR	(Between) */
	this->m_etaAir  /=  4.0;

	return;
}; //End: set initial values

PGL_NS_END(egd)

