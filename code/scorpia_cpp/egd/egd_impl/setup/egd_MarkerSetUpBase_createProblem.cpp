/**
 * \brief	This file contains the function that will set up everything.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUpBase.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)


egd_markerSetUpBase_t::CreationResult_t
egd_markerSetUpBase_t::creatProblem()
{
	using pgl::isValidFloat;

	/*
	 * Set up the internal states of *this.
	 * This is mainly for circurivating a designe
	 * flaw in C++
	 */
	this->hook_setUpInternals();

	/*
	 * Make some tests if the inspection was done
	 *
	 * This will only check if the implementation from the interfaces were called.
	 */
	if(this->wasStokesSolverInspected() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The Stokes solver was not inspected.");
	};
	if(this->wasTempSolverInspected() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The temperatire solver was not inspected.");
	};
	if(this->wasIntegratorInspected() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The Integrator was not inspected.");
	};
	if(this->wasRehologyInspected() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The material model was not inspected.");
	};


	/*
	 * Create the default list of marker properties.
	 */
	PropList_t props = this->hook_creatMarkerProperties();


	/*
	 * Perform a driver level post processing. This ensures
	 * that positions and so on are present.
	 */
	PropIdx::ensureProperty(&props, PropIdx::PosX());
	PropIdx::ensureProperty(&props, PropIdx::PosY());
	PropIdx::ensureProperty(&props, PropIdx::Type());

	/*
	 * Test if velocity is needed
	 */
	if(this->isIntertiaSolver() == true)
	{
		PropIdx::ensureProperty(&props, PropIdx::VelX());
		PropIdx::ensureProperty(&props, PropIdx::VelY());
	}; //End: add velocity to markers if needed


	if(PropIdx::isValidList(props, true) == false)
	{
		throw PGL_EXCEPT_RUNTIME("The list of properties is invalid.");
	}; //End: check for properties


	/*
	 * Create the position of the markers
	 */
	MarkerPositions_t xPos, yPos;

	{//Begin scope of generation
		auto posPair = this->hook_creatMarkerPositions(m_markerRand);
		xPos.swap(posPair.first );
		yPos.swap(posPair.second);

		pgl_assert(xPos.isFinite().all(),
			   yPos.isFinite().all() );
	}; //End creation of position

	//This is the number of markers
	const Index_t nMarkers = xPos.size();

	//test if they are good
	if(nMarkers <= 0          ||
	   nMarkers != yPos.size()  )
	{
		throw PGL_EXCEPT_RUNTIME("The number of grid points is wrong.");
	}; //End if: check


	/*
	 * Build the marker collection object
	 *
	 * But first modify the list of properties
	 * if the feel velocity is present.
	 */
	PropList_t mPropList(props.cbegin(), props.cend());	//this is the actual list that are used.

	//test if feel properties are aviable
	if(this->isFellVelPresent() == true)
	{
		//add the feel properties
		PropIdx::ensureProperty(&mPropList, PropIdx_t::FeelVelX() );
		PropIdx::ensureProperty(&mPropList, PropIdx_t::FeelVelY() );
			pgl_assert(PropIdx::isValidList(mPropList, true));
	}; //End if: add feel properties

	//Construct the marker conllection object, with the modified versions
	MarkerCollection_t mColl(nMarkers, mPropList);

	//Set the position
	mColl.getXPos().swap(xPos);
	mColl.getYPos().swap(yPos);
		pgl_assert(mColl.cgetXPos().size() == nMarkers,
			   mColl.cgetYPos().size() == nMarkers );


	/*
	 * Now we set the type property
	 */
	this->priv_setTypeProperty(&mColl);


	/*
	 * Fill the marker properties in the collection
	 */
	const bool createIndexMap = this->hool_setMarkerProperties(mColl);

	//Set the index if requested
	if(createIndexMap == true)
	{
		mColl.setUpIndexesMap();
	};

	//Perform a cehck on the marker property
	if(mColl.checkValues() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The setting up of the problem failed.");
	};


	/*
	 * Create the grid geometry.
	 */
	GridGeometry_t gridGeo = this->hook_creatGridGeometry();


	/*
	 * Now we create the actuall grid container
	 */
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_RUNTIME("Could not set up the grid.");
	};


	/*
	 * Call the hook to generate the default list.
	 */
	PropToGridMap_t gProps = this->hook_creatGridPropertyList(props);


	/*
	 * Here we do the private post processing of the grid property
	 * map. This will add the properties that are needed by the
	 * solvers and, if requested also add the ones that are
	 * inside for plotting.
	 */
	this->priv_finalizeGridPropertyList(&gProps, props);


	/*
	 * Here we call the hook function that allows calling code to
	 * change and or modify the list of properties tha are added
	 * to the grid container.
	 */
	this->hook_postProcessGridProperties(&gProps);


	/*
	 * Make a small test to ensure that the list is save
	 */
	if(gProps.isValid() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The list of grid properties is not valid.");
	};


	/*
	 * Ensure that the two maps are compatible with each other
	 */
	if(this->priv_testPropertyLists(gProps, props) == false)
	{
		throw PGL_EXCEPT_RUNTIME("Detected incompatibilities in the lists.");
	};


	/*
	 * Create the grid
	 */
	GridContainer_t grid(yNodeIdx_t(m_gridYNPoints),
			     xNodeIdx_t(m_gridXNPoints),
			     std::move(gridGeo),
			     gProps);

	/*
	 * Return the problem
	 */
	return std::make_pair(std::move(grid), std::move(mColl));
}; //End: create function


PGL_NS_END(egd)


