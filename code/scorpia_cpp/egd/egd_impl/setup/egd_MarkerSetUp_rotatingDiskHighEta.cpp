/**
 * \brief	This file contains all functions for the rotating ball in large viscosity.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_rotatingDiskHighEta.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif

#if defined(EGD_SETUP_ROT_NO_FLUID) && (EGD_SETUP_ROT_NO_FLUID != 0)
#	pragma message "Rotaing fluid is enabled (high eta)."
#else
#	pragma message "Rotaing fluid is disabled (high eta)."
#endif


using pgl::isValidFloat;


std::string
egd_markerSetUpRotDiscHighEta_t::print()
 const
{
	return (
#			if defined(EGD_SETUP_ROT_NO_FLUID) && (EGD_SETUP_ROT_NO_FLUID != 0)
			std::string("Rotating ball, large eta (fluid also rotates).")
#			else
			std::string("Rotating ball, large eta (fluid doesn't rotate).")
#			endif
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " aV = " + std::to_string(m_angularVel           ) + " 1/s;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


void
egd_markerSetUpRotDiscHighEta_t::hook_setUpInternals()
{
	if((isValidFloat(m_angularVel) == false) or
	   (             m_angularVel  <= 0.0  )   )
	{
		throw PGL_EXCEPT_InvArg("The angular momentum is not set correctly, it is " + std::to_string(m_angularVel));
	};

	/*	 BALL	 */
	m_etaBall  = 		   1e+9;		//Pitch
	m_rhoBall  = 		 865.64;		//https://www.aqua-calc.com/page/density-table/substance/lard
	m_radBall  = 		   0.20;		//In meters

	/*	AIR	*/
	m_etaAir  = 		   10.0;		//Viscosity of honey
	m_rhoAir  = 		    1.2;		//Density of air in kg / m^3

	return;
}; //End: set initial values


/*
 * ==================
 * Constructors
 */
egd_markerSetUpRotDiscHighEta_t::egd_markerSetUpRotDiscHighEta_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		angularVel,
	const bool 			markerRand,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 angularVel,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpRotDiscHighEta_t::egd_markerSetUpRotDiscHighEta_t()
 :
  Base_t()
{
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpRotDiscHighEta_t::egd_markerSetUpRotDiscHighEta_t(
	const egd_confObj_t& 		confObj)
 :
  Base_t(confObj)
{
	this->hook_setUpInternals();
}; // End: building constructor

egd_markerSetUpRotDiscHighEta_t::~egd_markerSetUpRotDiscHighEta_t()
 = default;


egd_markerSetUpRotDiscHighEta_t::egd_markerSetUpRotDiscHighEta_t(
	const egd_markerSetUpRotDiscHighEta_t&)
 = default;


egd_markerSetUpRotDiscHighEta_t&
egd_markerSetUpRotDiscHighEta_t::operator= (
	const egd_markerSetUpRotDiscHighEta_t&)
 = default;


egd_markerSetUpRotDiscHighEta_t::egd_markerSetUpRotDiscHighEta_t(
	egd_markerSetUpRotDiscHighEta_t&&)
 noexcept
 = default;


egd_markerSetUpRotDiscHighEta_t&
egd_markerSetUpRotDiscHighEta_t::operator= (
	egd_markerSetUpRotDiscHighEta_t&&)
 = default;

PGL_NS_END(egd)

