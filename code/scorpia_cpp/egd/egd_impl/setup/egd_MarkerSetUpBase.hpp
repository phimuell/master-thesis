#pragma once
/**
 * \brief	This is the concrete implementation of the original problem set up.
 *
 * This is a subducting slab.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "../rheology/egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)


/**
 * \class 	egd_markerSetUpBase_t
 * \brief	This class is the base of the setup process.
 *
 * It provides a general implementation that allows to
 * set up the process. The user only has to implement a
 * few functions to create a concrete setting.
 */
class egd_markerSetUpBase_t : public egd_MarkerSetUp_i
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_MarkerSetUp_i;			//!< This is the base implementation
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::PropIdx_t;
	using Base_t::PropList_t;
	using Base_t::PropToGridMap_t;

	using Base_t::MarkerCollection_t;
	using Base_t::MarkerPositions_t;
	using Base_t::MarkerProperty_t;


	/**
	 * \typedef 	CreationResult_t
	 * \brief	Return type of the interface function.
	 *
	 * The job of this interface is to create the grid
	 * and the marker collection and this is the return type
	 * of the function. It is a pair, such that everything can
	 * completly created internaly.
	 */
	using CreationResult_t 		= ::std::pair<GridContainer_t, MarkerCollection_t>;


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_markerSetUpBase_t();


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * This constructor will set up *this, filled with the geometrical
	 * properties of the domain. It does not set up any other
	 * properties.
	 * The data that are passed by consturctor will take precedence.
	 * However to allow full construction, the constructor accepts
	 * an optional pointer argument to the configuration object.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  yLengthM	The extension of the domain in y direction, in METERS.
	 * \param  xLengthM	The extension of the domain in x direction, in METERS.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  markerRand	Displace the markers randomly.
	 * \param  yDomStartM	Start of the domain in meters, in y direction.
	 * \param  xDomStartM   Start of the domain in meters, in x direction.
	 * \param  confObj	Optional pointer to the conf object.
	 */
	egd_markerSetUpBase_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const Numeric_t 	yLengthM,
		const Numeric_t 	xLengthM,
		const Index_t 		nMarkerY,
		const Index_t 		nMarkerX,
		const bool 		markerRand,
		const Numeric_t 	yDomStartM,
		const Numeric_t 	xDomStartM,
		const egd_confObj_t* const confObj = nullptr);


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * This constructor forwards the request to the first building constructor.
	 * But the start of the domain is set to zero in both directions.
	 */
	egd_markerSetUpBase_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const Numeric_t 	yLengthM,
		const Numeric_t 	xLengthM,
		const Index_t 		nMarkerY,
		const Index_t 		nMarkerX,
		const bool 		markerRand,
		const egd_confObj_t* const confObj = nullptr);



	/**
	 * \brief	Building constructor with
	 * 		 config object.
	 *
	 * This constructor forwards the request the
	 * explicit building constructor. This constructor
	 * is provided for compability, with the building
	 * process.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_markerSetUpBase_t(
		const egd_confObj_t&	confObj);



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBase_t(
		const egd_markerSetUpBase_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBase_t&
	operator= (
		const egd_markerSetUpBase_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_markerSetUpBase_t(
		egd_markerSetUpBase_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBase_t&
	operator= (
		egd_markerSetUpBase_t&&);


	/*
	 * =====================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	Default constructor.
	 *
	 * Is only used internaly. The result
	 * of this constructor, is an invalid
	 * object.
	 */
	egd_markerSetUpBase_t();



	/*
	 * =========================
	 * Query Functions
	 */
public:



	/*
	 * =========================
	 * Interface Functions
	 */
public:
	/**
	 * \brief	This function creates the marker collection
	 * 		 and the grid container.
	 *
	 * This function implements a driver for the process. It
	 * allows to construct the grid container and the marker
	 * collection.
	 * The process can be modified/controled by hooks.
	 *
	 * It is likely that the function must not be overwritten.
	 */
	virtual
	CreationResult_t
	creatProblem()
	 override;


	/**
	 * \brief	This function outputs information about
	 * 		 the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override
	 = 0;


	using Base_t::inspectDumper;
	using Base_t::inspectRehology;
	using Base_t::inspectIntegrator;
	using Base_t::inspectSolver;


	/*
	 * ============================
	 * Hooks
	 *
	 * These functions allows to modify the behaviour of this.
	 * Some of them are implemented by the base, but some are
	 * needed to be implemented by the deriving classes.
	 */
protected:
	/**
	 * \brief	This function distributes the markers.
	 *
	 * This function distributes the markers in side the
	 * domain. It is possible, but currently unsupported
	 * to randomly distorts the markers, the member variable
	 * is ignored.
	 *
	 * \param  doDistorbe		Should the marker by randomly disblaced.
	 */
	virtual
	std::pair<MarkerPositions_t, MarkerPositions_t>
	hook_creatMarkerPositions(
		const bool 	doDisturbe)
	 const;


	/**
	 * \brief	This function is called to generate the grid geometry.
	 *
	 * The default of this function is to set up a constant grid.
	 */
	virtual
	GridGeometry_t
	hook_creatGridGeometry()
	 const;



	/**
	 * \brief	This function creates a default list of grid properties.
	 *
	 * The property list that is build by this function is only a part of
	 * the properties that will later be stored in the grid. All properties
	 * that are needed by the solvers, are added by the base driver functionality
	 * automatically, thus the user does not need to consider them.
	 *
	 * The default implementation will add all properties that are found in the
	 * markers' property list, with the exception of the position, which will be
	 * ignored, to the map. All properties will be defined on the basic nodal
	 * grid.
	 * Also all properties that are already in the solver property lists will
	 * be ignored.
	 * Note that velocities are mapped to the respective grid, the extension is in
	 * agreement with teh solver.
	 *
	 * \param  mPropList	This is the marker property list that was used to create the collection.
	 */
	virtual
	PropToGridMap_t
	hook_creatGridPropertyList(
		const PropList_t& 	mPropList)
	 const;




	/**
	 * \brief	This function will modify the grid property map.
	 *
	 * This function allows to modify the property list after it was
	 * gewnerated. Note that this function does not add the properties
	 * that are needed by the solvers to the list, this is done before
	 * this function is called.
	 *
	 * The default of thsi function does nothing.
	 *
	 * \param  gPropMap	The partially filled default map.
	 */
	virtual
	void
	hook_postProcessGridProperties(
		PropToGridMap_t* const 		gPropMap)
	 const;




	/*
	 * =============================
	 * Needed Hooks
	 * These are all the hooks that the user needs
	 * to implement, thus they have no default.
	 */
protected:
	/**
	 * \brief	This function creates the property list of the markers.
	 *
	 * This is a function that the user must implement. It is a list of
	 * all properties that are defined on markers. Thus they must
	 * be representants.
	 * Note that the function must not add positions and type, this is
	 * handled by the griver code.
	 */
	virtual
	PropList_t
	hook_creatMarkerProperties()
	 const
	 = 0;


	/**
	 * \brief	This function determines in which material the given point has.
	 *
	 * This function is called by the driver code to determine which material
	 * is on which point. This function returns an integer that is then stored
	 * in the marker collection's type property.
	 *
	 * \param  x	The x coordinate of the position.
	 * \param  y 	The y coordinate of the position.
	 */
	virtual
	Index_t
	hook_findMaterial(
		const Numeric_t 	x,
		const Numeric_t 	y)
	 const
	 = 0;


	/**
	 * \brief	This hook is called to fill the marker
	 * 		 property collection.
	 *
	 * This function is called after the marker collection is
	 * build. It should set the markers properties to theier
	 * initial values.
	 *
	 * Note that the type property is allready set by the
	 * driver code. So the user can use them to simplyify
	 * implementation.
	 *
	 * \param  mColl 	The collection that should be set.
	 */
	virtual
	bool
	hool_setMarkerProperties(
		MarkerCollection_t& 		mColl)
	 const
	 = 0;



	/*
	 * =======================
	 * Private Functions
	 */
private:
	/**
	 * \brief	This function is used to set only the type property.
	 *
	 * This function will call the hook_findMaterial() function on each
	 * marker to determine which type that marker has and store the
	 * value in the collection.
	 *
	 * \param  mColl	The collection that is processed.
	 */
	virtual
	void
	priv_setTypeProperty(
		MarkerCollection_t* const 	mColl_)
	 const
	 final;


	/**
	 * \brief	This is an internal function that is called beofre
	 * 		 the preprocessing is performed.
	 *
	 * This function will add the properties that are needed by the solvers
	 * and also makes some basic checks. If the plot properties
	 * are reqeuested, then they will be added too.
	 */
	virtual
	void
	priv_finalizeGridPropertyList(
		PropToGridMap_t* const 		gProp,
		const PropList_t& 		mProp)
 	 const
 	 final;


 	/**
 	 * \brief	This function is used to test if the grid property
 	 * 		 and marker propetrty lists are compatible with each other.
 	 *
 	 * The function tests if all properties that are defined on the grid are
 	 * representanted by one of the marker properties.
 	 *
 	 * \param  pProps	Grid properties.
 	 * \param  mProps	Marker properties.
 	 */
 	virtual
 	bool
 	priv_testPropertyLists(
 		const PropToGridMap_t& 		gProps,
 		const PropList_t& 		mProps)
 	 const
 	 final;




	/*
	 * ===============================
	 * Interface helper
	 *
	 * These are functions that are provided by the interface
	 * They are non virtual
	 */
protected:
	using Base_t::getTempGrid;
	using Base_t::isTempOnExtGrid;
	using Base_t::isStokeOnExtGrid;
	using Base_t::getStokeGrid;
	using Base_t::isIntertiaSolver;
	using Base_t::getSolverPropMap;
	using Base_t::isFellVelPresent;
	using Base_t::wasIntegratorInspected;
	using Base_t::wasStokesSolverInspected;
	using Base_t::wasRehologyInspected;
	using Base_t::wasTempSolverInspected;


	/*
	 * ====================
	 * Material hook
	 */
protected:
	/**
	 * \brief	This function sets up the internal state of *this.
	 *
	 * This function is called by the buildiner at the beginning.
	 * It allows to set up states of deriving classes.
	 * This function is provided becuase C++ can not call viertual function
	 * inside a constructor.
	 */
	virtual
	void
	hook_setUpInternals()
	 = 0;



	/*
	 * =======================
	 * Internal Access functions
	 *
	 * These functions allows to access some private
	 * variables of *this.
	 */
protected:
	Numeric_t
	xDomStart()
	 const
	 noexcept
		{return m_domainXStart;};


	Numeric_t
	yDomStart()
	 const
	 noexcept
		{return m_domainYStart;};


	Numeric_t
	xDomLength()
	 const
	 noexcept
		{return m_domainXLength;};


	Numeric_t
	yDomLength()
	 const
	 noexcept
		{return m_domainYLength;};


	Size_t
	gridPointsX()
	 const
	 noexcept
		{return m_gridXNPoints;};


	Size_t
	gridPointsY()
	 const
	 noexcept
		{return m_gridYNPoints;};


	Size_t
	markerDensityX()
	 const
	 noexcept
		{return m_markerPerCellX;};


	Size_t
	markerDensityY()
	 const
	 noexcept
		{return m_markerPerCellY;};


	bool
	randDistribution()
	 const
	 noexcept
	 	{return m_markerRand;};




	/*
	 * ============================
	 * Private Variable
	 *
	 * All quantities are in SI units
	 * Values that are undefined, are not needed.
	 * They are set to zero.
	 */
private:
	Numeric_t 	m_domainXStart   = NAN;		//!< Start point in x direction of the computational domain.
	Numeric_t  	m_domainYStart   = NAN;		//!< Start point in y direction of the computational domain.
	Numeric_t 	m_domainXLength  = NAN;		//!< Length of the domain in x direction.
	Numeric_t 	m_domainYLength  = NAN;		//!< Length of the domain in y direction.

	Size_t 		m_gridXNPoints   = 0;		//!< Number of basic grid points in x direction.
	Size_t 		m_gridYNPoints   = 0;		//!< Number of basic grid points in y direction.
	Size_t 		m_markerPerCellX = 0;		//!< Number of markers in each cell in x direction.
	Size_t 		m_markerPerCellY = 0;		//!< Number of markers in each cell in y direction.
	bool 		m_markerRand     = false;	//!< Displace the marker randomly.
}; //End: class(egd_markerSetUpBase_t)


PGL_NS_END(egd)


