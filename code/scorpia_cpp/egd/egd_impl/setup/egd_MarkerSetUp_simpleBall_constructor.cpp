/**
 * \brief	This file contains the constructors that are needed for the simple ball situation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_simpleBall.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif



egd_markerSetUpBallOne_t::egd_markerSetUpBallOne_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 3.0, 1.0,
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpBallOne_t::egd_markerSetUpBallOne_t()
 :
  egd_markerSetUpBallOne_t(
  	yNodeIdx_t(301), xNodeIdx_t(101),	//grid points
  	5, 5,					//Marker density
  	false,					//no ransom displacement
  	nullptr)				//No configuration object aviable
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpBallOne_t::egd_markerSetUpBallOne_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerSetUpBallOne_t(
  		yNodeIdx_t(confObj.getNy()), xNodeIdx_t(confObj.getNx()),
  		confObj.getMarkerDensityY(), confObj.getMarkerDensityX(),
  		confObj.randomlyDisturbeMarkers(),
  		&confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpBallOne_t::~egd_markerSetUpBallOne_t()
 = default;


egd_markerSetUpBallOne_t::egd_markerSetUpBallOne_t(
	const egd_markerSetUpBallOne_t&)
 = default;


egd_markerSetUpBallOne_t&
egd_markerSetUpBallOne_t::operator= (
	const egd_markerSetUpBallOne_t&)
 = default;


egd_markerSetUpBallOne_t::egd_markerSetUpBallOne_t(
	egd_markerSetUpBallOne_t&&)
 noexcept
 = default;


egd_markerSetUpBallOne_t&
egd_markerSetUpBallOne_t::operator= (
	egd_markerSetUpBallOne_t&&)
 = default;


void
egd_markerSetUpBallOne_t::hook_setUpInternals()
{
	/*	 BALL	 */
	m_etaBall  = 		1000.0 ;		//~Lard
	m_rhoBall  = 		 865.64;		//https://www.aqua-calc.com/page/density-table/substance/lard
	m_radBall  = 		   0.20;		//In meters
	m_velYBall =   	          -4.0 ;		//Flowing upwards, must be negative because y is depth


	/*	AIR	*/
	m_etaAir  = 		   18.5e-4;		// 100 times the real viscosity Pa s
	m_rhoAir  = 		    1.2   ;		//Real density in kg / m^3

	return;
}; //End: set initial values

PGL_NS_END(egd)

