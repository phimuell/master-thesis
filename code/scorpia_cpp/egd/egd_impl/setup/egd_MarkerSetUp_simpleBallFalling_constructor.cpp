/**
 * \brief	This file contains the constructors that are needed for the simple ball situation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_simpleBallFalling.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)



egd_markerSetUpBallTwo_t::egd_markerSetUpBallTwo_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const Numeric_t 	yLengthM,
	const Numeric_t 	xLengthM,
	const Index_t 		nMarkerY,
	const Index_t 		nMarkerX,
	const bool 		markerRand)
 :
  Base_t(Ny, Nx,
  	 yLengthM, xLengthM,
  	 nMarkerY, nMarkerX,
  	 markerRand)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpBallTwo_t::egd_markerSetUpBallTwo_t()
 :
  Base_t(yNodeIdx_t(201), xNodeIdx_t(101),
  	2.0, 1.0,
  	5, 5, false)
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpBallTwo_t::egd_markerSetUpBallTwo_t(
	const egd_confObj_t& 		confObj)
 :
  Base_t(confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpBallTwo_t::~egd_markerSetUpBallTwo_t()
 = default;


egd_markerSetUpBallTwo_t::egd_markerSetUpBallTwo_t(
	const egd_markerSetUpBallTwo_t&)
 = default;


egd_markerSetUpBallTwo_t&
egd_markerSetUpBallTwo_t::operator= (
	const egd_markerSetUpBallTwo_t&)
 = default;


egd_markerSetUpBallTwo_t::egd_markerSetUpBallTwo_t(
	egd_markerSetUpBallTwo_t&&)
 noexcept
 = default;


egd_markerSetUpBallTwo_t&
egd_markerSetUpBallTwo_t::operator= (
	egd_markerSetUpBallTwo_t&&)
 = default;


void
egd_markerSetUpBallTwo_t::hook_setUpInternals()
{
	/*	 BALL	 */
	m_etaBall  = 		1000.0 ;		//~Lard
	m_rhoBall  = 		   3.0 ;		//Not real density, but LARGER than
	m_radBall  = 		   0.20;		//In mneters


	/*	AIR	*/
	m_etaAir  = 		   0.0001;		//Not realy air, but small enough
	m_rhoAir  = 		   1.2   ;		//Real densoity

	return;
}; //End: set initial values

PGL_NS_END(egd)

