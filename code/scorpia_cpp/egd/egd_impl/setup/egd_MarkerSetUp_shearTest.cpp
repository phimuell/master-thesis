/**
 * \brief	This file contains functions to set up the rotating disc functionality.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_shearTest.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;


std::string
egd_markerSetUpShearSetting_t::print()
 const
{
	return (
			std::string("Shear Setting")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpShearSetting_t::PropList_t
egd_markerSetUpShearSetting_t::hook_creatMarkerProperties()
 const
{
	/*
	 * Create the default list.
	 * Do not include positions and type, this is done by the driver code.
	 */
	PropList_t props = { PropIdx::Density(), PropIdx::Viscosity(),
		             PropIdx::VelX(),    PropIdx::VelY()      };

	return props;
}; //End: make makrker property


void
egd_markerSetUpShearSetting_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	//Every marker property is constant, beside the velocity
	dumper.addConstantMarkerProperty(PropIdx::Density()  )
	      .addConstantMarkerProperty(PropIdx::Viscosity())
	      .addConstantMarkerProperty(PropIdx::Type()     );

	//Ignore a lot of grid propertires
	dumper.ignoreGridProp(PropIdx::Density()  )
	      .ignoreGridProp(PropIdx::Viscosity())
	      .ignoreGridProp(PropIdx::VelX()     )
	      .ignoreGridProp(PropIdx::VelY()     );

	//Only dump represenatants
	dumper.onlyDumpRepOnGrid();

	//no temperature must be saved.
	dumper.noTempSol();

	return;
}; //End: marker


bool
egd_markerSetUpShearSetting_t::hool_setMarkerProperties(
	MarkerCollection_t& 		mColl)
 const
{
	using pgl::isValidFloat;

	/*
	 * Some helper functions/ claculations
	 */

	//Now we load all properties that we need
	      MarkerProperty_t& RHO   = mColl.getMarkerProperty(PropIdx::Density()   );
	      MarkerProperty_t& ETA   = mColl.getMarkerProperty(PropIdx::Viscosity() );
	      MarkerProperty_t& VELX  = mColl.getMarkerProperty(PropIdx::VelX()      );
	      MarkerProperty_t& VELY  = mColl.getMarkerProperty(PropIdx::VelY()      );
	const MarkerProperty_t& TYPE  = mColl.getMarkerProperty(PropIdx::Type()      );

	//Get the position of the markers
	const MarkerPositions_t& X = mColl.cgetXPos();
	const MarkerPositions_t& Y = mColl.cgetYPos();

	const Index_t   nMarkers = X.size();	//Numbers of markers

	/*
	 * Iterating through all the markers and set them
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		const Numeric_t xm = X[m];		//Load the positions
		const Numeric_t ym = Y[m];

		//Determine the type of the material
		const Index_t mType = Index_t(TYPE[m]);
			pgl_assert(Index_t(mType) == this->hook_findMaterial(xm, ym),
				   0 <= Index_t(mType), Index_t(mType) < TY_N);
			pgl_assert(isValidFloat(m_loWedge_eta), isValidFloat(m_loWedge_rho),
				   isValidFloat(m_upWedge_eta), isValidFloat(m_upWedge_rho) );

		//Set velocity to zero
		VELX[m] = 0.0;
		VELY[m] = 0.0;

		//Set the specific stuff
		switch(mType)
		{
		  case TY_LOWER_WEDGE:
		  	RHO [m] = m_loWedge_rho;
		  	ETA [m] = m_loWedge_eta;
		  break;

		  case TY_UPPER_WEDGE:
		  	RHO [m] = m_upWedge_rho;
		  	ETA [m] = m_upWedge_eta;
		  break;

		  default:
		  	throw PGL_EXCEPT_InvArg("Computed an invalid material type.");
		}; //End switch(mType):
	}; //End for(m):

	return true;
}; //End: default for setting up the problem geometry


Index_t
egd_markerSetUpShearSetting_t::hook_findMaterial(
	const Numeric_t 	x_m,
	const Numeric_t 	y_m)
 const
{
	if(x_m < this->xDomStart() )
	{
		throw PGL_EXCEPT_InvArg("X position is too small.");
	};
	if((this->xDomStart() + this->xDomLength() ) < x_m)
	{
		throw PGL_EXCEPT_InvArg("X position is too large.");
	};
	if(y_m < this->yDomStart() )
	{
		throw PGL_EXCEPT_InvArg("Y position is too small.");
	};
	if((this->yDomStart() + this->yDomLength() ) < y_m)
	{
		throw PGL_EXCEPT_InvArg("Y position is too large.");
	};

	//Determine in which half we are.
	if(x_m < y_m)
	{
		return TY_LOWER_WEDGE;
	}
	else
	{
		return TY_UPPER_WEDGE;
	};

	pgl_assert(false && "Reached unreacable code.");
	return -1;
}; //End: find location


PGL_NS_END(egd)

