/**
 * \brief	This file contains functions to set up the rotating disc functionality for the TC flow.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_rotatingDiskTC.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;

egd_markerSetUpRotDiscTC_t::Numeric_t
egd_markerSetUpRotDiscTC_t::getBeyondRadius()
 const
{
using std::fmin;
	const Numeric_t t = this->m_outRad + this->m_outWidth;
		pgl_assert(isValidFloat(t),
			   t > 0.0,
			   t <= (0.5 * fmin(this->xDomLength(), this->yDomLength())) );
	return t;
};


std::string
egd_markerSetUpRotDiscTC_t::print()
 const
{
	return (
			std::string("Rotating ball TC")
			+ " Lx =  " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly =  " + std::to_string(this->yDomLength()     ) + "m;"
			+ " aV =  " + std::to_string(this->m_angularVel     ) + " 1/s;"
			+ " aVo = " + std::to_string(this->m_angVelOut      ) + " 1/s;"
			+ " Nx =  " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny =  " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx =  " + std::to_string(this->markerDensityX() ) + ";"
			+ " My =  " + std::to_string(this->markerDensityY() ) + ";"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpRotDiscTC_t::PropList_t
egd_markerSetUpRotDiscTC_t::hook_creatMarkerProperties()
 const
{
	/*
	 * Create the default list.
	 * Do not include positions and type, this is done by the driver code.
	 */
	PropList_t props = { PropIdx::Density(), PropIdx::Viscosity(),
		             PropIdx::VelX(),    PropIdx::VelY()      };

	return props;
}; //End: make makrker property


void
egd_markerSetUpRotDiscTC_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	dumper.addConstantMarkerProperty(PropIdx::Density()   )
	      .addConstantMarkerProperty(PropIdx::Viscosity() )
	      .addConstantMarkerProperty(PropIdx::Type()      );

	dumper.noTempSol();


	return;
}; //End: marker


bool
egd_markerSetUpRotDiscTC_t::hool_setMarkerProperties(
	MarkerCollection_t& 		mColl)
 const
{
using pgl::isValidFloat;

	/*
	 * Some helper functions/ claculations
	 */

	//Now we load all properties that we need
	      MarkerProperty_t& RHO   = mColl.getMarkerProperty(PropIdx::Density()   );
	      MarkerProperty_t& ETA   = mColl.getMarkerProperty(PropIdx::Viscosity() );
	      MarkerProperty_t& VELX  = mColl.getMarkerProperty(PropIdx::VelX()      );
	      MarkerProperty_t& VELY  = mColl.getMarkerProperty(PropIdx::VelY()      );
	const MarkerProperty_t& TYPE  = mColl.getMarkerProperty(PropIdx::Type()      );

	//Get the position of the markers
	const MarkerPositions_t& X = mColl.cgetXPos();
	const MarkerPositions_t& Y = mColl.cgetYPos();

	//this is the number of markers
	const Index_t   nMarkers = X.size();	//Numbers of markers

	//this is the outer radius
	const Numeric_t outerRadius  = this->m_outRad;

	//this is the width of the spalt
	const Numeric_t spaltWidth    = this->m_outRad - this->m_radBall;
	const Numeric_t zeroWidth     = spaltWidth * 0.3;
	const Numeric_t zeroRadius    = this->m_radBall + zeroWidth;
		pgl_assert(isValidFloat(zeroWidth),
			   zeroWidth > 0.0,
			   zeroRadius <= outerRadius);
		pgl_assert(isValidFloat(this->m_angularVel),
			   isValidFloat(this->m_angVelOut ) );


	/*
	 * Iterating through all the markers and set them
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		const Numeric_t xm = X[m];		//Load the positions
		const Numeric_t ym = Y[m];

		const Numeric_t d2 = (xm * xm) + (ym * ym); 	//square distance from middle
		const Numeric_t d  = std::sqrt(d2);		//distance from the middle

		//Determine the type of the material
		const Index_t mType = Index_t(TYPE[m]);
			pgl_assert(Index_t(mType) == this->hook_findMaterial(xm, ym));

		/*
		 * This is teh magnitude of the rotation.
		 * it have to be assigned by the switch statement
		 *
		 * The component are computed with sine and cosine, see below.
		 */
		Numeric_t velMagnitude = NAN;

		//Set the specific stuff
		switch(mType)
		{
		  //Air between the two discs
		  case TY_STICKY_AIR:
		  	RHO [m] = m_rhoAir;
		  	ETA [m] = m_etaAir;

		  	if(d < zeroRadius)
			{
				//We are indside the range that should contain rotating fluid
				const Numeric_t tmp  = 1.0 - (d - this->m_radBall) / zeroWidth;
				velMagnitude = d * ( (0.3 * (this->m_angularVel)) * tmp);
			}

			else
			{
				//We are outside the range that should contain rotating fluid
				velMagnitude = 0.0;
			};
		  break;

		  //Outer cylinder
		  case TY_WALL:
		  	RHO[m] = this->m_outRho;
		  	ETA[m] = this->m_outEta;
			velMagnitude = d * (this->m_angVelOut);
		  break;

		  //Inner disc that is rotating
		  case TY_DISC:
		  	RHO[m] = m_rhoBall;
		  	ETA[m] = m_etaBall;
		  	velMagnitude = d * (this->m_angularVel);
		  break;

		  //Fluid beyond the outer cylinder
		  case TY_OUT_FLUID:
		  	RHO[m] = this->m_outFRho;
		  	ETA[m] = this->m_outFEta;
		  	velMagnitude = 0.0;
		  break;

		  default:
		  	throw PGL_EXCEPT_InvArg("Computed an invalid material type.");
		}; //End switch(mType):

		/*
		 * For the minus in front of the vx velocity.
		 * Imagine the first quadrant with \phi \approx 45Deg
		 * There the velocity vector, “\” has a positive y component, but an negative x component.
		 *
		 *  A
		 *  |
		 *  |xx
		 *  |   xxx  \
		 *  |      xx \
		 *  |         x\
		 *  |           x
		 *  |            x
		 *  O---------------->
		 *
		 * So we have to use the minus in from of the calculation.
		 */
		//compute the angles
		const Numeric_t phi    = std::atan2(ym, xm);	//can also handle both zero
		const Numeric_t sinPhi = std::sin(phi);
		const Numeric_t cosPhi = std::cos(phi);
			pgl_assert(pgl::isValidFloat(velMagnitude));

		VELX[m] = - velMagnitude * sinPhi;	//Minus is important and swapped cos sin
		VELY[m] =   velMagnitude * cosPhi;
	}; //End for(m):

	return true;
}; //End: default for setting up the problem geometry


Index_t
egd_markerSetUpRotDiscTC_t::hook_findMaterial(
	const Numeric_t 	x_m,
	const Numeric_t 	y_m)
 const
{

	if(x_m < this->xDomStart() )
	{
		throw PGL_EXCEPT_InvArg("X position is too small.");
	};
	if((this->xDomStart() + this->xDomLength() ) < x_m)
	{
		throw PGL_EXCEPT_InvArg("X position is too large.");
	};
	if(y_m < this->yDomStart() )
	{
		throw PGL_EXCEPT_InvArg("Y position is too small.");
	};
	if((this->yDomStart() + this->yDomLength() ) < y_m)
	{
		throw PGL_EXCEPT_InvArg("Y position is too large.");
	};
		pgl_assert(isValidFloat(m_radBall), m_radBall > 0.0);

	//this is the outer radius
	const Numeric_t outerRadius  = this->m_outRad;
	const Numeric_t beyondRadius = this->getBeyondRadius();

	//calculate the square distance from the ball centre
	const Numeric_t dist2   = (x_m * x_m) + (y_m * y_m);
		pgl_assert(isValidFloat(dist2), dist2 >= 0.0);

	if(dist2 < (m_radBall * m_radBall))
	{
		//it is the rotating disc
		return TY_DISC;
	}
	else if(dist2 >= (beyondRadius * beyondRadius) )	//order is important.
	{
		return TY_OUT_FLUID;
	}
	else if(dist2 >= (outerRadius * outerRadius))
	{
		//We are in the outer region, also known as wall
		return TY_WALL;
	}
	else
	{
		return TY_STICKY_AIR;
	};
	pgl_assert(false && "Reached unreacable code.");
}; //End: find location


PGL_NS_END(egd)

