/**
 * \brief	This file implements the constructors of the the rotating disc scenario.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_rotatingDisk.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif

using pgl::isValidFloat;



egd_markerSetUpRotDisc_t::egd_markerSetUpRotDisc_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		angularVel,
	const bool 			markerRand,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Ny, Nx,
  	 1.2, 1.2,		//1.2m diameter
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 -0.6, -0.6,		//move the start of teh coordinate system.
  	 confObj)
{
	if(isValidFloat(angularVel) == false)
	{
		throw PGL_EXCEPT_InvArg("The angular momentum is not a valid number.");
	};
	if(angularVel <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The angular momentum is negative.");
	};
	m_angularVel = angularVel;

	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpRotDisc_t::egd_markerSetUpRotDisc_t()
 :
  egd_markerSetUpRotDisc_t(
  		yNodeIdx_t(121), xNodeIdx_t(121),
  		5, 5,
  		40,	//Angular velocity
  		false)
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpRotDisc_t::egd_markerSetUpRotDisc_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerSetUpRotDisc_t(
  		yNodeIdx_t(confObj.getNy()), xNodeIdx_t(confObj.getNx()),
  		confObj.getMarkerDensityY(), confObj.getMarkerDensityX(),
  		confObj.setUpPar("angularvel", 40.0),
  		confObj.randomlyDisturbeMarkers(),
  		&confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
}; // End: building constructor

egd_markerSetUpRotDisc_t::~egd_markerSetUpRotDisc_t()
 = default;


egd_markerSetUpRotDisc_t::egd_markerSetUpRotDisc_t(
	const egd_markerSetUpRotDisc_t&)
 = default;


egd_markerSetUpRotDisc_t&
egd_markerSetUpRotDisc_t::operator= (
	const egd_markerSetUpRotDisc_t&)
 = default;


egd_markerSetUpRotDisc_t::egd_markerSetUpRotDisc_t(
	egd_markerSetUpRotDisc_t&&)
 noexcept
 = default;


egd_markerSetUpRotDisc_t&
egd_markerSetUpRotDisc_t::operator= (
	egd_markerSetUpRotDisc_t&&)
 = default;


void
egd_markerSetUpRotDisc_t::hook_setUpInternals()
{
	if((isValidFloat(m_angularVel) == false) or
	   (             m_angularVel  <= 0.0  )   )
	{
		throw PGL_EXCEPT_InvArg("The angular momentum is not set correctly, it is " + std::to_string(m_angularVel));
	};

	/*	 BALL	 */
	m_etaBall  = 		   1e+9;		//Pitch
	m_rhoBall  = 		 865.64;		//https://www.aqua-calc.com/page/density-table/substance/lard
	m_radBall  = 		   0.20;		//In meters

	/*	AIR	*/
	m_etaAir  = 		   18.5e-5;		// 10 times the real viscosity Pa s
	m_rhoAir  = 		    1.2   ;		//Real density in kg / m^3

	return;
}; //End: set initial values

PGL_NS_END(egd)

