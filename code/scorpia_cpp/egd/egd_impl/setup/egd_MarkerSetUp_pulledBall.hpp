#pragma once
/**
 * \brief	This file contains the definition of the pulled ball.
 *
 * Note that the concrete behaviour of the ball is most determined
 * by the rehology.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>
#include <egd_interfaces/egd_MarkerRehology_interface.hpp>

#include "./egd_MarkerSetUpBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)


/**
 * \class 	egd_markerSetUpPulledBall_t
 * \brief	This is a setup for the pulled ball.
 *
 * This setup will place a ball in a long pipe. The pipe has a length
 * (extension in x direction) of 3.5 meters and a hight (extension
 * in y direction) of 2 meters.
 *
 * The ball has a radius of 20cm, and is placed in half hight on the
 * right, but it could be specified.
 * Note that ball is now intended to be pulled to the left. This is
 * different from before where the ball was on theleft and was pulled
 * to the right. This is done to keep the pressure at infinity far
 * away from the ball where the moving happens.
 *
 * Note that this class only sets up the conditions actuall manipulations
 * that will make the behaviour sepcial, must be controled by a rehology.
 *
 * Note that the surrounding media of the ball is low but not zero. This
 * is done such that viscous forces are small. Also note that the viscosity
 * of the ball is larger than in other ball settings.
 *
 * Note this function will try to extract the velocities from the rehology.
 * If this is not possible, it will be set to zero.
 *
 * The quantities of the fluid are such that the Reynolds number is in the
 * order of 20, which is quite slow. This is mostly done by decreasing the
 * fluids density, but increasing the viscosity too.
 * The density of the ball is made, sich that the the ratio between them is
 * not too large.
 */
class egd_markerSetUpPulledBall_t : public egd_markerSetUpBase_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_markerSetUpBase_t;			//!< This is the base implementation
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::PropIdx_t;
	using Base_t::PropList_t;
	using Base_t::PropToGridMap_t;

	using Base_t::MarkerCollection_t;
	using Base_t::MarkerPositions_t;
	using Base_t::MarkerProperty_t;
	using Base_t::CreationResult_t;

	/*
	 * Brief	This are the name of the different materials we have.
	 */
	const static Index_t TY_STICKY_AIR 	= 0;		//This is the air
	const static Index_t TY_BALL            = 1;		//This is the ball
	const static Index_t TY_N 		= 2;


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_markerSetUpPulledBall_t();


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * This is the general building constructor, it allows to specifies
	 * the raius of the ball and its initial velocity.
	 * Note that it is legeal, but not recomended, to pass NAN as velocities.
	 * This is interpreted such that they will be loded from the rehology,
	 * see its inspect function.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  radBall	The radius of the ball.
	 * \param  ballVelY	The initial velocity of the ball in y direction.
	 * \param  ballVelX	The initial velocity of the ball in x direction.
	 * \param  markerRand	Displace the markers randomly.
	 * \param  confObj	Optional pointer to the configuration object.
	 */
	egd_markerSetUpPulledBall_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const Index_t 			nMarkerY,
		const Index_t 			nMarkerX,
		const Numeric_t 		radBall,
		const Numeric_t 		ballVelY,
		const Numeric_t 		ballVelX,
		const bool 			markerRand,
		const egd_confObj_t* const  	confObj = nullptr);


	/**
	 * \brief	This is the reduced building constructor.
	 *
	 * The radius of the ball is set to the 20cm and the the
	 * initial velocity of the ball is zero.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  markerRand	Displace the markers randomly.
	 * \param  confObj	Optional pointer to the configuration object.
	 */
	egd_markerSetUpPulledBall_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const Index_t 			nMarkerY,
		const Index_t 			nMarkerX,
		const bool 			markerRand,
		const egd_confObj_t* const  	confObj = nullptr);



	/**
	 * \brief	Building constructor with config object.
	 *
	 * It will atempt to read the radius from the configuration
	 * object key "radius" and the initial velocity as well,
	 * key "velx" and "vely". If they are not found the default
	 * values are used.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_markerSetUpPulledBall_t(
		const egd_confObj_t&	confObj);



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpPulledBall_t(
		const egd_markerSetUpPulledBall_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpPulledBall_t&
	operator= (
		const egd_markerSetUpPulledBall_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_markerSetUpPulledBall_t(
		egd_markerSetUpPulledBall_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpPulledBall_t&
	operator= (
		egd_markerSetUpPulledBall_t&&);


	/*
	 * =====================
	 * Protected Constructors
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is only used internaly. The result
	 * of this constructor, is an invalid
	 * object.
	 */
	egd_markerSetUpPulledBall_t();



	/*
	 * =========================
	 * Query Functions
	 */
public:


	/*
	 * =========================
	 * Interface Functions
	 */
public:
	using Base_t::creatProblem;


	/**
	 * \brief	This function outputs information about
	 * 		 the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/**
	 * \brief	This function will configure the dumper.
	 *
	 * This function will add the density, viscosity and the
	 * type property to the set of constant marker properties.
	 * No other operations are done.
	 * See also the rehology for more actions performed on
	 * the dumper.
	 *
	 * \param  dumper	This si is the dumper.
	 *
	 * \note 	This function was a bit more agressive before.
	 */
	virtual
	void
	inspectDumper(
		egd_dumperFile_t& 	dumper)
	 const
	 final;


	/**
	 * \brief	This function is used to inspect the rehology object.
	 *
	 * This function will read the velocity from the passed rehology object.
	 * Note if the velocities where provided, meaning they are not NAN,
	 * then they are set. otherwise this function does nothing.
	 * If the velocities vere not provided during construction or by
	 * the Rehology, an error is generated.
	 *
	 * \param  rheology	This is the rehology.
	 */
	virtual
	void
	inspectRehology(
		MarkerRehology_i* const 	rehology)
	 final;


	/**
	 * \brief	This function will inspect the solver.
	 *
	 * It will first call its base implementation and then
	 * will configure the solver, by passing x0 and y0.
	 */
	virtual
	void
	inspectSolver(
		StokesSolver_i* const 		solver)
	 override;


	/*
	 * ============================
	 * Hooks
	 */
protected:
	using Base_t::hook_creatMarkerPositions;
	using Base_t::hook_creatGridGeometry;
	using Base_t::hook_creatGridPropertyList;
	using Base_t::hook_postProcessGridProperties;


	/*
	 * =============================
	 * Needed Hooks
	 */
protected:
	/**
	 * \brief	This function creates the property list of the markers.
	 *
	 * This will add all the needed properties.
	 *
	 */
	virtual
	PropList_t
	hook_creatMarkerProperties()
	 const
	 final;


	/**
	 * \brief	This function determines in which material the given point has.
	 *
	 * \param  x	The x coordinate of the position.
	 * \param  y 	The y coordinate of the position.
	 */
	virtual
	Index_t
	hook_findMaterial(
		const Numeric_t 	x,
		const Numeric_t 	y)
	 const
	 final;


	/**
	 * \brief	This hook is called to fill the marker
	 * 		 property collection.
	 *
	 * This function is called after the marker collection is
	 * build. It should set the markers properties to theier
	 * initial values.
	 *
	 * Note that the type property is allready set by the
	 * driver code. So the user can use them to simplyify
	 * implementation.
	 *
	 * \param  mColl 	The collection that should be set.
	 */
	virtual
	bool
	hool_setMarkerProperties(
		MarkerCollection_t& 		mColl)
	 const
	 final;




	/*
	 * ===============================
	 * Interface helper
	 *
	 * These are functions that are provided by the interface
	 * They are non virtual
	 */
protected:
	using Base_t::getTempGrid;
	using Base_t::wasTempSolverInspected;
	using Base_t::getStokeGrid;
	using Base_t::wasStokesSolverInspected;
	using Base_t::isTempOnExtGrid;
	using Base_t::isStokeOnExtGrid;
	using Base_t::getSolverPropMap;



	/*
	 * =======================
	 * Internal Access functions
	 *
	 * These functions allows to access some private
	 * variables of *this.
	 */
protected:
	using Base_t::xDomStart;
	using Base_t::yDomStart;
	using Base_t::xDomLength;
	using Base_t::yDomLength;
	using Base_t::gridPointsX;
	using Base_t::gridPointsY;
	using Base_t::markerDensityX;
	using Base_t::markerDensityY;
	using Base_t::randDistribution;


	/*
	 * ====================
	 * Material hook
	 */
protected:
	/**
	 * \brief	This function sets up the internal state of *this.
	 *
	 * This function will set the density and the viscosity parameter
	 * of both the fluid and the ball.
	 *
	 * This function is implemented in the constructor file.
	 */
	virtual
	void
	hook_setUpInternals()
	 override;



	/*
	 * ============================
	 * Protected Variable
	 *
	 * They are protected, such that deriving classes can access them.
	 */
protected:
	//Ball
	Numeric_t 		m_etaBall  = NAN;		//!< This is the viscosity of the ball.
	Numeric_t 		m_rhoBall  = NAN;		//!< Density of the ball.
	Numeric_t 		m_radBall  = NAN;		//!< Radius f the ball.
	Numeric_t 		m_velYBall = NAN;		//!< Initial velocity of the ball in y direction.
	Numeric_t  		m_velXBall = NAN;		//!< INitial velocity of the ball in x direction.

	//Air
	Numeric_t 		m_etaAir   = NAN;		//!< This is the viscosity of the ball.
	Numeric_t 		m_rhoAir   = NAN;		//!< This is teh density of the ball.
}; //End: class(egd_markerSetUpPulledBall_t)

PGL_NS_END(egd)

