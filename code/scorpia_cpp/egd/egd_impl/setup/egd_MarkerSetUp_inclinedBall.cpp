/**
 * \brief	This file contains some of the functions of the inclined ball test szenario.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_inclinedBall.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif

std::string
egd_markerSetUpInclinedBall_t::print()
 const
{
	return (std::string("Inclined Ball.")
			+ " Lx  = " + std::to_string(this->xDomLength()             ) + "m;"
			+ " Ly  = " + std::to_string(this->yDomLength()             ) + "m;"
			+ " Nx  = " + std::to_string(this->gridPointsX()            ) + ";"
			+ " Ny  = " + std::to_string(this->gridPointsY()            ) + ";"
			+ " Mx  = " + std::to_string(this->markerDensityX() 	    ) + ";"
			+ " My  = " + std::to_string(this->markerDensityY()         ) + ";"
			+ " vel = " + std::to_string(this->m_velBall                ) + "m/s;"
			+ " ang = " + std::to_string(pgl::toDeg(-this->m_angleBall) ) + "deg;"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


void
egd_markerSetUpInclinedBall_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	//Every marker property is constant, beside the velocity
	dumper.addConstantMarkerProperty(PropIdx::Density()  )
	      .addConstantMarkerProperty(PropIdx::Viscosity())
	      .addConstantMarkerProperty(PropIdx::Type()     );

	//no temperature must be saved.
	dumper.noTempSol();

	return;
}; //End: marker



/*
 * ============================================
 * Constructors
 */

egd_markerSetUpInclinedBall_t::egd_markerSetUpInclinedBall_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		ballAngleDeg,
	const Numeric_t 		ballMagVel,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 3.0, 3.0,		//domain extension (Ly, Lx)
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 confObj)
{
using ::pgl::isValidFloat;

	if((isValidFloat(ballMagVel) == false) ||
	   (ballMagVel <= 0.0                )   )
	{
		throw PGL_EXCEPT_InvArg("The passed ball velocity value is bad it was " + std::to_string(ballMagVel));
	};

	if(isValidFloat(ballAngleDeg) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed angle of the ball is an invalid number.");
	};
	if(!((0.0 <= ballAngleDeg) && (ballAngleDeg <= 90.0)))
	{
		throw PGL_EXCEPT_InvArg("The passed angle is outside of the allowed range of [0.0, 90] degrees, it was " + std::to_string(ballAngleDeg));
	};

	/*
	 * If one makes a drawing it gets pretty clear.
	 * If we swap the sign of the angle, we transform it
	 * from the "normal" or rather my, coordinate system
	 * into the one that is used here.
	 */
	m_angleBall = pgl::toRad(-ballAngleDeg);

	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpInclinedBall_t::egd_markerSetUpInclinedBall_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerSetUpInclinedBall_t(
  		yNodeIdx_t(confObj.getNy()), xNodeIdx_t(confObj.getNx()),
  		confObj.getMarkerDensityY(), confObj.getMarkerDensityX(),
  		confObj.setUpPar("ballAngle", 45.0),	//Angle of the ball
  		confObj.setUpPar("ballVel"  ,  4.0),	//Magnitude of the velocity of the ball.
  		confObj.randomlyDisturbeMarkers(),
  		&confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpInclinedBall_t::~egd_markerSetUpInclinedBall_t()
 = default;


egd_markerSetUpInclinedBall_t::egd_markerSetUpInclinedBall_t(
	const egd_markerSetUpInclinedBall_t&)
 = default;


egd_markerSetUpInclinedBall_t&
egd_markerSetUpInclinedBall_t::operator= (
	const egd_markerSetUpInclinedBall_t&)
 = default;


egd_markerSetUpInclinedBall_t::egd_markerSetUpInclinedBall_t(
	egd_markerSetUpInclinedBall_t&&)
 noexcept
 = default;


egd_markerSetUpInclinedBall_t&
egd_markerSetUpInclinedBall_t::operator= (
	egd_markerSetUpInclinedBall_t&&)
 = default;

PGL_NS_END(egd)

