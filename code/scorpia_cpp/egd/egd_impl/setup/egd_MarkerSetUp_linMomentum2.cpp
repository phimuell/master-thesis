/**
 * \brief	This is the configuration for the linear momentum 2.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_linMomentum2.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif

std::string
egd_markerSetUpLinMomentum2_t::print()
 const
{
	return (std::string("Linear Momentum 2 Test.")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " Vx = " + std::to_string(this->m_velXBall       ) + "m/s;"
			+ " Vy = " + std::to_string(this->m_velYBall       ) + "m/s;"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpLinMomentum2_t::egd_markerSetUpLinMomentum2_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpLinMomentum2_t::egd_markerSetUpLinMomentum2_t()
 :
  Base_t()
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpLinMomentum2_t::egd_markerSetUpLinMomentum2_t(
	const egd_confObj_t& 		confObj)
 :
  Base_t(confObj)
{
	this->hook_setUpInternals();
}; // End: building constructor


egd_markerSetUpLinMomentum2_t::~egd_markerSetUpLinMomentum2_t()
 = default;


egd_markerSetUpLinMomentum2_t::egd_markerSetUpLinMomentum2_t(
	const egd_markerSetUpLinMomentum2_t&)
 = default;


egd_markerSetUpLinMomentum2_t&
egd_markerSetUpLinMomentum2_t::operator= (
	const egd_markerSetUpLinMomentum2_t&)
 = default;


egd_markerSetUpLinMomentum2_t::egd_markerSetUpLinMomentum2_t(
	egd_markerSetUpLinMomentum2_t&&)
 noexcept
 = default;


egd_markerSetUpLinMomentum2_t&
egd_markerSetUpLinMomentum2_t::operator= (
	egd_markerSetUpLinMomentum2_t&&)
 = default;


void
egd_markerSetUpLinMomentum2_t::hook_setUpInternals()
{
	/*	 BALL	 */
	m_etaBall  = 	           1e+8;		//Pitch
	m_rhoBall  = 		3200.0 ;		//3200	kg / m^3 ; Real density
	m_radBall  = 		   0.20;		//In meters
	m_velXBall =   	          -4.0 ;		//Flowing to the left, because of negative sign.
	m_velYBall =               0.0 ;		//No movement


	/*	AIR	*/
	m_etaAir  = 		   18.5e-5;		// 10 times the real viscosity Pa s
	m_rhoAir  = 		    1.2   ;		//Real density in kg / m^3

	return;
}; //End: set initial values


PGL_NS_END(egd)

