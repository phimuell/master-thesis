#pragma once
/**
 * \brief	This class is the setup process for a very simple ball test of the inertia solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUpBase.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)


/**
 * \class 	egd_markerSetUpBallTwo_t
 * \brief	This class creates the simnple ball case in the second incranation.
 *
 * In this setting there is a ball at the top of the domain.
 * The ball is initially at rest.
 */
class egd_markerSetUpBallTwo_t : public egd_markerSetUpBase_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_markerSetUpBase_t;			//!< This is the base implementation
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::PropIdx_t;
	using Base_t::PropList_t;
	using Base_t::PropToGridMap_t;

	using Base_t::MarkerCollection_t;
	using Base_t::MarkerPositions_t;
	using Base_t::MarkerProperty_t;
	using Base_t::CreationResult_t;

	/*
	 * Brief	This are the name of the different materials we have.
	 */
	const static Index_t TY_STICKY_AIR 	= 0;		//This is the air
	const static Index_t TY_BALL            = 1;		//This is the ball
	const static Index_t TY_N 		= 2;


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_markerSetUpBallTwo_t();


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * This constructor will be forwarded to the base implementation.
	 * It will then set up the internal settings of the prblem.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  yLengthM	The extension of the domain in y direction, in METERS.
	 * \param  xLengthM	The extension of the domain in x direction, in METERS.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  markerRand	Displace the markers randomly.
	 */
	egd_markerSetUpBallTwo_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const Numeric_t 	yLengthM,
		const Numeric_t 	xLengthM,
		const Index_t 		nMarkerY,
		const Index_t 		nMarkerX,
		const bool 		markerRand);



	/**
	 * \brief	Building constructor with
	 * 		 config object.
	 *
	 * Will be forwarded to the base's constructor
	 * and set up the paramters of this.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_markerSetUpBallTwo_t(
		const egd_confObj_t&	confObj);



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBallTwo_t(
		const egd_markerSetUpBallTwo_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBallTwo_t&
	operator= (
		const egd_markerSetUpBallTwo_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_markerSetUpBallTwo_t(
		egd_markerSetUpBallTwo_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBallTwo_t&
	operator= (
		egd_markerSetUpBallTwo_t&&);


	/*
	 * =====================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	Default constructor.
	 *
	 * Is only used internaly. The result
	 * of this constructor, is an invalid
	 * object.
	 */
	egd_markerSetUpBallTwo_t();



	/*
	 * =========================
	 * Query Functions
	 */
public:



	/*
	 * =========================
	 * Interface Functions
	 */
public:
	using Base_t::creatProblem;


	/**
	 * \brief	This function outputs information about
	 * 		 the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/**
	 * \brief	This function will ignore Density and viscosity on
	 * 		 the markers.
	 *
	 * \param  dumper	This si is the dumper.
	 */
	virtual
	void
	inspectDumper(
		egd_dumperFile_t& 	dumper)
	 const
	 override;


	/*
	 * ============================
	 * Hooks
	 */
protected:
	using Base_t::hook_creatMarkerPositions;
	using Base_t::hook_creatGridGeometry;
	using Base_t::hook_creatGridPropertyList;
	using Base_t::hook_postProcessGridProperties;


	/*
	 * =============================
	 * Needed Hooks
	 */
protected:
	/**
	 * \brief	This function creates the property list of the markers.
	 *
	 * This will add all the needed properties.
	 *
	 */
	virtual
	PropList_t
	hook_creatMarkerProperties()
	 const
	 override;


	/**
	 * \brief	This function determines in which material the given point has.
	 *
	 * \param  x	The x coordinate of the position.
	 * \param  y 	The y coordinate of the position.
	 */
	virtual
	Index_t
	hook_findMaterial(
		const Numeric_t 	x,
		const Numeric_t 	y)
	 const
	 override;


	/**
	 * \brief	This hook is called to fill the marker
	 * 		 property collection.
	 *
	 * This function is called after the marker collection is
	 * build. It should set the markers properties to theier
	 * initial values.
	 *
	 * Note that the type property is allready set by the
	 * driver code. So the user can use them to simplyify
	 * implementation.
	 *
	 * \param  mColl 	The collection that should be set.
	 */
	virtual
	bool
	hool_setMarkerProperties(
		MarkerCollection_t& 		mColl)
	 const
	 override;





	/*
	 * ===============================
	 * Interface helper
	 *
	 * These are functions that are provided by the interface
	 * They are non virtual
	 */
protected:
	using Base_t::getTempGrid;
	using Base_t::wasTempSolverInspected;
	using Base_t::getStokeGrid;
	using Base_t::wasStokesSolverInspected;
	using Base_t::isTempOnExtGrid;
	using Base_t::isStokeOnExtGrid;
	using Base_t::getSolverPropMap;



	/*
	 * =======================
	 * Internal Access functions
	 *
	 * These functions allows to access some private
	 * variables of *this.
	 */
protected:
	using Base_t::xDomStart;
	using Base_t::yDomStart;
	using Base_t::xDomLength;
	using Base_t::yDomLength;
	using Base_t::gridPointsX;
	using Base_t::gridPointsY;
	using Base_t::markerDensityX;
	using Base_t::markerDensityY;
	using Base_t::randDistribution;


	/*
	 * ====================
	 * Material hooks.
	 */
protected:
	/**
	 * \brief	This function sets up the internal state of *this.
	 *
	 * Variables that are not needed are set to zero.
	 */
	virtual
	void
	hook_setUpInternals()
	 override;


	/*
	 * ============================
	 * Private Variable
	 */
private:
	//Ball
	Numeric_t 		m_etaBall  = NAN;		//!< This is the viscosity of the ball.
	Numeric_t 		m_rhoBall  = NAN;		//!< Density of the ball.
	Numeric_t 		m_radBall  = NAN;		//!< Radius f the ball.

	//Air
	Numeric_t 		m_etaAir   = NAN;		//!< This is the viscosity of the ball.
	Numeric_t 		m_rhoAir   = NAN;		//!< This is teh density of the ball.
}; //End: class(egd_markerSetUpBallTwo_t)

PGL_NS_END(egd)

