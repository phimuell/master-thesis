#pragma once
/**
 * \brief	This class is the setup process for a very simple ball test of the inertia solver.
 *
 * In it just a ball that has an upwards trend is implemented.
 * Hower this time the viscosity of the air is zero.l
 * Thecniaclly the solver should be hable to handle, at least the NS solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_simpleBall.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)


/**
 * \class 	egd_markerSetUpBallZeroEta_t
 * \brief	This class creates the simnple ball case.
 *
 * But the surrounding fluid has a larger viscosity.
 */
class egd_markerSetUpBallZeroEta_t : public egd_markerSetUpBallOne_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_markerSetUpBallOne_t;			//!< This is the base implementation
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::PropIdx_t;
	using Base_t::PropList_t;
	using Base_t::PropToGridMap_t;

	using Base_t::MarkerCollection_t;
	using Base_t::MarkerPositions_t;
	using Base_t::MarkerProperty_t;
	using Base_t::CreationResult_t;



	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_markerSetUpBallZeroEta_t();


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * This constructor will be forwarded to the base implementation.
	 * It will then set up the internal settings of the prblem.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  markerRand	Displace the markers randomly.
	 * \param  confObj	Optional pointer to the configuration object.
	 */
	egd_markerSetUpBallZeroEta_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const Index_t 			nMarkerY,
		const Index_t 			nMarkerX,
		const bool 			markerRand,
		const egd_confObj_t* const 	confObj = nullptr);



	/**
	 * \brief	Building constructor with
	 * 		 config object.
	 *
	 * Will be forwarded to the base's constructor
	 * and set up the paramters of this.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_markerSetUpBallZeroEta_t(
		const egd_confObj_t&	confObj);



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBallZeroEta_t(
		const egd_markerSetUpBallZeroEta_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBallZeroEta_t&
	operator= (
		const egd_markerSetUpBallZeroEta_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_markerSetUpBallZeroEta_t(
		egd_markerSetUpBallZeroEta_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpBallZeroEta_t&
	operator= (
		egd_markerSetUpBallZeroEta_t&&);


	/*
	 * =====================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	Default constructor.
	 *
	 * Is only used internaly. The result
	 * of this constructor, is an invalid
	 * object.
	 */
	egd_markerSetUpBallZeroEta_t();



	/*
	 * =========================
	 * Query Functions
	 */
public:



	/*
	 * =========================
	 * Interface Functions
	 */
public:
	using Base_t::creatProblem;


	/**
	 * \brief	This function outputs information about
	 * 		 the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;

	using Base_t::inspectDumper;


	/*
	 * ============================
	 * Hooks
	 */
protected:
	using Base_t::hook_creatMarkerPositions;
	using Base_t::hook_creatGridGeometry;
	using Base_t::hook_creatGridPropertyList;
	using Base_t::hook_postProcessGridProperties;


	/*
	 * =============================
	 * Needed Hooks
	 */
protected:
	using Base_t::hook_creatMarkerProperties;
	using Base_t::hook_findMaterial;
	using Base_t::hool_setMarkerProperties;





	/*
	 * ===============================
	 * Interface helper
	 *
	 * These are functions that are provided by the interface
	 * They are non virtual
	 */
protected:
	using Base_t::getTempGrid;
	using Base_t::wasTempSolverInspected;
	using Base_t::getStokeGrid;
	using Base_t::wasStokesSolverInspected;
	using Base_t::isTempOnExtGrid;
	using Base_t::isStokeOnExtGrid;
	using Base_t::getSolverPropMap;



	/*
	 * =======================
	 * Internal Access functions
	 *
	 * These functions allows to access some private
	 * variables of *this.
	 */
protected:
	using Base_t::xDomStart;
	using Base_t::yDomStart;
	using Base_t::xDomLength;
	using Base_t::yDomLength;
	using Base_t::gridPointsX;
	using Base_t::gridPointsY;
	using Base_t::markerDensityX;
	using Base_t::markerDensityY;
	using Base_t::randDistribution;


	/*
	 * ====================
	 * Material hook
	 */
protected:
	/**
	 * \brief	This function sets up the internal state of *this.
	 *
	 * This will set up, the internal states of *this.
	 * This function is virtual, such that deriving classes can
	 * change the variables.
	 */
	virtual
	void
	hook_setUpInternals()
	 override;



	/*
	 * ============================
	 * Protected Variable
	 *
	 * Variables from the base class (simple ball one)
	 * are used.
	 */
protected:
}; //End: class(egd_markerSetUpBallZeroEta_t)

PGL_NS_END(egd)

