/**
 * \brief	This function implements the functions that are needed by the zero eta version of the simple ball.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_simpleBallZeroEta.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif

std::string
egd_markerSetUpBallZeroEta_t::print()
 const
{
	return (std::string("Simple Ball, ZERO ETA.")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpBallZeroEta_t::egd_markerSetUpBallZeroEta_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const bool 			markerRand,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpBallZeroEta_t::egd_markerSetUpBallZeroEta_t()
 :
  Base_t()
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpBallZeroEta_t::egd_markerSetUpBallZeroEta_t(
	const egd_confObj_t& 		confObj)
 :
  Base_t(confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpBallZeroEta_t::~egd_markerSetUpBallZeroEta_t()
 = default;


egd_markerSetUpBallZeroEta_t::egd_markerSetUpBallZeroEta_t(
	const egd_markerSetUpBallZeroEta_t&)
 = default;


egd_markerSetUpBallZeroEta_t&
egd_markerSetUpBallZeroEta_t::operator= (
	const egd_markerSetUpBallZeroEta_t&)
 = default;


egd_markerSetUpBallZeroEta_t::egd_markerSetUpBallZeroEta_t(
	egd_markerSetUpBallZeroEta_t&&)
 noexcept
 = default;


egd_markerSetUpBallZeroEta_t&
egd_markerSetUpBallZeroEta_t::operator= (
	egd_markerSetUpBallZeroEta_t&&)
 = default;


void
egd_markerSetUpBallZeroEta_t::hook_setUpInternals()
{
	/*	 BALL	 */
	m_etaBall  = 		1000.0 ;		//~Lard
	m_rhoBall  = 		 865.64;		//https://www.aqua-calc.com/page/density-table/substance/lard
	m_radBall  = 		   0.20;		//In meters
	m_velYBall =   	          -4.0 ;		//Flowing upwards, must be negative because y is depth


	/*	AIR	*/
	m_etaAir  = 		   0.0 ;		//Zero viscosity
	m_rhoAir  = 		   1.2 ;		//Density of air in [kg / m^3]; honey does have 1.38-1.45 [kg/l]

	return;
}; //End: set initial values

PGL_NS_END(egd)

