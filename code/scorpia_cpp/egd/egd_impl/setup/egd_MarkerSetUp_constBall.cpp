/**
 * \brief	This file implements some functions of thew linear mometum test setup.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_constBall.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;


std::string
egd_markerSetUpConstBall_t::print()
 const
{
	return (std::string("Const Ball Test.")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " Vx = " + std::to_string(this->m_velXBall       ) + "m/s;"
			+ " Vy = " + std::to_string(this->m_velYBall       ) + "m/s;"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


void
egd_markerSetUpConstBall_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	//Call the vase implementation
	this->Base_t::inspectDumper(dumper);

	return;
}; //End: marker


void
egd_markerSetUpConstBall_t::hook_setUpInternals()
{
	/*	 BALL	 */
	m_etaBall  = 	           1e+8;		//Pitch
	m_rhoBall  = 		  72.0 ;		//Real world 3200 kg / m^3
	m_radBall  = 		   0.20;		//In meters


	/*	AIR	*/
	m_etaAir  = 		   18.5e-4;		//
	m_rhoAir  = 		    0.12   ;		// Real would be 1.2 kg / m^3

	return;
}; //End: set initial values


void
egd_markerSetUpConstBall_t::inspectRehology(
	egd_MarkerRehology_i* const 		rehology)
{
using ::pgl::isValidFloat;

	/*
	 * Call the base version first
	 */
	this->Base_t::inspectRehology(rehology);


	/*
	 * If a velocity is NAN, at this point at least.
	 * this is not an error. but it means that the
	 * velocity was not set up yet, so we have to
	 * use the rehology
	 */
		pgl_assert(rehology != nullptr);

	//get the x velocity
	if(isValidFloat(this->m_velXBall) == false)
	{
		/* The x velocity was not given, so we use
		 * the rehology or set it to -4. */
		if(rehology->hasInitialValue(PropIdx_t::VelX(), TY_BALL) == true)
		{
			this->m_velXBall = rehology->getInitialValue(PropIdx_t::VelX(), TY_BALL);
		}
		else
		{
			this->m_velXBall = -4.0;

		};
	}; //End if: set x velocity

	//get the y velocity
	if(isValidFloat(this->m_velYBall) == false)
	{
		if(rehology->hasInitialValue(PropIdx::VelY(), TY_BALL) == true)
		{
			this->m_velYBall = rehology->getInitialValue(PropIdx_t::VelY(), TY_BALL);
		}
		else
		{
			this->m_velYBall = 0;
		};
	}; //End assigning Y

		pgl_assert(isValidFloat(this->m_velXBall),	//now it is an error
			   isValidFloat(this->m_velYBall) );

	return;
}; //End: inspect rehology





/*
 * =======================
 * Constructors
 */

egd_markerSetUpConstBall_t::egd_markerSetUpConstBall_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		Vy,
	const Numeric_t 		Vx,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();

	/*
	 * Note that the VX and VY must not be non nan
	 */
	this->m_velXBall = Vx;
	this->m_velYBall = Vy;

}; //End: building constructor


egd_markerSetUpConstBall_t::egd_markerSetUpConstBall_t()
 :
  egd_markerSetUpConstBall_t(
  	yNodeIdx_t(101), xNodeIdx_t(301),	//grid points
  	5, 5,					//Marker density
  	NAN, NAN,				//NAN instructs the implementation to read it from the rheology before using the default values
  	false,					//no ransom displacement
  	nullptr)				//No configuration object aviable
{}; //End: default constructor


egd_markerSetUpConstBall_t::egd_markerSetUpConstBall_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerSetUpConstBall_t(
  		yNodeIdx_t(confObj.getNy()), xNodeIdx_t(confObj.getNx()),
  		confObj.getMarkerDensityY(), confObj.getMarkerDensityX(),
  		confObj.setUpParNAN("Vy"), confObj.setUpParNAN("Vx"),
  		confObj.randomlyDisturbeMarkers(),
  		&confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpConstBall_t::~egd_markerSetUpConstBall_t()
 = default;


egd_markerSetUpConstBall_t::egd_markerSetUpConstBall_t(
	const egd_markerSetUpConstBall_t&)
 = default;


egd_markerSetUpConstBall_t&
egd_markerSetUpConstBall_t::operator= (
	const egd_markerSetUpConstBall_t&)
 = default;


egd_markerSetUpConstBall_t::egd_markerSetUpConstBall_t(
	egd_markerSetUpConstBall_t&&)
 noexcept
 = default;


egd_markerSetUpConstBall_t&
egd_markerSetUpConstBall_t::operator= (
	egd_markerSetUpConstBall_t&&)
 = default;


PGL_NS_END(egd)

