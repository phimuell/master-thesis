/**
 * \brief	This file contains unspecific functions of the set upper of the original project.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUpBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>



//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

using pgl::isValidFloat;


bool
egd_markerSetUpBase_t::priv_testPropertyLists(
 	const PropToGridMap_t& 		gProps,
 	const PropList_t& 		mProps)
 const
{
	if(gProps.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid property list is not valid.");
	};
	if(gProps.nProps() == 0)
	{
		throw PGL_EXCEPT_InvArg("The grid property map is empty.");
	};
	if(PropIdx::isValidList(mProps, true) == false)
	{
		throw PGL_EXCEPT_InvArg("The marker property list is nbot valid.");
	};
	if(mProps.empty() )
	{
		throw PGL_EXCEPT_InvArg("The markers list is empty.");
	};


	/*
	 * Iterate through the grid properties
	 */
	for(const auto g : gProps)
	{
		const PropIdx_t& gIdx = g.first;			//Unpack
		const PropIdx_t  gRep = gIdx.getRepresentant();		//Make representant out of it

		if(PropIdx::isPropertyInList(mProps, gRep) == false)
		{
			pgl_assert(false && "Found a non represented grid property.");
			return false;
		};
	}; //ENd for(g)


	return true;
}; //End: test for compatibility


void
egd_markerSetUpBase_t::priv_setTypeProperty(
	MarkerCollection_t* const 	mColl_)
 const
{
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;	//Alias

	const MarkerPositions_t& X = mColl.getMarkerProperty(PropIdx::PosX() );	//References to position
	const MarkerPositions_t& Y = mColl.getMarkerProperty(PropIdx::PosY() );

	MarkerProperty_t& TYPE = mColl.getMarkerProperty(PropIdx::Type() );	//Type which we modify

	if(X.size() !=    Y.size() ||
	   X.size() != TYPE.size()   )
	{
		throw PGL_EXCEPT_RUNTIME("Different sizes detected");
	}; //End if: check sizes


	const Index_t nMarkers = TYPE.size();	//Number of markers
		pgl_assert(nMarkers > 0);

	for(Index_t m = 0; m != nMarkers; ++m)
	{
		const Numeric_t x = X[m];
		const Numeric_t y = Y[m];
			pgl_assert(isValidFloat(x)    , isValidFloat(y),
				   m_domainXStart <= x, x <= m_domainXStart + m_domainXLength,
				   m_domainYStart <= y, y <= m_domainYStart + m_domainYLength );

		const Index_t type = this->hook_findMaterial(x, y);

		TYPE[m] = type;
	}; //End for(m):

	return;
}; //ENd: setup property type


void
egd_markerSetUpBase_t::priv_finalizeGridPropertyList(
	PropToGridMap_t* const 		gProp_,
	const PropList_t& 		mProp)
 const
{
	pgl_assert(gProp_ != nullptr);		//Alias argument
	PropToGridMap_t& gProp = *gProp_;

	if(gProp.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property map.");
	};


	/*
	 * Now we add the properties that are needed/requiered by the solver.
	 * We have this properties, from the inspection.
	 */
	const PropToGridMap_t& solverProp = this->getSolverPropMap();

	//Add them savely
	gProp.saveAdding(solverProp);

	/*
	 * This is only for plotting.
	 * This will add plot properties.
	 */
#if defined(EGD_DUMP_PLOT_PROPERTY) && EGD_DUMP_PLOT_PROPERTY == 1
#	pragma message "Added helper properties to the grid for plotting the temperature."

	//only add it if also in the markers list
	if(PropIdx::isPropertyInList(mProp, PropIdx::Temperature().getRepresentant()) )
	{
		gProp.addMapping(PropIdx::TemperaturePlotCC(), mkExt(eGridType::CellCenter));
		gProp.addMapping(PropIdx::TemperaturePlotBG(), mkReg(eGridType::BasicNode ));
	};
#else
	(void)mProp;
#endif

	return;
}; //ENd: finalize map


PGL_NS_END(egd)


