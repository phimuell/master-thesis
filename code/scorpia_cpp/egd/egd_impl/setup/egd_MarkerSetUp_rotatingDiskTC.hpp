#pragma once
/**
 * \brief	This class implements a rotating disc for a Taylor Coette flow.
 *
 * The disc rotates at a given speed, it is suppoeed to be powered.
 * There is an outer cilinder, that that is supposed to be fix.
 * The fluid is initially is rotating, but not in an equlibrium.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUpBase.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)


/**
 * \class 	egd_markerSetUpRotDiscTC_t
 * \brief	This class creates the rotating disc scenario for the Taylor Coette flow.
 */
class egd_markerSetUpRotDiscTC_t : public egd_markerSetUpBase_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_markerSetUpBase_t;			//!< This is the base implementation
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::PropIdx_t;
	using Base_t::PropList_t;
	using Base_t::PropToGridMap_t;

	using Base_t::MarkerCollection_t;
	using Base_t::MarkerPositions_t;
	using Base_t::MarkerProperty_t;
	using Base_t::CreationResult_t;

	/*
	 * Brief	This are the name of the different materials we have.
	 */
	const static Index_t TY_STICKY_AIR 	= 0;		//This is the air between the discs.
	const static Index_t TY_DISC            = 1;		//This is the inner disc.
	const static Index_t TY_WALL            = 2;		//This is the outer wall.
	const static Index_t TY_OUT_FLUID       = 3;		//This is the fluid beond the wall.
	const static Index_t TY_N 		= 4;


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_markerSetUpRotDiscTC_t();


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * Note that it will place the centre of the disc at the
	 * origin of the coordiante system.
	 * The constructor allows to set the angular velocity of the ball.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  angularVel	The angular velocit of the inner disc.
	 * \param  angVelOut	Anglar velocity of the outer cylinder.
	 * \param  markerRand	Displace the markers randomly.
	 */
	egd_markerSetUpRotDiscTC_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const Index_t 		nMarkerY,
		const Index_t 		nMarkerX,
		const Numeric_t 	angularVel,
		const Numeric_t 	angVelOut,
		const bool 		markerRand,
		const egd_confObj_t* const 	confObj = nullptr);



	/**
	 * \brief	Building constructor with
	 * 		 config object.
	 *
	 * This constructor will examine the configuration
	 * object, and forward the request to the building
	 * constructor.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_markerSetUpRotDiscTC_t(
		const egd_confObj_t&	confObj);



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDiscTC_t(
		const egd_markerSetUpRotDiscTC_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDiscTC_t&
	operator= (
		const egd_markerSetUpRotDiscTC_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_markerSetUpRotDiscTC_t(
		egd_markerSetUpRotDiscTC_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDiscTC_t&
	operator= (
		egd_markerSetUpRotDiscTC_t&&);


	/*
	 * =====================
	 * Protected Constructors
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is only used internaly. The result
	 * of this constructor, is an invalid
	 * object.
	 */
	egd_markerSetUpRotDiscTC_t();



	/*
	 * =========================
	 * Query Functions
	 */
public:



	/*
	 * =========================
	 * Interface Functions
	 */
public:
	using Base_t::creatProblem;


	/**
	 * \brief	This function outputs information about
	 * 		 the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/**
	 * \brief	This function will ignore Density and viscosity on
	 * 		 the markers.
	 *
	 * \param  dumper	This si is the dumper.
	 */
	virtual
	void
	inspectDumper(
		egd_dumperFile_t& 	dumper)
	 const
	 final;


	/*
	 * ============================
	 * Hooks
	 */
protected:
	using Base_t::hook_creatMarkerPositions;
	using Base_t::hook_creatGridGeometry;
	using Base_t::hook_creatGridPropertyList;
	using Base_t::hook_postProcessGridProperties;


	/*
	 * =============================
	 * Needed Hooks
	 */
protected:
	/**
	 * \brief	This function creates the property list of the markers.
	 *
	 * This will add all the needed properties.
	 *
	 */
	virtual
	PropList_t
	hook_creatMarkerProperties()
	 const
	 final;


	/**
	 * \brief	This function determines in which material the given point has.
	 *
	 * \param  x	The x coordinate of the position.
	 * \param  y 	The y coordinate of the position.
	 */
	virtual
	Index_t
	hook_findMaterial(
		const Numeric_t 	x,
		const Numeric_t 	y)
	 const
	 final;


	/**
	 * \brief	This hook is called to fill the marker
	 * 		 property collection.
	 *
	 * This function is called after the marker collection is
	 * build. It should set the markers properties to theier
	 * initial values.
	 *
	 * Note that the type property is allready set by the
	 * driver code. So the user can use them to simplyify
	 * implementation.
	 *
	 * \param  mColl 	The collection that should be set.
	 */
	virtual
	bool
	hool_setMarkerProperties(
		MarkerCollection_t& 		mColl)
	 const
	 final;





	/*
	 * ===============================
	 * Interface helper
	 *
	 * These are functions that are provided by the interface
	 * They are non virtual
	 */
protected:
	using Base_t::getTempGrid;
	using Base_t::wasTempSolverInspected;
	using Base_t::getStokeGrid;
	using Base_t::wasStokesSolverInspected;
	using Base_t::isTempOnExtGrid;
	using Base_t::isStokeOnExtGrid;
	using Base_t::getSolverPropMap;



	/*
	 * =======================
	 * Internal Access functions
	 *
	 * These functions allows to access some private
	 * variables of *this.
	 */
protected:
	using Base_t::xDomStart;
	using Base_t::yDomStart;
	using Base_t::xDomLength;
	using Base_t::yDomLength;
	using Base_t::gridPointsX;
	using Base_t::gridPointsY;
	using Base_t::markerDensityX;
	using Base_t::markerDensityY;
	using Base_t::randDistribution;


	/*
	 * ====================
	 * Material hook
	 */
protected:
	/**
	 * \brief	This function sets up the internal state of *this.
	 *
	 * This functin will set all internal parameters to their default value.
	 * With teh exception of the angular momentum.
	 * If this value is nan an error is generated.
	 */
	virtual
	void
	hook_setUpInternals()
	 override;



	/*
	 * ==========================
	 * Helper functions
	 */
protected:

	/**
	 * \brief	This function returns the radial distance at which the outer fluid begins.
	 *
	 * This is teh fluid that lies beyond the outer cylinder.
	 */
	virtual
	Numeric_t
	getBeyondRadius()
	 const
	 final;

	/*
	 * ============================
	 * Protected Variable
	 *
	 * They are protected, such that deriving classes can access them.
	 */
protected:
	//Ball
	Numeric_t 		m_etaBall    = NAN;		//!< This is the viscosity of the ball.
	Numeric_t 		m_rhoBall    = NAN;		//!< Density of the ball.
	Numeric_t 		m_angularVel = NAN;		//!< Angular velocity of the inner disc.
	Numeric_t 		m_angVelOut  = NAN;		//!< Angular velocity of the outer cylinder.
	Numeric_t 		m_radBall    = NAN;		//!< Radius f the ball.

	//outer wall
	Numeric_t 		m_outRad     = NAN;		//!< Radius of the outer wall.
	Numeric_t 		m_outEta     = NAN;		//!< Viscosity of the outer wall.
	Numeric_t 		m_outRho     = NAN;		//!< Density of the outer wall.
	Numeric_t 		m_outWidth   = NAN;		//!< The width of the outer sylinder.

	//Air; between the cylinders
	Numeric_t 		m_etaAir   = NAN;		//!< This is the viscosity of the ball.
	Numeric_t 		m_rhoAir   = NAN;		//!< This is teh density of the ball.

	//outer
	Numeric_t 		m_outFRho  = NAN;		//!< Density of the fluid beyond the outer cylinder.
	Numeric_t 		m_outFEta  = NAN;		//!< Viscosity of the fluid beyond the outer cylinder.
}; //End: class(egd_markerSetUpRotDiscTC_t)

PGL_NS_END(egd)

