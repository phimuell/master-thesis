/**
 * \brief	This file implements the constructors of the the rotating disc scenario in TC Mode.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_rotatingDiskTC.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif

using pgl::isValidFloat;



egd_markerSetUpRotDiscTC_t::egd_markerSetUpRotDiscTC_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		angularVel,
	const Numeric_t 		angVelOut,
	const bool 			markerRand,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Ny, Nx,
  	 1.2, 1.2,		//1.2m diameter
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 -0.6, -0.6,		//move the start of teh coordinate system.
  	 confObj)
{
	if(isValidFloat(angularVel) == false)
	{
		throw PGL_EXCEPT_InvArg("The angular velocity of the inner disc is not a valid number.");
	};
	if(isValidFloat(angVelOut) == false)
	{
		throw PGL_EXCEPT_InvArg("The angular velocity of the outer cylinder is invalid.");
	};

	this->m_angularVel = angularVel;
	this->m_angVelOut  = angVelOut;

	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpRotDiscTC_t::egd_markerSetUpRotDiscTC_t()
 :
  egd_markerSetUpRotDiscTC_t(
  		yNodeIdx_t(121), xNodeIdx_t(121),
  		5, 5,
  		40.0,	//Angular velocity inner disc
  		0.0,	//Angular velocity outer cylinder
  		false)
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpRotDiscTC_t::egd_markerSetUpRotDiscTC_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerSetUpRotDiscTC_t(
  		yNodeIdx_t(confObj.getNy()), xNodeIdx_t(confObj.getNx()),
  		confObj.getMarkerDensityY(), confObj.getMarkerDensityX(),
  		confObj.setUpPar("angularvel", 40.0),
  		confObj.setUpPar("angularvelout", 0.0),
  		confObj.randomlyDisturbeMarkers(),
  		&confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
}; // End: building constructor

egd_markerSetUpRotDiscTC_t::~egd_markerSetUpRotDiscTC_t()
 = default;


egd_markerSetUpRotDiscTC_t::egd_markerSetUpRotDiscTC_t(
	const egd_markerSetUpRotDiscTC_t&)
 = default;


egd_markerSetUpRotDiscTC_t&
egd_markerSetUpRotDiscTC_t::operator= (
	const egd_markerSetUpRotDiscTC_t&)
 = default;


egd_markerSetUpRotDiscTC_t::egd_markerSetUpRotDiscTC_t(
	egd_markerSetUpRotDiscTC_t&&)
 noexcept
 = default;


egd_markerSetUpRotDiscTC_t&
egd_markerSetUpRotDiscTC_t::operator= (
	egd_markerSetUpRotDiscTC_t&&)
 = default;


void
egd_markerSetUpRotDiscTC_t::hook_setUpInternals()
{
	if((isValidFloat(m_angularVel) == false))
	{
		throw PGL_EXCEPT_InvArg("The angular velocity of the inner disc is not set correctly, it is " + std::to_string(m_angularVel));
	};
	if((isValidFloat(m_angVelOut) == false))
	{
		throw PGL_EXCEPT_InvArg("The angular velocity of the outer cylinder is not set correctly, it is " + std::to_string(m_angularVel));
	};

	/*	 BALL	 */
	m_etaBall  = 	   1e+9;		//Pitch
	m_rhoBall  = 	 865.64;		//https://www.aqua-calc.com/page/density-table/substance/lard
	m_radBall  = 	   0.20;		//In meters

	/* Outer wall  */
	m_outRad   = 	   0.5 ;  		//In meters
	m_outWidth = 	   0.05;		// ~ 5cm
	m_outRho   =  m_rhoBall;
	m_outEta   =  m_etaBall;

	/*	AIR	(Between) */
	m_etaAir  = 	   1.0 ;
	m_rhoAir  = 	   1.2 ;

	/* AIR (Beyond) */
	m_outFEta = m_etaAir;
	m_outFRho = m_rhoAir;

	return;
}; //End: set initial values

PGL_NS_END(egd)

