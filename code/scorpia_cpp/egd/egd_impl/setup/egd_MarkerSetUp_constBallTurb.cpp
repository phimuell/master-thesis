/**
 * \brief	This file implements some functions of the constant turbulent pulled ball setting.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_constBallTurb.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;


std::string
egd_markerSetUpConstBallTurb_t::print()
 const
{
	return (std::string("Const Ball Turb Test.")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " Vx = " + std::to_string(this->m_velXBall       ) + "m/s;"
			+ " Vy = " + std::to_string(this->m_velYBall       ) + "m/s;"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


void
egd_markerSetUpConstBallTurb_t::hook_setUpInternals()
{
	/*	 BALL	 */
	m_etaBall  = 	           1e+8;		//Pitch
	m_rhoBall  = 		  72.0 ;		//Real world 3200 kg / m^3
	m_radBall  = 		   0.20;		//In meters


	/*	AIR	*/
	m_etaAir  = 		   18.5e-5;		// 10 times the real viscosity Pa s
	m_rhoAir  = 		    12.0;		// Real would be 1.2 kg / m^3

	return;
}; //End: set initial values




/*
 * =======================
 * Constructors
 */

egd_markerSetUpConstBallTurb_t::egd_markerSetUpConstBallTurb_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		Vy,
	const Numeric_t 		Vx,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 Vy, Vx,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();

	/*
	 * Note that the VX and VY must not be non nan
	 */
	this->m_velXBall = Vx;
	this->m_velYBall = Vy;

}; //End: building constructor


egd_markerSetUpConstBallTurb_t::egd_markerSetUpConstBallTurb_t()
 :
  Base_t()
{}; //End: default constructor


egd_markerSetUpConstBallTurb_t::egd_markerSetUpConstBallTurb_t(
	const egd_confObj_t& 		confObj)
 :
	Base_t(confObj)
{
	this->hook_setUpInternals();
}; // End: building constructor


egd_markerSetUpConstBallTurb_t::~egd_markerSetUpConstBallTurb_t()
 = default;


egd_markerSetUpConstBallTurb_t::egd_markerSetUpConstBallTurb_t(
	const egd_markerSetUpConstBallTurb_t&)
 = default;


egd_markerSetUpConstBallTurb_t&
egd_markerSetUpConstBallTurb_t::operator= (
	const egd_markerSetUpConstBallTurb_t&)
 = default;


egd_markerSetUpConstBallTurb_t::egd_markerSetUpConstBallTurb_t(
	egd_markerSetUpConstBallTurb_t&&)
 noexcept
 = default;


egd_markerSetUpConstBallTurb_t&
egd_markerSetUpConstBallTurb_t::operator= (
	egd_markerSetUpConstBallTurb_t&&)
 = default;


PGL_NS_END(egd)

