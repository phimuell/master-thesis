/**
 * \brief	This file contains the hooks for the pulled ball setup.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_pulledBall.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)

using pgl::isValidFloat;


std::string
egd_markerSetUpPulledBall_t::print()
 const
{
	return (std::string("Pulled Ball.")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " Rb = " + std::to_string(this->m_radBall        ) + ";"
			+ " V  = (" + std::to_string(this->m_velXBall) + ", " + std::to_string(this->m_velYBall) + ");"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpPulledBall_t::PropList_t
egd_markerSetUpPulledBall_t::hook_creatMarkerProperties()
 const
{
	/*
	 * Create the default list.
	 * Do not include positions and type, this is done by the driver code.
	 */
	PropList_t props = { PropIdx::Density(), PropIdx::Viscosity(),
		             PropIdx::VelX(),    PropIdx::VelY()      };

	return props;
}; //End: make makrker property


void
egd_markerSetUpPulledBall_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	//declare the obvious properties as constant
	dumper.addConstantMarkerProperty(PropIdx::Density()   )
	      .addConstantMarkerProperty(PropIdx::Viscosity() )
	      .addConstantMarkerProperty(PropIdx::Type()      );

	dumper.ignoreMarkerVel();	//Ignore the intrinsic velocity
	dumper.dumpFeelVelocity();	//Dump the feel velocity.
	return;
}; //End: inspect dumper


void
egd_markerSetUpPulledBall_t::inspectRehology(
	egd_MarkerRehology_i* const 		rehology)
{
using ::pgl::isValidFloat;

	/*
	 * Call the base version first
	 */
	this->Base_t::inspectRehology(rehology);


	/*
	 * If a velocity is NAN, at this point at least.
	 * this is not an error. but it means that the
	 * velocity was not set up yet, so we have to
	 * use the rehology
	 */
		pgl_assert(rehology != nullptr);

	//get the x velocity
	if(isValidFloat(this->m_velXBall) == false)
	{
		/* The x velocity was not given, so we use
		 * the rehology or set it to zero. */
		if(rehology->hasInitialValue(PropIdx_t::VelX(), TY_BALL) == true)
		{
			this->m_velXBall = rehology->getInitialValue(PropIdx_t::VelX(), TY_BALL);
		}
		else
		{
			throw PGL_EXCEPT_RUNTIME("Was not able to determine the initial velocty in x direction of the ball.");
		};
	}; //End if: set x velocity

	//get the y velocity
	if(isValidFloat(this->m_velYBall) == false)
	{
		if(rehology->hasInitialValue(PropIdx::VelY(), TY_BALL) == true)
		{
			this->m_velYBall = rehology->getInitialValue(PropIdx_t::VelY(), TY_BALL);
		}
		else
		{
			throw PGL_EXCEPT_RUNTIME("Was not able to determine the initial velocty in x direction of the ball.");
		};
	}; //End assigning Y

		pgl_assert(isValidFloat(this->m_velXBall),	//now it is an error
			   isValidFloat(this->m_velYBall) );

	return;
}; //End: inspect rehology


void
egd_markerSetUpPulledBall_t::inspectSolver(
		StokesSolver_i* const 		solver)
{
		pgl_assert(solver != nullptr);
	this->Base_t::inspectSolver(solver);

	//Load the property
	const Numeric_t xStart  = this->xDomStart();
	const Numeric_t xLength = this->xDomLength();
	const Numeric_t yStart  = this->yDomStart();
	const Numeric_t yLength = this->yDomLength();

	//Safty margin
	const Numeric_t xCentre = xStart + xLength - (2.0 * this->m_radBall);
		pgl_assert(isValidFloat(xCentre));

	//Placed in the middle
	const Numeric_t yCentre = yStart + 0.5 * yLength;
		pgl_assert(isValidFloat(yCentre));


	/*
	 * Configure the solver.
	 * Ignore the success state.
	 */
	(void)(solver->setBoundaryOption("x0", xCentre));
	(void)(solver->setBoundaryOption("y0", yCentre));

	return;
}; //End: inspect solver




bool
egd_markerSetUpPulledBall_t::hool_setMarkerProperties(
	MarkerCollection_t& 		mColl)
 const
{
	using pgl::isValidFloat;

	/*
	 * Some helper functions/ claculations
	 */

	//Now we load all properties that we need
	      MarkerProperty_t& RHO   = mColl.getMarkerProperty(PropIdx::Density()   );
	      MarkerProperty_t& ETA   = mColl.getMarkerProperty(PropIdx::Viscosity() );
	      MarkerProperty_t& VELX  = mColl.getMarkerProperty(PropIdx::VelX()      );
	      MarkerProperty_t& VELY  = mColl.getMarkerProperty(PropIdx::VelY()      );
	const MarkerProperty_t& TYPE  = mColl.getMarkerProperty(PropIdx::Type()      );

	//Get the position of the markers
	const MarkerPositions_t& X = mColl.cgetXPos();
	const MarkerPositions_t& Y = mColl.cgetYPos();

	const Index_t nMarkers = X.size();

	if((isValidFloat(this->m_velXBall) == false) ||
	   (isValidFloat(this->m_velYBall) == false)   )
	{
		throw PGL_EXCEPT_RUNTIME("The velocities vere not set up correctly."
				" most likely the inspect function was not called, or they were not provided.");
	};//End: not set up

	/*
	 * Iterating through all the markers and set them
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load the positions
		const Numeric_t xm = X[m];
		const Numeric_t ym = Y[m];

		//Determine the type of the material
		const Index_t mType = Index_t(TYPE[m]);
			pgl_assert(Index_t(mType) == this->hook_findMaterial(xm, ym));

		//We will now set the velocity to zero
		VELX[m] = 0.0;
		VELY[m] = 0.0;

		//Set the specific stuff
		switch(mType)
		{
		  case TY_STICKY_AIR:
		  	RHO [m] = m_rhoAir;
		  	ETA [m] = m_etaAir;
		  break;

		  case TY_BALL:
		  	RHO [m] = m_rhoBall;
		  	ETA [m] = m_etaBall;
		  	VELY[m] = m_velYBall;
		  	VELX[m] = m_velXBall;
		  break;

		  default:
		  	throw PGL_EXCEPT_InvArg("Computed an invalid material type.");
		}; //End switch(mType):
	}; //End for(m):

	return true;
}; //End: default for setting up the problem geometry


Index_t
egd_markerSetUpPulledBall_t::hook_findMaterial(
	const Numeric_t 	x_m,
	const Numeric_t 	y_m)
 const
{
	if(x_m < this->xDomStart() )
	{
		throw PGL_EXCEPT_InvArg("X position is too small.");
	};
	if((this->xDomStart() + this->xDomLength() ) < x_m)
	{
		throw PGL_EXCEPT_InvArg("X position is too large.");
	};
	if(y_m < this->yDomStart() )
	{
		throw PGL_EXCEPT_InvArg("Y position is too small.");
	};
	if((this->yDomStart() + this->yDomLength() ) < y_m)
	{
		throw PGL_EXCEPT_InvArg("Y position is too large.");
	};
		pgl_assert(isValidFloat(m_radBall), m_radBall > 0.0);

	//Load the property
	const Numeric_t xStart  = this->xDomStart();
	const Numeric_t xLength = this->xDomLength();
	const Numeric_t yStart  = this->yDomStart();
	const Numeric_t yLength = this->yDomLength();

	//Safty margin
	const Numeric_t xCentre = xStart + xLength - (2.0 * this->m_radBall);
		pgl_assert(isValidFloat(xCentre));

	//Placed in the middle
	const Numeric_t yCentre = yStart + 0.5 * yLength;
		pgl_assert(isValidFloat(yCentre));

	//calculate the square distance from the ball centre
	const Numeric_t dist2   = std::pow(xCentre - x_m, 2.0) + std::pow(yCentre - y_m, 2.0);
		pgl_assert(isValidFloat(dist2), dist2 >= 0.0);

	/* Test if it is a ball */
	if(dist2 < (m_radBall * m_radBall))
	{
		return TY_BALL;
	}

	return TY_STICKY_AIR;
	(void)xLength;
}; //End: find location


PGL_NS_END(egd)

