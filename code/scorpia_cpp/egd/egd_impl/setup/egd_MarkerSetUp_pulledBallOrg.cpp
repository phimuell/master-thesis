/**
 * \brief	This file contains the constructors of the pulled ball settiungs, in org mode.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_pulledBallOrg.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif

std::string
egd_markerSetUpPulledBallOrg_t::print()
 const
{
	return (std::string("Pulled Ball Org.")
			+ " Lx = " + std::to_string(this->xDomLength()     ) + "m;"
			+ " Ly = " + std::to_string(this->yDomLength()     ) + "m;"
			+ " Nx = " + std::to_string(this->gridPointsX()    ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()    ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX() ) + ";"
			+ " My = " + std::to_string(this->markerDensityY() ) + ";"
			+ " Rb = " + std::to_string(this->m_radBall        ) + ";"
			+ " V  = (" + std::to_string(this->m_velXBall) + ", " + std::to_string(this->m_velYBall) + ");"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print


egd_markerSetUpPulledBallOrg_t::egd_markerSetUpPulledBallOrg_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		radBall,
	const Numeric_t 		ballVelY,
	const Numeric_t 		ballVelX,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 radBall,
  	 ballVelY, ballVelX,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpPulledBallOrg_t::egd_markerSetUpPulledBallOrg_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 confObj)
{
	this->hook_setUpInternals();
};


egd_markerSetUpPulledBallOrg_t::egd_markerSetUpPulledBallOrg_t()
 :
  Base_t()
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpPulledBallOrg_t::egd_markerSetUpPulledBallOrg_t(
	const egd_confObj_t& 		confObj)
 :
  Base_t(confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpPulledBallOrg_t::~egd_markerSetUpPulledBallOrg_t()
 = default;


egd_markerSetUpPulledBallOrg_t::egd_markerSetUpPulledBallOrg_t(
	const egd_markerSetUpPulledBallOrg_t&)
 = default;


egd_markerSetUpPulledBallOrg_t&
egd_markerSetUpPulledBallOrg_t::operator= (
	const egd_markerSetUpPulledBallOrg_t&)
 = default;


egd_markerSetUpPulledBallOrg_t::egd_markerSetUpPulledBallOrg_t(
	egd_markerSetUpPulledBallOrg_t&&)
 noexcept
 = default;


egd_markerSetUpPulledBallOrg_t&
egd_markerSetUpPulledBallOrg_t::operator= (
	egd_markerSetUpPulledBallOrg_t&&)
 = default;


void
egd_markerSetUpPulledBallOrg_t::hook_setUpInternals()
{
	/*	 BALL	 */
	m_etaBall  = 	           1e+8;		//Pitch
	m_rhoBall  = 		 3200.0;		//Real value would be 3200 kg / m^3


	/*	AIR WATER	*/
	m_etaAir  =           1.0016e-2;		// real value would be 1.0016e-3 at 20°C
	m_rhoAir  = 	   1000.0      ;		// real value would be 1000 kg / m^3

	return;
}; //End: set initial values

PGL_NS_END(egd)

