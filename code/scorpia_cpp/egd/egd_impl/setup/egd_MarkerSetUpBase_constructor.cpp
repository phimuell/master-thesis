/**
 * \brief	This file contains the implementation of the code that
 * 		 Sets up the original problem.
 *
 * This file contains the constructors.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_orgProject.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)


egd_markerSetUpBase_t::egd_markerSetUpBase_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const Numeric_t 	yLengthM,
	const Numeric_t 	xLengthM,
	const Index_t 		nMarkerY,
	const Index_t 		nMarkerX,
	const bool 		markerRand,
	const Numeric_t 	yDomStartM,
	const Numeric_t 	xDomStartM,
	const egd_confObj_t* const confObj)
 :
  egd_markerSetUpBase_t()	//Calling the default constructor will set the default values
{
	using pgl::isValidFloat;

	/*
	 * Test Ints
	 */
#define TEST(e) if((e) <= Index_t(0)) {throw PGL_EXCEPT_InvArg("The argument \"" #e "\" was invalid, it was " + std::to_string(e));}
	TEST(      Ny);
	TEST(      Nx);
	TEST(nMarkerX);
	TEST(nMarkerY);
#undef TEST

	/*
	 * Test Floats
	 */
#define TEST(e) if(((e) <= 0.0) || (::pgl::isValidFloat(e) == false) ) {throw PGL_EXCEPT_InvArg("The argument \"" #e "\" was invalid, it was " + std::to_string(e) );}
	TEST(yLengthM);
	TEST(xLengthM);
#undef TEST
#define TEST(e) if((::pgl::isValidFloat(e) == false) ) {throw PGL_EXCEPT_InvArg("The argument \"" #e "\" was invalid, it was " + std::to_string(e) );}
	TEST(yDomStartM);
	TEST(xDomStartM);
#undef TEST

	//Now we can simply modify the values
	m_gridXNPoints   = Nx;
	m_markerPerCellX = nMarkerX;
	m_gridYNPoints   = Ny;
	m_markerPerCellY = nMarkerY;
	m_markerRand     = markerRand;
	m_domainXLength  = xLengthM;
	m_domainYLength  = yLengthM;

	m_domainXStart   = xDomStartM;
	m_domainYStart   = yDomStartM;
	(void)confObj;
}; //End: building constructor


egd_markerSetUpBase_t::egd_markerSetUpBase_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx,
	const Numeric_t 	yLengthM,
	const Numeric_t 	xLengthM,
	const Index_t 		nMarkerY,
	const Index_t 		nMarkerX,
	const bool 		markerRand,
	const egd_confObj_t* const confObj)
 :
  egd_markerSetUpBase_t(
  		Ny      , Nx      ,
  		yLengthM, xLengthM,
  		nMarkerY, nMarkerX,
  		markerRand,
  		0.0     , 0.0     ,
  		confObj            )
{};


egd_markerSetUpBase_t::egd_markerSetUpBase_t()
 = default;


egd_markerSetUpBase_t::egd_markerSetUpBase_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerSetUpBase_t(
		yNodeIdx_t(confObj.getNy())      ,
		xNodeIdx_t(confObj.getNx())      ,
		confObj.domainLengthY()          ,	//Not specified an error
		confObj.domainLengthX()          ,
		confObj.getMarkerDensityY()      ,
		confObj.getMarkerDensityX()      ,
		confObj.randomlyDisturbeMarkers(),
		0.0, 0.0			 ,	//Start point of the domain
		&confObj                          )	//pass the config object
{
	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpBase_t::~egd_markerSetUpBase_t()
 = default;


egd_markerSetUpBase_t::egd_markerSetUpBase_t(
	const egd_markerSetUpBase_t&)
 = default;


egd_markerSetUpBase_t&
egd_markerSetUpBase_t::operator= (
	const egd_markerSetUpBase_t&)
 = default;


egd_markerSetUpBase_t::egd_markerSetUpBase_t(
	egd_markerSetUpBase_t&&)
 noexcept
 = default;


egd_markerSetUpBase_t&
egd_markerSetUpBase_t::operator= (
	egd_markerSetUpBase_t&&)
 = default;



PGL_NS_END(egd)

