/**
 * \brief	This file contains the function that sets up the subducing slap.
 *
 * It is a default implementation of a hook.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerSetUp_orgProject.hpp"
#include "../rheology/egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>
#include <random>


PGL_NS_START(egd)


std::string
egd_markerSetUpOrgProj_t::print()
 const
{
	return (std::string("Original set up object.")
			+ " Lx = " + std::to_string(this->xDomLength() / 1000.) + "km;"
			+ " Ly = " + std::to_string(this->yDomLength() / 1000.) + "km;"
			+ " Nx = " + std::to_string(this->gridPointsX()       ) + ";"
			+ " Ny = " + std::to_string(this->gridPointsY()       ) + ";"
			+ " Mx = " + std::to_string(this->markerDensityX()    ) + ";"
			+ " My = " + std::to_string(this->markerDensityY()    ) + ";"
			+ " rand markers: " + (this->randDistribution() == true ? "YES" : "NO") );
}; //End: print

void
egd_markerSetUpOrgProj_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	PropList_t props = { PropIdx::RadioEnergy()     , PropIdx::Type()                 ,
		             PropIdx::ThermExpansAlpha(), PropIdx::CompressibililityBeta() };

	for(const auto& pIdx : props)
	{
		dumper.addConstantMarkerProperty(pIdx);
	};

	return;
}; //End: marker

egd_markerSetUpOrgProj_t::PropList_t
egd_markerSetUpOrgProj_t::hook_creatMarkerProperties()
 const
{
	/*
	 * Create the default list.
	 * Do not include positions and type, this is done by the driver code.
	 */
	PropList_t props = { PropIdx::Density(), 	    PropIdx::Viscosity(), 	      PropIdx::RhoCp(),
			     PropIdx::ThermConduct(), 	    PropIdx::Temperature(), 	      PropIdx::RadioEnergy(),
			     PropIdx::ThermExpansAlpha(),   PropIdx::CompressibililityBeta()                          };

	return props;
}; //End: make makrker property


bool
egd_markerSetUpOrgProj_t::hool_setMarkerProperties(
	MarkerCollection_t& 		mColl)
 const
{
	using pgl::isValidFloat;

	/*
	 * Some helper functions/ claculations
	 */

	const Index_t nMarkers = mColl.nMarkers();

	//This is the function that computes the thermal conductivity
	auto calcK = [](const Numeric_t T) -> Numeric_t { return 0.73 + 1293.0 / (T + 77);};

	//This function is needed to compute the temperature in the slab.
	const Numeric_t h = 60.0 / std::sqrt(2.0);
	const Numeric_t phi_1 = 430.0;
	const Numeric_t phi_2 = 250.0;
	const Numeric_t gamma_1 = 230.0;
	const Numeric_t gamma_2 =  50.0;
	const Numeric_t p_m_g_1 = phi_1 - gamma_1;
	const Numeric_t p_m_g_2 = phi_2 - gamma_2;
	const Numeric_t helperD = std::sqrt(p_m_g_1 * p_m_g_1 + p_m_g_2 * p_m_g_2);

	//This function calculates the distance from the bottom of the slap to a point inside the slab.
	//is needed for the linear temperature profile at the beginning
	//The input arguments are in meter, but the distance is returned in klilometer
	auto d_x = [=](const Numeric_t x, const Numeric_t y) -> Numeric_t
		{
		  const Numeric_t x0 = x / 1000.0;	//Makeing kilometer
		  const Numeric_t y0 = y / 1000.0;
		  return std::abs( (phi_2 - gamma_2) * x0 - (phi_1 - gamma_1) * y0 + phi_1 * gamma_2 - phi_2 * gamma_1) / helperD;
		};


	//Now we load all properties that we need
	MarkerProperty_t& RHO   = mColl.getMarkerProperty(PropIdx::Density()               );
	MarkerProperty_t& ETA   = mColl.getMarkerProperty(PropIdx::Viscosity()             );
	MarkerProperty_t& RHOCP = mColl.getMarkerProperty(PropIdx::RhoCp()                 );
	MarkerProperty_t& K     = mColl.getMarkerProperty(PropIdx::ThermConduct()          ); //Depends on temperature
	MarkerProperty_t& TEMP  = mColl.getMarkerProperty(PropIdx::Temperature()           );
	MarkerProperty_t& HR    = mColl.getMarkerProperty(PropIdx::RadioEnergy()           );
	MarkerProperty_t& ALPHA = mColl.getMarkerProperty(PropIdx::ThermExpansAlpha()     );
	MarkerProperty_t& BETA  = mColl.getMarkerProperty(PropIdx::CompressibililityBeta() );
	MarkerProperty_t& TYPE  = mColl.getMarkerProperty(PropIdx::Type()                  );

	//Get the position of the markers
	const MarkerPositions_t& X = mColl.cgetXPos();
	const MarkerPositions_t& Y = mColl.cgetYPos();

	/*
	 * Iterating through all the markers and set them
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load the positions
		const Numeric_t xm = X[m];
		const Numeric_t ym = Y[m];

		//Determine the type of the material
		const Index_t mType = Index_t(TYPE[m]);
			pgl_assert(Index_t(mType) == this->hook_findMaterial(xm, ym));

		//Set the specific stuff
		switch(mType)
		{
		  case TY_STICKY_AIR:
			K   [m] = m_initKAir;		//Thermal conductivity of the air
			TEMP[m] = m_initTemp_Air;	//Init temperature of the mantel
		  break;

		  case TY_ROCK_MANTLE:
		  	TEMP[m] = m_initTemp_Mantle;
		  	K   [m] = calcK(TEMP[m]);
		  break;

		  case TY_ROCK_NECKING:
		  	//No special handling
		  break;

		  case TY_ROCK_PLATE:
		  	//No special handling
		  break;

		  case TY_ROCK_THORN:
		  	//No special handling needed
		  break;

		  default:
		  	throw PGL_EXCEPT_InvArg("Computed an invalid material type.");
		}; //End switch(mType):


		/*
		 * Now set the values that are in parameter bvectors
		 */
		RHO  [m]  = m_initRho  [mType];
		ETA  [m]  = m_initEta  [mType];
		BETA [m]  = m_initBeta [mType];
		TYPE [m]  = (Numeric_t)(mType);
		ALPHA[m]  = m_initAlpha[mType];
		RHOCP[m]  = m_initCp   [mType];	//Need to include RHO
		RHOCP[m] *= RHO        [m    ];	//Include rho
		HR   [m]  = 0.0               ; //Constant to zero


		/*
		 * Calculate temperature in the full slab.
		 * This is the thorn and the necking
		 * Also the thermal conductivity is calculated
		 */
		if((mType == TY_ROCK_NECKING) || (mType == TY_ROCK_THORN))
		{
			const Numeric_t x = X[m];
			const Numeric_t y = Y[m];
			TEMP[m] = m_initTemp_plateTop + d_x(x, y) * (m_initTemp_plateBottom - m_initTemp_plateTop) / h;

			K[m] = calcK(TEMP[m]);
		}; //End full slab

		//Calculate temperature and conductiviuty inside the plate
		if(mType == TY_ROCK_PLATE)
		{
			const Numeric_t y = Y[m] / 1000.0;	//Express it in kilometers
			TEMP[m] = 273.0 + (m_initTemp_plateBottom - m_initTemp_plateTop) * (y - 50.0) / 50.0;

			K[m] = calcK(TEMP[m]);
		}; //End if: plate
	}; //End for(m):


	return true;
}; //End: default for setting up the problem geometry


Index_t
egd_markerSetUpOrgProj_t::hook_findMaterial(
	const Numeric_t 	x_m,
	const Numeric_t 	y_m)
 const
{
	if(x_m < this->xDomStart() )
	{
		throw PGL_EXCEPT_InvArg("X position is too small.");
	};
	if((this->xDomStart() + this->xDomLength() ) < x_m)
	{
		throw PGL_EXCEPT_InvArg("X position is too large.");
	};
	if(y_m < this->yDomStart() )
	{
		throw PGL_EXCEPT_InvArg("Y position is too small.");
	};
	if((this->yDomStart() + this->yDomLength() ) < y_m)
	{
		throw PGL_EXCEPT_InvArg("Y position is too large.");
	};


	/*
	 * These macros represents functions that describes
	 * the bounding of the slab.
	 */
#define T1(X) (100.0 +       ((X) - 220.0))
#define T2(X) ( 50.0 +       ((X) - 230.0))
#define T3(X) (100.0 - 5.0 * ((X) - 220.0))
#define T4(X) (250.0                      )


	/*
	 * Go into kilometer.
	 * The argument is in meter, but it is more convenient to
	 * work in kilometer, so we will now transform the input
	 * arguments into kilometewrs.
	 * We call them x and y for convenience.
	 *
	 * We now also take into account a shift that is introduced
	 * by the start.
	 */
	const Numeric_t x = (x_m - this->xDomStart() ) / 1000.0; //IN KM
	const Numeric_t y = (y_m - this->yDomStart() ) / 1000.0; //IN KM


	/*
	 * 	AIR
	 * It is very easy to test if we are in the air.
	 * If the y coordinate is smaller then 50km
	 */
	if(y <= 50)
	{
		// We are inside the air
		return TY_STICKY_AIR;
	}; //End if: handle airt case.


	/*
	 * As we see the thorn/slab als goes into the plate.
	 * Thus a simple check for the y coordinate will not
	 * work. Hower we will bound the slab by a bounding
	 * box that surrounds it. If we are not inside the box,
	 * we are either in the matle or the plate,
	 * depending on the depth.
	 * But if we are inside the box, we can still be inside
	 * the mantle/plate.
	 */
	if( ((x > 220.0) && x < 430.0) &&
	    ((y >  50.0) && y < 250.0)    )
	{
		/*
		 * We are inside the bounding box, so we have to check
		 * if we are realy inside the slab or not.
		 */

		//We now compute bounding values of the slap at the given position
		const Numeric_t y_1 = T1(x);
		const Numeric_t y_2 = T2(x);
		const Numeric_t y_3 = T3(x);
		const Numeric_t y_4 = T4(x);

		//Now test if we are inside the slap
		if(((y >= y_3) && (y >= y_2)) &&
	  	   ((y <= y_1) && (y <= y_4))   )
	  	{
	  		/*
	  		 * We are inside the slab, this also includes the neking region
	  		 * So we must now check the depth
	  		 */
	  		if(y >= 100.0 && y <= 150.0)
			{
				//We are in the necking area
				return TY_ROCK_NECKING;
			};

			//We are not in the necking area, so we are inside the
			//slap.
			return TY_ROCK_THORN;

		}
		else
		{
			/*
			 * We are outside the bounding box.
			 * This means that we are either inside the
			 * mantle or inside the plate.
			 * So a depth test must be done
			 */
			if(y <= 100.0)
			{
				//The depth is less than 100km, so we are inside the plate
				return TY_ROCK_PLATE;
			};

			//We are deeper than 100km but not inside the splap,
			//so we are inside the mantle
			return TY_ROCK_MANTLE;
		}; //End else: outside bounding
	//End if: we are inside the bounding box
	}
	else
	{
		/*
		 * We are outside the bounding box, so ww
		 * can check the y value to see in which material we are
		 */
		if(y <= 100.0)
		{
			//The depth is smaller 100km, so we are in the plate
			return TY_ROCK_PLATE;
		}
		else
		{
			//The depth is greater than 100km, so we are in the mantle for sure
			return TY_ROCK_MANTLE;
		};
	}; //End else: outside bounding box

	pgl_assert(false && "Reached unreacable code.");
}; //End: find location




PGL_NS_END(egd)


