#pragma once
/**
 * \brief	This file implements a setup for a rotating disc, with large surrounding viscisty.
 *
 * This is basically a specialization of the normal rotating disc case.
 * But some functions are overriden.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_rotatingDisk.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

/**
 * \class 	egd_markerSetUpRotDiscHighEta_t
 * \brief	This class creates the rotating disc scenario, with large viscosity for the surrounding media.
 *
 * This class basically inherents all methods from the normal rotating disc class and overrides, the
 * init function as well as the print function.
 */
class egd_markerSetUpRotDiscHighEta_t : public egd_markerSetUpRotDisc_t
{
	/*
	 * ========================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_markerSetUpRotDisc_t;			//!< This is the base implementation
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::PropIdx_t;
	using Base_t::PropList_t;
	using Base_t::PropToGridMap_t;

	using Base_t::MarkerCollection_t;
	using Base_t::MarkerPositions_t;
	using Base_t::MarkerProperty_t;
	using Base_t::CreationResult_t;

	/*
	 * Brief	This are the name of the different materials we have.
	 */
	const static Index_t TY_STICKY_AIR 	= 0;		//This is the air
	const static Index_t TY_DISC            = 1;		//This is the disc
	const static Index_t TY_N 		= 2;


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted and virtual.
	 */
	virtual
	~egd_markerSetUpRotDiscHighEta_t();


	/**
	 * \brief	This is the direct building constructor.
	 *
	 * Note that it will place the centre of the disc at the
	 * origin of the coordiante system.
	 * The constructor allows to set the angular velocity of the ball.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  nMarkerY	The number of markers per cell in y direction.
	 * \param  nMarkerX 	The number of markers per cell in x direction.
	 * \param  angularVel	The angular velocity.
	 * \param  markerRand	Displace the markers randomly.
	 */
	egd_markerSetUpRotDiscHighEta_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx,
		const Index_t 		nMarkerY,
		const Index_t 		nMarkerX,
		const Numeric_t 	angularVel,
		const bool 		markerRand,
		const egd_confObj_t* const 	confObj = nullptr);



	/**
	 * \brief	Building constructor with
	 * 		 config object.
	 *
	 * This constructor will examine the.
	 *
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_markerSetUpRotDiscHighEta_t(
		const egd_confObj_t&	confObj);



	/**
	 * \brief	Copy Constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDiscHighEta_t(
		const egd_markerSetUpRotDiscHighEta_t&);


	/**
	 * \brief	Copy assigment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDiscHighEta_t&
	operator= (
		const egd_markerSetUpRotDiscHighEta_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_markerSetUpRotDiscHighEta_t(
		egd_markerSetUpRotDiscHighEta_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerSetUpRotDiscHighEta_t&
	operator= (
		egd_markerSetUpRotDiscHighEta_t&&);


	/*
	 * =====================
	 * Protected Constructors
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is only used internaly. The result
	 * of this constructor, is an invalid
	 * object.
	 */
	egd_markerSetUpRotDiscHighEta_t();



	/*
	 * =========================
	 * Query Functions
	 */
public:



	/*
	 * =========================
	 * Interface Functions
	 */
public:
	using Base_t::creatProblem;


	/**
	 * \brief	This function outputs information about
	 * 		 the concrete implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	using Base_t::inspectDumper;


	/*
	 * ============================
	 * Hooks
	 */
protected:
	using Base_t::hook_creatMarkerPositions;
	using Base_t::hook_creatGridGeometry;
	using Base_t::hook_creatGridPropertyList;
	using Base_t::hook_postProcessGridProperties;


	/*
	 * =============================
	 * Needed Hooks
	 */
protected:
	using Base_t::hook_creatMarkerProperties;
	using Base_t::hook_findMaterial;
	using Base_t::hool_setMarkerProperties;


	/*
	 * ===============================
	 * Interface helper
	 *
	 * These are functions that are provided by the interface
	 * They are non virtual
	 */
protected:
	using Base_t::getTempGrid;
	using Base_t::wasTempSolverInspected;
	using Base_t::getStokeGrid;
	using Base_t::wasStokesSolverInspected;
	using Base_t::isTempOnExtGrid;
	using Base_t::isStokeOnExtGrid;
	using Base_t::getSolverPropMap;



	/*
	 * =======================
	 * Internal Access functions
	 *
	 * These functions allows to access some private
	 * variables of *this.
	 */
protected:
	using Base_t::xDomStart;
	using Base_t::yDomStart;
	using Base_t::xDomLength;
	using Base_t::yDomLength;
	using Base_t::gridPointsX;
	using Base_t::gridPointsY;
	using Base_t::markerDensityX;
	using Base_t::markerDensityY;
	using Base_t::randDistribution;


	/*
	 * ====================
	 * Material hook
	 */
protected:
	/**
	 * \brief	This function sets up the internal state of *this.
	 *
	 * This functin will set all internal parameters to their default value.
	 * With teh exception of the angular momentum.
	 * If this value is nan an error is generated.
	 */
	virtual
	void
	hook_setUpInternals()
	 override;



	/*
	 * ============================
	 * No variable they are are located in the base class
	 */
protected:
}; //End: class(egd_markerSetUpRotDiscHighEta_t)

PGL_NS_END(egd)

