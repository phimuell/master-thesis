/**
 * \brief	This file contains the constructors of the pulled ball settiungs.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_pulledBall.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


PGL_NS_START(egd)

#if !(defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0))
#	pragma message "Pressure scalling is not disabled, this setup has low pressure, so it should be enabled."
#endif



egd_markerSetUpPulledBall_t::egd_markerSetUpPulledBall_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const Numeric_t 		radBall,
	const Numeric_t 		ballVelY,
	const Numeric_t 		ballVelX,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  Base_t(Ny, Nx,
  	 2.0, 	//Ly
  	 3.5, 	//Lx
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 confObj)
{
using ::pgl::isValidFloat;

	/* Check */
	if((isValidFloat(radBall) == false) ||
	   (radBall               <= 0.0  )   )
	{
		throw PGL_EXCEPT_InvArg("The passed radius value " + std::to_string(radBall) + "is not valid.");
	};
	if((2.0 * radBall) >= (std::min(this->xDomLength(), this->yDomLength())) )
	{
		throw PGL_EXCEPT_InvArg("The radius is too large, the ball can not be fitted into the pipe."
				" The radius is " + std::to_string(radBall) + ", but the pipe has the extension "
				"Lx = " + std::to_string(this->xDomLength()) + ", Ly = " + std::to_string(this->yDomStart()));
	}

	/* Assign */
	this->m_radBall  = radBall;
	this->m_velXBall = ballVelX;	//is allowed to be NAN, in that case it will be determined by the rehology
	this->m_velYBall = ballVelY;	// the assigning function checks for NAN

	/* Initialize the other variables */
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpPulledBall_t::egd_markerSetUpPulledBall_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const bool 			markerRand,
	const egd_confObj_t* const  	confObj)
 :
  egd_markerSetUpPulledBall_t(
  		Ny, Nx,
  		nMarkerY, nMarkerX,
  		0.2,		/* radius of the ball */
  		NAN, NAN, 	/* indicates to the constructor, that they are loaded from the rehology */
  		markerRand,
  		confObj)
{};


egd_markerSetUpPulledBall_t::egd_markerSetUpPulledBall_t()
 :
  egd_markerSetUpPulledBall_t(
  	yNodeIdx_t(201), xNodeIdx_t(101),	//grid points
  	5, 5,					//Marker density
  	false,					//no ransom displacement
  	nullptr)				//No configuration object aviable
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpPulledBall_t::egd_markerSetUpPulledBall_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerSetUpPulledBall_t(
  		yNodeIdx_t(confObj.getNy()), xNodeIdx_t(confObj.getNx()),
  		confObj.getMarkerDensityY(), confObj.getMarkerDensityX(),
  		confObj.setUpPar("radius", 0.2),
  		confObj.setUpParNAN("vely"),
  		confObj.setUpParNAN("velx"),
  		confObj.randomlyDisturbeMarkers(),
  		&confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
	(void)confObj;
}; // End: building constructor


egd_markerSetUpPulledBall_t::~egd_markerSetUpPulledBall_t()
 = default;


egd_markerSetUpPulledBall_t::egd_markerSetUpPulledBall_t(
	const egd_markerSetUpPulledBall_t&)
 = default;


egd_markerSetUpPulledBall_t&
egd_markerSetUpPulledBall_t::operator= (
	const egd_markerSetUpPulledBall_t&)
 = default;


egd_markerSetUpPulledBall_t::egd_markerSetUpPulledBall_t(
	egd_markerSetUpPulledBall_t&&)
 noexcept
 = default;


egd_markerSetUpPulledBall_t&
egd_markerSetUpPulledBall_t::operator= (
	egd_markerSetUpPulledBall_t&&)
 = default;


void
egd_markerSetUpPulledBall_t::hook_setUpInternals()
{
	/*	 BALL	 */
	m_etaBall  = 	           1e+8;		//Pitch
	m_rhoBall  = 		    0.32;		//Real value would be 3200 kg / m^3


	/*	AIR WATER	*/
	m_etaAir  =           1.0016e-2;		// real value would be 1.0016e-3 at 20°C
	m_rhoAir  = 	            0.10;		// real value would be 1000 kg / m^3

	return;
}; //End: set initial values

PGL_NS_END(egd)

