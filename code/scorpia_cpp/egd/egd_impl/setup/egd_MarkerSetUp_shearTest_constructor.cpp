/**
 * \brief	Implements the constructor for the shearing szenario.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_markerCollection.hpp>

#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>

#include "./egd_MarkerSetUp_shearTest.hpp"



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#include <utility>


#if defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0)
#       pragma message "The pressure scaling is disabled. It is recomended for this setting to use it."
#endif


PGL_NS_START(egd)

using pgl::isValidFloat;

egd_markerSetUpShearSetting_t::egd_markerSetUpShearSetting_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const Index_t 			nMarkerY,
	const Index_t 			nMarkerX,
	const bool 			markerRand,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Ny, Nx,
  	 100'000.0, 100'000.0,
  	 nMarkerY, nMarkerX,
  	 markerRand,
  	 -0.6, -0.6,		//move the start of teh coordinate system.
  	 confObj)
{
	this->hook_setUpInternals();
}; //End: building constructor


egd_markerSetUpShearSetting_t::egd_markerSetUpShearSetting_t()
 :
  egd_markerSetUpShearSetting_t(
  		yNodeIdx_t(121), xNodeIdx_t(121),
  		5, 5,
  		false)
{
	//We call the default set up function
	this->hook_setUpInternals();
}; //End: default constructor


egd_markerSetUpShearSetting_t::egd_markerSetUpShearSetting_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerSetUpShearSetting_t(
  		yNodeIdx_t(confObj.getNy()), xNodeIdx_t(confObj.getNx()),
  		confObj.getMarkerDensityY(), confObj.getMarkerDensityX(),
  		confObj.randomlyDisturbeMarkers(),
  		&confObj)
{
	this->hook_setUpInternals();

	if(confObj.getINI().hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The config object does not have any keys.");
	};
}; // End: building constructor

egd_markerSetUpShearSetting_t::~egd_markerSetUpShearSetting_t()
 = default;


egd_markerSetUpShearSetting_t::egd_markerSetUpShearSetting_t(
	const egd_markerSetUpShearSetting_t&)
 = default;


egd_markerSetUpShearSetting_t&
egd_markerSetUpShearSetting_t::operator= (
	const egd_markerSetUpShearSetting_t&)
 = default;


egd_markerSetUpShearSetting_t::egd_markerSetUpShearSetting_t(
	egd_markerSetUpShearSetting_t&&)
 noexcept
 = default;


egd_markerSetUpShearSetting_t&
egd_markerSetUpShearSetting_t::operator= (
	egd_markerSetUpShearSetting_t&&)
 = default;


void
egd_markerSetUpShearSetting_t::hook_setUpInternals()
{
	/*
	 * LOWER WEDGE
	 * 	x_m < y_m
	 */
	this->m_loWedge_eta = 1e+21;
	this->m_loWedge_rho = 3'300.0;


	/*
	 * UPPER WEDGE
	 * 	x_m > y_m
	 */
	this->m_upWedge_eta = 1e+21;
	this->m_upWedge_rho = 3'200.0;

	return;
}; //End: set initial values

PGL_NS_END(egd)

