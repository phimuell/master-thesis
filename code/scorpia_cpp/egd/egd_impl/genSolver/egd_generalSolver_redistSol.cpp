/**
 * \brief	This file implements the default behaviour of the redistribution solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_generalSolver.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


void
egd_generalSolverImpl_t::hook_distributProperties(
		const Vectot_t&			sol,
		const GridContainer_t&		grid,
		SolverArg_i*  const 		solArg,
		SolverResult_i* const 		solRes)
 const
{
	pgl_assert(solRes != nullptr,
		   solArg != nullptr );

	//This is the number of unknowns
	const Index_t nUnknownsX = this->hook_nUnknownX();
	const Index_t nUnknownsY = this->hook_nUnknownY();
	const Index_t nProps     = this->hook_nPropPerNode();
	const Index_t nSolFields = solRes->nSolFields();
		pgl_assert(nUnknownsX > 0, nUnknownsY > 0, nProps > 0);

	if(nSolFields != nProps)
	{
		throw PGL_EXCEPT_RUNTIME("The number of solution fields in the tresult container is " + std::to_string(nSolFields)
				+ ", but the solver computed " + std::to_string(nProps) + " many properties.");
	}; //End if: check size of solution


	/*
	 * Install the time step
	 */
	if(solArg->hasTimeStep() == true)
	{
		PGL_TODO("Should we test if the solution already has an installed time?");

		const Numeric_t timeStep = solArg->getTimeStep();	//Load the time step from the solver
		Numeric_t oldResTimeStep = -1.0;			//Old time step

		solRes->setTimeStep(timeStep, &oldResTimeStep);		//Set the new time step
		const volatile Numeric_t oldResTimeStep_ = oldResTimeStep;	//Ensure that it is aviable for debuging
		(void)oldResTimeStep_;
	};//End if: set timestep in the result


	/*
	 * We now gho through the property to retrive them
	 * one by one. This means that the solution vector
	 * is traversed nProps times.
	 *
	 * Is not that efficient but very nice.
	 */
	for(Index_t k = 0; k != nProps; ++k)
	{
		SolverResult_i::SolField_t& SOL = solRes->getSolField(k);
			pgl_assert(SOL.rows() == nUnknownsY,
				   SOL.cols() == nUnknownsX );

		/*
		 * Fill the solution field
		 */
		for(Index_t j = 0; j != nUnknownsX; ++j)
		{
			for(Index_t i = 0; i != nUnknownsY; ++i)
			{
				//We now compute the index in the big matrix
				const Index_t k_retriveIdx = (j * nUnknownsY + i) * nProps + k;
					pgl_assert(0 <= k_retriveIdx, k_retriveIdx < sol.size());

				SOL.rawAccess(i, j) = sol[k_retriveIdx];
			}; //End for(i):
		};; //End for(j):
	}; //End for(k):

	//Test if the solution is finite
	pgl_assert(solRes->isFinite());

	return;
	(void)grid;
}; //End: redistribution process


void
egd_generalSolverImpl_t::hook_postProcessSolution(
	const Vectot_t&			sol,
	const GridContainer_t&		grid,
	SolverArg_i*  const 		solArg,
	SolverResult_i* const 		solRes)
 const
{
	pgl_assert(solArg != nullptr,
		   solRes != nullptr );

	return; 	//Do nothing
	(void)sol;
	(void)grid;
	(void)solArg;
	(void)solRes;
}; //End: postprocess redistribution



PGL_NS_END(egd)


