#pragma once
/**
 * \brief	This function contains the busness logic of the solver implementation.
 *
 * Thus they have only to be implemented once.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_generalSolverResult.hpp>
#include <egd_interfaces/egd_generalSolverArgument.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>




PGL_NS_START(egd)


/**
 * \brief	This is the standard solver.
 * \class 	egd_generalSolverImpl_t
 *
 * This is a common base class for all solvers. It implements
 * all solver related logic, such that the real solver
 * implementations can focuses on building the matrix without
 * handling buisness logic.
 *
 * This class was created out of the stokes solver.
 *
 * This class will also set all coefficient to zero,before
 * the matrix is build. It will also call the purn function
 * to remove any uneccessary zeros from the function.
 *
 */
class egd_generalSolverImpl_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
		static_assert(std::is_unsigned<uSize_t>::value, "uSize must be unsigned");
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used


	using SolverResult_i 	= egd_generalSolverResult_i;		//!< This is the result type of the solver.
	using SolverArg_i	= egd_generalSolverArg_i;		//!< This is the argument type of the solver.

	using GridContainer_t 	= egd_gridContainer_t;			//!< This is the grid container.
	using GridGeometry_t  	= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.
	using GridProperty_cref = GridContainer_t::GridProperty_cref;	//!< This is the property on the grid.
	using GridPosition_cref = GridContainer_t::GridPosition_cref;	//!< This is the grid position.

	using SystemMatrix_t 	= egd_SparseMatrix_t<Numeric_t>;	//!< This is the system matrix Type.
	using Vectot_t 		= egd_Vector_t<Numeric_t>;		//!< This is the type for a simple dense vector.
	using Matrix_t 	= egd_Matrix_t<Numeric_t>;	//!< This is the type of the reuslts.

private:
	using SolverImpl_t 	= ::Eigen::SparseLU<SystemMatrix_t, ::Eigen::COLAMDOrdering<Index_t> >;	//!< This is the implementation of the solver.
	using Solver_t 		= ::std::unique_ptr<SolverImpl_t>;					//!< This is the storage of the code

	using ScalingImpl_t 	= ::Eigen::IterScaling<SystemMatrix_t>;		//!< This is the scaling implementation.
	using Scaling_t 	= ::std::unique_ptr<ScalingImpl_t>;		//!< This is the scaling


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This constructors sets up the system. The user passes the
	 * number of basic nodal points to this function. Be aware
	 * that the ordering is swaped. First comes the number of
	 * grid points in Y direction, then seconds the ones for X.
	 * This is done to be consistent.
	 *
	 * The default boundary conditions are used.
	 *
	 * Note that this function does not call allocateSystem.
	 * The function is called upon the first solving process.
	 * This is due to work arround a problem in C++.
	 *
	 * \param  Ny	The number of nodal points in y direction.
	 * \param  Nx	The number of nodal points in x direction.
	 */
	egd_generalSolverImpl_t(
		const uSize_t 		Ny,
		const uSize_t 		Nx);

	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_generalSolverImpl_t(
		const egd_generalSolverImpl_t&)
	 = delete;


	/**
	 * \brief	Copy Assignement.
	 *
	 * Is deleted.
	 */
	egd_generalSolverImpl_t&
	operator= (
		const egd_generalSolverImpl_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverImpl_t(
		egd_generalSolverImpl_t&&);


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_generalSolverImpl_t&
	operator= (
		egd_generalSolverImpl_t&&);


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_generalSolverImpl_t();


	/*
	 * =======================
	 * Private Constructor
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * This is a private constructor, that is only provided
	 * for internal use.
	 *
	 * The boundary conditions are the default, no penetration
	 * and for the pressure 1e+9 is used as offset/force term.
	 * The numbers of basic nodal point is set to tero.
	 */
	egd_generalSolverImpl_t();




	/*
	 * ======================
	 * Driver function.
	 */
public:
	/**
	 * \brief	This function is the actuall driver code of the process.
	 *
	 * This function will do drive the solution process.
	 * It will set up the system matrix and solve the matrix equation.
	 * It will then distribute the solution in the appropriate
	 * result container.
	 *
	 * The argument is used to pass obtions to the subclass implementation.
	 * It is allso used to send information back to the calling routine
	 * thus it is not const.
	 *
	 * \param  grid		The grid container.
	 * \param  solArg	The argument container.
	 * \param  solRes	The container for storing the result.
	 *
	 * \throw	An error is raised if something happened.
	 *
	 * \note 	In a concrete implementation the solve function will
	 * 		 pass the call to this function.
	 */
	virtual
	void
	solve_impl(
		const GridContainer_t& 		grid,
		SolverArg_i*  const 		solArg,
		SolverResult_i* const 		solResult);


	/*
	 * ==========================
	 * Query function.
	 *
	 * This function allows to get the state and some
	 * parameter of *this. Note that they are not
	 * virtual.
	 */
public:

	/**
	 * \brief	Return the number of basic nodal points in X direction.
	 *
	 * If no allocation happens this function will return 0.
	 */
	uSize_t
	getNx()
	 const
	 noexcept
	{
		pgl_assert(pgl_implies(m_Nx == 0, m_Ny == 0));	//If m_Nx is zero then m_Ny must also be zero.
		return m_Nx;
	};


	/**
	 * \brief	Returns the number of basic nodal points in Y direction.
	 *
	 * This function returns zero, if called before the allocation happens.
	 */
	uSize_t
	getNy()
	 const
	 noexcept
	{
		pgl_assert(pgl_implies(m_Nx == 0, m_Ny == 0));	//If m_Nx is zero then m_Ny must also be zero.
		return m_Ny;
	};


	/**
	 * \brief	Returns true if *this is allocated.
	 *
	 * Note that even a full construction, does not make *this allocated.
	 * *this is only allocated if the allocateSystem() function was called.
	 */
	bool
	isAllocated()
	 const
	 noexcept
	{
		pgl_assert(pgl_implies(m_Nx == 0, m_Ny == 0));	//If m_Nx is zero then m_Ny must also be zero.
		pgl_assert(m_L.rows() == m_L.cols()  ,
			   m_L.rows() == m_rhs.size() );

		return ((m_rhs.size() > 0) && (m_L.size() > 0));
	}; //End: isAllocated


	/**
	 * \brief	This function returns true if ths system matrix is already builded.
	 *
	 * This is an internal state, but is provided to thepublic interface.
	 * When the solver is first used, this funnction will return true.
	 */
	bool
	isSystemBuilt()
	 const
	 noexcept
	{
		pgl_assert(pgl_implies(this->isAllocated() == false, m_L.nonZeros() == 0));
		pgl_assert(m_L.rows() == m_L.cols(),
			   m_L.rows() == m_rhs.size());

		return (m_L.nonZeros() > 0);
	};


	/**
	 * \brief	This function returns true if the system matrix is compressed.
	 *
	 * A true indicates that the system matrix is in a compressed column storage format.
	 * Note that this also implys that the system matrix is builded.
	 * Thus an empty system matrix is never compressed.
	 */
	bool
	isCompressed()
	 const
	 noexcept
	{
		pgl_assert(pgl_implies(this->isAllocated() == false, m_L.nonZeros() == 0));
		pgl_assert(m_L.rows() == m_L.cols(),
			   m_L.rows() == m_rhs.size() );

		return ((this->isSystemBuilt() == true) ? (this->m_L.isCompressed()) : false);
	}; //End: isCompressed


	/**
	 * \brief	This function computes the total number of unknowns of the system.
	 *
	 * The total number of unkowns is nPropPerNode() * nUnknownX()
	 * * nUnknownY().
	 */
	Index_t
	nTotUnknowns()
	 const
	{
		const Index_t unX = this->hook_nUnknownX();	//Load the components
		const Index_t unY = this->hook_nUnknownY();
		const Index_t unP = this->hook_nPropPerNode();
			pgl_assert(unX > 0, unY > 0, unP > 0);

		const Index_t tot = unX * unY * unP;	//Total

		if(tot <= 0)
		{
			throw PGL_EXCEPT_LOGIC("The number of total unkowns was negative.");
		};

		return tot;
	};//End: total number of unknonws



	/*
	 * ========================
	 * Hooks
	 *
	 * These functions have to be overwritten by the
	 * subclass if needed, some functions actually
	 * provide a default, but some functions did not,
	 * even the one that could provide one the reason
	 * is, that subclasses arer avare of what they are
	 * doing.
	 */
protected:
	/**
	 * \brief	This function is used to initialize the boundary for a step.
	 *
	 * It passes the grid container and the argument object, it can be used to
	 * further setup the configuration.
	 * Technically it is allowed to modify the argument option, but not recommended.
	 *
	 * This function has a default implementation, that does noting.
	 *
	 * \param  gridCont		The grid container.
	 * \param  solArg		The olver argument.
	 */
	virtual
	void
	hook_initBoundaries(
		const GridContainer_t& 		gridCont,
		SolverArg_i* const 		solArg);


	/**
	 * \brief	This function generates the system matrix.
	 *
	 * This function sets up the systenm matrix and the rhs.
	 * For that this function shall modify the respecitive
	 * members. This function is not allowed to change the
	 * size of both.
	 *
	 * This function performs the set up process arguments needed
	 * from the calle are passed by the grid container and the
	 * solver argument type, which also shall be modified
	 * if needed.
	 *
	 * \param  grid		This is the grid container.
	 * \param  solArg	This is the argument of the solver.
	 *
	 * \note	That the creation of the matrix is very inefficient,
	 * 		 even if we have reserved it. The reason is that our
	 * 		 insertion order is wrong. we can also not change
	 * 		 the storage order, since teh solver wants the column
	 * 		 order.
	 */
	virtual
	void
	hook_buildSystemMatrix(
		const GridContainer_t& 		grid,
		SolverArg_i* const 		solArg)
	 = 0;


	/**
	 * \brief	This hook allows the modification of the system matrix.
	 *
	 * This function is called after the system matrix and the rhs was updated.
	 * This function allows deriving classes to modify the sytsem matrix.
	 *
	 * This function is technically allowed to change the pattern of
	 * the system matrix, meaing to add or remove nnz entried in the
	 * matrix. If this is done the function must return true to indicate
	 * this. This is not necessary if the nnz values are changed.
	 *
	 * \param  grid		This is the grid container.
	 * \param  solArg 	This is the solution argument.
	 * \param  isFirstRun	This value indicates if this is the first run.
	 *
	 * \throw 	This function is allowd to throw.
	 *
	 * \note	The default implementation of this function does nothing.
	 */
	virtual
	bool
	hook_modifySystemMatrix(
		const GridContainer_t&	grid,
		SolverArg_i* const 	solArg,
		const bool 		isFirstRun);



	/**
	 * \brief	This function distributes the solution vector.
	 *
	 * The distribution extracts the solution, which is the composition
	 * of many separate paramter into the solution container.
	 *
	 * In order to comunicate with the caller the argument is passed
	 * to this function.
	 *
	 * \param  sol		This is the compzted solution vector.
	 * \param  grid		This is the grid that is used.
	 * \þaram  solArg	This is the argument that was passed to *this.
	 * \param  solResult	This is the object that will store the solution.
	 *
	 * Note that this function provides a default implementaion.
	 * Which is not super efficient, but can handle the situation.
	 *
	 * \note	It is not recomended to overwrite this function,
	 * 		 because the functionality must access the private
	 * 		 memeber functions of the grid property. Instead
	 * 		 consider implementing the hook_postProcessResult()
	 * 		 function.
	 *
	 */
	virtual
	void
	hook_distributProperties(
		const Vectot_t&			sol,
		const GridContainer_t&		grid,
		SolverArg_i*  const 		solArg,
		SolverResult_i* const 		solRes)
	 const;


	/**
	 * \brief	This function is called after the solution was distributed.
	 *
	 * This function is provided such that sub classes can influence the
	 * distribution process. The arguments are the same as they were for
	 * the normal distribution function.
	 *
	 * \param  sol		This is the compzted solution vector.
	 * \param  grid		This is the grid that is used.
	 * \þaram  solArg	This is the argument that was passed to *this.
	 * \param  solResult	This is the object that will store the solution.
	 *
	 * The default of this function does nothing.
	 */
	virtual
	void
	hook_postProcessSolution(
		const Vectot_t&			sol,
		const GridContainer_t&		grid,
		SolverArg_i*  const 		solArg,
		SolverResult_i* const 		solRes)
	 const;


	/**
	 * \brief	This function returns the numbers of unkowns in x direction.
	 *
	 * This is different from the number of grid points or so.
	 * This is  the number of unkowns, this is realy important.
	 */
	virtual
	Index_t
	hook_nUnknownX()
	 const
	 = 0;


	/**
	 * \brief	This function returns the number of unknowns in y direction.
	 *
	 * Also this is not necessaraly the number of grid points in y direction.
	 * This is the number of unknowns in y direction.
	 */
	virtual
	Index_t
	hook_nUnknownY()
	 const
	 = 0;


	/**
	 * \brief	This is the number of variables that are located
	 * 		 at a single grid node.
	 *
	 * The total number of unkowns is nPropPerNode() * nUnknownX()
	 * * nUnknownY().
	 */
	virtual
	Index_t
	hook_nPropPerNode()
	 const
	 = 0;

	/**
	 * \brief	This is the number of unkowns that should be reservered
	 * 		 in each column.
	 *
	 * It is better to overestimate this value than to choos a too conservative
	 * value. The default is 13.
	 */
	virtual
	Index_t
	hook_reservePerCol()
	 const;


	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not.
	 *
	 * True indicates that *this is an extextended grid.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 = 0;


	/**
	 * \brief	This function returns true if the system matrix and the
	 * 		 rhs should be damped into a text file.
	 *
	 * Note that this function is only called if the code is compiled
	 * with set NDEBUG falg. This function is passed a bool that indicates
	 * if the first run is happending.
	 *
	 * The default implementation returns its input argument, thus dumps the
	 * first matrix and RHS.
	 *
	 * \param  isFrisRun		Indicates if the first run is happening.
	 */
	virtual
	bool
	hook_writeSystem(
		const bool 	isFrisRun)
	 const;


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping.
	 *
	 * The returned token should be something that is decriptive
	 * to the class instance, such as "stokesStandardSolver".
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 = 0;


	/*
	 * ========================
	 * Private Internal function.
	 */
private:
	/**
	 * \brief	This function solves the system.
	 *
	 * It will solve the system
	 * 	m_L * m_sol == m_rhs
	 * It will thus access the matrix, solution and the rhs.
	 * This function requieres that the matrix is compressed.
	 *
	 * Note that the solver will keep the permutation pattern
	 * of the matrix alive and reuse it, if its argument is
	 * false. This should speed up the computation a bit.
	 * This is possible because this pattern does not
	 * depend on the values but on the pattern.
	 * Hiowever at some times this can go wrong.
	 *
	 * \param  doAnalizePattern	This flag instructs the solver to analize the pattern.
	 *
	 * \throw 	This function will perform a check if the
	 * 		 computed values are valid.
	 */
	void
	priv_solveSystemMatrix(
		bool 		doAnalizePattern);


	/**
	 * \brief	This function allocates the space.
	 *
	 * This function will be called by the solve function if *this
	 * is not allocated. This is needed because C++ has some problems
	 * with virtual functions in constructors.
	 *
	 * \throw 	If called twice or an error occured.
	 */
	void
	allocateSystem();


	/**
	 * \brief	This function is able to reserve the system matrix.
	 *
	 * This function will ignore the state of the matrix and just
	 * perform the reserve operation. This function will turn the
	 * system matrix into an uncompressed matrix.
	 * The value of the allocation is derived from the system size
	 * and can not be influenced.
	 * Note all coefficients will be set to zero, thus "unbuild"
	 * the system matrix.
	 *
	 * Thuis function will only carry out the reserve operation.
	 * If the size of the system, matrix and rhs, is not as expected
	 * an error will be generated.
	 */
	void
	reserveSystem();



	/**
	 * \brief	This function writes the matrix to a file.
	 *
	 * This function writes the matrix to file, in a triplet like format.
	 * The format is:
	 * 	ROW_IDX	[TAB] COL_IDX [TAB] VALUE
	 *
	 * \param  outFile	The output file that should be used, if exists
	 * 			 it will be overwritten.
	 */
	void
	priv_writeSystemMatrix(
		const std::string& 	outFile)
	 const;


	/**
	 * \brief	This function writes the right hand side to disk.
	 *
	 * Each line contains one value. Note that also the last line (value) has
	 * new line character.
	 *
	 * \param  outFile	The file to output to.
	 */
	void
	priv_writeRHS(
		const std::string& 	outFile)
	 const;






	/*
	 * ========================
	 * Private Members
	 *
	 * The system matrix and the rhs are protected.
	 * This way the subclasses can modify it,
	 * but the internals of *this are protected.
	 */
protected:
	SystemMatrix_t 		m_L;			//!< This matrix is the system matrix that will be created and solved.
	Vectot_t 		m_rhs;			//!< This is the right hand side of the system.
private:
	Vectot_t 		m_sol;			//!< This is the solution vector.
	Solver_t 		m_solverPtr;		//!< This is the solver (contains a copy of the matrix, but no easy was to get it); it is just a conatiner.
	Scaling_t 		m_scalPtr;		//!< This is a pointer to the implementation of the scalling.
	uSize_t 		m_Ny = 0;		//!< Number of nodal points (of the domain) in Y direction.
	uSize_t 		m_Nx = 0;		//!< Number of nodal points (of the domain) in X direction.
	bool 			m_wasPurned = false;	//!< Indicates that the matrix was purned, in the LAST timestep.
}; //End: class(egd_generalSolverImpl_t)

PGL_NS_END(egd)


