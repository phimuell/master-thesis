/**
 * \brief	This file contains the implementation of teh default hooks for the general solver.
 *
 * These hooks shold be provided, but it is possible to provide usefull default functions.
 * Note that technically the rediustribution function is also part of the default hook,
 * it is implemented in its own file.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_generalSolver.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


bool
egd_generalSolverImpl_t::hook_modifySystemMatrix(
	const GridContainer_t&	grid,
	SolverArg_i* const 	solArg,
	const bool 		isFirstRun)
{
	pgl_assert(solArg != nullptr,
		   this->isAllocated(),
		   this->isSystemBuilt());

	return false;	//this function does nothing; false for indicating no change is done
	(void)grid;
	(void)solArg;
	(void)isFirstRun;
}; //End: default for modifing the system matrix


egd_generalSolverImpl_t::Index_t
egd_generalSolverImpl_t::hook_reservePerCol()
 const
{
	return 13;
}; // End: default reserve columns


bool
egd_generalSolverImpl_t::hook_writeSystem(
	const bool 	isFrisRun)
 const
{
	return isFrisRun;
};


void
egd_generalSolverImpl_t::hook_initBoundaries(
	const GridContainer_t& 		gridCont,
	SolverArg_i* const 		solArg)
{
		pgl_assert(solArg != nullptr);
	return;
	PGL_UNUSED(gridCont, solArg);
};



PGL_NS_END(egd)

