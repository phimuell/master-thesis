/**
 * \brief	This file contains the functions that are needed to write the system to disc.
 *
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_generalSolver.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_algorithm.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>
#include <iostream>
#include <fstream>


PGL_NS_START(egd)

#define TAB '\t'


void
egd_generalSolverImpl_t::priv_writeSystemMatrix(
	const std::string& 	outFilePath)
 const
{
	using Triplet_t = ::Eigen::Triplet<Numeric_t, Index_t>;

	if(outFilePath.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("The given path for writting the system matrix of the Stokes solver is empty.");
	};

	std::ofstream out;
	out.open(outFilePath.c_str(), std::iostream::out | std::iostream::trunc);

	if(out.is_open() == false)
	{
		throw PGL_EXCEPT_RUNTIME("Could not open the file \"" + outFilePath + "\" for writting the system matrix of the Stokes solver.");
	};


	/*
	 * We need to transpose the matrix, since we want to sort them according to the colum,
	 * we do this in seveall steps.
	 */

	//This vector will store all the triplets
	pgl::pgl_vector_t<Triplet_t> triplets;
	triplets.reserve(m_L.nonZeros());

	for(int k = 0; k < m_L.outerSize(); ++k)
	{
  		for(SystemMatrix_t::InnerIterator it(m_L, k); it; ++it)
  		{
  			const Numeric_t val = it.value();
  			const Index_t   row = it.row();
  			const Index_t   col = it.col();

  			//Insert into triplet list
  			triplets.emplace_back(row, col, val);
		}; //End for(it): going through the rows oin one column
	}; //End for(k): going over the columns

	/*
	 * Now sorting then, such that they are like the rows are leading
	 */
	std::sort(triplets.begin(), triplets.end(),
		[](const Triplet_t& lhs, const Triplet_t& rhs) -> bool
		{
		  return (std::make_pair(lhs.row(), lhs.col()) < std::make_pair(rhs.row(), rhs.col()));
		});

	//Increase the presision to 16, the best that double can get
	out.precision(16);

	//Now write the triplets to the file
	for(const auto& it : triplets)
	{
		out << it.row() << TAB << it.col() << TAB << it.value() << '\n';
	}; //End for(it): writting to disc

	//Close the file
	out.close();

	return;
}; //End: write system matrix



void
egd_generalSolverImpl_t::priv_writeRHS(
	const std::string& 	outFilePath)
 const
{
	if(outFilePath.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("The given path for writting the rhs of the Stokes solver is empty.");
	};

	std::ofstream out;
	out.open(outFilePath.c_str(), std::iostream::out | std::iostream::trunc);

	if(out.is_open() == false)
	{
		throw PGL_EXCEPT_RUNTIME("Could not open the file \"" + outFilePath + "\" for writting the rhs of the Stokes solver.");
	};


	//Increase the presision to 16, the best that double can get
	out.precision(16);
	const auto N = m_rhs.size();

	//Now write the triplets to the file
	for(Index_t i = 0; i != N; ++i)
	{
		out << m_rhs[i] << "\n";
	}; //End for(it): writting to disc

	//Close the file
	out.close();

	return;
}; //End: write system matrix




PGL_NS_END(egd)


