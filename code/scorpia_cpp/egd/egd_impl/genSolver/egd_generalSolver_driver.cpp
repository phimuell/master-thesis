/**
 * \brief	This file implements the driving code for the solver.
 *
 * This is the code that creates the matrix set up the solution and so on.
 * It is basically the code that calls the memeber function in the right order.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_generalSolver.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

#if defined(EGD_SOLVER_STD_NO_CHECK) && EGD_SOLVER_STD_NO_CHECK != 0
	//Nothing to do
#else
#	pragma message "Extended checks in the general solver are activated."
#endif

#if defined(EGD_SOLVER_PRINT_PRUNED) && (EGD_SOLVER_PRINT_PRUNED != 0)
#	pragma message "If a purning happens it will be written to stderr."
#endif

#if defined(EGD_SOLVER_PURE_PRUNING) && (EGD_SOLVER_PURE_PRUNING != 0)
#	pragma message "Pure pruning is active."
#endif

#if defined(EGD_SOLVER_NO_PRUGING) && (EGD_SOLVER_NO_PRUGING != 0)
#	pragma message "Pruging is disabled."
#endif

#if !(defined(EGD_SOLVER_NO_SYSTEMMATRIX_DUMP) && (EGD_SOLVER_NO_SYSTEMMATRIX_DUMP != 0))
#	pragma message "The solver will dump the system matrix in the first iteration."
#endif

void
egd_generalSolverImpl_t::solve_impl(
	const GridContainer_t& 		grid,
	SolverArg_i* const 		solArg,
	SolverResult_i* const 		solResult)
{
	//Load the grid geometry
	const GridGeometry_t& gridGeo = grid.getGeometry();

	if(solArg == nullptr)
	{
		throw PGL_EXCEPT_NULL("The null pointer was passed as solution argument.");
	};
	if(solResult == nullptr)
	{
		throw PGL_EXCEPT_NULL("The nullpointer was passed as solution result object.");
	};
	if(solArg->isFinite() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed solver argument is not finite or valid.");
	};

	/*
	 * Test if the geometry was modified.
	 * This will basically clear the system, such as it was the first run.
	 * Note that since we use managed pointers, no resource leak will occure
	 */
	if(gridGeo.isNewVersion() == true)
	{
		//reset all
		this->m_L.setZero();
		this->m_rhs.setZero();
		this->m_sol.setZero();

		//Set the size
		this->m_Nx = gridGeo.xNPoints();
		this->m_Ny = gridGeo.yNPoints();

		//As a reminder to also change that.
		pgl_assert(gridGeo.isConstantGrid());
	}; //ENd if: grid geometry has changed


	//Allocate the system if needed
	if(this->isAllocated() == false)
	{
		if(this->isSystemBuilt() == false)
		{
			//The system is not build yet and aslo not allocated,
			//so we are in the first run and have thus to allocate the
			//system
			this->allocateSystem();
		}
		else
		{
			//We are not in the first run, so this is cclearly an error
			throw PGL_EXCEPT_LOGIC("The solver is not allocated.");
		};
		pgl_assert(this->isAllocated());	//Now the system must be allocated.
	};//End allocate and check

	//
	//Makes some checks
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set up.");
	};
	if(this->getNx() != (uSize_t)gridGeo.xNPoints())
	{
		throw PGL_EXCEPT_InvArg("The number of nodes in the x direction is not correct."
				" The solver has " + std::to_string(this->getNx()) + ", but the grid has " + std::to_string(gridGeo.xNPoints()) + ".");
	};
	if(this->getNy() != (uSize_t)gridGeo.yNPoints())
	{
		throw PGL_EXCEPT_InvArg("The number of nodes in the y direction is not correct."
				" The solver has " + std::to_string(this->getNy()) + ", but the grid has " + std::to_string(gridGeo.yNPoints()) + ".");
	};
	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("Only constant grids are supported.");
	};

	//Test if this is the first run
	const bool isFirstRun = (m_L.nonZeros() == 0) ? true : false;

	//This bool is used to store if a reverse has happened, only in the sense of pruning
	const bool wasReservedP = this->m_wasPurned;

	/*
	 * If the matrix was purned in the last iteration, we have now
	 * to reserve space again for it. This is because the insertion
	 * will take a lot of time otherwards.
	 *
	 * Note that this is not perfect but only a fix.
	 * Only do it if not the first run, because it was done already.
	 */
	if((this->m_wasPurned == true ) &&
	   (       isFirstRun == false)   )
	{
		this->reserveSystem();		//reserve actual space
		this->m_wasPurned = false;	//Reset the purne state
	}; //End if: reserve space.

	/*
	 * Set all coefficient to zero.
	 * This is a bit saver, because technically we can use
	 * += on any matrix update and do not have to
	 * consider if we have killed the value from last
	 * iteration already.
	 *
	 * Note this is only needed if no puring was done.
	 */
	if((isFirstRun == false)   &&
	   (wasReservedP == false)   )
	{
		//Set everything to zero
			pgl_assert(this->isSystemBuilt(),
				   this->isCompressed()  );
		this->m_L.coeffs().setConstant(0.0);
		this->m_rhs.setConstant(0.0);
	} //End set everything to zero
	else
	{
		pgl_assert(this->isSystemBuilt() == false);
	};

	//this is the old size of the matrix
	const Index_t oldNNZ = this->m_L.nonZeros();
		pgl_assert(pgl_implies(isFirstRun || wasReservedP, oldNNZ == 0));


	/*
	 * Now we initialize the boundaries.
	 * This function is not requiered to do anything.
	 */
	this->hook_initBoundaries(grid, solArg);


	/*
	 * Build or update the system, if it was alredy formed.
	 */
	this->hook_buildSystemMatrix(grid, solArg);

	//Store the number of nonZero coefficient after the system was build
	const Index_t buildNNZ = this->m_L.nonZeros();

	/*
	 * This hook is called it is provided for manipulating
	 * the matrix after it was updated.
	 *
	 * Note that this can update the sparsity pattern.
	 */
	const bool wasSparsityPatternModified = this->hook_modifySystemMatrix(grid, solArg, isFirstRun);

	//Test the user, since he is stupid. Oh, I am the only user
	if(wasSparsityPatternModified == false)
	{
		if(buildNNZ != this->m_L.nonZeros())
		{
			throw PGL_EXCEPT_RUNTIME("Return value indicates no update of sparsity patern, but test failed."
					" Expected " + std::to_string(buildNNZ) + " but found "
					+ std::to_string(this->m_L.nonZeros()) + " many non zero elements.");
		};
	};//End test if sparsity was vialated

	/*
	 * Compress the matrix.
	 * This is only needed if we are in the first run or if the matrix was updated
	 * or if a resize has happened.
	 */
	if((isFirstRun   	       == true) ||
	   (wasReservedP 	       == true) ||
	   (wasSparsityPatternModified == true)   )
	{
		/*
		 * We are in teh first run, so we must compress the matrix.
		 */
		m_L.makeCompressed();
			pgl_assert(m_L.isCompressed(), this->isCompressed());
	}; //End if: compressing the system matrix
	pgl_assert(m_L.isCompressed() == this->isCompressed());


#if !(defined(EGD_SOLVER_NO_PRUGING) && (EGD_SOLVER_NO_PRUGING != 0))
	/*
	 * Perform the pruning
	 *
	 * This is to avoid that pure zeros are inside the matrix.
	 * We will delete everything that is below the machine precisiion
	 * in absolute value.
	 */
	Index_t unPurnedNNZ = -1;
	Index_t purnedNNZ   = -1;
	{
		//This is the epsilon tolerance
		constexpr Numeric_t Epsilon  = ::std::numeric_limits<Numeric_t>::epsilon();
		constexpr Numeric_t mEpsilon = -Epsilon;

		//We also have to load the new number of unkowns.
		//This can happens if the sparsity patern was manipulated
		unPurnedNNZ = this->m_L.nonZeros();

		//Now perform the actual purning
		this->m_L.prune([Epsilon, mEpsilon]	//This is the KEEPING function
				(const Index_t& row, const Index_t& col, const Numeric_t& value)
				 -> bool
				{
# 					if defined(EGD_SOLVER_PURE_PRUNING) && (EGD_SOLVER_PURE_PRUNING != 0)
					//Here we do pure purning, this means that only 0.0 will be prunged
					return (value == 0.0)
						? false		//If value is 0.0, then do not keep that
						: true;
					(void)row;
					(void)col;
					(void)Epsilon;
					(void)mEpsilon;

#					else
					//Note that that this is the keeping function.
					//If true, the value is keept
					return (value < 0.0)
						? (value < mEpsilon)	//If v negative, we need a more negative value
						: (value >  Epsilon);	//If v positive, need a larger one
					(void)row;
					(void)col;
#					endif
				});
			pgl_assert(this->m_L.isCompressed());

		//Get the number of coefficient after the pruning step.
		purnedNNZ = this->m_L.nonZeros();

		if(purnedNNZ > unPurnedNNZ)	//This check is basically to force the compiler to keep the values
		{
			throw PGL_EXCEPT_RUNTIME("What the hell is going on, the purning created more value."
					" Started with " + std::to_string(unPurnedNNZ) + ", but ended with "
					+ std::to_string(purnedNNZ) + " many entries.");
		};
	}; //End scope: Pruning
		pgl_assert(unPurnedNNZ > 0, purnedNNZ > 0,
			   unPurnedNNZ >= purnedNNZ);

	/* Save the puring state */
	if(unPurnedNNZ != purnedNNZ)
	{
		this->m_wasPurned = true;
	};


#	if defined(EGD_SOLVER_PRINT_PRUNED) && (EGD_SOLVER_PRINT_PRUNED != 0)
	if(unPurnedNNZ != purnedNNZ)
	{
		std::cerr << ">> GenSolver: Pruned " << (unPurnedNNZ - purnedNNZ) << " many entries, the system matrix has " << purnedNNZ << " entries." << std::endl;
	}
#	endif
#endif

#if !(defined(EGD_SOLVER_NO_SYSTEMMATRIX_DUMP) && (EGD_SOLVER_NO_SYSTEMMATRIX_DUMP != 0))
	if(this->hook_writeSystem(isFirstRun) == true)
	{
		/*
		 * Write system matrix and rhs to disc
		 */
		const std::string fileName = this->hook_dumpFileName();
		this->priv_writeSystemMatrix(fileName + "_systemmatrix_firstRun.txt");
		this->priv_writeRHS(fileName + "_rhs_firstRun.txt");
	}; //End if: is first run
#endif


	/*
	 * We must now check if the systemmatrix is stuill compressed.
	 * This is needed because teh solver needs that.
	 * If the matrix has gone uncompressed, this indicates
	 * and error.
	 */
	if(m_L.isCompressed() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The matrix is no longer compressed. This indicates an error.");
	};


	/*
	 * We will now perform some tests on the matrix.
	 * It is implemented by an exception, but the exception can be disabled.
	 */
#if defined(EGD_SOLVER_STD_NO_CHECK) && EGD_SOLVER_STD_NO_CHECK != 0
	//Nothing to do
#else
	//The check is activated
	if(m_L.coeffs().isFinite().all() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The System matrix has invalide entries.");
	};
	if(m_rhs.array().isFinite().all() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The Right hand side contained invalid numbers.");
	};
#endif

	/*
	 * Solve
	 *
	 * This will result in one large vector. That will
	 * later be remapped to matricies.
	 * Argument is used to determine if the pattern must
	 * be reanalyzed.
	 */
	this->priv_solveSystemMatrix(
			isFirstRun 		   ||		//First run
			wasSparsityPatternModified ||		//The patern was modified
			(m_L.nonZeros() != oldNNZ)   );		//Numbers had changed.


	/*
	 * Test if some problems arrised
	 */
#if defined(EGD_SOLVER_STD_NO_CHECK) && EGD_SOLVER_STD_NO_CHECK != 0
	//Nothing to do
#else
	//The check is activated
	if(m_sol.array().isFinite().all() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The computed solution contains invalid values.");
	};
#endif

	/*
	 * Decompose the solution vector
	 */
	this->hook_distributProperties(m_sol, grid, solArg, solResult);


	/*
	 * Postprocess the solution
	 */
	this->hook_postProcessSolution(m_sol, grid, solArg, solResult);


	/*
	 * The process has ended.
	 */
	return;
}; //End: solve


PGL_NS_END(egd)


