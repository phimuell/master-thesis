/**
 * \brief	This file contains the implementation of the standard
 * 		 version of the Stokes solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_generalSolver.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <unsupported/Eigen/src/IterativeSolvers/Scaling.h>


//Include STD
#include <memory>


PGL_NS_START(egd)

//An anonymus namespace
namespace
{
	template<
		typename 	T
	>
	class egd_constValueVec_t
	{
	public:
		egd_constValueVec_t(
			const uSize_t 	N,
			const T		val)
		 :
		  m_val(val),
		  m_N(N)
		{};

		typedef T value_type;

		const T&
		operator[](
			const uSize_t 	i)
		 const
		 noexcept
		{
			pgl_assert(i < m_N);
			return m_val;
			(void)i;
		};

	private:
		T 		m_val;
		uSize_t 	m_N;
	}; //End class

}; //End: anonymus namespace


egd_generalSolverImpl_t::egd_generalSolverImpl_t(
	const uSize_t 		Ny,
	const uSize_t 		Nx)
 :
  egd_generalSolverImpl_t()	//use the default constructor
{
	if(Ny <= 2)
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in y direction is too small.");
	};
	if(Nx <= 2)
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in x direction is too small.");
	};

	//Set the new size
	m_Ny = Ny;
	m_Nx = Nx;
	m_wasPurned = false;	// Never purned
}; //End: building constructor.



egd_generalSolverImpl_t::egd_generalSolverImpl_t()
 = default;



egd_generalSolverImpl_t::egd_generalSolverImpl_t(
	egd_generalSolverImpl_t&&)
 = default;


egd_generalSolverImpl_t&
egd_generalSolverImpl_t::operator= (
	egd_generalSolverImpl_t&&)
 = default;


egd_generalSolverImpl_t::~egd_generalSolverImpl_t()
 = default;


void
egd_generalSolverImpl_t::allocateSystem()
{
	using std::to_string;

	//
	//Tests
	if(this->isAllocated() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate the matrix twice.");
	};
	if(this->isSystemBuilt() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate an already build system.");
	};


	//Input tests
	if((m_Ny <= 1) || (m_Nx <= 1))	//Less then two test.
	{
		throw PGL_EXCEPT_InvArg("The number of gird points are too small, the where (y = " + to_string(m_Ny) + ", x = " + to_string(m_Nx) + ").");
	};
	//pgl_assert(m_solverPtr == nullptr); If we have to change because of geometry, this will fail.

	//Allocate the solver
	m_solverPtr = std::make_unique<SolverImpl_t>();

	//Allocate he scalling
	m_scalPtr   = std::make_unique<ScalingImpl_t>();


	// This is the number of unkonws we have.
	const uSize_t nUnknowns = this->nTotUnknowns();

	//
	//Allocate the RHS
	m_rhs.setZero(nUnknowns);
		pgl_assert((uSize_t)m_rhs.size() == nUnknowns);

	//
	//Allocate the system
	m_L.resize(nUnknowns, nUnknowns);
	m_L.setZero();
		pgl_assert((uSize_t)m_L.rows() == nUnknowns,
			   (uSize_t)m_L.cols() == nUnknowns,
			        m_L.nonZeros() == 0         );

	/*
	 * Call the reserve function for the system matrix
	 */
	this->reserveSystem();

	pgl_assert(this->isAllocated()   == true,
		   this->isSystemBuilt() == false,
		   this->m_L.nonZeros()  == 0     );
	return;
}; //End allocate




void
egd_generalSolverImpl_t::reserveSystem()
{
	if(this->isAllocated() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not reserve space if the matrix is not allocated.");
	};

	const Index_t nUnknowns = this->nTotUnknowns();		//Get the number of unkowns that are needed.
	if((this->m_L.rows() != nUnknowns) || this->m_L.cols() != nUnknowns)
	{
		throw PGL_EXCEPT_LOGIC("The system matrix does not have expected size, it is of size "
				+ egd_printMatrixSize(this->m_L) + ", expected a ("
				+ std::to_string(nUnknowns) + "x" + std::to_string(nUnknowns) + ") matrix.");
	};
	if(this->m_rhs.size() != nUnknowns)
	{
		throw PGL_EXCEPT_LOGIC("The rhs vector has the wrong size. Expected " + std::to_string(nUnknowns)
				+ ", but got a " + std::to_string(this->m_rhs.size()));
	};


	/*
	 * This will reduce the system matrix to a zero
	 * matrix. Note that this function will only
	 * set the size of the nnz to zero. Memory
	 * will not be freed and the size will not
	 * be modified.
	 */
	this->m_L.setZero();
		pgl_assert(this->m_L.rows() == nUnknowns,
			   this->m_L.cols() == nUnknowns );

	/*
	 * For some perverse reason, reserve needs
	 * a compressed matrix. I have no idea why.
	 */
	if(m_L.isCompressed() == false)
	{
		m_L.makeCompressed();
			pgl_assert(m_L.isCompressed() == true);
	};

	/*
	 * now preallocate, use ten entries per row
	 * Used these lines on my code to optain the bound:
	 *  grep -P 'L\(k_vx,[[:space:]]+.+\)' stockesSolver_2.m | cut -d'(' -f2  | cut -d')' -f1 | tr -d ' ' | sort -u | wc -l
	 *  grep -P 'L\(k_vy,[[:space:]]+.+\)' stockesSolver_2.m | cut -d'(' -f2  | cut -d')' -f1 | tr -d ' ' | sort -u | wc -l
	 *  grep -P 'L\(K_pm,[[:space:]]+.+\)' stockesSolver_2.m | cut -d'(' -f2  | cut -d')' -f1 | tr -d ' ' | sort -u | wc -l
	 * (I think it should be possible with grep allone)
	 *
	 * This will make the matrix into an uncompressed matrix.
	 */
	const Index_t resPerCol = this->hook_reservePerCol();
	const auto revSize = egd_constValueVec_t<uSize_t>(nUnknowns, resPerCol);
	m_L.reserve(revSize);

	pgl_assert(this->isAllocated()   == true ,
		   this->isSystemBuilt() == false );

	return;
}; //End: reserve









PGL_NS_END(egd)


