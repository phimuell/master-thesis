/**
 * \brief	This file contains the code that performs the solving process of the matrix.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_generalSolver.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <unsupported/Eigen/src/IterativeSolvers/Scaling.h>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


void
egd_generalSolverImpl_t::priv_solveSystemMatrix(
	bool 		doAnalizePattern)
{
	if(this->isSystemBuilt() == false)
	{
		throw PGL_EXCEPT_LOGIC("Called the solver, but the system matrix is not assembled.");
	};
	if(this->isCompressed() == false)
	{
		throw PGL_EXCEPT_LOGIC("Called the solver, but the systen matrix is not compressed.");
	};

	const Index_t nUnkowns = this->nTotUnknowns();	//Get the number of total unkowns
	if(nUnkowns != m_L.rows()   ||
	   nUnkowns != m_L.cols()   ||
	   nUnkowns != m_rhs.size()   )
	{
		throw PGL_EXCEPT_RUNTIME("The system matrix does not have the correct size.");
	}; //End if: size checks


	//Test if we have to analyze the pattern
	if(doAnalizePattern == true)
	{
		this->m_solverPtr->analyzePattern(this->m_L);

		/*
		 * analyzePattern, does not set the compute info.
		 * This will in fact lead to an error, since the solver is not
		 * initialized, only factorize does this. See source of LU.
		 */
	}; //End if: analyze pattern.

	//Perform the scalling to equilibrate the matrix
	this->m_scalPtr->computeRef(m_L);	//Compute the scalling

	//Apply the scaling
	this->m_rhs = this->m_scalPtr->LeftScaling().cwiseProduct(this->m_rhs);

	//Perform the factorization
	this->m_solverPtr->factorize(this->m_L);

	//Test if the factorization was successfull
	if(this->m_solverPtr->info() != Eigen::Success)
	{
		throw PGL_EXCEPT_RUNTIME("Decomponsition of system matrix failed. Error message was: \"" + this->m_solverPtr->lastErrorMessage() + "\"");
	};

	//Solve the actual system
	m_sol = this->m_solverPtr->solve(this->m_rhs);

	//Test if the process was successful
	if(this->m_solverPtr->info() != Eigen::Success)
	{
		throw PGL_EXCEPT_RUNTIME("Solving of the linear problem failed. Error message was: \"" + this->m_solverPtr->lastErrorMessage() + "\"");
	};

	//Undo the scaling
	m_sol = m_scalPtr->RightScaling().cwiseProduct(m_sol);

	return;
}; //End: actuall solving


PGL_NS_END(egd)


