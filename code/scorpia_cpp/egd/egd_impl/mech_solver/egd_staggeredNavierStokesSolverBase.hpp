#pragma once
/**
 * \brief	This file contains a base class of the navier stokes equation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include <egd_impl/genSolver/egd_generalSolver.hpp>
#include <egd_impl/mech_solver/egd_staggeredStokesSolverBase.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverArgument.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>




PGL_NS_START(egd)


/**
 * \brief	This is the staggered grid base for inteia problems.
 * \class 	egd_staggeredNavierStokesSolverBase_t
 *
 * This class is an extension of the staggered base solver that
 * exists for the stokes equation. This class will override the
 * postprcessing function and make it final, but provide a new one.
 * In the post processing function it will implemnt the stokes term.
 */
class egd_staggeredNavierStokesSolverBase_t : public egd_staggeredStokesSolverBase_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
		static_assert(std::is_unsigned<uSize_t>::value, "uSize must be unsigned");
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using Matrix_t 	= egd_Matrix_t<Numeric_t>;	//!< This is the type of the reuslts.

	using ImplBase_t 	= egd_staggeredStokesSolverBase_t;	//!< This is the type that provides the general implementation.
	using InterfaceBase_t 	= egd_StokesSolver_i;			//!< This is the type that is the interface we are implementing.

	using SolverResult_t 	= egd_mechSolverResult_t;		//!< This is the result type of the mechanical solver, not an abstract one.
	using SolverResult_ptr 	= SolverResult_t::MechSolverResult_ptr;	//!< Pointer to the result type; note this is not the base and it is managed.

	using SolverArg_t  	= egd_mechSolverArg_t;			//!< This is the argument of the mechanical solver, this is not a base class.
	using SolverArg_ptr 	= SolverArg_t::MechSolverArg_ptr;	//!< Managed pointer to the argument class, note that this is not a base class.

	using GridContainer_t 	= egd_gridContainer_t;			//!< This is the grid container.
	using GridGeometry_t  	= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.
	using GridProperty_t 	= GridContainer_t::GridProperty_t;	//!< This is the type for grid property.
	using GridProperty_cref = GridContainer_t::GridProperty_cref;	//!< This is the property on the grid.
	using GridPosition_cref = GridContainer_t::GridPosition_cref;	//!< This is the grid position.
	using SystemMatrix_t 	= egd_SparseMatrix_t<Numeric_t>;	//!< This is the system matrix Type.
	using Vectot_t 		= egd_Vector_t<Numeric_t>;		//!< This is the type for a simple dense vector.

	using SourceTerm_t 	= InterfaceBase_t::SourceTerm_t;	//!< The source term.
	using SourceTerm_ptr 	= InterfaceBase_t::SourceTerm_ptr;	//!< Managed pointer to source term.

	using PropToGridMap_t   = InterfaceBase_t::PropToGridMap_t;	 //!< This is the map that deperimes which times are pressent.


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This function is forwarded to the base.
	 *
	 * \param  Ny	The number of nodal points in y direction.
	 * \param  Nx	The number of nodal points in x direction.
	 */
	egd_staggeredNavierStokesSolverBase_t(
		const uSize_t 		Ny,
		const uSize_t 		Nx);


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_staggeredNavierStokesSolverBase_t(
		const egd_staggeredNavierStokesSolverBase_t&)
	 = delete;


	/**
	 * \brief	Copy Assignement.
	 *
	 * Is deleted.
	 */
	egd_staggeredNavierStokesSolverBase_t&
	operator= (
		const egd_staggeredNavierStokesSolverBase_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_staggeredNavierStokesSolverBase_t(
		egd_staggeredNavierStokesSolverBase_t&&);


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_staggeredNavierStokesSolverBase_t&
	operator= (
		egd_staggeredNavierStokesSolverBase_t&&);


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_staggeredNavierStokesSolverBase_t();


	/*
	 * =======================
	 * Private Constructor
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * This is a private constructor, that is only provided
	 * for internal use.
	 */
	egd_staggeredNavierStokesSolverBase_t();





	/*
	 * ======================
	 * Interface functions
	 */
public:
	using ImplBase_t::solve;
	using ImplBase_t::creatResultContainer;
	using ImplBase_t::createArgument;
	using ImplBase_t::creatSourceTerm;
	using ImplBase_t::getGridType;


	/**
	 * \brief	This function returns a property map that is needed by the solver.
	 *
	 * This function will extend the base properties by adding velocity on the grid.
	 */
	virtual
	PropToGridMap_t
	mkNeededProperty()
	 const
	 override;


	/**
	 * \brief	This function returns true if *this is an inertial solver.
	 *
	 * This means it needs a past time step that is supplied by an argument.
	 *
	 * This function returns true.
	 */
	virtual
	bool
	isInertiaSolver()
	 const
	 override;



	/**
	 * \brief	This function outputs a description of *this.
	 *
	 * Since the interessting things are the boundary conditions,
	 * it is not very usefull to implement this function on this
	 * level, however it is usefull to provide a function that
	 * identifies *this, such that it can be used in the
	 * deriving code.
	 *
	 * See also hook_getImplName()
	 */
	virtual
	std::string
	print()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function is for configuring the boundary condition.
	 *
	 * See description in staggered stokes solver.
	 */
	using ImplBase_t::setBoundaryOption;


	/*
	 * ===================================
	 * General Hooks
	 *
	 * These are the hooks that are defined/requiered by
	 * the genral implementation class. Some are implemented
	 * by *this, some are redirected to the base and some are
	 * left abstract.
	 */
protected:
	using ImplBase_t::hook_initBoundaries;
	using ImplBase_t::hook_buildSystemMatrix;
	using ImplBase_t::hook_distributProperties;
	using ImplBase_t::hook_postProcessSolution;
	using ImplBase_t::hook_nUnknownX;
	using ImplBase_t::hook_nUnknownY;
	using ImplBase_t::hook_nPropPerNode;
	using ImplBase_t::hook_reservePerCol;


	/**
	 * \brief	This hook allows the modification of the system matrix.
	 * 		 See super class for more information.
	 *
	 * This function will first call the implementation of the base class and then
	 * introduces inertia to the system.
	 */
	virtual
	bool
	hook_modifySystemMatrix(
		const GridContainer_t&			grid,
		ImplBase_t::SolverArg_i* const 		solArg,
		const bool 				isFirstRun)
	 override;


	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not. See super class.
	 *
	 * True indicates that *this is an extextended grid.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping. See super class.
	 *
	 * This function is not implemented.
	 * It is implemented by the boundary.
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function outputs a small description
	 * 		 of the solver that is implemented.
	 *
	 * This function is used by the boundary conditions's
	 * print function to make a description.
	 *
	 * The default implementation outputs "Staggered Navier-Stokes Solver".
	 */
	virtual
	std::string
	hook_getImplName()
	 const
	 override;



	/*
	 * ===============================
	 * Stokes Hooks
	 *
	 * These are hooks that are needed because *this is a stokes hook.
	 * These fucntions are primarly needed by the composing function.
	 *
	 * There are also a thrid categories of hooks, these hooks deals with
	 * the handling of the boundary condition on a more fine grained scale.
	 */
protected:
	using ImplBase_t::hook_getPScale;



	/*
	 * ==============================
	 * BC Hooks
	 *
	 * These hooks delas exclusively with the boundary conditions.
	 * These functions are called by the building function.
	 * For eqach property there is a dediocated function.
	 * Note that all of these functions are requiered to handle the
	 * boundary conditions on their own.
	 */
protected:
	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VX property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a VX component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vx 	The global index of the node.
	 * \param  UP		Go to the upper VX node.
	 * \param  DOWN		Go to the lower VX node.
	 * \param  RIGHT	Go to the right VX node.
	 * \param  LEFT		Go to the left VX node.
	 */
	virtual
	bool
	hook_boundaryVX(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vx,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override
	 = 0;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VX grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVX(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override
	 = 0;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VY property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a VY component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vy 	The global index of the node.
	 * \param  UP		Go to the upper VY node.
	 * \param  DOWN		Go to the lower VY node.
	 * \param  RIGHT	Go to the right VY node.
	 * \param  LEFT		Go to the left VY node.
	 */
	virtual
	bool
	hook_boundaryVY(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vy,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override
	 = 0;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VY grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVY(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override
	 = 0;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the P property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a P component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_p 		The global index of the node.
	 * \param  UP		Go to the upper P node.
	 * \param  DOWN		Go to the lower P node.
	 * \param  RIGHT	Go to the right P node.
	 * \param  LEFT		Go to the left P node.
	 * \param  pScale	The p scale value.
	 */
	virtual
	bool
	hook_boundaryP(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_p,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT,
		const Numeric_t 	pScale)
	 override
	 = 0;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the P grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryP(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override
	 = 0;


	/*
	 * ======================
	 * Helper functions
	 *
	 * These are functions that are not virtual, but are provided for
	 * helpf of subclasses. They are mostly static.
	 */
protected:
	using ImplBase_t::prot_mkStokesProp;


	/**
	 * \brief	This function creates the property list for a
	 * 		 stokes solver.
	 *
	 * This will call the base version of this and then
	 * extend it.
	 */
	PropToGridMap_t
	prot_mkNavierStokesProp()
	 const;




	/*
	 * ========================
	 * Private Members
	 *
	 * This class does not have any memeber.
	 * It is basically a trampoline that connects the conrete
	 * sub class, the composing of the system matrix and the
	 * solver driver with one another.
	 */
private:
}; //End: class(egd_staggeredNavierStokesSolverBase_t)


PGL_NS_END(egd)


