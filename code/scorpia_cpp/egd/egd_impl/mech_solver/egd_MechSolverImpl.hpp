#pragma once
/**
 * \brief	This file contains all mechanical solver that are provided by EGD.
 */

//Include the confg file
#include <egd_core.hpp>

//Include all solvers
#include "./egd_staggeredStokesSolverBase.hpp"
#include "./egd_staggeredNavierStokesSolverBase.hpp"

//Include Boudnaries
#include "./egd_StokesBoundary_pipeFlow.hpp"
#include "./egd_StokesBoundary_shearFlow.hpp"
#include "./egd_StokesBoundary_pulledBallSin.hpp"
#include "./egd_StokesBoundary_pulledBallConst.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen


//Include STD



PGL_NS_START(egd)

/*
 * =========================
 * Pipe Boundary
 */

/**
 * \brief	This is the regular Stoke solver with pipe flow boundary.
 * \typedef	egd_StokesSolverStdReg_t
 *
 * This is teh regular solver, it is preety much the same that was
 * provided in the first one. However it is a bit different.
 * Now it does not perform interpolation of some properties by itself
 * but relly on the grid container.
 * Also the density stabilization was sigthly modified, such that now
 * it will make use of the newly aviable properties.
 */
using egd_StokesSolverStdReg_t = egd_stokesPipeBoundary_t<egd_staggeredStokesSolverBase_t, false>;


/**
 * \brief	This is the extended Stoke solver.
 * \typedef 	egd_StokesSolverStdExt_t
 *
 * It is basically a prort of the original one to the extended grid.
 * Every comment that was done for the egd_StokesSolverStdReg_t
 * also applies to this one.
 */
using egd_StokesSolverStdExt_t = egd_stokesPipeBoundary_t<egd_staggeredStokesSolverBase_t, true>;


/**
 * \brief	This is the regular Stoke solver with pipe flow boundary.
 * \typedef	egd_NavierStokesSolverStdReg_t
 *
 * This is the standard Navier Stokes solver with ppe flow boundary conditions.
 * It operates on the regular gird.
 */
using egd_NavierStokesSolverStdReg_t = egd_stokesPipeBoundary_t<egd_staggeredNavierStokesSolverBase_t, false>;


/**
 * \brief	This is the extended Stoke solver.
 * \typedef 	egd_NavierStokesSolverStdExt_t
 *
 * This is the basic navier stokes solver with inertia.
 * It operates on the extended grid.
 */
using egd_NavierStokesSolverStdExt_t = egd_stokesPipeBoundary_t<egd_staggeredNavierStokesSolverBase_t, true>;


/*
 * ===========================
 * Shearing solver
 */

/**
 * \brief	This is the extended Stokes solver, for the shear setting.
 * \typedef	egd_StokesSolverShearExt_t
 *
 * This solver solves the stokes equation on the extended grid. It empoloys
 * the shearing condition.
 */
using egd_StokesSolverShearExt_t = egd_stokesShearBoundary_t<egd_staggeredStokesSolverBase_t, true>;


/**
 * \brief	This is the extended Navier-Stokes solver, for the shear setting.
 * \typedef 	egd_NavierStokesSolverShearExt_t
 *
 * This class solves the Navier-Stokes equation on the extended grid.
 * As boundary conditions it uses the shear conditions.
 */
using egd_NavierStokesSolverShearExt_t = egd_stokesShearBoundary_t<egd_staggeredNavierStokesSolverBase_t, true>;


/*
 * ============================
 * Pulled ball solver
 */

/**
 * \brief	This is the extended navier stokes solver, with the sin pulled ball boundary.
 *
 * The boundary condition will impose a dirichlet condition in the ball's centre.
 * The analytical velocity is given by known sine function.
 * The parameters are readed from the confituration file.
 * The initial centre location of the ball is configured by the setup object.
 */
using egd_NavierStokesSolverPulledBallSinExt_t = egd_stokesPulledBallSinBoundary_t<egd_staggeredNavierStokesSolverBase_t, true>;


/**
 * \brief	This is the extended navier stokes solver, with the constantly pulled ball boundary.
 *
 * The boundary condition will impose a dirichlet condition in the ball's centre.
 * The parameters are readed from the confituration file.
 * The initial centre location of the ball is configured by the setup object.
 */
using egd_NavierStokesSolverPulledBallConstExt_t = egd_stokesPulledBallConstBoundary_t<egd_staggeredNavierStokesSolverBase_t, true>;


/**
 * \brief	This is the extended stokes solver, with the sin pulled ball boundary.
 *
 * The boundary condition will impose a dirichlet condition in the ball's centre.
 * The analytical velocity is given by known sine function.
 * The parameters are readed from the confituration file.
 * The initial centre location of the ball is configured by the setup object.
 *
 * Note that this is not a navier stokes solver, but only a stokes solver.
 */
using egd_StokesSolverPulledBallSinExt_t = egd_stokesPulledBallSinBoundary_t<egd_staggeredStokesSolverBase_t, true>;


/**
 * \brief	This is the extended stokes solver, with the constantly pulled ball boundary.
 *
 * The boundary condition will impose a dirichlet condition in the ball's centre.
 * The parameters are readed from the confituration file.
 * The initial centre location of the ball is configured by the setup object.
 *
 * Note that this is not a navier stokes solver, but only a stokes solver.
 */
using egd_StokesSolverPulledBallConstExt_t = egd_stokesPulledBallConstBoundary_t<egd_staggeredStokesSolverBase_t, true>;


PGL_NS_END(egd)

