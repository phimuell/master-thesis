#pragma once
/**
 * \brief	This class implements a boundary condition for the constaly pulled ball.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include "./egd_staggeredNavierStokesSolverBase.hpp"
#include "./egd_StokesBoundary_pulledBallSin.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_map.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>




PGL_NS_START(egd)


/**
 * \brief	This boundary condition is for imposing a velocity on the constantly moved ball.
 * \class 	egd_stokesPulledBallConstBoundary_t
 *
 * This class overrides the behaviour of the pulled ball for the sin version of the boundary.
 * The ball will now move according to a constant velocity.
 * The velocity is read from the setup field "vx" and "vy".
 *
 * See also discuission about the behaviiour of the kind of boundary.
 *
 * \tparam  BaseImpl_t 		This is the base implementation.
 * \tparam  isExtended		This is a bool that indicates if the grid is extended or not.
 *
 */
template<
	class 	ImplBase_T,
	bool 	isExtended
>
class egd_stokesPulledBallConstBoundary_t :
#if defined(YCM) && YCM != 0
	public egd_stokesPulledBallSinBoundary_t<egd_staggeredNavierStokesSolverBase_t, true>	//exposition only
#else
	public egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>
#endif
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Base_t = egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>;	//!< This is the base class that is used.

	using typename Base_t::Size_t;
	using typename Base_t::uSize_t;
	using typename Base_t::Index_t;
	using typename Base_t::Numeric_t;

	using typename Base_t::Matrix_t;
	using typename Base_t::InterfaceBase_t;

	using typename Base_t::SolverResult_t;
	using typename Base_t::SolverResult_ptr;

	using typename Base_t::SolverArg_t;
	using typename Base_t::SolverArg_ptr;
	using genSolverArg_t = egd_generalSolverArg_i;

	using typename Base_t::GridContainer_t;
	using typename Base_t::GridGeometry_t;
	using typename Base_t::GridPosition_cref;
	using typename Base_t::GridProperty_cref;
	using typename Base_t::SystemMatrix_t;
	using typename Base_t::Vectot_t;

	using typename Base_t::BoundaryCondition_t;
	using SingleCondition_t = typename BoundaryCondition_t::SideCondition_t;
	using typename Base_t::BCMap_t;


	/*
	 * =======================
	 * Static asserts
	 */
	static_assert(std::is_unsigned<uSize_t>::value, "uSize must be unsigned");



	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is a building constructor that
	 * 		 is requiered by the factory system.
	 *
	 * This constructor extracts the necessary variables
	 * from the configuration object. If thsi constructor
	 * is used it is requiered that the VX and VY
	 * values are present.
	 *
	 * \param  confObj 	The configuration object that
	 * 			 is beeing used.
	 */
	egd_stokesPulledBallConstBoundary_t(
		const egd_confObj_t& 		confObj);


	/**
	 * \brief	This constreuctor is used directly construct.
	 *
	 * This consttructor requieres that VX and VY are not NAN.
	 *
	 * \param   VX		Alpha parameter
	 * \param   VY 		Omega Parameter
	 * \param   confObj	The configuration object.
	 */
	egd_stokesPulledBallConstBoundary_t(
		const Numeric_t 		VX,
		const Numeric_t 		VY,
		const egd_confObj_t& 	 	confObj);



	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_stokesPulledBallConstBoundary_t(
		const egd_stokesPulledBallConstBoundary_t&)
	 = delete;


	/**
	 * \brief	Copy Assignement.
	 *
	 * Is deleted.
	 */
	egd_stokesPulledBallConstBoundary_t&
	operator= (
		const egd_stokesPulledBallConstBoundary_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_stokesPulledBallConstBoundary_t(
		egd_stokesPulledBallConstBoundary_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_stokesPulledBallConstBoundary_t&
	operator= (
		egd_stokesPulledBallConstBoundary_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_stokesPulledBallConstBoundary_t()
	 = default;


	/*
	 * =======================
	 * Private Constructor
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted used only for internal purpuse.
	 */
	egd_stokesPulledBallConstBoundary_t()
	 = default;





	/*
	 * ======================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 */
	virtual
	std::string
	print()
	 const
	 override
	{
		return (this->hook_getImplName() + " const pulled ball boundary"
			+ " Ny = " + std::to_string(this->getNy() ) + ";"
			+ " Nx = " + std::to_string(this->getNx() ) + ";"
			+ " Vel_x right = " + std::to_string(this->get_velXRight()  ) + ";"
			+ " Vel_x left  = " + std::to_string(this->get_velXLeft()   ) + ";"
			+ " Vel_y top   = " + std::to_string(this->get_velYTop()    ) + ";"
			+ " Vel_y bott  = " + std::to_string(this->get_velYBot()    ) + ";"
			+ " Press       = " + std::to_string(this->get_pressOffset()) + ";"
			+ " VXball      = " + std::to_string(this->m_constVelX      ) + ";"
			+ " VYball      = " + std::to_string(this->m_constVelY      ) + ";"
			+ " x0          = " + std::to_string(this->getX0()          ) + ";"
			+ " y0          = " + std::to_string(this->getY0()          ) + ";"
			+ " fullCell    = " + (this->useFullCellMode() ? std::string("Yes") : std::string("No"))
#			if defined(EGD_PULLEDBALL_AVG_PRESS) && (EGD_PULLEDBALL_AVG_PRESS != 0)
			+ " (with pressure average);"
#			else
			+ " (moved pressure offset point);"
#			endif
		);
	}; //End: Print


	/*
	 * ===================================
	 * General Hooks
	 *
	 * These are the hooks that are defined/requiered by
	 * the genral implementation class. Some are implemented
	 * by *this, some are redirected to the base and some are
	 * left abstract.
	 */
protected:
	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not. See super class.
	 *
	 * True indicates that *this is an extextended grid.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 override
	{
		return isExtended;
	};


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping. See super class.
	 *
	 * This function is not implemented.
	 *
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	{
		return (isExtended
			? std::string("extendedPulledConstStokes")
			: std::string("regularPulledConstStokes") );
	};





	/*
	 * ===============================
	 * Stokes Hooks
	 *
	 * These are hooks that are needed because *this is a stokes hook.
	 * These fucntions are primarly needed by the composing function.
	 *
	 * There are also a thrid categories of hooks, these hooks deals with
	 * the handling of the boundary condition on a more fine grained scale.
	 */
protected:
	using Base_t::hook_initBoundaries;


	/*
	 * ============================
	 * Pulled Ball Hooks
	 *
	 * These are hooks that are used/needed for the
	 * computation of teh boundary.
	 */
protected:
	/**
	 * \brief	This function returns the x coordinate of the ball at the given time.
	 *
	 * \param  t 		The given time.
	 */
	virtual
	::pgl::Numeric_t
	hook_computeXCoM(
		const Numeric_t 	t)
	 const
	 override;


	/**
	 * \brief	This function returns the y coordinate of the ball at the given time.
	 *
	 * \param  t 		The given time.
	 */
	virtual
	::pgl::Numeric_t
	hook_computeYCoM(
		const Numeric_t 	t)
	 const
	 override;


	/**
	 * \brief	This function is used to compute the velocity of the centre
	 */
	virtual
	void
	hook_computeBallVel(
		const Numeric_t 		t,
		Numeric_t* const 		VX,
		Numeric_t* const 		VY)
	 const
	 override;


	using Base_t::setBoundaryOption;





	/*
	 * ==============================
	 * BC Hooks
	 *
	 * This class does not implement the hooks.
	 * Instead the one of the super class are used.
	 *
	 *
	 */
protected:
	using Base_t::hook_boundaryVX;
	using Base_t::hook_testIfBoundaryVX;
	using Base_t::hook_boundaryVY;
	using Base_t::hook_testIfBoundaryVY;
	using Base_t::hook_boundaryP;
	using Base_t::hook_testIfBoundaryP;


	/*
	 * ================
	 * Variable access
	 */
protected:
#	define IMPORT(x) using Base_t:: get_ ## x
	IMPORT(velXLeft );
	IMPORT(velXRight);
	IMPORT(velYBot  );
	IMPORT(velYTop  );
	IMPORT(pressOffset);
#	undef IMPORT

	using Base_t::getT0;
	using Base_t::getX0;
	using Base_t::getY0;




	/*
	 * ========================
	 * Private Members
	 *
	 * This class stores the boundary conditions.
	 */
private:
	Numeric_t 	m_constVelX = NAN;		//!< Constant velocity in x direction
	Numeric_t 	m_constVelY = NAN;		//!< Constant velocity in y direction
}; //End: class(egd_stokesPulledBallConstBoundary_t)

PGL_NS_END(egd)


#if defined(YCM) && YCM != 0
#else
//If YCM is not parsing this, then include the implementation
#include "./egd_StokesBoundary_pulledBallConst_impl.cpp"
#endif



