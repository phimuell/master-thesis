/**
 * \brief	This file contains the functions that implements the code interface
 * 		 functions that are due to the stokes interface.
 *
 * Note that some of them are also implemented in the constructor file.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)

void
egd_staggeredStokesSolverBase_t::solve(
		const GridContainer_t& 				grid,
		InterfaceBase_t::SolverArg_t* const 		solArg,
		InterfaceBase_t::SolverResult_t* const 		solResult)
{
	pgl_assert(grid.getGeometry().isGridSet(),
		   solArg    != nullptr,
		   solResult != nullptr );

	this->ImplBase_t::solve_impl(
			grid,
			static_cast<ImplBase_t::SolverArg_i*>(solArg),
			static_cast<ImplBase_t::SolverResult_i*>(solResult)
			);

	return;
}; //End solver


bool
egd_staggeredStokesSolverBase_t::isInertiaSolver()
 const
{
	return false;
};


eGridType
egd_staggeredStokesSolverBase_t::getGridType()
 const
{
	if(this->hook_isExtendedGrid() == true)
	{
		return mkExt(eGridType::StGrid);
	};

	return mkReg(eGridType::StGrid);
}; //End: getGridType


egd_staggeredStokesSolverBase_t::PropToGridMap_t
egd_staggeredStokesSolverBase_t::mkNeededProperty()
 const
{
	return this->prot_mkStokesProp();
}; //End: make grid properties

PGL_NS_END(egd)

