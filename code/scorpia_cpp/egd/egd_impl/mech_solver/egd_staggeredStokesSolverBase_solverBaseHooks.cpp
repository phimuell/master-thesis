/**
 * \brief	This file contains the implemenetaion of some hooks thata re inherented
 * 		 By the solver base implementation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)


bool
egd_staggeredStokesSolverBase_t::hook_modifySystemMatrix(
	const GridContainer_t&			grid,
	ImplBase_t::SolverArg_i* const 		solArg,
	const bool 				isFirstRun)
{
	pgl_assert(solArg != nullptr,
		   grid.getGeometry().isGridSet());
	return this->egd_generalSolverImpl_t::hook_modifySystemMatrix(grid, solArg, isFirstRun);
};


void
egd_staggeredStokesSolverBase_t::hook_distributProperties(
	const Vectot_t&					sol,
	const GridContainer_t&				grid,
	ImplBase_t::SolverArg_i*  const 		solArg,
	ImplBase_t::SolverResult_i* const 		solRes)
 const
{
	pgl_assert(sol.size() == this->nTotUnknowns(),
		   grid.getGeometry().isGridSet(),
		   solArg != nullptr,
		   solRes != nullptr);
 	this->egd_generalSolverImpl_t::hook_distributProperties(sol, grid, solArg, solRes);
 	return;
};


void
egd_staggeredStokesSolverBase_t::hook_postProcessSolution(
	const Vectot_t&					sol,
	const GridContainer_t&				grid,
	ImplBase_t::SolverArg_i*  const 		solArg,
	ImplBase_t::SolverResult_i* const 		solRes)
 const
{
	pgl_assert(sol.size() == this->nTotUnknowns(),
		   grid.getGeometry().isGridSet(),
		   solArg != nullptr,
		   solRes != nullptr);

	const Numeric_t pScale = this->hook_getPScale(grid);
		pgl_assert(::pgl::isValidFloat(pScale), pScale > 0.0);

	//Make a mechanical result out of it
	InterfaceBase_t::SolverResult_t* const solResMech = static_cast<InterfaceBase_t::SolverResult_t*>(solRes);

	//Undo the pressure scallingf
	solResMech->getPressure().maxView() *= pScale;
		pgl_assert(solResMech->isFinite());

	return;
}; //End: postprocess solution


egd_staggeredStokesSolverBase_t::Index_t
egd_staggeredStokesSolverBase_t::hook_nUnknownX()
 const
{
	const Index_t Nx = this->getNx();
		pgl_assert(Nx > 1);

	if(this->hook_isExtendedGrid() == true)
	{
		return Index_t(Nx + 1);
	}

	return Nx;
};


egd_staggeredStokesSolverBase_t::Index_t
egd_staggeredStokesSolverBase_t::hook_nUnknownY()
 const
{
	const Index_t Ny = this->getNy();
		pgl_assert(Ny > 1);

	if(this->hook_isExtendedGrid() == true)
	{
		return Index_t(Ny + 1);
	}

	return Ny;
};


egd_staggeredStokesSolverBase_t::Index_t
egd_staggeredStokesSolverBase_t::hook_nPropPerNode()
 const
{
	return 3;
};


egd_staggeredStokesSolverBase_t::Index_t
egd_staggeredStokesSolverBase_t::hook_reservePerCol()
 const
{
	return this->ImplBase_t::hook_reservePerCol();
};


bool
egd_staggeredStokesSolverBase_t::hook_writeSystem(
	const bool 	isFrisRun)
 const
{
	return this->ImplBase_t::hook_writeSystem(isFrisRun);
};

std::string
egd_staggeredStokesSolverBase_t::hook_getImplName()
 const
{
	return std::string("Staggered Stokes Solver on "
			+ (this->hook_isExtendedGrid()
			   ? std::string("extended")
			   : std::string("regular") )
			+ " grid");
};

PGL_NS_END(egd)


