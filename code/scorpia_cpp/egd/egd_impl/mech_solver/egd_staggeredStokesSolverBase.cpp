/**
 * \brief	This file contains some functions that belongs to the base implementation of the stokes solvers.
 *
 * These are mostly helper functions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_staggeredStokesSolverBase_t::PropToGridMap_t
egd_staggeredStokesSolverBase_t::prot_mkStokesProp()
 const
{
	PropToGridMap_t gProps;

	const bool isOnExtGrid = this->hook_isExtendedGrid();

	gProps.addMapping(PropIdx::ViscosityCC(), eGridType::CellCenter, isOnExtGrid);
	gProps.addMapping(PropIdx::Viscosity()  , eGridType::BasicNode , false      );
	gProps.addMapping(PropIdx::Density()    , eGridType::BasicNode , false      );
	gProps.addMapping(PropIdx::DensityVY()  , eGridType::StVy      , isOnExtGrid);
	gProps.addMapping(PropIdx::DensityCC()  , eGridType::CellCenter, isOnExtGrid);

	return gProps;
}; //End: make grid properties


bool
egd_staggeredStokesSolverBase_t::setBoundaryOption(
	const std::string& 		key,
	const Numeric_t 		value)
{
	return false;
	PGL_UNUSED(key, value);
};

PGL_NS_END(egd)

