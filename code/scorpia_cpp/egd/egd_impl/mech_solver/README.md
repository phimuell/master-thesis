# Info
This folder contains the concrete implementation for the mechanical solvers.
These solvers deals with the Stokes solver.

## General Description
The main logic of the solvers are handled in the general solver class.
The solvers that are implemented here, only compose the system matrix.

### Reg vs. Ext
There are two sizes that are handled, the first one is regular size.
This is the original solver. The second is the extended size.
For this size we have an additional row and column.
The additional nodes allows a better handling of BC.

# Stokes vs. Navier-Stokes
The original problem consisted in solving the Stokes equation.
The extension, solving the Navier-Stokes Equation, is implemented quite elegant.
The class for composing the system matrix for the Navier-Stokes equation,
inherent from the Stocks class and will overwrite the function for
updating the matrix.

# Boundary Condition
The boundary condition are not handled inside the composition classes.
Instead they are handled by a different class.
The composition function defines a set of hooks, that allows to working with the boundary,
they also handle them. This classes are derived from the composition class.
This allows to use one composition class for many different boundary conditions.

Note the BC classes are templated, such that they are needed only once.
Note that the BC classes are closely linked to the applyBoundaryCondition classes,
and they must also match up.

## Pipe
This is a boundary condition where a free slip condition is applied.
Meaning that we forces components that are normal to a boundary to
be zero, or a certain value. Components that are tangential to a
boundary have a derivative of zero with respect to the normal
direction of the boundary.

## Shear
This is an extension of the pipe condition. At the outer boundary
it is like the pipe condition. However there is an additional
inner condition. This condition cuts the domain in half on the
interface a shearing condition is in place.

## PulledBall
There are two pulled ball implementation for the boundaries. Once with constant
speed and once with the sine function. Parameters are read in from the configuration
file. Note that the initial position is set by the setup object during its inspection
of the mechanical solver.


