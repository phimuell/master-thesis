/**
 * \brief	This file contains sone smaller functions that does not belongs to the other files.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredNavierStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>



PGL_NS_START(egd)

egd_staggeredNavierStokesSolverBase_t::PropToGridMap_t
egd_staggeredNavierStokesSolverBase_t::prot_mkNavierStokesProp()
 const
{
	//First create the one of the stokes equation.
	PropToGridMap_t gMap = this->prot_mkStokesProp();
		pgl_assert(gMap.isValid());

	//Now we extend it by adding the appropriate velocity properties.
	const bool isExt = this->hook_isExtendedGrid();

	//For VX equation
	gMap.ensureMapping(PropIdx::VelX()     , eGridType::StVx, isExt);
	gMap.ensureMapping(PropIdx::DensityVX(), eGridType::StVx, isExt);

	//For VY equation
	gMap.ensureMapping(PropIdx::VelY()     , eGridType::StVy, isExt);
	gMap.ensureMapping(PropIdx::DensityVY(), eGridType::StVy, isExt);

	pgl_assert(gMap.isValid());
	return gMap;
};//End: make properties.

PGL_NS_END(egd)

