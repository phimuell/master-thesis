/**
 * \brief	In this file contains the code that will modify the system
 * 		 matrix, such that an inertia problem is solved.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredNavierStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>



PGL_NS_START(egd)

bool
egd_staggeredNavierStokesSolverBase_t::hook_modifySystemMatrix(
	const GridContainer_t&			grid,
	ImplBase_t::SolverArg_i* const 		solArg_,
	const bool 				isFirstRun)
{
	using pgl::isValidFloat;

	/*
	 * Call the implementation of the base class
	 */
	const bool baseRes = this->ImplBase_t::hook_modifySystemMatrix(grid, solArg_, isFirstRun);

	/*
	 * In this function we will include inertia into the system.
	 * We do this by no longer assuming that \D_{t}[u_i] == 0,
	 * but that it is unequal zero.
	 */
	const bool isExt = this->hook_isExtendedGrid();		//Are we on an extended grid
	InterfaceBase_t::SolverArg_t* const solArg = dynamic_cast<InterfaceBase_t::SolverArg_t*>(solArg_);

	if(solArg->hasPrevDT() == false)
	{
		throw PGL_EXCEPT_InvArg("The argument does not have a previous time step, that is needed for the inertia solver.");
	};

	/*
	 * Loading the parameter and grid properties
	 */
	const GridProperty_t& RHO_VX   = grid.cgetProperty(PropIdx::DensityVX() );
	const GridProperty_t& RHO_VY   = grid.cgetProperty(PropIdx::DensityVY() );
	const GridProperty_t& VELXg    = grid.cgetProperty(PropIdx::VelX()      );
	const GridProperty_t& VELYg    = grid.cgetProperty(PropIdx::VelY()      );

	const Numeric_t  prevDeltaTime = solArg->getPrevDT();  pgl_assert(isValidFloat(prevDeltaTime), prevDeltaTime > 0.0);
	const Numeric_t iPrevDeltaTime = 1.0 / prevDeltaTime;



	/*
	 * Test the input parameters
	 */
	if(RHO_VX.onStVxPoints()   == false ||
	   RHO_VX.isExtendedGrid() != isExt   )
	{
		throw PGL_EXCEPT_InvArg("The DensityVX grid property is defined incorrectly."
				" Expected it to be on the " + (isExt ? std::string("extended") : std::string("regular")) + " VX grid."
				" But it was defined on " + RHO_VX.getGridType());
	};
	if(RHO_VY.onStVyPoints()   == false ||
	   RHO_VY.isExtendedGrid() != isExt   )
	{
		throw PGL_EXCEPT_InvArg("The DensityVY grid property is defined incorrectly."
				" Expected it to be on the " + (isExt ? std::string("extended") : std::string("regular")) + " VY grid."
				" But it was defined on " + RHO_VY.getGridType());
	};
	if(VELXg.onStVxPoints()   == false ||
	   VELXg.isExtendedGrid() != isExt   )
	{
		throw PGL_EXCEPT_InvArg("The VelX grid property is defined incorrectly."
				" Expected it to be on the " + (isExt ? std::string("extended") : std::string("regular")) + " VX grid."
				" But it was defined on " + VELXg.getGridType());
	};
	if(VELYg.onStVyPoints()   == false ||
	   VELYg.isExtendedGrid() != isExt   )
	{
		throw PGL_EXCEPT_InvArg("The VelY grid property is defined incorrectly."
				" Expected it to be on the " + (isExt ? std::string("extended") : std::string("regular")) + " VY grid."
				" But it was defined on " + VELYg.getGridType());
	};
	if(isValidFloat(iPrevDeltaTime) == false ||
	   iPrevDeltaTime               <= 0.0     )
	{
		throw PGL_EXCEPT_InvArg("The last time step is wrong., the value of prevDT is " + std::to_string(prevDeltaTime));
	};
	pgl_assert(solArg->getPrevDT() == solArg->getTimeStep());


	/*
	 * Load the geomatrical properties that are needed
	 */
	const Index_t nUnknownsX = this->hook_nUnknownX();
	const Index_t nUnknownsY = this->hook_nUnknownY();
	const Index_t nProps     = this->hook_nPropPerNode();
		pgl_assert(nProps == 3);

#	ifndef PGL_NDEBUG
	volatile Index_t nVx = 0, nVy = 0;
#	endif


	/*
	 * Updateing of the system matrix.
	 * We only have to go through the VX and VY equation.
	 */
	for(Index_t j = 0; j != nUnknownsX; ++j)
	{
		for(Index_t i = 0; i != nUnknownsY; ++i)
		{
			//We now compute the index in the big matrix
			const Index_t k_vx = (j * nUnknownsY + i) * 3 + 0;
			const Index_t k_vy = (j * nUnknownsY + i) * 3 + 1;
			const Index_t k_pm = (j * nUnknownsY + i) * 3 + 2;	(void)k_pm;
				pgl_assert(m_L.rows() == m_L.cols());		//This cames from matlab
				pgl_assert(k_vx >= 0           , k_vy >= 0           , k_pm >= 0         ,
					   k_vx <  m_L.rows()  , k_vy <  m_L.rows()  , k_pm <  m_L.rows(),
					   k_vx <  m_L.cols()  , k_vy <  m_L.cols()  , k_pm <  m_L.cols() );
				pgl_assert(k_vx <  m_rhs.size(), k_vy <  m_rhs.size(), k_pm <  m_rhs.size());


			/*
			 * BEGIN:	VX
			 */
			if(this->hook_testIfBoundaryVX(i, j) == false)
			{
				const Numeric_t    rhoVX_ij = RHO_VX(i, j);
				const Numeric_t velXPrev_ij = VELXg(i, j);
					pgl_assert(isValidFloat(rhoVX_ij), isValidFloat(velXPrev_ij) );
					pgl_assert(rhoVX_ij >= 0.0);	//This tests allows to distinguish between
#					ifndef PGL_NDEBUG
					if(!(rhoVX_ij >  0.0))
						{ throw PGL_EXCEPT_InvArg("Non positive density at VX (" + std::to_string(i) + ", " + std::to_string(j) + ") node detected, value was " + std::to_string(rhoVX_ij) + "."); };
#					endif
					pgl_assert(rhoVX_ij >  0.0);	//negative and zero density

				//Handle the current part
				this->m_L.coeffRef(k_vx, k_vx) += -rhoVX_ij * iPrevDeltaTime;

				//Handle the old inertia part
				this->m_rhs[k_vx] += -rhoVX_ij * iPrevDeltaTime * velXPrev_ij;

#				ifndef PGL_NDEBUG
				nVx += 1;
#				endif
			}; //End if: not on vx boundary
			/* END:		VX	*/


			/*
			 * BEGIN:	VY
			 */
			if(this->hook_testIfBoundaryVY(i, j) == false)
			{
				//Load the scalar values
				const Numeric_t    rhoVY_ij = RHO_VY(i, j);
				const Numeric_t velYPrev_ij = VELYg(i, j);
					pgl_assert(isValidFloat(rhoVY_ij), isValidFloat(velYPrev_ij) );
					pgl_assert(rhoVY_ij >= 0.0);	//This tests allows the distinguishen between
#					ifndef PGL_NDEBUG
					if(!(rhoVY_ij >  0.0))
						{ throw PGL_EXCEPT_InvArg("Non positive density at VY (" + std::to_string(i) + ", " + std::to_string(j) + ") node detected, value was " + std::to_string(rhoVY_ij) + "."); };
#					endif
					pgl_assert(rhoVY_ij >  0.0);	//Negative and zero density

				//Handle the current part
				this->m_L.coeffRef(k_vy, k_vy) += -rhoVY_ij * iPrevDeltaTime;

				//Handle the old inertia plan
				this->m_rhs[k_vy] += -rhoVY_ij * iPrevDeltaTime * velYPrev_ij;

#				ifndef PGL_NDEBUG
				nVy += 1;
#				endif
			};//End if: not on vy bound
			/* END:		VY	*/
		}; //End for(i):
	}; //End for(j):

	/*
	 * We only affect the diagonal, so we will not modify the
	 * sparity pattern, so we will return false.
	 */
	return false || baseRes;	//I konw, but this is vay more correctedr
	(void)isFirstRun;
};//ENd: modify sparity pattern

PGL_NS_END(egd)

