#pragma once
/**
 * \brief	This file implements a class that is used to define a boundary that is similar to a pipe.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include "./egd_staggeredStokesSolverBase.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_map.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>




PGL_NS_START(egd)


/**
 * \brief	This is a class that implements pipe boundaries.
 * \class 	egd_stokesBoundaryPipe_t
 *
 * These are boundaries where the velocity component that is
 * normal to the boundary, is forced to a specific value.
 * If this value is zero, then there is no penetration in that direction.
 * Velocities that are tangential are governed by a Neumann condition,
 * such that now flow ovvurees over that direction.
 * The corner points are handled by the Neumann condition.
 *
 * The pressure is special, such that a value is forced to a specific value.
 * Depending on the grid, several other conditions are applied.
 *
 * \tparam  BaseImpl_t 		This is the base implementation.
 * \tparam  isExtended		This is a bool that indicates if the grid is extended or not.
 *
 * \todo 	Add a second template parameter that lets choose the super class.
 */
template<
	class 	ImplBase_t,
	bool 	isExtended
>
class egd_stokesPipeBoundary_t : public
#if defined(YCM) && YCM != 0
						egd_staggeredStokesSolverBase_t
#else
						ImplBase_t
#endif
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
		static_assert(std::is_unsigned<uSize_t>::value, "uSize must be unsigned");
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using Matrix_t 	= egd_Matrix_t<Numeric_t>;	//!< This is the type of the reuslts.

	//using ImplBase_t 	= egd_generalSolverImpl_t;	//!< This is the type that provides the general implementation.
	using InterfaceBase_t 	= egd_StokesSolver_i;		//!< This is the type that is the interface we are implementing.

	using SolverResult_t 	= egd_StokesSolver_i::SolverResult_t;		//!< This is the result type of the Stokes solver.
	using SolverResult_ptr 	= SolverResult_t::MechSolverResult_ptr;	//!< Pointer to the result type.

	using SolverArg_t  	= egd_StokesSolver_i::SolverArg_t;
	using SolverArg_ptr 	= SolverArg_t::MechSolverArg_ptr;

	using GridContainer_t 	= egd_gridContainer_t;			//!< This is the grid container.
	using GridGeometry_t  	= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.
	using GridProperty_cref = GridContainer_t::GridProperty_cref;	//!< This is the property on the grid.
	using GridPosition_cref = GridContainer_t::GridPosition_cref;	//!< This is the grid position.
	using SystemMatrix_t 	= egd_SparseMatrix_t<Numeric_t>;	//!< This is the system matrix Type.
	using Vectot_t 		= egd_Vector_t<Numeric_t>;		//!< This is the type for a simple dense vector.

	using BoundaryCondition_t = egd_boundaryConditions_t;		//!< This is the class for storing boundaries.
	using BCMap_t		  = egd_BCMap_t;			//!< This is a map that stores BC.


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the full building constructor.
	 *
	 * This constructor allows to specifiy the boundary condition.
	 * The normal velocity at the respective boundaries can be specified,
	 * and the pressure offset can be specified.
	 *
	 * Note that the ordering of the Nx and Ny is swapped and that Ny
	 * comes first. Also the pressure term came first.
	 *
	 * Note that this function does not call allocateSystem.
	 * The function is called upon the first solving process.
	 * This is due to work arround a problem in C++.
	 *
	 * \param  Ny		The number of nodal points in y direction.
	 * \param  Nx		The number of nodal points in x direction.
	 * \param  pressOffset	The pressure force term.
	 * \param  velXLeft 	The x velocity at the left boundary.
	 * \param  velXRight	The x velocity at the right boundary.
	 * \param  velYTop	The y velocity at the top boundary.
	 * \param  velYBot 	The y velocity at the bottom boundary.
	 */
	egd_stokesPipeBoundary_t(
		const uSize_t 			Ny,
		const uSize_t 			Nx,
		const Numeric_t 		pressOffset,
		const Numeric_t 		velXLeft,
		const Numeric_t 		velXRight,
		const Numeric_t 		velYTop,
		const Numeric_t 		velYBot,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	This constructor operates on the boundary conditions.
	 *
	 * \param  Ny		The number of nodal points in y direction.
	 * \param  Nx		The number of nodal points in x direction.
	 * \param  pressCond	BC for the pressure.
	 * \param  velXCond	BC for the velocity in X.
	 * \param  velYCond	BC for the velocity in Y.
	 */
	egd_stokesPipeBoundary_t(
		const uSize_t 			Ny,
		const uSize_t 			Nx,
		const BoundaryCondition_t& 	pressCond,
		const BoundaryCondition_t& 	velXCond,
		const BoundaryCondition_t& 	velYCond,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	This constructor extracts everything from the
	 * 		 condition map.
	 *
	 * \param  Ny		The number of nodal points in y direction.
	 * \param  Nx		The number of nodal points in x direction.
	 * \param  bcMap 	The condition map
	 */
	egd_stokesPipeBoundary_t(
		const uSize_t 			Ny,
		const uSize_t 			Nx,
		const BCMap_t 			bcMap,
		const egd_confObj_t* const 	confObj = nullptr)
	 :
	  egd_stokesPipeBoundary_t(
	  		Ny, Nx,
	  		bcMap[PropIdx::Pressure()],
	  		bcMap[PropIdx::VelX()    ], bcMap[PropIdx::VelY()    ],
	  		confObj)
	{
		if(bcMap.isValid() == false)
			{ throw PGL_EXCEPT_InvArg("The passed BCMap is not valid."); };
	};



	/**
	 * \brief	This is a building constructor that
	 * 		 is requiered by the factory system.
	 *
	 * This constructor extracts the necessary variables
	 * from the configuration object.
	 *
	 * \param  confObj 	The configuration object that
	 * 			 is beeing used.
	 */
	egd_stokesPipeBoundary_t(
		const egd_confObj_t& 		confObj);


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_stokesPipeBoundary_t(
		const egd_stokesPipeBoundary_t&)
	 = delete;


	/**
	 * \brief	Copy Assignement.
	 *
	 * Is deleted.
	 */
	egd_stokesPipeBoundary_t&
	operator= (
		const egd_stokesPipeBoundary_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_stokesPipeBoundary_t(
		egd_stokesPipeBoundary_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_stokesPipeBoundary_t&
	operator= (
		egd_stokesPipeBoundary_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_stokesPipeBoundary_t()
	 = default;


	/*
	 * =======================
	 * Private Constructor
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted used only for internal purpuse.
	 */
	egd_stokesPipeBoundary_t()
	 = default;





	/*
	 * ======================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 */
	virtual
	std::string
	print()
	 const
	 override
	{
		return (this->hook_getImplName()
			+ " Ny = "          + std::to_string(this->getNy()) + ";"
			+ " Nx = "          + std::to_string(this->getNx()) + ";"
			+ " Vel_x right = " + std::to_string(m_velXRight  ) + ";"
			+ " Vel_x left  = " + std::to_string(m_velXLeft   ) + ";"
			+ " Vel_y top   = " + std::to_string(m_velYTop    ) + ";"
			+ " Vel_y bott  = " + std::to_string(m_velYBot    ) + ";"
			+ " Press       = " + std::to_string(m_pressOffset) + ";"
		);
	}; //End: Print


	/*
	 * ===================================
	 * General Hooks
	 *
	 * These are the hooks that are defined/requiered by
	 * the genral implementation class. Some are implemented
	 * by *this, some are redirected to the base and some are
	 * left abstract.
	 */
protected:
	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not. See super class.
	 *
	 * True indicates that *this is an extextended grid.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 override
	{
		return isExtended;
	};


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping. See super class.
	 *
	 * This function is not implemented.
	 *
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	{
		return (isExtended
			? std::string("extendedPipeStokes")
			: std::string("regularPipeStokes") );
	};


	/*
	 * ===============================
	 * Stokes Hooks
	 *
	 * These are hooks that are needed because *this is a stokes hook.
	 * These fucntions are primarly needed by the composing function.
	 *
	 * There are also a thrid categories of hooks, these hooks deals with
	 * the handling of the boundary condition on a more fine grained scale.
	 */
protected:


	/*
	 * ==============================
	 * BC Hooks
	 *
	 * These hooks delas exclusively with the boundary conditions.
	 * These functions are called by the building function.
	 * For eqach property there is a dediocated function.
	 * Note that all of these functions are requiered to handle the
	 * boundary conditions on their own.
	 */
protected:
	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VX property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a VX component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vx 	The global index of the node.
	 * \param  UP		Go to the upper VX node.
	 * \param  DOWN		Go to the lower VX node.
	 * \param  RIGHT	Go to the right VX node.
	 * \param  LEFT		Go to the left VX node.
	 */
	virtual
	bool
	hook_boundaryVX(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vx,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VX grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVX(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VY property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a VY component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vy 	The global index of the node.
	 * \param  UP		Go to the upper VY node.
	 * \param  DOWN		Go to the lower VY node.
	 * \param  RIGHT	Go to the right VY node.
	 * \param  LEFT		Go to the left VY node.
	 */
	virtual
	bool
	hook_boundaryVY(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vy,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VY grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVY(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the P property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a P component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_p 		The global index of the node.
	 * \param  UP		Go to the upper P node.
	 * \param  DOWN		Go to the lower P node.
	 * \param  RIGHT	Go to the right P node.
	 * \param  LEFT		Go to the left P node.
	 * \param  pScale	This is the p scale value.
	 */
	virtual
	bool
	hook_boundaryP(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_p,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT,
		const Numeric_t 	pScale)
	 override;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the P grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryP(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override;


	/*
	 * ================
	 * Variable access
	 */
protected:
#	define GET(x) Numeric_t get_ ## x () const { pgl_assert(::pgl::isValidFloat(this->m_ ## x)); return this->m_ ## x ;}
	GET(velXLeft );
	GET(velXRight);
	GET(velYTop );
	GET(velYBot);
	GET(pressOffset);
#	undef GET






	/*
	 * ========================
	 * Private Members
	 *
	 * This class stores the boundary conditions.
	 */
private:
	//Here are the boundary conditions
	Numeric_t 		m_velXLeft    = 0.0;	//!< Velocity of the x component at the left boundary
	Numeric_t 		m_velXRight   = 0.0;	//!< Velocity of the x component at the right boundary
	Numeric_t 		m_velYTop     = 0.0;	//!< Velocity of the y component at the top boundary
	Numeric_t 		m_velYBot     = 0.0;	//!< Velocity of the y component at the bottom boundary
	Numeric_t 		m_pressOffset = 1e+9;   //!< Pressure force in one node; needed for offset
}; //End: class(egd_stokesPipeBoundary_t)

PGL_NS_END(egd)


#if defined(YCM) && YCM != 0
#else
//If YCM is not parsing this, then include the implementation
#include "./egd_StokesBoundary_pipeFlow_impl.cpp"
#endif



