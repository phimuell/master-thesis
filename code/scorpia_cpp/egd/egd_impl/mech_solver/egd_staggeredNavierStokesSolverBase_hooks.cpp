/**
 * \brief	This file contains tzhe implementation of the smaller hook functions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredNavierStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_staggeredNavierStokesSolverBase_t::PropToGridMap_t
egd_staggeredNavierStokesSolverBase_t::mkNeededProperty()
 const
{
	PropToGridMap_t gMap = this->prot_mkNavierStokesProp();
		pgl_assert(gMap.isValid());

	return gMap;
}; //End: make property map


std::string
egd_staggeredNavierStokesSolverBase_t::hook_getImplName()
 const
{
	return std::string("Staggered Navier-Stokes Solver on "
			+ (this->hook_isExtendedGrid()
			   ? std::string("extended")
			   : std::string("regular") )
			+ " grid");
}; //ENnd get hook


bool
egd_staggeredNavierStokesSolverBase_t::isInertiaSolver()
 const
{
	return true;
};


PGL_NS_END(egd)

