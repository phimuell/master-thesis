#pragma once
/**
 * \brief	This class implements the solver boundary for the shearing szenario.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include "./egd_staggeredStokesSolverBase.hpp"
#include "./egd_StokesBoundary_pipeFlow.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_map.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>




PGL_NS_START(egd)


/**
 * \brief	This class implements the searing boundary conditons.
 * \class 	egd_stokesShearBoundary_t
 *
 * This class is its relative from teh apply family an extension of the
 * pipe bvoundary. It will add an internal boundary condition.
 *
 * Note that there are several restrictions. First *this supports
 * only extended solvers, and second, the domain must be squared.
 *
 * \tparam  BaseImpl_t 		This is the base implementation.
 * \tparam  isExtended		This is a bool that indicates if the grid is extended or not.
 *
 */
template<
	class 	ImplBase_t,
	bool 	isExtended
>
class egd_stokesShearBoundary_t :
#if defined(YCM) && YCM != 0
	public egd_stokesPipeBoundary_t<egd_staggeredStokesSolverBase_t, true>
#else
	public egd_stokesPipeBoundary_t<ImplBase_t, isExtended>
#endif
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Base_t = egd_stokesPipeBoundary_t<ImplBase_t, isExtended>;	//!< This is the base class that is used.

	using typename Base_t::Size_t;
	using typename Base_t::uSize_t;
	using typename Base_t::Index_t;
	using typename Base_t::Numeric_t;

	using typename Base_t::Matrix_t;
	using typename Base_t::InterfaceBase_t;

	using typename Base_t::SolverResult_t;
	using typename Base_t::SolverResult_ptr;

	using typename Base_t::SolverArg_t;
	using typename Base_t::SolverArg_ptr;

	using typename Base_t::GridContainer_t;
	using typename Base_t::GridGeometry_t;
	using typename Base_t::GridPosition_cref;
	using typename Base_t::GridProperty_cref;
	using typename Base_t::SystemMatrix_t;
	using typename Base_t::Vectot_t;

	using typename Base_t::BoundaryCondition_t;
	using SingleCondition_t = typename BoundaryCondition_t::SideCondition_t;
	using typename Base_t::BCMap_t;


	/*
	 * =======================
	 * Static asserts
	 */
	static_assert(std::is_unsigned<uSize_t>::value, "uSize must be unsigned"                                    );
	static_assert(isExtended == true              , "Only extended solvers are suppoted by this boundary class.");



	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief 	This is a boulding constructor. It operates directly on boundary conditions.
	 *
	 * Note that Ny and Nx must be the same value.
	 *
	 * \param  Ny		The number of nodal points in y direction.
	 * \param  Nx		The number of nodal points in x direction.
	 * \param  pressCond	BC for the pressure.
	 * \param  velXCond	BC for the velocity in X.
	 * \param  velYCond	BC for the velocity in Y.
	 */
	egd_stokesShearBoundary_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t		Nx,
		const BoundaryCondition_t& 	pressCond,
		const BoundaryCondition_t& 	velXCond,
		const BoundaryCondition_t& 	velYCond,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	This constructor extracts everything from the
	 * 		 condition map.
	 *
	 * \param  Ny		The number of nodal points in y direction.
	 * \param  Nx		The number of nodal points in x direction.
	 * \param  bcMap 	The condition map
	 */
	egd_stokesShearBoundary_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t		Nx,
		const BCMap_t 			bcMap,
		const egd_confObj_t* const 	confObj = nullptr)
	 :
	  egd_stokesShearBoundary_t(
	  		Ny, Nx,
	  		bcMap[PropIdx::Pressure()],
	  		bcMap[PropIdx::VelX()    ], bcMap[PropIdx::VelY()    ],
	  		confObj)
	{
		if(bcMap.isValid() == false)
			{ throw PGL_EXCEPT_InvArg("The passed BCMap is not valid."); };
	};



	/**
	 * \brief	This is a building constructor that
	 * 		 is requiered by the factory system.
	 *
	 * This constructor extracts the necessary variables
	 * from the configuration object.
	 *
	 * \param  confObj 	The configuration object that
	 * 			 is beeing used.
	 */
	egd_stokesShearBoundary_t(
		const egd_confObj_t& 		confObj);


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_stokesShearBoundary_t(
		const egd_stokesShearBoundary_t&)
	 = delete;


	/**
	 * \brief	Copy Assignement.
	 *
	 * Is deleted.
	 */
	egd_stokesShearBoundary_t&
	operator= (
		const egd_stokesShearBoundary_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_stokesShearBoundary_t(
		egd_stokesShearBoundary_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_stokesShearBoundary_t&
	operator= (
		egd_stokesShearBoundary_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_stokesShearBoundary_t()
	 = default;


	/*
	 * =======================
	 * Private Constructor
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted used only for internal purpuse.
	 */
	egd_stokesShearBoundary_t()
	 = default;





	/*
	 * ======================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 */
	virtual
	std::string
	print()
	 const
	 override
	{
		return (this->hook_getImplName() + " shear boundary"
			+ " Ny = " + std::to_string(this->getNy() ) + ";"
			+ " Nx = " + std::to_string(this->getNx() ) + ";"
			+ " Vel_x right = " + std::to_string(this->get_velXRight()  ) + ";"
			+ " Vel_x left  = " + std::to_string(this->get_velXLeft()   ) + ";"
			+ " Vel_y top   = " + std::to_string(this->get_velYTop()    ) + ";"
			+ " Vel_y bott  = " + std::to_string(this->get_velYBot()    ) + ";"
			+ " Press       = " + std::to_string(this->get_pressOffset()) + ";"
			+ " Low Wegd = (" + std::to_string(this->m_lowWed_VelX) + ", " + std::to_string(this->m_lowWed_VelY) + ") ;"
			+ " Up Wegd  = (" + std::to_string(this->m_upWed_VelX ) + ", " + std::to_string(this->m_upWed_VelY ) + ") ;"
		);
	}; //End: Print


	/*
	 * ===================================
	 * General Hooks
	 *
	 * These are the hooks that are defined/requiered by
	 * the genral implementation class. Some are implemented
	 * by *this, some are redirected to the base and some are
	 * left abstract.
	 */
protected:
	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not. See super class.
	 *
	 * True indicates that *this is an extextended grid.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 override
	{
		pgl_assert(isExtended);
		return isExtended;
	};


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping. See super class.
	 *
	 * This function is not implemented.
	 *
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	{
		pgl_assert(isExtended);
		return (isExtended
			? std::string("extendedPipeStokes")
			: std::string("regularPipeStokes") );
	};


	/*
	 * ===============================
	 * Stokes Hooks
	 *
	 * These are hooks that are needed because *this is a stokes hook.
	 * These fucntions are primarly needed by the composing function.
	 *
	 * There are also a thrid categories of hooks, these hooks deals with
	 * the handling of the boundary condition on a more fine grained scale.
	 */
protected:


	/*
	 * ==============================
	 * BC Hooks
	 *
	 * These are the hook that handling the boundary conditins.
	 * meaning manipulate the system matrix in the respective way.
	 * Note that the pressure condition boundary is not handled
	 * by this class but completly by the other class.
	 */
protected:
	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VX property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a VX component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vx 	The global index of the node.
	 * \param  UP		Go to the upper VX node.
	 * \param  DOWN		Go to the lower VX node.
	 * \param  RIGHT	Go to the right VX node.
	 * \param  LEFT		Go to the left VX node.
	 */
	virtual
	bool
	hook_boundaryVX(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vx,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VX grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVX(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VY property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a VY component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vy 	The global index of the node.
	 * \param  UP		Go to the upper VY node.
	 * \param  DOWN		Go to the lower VY node.
	 * \param  RIGHT	Go to the right VY node.
	 * \param  LEFT		Go to the left VY node.
	 */
	virtual
	bool
	hook_boundaryVY(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vy,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VY grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVY(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override;


	/*
	 * For handling the pressure condition use
	 * the one that are implemented for the
	 * pipe condition
	 */
	using Base_t::hook_boundaryP;
	using Base_t::hook_testIfBoundaryP;


	/*
	 * ================
	 * Variable access
	 */
protected:
#	define IMPORT(x) using Base_t:: get_ ## x
	IMPORT(velXLeft );
	IMPORT(velXRight);
	IMPORT(velYBot  );
	IMPORT(velYTop  );
	IMPORT(pressOffset);
#	undef IMPORT


	/*
	 * ========================
	 * Private Members
	 *
	 * This class stores the boundary conditions.
	 */
private:
	Numeric_t 		m_lowWed_VelY = NAN;		//!< y velcoity of the lower wedge.
	Numeric_t 		m_lowWed_VelX = NAN;		//!< x velocity of the lower wedge.
	Numeric_t 		m_upWed_VelY  = NAN;		//!< y velcoity of the upper wedge.
	Numeric_t 		m_upWed_VelX  = NAN;		//!< x velocity of the upper wedge.
}; //End: class(egd_stokesShearBoundary_t)

PGL_NS_END(egd)


#if defined(YCM) && YCM != 0
#else
//If YCM is not parsing this, then include the implementation
#include "./egd_StokesBoundary_shearFlow_impl.cpp"
#endif



