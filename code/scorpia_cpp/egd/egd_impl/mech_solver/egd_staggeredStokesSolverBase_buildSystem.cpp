/**
 * \brief	This file implements the system building process.
 *
 * It does not compose the boundaries directly.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredStokesSolverBase.hpp"
#include <egd_impl/genSolver/egd_generalSolver.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


void
egd_staggeredStokesSolverBase_t::hook_buildSystemMatrix(
	const GridContainer_t& 			grid,
	ImplBase_t::SolverArg_i* const 		solArg_)
{
	using ::pgl::isValidFloat;

	pgl_assert(this->isAllocated());
	pgl_assert(m_L.rows() == m_L.cols(),
		   m_L.rows() == m_rhs.size(),
		   m_L.rows() == this->nTotUnknowns(),
		   m_L.rows() > 0);

	//Load aliases
	const GridGeometry_t& gridGeo = grid.getGeometry();	//Alias the geometry
	pgl_assert(solArg_ != nullptr);
	InterfaceBase_t::SolverArg_t* const solArg = dynamic_cast<InterfaceBase_t::SolverArg_t*>(solArg_);

	/*
	 * Make some tests
	 */
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set.");
	};
	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("Only constant grids are supported.");
	};

	/*
	 * Load sourceterms if needed
	 */
	const bool hasSourceTerm = solArg->hasSourceTerm();
	const GridProperty_t* const sourceVelX = hasSourceTerm
						 ? &( solArg->getSourceFor(PropIdx::VelX()     ) )
						 : nullptr;
	const GridProperty_t* const sourceVelY = hasSourceTerm
						 ? &( solArg->getSourceFor(PropIdx::VelY()     ) )
						 : nullptr;
	const GridProperty_t* const sourceP    = hasSourceTerm
						 ? &( solArg->getSourceFor(PropIdx::Pressure() ) )
						 : nullptr;

	if(hasSourceTerm == true)
	{
		pgl_assert(sourceVelX != nullptr,
			   sourceVelY != nullptr,
			   sourceP != nullptr    );

		const bool thisIsExtended = this->hook_isExtendedGrid();

		if((sourceVelX->isExtendedGrid()   != thisIsExtended) ||
		   (sourceVelX->onStVxPoints()     == false         )   )
		{
			throw PGL_EXCEPT_RUNTIME("The passed source for VelX and the solver does not agree.");
		};
		if((sourceVelY->isExtendedGrid()   != thisIsExtended) ||
		   (sourceVelY->onStVyPoints()     == false         )   )
		{
			throw PGL_EXCEPT_RUNTIME("The passed source for VelY and the solver does not agree.");
		};
		if((sourceP->isExtendedGrid()      != thisIsExtended) ||
		   (sourceP->onStPresseurePoints() == false       )     )
		{
			throw PGL_EXCEPT_RUNTIME("The passed source for Pressure and the solver does not agree.");
		};
	}; //End check if they are correct


	/*
	 * Load grid properties
	 */

	//Basic nodal grids
	GridProperty_cref RHO = grid.getProperty(PropIdx::Density());
	GridProperty_cref ETA = grid.getProperty(PropIdx::Viscosity());
		if(RHO.onBasicNodalPoints() == false)
		{
			throw PGL_EXCEPT_InvArg("RHO is not on basic grid points.");
		};
		if(ETA.onBasicNodalPoints() == false)
		{
			throw PGL_EXCEPT_InvArg("ETA is not on basic grid points.");
		};

	//CC
	GridProperty_cref ETA_CC = grid.getProperty(PropIdx::ViscosityCC());
	GridProperty_cref RHO_CC = grid.getProperty(PropIdx::DensityCC()  );
	GridProperty_cref RHO_VY = grid.getProperty(PropIdx::DensityVY()  );
		if(ETA_CC.onCellCentrePoints() == false)
		{
			throw PGL_EXCEPT_InvArg("ETA_CC is not on CC points.");
		};
		if(RHO_CC.onCellCentrePoints() == false)
		{
			throw PGL_EXCEPT_InvArg("RHO_CC is not on CC points.");
		};
		if(RHO_VY.onStVyPoints() == false)
		{
			throw PGL_EXCEPT_InvArg("RHO_VY is not on VY points.");
		};

	/*
	 * Load grid geometry paramter.
	 */
	const Numeric_t DeltaX   = gridGeo.getXSpacing();
	const Numeric_t DeltaY   = gridGeo.getYSpacing();
		pgl_assert(isValidFloat(DeltaX), DeltaX > 0.0);
		pgl_assert(isValidFloat(DeltaY), DeltaY > 0.0);

	//Some hand values when dealing with spacings
	const Numeric_t DeltaY2  = DeltaY * DeltaY;
	const Numeric_t DeltaX2  = DeltaX * DeltaX;
	const Numeric_t DeltaXY  = DeltaX * DeltaY;
	const Numeric_t iDeltaX  = 1.0 / DeltaX;
	const Numeric_t iDeltaY  = 1.0 / DeltaY;
	const Numeric_t iDeltaX2 = 1.0 / DeltaX2;
	const Numeric_t iDeltaY2 = 1.0 / DeltaY2;
	const Numeric_t iDeltaXY = 1.0 / DeltaXY;

	//Unknowns
	const Index_t nUnknownsX = this->hook_nUnknownX();
	const Index_t nUnknownsY = this->hook_nUnknownY();
	const Index_t nProps     = this->hook_nPropPerNode();
		pgl_assert(nProps == 3);

	//
	//These are neighbours delta.
	//The delta is according to a drawing.
	//Meaning UP is goping upwards towards the ground.
	//DOWN is increaing the depth.
	const Index_t UP    = -3;		//Going towards SMALLER y values (going towards the surface)
	const Index_t DOWN  = +3;		//Going towards LARGER y values (going deeper)
	const Index_t LEFT  = -3 * nUnknownsY;	//Going towards smaller x values
	const Index_t RIGHT = +3 * nUnknownsY; 	//Going towards greater x values


	//Load the pressure scaling
	const Numeric_t pScale = this->hook_getPScale(grid);

	//
	//Unpack the argument

	//Timestep
	const bool useDensStab       = solArg->isDensityStabOn();	//Test if needed
	const Numeric_t rhoStabTime  = useDensStab
				       ? solArg->getDensityStabTime()
				       : 0.0;

	//Gravity
	const Numeric_t gravY = solArg->g_y();
	if(solArg->g_x() != 0.0)
	{
		throw PGL_EXCEPT_InvArg("Only y gravitation is supported.");
	};


	/*
	 * Doing it this way is not so optimal, but it has one great adventage,
	 * we have to write it only once. There are some semi hacky way
	 * to avoid that, but they are very complicated and uncalled for.
	 *
	 * So we will make it that way, we also have the great problem, that
	 * the way we do the insertion is not the way Eigen likes them.
	 * It is also not so cache friendly, but it is easy and clear.
	 *
	 * If the thing is too slow, then we should optimize here a bit.
	 *
	 *
	 * In the follwong loop we will iterate through all the nodal points
	 * and create the matrix.
	 * In the system matrix each row corresponds to one parameter.
	 * Each coefficient in that row corresponds towards an interaction
	 * with another parameter.
	 */
	for(Index_t j = 0; j != nUnknownsX; ++j)
	{
		for(Index_t i = 0; i != nUnknownsY; ++i)
		{
			//We now compute the index in the big matrix
			const Index_t k_vx = (j * nUnknownsY + i) * 3 + 0;
			const Index_t k_vy = (j * nUnknownsY + i) * 3 + 1;
			const Index_t k_pm = (j * nUnknownsY + i) * 3 + 2;
				pgl_assert(m_L.rows() == m_L.cols());		//This cames from matlab
				pgl_assert(k_vx >= 0           , k_vy >= 0           , k_pm >= 0         ,
					   k_vx <  m_L.rows()  , k_vy <  m_L.rows()  , k_pm <  m_L.rows(),
					   k_vx <  m_L.cols()  , k_vy <  m_L.cols()  , k_pm <  m_L.cols() );
				pgl_assert(k_vx <  m_rhs.size(), k_vy <  m_rhs.size(), k_pm <  m_rhs.size());


			/* BEGIN	V X 		*/

			//Detect if we are on the boundary
			if(this->hook_boundaryVX(		//This function also handles the boundary.
					i, j, k_vx,
					UP, DOWN, RIGHT, LEFT
				) == false)			//Testing of false, means that we are not on the boundary.
			{
				/*
				 * We are inside the domain
				 *
				 * 0-------------------------------------------> X/J
				 * |                    j
				 * |     o--------------o--------------o
				 * |     |              |              |
				 * |     |              |              |
				 * |     |              X              |
				 * |     |              | v_x2         |
				 * |     |              |              |     ^
				 * |     o-------X------o------X-------o     U
				 * |     |        v_y1  |       v_y3   |     P
				 * |     |              |              |     |
				 * |     X       x_ij   X      x_ijp   X    ---
				 * |     | v_x1         | v_x3         |     |
				 * |     |              |              |     D
				 * |  i  o-------X------O------X-------o     O
				 * |     |        v_y2  |       v_y4   |     W
				 * |     |              |              |     N
				 * |     |              X              |     v
				 * |     |              | v_x4         |
				 * |     |              |              |
				 * |     o--------------o--------------o
				 * |
				 * |		< LEFT--|--RIGHT >
				 * |
				 * V Y/I
				 *
				 * We are located at the node that this designated as "O"
				 * With the coordinate (i,j). At that point the physical quantities,
				 * such as density and viscosity are defined.
				 *
				 * "X" markes the positions of velocity nodes.
				 *
				 * "x" is the location of the pressure node, despides its name,
				 * it is used to solve the continuity equation.
				 *
				 * For the Stokes equation in x direction we need a viscosity that
				 * is defined at the pressure point. We obtain that value by harmonic
				 * averaging the four surrounding viscosities (done by egd_pProperty() ).
				 *
				 * The the node (i,j) the nodes x_ij, v_x3 and v_y2 are associated.
				 *
				 * We are dealing with the happening at the middle point, v_x3.
				 * We will express anything from that point.
				 * In the global matrix indexing regime, this index is k_vx.
				 *
				 * The full discretization computation can be found in the paper documentation.
				 */

				//Compute the viscosity in the pressure points
				const Numeric_t ETA_ij  = ETA_CC(i, j    ); //ETA_p1
				const Numeric_t ETA_ijp = ETA_CC(i, j + 1); //ETA_p2

				//Compute the index, these are ithe index in the global indexing scheme
				const Index_t idx_Vx3 = k_vx;		//This is the primary index
				const Index_t idx_Vx2 = idx_Vx3 + UP;
				const Index_t idx_Vx4 = idx_Vx3 + DOWN;
				const Index_t idx_Vx5 = idx_Vx3 + RIGHT;
				const Index_t idx_Vx1 = idx_Vx3 + LEFT;
				const Index_t idx_Vy2 = k_vy;		//This is the primary index
				const Index_t idx_Vy1 = idx_Vy2 + UP;
				const Index_t idx_Vy4 = idx_Vy2 + RIGHT;
				const Index_t idx_Vy3 = idx_Vy4 + UP; // == idx_Vy2 + UP + RIGHT
					pgl_assert(this->hook_testIfBoundaryVX(i, j) == false);
					pgl_assert(idx_Vx1 >= 0, idx_Vx1 < m_L.cols(),
						   idx_Vx2 >= 0, idx_Vx2 < m_L.cols(),
						   idx_Vx3 >= 0, idx_Vx3 < m_L.cols(),
						   idx_Vx4 >= 0, idx_Vx4 < m_L.cols(),
						   idx_Vx5 >= 0, idx_Vx5 < m_L.cols() );
					pgl_assert(idx_Vy1 >= 0, idx_Vy1 < m_L.cols(),
						   idx_Vy2 >= 0, idx_Vy2 < m_L.cols(),
						   idx_Vy3 >= 0, idx_Vy3 < m_L.cols(),
						   idx_Vy4 >= 0, idx_Vy4 < m_L.cols() );


				/*
				 * 	d_x[\sigma_{xx}^{\prime}]
				 *
				 * We are the first one that writting in that equation.
				 */
				m_L.coeffRef(k_vx, idx_Vx3)	//Interation with Vx3	(itself)
					= -2.0 * ETA_ijp * iDeltaX2 - 2.0 * ETA_ij  * iDeltaX2;
				m_L.coeffRef(k_vx, idx_Vx1)	//Interation with Vx1
					=  2.0 * ETA_ij  * iDeltaX2;
				m_L.coeffRef(k_vx, idx_Vx5)	//Interaction with Vx5
					=  2.0 * ETA_ijp * iDeltaX2;


				/*
				 * 	d_y[\sigma_{xy}^{\prime}]
				 *
				 * This part is composed of two parts. First there is interation
				 * with the x velocities and then there is interation with the
				 * y velocities.
				 *
				 * It is also important, that now we could delete some coefficient
				 * in the matrix, we have already created/modified. After some
				 * thinking it is crear that this affects only one coefficient
				 * the interation with the node Vx3, all other coefficients
				 * (interations) are not yet touched, so they can/must safely
				 * overwritten.
				 */

				//we load the viscossity now they are in the basic nodal points
				const Numeric_t ETA_2 = ETA(i    , j); 	//This is the viscossity in the associated nodal point "O"
				const Numeric_t ETA_1 = ETA(i - 1, j);

				//
				//Handle the interactions with the x velocities
				m_L.coeffRef(k_vx, idx_Vx3)	//Interaction with Vx3; was already written to, so update it
					+= -ETA_2 * iDeltaY2 - ETA_1 * iDeltaY2;	//Only location here, where update is needed.
				m_L.coeffRef(k_vx, idx_Vx2)	//Interation with Vx2
					 =  ETA_1 * iDeltaY2;
				m_L.coeffRef(k_vx, idx_Vx4)	//Interaction with Vx4
					 =  ETA_2 * iDeltaY2;

				//
				//Handle the inderactions with the y velocities; No coefficient was yet written to
				m_L.coeffRef(k_vx, idx_Vy1)	//Interaction with Vy1
					 =   ETA_1  * iDeltaXY;
				m_L.coeffRef(k_vx, idx_Vy2)	//Interaction with Vy2; This is our node
					 = (-ETA_2) * iDeltaXY;
				m_L.coeffRef(k_vx, idx_Vy3)	//Interaction with Vy3
					 = (-ETA_1) * iDeltaXY;
				m_L.coeffRef(k_vx, idx_Vy4)	//Interaction with Vy4
					 =   ETA_2  * iDeltaXY;


				/*
				 * 	-d_x[p]		//Note the minus
				 *
				 * The pressure part is a simple derivation. That involves a finite
				 * difference stencil between the node "x_ijp", called p2 and "x_ij",
				 * which is called p1.
				 *
				 * We also apply a form of stabilization to it. This is to make the
				 * condition number smaller.
				 *
				 * The stencil is, with the minus already incooperated:
				 *
				 *  -d_x[p] = -\frac{p2 - p1}{\Delta x}
				 *     = -\frac{1}{\Delta x} p2 + \frac{1}{\Delta x} p1
				 */

				//
				//Index
				const Index_t idx_p1 = k_pm;	 	//This is the node that is associated to (i, j)
				const Index_t idx_p2 = idx_p1 + RIGHT;	//This is the one that is right (more in x dir)
					pgl_assert(idx_p1 >= 0, idx_p1 < m_L.cols(),
						   idx_p2 >= 0, idx_p2 < m_L.cols() );

				m_L.coeffRef(k_vx, idx_p1)	//the minus is incooperated into the stencil
					=  pScale * iDeltaX;
				m_L.coeffRef(k_vx, idx_p2)
					= -pScale * iDeltaX;


				/*
				 * The right hand side
				 */

				//We have nothing to set
				m_rhs[k_vx] = 0.0;

				//Source Term
				if(hasSourceTerm == true)
				{
					m_rhs[k_vx] += sourceVelX->operator()(i, j);
				}; //End if: handle source term
			//End if: Handleing inside the domain
			}
			else
			{
				pgl_assert(this->hook_testIfBoundaryVX(i, j));
			};

			/* END 		V X 		*/


			/* BEGIN	V Y		*/

			//Test if we are at the boundary
			if(this->hook_boundaryVY(			//Test and handle if we are on the boundary.
					i, j, k_vy,
					UP, DOWN, RIGHT, LEFT
				) == false)				//Testing for false makes that we enter if not on boundary.
			{
				/*
				 * We are an internal node.
				 *
				 * Here we make the same as we have done in the x equation. But this time we
				 * have also the graviational term. This will also involve a stabilization term.
				 *
				 *
				 * 0-----------------------------------------------------------------> X/J
				 * |                            v_y2    j
				 * |      o--------------o------X-------o--------------o
				 * |      |              |              |              |
				 * |      |              |              |              |
				 * |      |              X      x_ij    X              |
				 * |      |         v_x1 |              | v_x3         |
				 * |      |              |              |              |
				 * |    i o------X-------o------X-------O------X-------o
				 * |      |      v_y1    |      v_y3    |      v_y5    |
				 * |      |              |              |              |
				 * |      |         v_x2 X      x_ipj   X              |
				 * |      |              |              | v_x4         |
				 * |      |              |              |              |
				 * |      o--------------o------X-------o--------------o
				 * |                            v_y4
				 * V Y/I
				 *
				 * We consider the node "0" with teh coordinate (i,j).
				 * The associated quantities to that node are v_y3,
				 * which has the global index k_vy, v_x3 and x_ij,
				 * which is the pressure node.
				 * The unknown v_y3 will be determined by this equation.
				 * Similary to the equation for x, we need also the viscosity
				 * that is defined in the two pressure points.
				 * We will determine them the same way as we have done
				 * before.
				 */

				//
				//Determine the index
				const Index_t idx_Vy3 = k_vy;		//This is the basis index (center); we solve for that
				const Index_t idx_Vy2 = idx_Vy3 + UP;
				const Index_t idx_Vy4 = idx_Vy3 + DOWN;
				const Index_t idx_Vy1 = idx_Vy3 + LEFT;
				const Index_t idx_Vy5 = idx_Vy3 + RIGHT;
				const Index_t idx_Vx3 = k_vx;		//Basis index
				const Index_t idx_Vx1 = idx_Vx3 + LEFT;
				const Index_t idx_Vx2 = idx_Vx1 + DOWN;	// == idx_Vx3 + DOWN + LEFT
				const Index_t idx_Vx4 = idx_Vx3 + DOWN;
					pgl_assert(this->hook_testIfBoundaryVY(i, j) == false);
					pgl_assert(idx_Vy1 >= 0, idx_Vy1 < m_L.cols(),
						   idx_Vy2 >= 0, idx_Vy2 < m_L.cols(),
						   idx_Vy3 >= 0, idx_Vy3 < m_L.cols(),
						   idx_Vy4 >= 0, idx_Vy4 < m_L.cols(),
						   idx_Vy5 >= 0, idx_Vy5 < m_L.cols() );
					pgl_assert(idx_Vx1 >= 0, idx_Vx1 < m_L.cols(),
						   idx_Vx2 >= 0, idx_Vx2 < m_L.cols(),
						   idx_Vx3 >= 0, idx_Vx3 < m_L.cols(),
						   idx_Vx4 >= 0, idx_Vx4 < m_L.cols() );

				//Compuet the viscosity at the grid points
				const Numeric_t ETA_p1 = ETA_CC(i    , j);	//This is at "x_ij"
				const Numeric_t ETA_p2 = ETA_CC(i + 1, j);  //This is at "x_ipj"


				/*
				 * 	d_y[\sigma_{yy}^{\prime}]
				 *
				 * Here we are sure that nowbody has written to the coefficient yet.
				 * And we can safly assigne them.
				 * Note that here we have only interaction in y!
				 */
				m_L.coeffRef(k_vy, idx_Vy3)	//Interaction with Vy3 (itself)
					= -2.0 * ETA_p2 * iDeltaY2 - 2.0 * ETA_p1 * iDeltaY2;
				m_L.coeffRef(k_vy, idx_Vy2)	//Interaction with Vy2
					=  2.0 * ETA_p1 * iDeltaY2;
				m_L.coeffRef(k_vy, idx_Vy4)	//Interaction with Vy4
					=  2.0 * ETA_p2 * iDeltaY2;



				/*
				 * 	d_x[\sigma_{yx}^{\prime}]
				 *
				 * We have here two parts, interation with Vy and interaction
				 * with Vx, we will handle them seperatly.
				 *
				 * When the stencil is examinated, we see that we have only written
				 * to one index before, so we must update it and not override it.
				 */

				//Load the viscosities this time from the basic nodal point
				const Numeric_t ETA_1 = ETA(i, j - 1);
				const Numeric_t ETA_2 = ETA(i, j    );

				//Handle the y interaction
				m_L.coeffRef(k_vy, idx_Vy3)	//Interaction with Vy3 (itself); Only one to update
					+= -ETA_2 * iDeltaX2 - ETA_1 * iDeltaX2;
				m_L.coeffRef(k_vy, idx_Vy1)	//Interaction with Vy1
					 =  ETA_1 * iDeltaX2;
				m_L.coeffRef(k_vy, idx_Vy5)	//Interaction with Vy5
					 =  ETA_2 * iDeltaX2;

				//Handle the x interaction
				m_L.coeffRef(k_vy, idx_Vx1)	//Interaction with Vx1
					 =  ETA_1 * iDeltaXY;
				m_L.coeffRef(k_vy, idx_Vx2)	//Interaction with Vx2
					 = -ETA_1 * iDeltaXY;
				m_L.coeffRef(k_vy, idx_Vx3)	//Interaction with Vx3
					 = -ETA_2 * iDeltaXY;
				m_L.coeffRef(k_vy, idx_Vx4)	//Interaction with Vx4
					 =  ETA_2 * iDeltaXY;



				/*
				 * 	-d_y[p]		//Note the minus
				 *
				 * Handle the pressure term, we will also merge the minus
				 * into the stencil, see x part for more information.
				 */

				const Index_t idx_p1 = k_pm;		//This is the pressure node that is assoicated to (i,j).
				const Index_t idx_p2 = idx_p1 + DOWN;	//This is the node below the (i,j) node.
					pgl_assert(idx_p1 >= 0, idx_p1 < m_L.cols(),
						   idx_p2 >= 0, idx_p2 < m_L.cols() );

				m_L.coeffRef(k_vy, idx_p1)		//Minus sign is incooperated into the stencil
					=  pScale * iDeltaY;
				m_L.coeffRef(k_vy, idx_p2)
					= -pScale * iDeltaY;


				/*
				 * Stabilization / Density / Gravity Term
				 *
				 * Main goal is to make the density depending on the new
				 * configuration, this will stabilize the process.
				 * In the following we are sitting in the Vy3 node.
				 * We assume that we can write the density as:
				 * 	\rho_{dt} := \rho_{0} + dt \cdot \d{\rho}/\d{t}
				 *
				 * By continuity we can write
				 * 	\d{\rho}/\d{t} = -\grad{\rho} \cdot \vec{v}
				 * 		= u_x \cdot \d{\rho}/\d{x} + u_y \cdot \d{\rho}/\d{y}
				 *
				 * \dt is the time step and \rho_{0} is the current density configuration,
				 * which is knwon and constant.
				 *
				 * Only the \rho_{0} term is known, the rest will contains unknown variables.
				 * And are on the left side of the great equation.
				 */
				if(useDensStab == true)
				{

					/*
					 * Handling of:
					 * 	u_x \cdot \d{\rho}/\d{x}
					 *
					 * For the derivative of the denity we can simply use the
					 * two densities that lies left and right of the Vy3 point.
					 * u_x is not explicit aviable, so we will use the average
					 * of the four surounding x velocities.
					 */
					{
						//This is the derivative of the density in x direction
						const Numeric_t dRho_x = (RHO(i, j) - RHO(i, j - 1)) * iDeltaX;

						//This is the coefficient that is used it will modify
						//the four surrounding x velocities. Thus an update is needed
						//The 1/4 is used for the average.
						//The minus is because we must bring it to the other side.
						const Numeric_t coeff  = -rhoStabTime * dRho_x * 0.25 * gravY;

						//Updating the coefficients
						m_L.coeffRef(k_vy, idx_Vx1) += coeff;	//Updating Vx1
						m_L.coeffRef(k_vy, idx_Vx2) += coeff;	//Updating Vx2
						m_L.coeffRef(k_vy, idx_Vx3) += coeff;	//Updating Vx3
						m_L.coeffRef(k_vy, idx_Vx4) += coeff;	//Updating Vx4
					}; //End: Handling of x part of stabilization


					/*
					 * Handling of:
					 * 	-v_y * d{Rho}/d{y} * dt
					 *
					 * The new designe of the solver, where we have the density
					 * at the cell centre points, allows us avoid the crude
					 * approximation we had done before to get d{Rho}/d{y}.
					 */
					{
						//Density above the VY node
						const Numeric_t Rho_1 = RHO_CC(i    , j);

						//Density below the VY node
						const Numeric_t Rho_2 = RHO_CC(i + 1, j);

						//CAlculate the derivative
						const Numeric_t dRho_y = (Rho_2 - Rho_1) * iDeltaY;

						//Now we update the coeficient in the matrix. We have
						//only the velocity that is unknown which is the central
						//one and already manipulated, so an update is needed.
						//The minus is because we have to bring this to the other side
						m_L.coeffRef(k_vy, idx_Vy3) += -rhoStabTime * dRho_y * gravY;
					}; //End: Handling of y part of stabilization.
				}; //End if: use density stabilization.


				/*
				 * RHS
				 *
				 * Now we handle the rhs. The only contribution of this is
				 * The gravity times the current density at the Vy3 node.
				 * The new solver implementation provides us with the density
				 * at that location.
				 *
				 * Note that this is not part of the density stabilitazion,
				 * this is always there.
				 */
				{
					//Compte an approximation of the density
					const Numeric_t rho_0 = RHO_VY(i, j);

					m_rhs[k_vy] = -gravY * rho_0;	//TODO: Why a minus?
				}; //End: Modifing the RHS

				//
				//Sourceterm
				if(hasSourceTerm == true)
				{
					m_rhs[k_vy] += sourceVelY->operator()(i, j);
				}; //End if: has sourceterm
			//End if: Internal handling
			}
			else
			{
				pgl_assert(this->hook_testIfBoundaryVY(i, j));
			};
			/* END		V Y		*/


			/* BEGIN	P 		*/

			/*
			 * In the velocities part we had a row or a column that contained ghost nodes.
			 * The pressure part has both the first column (right one) and the top row
			 * are ghost nodes.
			 *
			 * Also the boundary is only composed of the four corner nodes and one additional node.
			 * Each node inforces that its value is the same that its right and left,
			 * depending which one exists, neghobour.
			 * The last one, which is the neighbour of the top left corner, is forced to have a value.
			 *
			 * TODO: Ask Taras about them.
			 */
			if(this->hook_boundaryP(			//Test and handle the boundary.
					i, j, k_pm,
					UP, DOWN, RIGHT, LEFT,
					pScale
				) == false)				//testing for false result in only entering in inner
			{
				/*
				 * We are inside the domain.
				 * We will here detrmine the pressure value.
				 *
				 *    0----------------------------------------------------------> X/J
				 *    |                                 i
				 *    |       o------------X------------o
				 *    |       |            v_x1         |
				 *    |       |                         |
				 *    |       |                         |
				 *    |       |                         |
				 *    |       X            P            X
				 *    |       | v_x1                    | v_x2
				 *    |       |                         |
				 *    |       |                         |
				 *    |       |                         |
				 *    |     j o------------X------------O
				 *    |                    v_y2
				 *    |
				 *    V Y/I
				 *
				 * We are at node (i,j), the assoicated properties are
				 * "P" (Presure), "v_x2" and "v_y2".
				 *
				 * We solve the continuity equation on the pressure node,
				 * which has the funny effect that the pressure is not
				 * used here.
				 *
				 * In incompressible form the equation reads as
				 * 	d_x[Vx] + d_y[Vy] == 0
				 */

				const Index_t idx_Vx2 = k_vx;
				const Index_t idx_Vy2 = k_vy;
				const Index_t idx_Vx1 = idx_Vx2 + LEFT;
				const Index_t idx_Vy1 = idx_Vy2 + UP;
					pgl_assert(this->hook_testIfBoundaryP(i, j) == false);
					pgl_assert(idx_Vx1 >= 0, idx_Vx1 < m_L.cols(),
						   idx_Vx2 >= 0, idx_Vx2 < m_L.cols() );
					pgl_assert(idx_Vy1 >= 0, idx_Vy1 < m_L.cols(),
						   idx_Vy2 >= 0, idx_Vy2 < m_L.cols() );

				/*
				 * 	d_x[Vx]
				 *
				 *   ~> \frac{Vx2 - Vx1}{\Delta x}
				 *   ~> \frac{1}{\Delta x} Vx2 -\frac{1}{\Delta x} Vx1
				 */
				m_L.coeffRef(k_pm, idx_Vx2)	//Interaction with Vx2
					=  iDeltaX;	        // == 1.0 / DeltaX
				m_L.coeffRef(k_pm, idx_Vx1)	//interaction with Vx1
					= -iDeltaX;

				/*
				 * 	d_y[Vy]
				 *
				 * This is handled similary to the x part.
				 */
				m_L.coeffRef(k_pm, idx_Vy2)	//Interaction with Vy2
					=  iDeltaY;		// == 1.0 / DeltaY
				m_L.coeffRef(k_pm, idx_Vy1)	//interaction with Vy1
					= -iDeltaY;

				/*
				 * 	RHS
				 *
				 * The summ of all these must be zero
				 */
				m_rhs[k_pm] = 0.0;

				//Handle the source term
				if(hasSourceTerm == true)
				{
					m_rhs[k_pm] += sourceP->operator()(i, j);
				};//End if: source
			//End if: inside domain
			}
			else
			{
				pgl_assert(this->hook_testIfBoundaryP(i, j));
			};
			/* END		P 		*/

		}; //End for(i): handling the y points
	}; //End for(j): handling the x points


	/*
	 * The checking will not be done here, but latter.
	 * After the hook has run.
	 *
	 * We will also not call the compression function yet.
	 * This will be done after the hook too.
	 */

	return;
}; //End: build system matrix

PGL_NS_END(egd)


