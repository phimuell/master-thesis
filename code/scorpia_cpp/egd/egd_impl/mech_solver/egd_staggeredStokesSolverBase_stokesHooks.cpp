/**
 * \brief	This file contains the hookes that are needed for the stokes solver and are not related
 * 		 to boundary condition.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>
#include <unsupported/Eigen/src/IterativeSolvers/Scaling.h>


//Include STD
#include <memory>

#if defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0)
#	pragma message "The pressure scaling in the staggered stokes base solver is disabled."
#else
#	pragma message "The pressure scaling in the staggered stokes base solver is enabled."
#endif



PGL_NS_START(egd)

egd_staggeredStokesSolverBase_t::Numeric_t
egd_staggeredStokesSolverBase_t::hook_getPScale(
	const GridContainer_t& 		grid)
 const
{
#if defined(EGD_NO_PRESSURE_SCALING) && (EGD_NO_PRESSURE_SCALING != 0)
	return 1.0;
	(void)grid;
#else
		pgl_assert(grid.getGeometry().isConstantGrid());
	return ((1e+21) / grid.getGeometry().getXSpacing());
#endif
};


PGL_NS_END(egd)


