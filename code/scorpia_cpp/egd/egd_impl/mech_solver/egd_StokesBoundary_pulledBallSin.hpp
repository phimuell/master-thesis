#pragma once
/**
 * \brief	This class implements a boundary condition for the pulled ball.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include "./egd_staggeredStokesSolverBase.hpp"
#include "./egd_StokesBoundary_pipeFlow.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_map.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>




PGL_NS_START(egd)

/**
 * \define	This macro controles how the pressure problem is resolved.
 * \define 	EGD_PULLEDBALL_AVG_PRESS
 *
 * If this macro is set to a non zero value, the default, the boundary condition,
 * will determine the pressure value in the ball's centre by averaging the four
 * sorrounding nodes.
 * If set to zero, the offset point for pressure will be moved to that location.
 *
 */
#if defined(YCM) && YCM != 0
#	define EGD_PULLEDBALL_AVG_PRESS 1
#else
#	if !defined(EGD_PULLEDBALL_AVG_PRESS)
#		define EGD_PULLEDBALL_AVG_PRESS 1
#	endif
#endif



/**
 * \brief	This class implements the pulled ball boundary conditons.
 * \class 	egd_stokesPulledBallSinBoundary_t
 *
 * This class is a normal pipe boundary implementation that has a certain internal
 * velocity condition. It will prescribe the cell in which the centre of the ball
 * is located, with the analytical value. The solid ball should not deforme and the
 * condition should propagate, to all locations that are ball, at least I hope that.
 *
 * There are two modes that are supported. Each basic nodal cell has four velocity
 * nodes associated to it, two x components, at the left and right cell interface,
 * and two for y, located at the top and bottom cell interface.
 * In the first mode, which is also the default, only two nodes, one per components,
 * are prescribed, ve prescribed the right and the bottom one.
 * In the second mode, we prescribe all four. However this leads to a problem, because
 * the system becomes singular, because it is impossible to determine the pressure.
 * We would like to point out, that continuuity is guaranteed in the cell.
 * To solve these two possibilities are supported. It is only possible to select them
 * in the compiliation phase. The default apprache is to define the pressure in the cell
 * as the average value of the four sourounding nodes. In the second method, is different.
 * Since we only compute the pressure gradiant, we need an offset, this offset is usually
 * imposed at the (1, 1) node, but in this mode, the node is changed and now lies in the
 * cell.
 * How this is resolved is controled with the macro EGD_PULLEDBALL_AVG_PRESS.
 *
 * \tparam  BaseImpl_t 		This is the base implementation.
 * \tparam  isExtended		This is a bool that indicates if the grid is extended or not.
 *
 */
template<
	class 	ImplBase_T,
	bool 	isExtended
>
class egd_stokesPulledBallSinBoundary_t :
#if defined(YCM) && YCM != 0
	public egd_stokesPipeBoundary_t<egd_staggeredStokesSolverBase_t, true>	//exposition only
#else
	public egd_stokesPipeBoundary_t<ImplBase_T, isExtended>
#endif
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Base_t = egd_stokesPipeBoundary_t<ImplBase_T, isExtended>;	//!< This is the base class that is used.

	using typename Base_t::Size_t;
	using typename Base_t::uSize_t;
	using typename Base_t::Index_t;
	using typename Base_t::Numeric_t;

	using typename Base_t::Matrix_t;
	using typename Base_t::InterfaceBase_t;

	using typename Base_t::SolverResult_t;
	using typename Base_t::SolverResult_ptr;

	using typename Base_t::SolverArg_t;
	using typename Base_t::SolverArg_ptr;
	using genSolverArg_t = egd_generalSolverArg_i;

	using typename Base_t::GridContainer_t;
	using typename Base_t::GridGeometry_t;
	using typename Base_t::GridPosition_cref;
	using typename Base_t::GridProperty_cref;
	using typename Base_t::SystemMatrix_t;
	using typename Base_t::Vectot_t;

	using typename Base_t::BoundaryCondition_t;
	using SingleCondition_t = typename BoundaryCondition_t::SideCondition_t;
	using typename Base_t::BCMap_t;


	/*
	 * =======================
	 * Static asserts
	 */
	static_assert(std::is_unsigned<uSize_t>::value, "uSize must be unsigned");



	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is a building constructor that
	 * 		 is requiered by the factory system.
	 *
	 * This constructor extracts the necessary variables
	 * from the configuration object. If thsi constructor
	 * is used it is requiered that the alpha and omega
	 * values are present.
	 *
	 * \param  confObj 	The configuration object that
	 * 			 is beeing used.
	 */
	egd_stokesPulledBallSinBoundary_t(
		const egd_confObj_t& 		confObj);


	/**
	 * \brief	This constreuctor is used directly construct.
	 *
	 * The value for alpha and omega are passed by this constructor.
	 * Note that it is allowd that they are NAN, in which case they
	 * should not be used.
	 * This constructor is ment to be called by base objects.
	 *
	 * \param   alpha	Alpha parameter
	 * \param   omega 	Omega Parameter
	 * \param   confObj	The configuration object.
	 */
	egd_stokesPulledBallSinBoundary_t(
		const Numeric_t 		alpha,
		const Numeric_t 		omega,
		const egd_confObj_t& 	 	confObj);



	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_stokesPulledBallSinBoundary_t(
		const egd_stokesPulledBallSinBoundary_t&)
	 = delete;


	/**
	 * \brief	Copy Assignement.
	 *
	 * Is deleted.
	 */
	egd_stokesPulledBallSinBoundary_t&
	operator= (
		const egd_stokesPulledBallSinBoundary_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_stokesPulledBallSinBoundary_t(
		egd_stokesPulledBallSinBoundary_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_stokesPulledBallSinBoundary_t&
	operator= (
		egd_stokesPulledBallSinBoundary_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_stokesPulledBallSinBoundary_t()
	 = default;


	/*
	 * =======================
	 * Private Constructor
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted used only for internal purpuse.
	 */
	egd_stokesPulledBallSinBoundary_t()
	 = default;





	/*
	 * ======================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 */
	virtual
	std::string
	print()
	 const
	 override
	{
		return (this->hook_getImplName() + " pulled ball boundary"
			+ " Ny = " + std::to_string(this->getNy() ) + ";"
			+ " Nx = " + std::to_string(this->getNx() ) + ";"
			+ " Vel_x right = " + std::to_string(this->get_velXRight()  ) + ";"
			+ " Vel_x left  = " + std::to_string(this->get_velXLeft()   ) + ";"
			+ " Vel_y top   = " + std::to_string(this->get_velYTop()    ) + ";"
			+ " Vel_y bott  = " + std::to_string(this->get_velYBot()    ) + ";"
			+ " Press       = " + std::to_string(this->get_pressOffset()) + ";"
			+ " alpha       = " + std::to_string(this->m_alpha          ) + ";"
			+ " omega       = " + std::to_string(this->m_omega          ) + ";"
			+ " x0          = " + std::to_string(this->m_x0             ) + ";"
			+ " y0          = " + std::to_string(this->m_y0             ) + ";"
			+ " fullCell    = " + (this->m_fullCell ? std::string("Yes") : std::string("No"))
#			if defined(EGD_PULLEDBALL_AVG_PRESS) && (EGD_PULLEDBALL_AVG_PRESS != 0)
			+ " (with pressure average);"
#			else
			+ " (moved pressure offset point);"
#			endif
		);
	}; //End: Print


	/*
	 * ===================================
	 * General Hooks
	 *
	 * These are the hooks that are defined/requiered by
	 * the genral implementation class. Some are implemented
	 * by *this, some are redirected to the base and some are
	 * left abstract.
	 */
protected:
	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not. See super class.
	 *
	 * True indicates that *this is an extextended grid.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 override
	{
		return isExtended;
	};


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping. See super class.
	 *
	 * This function is not implemented.
	 *
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	{
		return (isExtended
			? std::string("extendedPulledSinStokes")
			: std::string("regularPulledSinStokes") );
	};





	/*
	 * ===============================
	 * Stokes Hooks
	 *
	 * These are hooks that are needed because *this is a stokes hook.
	 * These fucntions are primarly needed by the composing function.
	 *
	 * There are also a thrid categories of hooks, these hooks deals with
	 * the handling of the boundary condition on a more fine grained scale.
	 */
protected:
	/**
	 * \brief	This function is used to initialize the boundary for a step.
	 *
	 * This hook will setup all the internal state, such that it can be used for
	 * the construction of the system.
	 * In the first iteration, all internal values are set up, then no changes
	 * are accepted.
	 *
	 * \param  gridCont		The grid container.
	 * \param  solArg		The olver argument.
	 */
	virtual
	void
	hook_initBoundaries(
		const GridContainer_t& 		gridCont,
		genSolverArg_t* const 		solArg)
	 override;


	/*
	 * ============================
	 * Pulled Ball Hooks
	 *
	 * These are hooks that are used/needed for the
	 * computation of teh boundary.
	 */
protected:
	/**
	 * \brief	This function returns the x coordinate of the ball at the given time.
	 *
	 * \param  t 		The given time.
	 */
	virtual
	::pgl::Numeric_t
	hook_computeXCoM(
		const Numeric_t 	t)
	 const;


	/**
	 * \brief	This function returns the y coordinate of the ball at the given time.
	 *
	 * \param  t 		The given time.
	 */
	virtual
	::pgl::Numeric_t
	hook_computeYCoM(
		const Numeric_t 	t)
	 const;


	/**
	 * \brief	This function is used to set the centre of the ball at the beginning.
	 *
	 * This function accpets "y0" and "x0" and interpeets it as the coordinate
	 * of the ball centre at the beginning.
	 *
	 * \param  key		How to identiy.
	 * \param  value	The position.
	 *
	 */
	virtual
	bool
	setBoundaryOption(
		const std::string& 		key,
		const Numeric_t 		value)
	 override;


	/**
	 * \brief	This function is used to compute the velocity of the centre
	 */
	virtual
	void
	hook_computeBallVel(
		const Numeric_t 		t,
		Numeric_t* const 		VX,
		Numeric_t* const 		VY)
	 const;



	/*
	 * ==============================
	 * BC Hooks
	 *
	 * This function will first call the version of teh pipe
	 * boiundary, if they are not active it is checked if the
	 * centre of the ball is inside the cell. If so, they will
	 * impose a condition on them.
	 */
protected:
	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VX property.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vx 	The global index of the node.
	 * \param  UP		Go to the upper VX node.
	 * \param  DOWN		Go to the lower VX node.
	 * \param  RIGHT	Go to the right VX node.
	 * \param  LEFT		Go to the left VX node.
	 */
	virtual
	bool
	hook_boundaryVX(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vx,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VX grid.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVX(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VY property.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vy 	The global index of the node.
	 * \param  UP		Go to the upper VY node.
	 * \param  DOWN		Go to the lower VY node.
	 * \param  RIGHT	Go to the right VY node.
	 * \param  LEFT		Go to the left VY node.
	 */
	virtual
	bool
	hook_boundaryVY(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vy,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VY grid.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVY(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the P property.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_p 		The global index of the node.
	 * \param  UP		Go to the upper P node.
	 * \param  DOWN		Go to the lower P node.
	 * \param  RIGHT	Go to the right P node.
	 * \param  LEFT		Go to the left P node.
	 * \param  pScale	This is the p scale value.
	 */
	virtual
	bool
	hook_boundaryP(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_p,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT,
		const Numeric_t 	pScale)
	 override;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the P grid.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryP(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 override;


	/*
	 * ================
	 * Variable access
	 */
protected:
#	define IMPORT(x) using Base_t:: get_ ## x
	IMPORT(velXLeft );
	IMPORT(velXRight);
	IMPORT(velYBot  );
	IMPORT(velYTop  );
	IMPORT(pressOffset);
#	undef IMPORT

	/**
	 * \brief	Returns the value of t0.
	 */
	Numeric_t
	getT0()
	 const
	{
			pgl_assert(::pgl::isValidFloat(this->m_t0));
		return this->m_t0;
	};


	/**
	 * \brief	Return the x0 value.
	 */
	Numeric_t
	getX0()
	 const
	{
			pgl_assert(::pgl::isValidFloat(this->m_x0));
		return this->m_x0;
	};


	/**
	 * \brief	Returns the y0 value.
	 */
	Numeric_t
	getY0()
	 const
	{
			pgl_assert(::pgl::isValidFloat(this->m_y0));
		return this->m_y0;
	};


	/**
	 * \brief	This function returns true if full cell mode is used.
	 */
	bool
	useFullCellMode()
	 const
	{
		return this->m_fullCell;
	};




	/*
	 * ========================
	 * Private Members
	 *
	 * This class stores the boundary conditions.
	 */
private:
	//this are more like the parameters.
	Numeric_t 	m_alpha = NAN;			//!< Value of alpha parameter, is positive.
	Numeric_t  	m_omega = NAN;			//!< Value of the omega parameter.
	Numeric_t 	m_t0 = NAN;			//!< Beginning of the calculation.
	Numeric_t  	m_x0 = NAN;			//!< x CoM coordinate of the ball at the beginning.
	Numeric_t 	m_y0 = NAN;			//!< y CoM coordinate of the ball at the beginning.
	Numeric_t 	m_dx = NAN;			//!< Grid spacing in x direction.
	Numeric_t 	m_dy = NAN;			//!< Grid spacing in y direction.

	//this are some working data
	Index_t 	m_bcI;				//!< I coordinate of the ball.
	Index_t 	m_bcJ;				//!< J cooedinate of the ball.
	Numeric_t 	m_bcVX = NAN;			//!< Velocity of the centre in x direction.
	Numeric_t 	m_bcVY = NAN;			//!< Velocity of the centre in y direction.

	//Logic controll
	bool 		m_fullCell = false;		//!< This falg controles if all four values are prescribed or just two.
}; //End: class(egd_stokesPulledBallSinBoundary_t)

PGL_NS_END(egd)


#if defined(YCM) && YCM != 0
#else
//If YCM is not parsing this, then include the implementation
#include "./egd_StokesBoundary_pulledBallSin_impl.cpp"
#endif



