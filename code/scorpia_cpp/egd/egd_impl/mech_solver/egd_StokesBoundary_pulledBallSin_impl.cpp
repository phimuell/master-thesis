/**
 * \brief	This file is contains the implementation of the pulled ball pipe flow boundary.
 *
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include <egd_interfaces/egd_generalSolverArgument.hpp>

//Only include the header if YCM parses it
#if defined(YCM) && YCM != 0
#include "./egd_StokesBoundary_pulledBallSin.hpp"
#else
#endif

#if defined(EGD_PULLEDBALL_AVG_PRESS) && (EGD_PULLEDBALL_AVG_PRESS != 0)
#	pragma message "Pressure is determined by average."
#else
#	pragma message "Pressure offset point is relocated."
#endif


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>


/*
 * This message is not needed, since it is fully handled
 * in the pipe constructor. Hower since it will use it, we will keep the printing.
 */
#if defined(EGD_IGNORE_NEG_PRESSURE) && (EGD_IGNORE_NEG_PRESSURE != 0)
#	pragma message "The pulled ball sine boundary condition will accept negative prerssure offsets through its explicit constructor."
#endif



PGL_NS_START(egd)

using ::pgl::isValidFloat;

template<
	class 		ImplBase_T,
	bool 		isExtended
>
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::egd_stokesPulledBallSinBoundary_t(
	const Numeric_t 		alpha,
	const Numeric_t 		omega,
	const egd_confObj_t&	 	confObj)
 :
  Base_t(confObj)
{
		pgl_assert(isValidFloat(alpha) == isValidFloat(omega));		//both are valid or invalid.

	//We have to set up the two parameters only if they are valid
	//otherwhise they are NAN
	if(isValidFloat(alpha) == true)
	{
		this->m_alpha = alpha;		pgl_assert(this->m_alpha > 0.0);
		this->m_omega =	omega;		pgl_assert(this->m_omega > 0.0);
	};

	/*
	 * Read in the mode
	 */
	this->m_fullCell = confObj.solverUseFullCell();
}; //End: building constructor with config object


template<
	class 		ImplBase_T,
	bool 		isExtended
>
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::egd_stokesPulledBallSinBoundary_t(
	const egd_confObj_t& 		confObj)
 :
  egd_stokesPulledBallSinBoundary_t(
  		confObj.setUpPar("alpha"),	//requiere alpha parameter
  		confObj.setUpPar("omega"),
  		confObj                 )
{
	/*
	 * Read in the mode
	 */
	this->m_fullCell = confObj.solverUseFullCell();
}; //End: building constructor with config object


/*
 * Boundary Controll
 */
template<
	class 		ImplBase_T,
	bool 		isExtended
>
bool
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::setBoundaryOption(
	const std::string& 		key,
	const Numeric_t 		value)
{
	//Allow the base to handle it
	this->Base_t::setBoundaryOption(key, value);

	//check if we are the one
	if(key == "x0" || key == "X0")
	{
			pgl_assert(isValidFloat(value));
		this->m_x0 = value;
		return true;
	}
	if(key == "y0" || key == "Y0")
	{
			pgl_assert(isValidFloat(value));
		this->m_y0 = value;
		return true;
	}

	return false;
}; //End


template<
	class 		ImplBase_T,
	bool 		isExtended
>
void
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_initBoundaries(
		const GridContainer_t& 		gridCont,
		genSolverArg_t* const 		solArg)
{
	pgl_assert(solArg != nullptr);

	//test if it is the first time
	if(isValidFloat(this->m_t0) == false)
	{
		//Now we have to set up
		this->m_t0 = solArg->getCurrentTime();		pgl_assert(isValidFloat(this->m_t0));

		//get the geometry
		const GridGeometry_t& gridGeo = gridCont.getGeometry();

		//set up the grid spacing
		this->m_dx = gridGeo.getXBasicPos()[1] - gridGeo.getXBasicPos()[0];	pgl_assert(isValidFloat(this->m_dx), this->m_dx > 0.0);
		this->m_dy = gridGeo.getYBasicPos()[1] - gridGeo.getYBasicPos()[0];	pgl_assert(isValidFloat(this->m_dy), this->m_dy > 0.0);
	}; //End if: it is the first time
		//No constraints on alpha and omega, since they may not be used here
		pgl_assert(isValidFloat(this->m_x0),
			   isValidFloat(this->m_y0),
			   isValidFloat(this->m_dx), this->m_dx > 0.0,
			   isValidFloat(this->m_dy), this->m_dy > 0.0 );

	//load the current time
	const Numeric_t t = solArg->getCurrentTime();		pgl_assert(isValidFloat(t), t >= this->m_t0);


	//Get the start of the domain
	const Numeric_t xDomStart = gridCont.getGeometry().xDomainStart();
	const Numeric_t yDomStart = gridCont.getGeometry().yDomainStart();
	const Numeric_t xDomEnd   = gridCont.getGeometry().xDomainEnd();
	const Numeric_t yDomEnd   = gridCont.getGeometry().yDomainEnd();

	//now we compute the location at which the centre is located
	const Numeric_t Xt = this->hook_computeXCoM(t);		pgl_assert(xDomStart <= Xt, Xt <= xDomEnd);
	const Numeric_t Yt = this->hook_computeYCoM(t);		pgl_assert(yDomStart <= Yt, Yt <= yDomEnd);

	//bnow compute the fractional index
	const Numeric_t X_f = (Xt - xDomStart) / (this->m_dx);
	const Numeric_t Y_f = (Yt - yDomStart) / (this->m_dy);

	//Cast it to an int to find the associated node
	this->m_bcI = Size_t(Y_f) + 1;		//is for convenience
	this->m_bcJ = Size_t(X_f) + 1;

	//Compute the velocity
	this->hook_computeBallVel(t, &(this->m_bcVX), &(this->m_bcVY));
		pgl_assert(isValidFloat(this->m_bcVX),
			   isValidFloat(this->m_bcVY) );

	return;
}; //End: init the boundaries


template<
	class 		ImplBase_T,
	bool 		isExtended
>
void
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_computeBallVel(
	const Numeric_t 		t,
	Numeric_t* const 		VX,
	Numeric_t* const 		VY)
 const
{
using std::sin;
		pgl_assert(isValidFloat(t),
			   isValidFloat(this->m_t0), t >= this->m_t0,
			   isValidFloat(this->m_alpha), this->m_alpha > 0.0,
			   isValidFloat(this->m_omega), this->m_omega > 0.0,
			   isValidFloat(this->m_x0),
			   isValidFloat(this->m_y0),
			   VX != nullptr,
			   VY != nullptr );
	*VY = 0.0;
	*VX = -(this->m_alpha) * (
			sin(((2.0 * M_PI) / (this->m_omega)) * (t - (this->m_t0))) + 1.0
			);

	return;
}; //End: compute velocity


template<
	class 		ImplBase_T,
	bool 		isExtended
>
::pgl::Numeric_t
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_computeXCoM(
	const Numeric_t 		t)
 const
{
using std::cos;
		pgl_assert(isValidFloat(t),
			   isValidFloat(this->m_t0), t >= this->m_t0,
			   isValidFloat(this->m_alpha), this->m_alpha > 0.0,
			   isValidFloat(this->m_omega), this->m_omega > 0.0,
			   isValidFloat(this->m_x0),
			   isValidFloat(this->m_y0) );

	const Numeric_t zPi = 2.0 * M_PI;
	const Numeric_t tmp = (((-(this->m_alpha)) * ((this->m_omega) + zPi * t - zPi * (this->m_t0)) + zPi * (this->m_x0) - (-(this->m_alpha)) * (this->m_omega) * cos((zPi / (this->m_omega)) * (t - (this->m_t0)))) / zPi);
	return tmp;
};


template<
	class 		ImplBase_T,
	bool 		isExtended
>
::pgl::Numeric_t
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_computeYCoM(
	const Numeric_t 		t)
 const
{
		pgl_assert(isValidFloat(t),
			   isValidFloat(this->m_t0), t >= this->m_t0,
			   isValidFloat(this->m_alpha), this->m_alpha > 0.0,
			   isValidFloat(this->m_omega), this->m_omega > 0.0,
			   isValidFloat(this->m_x0),
			   isValidFloat(this->m_y0) );

	return (this->m_y0);	//no movement in y, so no change.
};


/*
 * ==================================
 * Setting the boundaries
 */

template<
	class 		ImplBase_T,
	bool 		isExtended
>
bool
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_boundaryVX(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vx,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
{
	/*
	 * We will first check for the external boundary.
	 * This boundary is handled by the base class object,
	 * which is a pipe object.
	 * This will also handle the ghost nodes.
	 */
	if(this->Base_t::hook_boundaryVX(i, j, k_vx, UP, DOWN, RIGHT, LEFT) == true)
	{
		/* If we are here then the pipe base has handled the node already.
		 * So we will now return true.  */
		return true;
	}; //End if: pipe handled the node.

	/*
	 * Now we have to check if it is the ball centre
	 */
	if(((i == this->m_bcI)                                                       ) &&
	   ((j == this->m_bcJ) || (this->m_fullCell ? ((j+1) == this->m_bcJ) : false))   )
	{
		this->m_L.coeffRef(k_vx, k_vx) = 1.0;	//setting the condition, such that a dirichlet condition happes.

		this->m_rhs[k_vx] = this->m_bcVX;

		//return true to indicate that we have handled the element.
		return true;
	};//end if: inside boundary

	/*
	 * We are here, this means that neider the pipe boundary
	 * nor the internal boundary is active, so we have to
	 * return false to indicate this
	 */
	return false;
	pgl_onlyForDebugging(Nx, thisGrid, nRows, nCols);
}; //End: handle VX


template<
	class 		ImplBase_T,
	bool 		isExtended
>
bool
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_testIfBoundaryVX(
		const Index_t 		i,
		const Index_t 		j)
 const
{
	/*
	 * Test if we are under the influence of the pipe boundary.
	 */
	if(this->Base_t::hook_testIfBoundaryVX(i, j) == true)
	{
		/* We are on a pipe boundary point, so we have to return true */
		return true;
	}; //End if: test on pipe

	/*
	 * Now we have to check if it is the ball centre
	 */
	if(((i == this->m_bcI)                                                       ) &&
	   ((j == this->m_bcJ) || (this->m_fullCell ? ((j+1) == this->m_bcJ) : false))   )
	{
		return true;
	};//end if: inside boundary

	/*
	 * We are not on an internal boundary too. so we return false
	 * to indicae this
	 */
	return false;
}; //End: handle VX


template<
	class 		ImplBase_T,
	bool 		isExtended
>
bool
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_boundaryVY(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vy,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
{
	/*
	 * First we will test if the pipe boundary is active
	 * This is done by calling the function of the base class.
	 */
	if(this->Base_t::hook_boundaryVY(i, j, k_vy, UP, DOWN, RIGHT, LEFT) == true)
	{
		/* We are on the pipe boundary, so it was handled already. */
		return true;
	}; //end if: test if pipe


	/*
	 * Check if we are inside the cell which contains the ball centre
	 */
	if(((i == this->m_bcI) || (this->m_fullCell ? ((i+1) == this->m_bcI) : false)) &&
	   ((j == this->m_bcJ)                                                       )   )
	{
		this->m_L.coeffRef(k_vy, k_vy) = 1.0;		//setting the coefficient for dirichlet.

		this->m_rhs[k_vy] = this->m_bcVY;

		//return true to indicate that we have handled the boundary.
		return true;
	}; //End if: test on internal

		pgl_assert(this->hook_testIfBoundaryVY(i, j) == false);
	return false;
};//End: handle VY


template<
	class 		ImplBase_T,
	bool 		isExtended
>
bool
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_testIfBoundaryVY(
		const Index_t 		i,
		const Index_t 		j)
 const
{
	if(this->Base_t::hook_testIfBoundaryVY(i, j) == true)
	{
		/* We are on the pipe boundary */
		return true;
	}; //end if: test if pipe

	if(((i == this->m_bcI) || (this->m_fullCell ? ((i+1) == this->m_bcI) : false)) &&
	   ((j == this->m_bcJ)                                                       )   )
	{
		//return true to indicate that we have handled the boundary.
		return true;
	}; //End if: test on internal

	/*
	 * If we are here we are neither on the pipe boundary
	 * nor on the internal boundary, so we return false.
	 */
	return false;
};//End: handle VY


template<
	class 		ImplBase_T,
	bool 		isExtended
>
bool
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_boundaryP(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_p,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT,
		const Numeric_t 	pScale)
{
	/*
	 * Before we handle the conditions we have to
	 * handle the ghost nodes.
	 * This is done the same way as for the pipe condition
	 */
	const Index_t Nx = this->getNx();
	const Index_t Ny = this->getNy();
	const eGridType thisGrid = (isExtended
				    ? mkExt(eGridType::StPressure)
				    : mkReg(eGridType::StPressure));


		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   0 <= k_p, k_p <= this->m_L.rows(),
			   this->m_L.rows() > 0);

	/*
	 * This handles the ghost nodes for all cases
	 */
	if(GridGeometry_t::isGhostNode(i, j, thisGrid, yNodeIdx_t(Ny), xNodeIdx_t(Nx)) == true)
	{
			pgl_assert(this->hook_testIfBoundaryP(i, j) );
		/*
		 * Handle the coefficients
		 */
		this->m_L.coeffRef(k_p, k_p) = 1.0;
		this->m_rhs[k_p]             = 0.0;

		return true;
	}; //End if: hanlde ghost node


	/*
	 * Now we have handled the ghost nodes
	 * ===============
	 */
	if(this->m_fullCell == false)
	{
		/*
		 * FullCell is deactivated, so only two nodes are used.
		 * This means that the pressure condition is handled as
		 * useual.
		 * Important here is, that nothing more had to be done.
		 *
		 * This fuction will never handle the ghost node,
		 * because they are already handled above.
		 */
		if(this->Base_t::hook_boundaryP(i, j, k_p, UP, DOWN, RIGHT, LEFT, pScale) == true)
		{
			/* We are on the pipe boundary, so it was handled already. */
			return true;
		}; //end if: test if pipe

		/* Because nothing more has to be done here, we exit
		 * in this mode */
		return false;
	//end if: fullCell deactivated
	}
	else
	{
		/*
		 * If the fullCell mode, we must also impiose the pressure point.
		 * However this is only needed when the average mode is used.
		 * In the other mode the pressure offset is handled below.
		 *
		 * Important here is, thet we have only to exit iff,
		 * we have handled the node here.
		 *
		 * Also no ghost node will be handled here.
		 */
#		if defined(EGD_PULLEDBALL_AVG_PRESS) && (EGD_PULLEDBALL_AVG_PRESS != 0)
		if(this->Base_t::hook_boundaryP(i, j, k_p, UP, DOWN, RIGHT, LEFT, pScale) == true)
		{
			/* We are on the pipe boundary, so it was handled already. */
			return true;
		}; //end if: test if pipe
#		endif /* End average mode */

		//no ereturn statement; work hbas to be do
	}; //End else: full cell activated.

	/*
	 * If we are here then all four velocities are modified.
	 * In this case we must test if we are on the right location.
	 * and if so we must manipulate the boundary.
	 */
	if((i == this->m_bcI) && (j == this->m_bcJ))
	{
#		if defined(EGD_PULLEDBALL_AVG_PRESS) && (EGD_PULLEDBALL_AVG_PRESS != 0)
		/* We are in the pressure average mode
		 * p_{i,j} = \frac{1}{4} ( p_{i+1,j} + p_{i-1,j} + p_{i,j+1} + p_{i,j-1} )
		 * ==> 0 =  -p_{i,j} + \frac{1}{4} ( p_{i+1,j} + p_{i-1,j} + p_{i,j+1} + p_{i,j-1} )
		 */
		this->m_L.coeffRef(k_p, k_p        ) = -1.0;		//p_{i,j}
		this->m_L.coeffRef(k_p, k_p + RIGHT) =  0.25;
		this->m_L.coeffRef(k_p, k_p + LEFT ) =  0.25;
		this->m_L.coeffRef(k_p, k_p + DOWN ) =  0.25;
		this->m_L.coeffRef(k_p, k_p + UP   ) =  0.25;

		this->m_rhs[k_p] = 0.0;		//they should sum to

#		else
		/* Here we have moved the point where we have imposed the pressure.
		 * This it is a dirichlet condition */
		this->m_L(k_p, k_p) = 1.0;
		this->m_rhs[k_p]    = this->get_pressOffset();
#		endif

		//return true to indicate that we have handled the boundary.
		return true;
	}; //End if: test on internal

	/*
	 * If we are here, then nothing has been used.
	 */
	return false;
};//End: handle P



template<
	class 		ImplBase_T,
	bool 		isExtended
>
bool
egd_stokesPulledBallSinBoundary_t<ImplBase_T, isExtended>::hook_testIfBoundaryP(
		const Index_t 		i,
		const Index_t 		j)
 const
{
	/*
	 * See the comented pressure mode
	 */

	const Index_t Nx = this->getNx();
	const Index_t Ny = this->getNy();
	const eGridType thisGrid = (isExtended
				    ? mkExt(eGridType::StPressure)
				    : mkReg(eGridType::StPressure));

		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   this->m_L.rows() > 0);

	if(GridGeometry_t::isGhostNode(i, j, thisGrid, yNodeIdx_t(Ny), xNodeIdx_t(Nx)) == true)
	{
		return true;
	}; //End if: hanlde ghost node


	if(this->m_fullCell == false)
	{
		if(this->Base_t::hook_testIfBoundaryP(i, j) == true)
		{
			return true;
		}; //end if: test if pipe

		/* Because nothing more has to be done here, we exit
		 * in this mode */
		return false;
	//end if: fullCell deactivated
	}
	else
	{
#		if defined(EGD_PULLEDBALL_AVG_PRESS) && (EGD_PULLEDBALL_AVG_PRESS != 0)
		if(this->Base_t::hook_testIfBoundaryP(i, j) == true)
		{
			return true;
		}; //end if: test if pipe
#		endif /* End average mode */

		//no ereturn statement; work hbas to be do
	}; //End else: full cell activated.

	if((i == this->m_bcI) && (j == this->m_bcJ))
	{
		return true;
	}; //End if: test on internal

	return false;
};//End: test P


PGL_NS_END(egd)


