/**
 * \brief	This file is contains the implementation of the pipe flow boundary.
 *
 *
 * Since we work with templates, we have to provide an instatiation in compile uints.
 * We will include this file in by the "header file" of the pipe class.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>

//Only include the header if YCM parses it
#if defined(YCM) && YCM != 0
#include "./egd_StokesBoundary_pipeFlow.hpp"
#else
#endif


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>


#if defined(EGD_IGNORE_NEG_PRESSURE) && (EGD_IGNORE_NEG_PRESSURE != 0)
#	pragma message "The pipe boundary condition will accept negative prerssure offsets through its explicit constructor."
#endif



PGL_NS_START(egd)

template<
	class 		ImplBase_t,
	bool 		isExtended
>
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::egd_stokesPipeBoundary_t(
	const uSize_t 			Ny,
	const uSize_t 			Nx,
	const Numeric_t 		pressOffset,
	const Numeric_t 		velXLeft,
	const Numeric_t 		velXRight,
	const Numeric_t 		velYTop,
	const Numeric_t 		velYBot,
	const egd_confObj_t* const 	confObj)
 :
# if defined(YCM) && YCM != 0
  egd_staggeredStokesSolverBase_t(Ny, Nx)
# else
  ImplBase_t(Ny, Nx)
# endif
{
	//Set the variables
#	define SET(r) if(pgl::isValidFloat(r) == false) {throw PGL_EXCEPT_InvArg("The argument " #r " is an invalid number");}; m_ ## r = r

	// Set the velocity boundaries
	SET(velXLeft);
	SET(velXRight);
	SET(velYTop);
	SET(velYBot);

	/*	 Handle the pressure	 */
#	if defined(EGD_IGNORE_NEG_PRESSURE) && (EGD_IGNORE_NEG_PRESSURE != 0)
	if(pressOffset < 0.0)
	{
#		if defined(EGD_PRINT_NEG_PRESSURE) && (EGD_PRINT_NEG_PRESSURE != 0)
		std::cerr
			<< "<<<! The supplied pressure (" << pressOffset << ") value is negative.\n"
			<< "        This will be ignored"
			<< std::endl;
#		endif
	};//End if: allow negative pressure
#	else
	if(m_pressOffset < 0.0)
	{
		throw PGL_EXCEPT_InvArg("The supplied pressure offset, " + std::to_string(m_pressOffset) + ", is negative.");
	};
#	endif
	SET(pressOffset);	//Set the pressure

	(void)confObj;
#	undef SET
}; //End constructor


template<
	class 		ImplBase_t,
	bool 		isExtended
>
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::egd_stokesPipeBoundary_t(
	const uSize_t 			Ny,
	const uSize_t 			Nx,
	const BoundaryCondition_t& 	pressCond,
	const BoundaryCondition_t& 	velXCond,
	const BoundaryCondition_t& 	velYCond,
	const egd_confObj_t* const 	confObj)
 :
  egd_stokesPipeBoundary_t(
  		Ny, Nx, 	//Passing the size
  		pressCond.getValue(eRandLoc::PRESS ),
  		velXCond .getValue(eRandLoc::LEFT  ),
  		velXCond .getValue(eRandLoc::RIGHT ),
  		velYCond .getValue(eRandLoc::TOP   ),
  		velYCond .getValue(eRandLoc::BOTTOM),
  		confObj                              )
{
#define IS_OK(c) if(c .isValid() == false) {throw PGL_EXCEPT_InvArg("The condition " #c " is not valid.");}; \
		 if(c .isFinal() == false) {throw PGL_EXCEPT_InvArg("The condition " #c " is not final.");}
	IS_OK(pressCond);
	IS_OK(velXCond );
	IS_OK(velYCond );
#undef IS_OK

	//Test if they are what they are
	if(pressCond.isPressure() == false)
		{ throw PGL_EXCEPT_InvArg("The presser condition was not a pressure condition."); };
	if(pressCond.isDirichlet(eRandLoc::PRESS) == false)
		{ throw PGL_EXCEPT_InvArg("Pressure condition is not a dirichlet condition."); };

	if(velXCond.getPropIdx() != PropIdx::VelX().getRepresentant())
		{ throw PGL_EXCEPT_InvArg("Velocity condition is not for velocity."); };

	if(velXCond.isFreeSlip (eRandLoc::TOP   ) == false ||
	   velXCond.isDirichlet(eRandLoc::RIGHT ) == false ||
	   velXCond.isFreeSlip (eRandLoc::BOTTOM) == false ||
	   velXCond.isDirichlet(eRandLoc::LEFT  ) == false   )
	{
		throw PGL_EXCEPT_InvArg("The velocity x component is invalid.");
	};

	if(velYCond.isDirichlet(eRandLoc::TOP   ) == false ||
	   velYCond.isFreeSlip (eRandLoc::RIGHT ) == false ||
	   velYCond.isDirichlet(eRandLoc::BOTTOM) == false ||
	   velYCond.isFreeSlip (eRandLoc::LEFT  ) == false   )
	{
		throw PGL_EXCEPT_InvArg("The velocity y component is invalid.");
	};
};//End: condition constructor


template<
	class 		ImplBase_t,
	bool 		isExtended
>
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::egd_stokesPipeBoundary_t(
	const egd_confObj_t& 		confObj)
 :
  egd_stokesPipeBoundary_t(
  		confObj.getNy(),	//Swapped order
  		confObj.getNx(),
  		confObj.getBC(eRandProp::Pressure),
  		confObj.getBC(eRandProp::VelX    ),
  		confObj.getBC(eRandProp::VelY    ),
  		&confObj                           )
{}; //End: building constructor with config object



template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::hook_boundaryVX(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vx,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
{
	//Get the grid properties
	const eGridType thisGrid = isExtended ? mkExt(eGridType::StVx) : mkReg(eGridType::StVx);
	const Index_t   Ny       = this->getNy();
	const Index_t   Nx       = this->getNx();
	const Index_t   nRows    = GridGeometry_t::getNGridPointsY(yNodeIdx_t(Ny), thisGrid);
	const Index_t   nCols    = GridGeometry_t::getNGridPointsX(xNodeIdx_t(Nx), thisGrid);

	//this are the coordinates for the boundary
	const Index_t   i_top = (isExtended
				 ? 0 			//ext: top row is no ghost
				 : 1);			//reg: top row is ghost
	const Index_t   i_bot = nRows - 1;		//Is always at the bottom
	const Index_t   j_lef = 0;			//Always left.
	const Index_t   j_rig = (isExtended
			         ? (nCols - 2)		//ext: ghost node row
			         : (nCols - 1));	//reg: last row

		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   0 <= k_vx, k_vx < this->m_L.rows(),
			   this->m_L.rows() > 0);
		pgl_assert(0 <= i, i <= nRows,
			   0 <= j, j <= nCols );


	/*
	 * We are on the boundary
	 *
	 * Here the top row is a virtual row, that is there
	 * but has no physical meaning. It is an artefact from the grid.
	 */

	if(GridGeometry_t::isGhostNode(i, j, thisGrid, yNodeIdx_t(Ny), xNodeIdx_t(Nx)) == true)	//Are we at a virtual node
	{
		//We are at a virtual node
			pgl_assert(k_vx < this->m_L.rows()           );
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVX(i, j) );

		//We force the virtual node to be zero
		this->m_L.coeffRef(k_vx, k_vx) 	= 1.0;
		this->m_rhs[k_vx] 			= 0.0;

		return true;
	}; //End if: handle ghost node




	/*
	 * Test if we are on the boundary or not
	 */
	if((i == i_top || i == i_bot) ||
	   (j == j_lef || j == j_rig)   )
	{
		//
		//We are at a real boundary

		/*
		 * Handling the no slip condition, force the
		 * normal velocity to zero. This will make the
		 * left and right wall unpenterable.
		 *
		 * Notice that the corner point are assigned
		 * to the free slip condition. This is done
		 * by the matlab code, and some other
		 * parts of the code.
		 * TODO: Find out why
		 *
		 * By using checking first for the top/bottom condition.
		 * the condition takes precedence.
		 */
		if(i == i_top || i == i_bot)
		{
			//We are at the top or the bottom

			/*
			 * Handling the free slip condition.
			 * This condition is that the derivation of the x velocity
			 * at the top, with respect to y is zero.
			 *
			 * Note that the MATLAB code also applied this condition to
			 * the velocities at left/right boundary, but this code
			 * does not do that.
			 */
			if(i == i_top)	//We are at the top wall
			{
					pgl_assert(k_vx <   this->m_L.rows(),  k_vx         < this->m_L.cols(),
						   0    <= (k_vx + DOWN)    , (k_vx + DOWN) < this->m_L.cols() );
					pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVX(i, j) );
				this->m_L.coeffRef(k_vx, k_vx       ) =  1.0;
				this->m_L.coeffRef(k_vx, k_vx + DOWN) = -1.0;
				this->m_rhs[k_vx]			=  0.0;
			};
			if(i == i_bot)	//We are at the bottom wall
			{
					pgl_assert(k_vx <   this->m_L.rows(),  k_vx       < this->m_L.cols(),
						   0    <= (k_vx + UP)      , (k_vx + UP) < this->m_L.cols() );
					pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVX(i, j) );
				this->m_L.coeffRef(k_vx, k_vx       )	=  1.0;
				this->m_L.coeffRef(k_vx, k_vx + UP  ) = -1.0;
				this->m_rhs[k_vx]			=  0.0;
			};
		//End if: We are at the top/bottom
		}
		else
		{
			/*
			 * We are at the left/right boundary.
			 * Because we have first checked for the top/
			 * bottom boundary, we have excluded the top/
			 * bottom corners
			 *
			 * We will now apply a no slip condition.
			 * This condition will force the normal velocity
			 * at the left/right boundary to zero.
			 * Thus no penetration happens.
			 */
				pgl_assert(i >  i_top && i <  i_bot,
					   j == j_lef || j == j_rig );
				pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVX(i, j) );

			//The coefficient is in either case the same
			this->m_L.coeffRef(k_vx, k_vx) = 1.0;

			//Testing on which side we are
			if(j == j_lef)
			{
				/*
				 * We are on the left side
				 */
					pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVX(i, j) );
				this->m_rhs[k_vx] = m_velXLeft;
			}
			else
			{
				/*
				 * Right side
				 */
					pgl_assert(j == j_rig                        );	//Ensures that we are on the right side
					pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVX(i, j) );
				this->m_rhs[k_vx]  = m_velXRight;
			};
		}; //End else: handle dirichlet condition

		/*
		 * Return true to indicate that we have handled the case
		 */
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVX(i, j) );
		return true;
	}; //End if: on boundary

	/*
	 * We are not at the boundary, so we have not handled the case.
	 */
		//pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVX(i, j) == false);
	return false;
	(void)RIGHT;
	(void)LEFT;
}; //End: handle VX


template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::hook_testIfBoundaryVX(
		const Index_t 		i,
		const Index_t 		j)
 const
{
	//Get the grid properties
	const eGridType thisGrid = isExtended ? mkExt(eGridType::StVx) : mkReg(eGridType::StVx);
	const Index_t   Ny       = this->getNy();
	const Index_t   Nx       = this->getNx();
	const Index_t   nRows    = GridGeometry_t::getNGridPointsY(yNodeIdx_t(Ny), thisGrid);
	const Index_t   nCols    = GridGeometry_t::getNGridPointsX(xNodeIdx_t(Nx), thisGrid);

	//this are the coordinates for the boundary
	const Index_t   i_top = (isExtended
				 ? 0 			//ext: top row is no ghost
				 : 1);			//reg: top row is ghost
	const Index_t   i_bot = nRows - 1;		//Is always at the bottom
	const Index_t   j_lef = 0;			//Always left.
	const Index_t   j_rig = (isExtended
			         ? (nCols - 2)		//ext: ghost node row
			         : (nCols - 1));	//reg: last row

		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   this->m_L.rows() > 0);
		pgl_assert(0 <= i, i <= nRows,
			   0 <= j, j <= nCols );


	if(GridGeometry_t::isGhostNode(i, j, thisGrid, yNodeIdx_t(Ny), xNodeIdx_t(Nx)) == true)	//Are we at a virtual node
	{
		return true;
	}; //End if: handle ghost node


	/*
	 * Test if we are on the boundary or not
	 */
	if((i == i_top || i == i_bot) ||
	   (j == j_lef || j == j_rig)   )
	{
		return true;
	}; //End if: on boundary

	/*
	 * We are not at the boundary, so we have not handled the case.
	 */
	return false;
}; //End: handle VX


template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::hook_boundaryVY(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vy,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
{
	const Index_t Nx = this->getNx();
	const Index_t Ny = this->getNy();
	const eGridType thisGrid = (isExtended
				    ? mkExt(eGridType::StVy)
				    : mkReg(eGridType::StVy));

	//Test if we are on a ghost node
	if(GridGeometry_t::isGhostNode(i, j, thisGrid, yNodeIdx_t(Ny), xNodeIdx_t(Nx)) == true)
	{
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVY(i, j) );
		this->m_L.coeffRef(k_vy, k_vy) = 1.0;
		this->m_rhs[k_vy]              = 0.0;
		return true;
	}; //End if: hanlde ghost node

	//Sie of the grid
	const Index_t   nRows    = GridGeometry_t::getNGridPointsY(yNodeIdx_t(Ny), thisGrid);
	const Index_t   nCols    = GridGeometry_t::getNGridPointsX(xNodeIdx_t(Nx), thisGrid);

	//Determine the egdes
	const Index_t i_top = 0; 		//Always
	const Index_t i_bot = isExtended
			      ? (nRows - 2)	//Ext: -1 ghost node, so one less
			      : (nRows - 1);	//Simply last row
	const Index_t j_lef = isExtended
			      ? 0		//Ext: no ghost row is left
			      : 1;		//Reg: ghost row is left
	const Index_t j_rig = nCols - 1;	//Always the most right one


		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   0 <= k_vy, k_vy < this->m_L.rows(),
			   this->m_L.rows() > 0);
		pgl_assert(0 <= i, i <= nRows,
			   0 <= j, j <= nCols );

	//Test if we are at the boundary
	if(((i == i_bot) || (i == i_top)) ||
	   ((j == j_lef) || (j == j_rig))   )
	{
		/*
		 * We are at the border.
		 */

		/*
		 * Free slip
		 * At the left/right boundaries we have free slip, meaning that d_x[v_y] == 0
		 * must hold. The corner points are also managed by this condition and
		 * not by the other one.
		 * Thus we will managed free splip first which will take precendence.
		 */
		if(j == j_lef || j == j_rig)	//note we have to account for the ghost column
		{
			/*
			 * Free slip
			 * The derivative, with respect to x, of the y velocity at the
			 * left and right boundary is zero.
			 * This boundary are tangential to the flow.
			 */
			if(j == j_lef)	//Left wall
			{
				pgl_assert((k_vy + RIGHT) >= 0         ,
					   (k_vy + RIGHT) <  this->m_L.cols()      );
				pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVY(i, j) );

				this->m_L.coeffRef(k_vy, k_vy        ) =  1.0;
				this->m_L.coeffRef(k_vy, k_vy + RIGHT) = -1.0;
				this->m_rhs[k_vy]			 =  0.0;
			}
			else	//Right wall
			{
				pgl_assert((k_vy + LEFT) >= 0         ,
					   (k_vy + LEFT) <  this->m_L.cols()       );
				pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVY(i, j) );

				this->m_L.coeffRef(k_vy, k_vy       ) =  1.0;
				this->m_L.coeffRef(k_vy, k_vy + LEFT) = -1.0;
				this->m_rhs[k_vy] 			=  0.0;
			};

			//End if: handle free slip
		}
		else
		{
			/*
			 * No slip
			 * The velocity of the y velocity at the top and bottom is
			 * zero, this ensures that the wall is not penetrated.
			 * Note that the corner of the domain are excluded by this and
			 * managed by the free slip condition.
			 */
			pgl_assert(i == i_top || i == i_bot,	//Test if we are in correct location
				   j >  j_lef && j <  j_rig    );
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVY(i, j) );

			//Set the coefficient in the matrix, in both cases it is simply a one
			this->m_L.coeffRef(k_vy, k_vy) = 1.0;

			//Test on which side we are
			if(i == i_top)
			{
				/*
				 * We are on the top
				 */
				this->m_rhs[k_vy] = m_velYTop;
			}
			else
			{
				/*
				 * We are at the bottom
				 */
				pgl_assert(i == i_bot);		//Ensure that we are on the right line

				this->m_rhs[k_vy] = m_velYBot;
			};
		}; //End else: handle no slip

		/*
		 * Return true to indicate that the handling was done
		 */
			//pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVY(i, j) == false);
		return true;
	}//End if: we are at the boundary


	/*
	 * Return false to indicate that we do not
	 * have handled the case yet.
	 */
		pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryVY(i, j) == false);
	return false;
	(void)UP;
	(void)DOWN;
};//End: handle VY


template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::hook_testIfBoundaryVY(
		const Index_t 		i,
		const Index_t 		j)
 const
{
	const Index_t Nx = this->getNx();
	const Index_t Ny = this->getNy();
	const eGridType thisGrid = (isExtended
				    ? mkExt(eGridType::StVy)
				    : mkReg(eGridType::StVy));

	//Test if we are on a ghost node
	if(GridGeometry_t::isGhostNode(i, j, thisGrid, yNodeIdx_t(Ny), xNodeIdx_t(Nx)) == true)
	{
		return true;
	}; //End if: hanlde ghost node

	//Sie of the grid
	const Index_t   nRows    = GridGeometry_t::getNGridPointsY(yNodeIdx_t(Ny), thisGrid);
	const Index_t   nCols    = GridGeometry_t::getNGridPointsX(xNodeIdx_t(Nx), thisGrid);

	//Determine the egdes
	const Index_t i_top = 0; 		//Always
	const Index_t i_bot = isExtended
			      ? (nRows - 2)	//Ext: -1 ghost node, so one less
			      : (nRows - 1);	//Simply last row
	const Index_t j_lef = isExtended
			      ? 0		//Ext: no ghost row is left
			      : 1;		//Reg: ghost row is left
	const Index_t j_rig = nCols - 1;	//Always the most right one


		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   this->m_L.rows() > 0);
		pgl_assert(0 <= i, i <= nRows,
			   0 <= j, j <= nCols );

	//Test if we are at the boundary
	if(((i == i_bot) || (i == i_top)) ||
	   ((j == j_lef) || (j == j_rig))   )
	{
		return true;
	}//End if: we are at the boundary


	/*
	 * That we are not at the boundary
	 */
	return false;
};//End: handle VY


template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::hook_boundaryP(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_pm,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT,
		const Numeric_t 	pScale)
{
	const Index_t Nx = this->getNx();
	const Index_t Ny = this->getNy();
	const eGridType thisGrid = (isExtended
				    ? mkExt(eGridType::StPressure)
				    : mkReg(eGridType::StPressure));


		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   0 <= k_pm, k_pm <= this->m_L.rows(),
			   this->m_L.rows() > 0);

	/*
	 * This handles the ghost nodes for all cases
	 */
	if(GridGeometry_t::isGhostNode(i, j, thisGrid, yNodeIdx_t(Ny), xNodeIdx_t(Nx)) == true)
	{
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j) );
		/*
		 * Handle the coefficients
		 */
		this->m_L.coeffRef(k_pm, k_pm) = 1.0;
		this->m_rhs[k_pm]              = 0.0;

		return true;
	}; //End if: hanlde ghost node

	if(isExtended == false)
	{
		/*
		 * Normal regular case
		 */
		if((j == 1 && i ==  1      ) || 		//top left corner
		   (j == 1 && i == (Ny - 1))   )		//bottom left corner
		{
			/*
			 * We force that the value in these nodes has the same
			 * value as their right neighbout.
			 * We do this by forcing that their difference is zero.
			 * It thus looks like a von Neumann condition.
			 */
			pgl_assert( k_pm          >= 0,  k_pm          < this->m_L.cols(),
				   (k_pm + RIGHT) >= 0, (k_pm + RIGHT) < this->m_L.cols() );
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j)                 );

			this->m_L.coeffRef(k_pm, k_pm        )	//The current node
				=  1.0;
			this->m_L.coeffRef(k_pm, k_pm + RIGHT)	//The right neighbour
				= -1.0;
			this->m_rhs[k_pm] = 0;
		//End if: coupling on the left
		}
		else if((j == (Nx - 1) && i == 1       ) ||		//Top right corner
			(j == (Nx - 1) && i == (Ny - 1))   )		//Bottom right corner
		{
			/*
			 * We do the same than we have done for the left corner.
			 * But now we use the left neighbour.
			 */
			pgl_assert( k_pm         >= 0,  k_pm         < this->m_L.cols(),
				   (k_pm + LEFT) >= 0, (k_pm + LEFT) < this->m_L.cols() );
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j)               );

			this->m_L.coeffRef(k_pm, k_pm       )		//Current node
				=  1.0;
			this->m_L.coeffRef(k_pm, k_pm + LEFT)		//the left neihbour
				= -1.0;
			this->m_rhs[k_pm] = 0.0;
		//End if: Coupling on the right
		}
		else if(i == 1 && j == 2)			//This is the point where we impose one value
		{
			/*
			 * This is the right neighbour of the top left corner.
			 * We force it to a value.
			 *
			 * TODO: Why exactly. Is is because we can determine presure only up to an
			 * 	  additive constant?
			 */
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j));

			this->m_L.coeffRef(k_pm, k_pm) = 1.0 * pScale;
			this->m_rhs[k_pm]		 = m_pressOffset;
		//End if: Imposing value
		}
		else
		{
			/*
			 * If we are here, then we did not handle any case
			 * so we must return false.
			 */
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j) == false);
			return false;
		}; //End else: no boundary

		/*
		 * If we are here, then we have handled one case.
		 * So we must return true to indicate this.
		 */
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j) );
		return true;
	//End if: Handle regular grid.
	}
	else
	{
		/*
		 * In the extended grid, we force the top left node, that is no ghost node
		 * to a certain value.
		 * Note that no further ghost nodes has to be handled.
		 */
		if((i == 1) && (j == 1))
		{
				pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j) );
			this->m_L.coeffRef(k_pm, k_pm) = 1.0 * pScale;
			this->m_rhs[k_pm]              = m_pressOffset;

			return true;	//Incicate that we have taken care of
		}; //End if: handle offset


		/*
		 * If we are here than the extended case was not handled
		 * and we have to return false.
		 */
			pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j) == false);
		return false;
	}; //End else: handle extended grid



	/*
	 * Note we will never be here, since all cases are handled in the branches of
	 * the above if statement. However I do not like functions without a return
	 * statement, so I have decided to put a true here.
	 * The reason is that this will lead to a singular matrix.
	 */
		//pgl_assert(this->egd_stokesPipeBoundary_t::hook_testIfBoundaryP(i, j));
		pgl_assert(false);
	return true;

	(void)UP;
	(void)DOWN;
}; //End: handle P


template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesPipeBoundary_t<ImplBase_t, isExtended>::hook_testIfBoundaryP(
		const Index_t 		i,
		const Index_t 		j)
 const
{
	const Index_t Nx = this->getNx();
	const Index_t Ny = this->getNy();
	const eGridType thisGrid = (isExtended
				    ? mkExt(eGridType::StPressure)
				    : mkReg(eGridType::StPressure));


		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   this->m_L.rows() > 0);

	/*
	 * This handles the ghost nodes for all cases
	 */
	if(GridGeometry_t::isGhostNode(i, j, thisGrid, yNodeIdx_t(Ny), xNodeIdx_t(Nx)) == true)
	{
		return true;
	}; //End if: hanlde ghost node

	if(isExtended == false)
	{
		/*
		 * Normal regular case
		 */
		if((j == 1 && i ==  1      ) || 		//top left corner
		   (j == 1 && i == (Ny - 1))   )		//bottom left corner
		{
			return true;
		//End if: coupling on the left
		}
		else if((j == (Nx - 1) && i == 1       ) ||		//Top right corner
			(j == (Nx - 1) && i == (Ny - 1))   )		//Bottom right corner
		{
			return true;
		//End if: Coupling on the right
		}
		else if(i == 1 && j == 2)			//This is the point where we impose one value
		{
			return true;
		//End if: Imposing value
		}
		else
		{
			/*
			 * If we are here, then we are not at the boundary.
			 */
			return false;
		}; //End else: no boundary

		/*
		 * If we are here, we are at the boundary.
		 */
		return true;
	//End if: Handle regular grid.
	}
	else
	{
		/*
		 * In the extended grid, we force the top left node, that is no ghost node
		 * to a certain value.
		 * Note that no further ghost nodes has to be handled.
		 */
		if((i == 1) && (j == 1))
		{
			return true;	//Incicate that we have taken care of
		}; //End if: handle offset


		return false;
	}; //End else: handle extended grid



	/*
	 * Note we will never be here, since all cases are handled in the branches of
	 * the above if statement. However I do not like functions without a return
	 * statement, so I have decided to put a true here.
	 * The reason is that this will lead to a singular matrix.
	 */
		pgl_assert(false);
	return true;
}; //End: handle P

PGL_NS_END(egd)


