/**
 * \brief	This file implements the construction functions of the staggered base.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_staggeredStokesSolverBase_t::egd_staggeredStokesSolverBase_t(
	const uSize_t 		Ny,
	const uSize_t 		Nx)
 :
  egd_generalSolverImpl_t(Ny, Nx), egd_StokesSolver_i()
{
	if(Ny <= 2)
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in y direction is too small.");
	};
	if(Nx <= 2)
	{
		throw PGL_EXCEPT_InvArg("The number of grid points in x direction is too small.");
	};
}; //End: building constructor.



egd_staggeredStokesSolverBase_t::egd_staggeredStokesSolverBase_t()
 = default;



egd_staggeredStokesSolverBase_t::egd_staggeredStokesSolverBase_t(
	egd_staggeredStokesSolverBase_t&&)
 = default;


egd_staggeredStokesSolverBase_t&
egd_staggeredStokesSolverBase_t::operator= (
	egd_staggeredStokesSolverBase_t&&)
 = default;


egd_staggeredStokesSolverBase_t::~egd_staggeredStokesSolverBase_t()
 = default;



egd_staggeredStokesSolverBase_t::InterfaceBase_t::SolverResult_ptr
egd_staggeredStokesSolverBase_t::creatResultContainer()
 const
{
	//We need to construct the result container in two steps.
	//This is because in C++ virtual dispace and constructors
	//are a bit strange.
	InterfaceBase_t::SolverResult_ptr res = ::std::make_unique<SolverResult_t>();
	res->allocateSize(yNodeIdx_t(this->getNy()), xNodeIdx_t(this->getNx()), this->hook_isExtendedGrid());

	return res;
};


egd_staggeredStokesSolverBase_t::SourceTerm_ptr
egd_staggeredStokesSolverBase_t::creatSourceTerm()
 const
{
	return ::std::make_unique<SourceTerm_t>(
			yNodeIdx_t(this->getNy()), xNodeIdx_t(this->getNx()),
			this->onExtendedGrid() );
};


egd_staggeredStokesSolverBase_t::InterfaceBase_t::SolverArg_ptr
egd_staggeredStokesSolverBase_t::createArgument()
 const
{
	InterfaceBase_t::SolverArg_ptr Arg = ::std::make_unique<SolverArg_t>();
	Arg->g_x( 0.0);	//Set x component to zero
	Arg->g_y(9.81); //Set y component to the standard value

	return Arg;
};

PGL_NS_END(egd)

