#pragma once
/**
 * \brief	This file contains the base implementation of the stockes solver that
 * 		 operates on the staggered grid.
 *
 * From thsi class the most solver will inherend.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include <egd_impl/genSolver/egd_generalSolver.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverArgument.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>




PGL_NS_START(egd)


/**
 * \brief	This is the staggered grid base.
 * \class 	egd_staggeredStokesSolverBase_t
 *
 * The staggered grid base for the Stokes solver is an
 * implementation that is basically responsible for composing
 * the system matrix. The system matrix is composed in an
 * agnostic way. This means only inner points are handled.
 *
 * Points on the boundary are redirected to the subclasses
 * by calling some hooks. This function will implement some
 * hooks from the general solver class, but not all.
 * It will also declares several other functions.
 */
class egd_staggeredStokesSolverBase_t : public egd_generalSolverImpl_t, public egd_StokesSolver_i
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
		static_assert(std::is_unsigned<uSize_t>::value, "uSize must be unsigned");
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using Matrix_t 	= egd_Matrix_t<Numeric_t>;	//!< This is the type of the reuslts.

	using ImplBase_t 	= egd_generalSolverImpl_t;	//!< This is the type that provides the general implementation.
	using InterfaceBase_t 	= egd_StokesSolver_i;		//!< This is the type that is the interface we are implementing.

	using SolverResult_t 	= egd_mechSolverResult_t;	//!< This is the result type of the mechanical solver, not an abstract one.
	using SolverResult_ptr 	= SolverResult_t::MechSolverResult_ptr;	//!< Pointer to the result type; note this is not the base and it is managed.

	using SolverArg_t  	= egd_mechSolverArg_t;			//!< This is the argument of the mechanical solver, this is not a base class.
	using SolverArg_ptr 	= SolverArg_t::MechSolverArg_ptr;	//!< Managed pointer to the argument class, note that this is not a base class.

	using GridContainer_t 	= egd_gridContainer_t;			//!< This is the grid container.
	using GridGeometry_t  	= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.
	using GridProperty_t 	= GridContainer_t::GridProperty_t;	//!< This is the type for grid property.
	using GridProperty_cref = GridContainer_t::GridProperty_cref;	//!< This is the property on the grid.
	using GridPosition_cref = GridContainer_t::GridPosition_cref;	//!< This is the grid position.
	using SystemMatrix_t 	= egd_SparseMatrix_t<Numeric_t>;	//!< This is the system matrix Type.
	using Vectot_t 		= egd_Vector_t<Numeric_t>;		//!< This is the type for a simple dense vector.

	using SourceTerm_t 	= InterfaceBase_t::SourceTerm_t;	//!< The source term.
	using SourceTerm_ptr 	= InterfaceBase_t::SourceTerm_ptr;	//!< Managed pointer to source term.

	using PropToGridMap_t   = InterfaceBase_t::PropToGridMap_t;	 //!< This is the map that deperimes which times are pressent.


	/*
	 * =========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This constructors sets up the system. The user passes the
	 * number of basic nodal points to this function. Be aware
	 * that the ordering is swaped. First comes the number of
	 * grid points in Y direction, then seconds the ones for X.
	 * This is done to be consistent.
	 *
	 * The default boundary conditions are used.
	 *
	 * Note that this function does not call allocateSystem.
	 * The function is called upon the first solving process.
	 * This is due to work arround a problem in C++.
	 *
	 * \param  Ny	The number of nodal points in y direction.
	 * \param  Nx	The number of nodal points in x direction.
	 */
	egd_staggeredStokesSolverBase_t(
		const uSize_t 		Ny,
		const uSize_t 		Nx);


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	egd_staggeredStokesSolverBase_t(
		const egd_staggeredStokesSolverBase_t&)
	 = delete;


	/**
	 * \brief	Copy Assignement.
	 *
	 * Is deleted.
	 */
	egd_staggeredStokesSolverBase_t&
	operator= (
		const egd_staggeredStokesSolverBase_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_staggeredStokesSolverBase_t(
		egd_staggeredStokesSolverBase_t&&);


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_staggeredStokesSolverBase_t&
	operator= (
		egd_staggeredStokesSolverBase_t&&);


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_staggeredStokesSolverBase_t();


	/*
	 * =======================
	 * Private Constructor
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * This is a private constructor, that is only provided
	 * for internal use.
	 */
	egd_staggeredStokesSolverBase_t();





	/*
	 * ======================
	 * Interface functions
	 */
public:
	/**
	 * \brief	See interface function for a description.
	 *
	 * This function will call the implementation function of the supper class.
	 */
	void
	solve(
		const GridContainer_t& 				grid,
		InterfaceBase_t::SolverArg_t* const 		solArg,
		InterfaceBase_t::SolverResult_t* const 		solResult)
	 override;


	/**
	 * \brief	This fucntion returns an instance of the result object.
	 *
	 * This function will call the allocation function.
	 * With teh appropriate arguments.
	 */
	virtual
	InterfaceBase_t::SolverResult_ptr
	creatResultContainer()
	 const
	 override;


	/**
	 * \brief	This function returns an instance of the solver argument.
	 *
	 * This function will create an argument instance.
	 * The x value of gravity is set to zero, the rest is not modified.
	 */
	virtual
	InterfaceBase_t::SolverArg_ptr
	createArgument()
	 const
	 override;


	/**
	 * \brief	This function returns an instace of the source term.
	 *
	 * This function returns an initialized source term.
	 */
	virtual
	SourceTerm_ptr
	creatSourceTerm()
	 const
	 override;


	/**
	 * \brief	This function returns a property map that is needed by the solver.
	 *
	 * This function adds all properties to the solver that are generally needed.
	 * Note that this function may have to be overridden for some new solver.
	 *
	 * The default implementation calls the function prot_mkStokesProp().
	 */
	virtual
	PropToGridMap_t
	mkNeededProperty()
	 const
	 override;

	/**
	 * \brief	This function returns true if *this is an inertial solver.
	 *
	 * This means it needs a past time step that is supplied by an argument.
	 *
	 * This function will return false, since the Stokes eqwuation by itself
	 * does not consider inertia.
	 */
	virtual
	bool
	isInertiaSolver()
	 const
	 override;


	/**
	 * \brief	This function outputs a description of *this.
	 *
	 * Since the interessting things are the boundary conditions,
	 * it is not very usefull to implement this function on this
	 * level, however it is usefull to provide a function that
	 * identifies *this, such that it can be used in the
	 * deriving code.
	 *
	 * See also hook_getImplName()
	 */
	virtual
	std::string
	print()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns the grid type of the solver.
	 *
	 * This function returns a fully staggered grid. If the grid is extended
	 * or not is determined by the isExtended hook function.
	 */
	virtual
	eGridType
	getGridType()
	 const
	 override;


	/**
	 * \brief	This function is for configuring the boundary condition.
	 *
	 * This function is used to firther configure the boundary conditions.
	 * This option is used by several inspecting function to compunicate.
	 * If the value was used by *this true is returned. A false does not
	 * indicate an error, only that the value was not used.
	 *
	 * The default will return false, and ignore any argument.
	 * This function should be impolemented by the boundary objects.
	 *
	 * \param  key		The key of the setting.
	 * \param  value	The option value.
	 */
	virtual
	bool
	setBoundaryOption(
		const std::string& 		key,
		const Numeric_t 		value)
	 override;


	/*
	 * ===================================
	 * General Hooks
	 *
	 * These are the hooks that are defined/requiered by
	 * the genral implementation class. Some are implemented
	 * by *this, some are redirected to the base and some are
	 * left abstract.
	 */
protected:
	using ImplBase_t::hook_initBoundaries;


	/**
	 * \brief	This function generates the system matrix.
	 * 		 See super class for more information.
	 *
	 * This function composes the system matrix.
	 * It assumes constant grid spacing.
	 *
	 * \param  grid		This is the grid container.
	 * \param  solArg	This is the argument of the solver.
	 */
	virtual
	void
	hook_buildSystemMatrix(
		const GridContainer_t& 			grid,
		ImplBase_t::SolverArg_i* const 		solArg)
	 override;


	/**
	 * \brief	This hook allows the modification of the system matrix.
	 * 		 See super class for more information.
	 *
	 * This function class the super function.
	 */
	virtual
	bool
	hook_modifySystemMatrix(
		const GridContainer_t&			grid,
		ImplBase_t::SolverArg_i* const 		solArg,
		const bool 				isFirstRun)
	 override;


	/**
	 * \brioef	Thsi function copies the solution in teh result container.
	 * 		 See super class for more information.
	 *
	 * This function redirectes to its base.
	 */
	virtual
	void
	hook_distributProperties(
		const Vectot_t&					sol,
		const GridContainer_t&				grid,
		ImplBase_t::SolverArg_i*  const 		solArg,
		ImplBase_t::SolverResult_i* const 		solRes)
	 const
	 override;


	/**
	 * \brief	This function is called after the solution was distributed.
	 * 		 See super class for more information.
	 *
	 * This class applies the pressure scalling to the pcomputed pressure to
	 * undo its effects.
	 *
	 * \param  sol		This is the compzted solution vector.
	 * \param  grid		This is the grid that is used.
	 * \þaram  solArg	This is the argument that was passed to *this.
	 * \param  solResult	This is the object that will store the solution.
	 *
	 * The default of this function does nothing.
	 */
	virtual
	void
	hook_postProcessSolution(
		const Vectot_t&					sol,
		const GridContainer_t&				grid,
		ImplBase_t::SolverArg_i*  const 		solArg,
		ImplBase_t::SolverResult_i* const 		solRes)
	 const
	 override;


	/**
	 * \brief	This function returns the numbers of unkowns in x direction.
	 * 		 See super class for more information.
	 *
	 * If *this is not extended, it will returns the number of basic grid points
	 * in x direction. If *this is extended it will return the former value + 1.
	 */
	virtual
	Index_t
	hook_nUnknownX()
	 const
	 override;


	/**
	 * \brief	This function returns the number of unknowns in y direction.
	 * 		 See super class for more information.
	 *
	 * If *this is not extended, it will returns the number of basic grid points
	 * in y direction. If *this is extended it will return the former value + 1.
	 */
	virtual
	Index_t
	hook_nUnknownY()
	 const
	 override;


	/**
	 * \brief	This is the number of variables that are located
	 * 		 at a single grid node.
	 * 		 See super class for more information.
	 *
	 * This function returns 3.
	 *  0 :->	VX
	 *  1 :->	VY
	 *  2 :->	P
	 */
	virtual
	Index_t
	hook_nPropPerNode()
	 const
	 override;


	/**
	 * \brief	This is the number of unkowns that should be reservered
	 * 		 in each column.
	 * 		 See super class.
	 *
	 * This function calss the default.
	 */
	virtual
	Index_t
	hook_reservePerCol()
	 const
	 override;


	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not. See super class.
	 *
	 * True indicates that *this is an extextended grid.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns true if the system matrix and the
	 * 		 rhs should be damped into a text file.
	 *
	 * This function calls the default.
	 *
	 * \param  isFrisRun		Indicates if the first run is happening.
	 */
	virtual
	bool
	hook_writeSystem(
		const bool 	isFrisRun)
	 const
	 override;


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping. See super class.
	 *
	 * This function is not implemented.
	 *
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function outputs a small description
	 * 		 of the solver that is implemented.
	 *
	 * This function is used by the boundary conditions's
	 * print function to make a description.
	 *
	 * The default implementation outputs "Staggered Stokes Solver".
	 */
	virtual
	std::string
	hook_getImplName()
	 const;


	/*
	 * ===============================
	 * Stokes Hooks
	 *
	 * These are hooks that are needed because *this is a stokes hook.
	 * These fucntions are primarly needed by the composing function.
	 *
	 * There are also a thrid categories of hooks, these hooks deals with
	 * the handling of the boundary condition on a more fine grained scale.
	 */
protected:
	/**
	 * \brief	This function returns the pScale value.
	 *
	 * This is the p scale. This is a scaling factor that
	 * enters the matrix. Its result is that it will make
	 * the solution value, that is obtained by solving the
	 * linear system smaller. This is needed because the
	 * velocities are very small and the pressure is very
	 * large. Thus the values will become more stable.
	 *
	 * When writting the solution the modification must be
	 * accounted for.
	 *
	 * \param  grid		The grid object that is processed.
	 *
	 * \note	The default of this function returns 1+e21 / dx,
	 * 		 where dx is the grid spacing in the x direction.
	 */
	virtual
	Numeric_t
	hook_getPScale(
		const GridContainer_t& 		grid)
	 const;



	/*
	 * ==============================
	 * BC Hooks
	 *
	 * These hooks delas exclusively with the boundary conditions.
	 * These functions are called by the building function.
	 * For eqach property there is a dediocated function.
	 * Note that all of these functions are requiered to handle the
	 * boundary conditions on their own.
	 */
protected:
	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VX property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a VX component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vx 	The global index of the node.
	 * \param  UP		Go to the upper VX node.
	 * \param  DOWN		Go to the lower VX node.
	 * \param  RIGHT	Go to the right VX node.
	 * \param  LEFT		Go to the left VX node.
	 */
	virtual
	bool
	hook_boundaryVX(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vx,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 = 0;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VX grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVX(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 = 0;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the VY property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a VY component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_vy 	The global index of the node.
	 * \param  UP		Go to the upper VY node.
	 * \param  DOWN		Go to the lower VY node.
	 * \param  RIGHT	Go to the right VY node.
	 * \param  LEFT		Go to the left VY node.
	 */
	virtual
	bool
	hook_boundaryVY(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vy,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 = 0;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the VY grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryVY(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 = 0;


	/**
	 * \brief	This function handles the boundary conditions for
	 * 		 the P property.
	 *
	 * This function returns true if the passed index, a global index that
	 * referes to a P component, belongs to a boundary node.
	 * It is also requiered to handle the system matrix in that case,
	 * and the RHS as well to accomodate for that case.
	 * All movement has to be applied with a +.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 * \param  k_p 		The global index of the node.
	 * \param  UP		Go to the upper P node.
	 * \param  DOWN		Go to the lower P node.
	 * \param  RIGHT	Go to the right P node.
	 * \param  LEFT		Go to the left P node.
	 * \param  pScale	The p scale value.
	 */
	virtual
	bool
	hook_boundaryP(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_p,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT,
		const Numeric_t 	pScale)
	 = 0;


	/**
	 * \brief	This function TESTS if (i, j) is a boundary node
	 * 		 on the P grid.
	 *
	 * This function does not handle the boundary condition. Its
	 * only job is to test if it is a boundary or not.
	 *
	 * \param  i		The ith coordinate of the point (y).
	 * \param  j 		The jth coordinate of the point (x).
	 */
	virtual
	bool
	hook_testIfBoundaryP(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 = 0;


	/*
	 * ======================
	 * Helper functions
	 *
	 * These are functions that are not virtual, but are provided for
	 * helpf of subclasses. They are mostly static.
	 */
protected:
	/**
	 * \brief	This function creates the property list for a
	 * 		 stokes solver.
	 *
	 * This is the original solver, where no inertia is pressent.
	 */
	PropToGridMap_t
	prot_mkStokesProp()
	 const;




	/*
	 * ========================
	 * Private Members
	 *
	 * This class does not have any memeber.
	 * It is basically a trampoline that connects the conrete
	 * sub class, the composing of the system matrix and the
	 * solver driver with one another.
	 */
private:
}; //End: class(egd_staggeredStokesSolverBase_t)


PGL_NS_END(egd)


