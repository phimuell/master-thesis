/**
 * \brief	This file is contains the implementation of the pipe flow boundary.
 *
 *
 * Since we work with templates, we have to provide an instatiation in compile uints.
 * We will include this file in by the "header file" of the pipe class.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>

//Only include the header if YCM parses it
#if defined(YCM) && YCM != 0
#include "./egd_StokesBoundary_shearFlow.hpp"
#else
#endif


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>


/*
 * This message is not needed, since it is fully handled
 * in the pipe constructor. Hower since it will use it, we will keep the printing.
 */
#if defined(EGD_IGNORE_NEG_PRESSURE) && (EGD_IGNORE_NEG_PRESSURE != 0)
#	pragma message "The shear boundary condition will accept negative prerssure offsets through its explicit constructor."
#endif



PGL_NS_START(egd)

using ::pgl::isValidFloat;


template<
	class 		ImplBase_t,
	bool 		isExtended
>
egd_stokesShearBoundary_t<ImplBase_t, isExtended>::egd_stokesShearBoundary_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t		Nx,
	const BoundaryCondition_t& 	pressCond,
	const BoundaryCondition_t& 	velXCond,
	const BoundaryCondition_t& 	velYCond,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Index_t(Ny), Index_t(Nx),
  	 pressCond,
  	 velXCond, velYCond,
  	 confObj)
{
#define IS_OK(c) if(c .isValid() == false) {throw PGL_EXCEPT_InvArg("The condition " #c " is not valid.");}; \
		 if(c .isFinal() == false) {throw PGL_EXCEPT_InvArg("The condition " #c " is not final.");}
	IS_OK(pressCond);
	IS_OK(velXCond );
	IS_OK(velYCond );
#undef IS_OK
	if(velXCond.hasInternalBC() == false)
	{
		throw PGL_EXCEPT_InvArg("No internal VelX condition passed.");
	};
	if(velYCond.hasInternalBC() == false)
	{
		throw PGL_EXCEPT_InvArg("No internal VelY condition passed.");
	};
	if(Index_t(Nx) != Index_t(Ny))
	{
		throw PGL_EXCEPT_InvArg("The shearing conditon only supports square domains,"
				" with the same number of nodes in each direction."
				" But a domain with " + std::to_string(Ny) + "x" + std::to_string(Nx)
				+ " nodes was passed.");
	};
	if(confObj != nullptr)
	{
		const Numeric_t Lx = confObj->domainLengthX();
		const Numeric_t Ly = confObj->domainLengthY();

		if(Lx <= 0.0 || Ly <= 0.0)
		{
			throw PGL_EXCEPT_InvArg("Passed a negative domain length "
					+ std::to_string(Ly) + "x" + std::to_string(Lx) );
		};
		if(Lx != Ly)
		{
			throw PGL_EXCEPT_InvArg("The domain is not square."
					" instead the dimensions are "
					+ std::to_string(Ly) + "x" + std::to_string(Lx) );
		};
	}; //end if: optional checks if the conf object was passed

	/* Extract the conditions for the lower wedge */
	{
		const SingleCondition_t& lowWedg_x = velXCond.getSideCond(eRandLoc::INT_LOWSHEAR);
		const SingleCondition_t& lowWedg_y = velYCond.getSideCond(eRandLoc::INT_LOWSHEAR);
			pgl_assert(lowWedg_x.isDirichlet(), lowWedg_x.hasValue(),
				   lowWedg_y.isDirichlet(), lowWedg_y.hasValue() );

		this->m_lowWed_VelX = lowWedg_x.getValue();	pgl_assert(isValidFloat(this->m_lowWed_VelX));
		this->m_lowWed_VelY = lowWedg_y.getValue();	pgl_assert(isValidFloat(this->m_lowWed_VelY));
	}; //End scope: lower wedge extraction


	/* Extract the conditions for the lower wedge */
	{
		const SingleCondition_t& upWedg_x = velXCond.getSideCond(eRandLoc::INT_UPSHEAR);
		const SingleCondition_t& upWedg_y = velYCond.getSideCond(eRandLoc::INT_UPSHEAR);
			pgl_assert(upWedg_x.isDirichlet(), upWedg_x.hasValue(),
				   upWedg_y.isDirichlet(), upWedg_y.hasValue() );

		this->m_upWed_VelX = upWedg_x.getValue();	pgl_assert(isValidFloat(this->m_upWed_VelX));
		this->m_upWed_VelY = upWedg_y.getValue();	pgl_assert(isValidFloat(this->m_upWed_VelY));
	}; //End: scope: upper wedge extraction

	(void)confObj;
};//End: condition constructor


template<
	class 		ImplBase_t,
	bool 		isExtended
>
egd_stokesShearBoundary_t<ImplBase_t, isExtended>::egd_stokesShearBoundary_t(
	const egd_confObj_t& 		confObj)
 :
  egd_stokesShearBoundary_t(
  		yNodeIdx_t(confObj.getNy()),	//Swapped order
  		xNodeIdx_t(confObj.getNx()),
  		confObj.getBC(eRandProp::Pressure),
  		confObj.getBC(eRandProp::VelX    ),
  		confObj.getBC(eRandProp::VelY    ))
  		//,&confObj                           )
{}; //End: building constructor with config object



template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesShearBoundary_t<ImplBase_t, isExtended>::hook_boundaryVX(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vx,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
{
	/*
	 * We will first check for the external boundary.
	 * This boundary is handled by the base class object,
	 * which is a pipe object.
	 * This will also handle the ghost nodes.
	 */
	if(this->Base_t::hook_boundaryVX(i, j, k_vx, UP, DOWN, RIGHT, LEFT) == true)
	{
		/* If we are here then the pipe base has handled the node already.
		 * So we will now return true.  */
		return true;
	}; //End if: pipe handled the node.

	/*
	 * If we are here, then wer are not governed by the pipe boudnary.
	 * So we have to check if we are on an internal boundary
	 */

	//Get the grid properties
	const eGridType thisGrid = isExtended ? mkExt(eGridType::StVx) : mkReg(eGridType::StVx);
	const Index_t   Ny       = this->getNy();
	const Index_t 	Ny1 	 = Ny + 1;
	const Index_t   Nx       = this->getNx();
	const Index_t   nRows    = GridGeometry_t::getNGridPointsY(yNodeIdx_t(Ny), thisGrid);
	const Index_t   nCols    = GridGeometry_t::getNGridPointsX(xNodeIdx_t(Nx), thisGrid);
		pgl_assert(Ny == Nx);
		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   0 <= k_vx, k_vx < this->m_L.rows(),
			   this->m_L.rows() > 0);
		pgl_assert(0 <= i, i <= nRows,
			   0 <= j, j <= nCols );

	/*
	 * We will first test if we are on the internal condition.
	 * This checkes are taken direcly from the Marker_divergence.m
	 * script, addaptions where done for translating it to C.
	 *
	 * Let i_m by a matlab index, and i_c is a C indexing. Assume A
	 * is an array, or something to index. If we have a matlab code
	 * with A[i_m] how can we translate that into C++?
	 * I.e. how we can access the same element (position)?
	 *
	 * Since matlab starts counting at one, and C at 0, it is quite simple.
	 * A[1] denotes the first entry in matlab and A[0] in C. So we have
	 * the simple relation, i_m == i_c + 1.
	 */
	if((i > 2        ) &&  // (i_m > 3)    	transformation needed
	   (i < (Ny1 - 3)) &&  // (i_n < Ny1-2) Transformation needed
	   ((j == (i - 0)) || (j == (i + 2))) )	//no transformation needed, because apears on both side.
	{
		/*
		 * We are on the shear lines, now we have to figuring out
		 * on which line we are exactly.
		 * Hower only the RHS is affected by this.
		 */
		this->m_L.coeffRef(k_vx, k_vx) = 1.0;	//setting the condition, such that a dirichlet condition happes.

		//handling the RHS value
		if(j == (i - 0))
		{
			//We are on the lower wedge
			this->m_rhs[k_vx] = this->m_lowWed_VelX;
		}
		else
		{
			//We are on the upper wedge
			this->m_rhs[k_vx] = this->m_upWed_VelX;
		};

		//return true to indicate that we have handled the element.
		return true;
	};//end if: inside boundary

	/*
	 * We are here, this means that neider the pipe boundary
	 * nor the internal boundary is active, so we have to
	 * return false to indicate this
	 */
	return false;
	pgl_onlyForDebugging(Nx, thisGrid, nRows, nCols);
}; //End: handle VX


template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesShearBoundary_t<ImplBase_t, isExtended>::hook_testIfBoundaryVX(
		const Index_t 		i,
		const Index_t 		j)
 const
{
	/*
	 * Test if we are under the influence of the pipe boundary.
	 */
	if(this->Base_t::hook_testIfBoundaryVX(i, j) == true)
	{
		/* We are on a pipe boundary point, so we have to return true */
		return true;
	}; //End if: test on pipe

	/*
	 * If we are here then we are not on a pipe boundary.
	 * So we have to check if we are on an internal boundary.
	 */

	//Get the grid properties
	//const eGridType thisGrid = isExtended ? mkExt(eGridType::StVx) : mkReg(eGridType::StVx);
	const Index_t   Ny       = this->getNy();
	const Index_t   Ny1      = Ny + 1;
		pgl_assert(this->getNx() == this->getNy());

	/*
	 * See description in the handling function for an explantion.
	 * This checkes are taken direcly from the Marker_divergence.m
	 * script, addaptions where done for translating it to C.
	 */
	if((i > 2        ) &&
	   (i < (Ny1 - 3)) &&
	   ((j == (i - 0)) || (j == (i + 2))) )
	{
		//return true to indicate that we are indeed on an inner boundary.
		return true;
	};//End if: test on internal

	/*
	 * We are not on an internal boundary too. so we return false
	 * to indicae this
	 */
	return false;
}; //End: handle VX


template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesShearBoundary_t<ImplBase_t, isExtended>::hook_boundaryVY(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_vy,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
{
	/*
	 * First we will test if the pipe boundary is active
	 * This is done by calling the function of the base class.
	 */
	if(this->Base_t::hook_boundaryVY(i, j, k_vy, UP, DOWN, RIGHT, LEFT) == true)
	{
		/* We are on the pipe boundary, so it was handled already. */
		return true;
	}; //end if: test if pipe


	//Load values
	const eGridType thisGrid = isExtended ? mkExt(eGridType::StVx) : mkReg(eGridType::StVx);
	const Index_t   Nx = this->getNx();	//load some value
	const Index_t   Ny = this->getNy();
	const Index_t   nRows    = GridGeometry_t::getNGridPointsY(yNodeIdx_t(Ny), thisGrid);
	const Index_t   nCols    = GridGeometry_t::getNGridPointsX(xNodeIdx_t(Nx), thisGrid);
		pgl_assert(Ny == Nx);
		pgl_assert(this->getNx() == this->getNy());
		pgl_assert(this->m_L.rows() == this->m_L.cols(),
			   this->m_L.rows() == this->m_rhs.size(),
			   0 <= k_vy, k_vy < this->m_L.rows(),
			   this->m_L.rows() > 0);
		pgl_assert(0 <= i, i <= nRows,
			   0 <= j, j <= nCols );


	/*
	 * If we are here we must now test if we are on the internal boundary.
	 *
	 * Test if we are on the inner shearing condition.
	 * This checkes are taken direcly from the Marker_divergence.m
	 * script, addaptions where done for translating it to C.
	 *
	 * See the handling code for the x velcoity for an explanation.
	 * No Ny1 is needed, only Ny is needed.
	 */
	if((i >  2      )  && 			// (i_m > 3); needed translation
	   (i < (Ny - 3))  &&			// (i_m < Ny-2); need translation, also note usage of Ny
	   ((j == (i + 1)) || (j == (i + 3))) )	//no translation needed
	{
		/* We are on an internal boundary, now we must test on which
		 * one we are exactly */

		this->m_L.coeffRef(k_vy, k_vy) = 1.0;		//setting the coefficient for dirichlet.
		if(i == (i + 1))
		{
			//We are on the lower boundary
			this->m_rhs[k_vy] = this->m_lowWed_VelY;
		}
		else
		{
			//We are on the upper boundary
			this->m_rhs[k_vy] = this->m_upWed_VelY;
		};

		//return true to indicate that we have handled the boundary.
		return true;
	}; //End if: test on internal

	/*
	 * If we are here then neither the pipe or the internal boundary is
	 * in place. So we will thus return tuero to indicate such a
	 * case.
	 */

		pgl_assert(this->hook_testIfBoundaryVY(i, j) == false);
	return false;
	pgl_onlyForDebugging(Nx, thisGrid, nRows, nCols);
};//End: handle VY


template<
	class 		ImplBase_t,
	bool 		isExtended
>
bool
egd_stokesShearBoundary_t<ImplBase_t, isExtended>::hook_testIfBoundaryVY(
		const Index_t 		i,
		const Index_t 		j)
 const
{
	/*
	 * First we will test if the pipe boundary is active
	 * This is done by calling the function of the base class.
	 */
	if(this->Base_t::hook_testIfBoundaryVY(i, j) == true)
	{
		/* We are on the pipe boundary */
		return true;
	}; //end if: test if pipe

	//Load values
	const Index_t   Nx = this->getNx();	//load some value
	const Index_t   Ny = this->getNy();
		pgl_assert(Nx == Ny);

	/*
	 * Test if we are on an internal boundary.
	 * See description of the handling function for more details.
	 */
	if((i >  2      )  &&
	   (i < (Ny - 3))  &&
	   ((j == (i + 1)) || (j == (i + 3))) )
	{
		/* We are on an internal boundary */
		return true;
	}; //End if: test on internal

	/*
	 * If we are here we are neither on the pipe boundary
	 * nor on the internal boundary, so we return false.
	 */
	return false;
};//End: handle VY

PGL_NS_END(egd)


