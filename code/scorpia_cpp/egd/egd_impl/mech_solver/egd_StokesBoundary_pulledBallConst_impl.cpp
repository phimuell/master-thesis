/**
 * \brief	This file is contains the implementation of the const pulled ball pipe flow boundary.
 *
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include <egd_interfaces/egd_generalSolverArgument.hpp>

//Only include the header if YCM parses it
#if defined(YCM) && YCM != 0
#include "./egd_StokesBoundary_pulledBallConst.hpp"
#else
#endif



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>




PGL_NS_START(egd)

using ::pgl::isValidFloat;

template<
	class 		ImplBase_T,
	bool 		isExtended
>
egd_stokesPulledBallConstBoundary_t<ImplBase_T, isExtended>::egd_stokesPulledBallConstBoundary_t(
	const Numeric_t 		VX,
	const Numeric_t 		VY,
	const egd_confObj_t&	 	confObj)
 :
  Base_t(NAN, 		//alpha and omega are passed as NAN, this will instruct the base
  	 NAN,		//to not use them not look for them.
  	 confObj)
{
	this->m_constVelX = VX;		pgl_assert(isValidFloat(VX), VX <= 0.0);
	this->m_constVelY = VY;		pgl_assert(isValidFloat(VY)           );
}; //End: building constructor with config object


template<
	class 		ImplBase_T,
	bool 		isExtended
>
egd_stokesPulledBallConstBoundary_t<ImplBase_T, isExtended>::egd_stokesPulledBallConstBoundary_t(
	const egd_confObj_t& 		confObj)
 :
  egd_stokesPulledBallConstBoundary_t(
  		confObj.setUpPar("vx"),	//requiere parameters
  		confObj.setUpPar("vy"),
  		confObj                )
{
}; //End: building constructor with config object



template<
	class 		ImplBase_T,
	bool 		isExtended
>
void
egd_stokesPulledBallConstBoundary_t<ImplBase_T, isExtended>::hook_computeBallVel(
	const Numeric_t 		t,
	Numeric_t* const 		VX,
	Numeric_t* const 		VY)
 const
{
		pgl_assert(isValidFloat(t),
			   isValidFloat(this->getT0() ), t >= this->getT0(),
			   isValidFloat(this->m_constVelX),
			   isValidFloat(this->m_constVelY),
			   isValidFloat(this->getX0()),
			   isValidFloat(this->getY0()),
			   VX != nullptr,
			   VY != nullptr );

	*VY = this->m_constVelY;
	*VX = this->m_constVelX;
	return;
}; //End: compute velocity


template<
	class 		ImplBase_T,
	bool 		isExtended
>
::pgl::Numeric_t
egd_stokesPulledBallConstBoundary_t<ImplBase_T, isExtended>::hook_computeXCoM(
	const Numeric_t 		t)
 const
{
		pgl_assert(isValidFloat(t),
			   isValidFloat(this->getT0() ), t >= this->getT0(),
			   isValidFloat(this->m_constVelX),
			   isValidFloat(this->m_constVelY),
			   isValidFloat(this->getX0()),
			   isValidFloat(this->getY0()) );
	const Numeric_t Xt = this->m_constVelX * (t - this->getT0()) + this->getX0();
		pgl_assert(isValidFloat(Xt));
	return Xt;
};


template<
	class 		ImplBase_T,
	bool 		isExtended
>
::pgl::Numeric_t
egd_stokesPulledBallConstBoundary_t<ImplBase_T, isExtended>::hook_computeYCoM(
	const Numeric_t 		t)
 const
{
		pgl_assert(isValidFloat(t),
			   isValidFloat(this->getT0() ), t >= this->getT0(),
			   isValidFloat(this->m_constVelX),
			   isValidFloat(this->m_constVelY),
			   isValidFloat(this->getX0()),
			   isValidFloat(this->getY0()) );
	const Numeric_t Yt = this->m_constVelY * (t - this->getT0()) + this->getY0();
		pgl_assert(isValidFloat(Yt));
	return Yt;
};


PGL_NS_END(egd)


