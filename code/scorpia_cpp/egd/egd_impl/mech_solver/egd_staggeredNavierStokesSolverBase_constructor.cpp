/**
 * \brief	This file impllements the constructors that are needed by the navier stokes solver.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_staggeredNavierStokesSolverBase.hpp"

#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)

egd_staggeredNavierStokesSolverBase_t::egd_staggeredNavierStokesSolverBase_t(
	const uSize_t 		Ny,
	const uSize_t 		Nx)
 :
  ImplBase_t(Ny, Nx)
{
}; //End: building constructor.



egd_staggeredNavierStokesSolverBase_t::egd_staggeredNavierStokesSolverBase_t()
 = default;



egd_staggeredNavierStokesSolverBase_t::egd_staggeredNavierStokesSolverBase_t(
	egd_staggeredNavierStokesSolverBase_t&&)
 = default;


egd_staggeredNavierStokesSolverBase_t&
egd_staggeredNavierStokesSolverBase_t::operator= (
	egd_staggeredNavierStokesSolverBase_t&&)
 = default;


egd_staggeredNavierStokesSolverBase_t::~egd_staggeredNavierStokesSolverBase_t()
 = default;


PGL_NS_END(egd)

