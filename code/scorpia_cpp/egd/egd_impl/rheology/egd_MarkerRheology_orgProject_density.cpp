/**
 * \brief	This file calculates the density.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_MarkerRehology_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include "./egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

void
egd_markerRheologyOrgProj_t::priv_updateDensity(
	MarkerCollection_t* const 	mColl_)
 const
{
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;	//Create an alias of the collection.

	//Load the number of markers
	const auto nMarkers = mColl.nMarkers();
		pgl_assert(nMarkers >  0                ,
			   nMarkers == m_dEpsII  .size(),
			   nMarkers == m_pressure.size() );

	/*
	 * The formulat for the density is given as:
	 * 	\rho = \rho(P, T) = \rho_0 * \frac{1 + \beta * (P - 10^{5})}{1 + \alpha * (T - 273)}
	 *
	 * \alpha and \beta are parameters that are amrkerproperties themself.
	 * (I have no idea why they are implemented as such.)
	 * The only paramter that depends on the type is \rho_0.
	 */

	//Load the variables
	const MarkerProperty_t& ALPHA = mColl.cgetMarkerProperty(PropIdx::ThermExpansAlpha() )    ;
	const MarkerProperty_t& BETA  = mColl.cgetMarkerProperty(PropIdx::CompressibililityBeta() );
	const MarkerProperty_t& T     = mColl.cgetMarkerProperty(PropIdx::Temperature() )          ;
	const MarkerProperty_t& P     = m_pressure                                                 ;
		pgl_assert(nMarkers == ALPHA.size(),
			   nMarkers ==  BETA.size(),
			   nMarkers ==     T.size(),
			   nMarkers ==     P.size() );

	//This is the target variable
	MarkerProperty_t& RHO   = mColl.getMarkerProperty(PropIdx::Density() );
	MarkerProperty_t& RHOCP = mColl.getMarkerProperty(PropIdx::RhoCp()   );
		pgl_assert(nMarkers == RHO  .size(),
			   nMarkers == RHOCP.size() );

	//This is a counter that counts how many updates where done
	uSize_t nUpdates = 0;


	/*
	 * Iterate over the rocks
	 * This will ignore the air.
	 */
	for(Index_t currType = TY_ROCK_START; currType != TY_END; ++currType)
	{
		//Get a reference to the index and the type
		const MarkerIndexes_t& markerList = mColl.cgetIndexes(currType);
			pgl_assert(markerList.size() > 0);

		//Load the rho0 paramter
		const Numeric_t RHO_0 = m_rho0[currType];
			pgl_assert(pgl::isValidFloat(RHO_0), RHO_0 > 0.0);

		//Load the Cp value
		const Numeric_t Cp = m_Cp[currType];
			pgl_assert(pgl::isValidFloat(Cp), Cp > 0.0);

		//Iterate over all markers in the current set and compute the new density
		for(const auto& m : markerList)
		{
				pgl_assert(m >= 0, m < nMarkers);

			//Load marker specific data
			const Numeric_t     P_m = P[m];
			const Numeric_t     T_m = T[m];
			const Numeric_t  BETA_m = BETA[m];
			const Numeric_t ALPHA_m = ALPHA[m];

			//Compute the new density
			const Numeric_t newDensity = RHO_0 * (1.0 + BETA_m * (P_m - 10e+5)) / (1.0 + ALPHA_m * (T_m - 273.0));
				pgl_assert(pgl::isValidFloat(newDensity), newDensity > 0.0);

			//Calculate the new Cp value
			const Numeric_t newCp = newDensity * Cp;

			//Write back
			RHO  [m] = newDensity;
			RHOCP[m] = newCp;

			//Increase the counter for updates
			nUpdates += 1;
		}; //End for(m):
	}; //End for(mClasses):
		pgl_assert(RHO  .array().isFinite().all(),
			   RHOCP.array().isFinite().all() );

	return;
}; //End: update density


PGL_NS_END(egd)











