# Info
This folder contains the code that manages the physical properties of the markers.
It will change the density and so on as the conditions changes.

Note that the rheology is either very tightly connected to the setting or super general.

## Pulled Ball Sin
This is a very special rheology, that is intended to be used with a pulled ball setting.
It influences the velocity property of the markers. It imposes a time depended velocity
field.

## Pulled Ball Const
In this setting the ball is moved at constant velocity. The velocity itself is read in from
the configuration file.


## NULL Rheology
This is a rheology that does not modify the markers property. It can be seen as a noops.
It is provided for special function operation.

## Standard Rheology
This is an implementation of the original rheology, that was used in the Geodynamic project.

### Remark to Standard Implementation
The standard implementation was not ported to the new structure, it is basically the same thing as it was before.
The only changes that are done are changes to meet the compatibility.
The reason for that is that the rheology is a very low priority object.


