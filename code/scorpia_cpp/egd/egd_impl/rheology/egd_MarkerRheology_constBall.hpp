#pragma once
/**
 * \brief	This rheology will apply a constant velcity to the ball.
 *
 * It builds on the sin pulled ball sczenario.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_MarkerRehology_interface.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_constBall.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_MarkerRehologyConstBall_t
 * \brief	This is a rehology that implements a constant moving ball.
 *
 * The ball moves with constant velocity in a specified direction.
 * It can be specified by the configuration file, see the constructors.
 *
 * It is possible by setting the key "imposeVel" to false, to deactivate
 * the imposing of intrinsic marker velocities.
 *
 */
class egd_MarkerRehologyConstBall_t final : public egd_MarkerRehology_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t 			= egd_MarkerRehology_i;

	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using Base_t::DeviatoricStress_t;
	using Base_t::DeviatoricStrainRate_t;
	using Base_t::MarkerRehology_ptr;
	using Base_t::MarkerProperty_t;
	using Base_t::GridProperty_t;

	using AssocSetUp_t = egd::egd_markerSetUpConstBall_t;		//!< This is the associated marker setup object.

	/*
	 * Brief	This are the name of the different materials we have.
	 */
	const static Index_t TY_STICKY_AIR 	= AssocSetUp_t::TY_STICKY_AIR;
	const static Index_t TY_BALL            = AssocSetUp_t::TY_BALL;
	const static Index_t TY_N 		= AssocSetUp_t::TY_N;


	/*
	 * ======================
	 * Public Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_MarkerRehologyConstBall_t();


	/**
	 * \brief	This is the building constructor.
	 *
	 * Allows to specify the velocity of the ball.
	 *
	 *
	 * \param  Vy		Speed in Y direction.
	 * \param  Vx 		Speed in X direction.
	 * \param  imposeVel	Impose velocity
	 * \param  confObj	Optional ponter like parameter to construct *this.
	 */
	egd_MarkerRehologyConstBall_t(
		const Numeric_t 		Vy,
		const Numeric_t 		Vx,
		const bool 			imposeVel,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	This is the building constructor that
	 * 		 operates on config objects.
	 *
	 * This will read in the setup section of the ini file
	 * The keys are Vx and Vy.
	 * Note that the pulled ball setup has different keys.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_MarkerRehologyConstBall_t(
		const egd_confObj_t&		confObj);


	/**
	 * \brief	Default constructor
	 *
	 * Like calling the building constructor with the
	 * default values. (alpha = omega = 1).
	 */
	egd_MarkerRehologyConstBall_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyConstBall_t(
		const egd_MarkerRehologyConstBall_t&)
	 noexcept;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyConstBall_t&
	operator= (
		const egd_MarkerRehologyConstBall_t&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyConstBall_t(
		egd_MarkerRehologyConstBall_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyConstBall_t&
	operator= (
		egd_MarkerRehologyConstBall_t&&)
	 noexcept;


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function performs the material change.
	 *
	 * This function returns imediatly.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  nodeStress 	This is the stress at the "nodes".
	 * \param  nodeStrain 	This is the strain at the "nodes".
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	modifyMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const DeviatoricStress_t& 	nodeStress,
		const DeviatoricStrainRate_t&	nodeStrain,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/**
	 * \brief	This function will inspect the dumper.
	 *
	 * This function will configure the dumper to ignore several
	 * things. This is mostly done to takle the otherwise large
	 * dump files that would be created.
	 *
	 * It will disable the dumping of of the velocity marker
	 * properties. This is done, because they are not realy neaded
	 * and in extreme situations they could also be computed again.
	 *
	 * It will not ignore grid properties. The reason for that is that
	 * they can come in handy and does need way less storage than
	 * marker properties.
	 *
	 * *this will also ignore the strain property, so only stress can
	 * be configured by the ini file. Also the temperature setting
	 * will be disabled.
	 *
	 * \param  dumper	The dumper object.
	 */
	virtual
	void
	inspectDumper(
		egd_dumperFile_t& 	dumper)
	 const
	 override;


	/*
	 * ====================
	 * Querring
	 */
public:
	/**
	 * \brief	This function allows to test which initial value *this
	 * 		 can give.
	 *
	 * This function is implemented only for the ball and the velocities.
	 *
	 * \param  pIdx		The property in question.
	 * \param  mType	Material index.
	 */
	virtual
	bool
	hasInitialValue(
		const PropIdx_t& 	pIdx,
		const Int_t 		mType)
	 const
	 override;


	/**
	 * \brief	This function returns the initial properties.
	 *
	 * It is possible only for the ball to retrive initial properties.
	 * Also only for the two velocities it is possible to retrive them.
	 *
	 * \param  pIdx		The property in question.
	 * \param  mType	Material index.
	 */
	virtual
	Numeric_t
	getInitialValue(
		const PropIdx_t& 	pIdx,
		const Int_t 		mType)
	 const
	 override;


	/*
	 * =====================
	 * Private Members
	 */
private:
	Numeric_t 	m_velX		= NAN;			//!< Velocity in x direction.
	Numeric_t 	m_velY 		= NAN;			//!< Velocity in y direction.
	bool 		m_imposeVel     = true;			//!< Impose Velocity.
}; //End class(egd_MarkerRehologyConstBall_t)

PGL_NS_END(egd)

