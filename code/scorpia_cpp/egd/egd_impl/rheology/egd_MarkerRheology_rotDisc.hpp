#pragma once
/**
 * \brief	This rhehology is made to rotate a disc at constant speed.
 *
 * It is intended for the rotating class of luids.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_MarkerRehology_interface.hpp>

#include <egd_impl/setup/egd_MarkerSetUp_rotatingDiskTC.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_MarkerRehologyRotDiscTC_t
 *
 * This class will set the velocity of the inner disc marker to
 * a constant rotating speed with angular vel x.
 * It is assumed that the disc is rotating around the origin.
 *
 * Velocity of the outer fluid is set to zero.
 */
class egd_MarkerRehologyRotDiscTC_t final : public egd_MarkerRehology_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t 			= egd_MarkerRehology_i;

	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using Base_t::DeviatoricStress_t;
	using Base_t::DeviatoricStrainRate_t;
	using Base_t::MarkerRehology_ptr;
	using Base_t::MarkerProperty_t;
	using Base_t::GridProperty_t;

	using AssocSetUp_t = egd::egd_markerSetUpRotDiscTC_t;		//!< The associated setup.

	/*
	 * Brief	This are the name of the different materials we have.
	 */
	const static Index_t TY_STICKY_AIR 	= AssocSetUp_t::TY_STICKY_AIR;
	const static Index_t TY_DISC            = AssocSetUp_t::TY_DISC;
	const static Index_t TY_WALL            = AssocSetUp_t::TY_WALL;
	const static Index_t TY_OUT_FLUID 	= AssocSetUp_t::TY_OUT_FLUID;
	const static Index_t TY_N 		= AssocSetUp_t::TY_N;


	/*
	 * ======================
	 * Public Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_MarkerRehologyRotDiscTC_t();


	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor allows to set the calling factor and
	 * omega.
	 *
	 * \param  rotVel 	The supposed rotational velocity of the disc.
	 * \param  outVelOut 	Rotation speed of the outer cylinder.
	 * \param  imposeVel	Should velocity be imposed.
	 */
	egd_MarkerRehologyRotDiscTC_t(
		const Numeric_t 		rotVel,
		const Numeric_t 		rotVelOut,
		const bool 			imposeVel,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	This is the building constructor that
	 * 		 operates on config objects.
	 *
	 * Note that values for alpha and omega are read from teh
	 * SetUP section. If not specified default values are used.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_MarkerRehologyRotDiscTC_t(
		const egd_confObj_t&		confObj);


	/**
	 * \brief	Default constructor
	 *
	 * Like calling the building constructor with the
	 * default values. (alpha = omega = 1).
	 */
	egd_MarkerRehologyRotDiscTC_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyRotDiscTC_t(
		const egd_MarkerRehologyRotDiscTC_t&)
	 noexcept;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyRotDiscTC_t&
	operator= (
		const egd_MarkerRehologyRotDiscTC_t&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyRotDiscTC_t(
		egd_MarkerRehologyRotDiscTC_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyRotDiscTC_t&
	operator= (
		egd_MarkerRehologyRotDiscTC_t&&)
	 noexcept;


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function performs the material change.
	 *
	 * This function returns imediatly.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  nodeStress 	This is the stress at the "nodes".
	 * \param  nodeStrain 	This is the strain at the "nodes".
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	modifyMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const DeviatoricStress_t& 	nodeStress,
		const DeviatoricStrainRate_t&	nodeStrain,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/**
	 * \brief	This function will inspect the dumper.
	 *
	 * This function will configure the dumper to ignore several
	 * things. This is mostly done to takle the otherwise large
	 * dump files that would be created.
	 *
	 * It will disable the dumping of of the velocity marker
	 * properties. This is done, because they are not realy neaded
	 * and in extreme situations they could also be computed again.
	 *
	 * It will not ignore grid properties. The reason for that is that
	 * they can come in handy and does need way less storage than
	 * marker properties.
	 *
	 * *this will also ignore the strain property, so only stress can
	 * be configured by the ini file. Also the temperature setting
	 * will be disabled.
	 *
	 * \param  dumper	The dumper object.
	 */
	virtual
	void
	inspectDumper(
		egd_dumperFile_t& 	dumper)
	 const
	 override;


	/*
	 * ====================
	 * Querring
	 * All default.
	 */
public:
	using Base_t::hasInitialValue;
	using Base_t::getInitialValue;


	/*
	 * =====================
	 * Private Members
	 */
private:
	Numeric_t 	m_angVel        = NAN;			//!< Supposed angular velocity of the inner disc.
	Numeric_t 	m_angVelOut 	= NAN;			//!< SUpposed angular velocity of the outer disc.
	bool 		m_imposeVel     = true;			//!< Should we impose the velocity.
}; //End class(egd_MarkerRehologyRotDiscTC_t)

PGL_NS_END(egd)

