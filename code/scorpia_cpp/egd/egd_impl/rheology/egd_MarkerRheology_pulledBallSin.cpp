/**
 * \brief	Tghis file contains the code for the pulled marker rehology.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerRheology_pulledBallSin.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)


void
egd_MarkerRehologyPulledBallSin_t::modifyMarkers(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const DeviatoricStress_t& 	nodeStress,
	const DeviatoricStrainRate_t&	nodeStrain,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
using ::pgl::isValidFloat;
using ::std::sin;
		pgl_assert(mColl_ != nullptr);	//make an alias
	MarkerCollection_t& mColl = *mColl_;
		pgl_assert(isValidFloat(thisDt), isValidFloat(currTime),
			   thisDt > 0.0, tStepIdx >= 0);
		pgl_assert((tStepIdx != 0) == isValidFloat(this->m_t0));
		pgl_assert(mColl.hasIndexMap());

	//Test if we ghave to set the start time index
	if(tStepIdx == 0)
	{
			pgl_assert(isValidFloat(this->m_t0) == false);
		this->m_t0 = currTime;
	}; //End if: set the start time
		pgl_assert(isValidFloat(this->m_t0));

	/* check if no imposing is requested */
	if(this->m_imposeVel == false)
	{
		return;
	};


	//CVaculate the time point which we use to compute the velcoities
	const Numeric_t nextTime = currTime;

	//now we calculate the velocity value
	const Numeric_t velX = (-1.0) * ((this->m_alpha) * (sin((this->m_omegaPrime) * (nextTime - (this->m_t0))) + 1.0));
		pgl_assert(isValidFloat(velX), velX <= 0.0);

	//Now get the marker properties
	      MarkerProperty_t& VELX = mColl.getMarkerProperty(PropIdx::VelX() );
	      MarkerProperty_t& VELY = mColl.getMarkerProperty(PropIdx::VelY() );
	const MarkerProperty_t& TYPE = mColl.getMarkerProperty(PropIdx::Type() );


	/*
	 * Go through all trhe markers and set teh velocity where needed
	 */
	for(const Index_t m : mColl.getIndexes(TY_BALL))
	{
			pgl_assert(0 <= m, m < TYPE.size()    ,
				   TY_BALL == Index_t(TYPE[m]) );
		VELX[m] = velX;
		VELY[m] = 0.0;
	}; //End for(m):

	return;
	(void)grid;
	(void)mSol;
	(void)tSol;
	(void)nodeStress;
	(void)nodeStrain;
}; //End: modify the marker properties.


std::string
egd_MarkerRehologyPulledBallSin_t::print()
 const
{
	return (std::string("PulledBall Rehology")
			+ " \\alpha = " + std::to_string(this->m_alpha)
			+ " \\omega = " + std::to_string(2 * M_PI / this->m_omegaPrime)
			+ " imposeVel = " + (this->m_imposeVel ? std::string("Yes") : std::string("No"))
		);
}; //End: print function


void
egd_MarkerRehologyPulledBallSin_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	//Remove the velocity from the set of constant marker properties
	if(dumper.isInConstMarkerSet(PropIdx::VelX()) )
		{ dumper.rmConstantMarkerProperty(PropIdx::VelX() );};
	if(dumper.isInConstMarkerSet(PropIdx::VelY()) )
		{ dumper.rmConstantMarkerProperty(PropIdx::VelY() );};

	//If the imposing of the velocities is deactivated, we do not add the
	//intrinsic marker velocity to the list of ignored property.
	//This is then handled by the ini file.
	if(this->m_imposeVel == true)
	{
		dumper.ignoreMarkerVel();
			pgl_assert(dumper.markerVelAreDumped() == false);
	};

	//Do not store the temperature solution
	dumper.noTempSol()
	      .onlyFullHeatingTerm();

#	if 0
	//Add all but the type property and the velocities to the ignored
	//set, haveing the type property will allow us to visualize
	dumper.ignoreGridProp(PropIdx::Viscosity() ).
	       ignoreGridProp(PropIdx::Density()   );
# 	endif

	//Also dump only representative
	dumper.onlyDumpRepOnGrid();

	//Ignore onyl the strain, stress can be ignored by ini file.
	dumper.noStrainRateDump();

	return;
}; //End: inspect dumper


/*
 * ========================
 * Quriing
 */
egd_MarkerRehologyPulledBallSin_t::Numeric_t
egd_MarkerRehologyPulledBallSin_t::getInitialValue(
	const PropIdx_t& 	pIdx,
	const Int_t 		mType)
 const
{
using ::pgl::isValidFloat;
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(pIdx.isRepresentant() == false)
	{
		throw PGL_EXCEPT_InvArg("Passes a property that is not a representant, passed " + pIdx);
	};
	if(mType != TY_BALL)
	{
		throw PGL_EXCEPT_InvArg("Passed an unkown marker type " + std::to_string(mType));
	};

		pgl_assert(isValidFloat(this->m_alpha),
			   isValidFloat(this->m_omegaPrime) );
			   //t_0 is not yet knwon for sure.

	switch(pIdx.getProp())
	{
	  case eMarkerProp::VelX:
	  	return (-(this->m_alpha)); //sin is zero so only the +1 survivesm - in front for accounting.
	  case eMarkerProp::VelY:
	  	return 0.0;	      //is zero

	  default:
		throw PGL_EXCEPT_InvArg("Does not known an initial value for " + pIdx);
	}; //end switch(pIdx)

		pgl_assert(false && "Unreachable code");
	return NAN;
}; //End: getInitial value


bool
egd_MarkerRehologyPulledBallSin_t::hasInitialValue(
	const PropIdx_t& 	pIdx,
	const Int_t 		mType)
 const
{
using ::pgl::isValidFloat;
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(pIdx.isRepresentant() == false)
	{
		throw PGL_EXCEPT_InvArg("Passes a property that is not a representant, passed " + pIdx);
	};
	if(mType != TY_BALL)
	{
		throw PGL_EXCEPT_InvArg("Passed an unkown marker type " + std::to_string(mType));
	};

		pgl_assert(isValidFloat(this->m_alpha),
			   isValidFloat(this->m_omegaPrime) );
			   //t_0 is not yet knwon for sure.

	switch(pIdx.getProp())
	{
	  case eMarkerProp::VelX:
	  case eMarkerProp::VelY:
	  	  return true;

	  default:
	  	  return false;
	}; //end switch(pIdx)

	return false;
}; //End: hasInitial value

/*
 * ========================
 * Building constructors
 */

egd_MarkerRehologyPulledBallSin_t::egd_MarkerRehologyPulledBallSin_t(
	const Numeric_t 		alpha,
	const Numeric_t 		omega,
	const bool 			imposeVel,
	const egd_confObj_t* const 	confObj)
 :
  Base_t()
{
using ::pgl::isValidFloat;
	if((isValidFloat(alpha) == false) ||
           (alpha               <= 0.0  )   )
	{
		throw PGL_EXCEPT_InvArg("The passed alpha value of " + std::to_string(alpha) + " is invalid.");
	};
	if((isValidFloat(omega) == false) ||
           (omega               <= 0.0  )   )
	{
		throw PGL_EXCEPT_InvArg("The passed omega value of " + std::to_string(omega) + " is invalid.");
	};

	/* setting the value */
	this->m_imposeVel = imposeVel;
	this->m_alpha = alpha;
	this->m_omegaPrime = 2.0 * M_PI / omega;	// this will get us omega prime
		pgl_assert(isValidFloat(this->m_omegaPrime));

	PGL_UNUSED(confObj);
}; //End building constructor


egd_MarkerRehologyPulledBallSin_t::egd_MarkerRehologyPulledBallSin_t()
 :
  egd_MarkerRehologyPulledBallSin_t(1.0, 1.0, true, nullptr)
{};


egd_MarkerRehologyPulledBallSin_t::egd_MarkerRehologyPulledBallSin_t(
	const egd_confObj_t& 		confObj)
 :
  egd_MarkerRehologyPulledBallSin_t(
  		confObj.setUpPar("alpha", 1.0),
  		confObj.setUpPar("omega", 1.0),
  		confObj.rheologyImposeVel(),
  		&confObj)
{};


/*
 * ========================
 * Defaulted constructors
 */
egd_MarkerRehologyPulledBallSin_t::~egd_MarkerRehologyPulledBallSin_t()
 = default;




egd_MarkerRehologyPulledBallSin_t::egd_MarkerRehologyPulledBallSin_t(
	const egd_MarkerRehologyPulledBallSin_t&)
 noexcept
 = default;


egd_MarkerRehologyPulledBallSin_t&
egd_MarkerRehologyPulledBallSin_t::operator= (
	const egd_MarkerRehologyPulledBallSin_t&)
 noexcept
 = default;


egd_MarkerRehologyPulledBallSin_t::egd_MarkerRehologyPulledBallSin_t(
	egd_MarkerRehologyPulledBallSin_t&&)
 noexcept
 = default;


egd_MarkerRehologyPulledBallSin_t&
egd_MarkerRehologyPulledBallSin_t::operator= (
	egd_MarkerRehologyPulledBallSin_t&&)
 noexcept
 = default;

PGL_NS_END(egd)

