/**
 * \brief	This file contains the construction fucntiuons of the
 * 		 standard rheology.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_MarkerRehology_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include "./egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

namespace
{

/**
 * \brief	This function sets all rocks entries to the given value
 *
 * \param  pVectore	The parameter vector that should be maipulated.
 * \param  valueDry	The value that should be used, for the not mantle rocks.
 * \param  valueWet	This is the value of the mantel.
 * \param  airValue 	Which value should be assigned to the air.
 * 			 If omitted nan will be used.
 */
void
setParamVec(
	egd_markerRheologyOrgProj_t::ParamVector_t* const 	pVector,
	egd_markerRheologyOrgProj_t::Numeric_t 			valueDry,
	egd_markerRheologyOrgProj_t::Numeric_t 			valueWet,
	egd_markerRheologyOrgProj_t::Numeric_t 			airValue = NAN)
{
	pgl_assert(pVector != nullptr);
	pgl_assert(::pgl::isValidFloat(valueWet),
		   ::pgl::isValidFloat(valueDry) );
	pgl_assert(pVector->size() == egd_markerRheologyOrgProj_t::TY_N);

	for(Index_t it  = egd_markerRheologyOrgProj_t::TY_ROCK_START;
		    it != egd_markerRheologyOrgProj_t::TY_END       ;
		  ++it 						     )
	{
		//Do not handle the mantle here.
		if(it == egd_markerRheologyOrgProj_t::TY_ROCK_MANTLE)
		{
			continue;
		};

		//Set the value, with the exception it is the mantle
		(*pVector)[it] = valueDry;
	}; //End for(it):

	//Set the mantel
	(*pVector)[egd_markerRheologyOrgProj_t::TY_ROCK_MANTLE] = valueWet;

	//Set the air value if needed
	if(::pgl::isValidFloat(airValue) == true)
	{
		(*pVector)[egd_markerRheologyOrgProj_t::TY_STICKY_AIR] = airValue;
	}
	else
	{
		(*pVector)[egd_markerRheologyOrgProj_t::TY_STICKY_AIR] = NAN;
	};

	return;
}; //End: setParamVec

}; //End: Ananymus namespace


void
egd_markerRheologyOrgProj_t::priv_setUpConstants()
{
	/*
	 * Setting The values.
	 * Beside the m_CP nothing is needed for the air.
	 * Since the air is set initially and will not modify.
	 */
	setParamVec(&m_Cp      , 1'000  , 1'000  , 3'300'000);
	setParamVec(&m_rho0    , 3'400  , 3'300             );
	setParamVec(&m_etaAD   , 2.5e-17, 2e-21             );
	setParamVec(&m_etaN    , 3.5    , 4.0               );
	setParamVec(&m_etaVa   , 8e-6   , 4e-6              );
	setParamVec(&m_etaEa   , 532'000, 471'000           );
	setParamVec(&m_etaMIN  , 1e+18  , 1e+18             );
	setParamVec(&m_etaMAX  , 1e+24  , 1e+24             );
	setParamVec(&m_strength, 1e+8   , 5e+7              );
	m_strength[TY_ROCK_NECKING] = 2e+7;	//Weakening the necking

	return;
}; //End: setup of constants


egd_markerRheologyOrgProj_t::egd_markerRheologyOrgProj_t(
	const egd_confObj_t& 		confObj)
 :
  egd_markerRheologyOrgProj_t()		//Sets up the internals
{
	/*
	 * We do not need anything else
	 */
	pgl_assert(confObj.getINI().hasKeys());
	(void)confObj;
}; //End: conf builder constructor



egd_markerRheologyOrgProj_t::~egd_markerRheologyOrgProj_t()
 = default;


egd_markerRheologyOrgProj_t::egd_markerRheologyOrgProj_t()
{
	//Set up the constants
	this->priv_setUpConstants();
}; //End default impl


egd_markerRheologyOrgProj_t::egd_markerRheologyOrgProj_t(
	const egd_markerRheologyOrgProj_t&)
 = default;


egd_markerRheologyOrgProj_t&
egd_markerRheologyOrgProj_t::operator= (
	const egd_markerRheologyOrgProj_t&)
 = default;


egd_markerRheologyOrgProj_t::egd_markerRheologyOrgProj_t(
		egd_markerRheologyOrgProj_t&&)
 noexcept
 = default;


egd_markerRheologyOrgProj_t&
egd_markerRheologyOrgProj_t::operator= (
		egd_markerRheologyOrgProj_t&&)
 noexcept
 = default;


PGL_NS_END(egd)











