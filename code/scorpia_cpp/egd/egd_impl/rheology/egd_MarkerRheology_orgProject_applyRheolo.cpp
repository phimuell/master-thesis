/**
 * \brief	This function does apply the rheology.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_MarkerRehology_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include "./egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)


void
egd_markerRheologyOrgProj_t::modifyMarkers(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const DeviatoricStress_t& 	nodeStress,
	const DeviatoricStrainRate_t&	nodeStrain,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;	//Allias the map

	if(mColl.isNewVersion() )
	{
		throw PGL_EXCEPT_illMethod("The rehology can not handle changed markers.");
	};
	if(grid.isNewVersion() )
	{
		throw PGL_EXCEPT_illMethod("The rehology can not handle a changed geometry.");
	};

	/*
	 * First we have to updsate the internal structures of *this
	 */
	this->priv_setUpInternals(grid, mColl, mSol, nodeStrain);


	/*
	 * Update the markers
	 */
	this->priv_updateViscosity          (&mColl);
	this->priv_updateDensity            (&mColl);
	this->priv_updateThermalConductivity(&mColl);

	//End
	return;

	(void)tSol;
	(void)nodeStress;
	(void)thisDt;
	(void)tStepIdx;
	(void)currTime;
}; //End: modiy markers




PGL_NS_END(egd)






