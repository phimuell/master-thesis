/**
 * \brief	This file contains the functions that are declared by the rehology interface.
 * 		 This are primarly the constructors.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerRheologyNULL.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)




void
egd_MarkerRehologyNull_t::modifyMarkers(
	MarkerCollection_t* const 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const DeviatoricStress_t& 	nodeStress,
	const DeviatoricStrainRate_t&	nodeStrain,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
		pgl_assert(::pgl::isValidFloat(thisDt), thisDt > 0.0);

	return;
	(void)mColl;
	(void)grid;
	(void)mSol;
	(void)tSol;
	(void)nodeStress;
	(void)nodeStrain;
	(void)thisDt;
	(void)tStepIdx;
	(void)currTime;
};


std::string
egd_MarkerRehologyNull_t::print()
 const
{
	return std::string("Null Rehology.");
};


egd_MarkerRehologyNull_t::egd_MarkerRehologyNull_t(
	const egd_confObj_t& 		confObj)
 noexcept
 :
  egd_MarkerRehologyNull_t()
{
	(void)confObj;
}


egd_MarkerRehologyNull_t::~egd_MarkerRehologyNull_t()
 = default;


egd_MarkerRehologyNull_t::egd_MarkerRehologyNull_t()
 noexcept
 = default;


egd_MarkerRehologyNull_t::egd_MarkerRehologyNull_t(
	const egd_MarkerRehologyNull_t&)
 noexcept
 = default;


egd_MarkerRehologyNull_t&
egd_MarkerRehologyNull_t::operator= (
	const egd_MarkerRehologyNull_t&)
 noexcept
 = default;


egd_MarkerRehologyNull_t::egd_MarkerRehologyNull_t(
	egd_MarkerRehologyNull_t&&)
 noexcept
 = default;


egd_MarkerRehologyNull_t&
egd_MarkerRehologyNull_t::operator= (
	egd_MarkerRehologyNull_t&&)
 noexcept
 = default;

PGL_NS_END(egd)

