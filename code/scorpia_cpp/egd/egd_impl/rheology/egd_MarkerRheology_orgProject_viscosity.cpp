/**
 * \brief	This file calculates the viscosity.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_MarkerRehology_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include "./egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)

void
egd_markerRheologyOrgProj_t::priv_updateViscosity(
	MarkerCollection_t* const 	mColl_)
 const
{
	using pgl::isValidFloat;

	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;	//Create an alias of the collection.

	//Load the number of markers
	const auto nMarkers = mColl.nMarkers();
		pgl_assert(nMarkers >  0                ,
			   nMarkers == m_dEpsII  .size(),
			   nMarkers == m_pressure.size() );

	/*
	 * The formulat for the viscosity is given as:
	 * 	\eta_d = \frac{0.5}{AD^{1/n}} * (\epsilon_{II})^{1/n - 1} * \Exp{\frac{Ea + P * Va}{8.314 * T * n}}
	 *
	 * Where basically everything depends on the marker type.
	 *
	 * The formula above is the ductile viscosity.
	 * However there is also a bulk viscosity. that is calculated as
	 * 	\eta_b = \frac{\sigma_{yield}}{2 \epsilon_{II}}
	 *
	 * We take the smaller of both (WHY? whx not average?).
	 */

	//Load the variables
	const MarkerProperty_t& T      = mColl.cgetMarkerProperty(PropIdx::Temperature() );
	const MarkerProperty_t& P      = m_pressure                                       ;
	const MarkerProperty_t& dEPSII = m_dEpsII                                         ;
		pgl_assert(nMarkers == dEPSII.size(),
			   nMarkers ==      T.size(),
			   nMarkers ==      P.size() );

	//This is the target variable
	MarkerProperty_t& ETA = mColl.getMarkerProperty(PropIdx::Viscosity() );
		pgl_assert(nMarkers == ETA.size());

	//This is a counter that counts how many updates where done
	uSize_t nUpdates = 0;


	/*
	 * Iterate over the rocks
	 */
	for(Index_t currType = TY_ROCK_START; currType != TY_END; ++currType)
	{
		//Get a reference to the index and the type
		const MarkerIndexes_t& markerList = mColl.cgetIndexes(currType);
			pgl_assert(markerList.size() > 0);

		//
		//Load parameters of the equation
		const Numeric_t AD = m_etaAD[currType];
		const Numeric_t n  = m_etaN [currType];
		const Numeric_t Ea = m_etaEa[currType];
		const Numeric_t Va = m_etaVa[currType];
			pgl_assert(isValidFloat(AD),
				   isValidFloat(n ),
				   isValidFloat(Ea),
				   isValidFloat(Va) );

		//Load the yielding stress
		const Numeric_t yieldStress = m_strength[currType];
			pgl_assert(isValidFloat(yieldStress), yieldStress > 0.0);

		//load the eta range
		const Numeric_t ETA_MAX = m_etaMAX[currType];
		const Numeric_t ETA_MIN = m_etaMIN[currType];
			pgl_assert(isValidFloat(ETA_MAX),
				   isValidFloat(ETA_MIN),
				   ETA_MIN < ETA_MAX    ,
				   ETA_MIN > 0           );

		//
		//Precalculate some values

		//this is the first part: 0.5 / (AD^{1/n})
		const Numeric_t firstTerm = 0.5 / std::pow(AD, 1.0 / n);
			pgl_assert(isValidFloat(firstTerm));

		//This part of the exponent denumerator
		const Numeric_t expDenumeratorPart = 8.314 * n;
			pgl_assert(isValidFloat(expDenumeratorPart));

		//This is the exponent of the invariant
		const Numeric_t dEPSII_exp = (1.0 / n) - 1.0;
			pgl_assert(isValidFloat(dEPSII_exp));

		//this is half of the hielding stress
		const Numeric_t yieldStressHalf = yieldStress / 2.0;


		//Iterate over all markers in the current set and compute the new viscosity
		for(const auto& m : markerList)
		{
				pgl_assert(m >= 0, m < nMarkers);

			//Load marker specific data
			const Numeric_t      P_m = P[m];
			const Numeric_t      T_m = T[m];
			const Numeric_t dEPSII_m = dEPSII[m];

			//Compute the new ductile viscossity
			const Numeric_t newViscos_duc = firstTerm * std::pow(dEPSII_m, dEPSII_exp) * std::exp((Ea + P_m * Va) / (expDenumeratorPart * T_m));
				pgl_assert(pgl::isValidFloat(newViscos_duc), newViscos_duc > 0.0);

			//Calculate the bulk viscosity
			const Numeric_t newViscos_bul = yieldStressHalf / dEPSII_m;
				pgl_assert(isValidFloat(newViscos_bul), newViscos_bul > 0.0);

			//Take the smaller of the twos (figuring out why)
			const Numeric_t newViscos_taken = std::min(newViscos_bul, newViscos_duc);

			//Now clamp the result in a range
			const Numeric_t newViscos_fin = std::min(ETA_MAX, std::max(ETA_MIN, newViscos_taken));

			//Write the result back
			ETA[m] = newViscos_fin;

			//Increase the counter for updates
			nUpdates += 1;
		}; //End for(m):
	}; //End for(mClasses):
		pgl_assert(ETA.array().isFinite().all());

	return;
}; //End: update density


PGL_NS_END(egd)











