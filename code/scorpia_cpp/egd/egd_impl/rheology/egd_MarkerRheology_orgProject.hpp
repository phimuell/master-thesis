#pragma once
/**
 * \brief	This is the "standard" marker rheology implementation.
 *
 * This it is not named "standard" because rheologies, in generals,
 * are problem specific. So is the rheology for the original project.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_MarkerRehology_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_markerRheologyOrgProj_t
 * \brief	This is is the rheology of the original project.
 *
 * This is the same rheology that was used in the project in the year 2017.
 * It makes several marker property dependend on the conditions.
 * See the project sheet for more information, you can find it in the
 * folder of the original code.
 *
 * Note that this class ignores the air and only modifies the rock markers.
 */
class egd_markerRheologyOrgProj_t : public egd_MarkerRehology_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using MarkerCollection_t 	= egd_markerCollection_t;	//!< This is the marker collection.
	using GridContainer_t 		= egd_gridContainer_t;		//!< This is the grid container.
	using GridGeometry_t 		= GridContainer_t::GridGeometry_t;	//!< This is the grid geometry.

	using TempSolverResult_t 	= egd_tempSolverResult_t;	//!< This is the result container for the temperature solver.
	using MechSolverResult_t 	= egd_mechSolverResult_t;	//!< This is the result container for the mechanical solver.
	using DeviatoricStress_t 	= egd_deviatoricStress_t;	//!< This is the container for the deviatoric stress.
	using DeviatoricStrainRate_t 	= egd_deviatoricStrainRate_t;	//!< This is the container for the deviatoric strain rate.

	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;	//!< This is the type that stores a marker property.
	using MarkerIndexes_t 		= MarkerCollection_t::IndexVector_t;	//!< Ist of all markers of a certain type.

	using GridProperty_t 		= GridContainer_t::GridProperty_t;	//!< This is a grid property.


	/*
	 * =======================
	 * Material Types
	 * This are IDs for the different material types.
	 * They are indexed, such that they can be used to
	 * index an array.
	 *
	 * Note that the material index, that is stored inside
	 * the marker collection is a double (because of uniform
	 * access) it will be casted to an int to get the real
	 * type.
	 */
public:
	const static Index_t TY_STICKY_AIR 	= 0;		//!< Index for the sticky air.
	const static Index_t TY_ROCK_PLATE 	= 1;		//!< This is the top plate, our ground so to speak.
	const static Index_t TY_ROCK_START 	= TY_ROCK_PLATE;//!< This is the first index that points to a valid rock type.
	const static Index_t TY_ROCK_NECKING	= 2;		//!< This is the weak spot of the slap.
	const static Index_t TY_ROCK_THORN 	= 3;		//!< This is the rest of the spap.
	const static Index_t TY_ROCK_MANTLE	= 4;		//!< This is the mantle of the earth.
	const static Index_t TY_END 		= 5;		//!< This is the past the end index and the number of the different types.
	const static Index_t TY_N 		= TY_END;	//!< This is an alias of TY_END, consider it as syntactic sugar.

	using ParamVector_t 			= egd_Vector_t<Numeric_t, TY_N>;	//!< This is a vector that is used o store variables for the rehology.


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markerRheologyOrgProj_t();


	/**
	 * \brief	Building Constructor.
	 *
	 * Thsi constructor is requiered by the build
	 * processm but since nothing needs to be read
	 * or set thsi constructor will simply call the
	 * default one and the argument will be ignored.
	 *
	 * \param  confObj	The Configuration object.
	 */
	egd_markerRheologyOrgProj_t(
		const egd_confObj_t& 	confObj);


	/**
	 * \brief	Default constructor and building constructor.
	 *
	 * THis is the" true building constructor. It will set up the
	 * internal constants. Since this class does not allows to
	 * change some parameters it is the only one that is supported.
	 */
	egd_markerRheologyOrgProj_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_markerRheologyOrgProj_t(
		const egd_markerRheologyOrgProj_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_markerRheologyOrgProj_t&
	operator= (
		const egd_markerRheologyOrgProj_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerRheologyOrgProj_t(
		egd_markerRheologyOrgProj_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerRheologyOrgProj_t&
	operator= (
		egd_markerRheologyOrgProj_t&&)
	 noexcept;


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function performs the material change.
	 *
	 * This function is called before the marker are moved.
	 * All properties have already been interpolated back,
	 * to the current location of the markers.
	 *
	 * This function is now allowed to modify them.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	modifyMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const DeviatoricStress_t& 	nodeStress,
		const DeviatoricStrainRate_t&	nodeStrain,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function shoud at least name the concrete instance
	 * of *this and if it is aprameterized also output them.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/*
	 * ====================
	 * Internal Functions
	 */
private:
	/*
	 * \brief	This function sets *this up for the computation.
	 *
	 * This function will set up the internal temporaries of *this.
	 * This is the computation of the pressure at the maker location
	 * and the second invariante of the deviatoric strain rate.
	 *
	 * \param  grid		This is the grid.
	 * \param  mColl 	This is the marker collection.
	 * \param  mechSol	This is the mechanical solution.
	 * \param  strainRate	This is the strain rate that was computed.
	 */
	void
	priv_setUpInternals(
		const GridContainer_t& 		grid,
		const MarkerCollection_t& 	mColl,
		const MechSolverResult_t& 	mechSol,
		const DeviatoricStrainRate_t&	strainRate);


	/**
	 * \brief	This function sets up the internal state.
	 *
	 * It will sets all the constants that are needed.
	 */
	void
	priv_setUpConstants();


	/**
	 * \brief	This function handles the updateing of the density.
	 *
	 * This function will update the density of the markers.
	 * The formula is given on the project sheet, and can be found
	 * in the cpp file, that implements this functionality.
	 *
	 * This function assumes that the temperature of the marker was updated.
	 *
	 * This function will also update the RhoCP value.
	 *
	 * \param mColl		This is the marker collection.
	 */
	void
	priv_updateDensity(
		MarkerCollection_t* const 	mColl)
	 const;


	/**
	 * \brief	This function updates the thermal conductivity,
	 * 		 also known as "K".
	 *
	 * This function assumes tha  the temperatiure was updated before.
	 *
	 * \param  mColl 	This is the marker collection.
	 */
	void
	priv_updateThermalConductivity(
		MarkerCollection_t* const 	mColl)
	 const;


	/**
	 * \brief	This function computes the new viscosity.
	 *
	 * This function asslumes that the temperature was updated
	 * and also the internal structures of this too.
	 *
	 * \param  mColl	The marker collection.
	 */
	void
	priv_updateViscosity(
		MarkerCollection_t* const 	mColl)
 	 const;



	/*
	 * =====================
	 * Private Memebers
	 */
private:
	ParamVector_t 		m_Cp;		//!< This vector contains the value of Cp, this is constant.
	ParamVector_t		m_rho0;		//!< This is the rho 0 value; meaning only for rocks.
	ParamVector_t 		m_etaAD;	//!< The AD value, needed for the viscosity; meaning only for rocks.
	ParamVector_t 		m_etaN;		//!< The n value, needed for the viscosity; meaning only for rocks.
	ParamVector_t 		m_etaVa;	//!< The Va value, needed for the viscosity; meaning only for rocks.
	ParamVector_t 		m_etaEa;	//!< The Ea value, needed for the viscosity; meaning only for rocks.
	ParamVector_t 		m_etaMIN;	//!< Minimal viscosity of rocks; only meaningfull for rocks.
	ParamVector_t 		m_etaMAX;	//!< Maximal viscosity of rocks; only meaningfull for rocks.
	ParamVector_t 		m_strength;	//!< This is the strength of the material; meaning only for rocks.

	/*
	 * Helper variables. These variables are used
	 * to store some temporary data.
	 */
	MarkerProperty_t 	m_dEpsII;	//!< This is the second invariante of the stress at the marker; Variable used for computation.
	MarkerProperty_t 	m_pressure;	//!< The pressure at the marker location; Variable used as temporary.
}; //End class(egd_markerRheologyOrgProj_t)




PGL_NS_END(egd)




