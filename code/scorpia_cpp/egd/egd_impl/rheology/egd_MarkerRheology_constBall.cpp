/**
 * \brief	Tghis file contains the code for the pulled marker rehology.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerRheology_constBall.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)


void
egd_MarkerRehologyConstBall_t::modifyMarkers(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const DeviatoricStress_t& 	nodeStress,
	const DeviatoricStrainRate_t&	nodeStrain,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
using ::pgl::isValidFloat;
using ::std::sin;
		pgl_assert(mColl_ != nullptr);	//make an alias
	MarkerCollection_t& mColl = *mColl_;
		pgl_assert(isValidFloat(thisDt), isValidFloat(currTime),
			   thisDt > 0.0, tStepIdx >= 0);
		pgl_assert(mColl.hasIndexMap());

	if(isValidFloat(this->m_velX) == false)
	{
		throw PGL_EXCEPT_InvArg("The x velcity is invalid.");
	};
	if(isValidFloat(this->m_velY) == false)
	{
		throw PGL_EXCEPT_InvArg("The y velocity is invalid.");
	};

	/* Check if no imposing of velocity was requested */
	if(this->m_imposeVel == false)
	{
		return;
	};

	//Now get the marker properties
	      MarkerProperty_t& VELX = mColl.getMarkerProperty(PropIdx::VelX() );
	      MarkerProperty_t& VELY = mColl.getMarkerProperty(PropIdx::VelY() );
	const MarkerProperty_t& TYPE = mColl.getMarkerProperty(PropIdx::Type() );


	/*
	 * Go through all trhe markers and set teh velocity where needed
	 */
	for(const Index_t m : mColl.getIndexes(TY_BALL))
	{
			pgl_assert(0 <= m, m < TYPE.size()    ,
				   TY_BALL == Index_t(TYPE[m]) );
		VELX[m] = this->m_velX;
		VELY[m] = this->m_velY;
	}; //End for(m):

	return;
	(void)grid;
	(void)mSol;
	(void)tSol;
	(void)nodeStress;
	(void)nodeStrain;
}; //End: modify the marker properties.


std::string
egd_MarkerRehologyConstBall_t::print()
 const
{
	return (std::string("ConstBall Rehology")
			+ " Vx = " + std::to_string(this->m_velX) + "m/s;"
			+ " Vy = " + std::to_string(this->m_velY) + "m/s"
			+ " imposeVel = " + (this->m_imposeVel ? std::string("Yes") : std::string("No"))
		);
}; //End: print function


void
egd_MarkerRehologyConstBall_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	//Remove the velocity from the set of constant marker properties
	if(dumper.isInConstMarkerSet(PropIdx::VelX()) )
		{ dumper.rmConstantMarkerProperty(PropIdx::VelX() );};
	if(dumper.isInConstMarkerSet(PropIdx::VelY()) )
		{ dumper.rmConstantMarkerProperty(PropIdx::VelY() );};

	//If the imposing of the velocities is deactivated, we do not add the
	//intrinsic marker velocity to the list of ignored property.
	//This is then handled by the ini file.
	if(this->m_imposeVel == true)
	{
		dumper.ignoreMarkerVel();
			pgl_assert(dumper.markerVelAreDumped() == false);
	};

	//Do not store the temperature solution
	dumper.noTempSol()
	      .onlyFullHeatingTerm();

#	if 0
	//Add all but the type property and the velocities to the ignored
	//set, haveing the type property will allow us to visualize
	dumper.ignoreGridProp(PropIdx::Viscosity() ).
	       ignoreGridProp(PropIdx::Density()   );
# 	endif

	//Also dump only representative
	dumper.onlyDumpRepOnGrid();

	//Ignore onyl the strain, stress can be ignored by ini file.
	dumper.noStrainRateDump();

	return;
}; //End: inspect dumper


/*
 * ========================
 * Quriing
 */
egd_MarkerRehologyConstBall_t::Numeric_t
egd_MarkerRehologyConstBall_t::getInitialValue(
	const PropIdx_t& 	pIdx,
	const Int_t 		mType)
 const
{
using ::pgl::isValidFloat;
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(pIdx.isRepresentant() == false)
	{
		throw PGL_EXCEPT_InvArg("Passes a property that is not a representant, passed " + pIdx);
	};
	if(mType != TY_BALL)
	{
		throw PGL_EXCEPT_InvArg("Passed an unkown marker type " + std::to_string(mType));
	};

		pgl_assert(isValidFloat(this->m_velX),
			   isValidFloat(this->m_velY) );

	switch(pIdx.getProp())
	{
	  case eMarkerProp::VelX:
	  	return (this->m_velX);
	  case eMarkerProp::VelY:
	  	return (this->m_velY);

	  default:
		throw PGL_EXCEPT_InvArg("Does not known an initial value for " + pIdx);
	}; //end switch(pIdx)

		pgl_assert(false && "Unreachable code");
	return NAN;
}; //End: getInitial value


bool
egd_MarkerRehologyConstBall_t::hasInitialValue(
	const PropIdx_t& 	pIdx,
	const Int_t 		mType)
 const
{
using ::pgl::isValidFloat;
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(pIdx.isRepresentant() == false)
	{
		throw PGL_EXCEPT_InvArg("Passes a property that is not a representant, passed " + pIdx);
	};
	if(mType != TY_BALL)
	{
		throw PGL_EXCEPT_InvArg("Passed an unkown marker type " + std::to_string(mType));
	};

		pgl_assert(isValidFloat(this->m_velX),
			   isValidFloat(this->m_velY) );

	switch(pIdx.getProp())
	{
	  case eMarkerProp::VelX:
	  case eMarkerProp::VelY:
	  	  return true;

	  default:
	  	  return false;
	}; //end switch(pIdx)

	return false;
}; //End: hasInitial value



/*
 * ========================
 * Building constructors
 */

egd_MarkerRehologyConstBall_t::egd_MarkerRehologyConstBall_t(
	const Numeric_t 		Vy,
	const Numeric_t 		Vx,
	const bool 			imposeVel,
	const egd_confObj_t* const 	confObj)
 :
  Base_t()
{
using ::pgl::isValidFloat;

	//Set the values
	this->m_velX = Vx;
	this->m_velY = Vy;
	this->m_imposeVel = imposeVel;

	if(isValidFloat(this->m_velX) == false)
	{
		throw PGL_EXCEPT_InvArg("The x velcity is invalid.");
	};
	if(isValidFloat(this->m_velY) == false)
	{
		throw PGL_EXCEPT_InvArg("The y velocity is invalid.");
	};

	PGL_UNUSED(confObj);
}; //End building constructor


egd_MarkerRehologyConstBall_t::egd_MarkerRehologyConstBall_t()
 :
  egd_MarkerRehologyConstBall_t(1.0, 1.0, true, nullptr)
{};


egd_MarkerRehologyConstBall_t::egd_MarkerRehologyConstBall_t(
	const egd_confObj_t& 		confObj)
 :
  egd_MarkerRehologyConstBall_t(
  		confObj.setUpPar("Vy",  0.0),
  		confObj.setUpPar("Vx", -4.0),
  		confObj.rheologyImposeVel(),
  		&confObj)
{};


/*
 * ========================
 * Defaulted constructors
 */
egd_MarkerRehologyConstBall_t::~egd_MarkerRehologyConstBall_t()
 = default;




egd_MarkerRehologyConstBall_t::egd_MarkerRehologyConstBall_t(
	const egd_MarkerRehologyConstBall_t&)
 noexcept
 = default;


egd_MarkerRehologyConstBall_t&
egd_MarkerRehologyConstBall_t::operator= (
	const egd_MarkerRehologyConstBall_t&)
 noexcept
 = default;


egd_MarkerRehologyConstBall_t::egd_MarkerRehologyConstBall_t(
	egd_MarkerRehologyConstBall_t&&)
 noexcept
 = default;


egd_MarkerRehologyConstBall_t&
egd_MarkerRehologyConstBall_t::operator= (
	egd_MarkerRehologyConstBall_t&&)
 noexcept
 = default;

PGL_NS_END(egd)

