#pragma once
/**
 * \brief	This file provides a NULL rehology. This is a rehology that does not do anything.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_MarkerRehology_interface.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_MarkerRehologyNull_t
 * \breif	This is the NULL rheology, it does not do anything.
 *
 * This class does not make any changes.
 */
class egd_MarkerRehologyNull_t final : public egd_MarkerRehology_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t 			= egd_MarkerRehology_i;

	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using Base_t::DeviatoricStress_t;
	using Base_t::DeviatoricStrainRate_t;
	using Base_t::MarkerRehology_ptr;


	/*
	 * ======================
	 * Public Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_MarkerRehologyNull_t();


	/**
	 * \brief	This is the building constructor that
	 * 		 operates on config objects.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_MarkerRehologyNull_t(
		const egd_confObj_t&		confObj)
	 noexcept;


	/**
	 * \brief	Default constructor
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyNull_t()
	 noexcept;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyNull_t(
		const egd_MarkerRehologyNull_t&)
	 noexcept;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyNull_t&
	operator= (
		const egd_MarkerRehologyNull_t&)
	 noexcept;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyNull_t(
		egd_MarkerRehologyNull_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_MarkerRehologyNull_t&
	operator= (
		egd_MarkerRehologyNull_t&&)
	 noexcept;


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function performs the material change.
	 *
	 * This function returns imediatly.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  nodeStress 	This is the stress at the "nodes".
	 * \param  nodeStrain 	This is the strain at the "nodes".
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	modifyMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const DeviatoricStress_t& 	nodeStress,
		const DeviatoricStrainRate_t&	nodeStrain,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 */
	virtual
	std::string
	print()
	 const
	 override;


	using Base_t::inspectDumper;


	/*
	 * ====================
	 * Querring
	 * These functions can be used to further configure the
	 * setup object.
	 */
public:
	using Base_t::hasInitialValue;
	using Base_t::getInitialValue;

}; //End class(egd_MarkerRehologyNull_t)

PGL_NS_END(egd)

