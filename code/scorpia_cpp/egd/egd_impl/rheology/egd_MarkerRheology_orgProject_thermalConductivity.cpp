/**
 * \brief	This file calculates the new thermal conductivity.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_MarkerRehology_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include "./egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

void
egd_markerRheologyOrgProj_t::priv_updateThermalConductivity(
	MarkerCollection_t* const 	mColl_)
 const
{
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;	//Create an alias of the collection.

	//Load the number of markers
	const auto nMarkers = mColl.nMarkers();
		pgl_assert(nMarkers >  0                ,
			   nMarkers == m_dEpsII  .size(),
			   nMarkers == m_pressure.size() );

	/*
	 * The formula is given as:
	 * 	0.73 + \frac{1293}{T + 77}
	 *
	 * Since only marker properties (to be exact only one) is
	 * involved, we can simply oiterate over the array and update it.
	 */

	//Load the variables
	const MarkerProperty_t& T     = mColl.cgetMarkerProperty(PropIdx::Temperature() );
		pgl_assert(nMarkers == T.size());

	//This is the target variable
	MarkerProperty_t& K = mColl.getMarkerProperty(PropIdx::ThermConduct() )        ;
		pgl_assert(nMarkers == K.size());

	//Iterate over all stone markers
	for(Index_t currType = TY_ROCK_START; currType != TY_END; ++currType)
	{
		//Load the list of markers of the czrrent type
		const auto& thisIndexes = mColl.cgetIndexes(currType);

		//Iterate over the markers of the current type
		for(const Index_t m : thisIndexes)
		{
			pgl_assert(0 <= m, m < nMarkers);

			//Load the temperatire
			const Numeric_t T_m = T[m];
				pgl_assert(pgl::isValidFloat(T_m), T_m >= 0.0);

			//Calculate the new conductivity
			const Numeric_t newCondi = 0.73 + 1293.0 / (T_m + 77.0);
				pgl_assert(pgl::isValidFloat(newCondi), newCondi >= 0.0);

			//Write back
			K[m] = newCondi;
		}; //End for(m):
	}; //End for(currType):
		pgl_assert(K.array().isFinite().all());

	return;
}; //End: update density


PGL_NS_END(egd)











