/**
 * \brief	This function sets up the internals.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_util_interpolState.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>

#include <egd_interfaces/egd_MarkerRehology_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include "./egd_MarkerRheology_orgProject.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


void
egd_markerRheologyOrgProj_t::priv_setUpInternals(
	const GridContainer_t& 		gridContainer,
	const MarkerCollection_t& 	mColl,
	const MechSolverResult_t& 	mechSol,
	const DeviatoricStrainRate_t&	strainRate)
{
	using pgl::isValidFloat;

	//Alias the grid geometry to grid, this is way more easier
	const GridGeometry_t& grid = gridContainer.getGeometry();

	if(grid.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set up.");
	};
	if(grid.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("Only constant grids are supported.");
	};

	//load some variables
	const Index_t nMarkers = mColl.nMarkers();
		pgl_assert(nMarkers > 0);

	const Index_t Nx = grid.xNPoints();
	const Index_t Ny = grid.yNPoints();
		pgl_assert(Nx > 0, Ny > 0);


	//Load the grid Property
	const GridProperty_t& P       = mechSol.getPressure();
	const GridProperty_t& dEPS_xx = strainRate.xx();
	const GridProperty_t& dEPS_yy = strainRate.yy();
	const GridProperty_t& dEPS_xy = strainRate.xy();
		pgl_assert(Nx ==       P.Nx(), Ny ==       P.Ny(),
			   Nx == dEPS_xx.Nx(), Ny == dEPS_xx.Ny(),
			   Nx == dEPS_yy.Nx(), Ny == dEPS_yy.Ny(),
			   Nx == dEPS_xy.Nx(), Ny == dEPS_xy.Ny() );


	/*
	 * Set the solution vectors
	 */
	m_pressure.resize(nMarkers);	//Allocation happens only if sizes does not match
	m_dEpsII  .resize(nMarkers);
		pgl_assert(nMarkers == m_dEpsII.size()  ,
			   nMarkers == m_pressure.size() );


#ifndef PGL_NDEBUG
	//Helper vectors to check the strain on the markers
	egd_Vector_t<Numeric_t> dEPSxx(nMarkers);
	egd_Vector_t<Numeric_t> dEPSyy(nMarkers);
	egd_Vector_t<Numeric_t> dEPSyx(nMarkers);

	//Maximal coefficients used for determin bounds in the interpolation
	const Numeric_t dEPSxx_max_ = dEPS_xx.maxCoeff();
	const Numeric_t dEPSxx_min_ = dEPS_xx.minCoeff();
	const Numeric_t dEPSyy_max_ = dEPS_yy.maxCoeff();
	const Numeric_t dEPSyy_min_ = dEPS_yy.minCoeff();
	const Numeric_t dEPSxy_max_ = dEPS_xy.maxCoeff();
	const Numeric_t dEPSxy_min_ = dEPS_xy.minCoeff();

	//We have to circumivlate around some numerical issues, so we do an addaption.
#define AD_MAX(what) (((what) >= 0.0) ? ((what) * 10.0) : ((what) / 10.0))
#define AD_MIN(what) (((what) >= 0.0) ? ((what) / 10.0) : ((what) * 10.0))
#define DO(what) const Numeric_t dEPS ## what ## _max = AD_MAX(dEPS ## what ## _max_);	\
                 const Numeric_t dEPS ## what ## _min = AD_MIN(dEPS ## what ## _min_)
        DO(xx);
        DO(yy);
        DO(xy);

#undef AD_MAX
#undef AD_MIN
#undef DO
#endif

	/*
	 * Now we will perform a pull baxk operation to the markers.
	 * For that we use the fact, that the pressure and the normal
	 * strain are defined at the same points, althought at a different
	 * grid, but we can use the same interpolatation routine.
	 *
	 * The shear stress needs a different interpolation routine
	 */

	//Operating CC
	{
		egd_interpolState_t interpolState;	//This is the interpolation state
		egd_initInterpolation(mColl, grid, dEPS_xx.getGridType(), &interpolState, false, true);
			pgl_assert(interpolState.isValid(), interpolState.isFinite());

		egd_markerProperty_t helperVar;		//This is a helper variable

		//Normal stresses
		egd_pullBackToMarker(&helperVar , dEPS_xx, interpolState, false);
		m_dEpsII  = helperVar.pow(2.0);
			pgl_assert((dEPSxx_min <= helperVar).all(),
				   (dEPSxx_max >= helperVar).all() );

		egd_pullBackToMarker(&helperVar , dEPS_yy, interpolState, false);
		m_dEpsII += helperVar.pow(2.0);
			pgl_assert((dEPSyy_min <= helperVar).all(),
				   (dEPSyy_max >= helperVar).all() );

		//Now we have to multiply the tentative result for m_dEpsII with 0.5
		m_dEpsII *= 0.5;

		//Pressure
		interpolState.changeGridType(P.getGridType());		//We have to change the underling pressure type
		egd_pullBackToMarker(&m_pressure, P      , interpolState, false);
	}; //End: CC

	//Operation on BASIC nodal points
	{
		egd_interpolState_t interpolState;	//This is the interpolation state
		egd_initInterpolation(mColl, grid, dEPS_xy.getGridType(), &interpolState, false, true);
			pgl_assert(interpolState.isValid(), interpolState.isFinite());

		egd_markerProperty_t helperVar;		//This is a helper variable

		//Shear stress
		egd_pullBackToMarker(&helperVar, dEPS_xy, interpolState, false);
		m_dEpsII += helperVar.pow(2.0);
			pgl_assert((dEPSxy_min <= helperVar).all(),
				   (dEPSxy_max >= helperVar).all() );
	}; //End: BG

	//Now we have to take the square root of the value
	m_dEpsII = Eigen::sqrt(m_dEpsII);

		pgl_assert(m_pressure.array().isFinite().all(),
			   m_dEpsII  .array().isFinite().all() );

	return;
}; //End: set up internal

PGL_NS_END(egd)


