/**
 * \brief	This is the implementation for the rotating disc szenario.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include "./egd_MarkerRheology_rotDisc.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)


void
egd_MarkerRehologyRotDiscTC_t::modifyMarkers(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const DeviatoricStress_t& 	nodeStress,
	const DeviatoricStrainRate_t&	nodeStrain,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
using ::pgl::isValidFloat;
using ::std::sin;
using ::std::cos;
using ::std::pow;
using ::std::sqrt;

		pgl_assert(mColl_ != nullptr);	//make an alias
	MarkerCollection_t& mColl = *mColl_;


	/* check if no imposing is requested */
	if(this->m_imposeVel == false)
	{
		return;
	};
		pgl_assert(isValidFloat(this->m_angVel),
			   isValidFloat(this->m_angVelOut));

	//Now get the marker properties
	      MarkerProperty_t& VELX = mColl.getMarkerProperty(PropIdx::VelX() );
	      MarkerProperty_t& VELY = mColl.getMarkerProperty(PropIdx::VelY() );
	const MarkerProperty_t& TYPE = mColl.getMarkerProperty(PropIdx::Type() );
	const MarkerProperty_t& X    = mColl.getMarkerProperty(PropIdx::PosX() );
	const MarkerProperty_t& Y    = mColl.getMarkerProperty(PropIdx::PosY() );


	/*
	 * Setting the velocity of the disc
	 */
	for(const Index_t TY : {TY_DISC, TY_WALL})
	{
		Numeric_t angVelTmp = NAN;

		// Select the velocity
		switch(TY)
		{
		  case TY_DISC:
		  	angVelTmp = this->m_angVel;
		  break;

		  case TY_WALL:
		  	angVelTmp = this->m_angVelOut;
		  	break;

		  default:
		  	pgl_assert(false && "Reached unreachable code.");
		}; //End switch(TY)


		//Test if a velocity was found
		if(isValidFloat(angVelTmp) == false)
		{
			throw PGL_EXCEPT_RUNTIME("Did not found a velocity for material " + std::to_string(TY));
		};

		//This is the angular velocity that is currently in use
		const Numeric_t thisAngVel = angVelTmp;

		//Setting all velocities
		for(const Index_t m : mColl.getIndexes(TY))
		{
				pgl_assert(0 <= m, m < TYPE.size(),
					TY == Index_t(TYPE[m]) );

			//Load the actuall position
			const Numeric_t x = X[m];
			const Numeric_t y = Y[m];

			//compute the distance from the rigin, the assumed point of rotation
			const Numeric_t d2 = pow(x, 2) + pow(y, 2);
			const Numeric_t d  = sqrt(d2);

			//this is the velocity we expect
			const Numeric_t velMagnitude = d * thisAngVel;

			const Numeric_t phi    = std::atan2(y, x);	//can also handle both zero
			const Numeric_t sinPhi = std::sin(phi);
			const Numeric_t cosPhi = std::cos(phi);
				pgl_assert(pgl::isValidFloat(velMagnitude));

			VELX[m] = - velMagnitude * sinPhi;	//Minus is important and swapped cos sin
			VELY[m] =   velMagnitude * cosPhi;
		}; //End for(m):
	}; //End for(TY): itertaing through the domains


	/*
	 * Setting the fluid to disc
	 */
	for(const Index_t m : mColl.cgetIndexes(TY_OUT_FLUID))
	{
			pgl_assert(0  <= m, m < TYPE.size(),
				   TY_OUT_FLUID == Index_t(TYPE[m]));

		VELX[m] = 0.0;
		VELY[m] = 0.0;
	}; //ENd for(m):

	return;
	(void)grid;
	(void)mSol;
	(void)tSol;
	(void)nodeStress;
	(void)nodeStrain;
	(void)thisDt;
	(void)tStepIdx;
	(void)currTime;
}; //End: modify the marker properties.


std::string
egd_MarkerRehologyRotDiscTC_t::print()
 const
{
	return (std::string("TC RotDisc Rehology")
			+ " angVel    = " + std::to_string(this->m_angVel   ) + ";"
			+ " angVelOut = " + std::to_string(this->m_angVelOut) + ";"
			+ " imposeVel = " + (this->m_imposeVel ? std::string("Yes") : std::string("No"))
		);
}; //End: print function


void
egd_MarkerRehologyRotDiscTC_t::inspectDumper(
	egd_dumperFile_t& 	dumper)
 const
{
	//Remove the velocity from the set of constant marker properties
	if(dumper.isInConstMarkerSet(PropIdx::VelX()) )
		{ dumper.rmConstantMarkerProperty(PropIdx::VelX() );};
	if(dumper.isInConstMarkerSet(PropIdx::VelY()) )
		{ dumper.rmConstantMarkerProperty(PropIdx::VelY() );};

	//If the imposing of the velocities is deactivated, we do not add the
	//intrinsic marker velocity to the list of ignored property.
	//This is then handled by the ini file.
	if(this->m_imposeVel == true)
	{
		dumper.ignoreMarkerVel();
			pgl_assert(dumper.markerVelAreDumped() == false);
	};

	//Do not store the temperature solution
	dumper.noTempSol()
	      .onlyFullHeatingTerm();

#	if 0
	//Add all but the type property and the velocities to the ignored
	//set, haveing the type property will allow us to visualize
	dumper.ignoreGridProp(PropIdx::Viscosity() ).
	       ignoreGridProp(PropIdx::Density()   );
# 	endif

	//Also dump only representative
	dumper.onlyDumpRepOnGrid();

	//Ignore onyl the strain, stress can be ignored by ini file.
	dumper.noStrainRateDump();

	return;
}; //End: inspect dumper



/*
 * ========================
 * Building constructors
 */

egd_MarkerRehologyRotDiscTC_t::egd_MarkerRehologyRotDiscTC_t(
	const Numeric_t 		angVel,
	const Numeric_t 		angVelOut,
	const bool 			imposeVel,
	const egd_confObj_t* const 	confObj)
 :
  Base_t()
{
using ::pgl::isValidFloat;

	if(isValidFloat(angVel) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed angVel value of " + std::to_string(angVel) + " is invalid.");
	};
	if(isValidFloat(angVelOut) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed angVelOut value of " + std::to_string(angVelOut) + " is invalid.");
	};

	/* setting the value */
	this->m_imposeVel = imposeVel;
	this->m_angVel    = angVel;
	this->m_angVelOut = angVelOut;

	PGL_UNUSED(confObj);
}; //End building constructor



egd_MarkerRehologyRotDiscTC_t::egd_MarkerRehologyRotDiscTC_t()
 :
  egd_MarkerRehologyRotDiscTC_t(40.0, 		//inner disc
  		  		0.0,		//outer cylinder
  		  		true, 		//impose vel
  		  		nullptr)	//conf object
{};


egd_MarkerRehologyRotDiscTC_t::egd_MarkerRehologyRotDiscTC_t(
	const egd_confObj_t& 		confObj)
 :
  egd_MarkerRehologyRotDiscTC_t(
  		confObj.setUpPar("angularvel", 40.0),
  		confObj.setUpPar("angularvelout", 0.0),
  		confObj.rheologyImposeVel(),
  		&confObj)
{};


/*
 * ========================
 * Defaulted constructors
 */
egd_MarkerRehologyRotDiscTC_t::~egd_MarkerRehologyRotDiscTC_t()
 = default;




egd_MarkerRehologyRotDiscTC_t::egd_MarkerRehologyRotDiscTC_t(
	const egd_MarkerRehologyRotDiscTC_t&)
 noexcept
 = default;


egd_MarkerRehologyRotDiscTC_t&
egd_MarkerRehologyRotDiscTC_t::operator= (
	const egd_MarkerRehologyRotDiscTC_t&)
 noexcept
 = default;


egd_MarkerRehologyRotDiscTC_t::egd_MarkerRehologyRotDiscTC_t(
	egd_MarkerRehologyRotDiscTC_t&&)
 noexcept
 = default;


egd_MarkerRehologyRotDiscTC_t&
egd_MarkerRehologyRotDiscTC_t::operator= (
	egd_MarkerRehologyRotDiscTC_t&&)
 noexcept
 = default;

PGL_NS_END(egd)

