#pragma once
/**
 * \brief	This file implements the base class of all temperature solver.
 *
 * This is done by extending the general solver, such that it is apporopriate
 * for the temperature equation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>
#include <egd_impl/genSolver/egd_generalSolver.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverArgument.hpp>

#include <egd_phys/egd_heatingTerm.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_TempSolverBaseImpl_t
 * \brief	This is the standard base of all temperature solver.
 *
 * This solver solves the following equation on the grid:
 * 	 \rho C_p \cdot \D_t[T] = - \div[\vec{q}] + H_s
 *
 * Where:
 * 	\vec{q}  := -k * \grad_x[T] 	~ The temperature flow.
 * 	k        :~  	Thermal conductivity (variable)
 * 	\rho C_p :~ 	Volumetric heat capacity.
 *
 * This class is written such that it is completly agnositic to the grid.
 * It does this by exctending the base solver such that it is agnosic.
 * It defines some abstract function of the general interface, but also
 * declares new ones.
 *
 * It is important that this function does not apply boundary conditions,
 * but provides methods such that sub classes can apply them.
 */
class egd_TempSolverBaseImpl_t : public egd_TempSolver_i, public egd_generalSolverImpl_t
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using InterfaceBase_t  	= egd_TempSolver_i;			//!< This is the interface that is implemented.
	using ImplBase_t 	= egd_generalSolverImpl_t;		//!< This is the implementation of the base.

	using SolverResult_t 	= InterfaceBase_t::SolverResult_t;	//!< This is the result type of the temperature solver.
	using SolverResult_ptr 	= SolverResult_t::TempSolverResult_ptr;	//!< Pointer to the result type.

	using SolverArg_t 	= InterfaceBase_t::SolverArg_t;		//!< This is the argument type.
	using SolverArg_ptr 	= SolverArg_t::TempSolverArg_ptr;	//!< This is the managed pointer type.

	using GridContainer_t 	= egd_gridContainer_t;			//!< This is the grid container
	using GridGeometry_t    = GridContainer_t::GridGeometry_t;	//!< The geometry object.
	using HeatingTerm_t 	= egd_heatingTerm_t;			//!< This is the heating term for the temperature equation.

	using SystemMatrix_t 	= ImplBase_t::SystemMatrix_t;		//!< This is the system matrix that is solved.
	using Vectot_t 		= ImplBase_t::Vectot_t;			//!< This is the type of the RHS.


	/*
	 * ===========================
	 * Constructors
	 *
	 * *this only allows move and no copy.
	 * The reason is, because Eigen did not
	 * allow copying.
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is the building constructor, it is able to
	 * to set the internal structures up.
	 * This means it will allocate memeory.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 */
	egd_TempSolverBaseImpl_t(
		const uSize_t 		Ny,
		const uSize_t 		Nx);


	/**
	 * \brief	Destructor.
	 *
	 * Is virtual and defaulted.
	 */
	virtual
	~egd_TempSolverBaseImpl_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted, reason is because Eigen LU
	 * solver does not support it.
	 */
	egd_TempSolverBaseImpl_t(
		const egd_TempSolverBaseImpl_t&)
	 = delete;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is deleted, reason is because Eigen LU
	 * solver does not support it.
	 */
	egd_TempSolverBaseImpl_t&
	operator= (
		const egd_TempSolverBaseImpl_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_TempSolverBaseImpl_t(
		egd_TempSolverBaseImpl_t&&);


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_TempSolverBaseImpl_t&
	operator= (
		egd_TempSolverBaseImpl_t&&);


	/*
	 * =========================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted and protected.
	 * Id used for internal use only.
	 */
	egd_TempSolverBaseImpl_t();



	/*
	 * ========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function compoutes the new
	 * 		 temperature distribution.
	 *
	 * This function redirects its work to the implementing
	 * code function of the base implementation.
	 * The the interface function for more details.
	 *
	 * Note that the heating term is automatically introdiced into
	 * the argument if no one is given. It is also removed after wards
	 *
	 * \param  grid		The grid container that should be used.
	 * \param  heatingTerm	The heating term, source term in the equation.
	 * \param  solArg	The argument type.
	 * \param  solResult	This is the colver result.
	 *
	 * \throw	An error is raised if something happened.
	 */
	virtual
	void
	solve(
		const GridContainer_t& 		grid,
		const HeatingTerm_t& 		heatingTerm,
		SolverArg_t* const 		solArg,
		SolverResult_t* const 		solResult)
	 override;


	/**
	 * \brief	This function outputs a description of *this.
	 */
	virtual
	std::string
	print()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns the grid on which *this operates.
	 *
	 * In the case of a regular grid, the basic nodal grid is returned, in case
	 * of a an extended grid, the extended CC grid is returned. If the grid
	 * is extended or not, is determined by hooks.
	 */
	virtual
	eGridType
	getGridType()
	 const
	 override;


	/**
	 * \brief	This fucntion returns an instance of the result object.
	 *
	 * This function is something like the builder function of the
	 * result class. The returned object must not be initalized.
	 *
	 * This function has a default implementation and generates the
	 * standard result class.
	 */
	virtual
	SolverResult_ptr
	creatResultContainer()
	 const
	 override;


	/**
	 * \brief	This function generates a standard argument object.
	 *
	 */
	virtual
	SolverArg_ptr
	createArgument()
	 const
	 override;


	/**
	 * \brief	This function returns an allocated temperature source term.
	 *
	 * The internal hooks are used to determine the paramters that are needed.
	 */
	virtual
	TempSourceTerm_ptr
	creatSourceTerm()
	 const
	 override;


	/**
	 * \brief	This function returns a property map that is needed by the solver.
	 *
	 * This function allows to solver independend create the needed property.
	 * The returned property map must then be incooperated by the setup object.
	 *
	 * This function calls the hook_mkTempProp() function.
	 * Which is a solver hook and is implemented by the concrete extensions.
	 */
	virtual
	PropToGridMap_t
	mkNeededProperty()
	 const
	 override;


	/*
	 * =======================
	 * Utility functions
	 *
	 * These functions are used to perform some standard tasks.
	 */
public:


	/*
	 * ==========================
	 * Internal helper functions
	 *
	 * Here are the internal helper functions
	 * which implements the solving of the equation.
	 */
private:


	/*
	 * ======================
	 * Hooks
	 */
protected:
	/**
	  * \brief	This function composes the system matrix.
	  *
	  * Note this function is a convinient function and not the
	  * one that is exposed/defined by the general solver
	  * implementation, the rason is that the heating term is
	  * missing.
	  * This function should be overwritten by the calling code.
	  * Note that the solArg is already casted into a temperature
	  * argument.
	  *
	  * \param  grid		This is the grid container.
	  * \param  heatingTerm	This is the heating term.
	  * \param  solArg	This is the argument of the solver.
	  */

	virtual
	void
	hook_buildSystemMatrix(
		const GridContainer_t& 				grid,
		const HeatingTerm_t& 				heatingTerm,
		InterfaceBase_t::SolverArg_t* const 		solArg)
	 = 0;


	/**
	 * \brief	This hook allows the modification of the system matrix.
	 *
	 * The default of this function redirects to the base implementation.
	 *
	 * \param  grid		This is the grid container.
	 * \param  solArg 	This is the solution argument.
	 * \param  isFirstRun	This value indicates if this is the first run.
	 *
	 * \throw 	This function is allowd to throw.
	 */
	virtual
	bool
	hook_modifySystemMatrix(
		const GridContainer_t&	grid,
		SolverArg_i* const 	solArg,
		const bool 		isFirstRun)
	 override;

	/**
	 * \brief	This function is called after the solution was distributed.
	 *
	 * This function will turn the computed field, that is stored inside
	 * solRes, which is currently an absolute temperature filed
	 * into a difference temperature field.
	 *
	 * \param  sol		This is the compzted solution vector.
	 * \param  grid		This is the grid that is used.
	 * \þaram  solArg	This is the argument that was passed to *this.
	 * \param  solResult	This is the object that will store the solution.
	 *
	 * The default of this function does nothing.
	 */
	virtual
	void
	hook_postProcessSolution(
		const Vectot_t&			sol,
		const GridContainer_t&		grid,
		SolverArg_i*  const 		solArg,
		SolverResult_i* const 		solRes)
	 const
	 override;


	/**
	 * \brief	This function generates a property to grid map of the needed properties.
	 *
	 * This function is used to get the properties that are needed by the solver.
	 */
	virtual
	PropToGridMap_t
	hook_mkTempProps()
	 const
	 = 0;


	/**
	 * \brief	This function returns the numbers of unkowns in x direction.
	 *
	 * See the super class function for more. Depending on the state, if *this is
	 * extended or not, the appropriate number is selected.
	 */
	virtual
	Index_t
	hook_nUnknownX()
	 const
	 override;


	/**
	 * \brief	This function returns the number of unknowns in y direction.
	 *
	 * See the super class function for more. Depending on the state, if *this is
	 * extended or not, the appropriate number is selected.
	 */
	virtual
	Index_t
	hook_nUnknownY()
	 const
	 override;


	/**
	 * \brief	This is the number of variables that are located
	 * 		 at a single grid node.
	 *
	 * This function will return one, since we only solve for temperature.
	 */
	virtual
	Index_t
	hook_nPropPerNode()
	 const
	 override;


	/**
	 * \brief	This is the number of unkowns that should be reservered
	 * 		 in each column.
	 *
	 * This function will return 6.
	 */
	virtual
	Index_t
	hook_reservePerCol()
	 const
	 override;


	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not.
	 *
	 * True indicates that *this is an extextended grid.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function returns true if the system matrix and the
	 * 		 rhs should be damped into a text file.
	 *
	 * See super class for more details, this function calls the default.
	 *
	 * \param  isFrisRun		Indicates if the first run is happening.
	 */
	virtual
	bool
	hook_writeSystem(
		const bool 	isFrisRun)
	 const
	 override;


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping.
	 *
	 * The returned token should be something that is decriptive
	 * to the class instance, such as "stokesStandardSolver".
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	 = 0;



	/**
	 * \brief	This is the interface building function.
	 *
	 * The general solver impl function will call this function.
	 * But this is not the one that is used by the temperature
	 * equation. It is missing the heating term.
	 * So this function will unpack the heating term from
	 * the argument and call the function that is recomended above.
	 *
	 * \param  grid		The grid container.
	 * \param  solArg	The solver argument.
	 */
	virtual
	void
	hook_buildSystemMatrix(
		const GridContainer_t& 			grid,
		ImplBase_t::SolverArg_i* const 		solArg)
	override;


	/*
	 * ==========================
	 * Boundary hooks
	 *
	 * These hooks are needed for handling the boundary condition.
	 */
protected:
	/**
	 * \brief	Thid function is a hook that allows to call the boundary.
	 *
	 * The offset are applied by adding them to an index.
	 * If this function detects that it is on the boundary, then the matrix must
	 * be updated and true is returned, if the passed point is not on the
	 * boundary, false is returned.
	 *
	 * \param  i		The i index of the point; first in matrix or y spatial.
	 * \param  j 		The j index of the point; second in matrix of x spatial.
	 * \param  k_t 		The global index of that node.
	 * \param  nRows	Number of rows; equal nUnknownsY.
	 * \param  nCols	Number of cols; equal nUnknownsX.
	 * \param  UP		Offset to go one up.
	 * \param  DOWN		Offset to go one down.
	 * \param  RIGHT	Offset to go one to the right.
	 * \param  LEFT		Offset to go one to the left.
	 */
	virtual
	bool
	hook_handleBC(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_t,
		const Index_t 		nRows,
		const Index_t 		nCols,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 = 0;


	/*
	 * ==================
	 * Private Members
	 * This class does not have any memebers, sinc it fully
	 * operates on the general implementation.
	 */
protected:
}; //End: class(egd_TempSolverBaseImpl_t)

PGL_NS_END(egd)


