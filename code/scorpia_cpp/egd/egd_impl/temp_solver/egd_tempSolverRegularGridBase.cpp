/**
 * \brief	This file implements the fucntions that are needed by the regular grid solver.
 *
 * I have descided to put all in one file, because most of them are so small.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_tempSolverRegularGridBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)


void
egd_tempSolverRegGridBase_t::hook_buildSystemMatrix(
	const GridContainer_t& 				grid,
	const HeatingTerm_t& 				heatingTerm,
	InterfaceBase_t::SolverArg_t* const 		solArg_)
{
	using ::pgl::isValidFloat;
	const GridGeometry_t& gridGeo = grid.getGeometry();
		pgl_assert(gridGeo.isGridSet());

	pgl_assert(solArg_ != nullptr);
	InterfaceBase_t::SolverArg_t& solArg = *solArg_;

	//Unpack the argument
	const Numeric_t thisDt = solArg.getDT();

	//Load some properties of the grid
	const Index_t Nx = this->getNx();
	const Index_t Ny = this->getNy();

	//
	//Make some tests
	if(gridGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_LOGIC("The grid is not constant.");
	};
	if(pgl::isValidFloat(thisDt) == false || thisDt <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The timestep is invalid, it was " + std::to_string(thisDt) + ".");
	};
	if(Nx <= 2)
	{
		throw PGL_EXCEPT_LOGIC("The grid is too small, in x it has only " + std::to_string(Nx) + " grid points.");
	};
	if(Ny <= 2)
	{
		throw PGL_EXCEPT_LOGIC("The grid is too small, in y it has only " + std::to_string(Ny) + " grid points.");
	};
	pgl_assert(this->hook_nPropPerNode() == 1);


	/*
	 * Test if there is a source term
	 */
	const bool hasSource = solArg.hasSourceTerm();
	const TempSourceTerm_t::Source_t* const sourceTerm = (hasSource)
						    ? &(solArg.getSourceFor(PropIdx::Temperature()) )
						    : nullptr;

	//Get some variables
	const Index_t xPoints = Nx         ;		//Number of points in x direction (all not just inner).
	const Index_t yPoints = Ny         ;		//Number of points in y direction (all not just inner).
	const Index_t xLast   = xPoints - 1;		//This is the last valid index in x direction; right boundary so to speak.
	const Index_t yLast   = yPoints - 1;		//Same but for y direction (first index).

	const Numeric_t  DeltaX  = gridGeo.getXSpacing()  ;	//Spacing in x direction
	const Numeric_t  DeltaY  = gridGeo.getYSpacing()  ;	//Spacing in y direction
	const Numeric_t iDeltaX  = 1.0 /  DeltaX          ;	//Inverse of the x spacing
	const Numeric_t iDeltaY  = 1.0 /  DeltaY          ;	//Inverse of the y spacing
	const Numeric_t iDeltaX2 = 1.0 / (DeltaX * DeltaX);	//Square of inverse spacing in x
	const Numeric_t iDeltaY2 = 1.0 / (DeltaY * DeltaY);

	//These are the differences for moving in the global indexing regime
	const Index_t UP    = -1;		//Towards lesser y values; ascend to the surface; going up; decrease first index by one
	const Index_t DOWN  = +1;	//Towards larger y values; going deeper; increase first index by one
	const Index_t RIGHT = +yPoints;	//Towards larger x values; move to the right; increas second index by one
	const Index_t LEFT  = -yPoints;	//Towards lesser x values; move to the left; decrease second index by one

	//Load the property matrices we need
	const auto& K_x       = grid.cgetProperty(PropIdx::ThermConductVY() );	//Get thermal conductivity for x heat flux
	const auto& K_y       = grid.cgetProperty(PropIdx::ThermConductVX() );	//Get thermal conductivity for y heat flux.
	const auto& RHOCP     = grid.cgetProperty(PropIdx::RhoCp()          ); 	//Get volumentric heat capacity
	const auto& CURR_TEMP = grid.cgetProperty(PropIdx::Temperature()    );	//Get the current temperature
		pgl_assert(yPoints == K_x.Ny()      , xPoints == K_x.Nx()      ,
		           yPoints == K_x.Ny()      , xPoints == K_x.Nx()      ,
			   yPoints == RHOCP.Ny()    , xPoints == RHOCP.Nx()    ,
			   yPoints == CURR_TEMP.Ny(), xPoints == CURR_TEMP.Nx(),
			   Ny      == K_x.rows()    , Nx      == K_x.cols()     );

	if(K_x.getType() != mkReg(eGridType::StVy))
	{
		throw PGL_EXCEPT_InvArg("K_x is n the wrong grid.");
	};
	if(K_y.getType() != mkReg(eGridType::StVx))
	{
		throw PGL_EXCEPT_InvArg("K_y is on the wrong grid.");
	};
	if(RHOCP.getType() != mkReg(eGridType::BasicNode))
	{
		throw PGL_EXCEPT_InvArg("RHOCP is on the wrong grid.");
	};
	if(CURR_TEMP.getType() != mkReg(eGridType::BasicNode))
	{
		throw PGL_EXCEPT_InvArg("CURR_TEMP on the wrong grid.");
	};

	//This is the source term we use the overloaded () operator to access the values
	const auto& HEATING = heatingTerm;
		if(HEATING.getGridType() != mkReg(eGridType::BasicNode))
		{
			throw PGL_EXCEPT_InvArg("HEATING is on wrong grid.");
		};


	/*
	 * We will now create/update the matrix.
	 * Notice as it was written before, this is not very efficient,
	 * since the matrix has the wrong order, but the order is needed
	 * because the solver wants it!
	 */
	for(Index_t j = 0; j != xPoints; ++j)
	{
		for(Index_t i = 0; i != yPoints; ++i)
		{
			//Compute the global index, this is the row we currently handle
			//Note that the point is defined on a grid point.
			const Index_t k_t = yPoints * j + i;
				pgl_assert(k_t >= 0, k_t < m_L.rows(),
					             k_t < m_rhs.size() );

			//Test if we are n the boundary
			if(this->hook_handleBC(
					i, j, k_t,			//Location
					Ny, Nx,				//Extension of grid
					UP, DOWN, RIGHT, LEFT)		//Delta for move
				== false)
			{
				/*
				 * We handle here the internal domain.
				 * We have the following situation.
				 *
				 *   0---------------------------------------> X/J
				 *   |                  j
				 *   |     o------------o------------o
				 *   |     |            | T2         |
				 *   |     |            |            |
				 *   |     |            x q3         |        ^
				 *   |     |            |            |        U
				 *   |     |      q1    |      q2    |        P
				 *   |   i o------x-----O------x-----o       ---
				 *   |     | T1         | T3         | T5     D
				 *   |     |            |            |        O
				 *   |     |            x q4         |        W
				 *   |     |            |            |        N
				 *   |     |            |            |        v
				 *   |     o------------o------------o
				 *   |                   T3
				 *   V Y/I
				 *
				 * We are located at the node "O" with coordinate (i, j).
				 * There also "T3" is located. The qunatities labeled as
				 * "T" are the temperature, they are tied to a node.
				 * The quantities "q" are the heat flux. They are located
				 * between two temperature points. They are not needed,
				 * since they only apear in the derivation.
				 *
				 */

				//Get the index
				const Index_t idx_T3  = k_t           ;	//This is the base index
				const Index_t idx_T1  = idx_T3 + LEFT ;
				const Index_t idx_T2  = idx_T3 + UP   ;
				const Index_t idx_T4  = idx_T3 + DOWN ;
				const Index_t idx_T5  = idx_T3 + RIGHT;
					pgl_assert(0 <= idx_T1, idx_T1 < m_L.cols(),
						   0 <= idx_T2, idx_T2 < m_L.cols(),
						   0 <= idx_T3, idx_T3 < m_L.cols(),
						   0 <= idx_T4, idx_T4 < m_L.cols(),
						   0 <= idx_T5, idx_T5 < m_L.cols() );

				//Load the values we need
				const Numeric_t TEMP_T3  = CURR_TEMP(i    , j    );	//Current temperature at T3 node
				const Numeric_t RhoCp_T3 =     RHOCP(i    , j    );	//Volumetric heat capacity at node.
				const Numeric_t K_q1     =       K_x(i    , j    );	//k_x_1
				const Numeric_t K_q2 	 =       K_x(i    , j + 1);	//k_x_2
				const Numeric_t K_q3     =       K_y(i    , j    );	//k_y_1
				const Numeric_t K_q4     =       K_y(i + 1, j    );	//k_y_2
					pgl_assert(isValidFloat(K_q1), isValidFloat(K_q2),
						   isValidFloat(K_q3), isValidFloat(K_q4) );
					pgl_assert(isValidFloat(TEMP_T3 ), TEMP_T3  >= 0.0);
					pgl_assert(isValidFloat(RhoCp_T3), RhoCp_T3 >= 0.0);

				//Now Filling in the coefficients; we are the first one, so no updating needed
				m_L.coeffRef(k_t, idx_T1)		//Interaction with T1
					= -K_q1 * iDeltaX2;

				m_L.coeffRef(k_t, idx_T2)		//Interacting with T2
					= -K_q3 * iDeltaY2;

				m_L.coeffRef(k_t, idx_T3)		//Interacting with T3
					=  RhoCp_T3 / thisDt + (K_q1 + K_q2) * iDeltaX2 + (K_q3 + K_q4) * iDeltaY2;

				m_L.coeffRef(k_t, idx_T4)		//Interacting with T4
					= -K_q4 * iDeltaY2;

				m_L.coeffRef(k_t, idx_T5)		//Interacting with T5
					= -K_q2 * iDeltaX2;

				/*
				 * RHS
				 */
				const Numeric_t heating_T3 	= HEATING(i, j);	//Heating that occures at that node
					pgl_assert(isValidFloat(heating_T3));

				m_rhs[k_t] = ( TEMP_T3 * RhoCp_T3 / thisDt ) + heating_T3;


				/*
				 * Handle source term
				 */
				if(hasSource == true)
				{
					m_rhs[k_t] += sourceTerm->operator()(i, j);
				};//End if: handle source term
			}; //End else: handling internal boundary
		}; //End for(i):
	}; //End for(j):

	return;
	(void)xLast;
	(void)yLast;
	(void)iDeltaX;
	(void)iDeltaY;
}; //End composing matrix




/*
 * ===========================================
 */

egd_tempSolverRegGridBase_t::PropToGridMap_t
egd_tempSolverRegGridBase_t::hook_mkTempProps()
 const
{
	PropToGridMap_t gProps;

	//Adding universal properties
	gProps.addMapping(PropIdx::ThermConductVX(), eGridType::StVx, false);
	gProps.addMapping(PropIdx::ThermConductVY(), eGridType::StVy, false);

	//These are special in the sense that they are on the cell centres.
	gProps.addMapping(PropIdx::RhoCp()           , eGridType::BasicNode, false);
	gProps.addMapping(PropIdx::ThermExpansAlpha(), eGridType::BasicNode, false);
	gProps.addMapping(PropIdx::RadioEnergy()     , eGridType::BasicNode, false);

	//By definition is not needed to specialize where the temperature is
	gProps.addMapping(PropIdx::Temperature()     , eGridType::BasicNode, false);

	return gProps;
}; //ENd: mkProps


bool
egd_tempSolverRegGridBase_t::hook_isExtendedGrid()
 const
{
	return false;
};


egd_tempSolverRegGridBase_t::egd_tempSolverRegGridBase_t(
	const uSize_t 		Ny,
	const uSize_t 		Nx)
 :
  egd_TempSolverBaseImpl_t(Ny, Nx)
{
	/*
	 * We have noting to do here.
	 * The called constructor handles it.
	 */
}; //End: building constructor.



egd_tempSolverRegGridBase_t::egd_tempSolverRegGridBase_t()
 = default;



egd_tempSolverRegGridBase_t::egd_tempSolverRegGridBase_t(
	egd_tempSolverRegGridBase_t&&)
 = default;


egd_tempSolverRegGridBase_t&
egd_tempSolverRegGridBase_t::operator= (
	egd_tempSolverRegGridBase_t&&)
 = default;


egd_tempSolverRegGridBase_t::~egd_tempSolverRegGridBase_t()
 = default;



PGL_NS_END(egd)


