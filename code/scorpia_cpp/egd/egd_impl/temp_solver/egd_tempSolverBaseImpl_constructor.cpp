/**
 * \brief	This file implements the constructors for the temperature solver base.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_tempSolverBaseImpl.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)



egd_TempSolverBaseImpl_t::SolverResult_ptr
egd::egd_TempSolverBaseImpl_t::creatResultContainer()
 const
{
	SolverResult_ptr res = std::make_unique<SolverResult_t>();	//Create a default solver
	res->allocateSize(yNodeIdx_t(this->getNy()), xNodeIdx_t(this->getNx()), this->getGridType());

	return res;
}; //End: creatResult container


egd_TempSolverBaseImpl_t::SolverArg_ptr
egd_TempSolverBaseImpl_t::createArgument()
 const
{
	SolverArg_ptr res = std::make_unique<SolverArg_t>();

	return res;
};//End: create argument


egd_TempSolverBaseImpl_t::TempSourceTerm_ptr
egd_TempSolverBaseImpl_t::creatSourceTerm()
 const
{
	TempSourceTerm_ptr tmp = ::std::make_unique<TempSourceTerm_t>();

	//Call the allocation
	tmp->allocateTerm(
		yNodeIdx_t(this->getNy()),
		xNodeIdx_t(this->getNx()),
		this->hook_isExtendedGrid() );

	return tmp;
};



egd_TempSolverBaseImpl_t::egd_TempSolverBaseImpl_t(
	const uSize_t 		Ny,
	const uSize_t 		Nx)
 :
  egd_generalSolverImpl_t(Ny, Nx)
{
	/*
	 * We have noting to do here.
	 * The called constructor handles it.
	 */
}; //End: building constructor.



egd_TempSolverBaseImpl_t::egd_TempSolverBaseImpl_t()
 = default;



egd_TempSolverBaseImpl_t::egd_TempSolverBaseImpl_t(
	egd_TempSolverBaseImpl_t&&)
 = default;


egd_TempSolverBaseImpl_t&
egd_TempSolverBaseImpl_t::operator= (
	egd_TempSolverBaseImpl_t&&)
 = default;


egd_TempSolverBaseImpl_t::~egd_TempSolverBaseImpl_t()
 = default;



PGL_NS_END(egd)


