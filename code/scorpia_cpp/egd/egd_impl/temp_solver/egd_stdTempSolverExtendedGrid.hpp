#pragma once
/**
 * \brief	This file contains the class for the solver of the extended grid, with the std boundary.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>
#include <egd_impl/genSolver/egd_generalSolver.hpp>
#include "./egd_tempSolverExtendedGridBase.hpp"
#include "./egd_tempSolverStdBoundary.hpp"

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


/**
 * \brief	This typedef is a temperature solver on the extended grid, with std boundary.
 * \typedef	egd_StdTempSolverExtGrid_t
 */
using egd_StdTempSolverExtGrid_t = egd_tempSolverStdBoundary_t<egd_tempSolverExtGridBase_t, true>;





PGL_NS_END(egd)


