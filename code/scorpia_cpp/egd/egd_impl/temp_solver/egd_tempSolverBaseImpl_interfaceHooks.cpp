/**
 * \brief	This file implements the functions that are requiered
 * 		 by the temperature solvber interface.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include "./egd_tempSolverBaseImpl.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD
#include <memory>


PGL_NS_START(egd)

void
egd_TempSolverBaseImpl_t::solve(
	const GridContainer_t& 				grid,
	const HeatingTerm_t& 				heatingTerm,
	InterfaceBase_t::SolverArg_t* const 		solArg,
	InterfaceBase_t::SolverResult_t* const 		solRes)
{
	pgl_assert(grid.getGeometry().isGridSet(),
		   heatingTerm.isValid(),
		   solArg != nullptr && solArg->isFinite(),
		   solRes != nullptr && solRes->isFinite() );

	//Test if the heating term must be managed
	const bool htManage = (solArg->hasHeatingTerm() ? false : true);

	if(htManage == true)	//Store the heating term
	{
		solArg->setHeatingTerm(heatingTerm);
	};

	//Call the implementation function
	this->solve_impl(grid,
		dynamic_cast<ImplBase_t::SolverArg_i*>(solArg),
		dynamic_cast<ImplBase_t::SolverResult_i*>(solRes) );

	//Remove the heating term again from the argument if we added it
	if(htManage == true)
	{
		solArg->resetHeatingTerm();
	};

	return;
}; //End: solver function


eGridType
egd_TempSolverBaseImpl_t::getGridType()
	 const
{
	if(this->hook_isExtendedGrid() == true)
	{
		return mkExt(eGridType::CellCenter);
	};

	return mkReg(eGridType::BasicNode);
}; //End: getGridType



egd_TempSolverBaseImpl_t::PropToGridMap_t
egd_TempSolverBaseImpl_t::mkNeededProperty()
 const
{
	return this->hook_mkTempProps();
};


PGL_NS_END(egd)

