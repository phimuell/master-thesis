#pragma once
/**
 * \brief	This file contains the NULL temp solver.
 *
 * This is a solver that does not create any tenmperature solutioon.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverArgument.hpp>

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_TempSolverNULL_t
 * \brief	This is the NULL temperature solver.
 *
 * As the name already implies, this solver does not compute
 * any solution on its own. It will however always output
 * 0, meaning that nop temperature change occured.
 * This is usefull if the temperature tretment is ignored.
 *
 * It will also not impose any conditions.
 *
 * It is templated that allows it to switch between
 * extended and regular.
 *
 * \tparam  isExt	Indicates if the grid is an extended one.
 */
template<
	bool 	isExt
>
class egd_TempSolverNULL_t final : public egd_TempSolver_i
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using Base_t 		= egd_TempSolver_i;			//!< This is the base.
	using Base_t::Size_t;
	using Base_t::uSize_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::Matrix_t;
	using Base_t::SolverResult_t;
	using Base_t::SolverResult_ptr;
	using Base_t::SolverArg_t;
	using Base_t::SolverArg_ptr;
	using Base_t::TempSourceTerm_t;
	using Base_t::TempSourceTerm_ptr;
	using Base_t::GridContainer_t;
	using Base_t::HeatingTerm_t;
	using Base_t::TempSolver_ptr;
	using Base_t::PropToGridMap_t;


	/*
	 * ===========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Destructor.
	 *
	 * Is virtual and defaulted.
	 */
	virtual
	~egd_TempSolverNULL_t()
	 noexcept
	 = default;


	/**
	 * \brief	This is the building constructor.
	 *
	 * It takes the size of the basic nodal grid.
	 *
	 * \param  nBasicY	Number of basic grid points in y direction.
	 * \param  nBasicX	Number of basic grid points in x direction.
	 */
	egd_TempSolverNULL_t(
		const yNodeIdx_t 	nBasicY,
		const xNodeIdx_t 	nBasicX)
	 :
	  m_Ny(nBasicY),
	  m_Nx(nBasicX)
	{
		if(m_Ny <= Index_t(3))
			{ throw PGL_EXCEPT_InvArg("Number of y grid points too small.");};
		if(m_Nx <= Index_t(3))
			{ throw PGL_EXCEPT_InvArg("Number of x grid points too small.");};
	};//End building


	/**
	 * \brief	This uis the building constructor that operates on
	 * 		 configuaration objects directly.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_TempSolverNULL_t(
		const egd_confObj_t& 	confObj)
	 :
	  egd_TempSolverNULL_t(
	  		yNodeIdx_t(confObj.getNy()),
	  		xNodeIdx_t(confObj.getNx()) )
	{};



	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_TempSolverNULL_t()
	 noexcept
	 = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_TempSolverNULL_t(
		const egd_TempSolverNULL_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_TempSolverNULL_t&
	operator= (
		const egd_TempSolverNULL_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_TempSolverNULL_t(
		egd_TempSolverNULL_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_TempSolverNULL_t&
	operator= (
		egd_TempSolverNULL_t&&)
	 noexcept
	 = default;


	/*
	 * ========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function compoutes the new
	 * 		 temperature distribution.
	 *
	 * This function will set the the result to zero.
	 *
	 * \param  grid		The grid container that should be used.
	 * \param  heatingTerm	This is the heating term, it is the source.
	 * \param  solArg	This is the argument type.
	 * \param  solResult	This is the colver result.
	 *
	 * \throw	An error is raised if something happened.
	 *
	 */
	virtual
	void
	solve(
		const GridContainer_t& 		grid,
		const HeatingTerm_t& 		heatingTerm,
		SolverArg_t* const 		solArg,
		SolverResult_t* const 		solResult)
	 final
	{

		if(grid.basicNodalPointsX() != m_Nx)
		{
			throw PGL_EXCEPT_InvArg("The number of basic nodal points in x is wrong."
					" It should be " + std::to_string(m_Nx) + " but got " + std::to_string(grid.basicNodalPointsX()) );
		};
		if(grid.basicNodalPointsY() != m_Ny)
		{
			throw PGL_EXCEPT_InvArg("The number of basic nodal points in y is wrong."
					" It should be " + std::to_string(m_Ny) + " but got " + std::to_string(grid.basicNodalPointsY()) );
		};

		//Test if we have to allocate it
		if(solResult->isAllocated() == false)
		{
			solResult->allocateSize(m_Ny, m_Nx, isExt);
		}; //Allocate it

		if(solResult->Ny() != m_Ny)
		{
			throw PGL_EXCEPT_InvArg("The solver result was allocated but had the wrong size in y."
					" Expected " + std::to_string(m_Ny) + ", but got " + std::to_string(solResult->Ny()) );
		};
		if(solResult->Nx() != m_Nx)
		{
			throw PGL_EXCEPT_InvArg("The solver result was allocated but had the wrong size in x."
					" Expected " + std::to_string(m_Nx) + ", but got " + std::to_string(solResult->Nx()) );
		};
		if(solResult->onExtendedGrid() != isExt)
		{
			throw PGL_EXCEPT_InvArg("The results are on different grids.");
		};


		/*
		 * New set everything to zero.
		 */
		solResult->setZero();

		return;
		(void)heatingTerm;
		(void)solArg;
	}; //End solve


	/**
	 * \brief	This fucntion returns an instance of the result object.
	 *
	 * This function is something like the builder function of the
	 * result class. The returned object must not be initalized.
	 *
	 * This function has a default implementation and generates the
	 * standard result class.
	 */
	virtual
	SolverResult_ptr
	creatResultContainer()
	 const
	{
		SolverResult_ptr res = std::make_unique<SolverResult_t>();	//Create a default solver
		res->allocateSize(m_Ny, m_Nx, isExt);
		return res;
	};


	/**
	 * \brief	This function generates a standard argument object.
	 *
	 * This function returns the default constructed argument back.
	 */
	virtual
	SolverArg_ptr
	createArgument()
	 const
	{
		SolverArg_ptr res = std::make_unique<SolverArg_t>();
		return res;
	};//End: create argument


	/**
	 * \brief	This function creats a temeperature source term.
	 */
	virtual
	TempSourceTerm_ptr
	creatSourceTerm()
	 const
	{
		TempSourceTerm_ptr tmp = ::std::make_unique<TempSourceTerm_t>();

		//Call the allocation
		tmp->allocateTerm(
				m_Ny, m_Nx,
				isExt );
		return tmp;
	};//End: make source


	/**
	 * \brief	This function returns a property map that is needed by the solver.
	 *
	 * This function returns teh empty property map.
	 */
	virtual
	PropToGridMap_t
	mkNeededProperty()
	 const
	{
		return PropToGridMap_t();
	};


	/*
	 * ================================
	 * Status functions.
	 *
	 * These functions allows to quiery the status of *this.
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 *
	 * This is mostly used for debugging and output.
	 */
	virtual
	std::string
	print()
	 const
	{
		return std::string("NULL Temperature solver.");
	};


	/**
	 * \brief	This function returns the grid type
	 * 		 on which the solver operates.
	 *
	 * This function conforms with the convention in EGD
	 */
	virtual
	eGridType
	getGridType()
	 const
	{
		return isExt
		       ? mkExt(eGridType::CellCenter)
		       : mkReg(eGridType::BasicNode );
	};


	/**
	 * \brief	This function return true if *this operates
	 * 		 on an extended grid.
	 */
	virtual
	bool
	onExtendedGrid()
	 const
	{
		return isExt;
	};


	/**
	 * \brief	This function returns true if *this operates
	 * 		 on a regular grid.
	 */
	virtual
	bool
	onRegularGrid()
	 const
	{
		return !isExt;
	};


	/*
	 * ======================
	 * Private Members
	 */
private:
	yNodeIdx_t 		m_Ny;
	xNodeIdx_t 		m_Nx;
}; //End: class(egd_TempSolverNULL_t)


/**
 * \brief	This is an alias of the extended NULL solver.
 * \typedef	egd_NullTempSolverExt_t
 */
using egd_NullTempSolverExt_t = egd_TempSolverNULL_t<true>;


/**
 * \brief	This is an alias of the regular NULL solver.
 * \typedef	egd_NullTempSolverReg_t
 */
using egd_NullTempSolverReg_t = egd_TempSolverNULL_t<false>;


PGL_NS_END(egd)

