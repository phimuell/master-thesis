/**
 * \brief	This file implements the hooks that are defined by the general interface function.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>
#include <egd_impl/genSolver/egd_generalSolver.hpp>
#include "./egd_tempSolverBaseImpl.hpp"

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

bool
egd_TempSolverBaseImpl_t::hook_modifySystemMatrix(
	const GridContainer_t&	grid,
	SolverArg_i* const 	solArg,
	const bool 		isFirstRun)
{
	return this->ImplBase_t::hook_modifySystemMatrix(grid, solArg, isFirstRun);
};


void
egd_TempSolverBaseImpl_t::hook_postProcessSolution(
	const Vectot_t&			sol,
	const GridContainer_t&		grid,
	ImplBase_t::SolverArg_i*  const 		solArg_,
	ImplBase_t::SolverResult_i* const 		solRes_)
 const
{
	pgl_assert(solArg_ != nullptr, solArg_->isFinite());
	pgl_assert(solRes_ != nullptr, solRes_->isFinite());

	InterfaceBase_t::SolverResult_t* solRes = dynamic_cast<InterfaceBase_t::SolverResult_t*>(solRes_);

	//Currently the stored solution field is an absolute temperature.
	InterfaceBase_t::SolverResult_t::SolField_t& TEMP_NEXT = solRes->getTempDiff();

	//This is the target, where we want the temperature difference; note that this aliases TEMP_NEXT
	InterfaceBase_t::SolverResult_t::SolField_t& TEMP_DIFF = solRes->getTempDiff();

	//This is the current temperature field
	const auto& TEMP_CURR = grid.getProperty(PropIdx::Temperature() );	//Future proof

	if(TEMP_CURR.getType() != TEMP_NEXT.getType())
	{
		throw PGL_EXCEPT_RUNTIME("The two temperature grids are different.");
	};

	TEMP_DIFF.maxView() = TEMP_NEXT.maxView() - TEMP_CURR.maxView();

	return;
	(void)sol;
}; //End: postprocess the distribution.


Index_t
egd_TempSolverBaseImpl_t::hook_nUnknownX()
 const
{
	return (this->hook_isExtendedGrid()
		? GridGeometry_t::getNGridPointsX(xNodeIdx_t(this->getNx()), mkExt(eGridType::CellCenter))
		: GridGeometry_t::getNGridPointsX(xNodeIdx_t(this->getNx()), mkReg(eGridType::BasicNode )) );
};


Index_t
egd_TempSolverBaseImpl_t::hook_nUnknownY()
 const
{
	return (this->hook_isExtendedGrid()
		? GridGeometry_t::getNGridPointsY(yNodeIdx_t(this->getNy()), mkExt(eGridType::CellCenter))
		: GridGeometry_t::getNGridPointsY(yNodeIdx_t(this->getNy()), mkReg(eGridType::BasicNode )) );
};


Index_t
egd_TempSolverBaseImpl_t::hook_nPropPerNode()
 const
{
	return 1;
};


Index_t
egd_TempSolverBaseImpl_t::hook_reservePerCol()
 const
{
	return 6;
};


bool
egd_TempSolverBaseImpl_t::hook_writeSystem(
	const bool 	isFrisRun)
 const
{
	return this->ImplBase_t::hook_writeSystem(isFrisRun);
};


void
egd_TempSolverBaseImpl_t::hook_buildSystemMatrix(
	const GridContainer_t& 			grid,
	ImplBase_t::SolverArg_i* const 		solArg_)
{
	pgl_assert(solArg_ != nullptr);
	InterfaceBase_t::SolverArg_t* const solArg = dynamic_cast<InterfaceBase_t::SolverArg_t*>(solArg_);

	pgl_assert(grid.getGeometry().isGridSet(),
		   solArg->hasHeatingTerm());

	//Redirect
	this->hook_buildSystemMatrix(
		grid,
		solArg->getHeatingTerm(),
		dynamic_cast<InterfaceBase_t::SolverArg_t*>(solArg_)
	);

	return;
}; //End: trampolin for building a matrix



PGL_NS_END(egd)

