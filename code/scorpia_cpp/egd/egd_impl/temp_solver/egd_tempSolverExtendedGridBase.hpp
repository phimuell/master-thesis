#pragma once
/**
 * \brief	This file defines a class that implements the composing of the system matrix on an extended grid.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>
#include <egd_impl/genSolver/egd_generalSolver.hpp>
#include "./egd_tempSolverBaseImpl.hpp"

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_tempSolverExtGridBase_t
 * \brief	This class implements the temperature equation of on the extended grid.
 *
 * As before the boundary condition is not handled here.
 */
class egd_tempSolverExtGridBase_t : public egd_TempSolverBaseImpl_t
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using InterfaceBase_t  	= egd_TempSolver_i;			//!< This is the interface that is implemented.
	using ImplBase_t 	= egd_TempSolverBaseImpl_t;		//!< This is the implementation of the base.

	using SolverResult_t 	= InterfaceBase_t::SolverResult_t;	//!< This is the result type of the temperature solver.
	using SolverResult_ptr 	= SolverResult_t::TempSolverResult_ptr;	//!< Pointer to the result type.

	using SolverArg_t 	= InterfaceBase_t::SolverArg_t;		//!< This is the argument type.
	using SolverArg_ptr 	= SolverArg_t::TempSolverArg_ptr;	//!< This is the managed pointer type.

	using GridContainer_t 	= egd_gridContainer_t;			//!< This is the grid container
	using GridGeometry_t    = GridContainer_t::GridGeometry_t;	//!< The geometry object.
	using HeatingTerm_t 	= egd_heatingTerm_t;			//!< This is the heating term for the temperature equation.

	using SystemMatrix_t 	= ImplBase_t::SystemMatrix_t;		//!< This is the system matrix that is solved.
	using Vectot_t 		= ImplBase_t::Vectot_t;			//!< This is the type of the RHS.


	/*
	 * ===========================
	 * Constructors
	 *
	 * *this only allows move and no copy.
	 * The reason is, because Eigen did not
	 * allow copying.
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is the building constructor, it is able to
	 * to set the internal structures up.
	 * This means it will allocate memeory.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 */
	egd_tempSolverExtGridBase_t(
		const uSize_t 		Ny,
		const uSize_t 		Nx);


	/**
	 * \brief	Destructor.
	 *
	 * Is virtual and defaulted.
	 */
	virtual
	~egd_tempSolverExtGridBase_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted, reason is because Eigen LU
	 * solver does not support it.
	 */
	egd_tempSolverExtGridBase_t(
		const egd_tempSolverExtGridBase_t&)
	 = delete;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is deleted, reason is because Eigen LU
	 * solver does not support it.
	 */
	egd_tempSolverExtGridBase_t&
	operator= (
		const egd_tempSolverExtGridBase_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_tempSolverExtGridBase_t(
		egd_tempSolverExtGridBase_t&&);


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_tempSolverExtGridBase_t&
	operator= (
		egd_tempSolverExtGridBase_t&&);


	/*
	 * =========================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is defaulted and protected.
	 * Id used for internal use only.
	 */
	egd_tempSolverExtGridBase_t();



	/*
	 * ========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 */
	virtual
	std::string
	print()
	 const
	 override
	 = 0;


	/*
	 * =======================
	 * Utility functions
	 *
	 * These functions are used to perform some standard tasks.
	 */
public:


	/*
	 * ==========================
	 * Internal helper functions
	 *
	 * Here are the internal helper functions
	 * which implements the solving of the equation.
	 */
private:


	/*
	 * ======================
	 * Hooks
	 */
protected:
	/**
	  * \brief	This function composes the system matrix.
	  *
	  * Note this function is a convinient function and not the
	  * one that is exposed/defined by the general solver
	  * implementation, the rason is that the heating term is
	  * missing.
	  * This function should be overwritten by the calling code.
	  *
	  * \param  grid		This is the grid container.
	  * \param  heatingTerm	This is the heating term.
	  * \param  solArg	This is the argument of the solver.
	  */

	virtual
	void
	hook_buildSystemMatrix(
		const GridContainer_t& 				grid,
		const HeatingTerm_t& 				heatingTerm,
		InterfaceBase_t::SolverArg_t* const 		solArg)
	 override;


	/**
	 * \brief	This function returns true if the solver is operating
	 * 		 on an extended grid or not.
	 *
	 * This function returns true.
	 */
	virtual
	bool
	hook_isExtendedGrid()
	 const
	 override;


	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping.
	 *
	 * The returned token should be something that is decriptive
	 * to the class instance, such as "stokesStandardSolver".
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	 = 0;


	/**
	 * \brief	This function generates a property to grid map of the needed properties.
	 *
	 * This function wwill add all properties that are needed for the extended solver.
	 */
	virtual
	PropToGridMap_t
	hook_mkTempProps()
	 const
	 override;



	/*
	 * ==========================
	 * Boundary hooks
	 *
	 * These hooks are needed for handling the boundary condition.
	 */
protected:
	/**
	 * \brief	Thid function is a hook that allows to call the boundary.
	 *
	 * The offset are applied by adding them to an index.
	 * If this function detects that it is on the boundary, then the matrix must
	 * be updated and true is returned, if the passed point is not on the
	 * boundary, false is returned.
	 *
	 * \param  i		The i index of the point; first in matrix or y spatial.
	 * \param  j 		The j index of the point; second in matrix of x spatial.
	 * \param  k_t 		The global index of that node.
	 * \param  nRows	Number of rows; equal nUnknownsY.
	 * \param  nCols	Number of cols; equal nUnknownsX.
	 * \param  UP		Offset to go one up.
	 * \param  DOWN		Offset to go one down.
	 * \param  RIGHT	Offset to go one to the right.
	 * \param  LEFT		Offset to go one to the left.
	 */
	virtual
	bool
	hook_handleBC(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_t,
		const Index_t 		nRows,
		const Index_t 		nCols,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override
	 = 0;


	/*
	 * ==================
	 * Private Members
	 * This class does not have any memebers, sinc it fully
	 * operates on the general implementation.
	 */
protected:
}; //End: class(egd_tempSolverExtGridBase_t)

PGL_NS_END(egd)


