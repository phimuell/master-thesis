# Info
This folder contains the implementation of the temperature solver.
It uses the general solver. As it was the case for the momentum solver,
boundary condition and type are connected, which is a design deficit.

There are two different implementation, one is for using the regular version.
In this version the equation is solved on the basic nodal grid.
And the second version uses the extended grid at which the extended CC grid is used.



