#pragma once
/**
 * \brief	This file implements the standard boundary.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_core/egd_eigenSparse.hpp>

#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>
#include <egd_impl/genSolver/egd_generalSolver.hpp>
#include "./egd_tempSolverBaseImpl.hpp"

#include <egd_phys/egd_heatingTerm.hpp>
#include <egd_phys/egd_boundaryCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include Eigen
#include <Eigen/SparseCore>


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_tempSolverStdBoundary_t
 * \brief	This class implement the standard boundary conditions.
 *
 * It is templated and expects that it is template is a class that
 * confermes with the standard temperature solver concept.
 *
 * This class implements boundaries such that the top and bottom
 * boundary have a prescribed temperature and left and right
 * thermal insulation is applied. Note that the dirichlet conditions
 * takes precendence over the Neumann conditions.
 *
 * Noet that the grid size (extended or not) is irrelevant for detecting
 * if something is a boundary or not, since in our case all out nodes,
 * nodes that do not have four neighbours, are boundary nodes.
 * Howver the dirichlet condition is implemnted differently, if we are
 * on the standard grid, then the condition is natuaraly inforced.
 * But if we are on the extended grid (CC) then the dirichlet conditions
 * are not so naturaly, we have to approximate them, this is done
 * by linear interpolation. The Neumann conditions are the same on
 * both grids.
 *
 * \tparam  Impl_t 	This is the class that implements everything the boundary.
 * \tparam  isExtGrid	Bool to indicate if we are on the extended boundary or not.
 */
template<
	class  	Impl_t,
	bool 	isExtGrid
>
class egd_tempSolverStdBoundary_t : public Impl_t
{
	/*
	 * =====================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using uSize_t 	= ::egd::uSize_t;		//!< Unsigned Size_t type.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using InterfaceBase_t  	= egd_TempSolver_i;			//!< This is the interface that is implemented.
	using ImplBase_t 	= Impl_t;				//!< This is the implementation of the base.

	using SolverResult_t 	= InterfaceBase_t::SolverResult_t;	//!< This is the result type of the temperature solver.
	using SolverResult_ptr 	= SolverResult_t::TempSolverResult_ptr;	//!< Pointer to the result type.

	using SolverArg_t 	= InterfaceBase_t::SolverArg_t;		//!< This is the argument type.
	using SolverArg_ptr 	= SolverArg_t::TempSolverArg_ptr;	//!< This is the managed pointer type.

	using GridContainer_t 	= egd_gridContainer_t;			//!< This is the grid container
	using GridGeometry_t    = GridContainer_t::GridGeometry_t;	//!< The geometry object.
	using HeatingTerm_t 	= egd_heatingTerm_t;			//!< This is the heating term for the temperature equation.

	using SystemMatrix_t 	= typename ImplBase_t::SystemMatrix_t;	//!< This is the system matrix that is solved.
	using Vectot_t 		= typename ImplBase_t::Vectot_t;	//!< This is the type of the RHS.

	using BoudaryCondition_t= egd_boundaryConditions_t;		//!> Class for storing boundary conditions.


	/*
	 * ===========================
	 * Constructors
	 */
public:
	/**
	 * \brief	Building constructor.
	 *
	 * This is the building constructor, it is able to
	 * to set the internal structures up.
	 * This means it will allocate memeory.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 *
	 * For the boundary the default is used, 273 at the top and 1573
	 * at the bottom.
	 */
	egd_tempSolverStdBoundary_t(
		const uSize_t 		Ny,
		const uSize_t 		Nx)
	 :
	  egd_tempSolverStdBoundary_t(Ny, Nx, 273.0, 1573.0)
	{
	};


	/**
	 * \brief	Building constructor.
	 *
	 * This is the building constructor, it is able to
	 * to set the internal structures up.
	 * This means it will allocate memeory.
	 *
	 * \param  Ny		The number of grid points in y direction.
	 * \param  Nx 		The number of grid points in x direction.
	 * \param  tempTop	Temperature at the top.
	 * \param  tempBot 	Temperature at the bottom.
	 */
	egd_tempSolverStdBoundary_t(
		const uSize_t 		Ny,
		const uSize_t 		Nx,
		const Numeric_t 	tempTop,
		const Numeric_t 	tempBot)
	 :
	  Impl_t(Ny, Nx)
	{
		//Test the temperature values
		if(pgl::isValidFloat(tempTop) == false)
		{
			throw PGL_EXCEPT_InvArg("The passed temperature at the top is invalid.");
		};
		if(tempTop < 0.0)
		{
			throw PGL_EXCEPT_InvArg("The passed temperature, " + std::to_string(tempTop) + ", is negative.");
		};
		if(pgl::isValidFloat(tempBot) == false)
		{
			throw PGL_EXCEPT_InvArg("The passed temperature at the bottom is invalid.");
		};
		if(tempBot < 0.0)
		{
			throw PGL_EXCEPT_InvArg("The passed temperature at the bottom, " + std::to_string(tempBot) + ", is negative.");
		};

		//Load the temperature values into the member variable
		m_tempTop = tempTop;
		m_tempBot = tempBot;
	}; //End full building


	/**
	 * \brief	This constructor operates on a boundary condition.
	 *
	 * \param  Ny 		The size in y direction.
	 * \param  Nx 		The size in x direction.
	 * \param  bCond	The boundary condition that is used.
	 */
	egd_tempSolverStdBoundary_t(
		const uSize_t 			Ny,
		const uSize_t 			Nx,
		const BoudaryCondition_t& 	bCond)
	 :
	  egd_tempSolverStdBoundary_t(
	  	Ny, Nx,		//Pass the size
	  	bCond.getValue(eRandLoc::TOP),		//Load the temperature
	  	bCond.getValue(eRandLoc::BOTTOM) )
	{
		/*
		 * Check if the condition si correct
		 */
		if(bCond.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed boundary condition is invalid.");
		};

		if(bCond.isFinal() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed boundary condition is not final.");
		};

		if(bCond.getPropIdx() != PropIdx::Temperature().getRepresentant())
		{
			throw PGL_EXCEPT_InvArg("The condition is not for temperature but for " + bCond.getPropIdx());
		}; //End if: no correct property

		if(bCond.isDirichlet( eRandLoc::TOP   ) == false ||
		   bCond.isInsulation(eRandLoc::RIGHT ) == false ||
		   bCond.isDirichlet( eRandLoc::BOTTOM) == false ||
		   bCond.isInsulation(eRandLoc::LEFT  ) == false   )
		{
			throw PGL_EXCEPT_InvArg("The boundary condition is invalid.");
		}; //End if: check for BC
	};//End building constructor with condition


	/**
	 * \brief	This constructor operates on a configuration object.
	 *
	 * Thsi constructor will extract the boundary condition from the
	 * configuration object.
	 *
	 * \param  confObj	The configuration object.
	 */
	egd_tempSolverStdBoundary_t(
		const egd_confObj_t& 		confObj)
	:
	  egd_tempSolverStdBoundary_t(
			confObj.getNy(),	//Get size of the domain
			confObj.getNx(),
			confObj.getBC(eRandProp::Temp) )	//Get the BC object
	{
		//If the codition is correct is checked in the derived constructor
	}; //End: config object building construction


	/**
	 * \brief	Destructor.
	 *
	 * Is virtual and defaulted.
	 */
	virtual
	~egd_tempSolverStdBoundary_t()
	 = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted, reason is because Eigen LU
	 * solver does not support it.
	 */
	egd_tempSolverStdBoundary_t(
		const egd_tempSolverStdBoundary_t&)
	 = delete;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is deleted, reason is because Eigen LU
	 * solver does not support it.
	 */
	egd_tempSolverStdBoundary_t&
	operator= (
		const egd_tempSolverStdBoundary_t&)
	 = delete;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_tempSolverStdBoundary_t(
		egd_tempSolverStdBoundary_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted, protected and noexcept.
	 */
	egd_tempSolverStdBoundary_t&
	operator= (
		egd_tempSolverStdBoundary_t&&)
	 = default;


	/*
	 * =========================
	 * Protected constructors
	 */
protected:
	/**
	 * \brief	Default constructor.
	 *
	 * Is deleted and protected.
	 */
	egd_tempSolverStdBoundary_t()
	 = delete;



	/*
	 * ========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function outputs a description of *this.
	 */
	virtual
	std::string
	print()
	 const
	 override
	{
		pgl_assert(this->hook_isExtendedGrid() == isExtGrid);

		return ((this->hook_isExtendedGrid() ? "Extended" : "Regular")
			+ std::string(" temperature solver.")
			+ " Ny = " + std::to_string(this->getNy() ) + ";"
			+ " Nx = " + std::to_string(this->getNx() ) + ";"
			+ " Temp_{top} = " + std::to_string(m_tempTop) + ";"
			+ " Temp_{bot} = " + std::to_string(m_tempBot)
	       );
	};


	/*
	 * =======================
	 * Utility functions
	 *
	 * These functions are used to perform some standard tasks.
	 */
public:


	/*
	 * ==========================
	 * Internal helper functions
	 *
	 * Here are the internal helper functions
	 * which implements the solving of the equation.
	 */
private:


	/*
	 * ======================
	 * Hooks
	 */
protected:
	/**
	 * \brief	This function returns a part of the file name
	 * 		 that is used for dumping.
	 *
	 * The returned token should be something that is decriptive
	 * to the class instance, such as "stokesStandardSolver".
	 */
	virtual
	std::string
	hook_dumpFileName()
	 const
	 override
	{
		return std::string((this->hook_isExtendedGrid() ? "Extended" : "Regular")
				+ std::string("TemperatureSolverWithStdBoundary"));
	};



	/*
	 * ==========================
	 * Boundary hooks
	 *
	 * These hooks are needed for handling the boundary condition.
	 */
protected:
	/**
	 * \brief	Thid function is a hook that allows to call the boundary.
	 *
	 * The offset are applied by adding them to an index.
	 * If this function detects that it is on the boundary, then the matrix must
	 * be updated and true is returned, if the passed point is not on the
	 * boundary, false is returned.
	 *
	 * \param  i		The i index of the point; first in matrix or y spatial.
	 * \param  j 		The j index of the point; second in matrix of x spatial.
	 * \param  k_t 		The global index of that node.
	 * \param  nRows	Number of rows; equal nUnknownsY.
	 * \param  nCols	Number of cols; equal nUnknownsX.
	 * \param  UP		Offset to go one up.
	 * \param  DOWN		Offset to go one down.
	 * \param  RIGHT	Offset to go one to the right.
	 * \param  LEFT		Offset to go one to the left.
	 */
	virtual
	bool
	hook_handleBC(
		const Index_t 		i,
		const Index_t 		j,
		const Index_t 		k_t,
		const Index_t 		nRows,
		const Index_t 		nCols,
		const Index_t 		UP,
		const Index_t 		DOWN,
		const Index_t 		RIGHT,
		const Index_t 		LEFT)
	 override
	{
		//First of all no ghost nodes are here

		//The last valid index in each direction is
		//allways a boundary node.
		const Index_t yLast = nRows - 1;
		const Index_t xLast = nCols - 1;
			pgl_assert(this->hook_isExtendedGrid() == isExtGrid);
			pgl_assert(yLast > 1, xLast > 1);
			pgl_assert(0 <= i, i < nRows,
				   0 <= j, j < nCols );
			pgl_assert(this->m_L.rows() == this->m_L.cols()  ,
				   this->m_L.rows() == this->m_rhs.size(),
				   this->m_L.rows() >  0                 ,
				   0 <= k_t, k_t < this->m_L.rows()       );



		if(((i == 0) || (i == yLast)) ||
		   ((j == 0) || (j == xLast))   )
		{

			/*
			 * We are on the boundary!
			 *
			 * We will check the top/bottom condition first.
			 * The effect is that these conditions, which are
			 * Dirichlet condition, will take precedence over
			 * the Neumann condition.
			 */
			if(i == 0 || i == yLast)
			{
				/*
				 * Now we have to implement Dirichlet conditions.
				 * Depending on the grid on which we are they differ.
				 */

				if(isExtGrid == true)
				{

					/*
					 * We have to implement dirichlet conditions.
					 * Now it is not trivial, because we have no
					 * node that is in the right position, instead
					 * we have to interpolate.
					 * With some paper we find that this boundary
					 * condition is equivalent in saying, that
					 * we force the averige between the two to
					 * a certain value.
					 */
					if(i == 0)	//Top
					{
							pgl_assert(0 <= (k_t + DOWN), (k_t + DOWN) < this->m_L.rows());
						this->m_L.coeffRef(k_t, k_t       ) = 0.5;
						this->m_L.coeffRef(k_t, k_t + DOWN) = 0.5;
						this->m_rhs[k_t]                    = m_tempTop;
					}
					else 	//Bottom
					{
							pgl_assert(0 <= (k_t + UP  ), (k_t + UP  ) < this->m_L.rows());
						this->m_L.coeffRef(k_t, k_t       ) = 0.5;
						this->m_L.coeffRef(k_t, k_t + UP  ) = 0.5;
						this->m_rhs[k_t]                    = m_tempBot;
					};
				//End if: is extended grid
				}
				else
				{
					/*
					 * Dirichlet on the regular grid is very easy.
					 * Since the node is right where it needs to be,
					 * we can simply implement the condition by forcing
					 * such a node to a specific value.
					 */
					if(i == 0) 	//Top
					{
						this->m_L.coeffRef(k_t, k_t       ) = 1.0;
						this->m_rhs[k_t]                    = m_tempTop;
					}
					else 		//Bottom
					{
						this->m_L.coeffRef(k_t, k_t       ) = 1.0;
						this->m_rhs[k_t]                    = m_tempBot;
					};
				};//End else: regular grid
			//End if: dirichlet
			}
			else
			{
				/*
				 * We are on the Neumann boundary.
				 * Here we have to force zero flux.
				 * By some simple calculations, we see
				 * that this means that the outside value
				 * has to be the same as the inside value.
				 *
				 * This is independend of the grid.
				 */
				if(j == 0)	//Left
				{
						pgl_assert(0 <= (k_t + RIGHT), (k_t + RIGHT) < this->m_L.rows());
					this->m_L.coeffRef(k_t, k_t + RIGHT) =  1.0;
					this->m_L.coeffRef(k_t, k_t        ) = -1.0;
					this->m_rhs[k_t]                     =  0.0;
				}
				else		//Right
				{
						pgl_assert(0 <= (k_t + LEFT ), (k_t + LEFT ) < this->m_L.rows());
					this->m_L.coeffRef(k_t, k_t + LEFT)  =  1.0;
					this->m_L.coeffRef(k_t, k_t       )  = -1.0;
					this->m_rhs[k_t]                     =  0.0;
				};
			}; //End else: handle Neumann boundary

			/*
			 * If we are here, we have handled a case,
			 * so we return true, to indicate that.
			 */
			return true;
		}; //End if: we have found a boundary

		/*
		 * If we are here, we are not on the boundary.
		 * So we return false, to indicate that we did notthing.
		 */
		return false;
	}; //End: handle boundary conditions


	/*
	 * ==================
	 * Private Members
	 */
private:
	Numeric_t 		m_tempBot =  273.0;	//!< Temperature at the bottom.
	Numeric_t 		m_tempTop = 1573.0;	//!< Temperature at the top.
}; //End: class(egd_tempSolverStdBoundary_t)

PGL_NS_END(egd)


