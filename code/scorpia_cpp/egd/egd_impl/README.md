# Info
In this folder the code for the concrete implementations of the interfaces are located.
In order to keep the number of files small they are further split into several folders.

## Folders
Here is the folders

### SETUP (Step 0)
This folder contains the implementation for the code that sets up the grid and markers.

### MARKER_TO_GRID (Step 1)
This folder contains the implementation of the interface that maps markers to the grid.

### MECH_SOLVER (Step 2)
This folder contains the solver for the mechanical part.
The solver should solve the Stokes equation.

### TEMP_SOLVER (Step 3)
This folder contains the solver for the temperature.
Please also read the documentation for the temperature solver interface.

### GRID_TO_MARKER (Step 4)
This folder contains the implementation of the interface that maps the new grid properties to the marker.

### RHEOLOGY (Step 5)
This folder contains the implementation of the interfaces that deals with the rheology or material model.

### INTEGRATOR
This folder contains the implementation of the interface that advect/moves markers.

### GENSOLVER
This is a general solver, that is used as a base for the other solvers.
You can consider it as a business logic layer.

### APPLY_BOUNDARY
This folder contains the implementation of the apply boundary interface.
This is the code that applies boundary to grid property, but does not store them.
The code in this folder is also connected with the code located in the egd_phys hierarchy.


