/**
 * \brief	This file contains the code for the constructor and the non hook related functions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_RK4ContinuumBased.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


/*
 * ==================
 * Integrator interface functions
 */
std::string
egd_markerIntegratorRK4Std_t::print()
 const
{
	return (std::string("Standard RK4 Integrator, with continuity velocity field.")
			+ " VelX BC: " + this->getBCVelX().print() + ";"
			+ " VelY BC: " + this->getBCVelY().print() + ";"
		);
}; //End: print


/*
 * ====================
 * Internal query functions
 */
const egd_markerIntegratorRK4Std_t::ExtVelField_t&
egd_markerIntegratorRK4Std_t::getVelYCC()
 const
{
	return this->m_extVy;
};


const egd_markerIntegratorRK4Std_t::ExtVelField_t&
egd_markerIntegratorRK4Std_t::getVelXCC()
 const
{
	return this->m_extVx;
};


/*
 * =======================
 * Constructor
 */
egd_markerIntegratorRK4Std_t::egd_markerIntegratorRK4Std_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const BoundaryCondition_t& 	bcVelX,
	const BoundaryCondition_t& 	bcVelY,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Ny, Nx, bcVelX, bcVelY, confObj),				//setup the base
  m_extVx(Ny, Nx, mkExt(eGridType::CellCenter), PropIdx::VelXCC()),	//set up the internals
  m_extVy(Ny, Nx, mkExt(eGridType::CellCenter), PropIdx::VelYCC())
{
	/* Handle the grid */
	if(Ny < Index_t(3) || Nx < Index_t(3))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points is wrong.");
	}; //End if: check the size
}; //End: constructor


egd_markerIntegratorRK4Std_t::egd_markerIntegratorRK4Std_t(
	const egd_confObj_t& 	confObj)
 :
  egd_markerIntegratorRK4Std_t(
  		yNodeIdx_t(confObj.getNy()),
  		xNodeIdx_t(confObj.getNx()),
  		confObj.getBC(eRandProp::VelX),
  		confObj.getBC(eRandProp::VelY),
  		&confObj)
{};


egd_markerIntegratorRK4Std_t::~egd_markerIntegratorRK4Std_t()
 = default;


egd_markerIntegratorRK4Std_t::egd_markerIntegratorRK4Std_t(
	const egd_markerIntegratorRK4Std_t&)
 = default;


egd_markerIntegratorRK4Std_t&
egd_markerIntegratorRK4Std_t::operator= (
	const egd_markerIntegratorRK4Std_t&)
 = default;


egd_markerIntegratorRK4Std_t::egd_markerIntegratorRK4Std_t(
	egd_markerIntegratorRK4Std_t&&)
 noexcept
 = default;


egd_markerIntegratorRK4Std_t&
egd_markerIntegratorRK4Std_t::operator= (
	egd_markerIntegratorRK4Std_t&&)
 = default;


PGL_NS_END(egd)

