#pragma once
/**
 * \brief	This file implements a container for storing the velcoities of the cells.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>

#include "./egd_MarkerIntegrator_jennyMeyer_cornerVelocities.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \brief	This class implements a storage for teh velocities in cells.
 * \class 	egd_jmICellVel_t
 *
 * This class is a container for storing the corner velocities of all cells in a domain.
 * It wraps a vector of corner velocities and provides syntactic sugar.
 * Note that it is cell based and thus has less points, and a different strinde, then
 * the grid properties.
 */
class egd_jmICellVel_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using PropIdx_t = egd_propertyIndex_t;	//!< The property index.


	using CornerVelocity_t = egd_jmICornerVel_t;			//!< This is the corner velocity type.
	using CornerVelArray_t = std::vector<CornerVelocity_t>;		//!< This is the array for corner velocity.
	using iterator 	       = CornerVelArray_t::iterator;		//!< Iterator for the corner velocity array.
	using const_iterator   = CornerVelArray_t::const_iterator;	//!< Constant iterator for the corner velcoity array.


	/*
	 * =======================
	 * Constructors
	 */
public:
	/**
	 * \brief	The building constructor of this.
	 *
	 * This constructor will construct a consistent, meaning the sizes
	 * are correct, but invalid, meaning the corner velocities are NAN,
	 * object.
	 *
	 * It is important that this class is cell based, and thus the number
	 * of cells must be passed to its constructor. For the basic nodal
	 * grid this means one less than nodal points.
	 *
	 * \param  nCellY	Number of cells in y direction.
	 * \param  nCellX	Number of cells in x direction.
	 * \param  velComp	Which velocity component.
	 *
	 * \note	In a previous version, basic nodal points where used,
	 * 		 this was changed to cells.
	 */
	egd_jmICellVel_t(
		const yNodeIdx_t 	nCellY,
		const xNodeIdx_t 	nCellX,
		const PropIdx_t 	velComp);


	/**
	 * \brief	Default constructor is forbidden.
	 */
	egd_jmICellVel_t()
	 = delete;


	/**
	 * \brief	Copy constructor
	 */
	egd_jmICellVel_t(
		const egd_jmICellVel_t&);


	/**
	 * \brief	Move constructor
	 */
	egd_jmICellVel_t(
		egd_jmICellVel_t&&)
	 noexcept;


	/**
	 * \brief	Copy assignment
	 */
	egd_jmICellVel_t&
	operator= (
		const egd_jmICellVel_t&);


	/**
	 * \brief	Move assignment
	 */
	egd_jmICellVel_t&
	operator= (
		egd_jmICellVel_t&&);


	/**
	 * \brief	Destructor
	 */
	~egd_jmICellVel_t()
	 noexcept;


	/*
	 * =======================
	 * Quering Functions
	 */
public:
	/**
	 * \brief	This function returns the number of cells in x direction.
	 */
	Index_t
	nCellX()
	 const
	 noexcept
	{
		pgl_assert(this->m_nCellX >= 1);
		return this->m_nCellX;
	};


	/**
	 * \brief	This function returns the number of cells in y direction.
	 */
	Index_t
	nCellY()
	 const
	 noexcept
	{
		pgl_assert(this->m_nCellY >= 1);
		return this->m_nCellY;
	};


	/**
	 * \brief	Returns teh number of cells that are stored.
	 *
	 * This function is not safe to call after *this was cleared.
	 * To test if *this was cleared or not use the isClear() function.
	 */
	Size_t
	nCell()
	 const
	 noexcept
	{
		pgl_assert(this->m_nCellX >= 1,
			   this->m_nCellY >= 1,
			   uSize_t(this->m_nCellX * this->m_nCellY) == this->m_cornerVels.size());
		return this->m_cornerVels.size();
	};


	/**
	 * \brief	This function returns true if *this was cleared.
	 *
	 * *this is cleared if the number of cells that should be here are
	 * valid, but the size of the underliying container is zero.
	 *
	 * If this function returns clear, it is safe to reallocate *this.
	 */
	bool
	isClear()
	 const
	 noexcept
	{
		pgl_assert(this->m_nCellX >= 1,
			   this->m_nCellY >= 1,
			   pgl_implies(this->m_cornerVels.empty() == false, uSize_t(this->m_nCellX * this->m_nCellY) == this->m_cornerVels.size()) );

		return this->m_cornerVels.empty();
	}; //End: isClear


	/**
	 * \brief	This function returns the property of *this.
	 *
	 * This is guaranteed to be a velocity representant.
	 */
	PropIdx_t
	getProp()
	 const
	 noexcept
	{
		pgl_assert(this->m_prop.isRepresentant(),
			   this->m_prop.isVelocity()     );
		return this->m_prop;
	};


	/**
	 * \brief	Returns true if *this is the x velocity.
	 */
	bool
	isVelX()
	 const
	 noexcept
	{
		pgl_assert(this->m_prop.isRepresentant(),
			   this->m_prop.isVelocity()     );
		return this->m_prop.isVelX();
	};


	/**
	 * \brief	Returns true if *this is the y velocity.
	 */
	bool
	isVelY()
	 const
	 noexcept
	{
		pgl_assert(this->m_prop.isRepresentant(),
			   this->m_prop.isVelocity()     );
		return this->m_prop.isVelY();
	};



	/*
	 * =======================
	 * Status Functions
	 */
public:
	/**
	 * \brief	This function checks if *this is consistent.
	 *
	 * A consistent check is fast and superficial, it will check if the number
	 * of cells is invalid or the property is wrong, but it will not check
	 * the corner velocities.
	 */
	bool
	isConsistent()
	 const
	 noexcept;


	/**
	 * \brief	This function checks if *this is valid.
	 *
	 * *this is valid if it is consistent and all cells are valid too.
	 */
	bool
	isValid()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if *this is invalid.
	 */
	bool
	isInValid()
	 const
	 noexcept
	{
		return (this->isValid() == false);
	};



	/*
	 * =============================
	 * Access functions
	 */
public:
	/**
	 * \brief	This function returns a reference to the corner velocity
	 * 		 object for cell (i, j).
	 *
	 * Important i is the y coordinate and j is the x coordinate, so it is
	 * swapped. This function is only a consatnt reference, for getting a
	 * mutable reference, you must use the get function.
	 *
	 * \param  i		The y coordinate of the cell (rows).
	 * \param  j 		The x coordinate of the cell (columns).
	 */
	const CornerVelocity_t&
	operator() (
		const Index_t 		i,
		const Index_t 		j)
	 const
	{
		if((i < 0) || (i >= this->m_nCellY))
		{
			throw PGL_EXCEPT_OutOfBound("i index is too large, it was " + std::to_string(i)
					+ ", but only " + std::to_string(this->m_nCellY) + " are pressent.");
		};
		if((j < 0) || (j >= this->m_nCellX))
		{
			throw PGL_EXCEPT_OutOfBound("j index is too large, it was " + std::to_string(j)
					+ ", but only " + std::to_string(this->m_nCellX) + " are pressent.");
		};

		const Index_t k = i * (this->m_nCellX) + j;
			pgl_assert(        k  >= 0                        ,
				   uSize_t(k) <  this->m_cornerVels.size() );

		return this->m_cornerVels[k];
	}; //End: operator(i, j)


	/**
	 * \brief	This function returns a mutable reference to the corner
	 * 		 velocity object of cell (i, j).
	 *
	 * See the operator(i, j) function for a discription.
	 *
	 * \param  i		The y coordinate of the cell (rows).
	 * \param  j 		The x coordinate of the cell (columns).
	 */
	CornerVelocity_t&
	get(
		const Index_t 		i,
		const Index_t 		j)
	{
		if((i < 0) || (i >= this->m_nCellY))
		{
			throw PGL_EXCEPT_OutOfBound("i index is too large, it was " + std::to_string(i)
					+ ", but only " + std::to_string(this->m_nCellY) + " are pressent.");
		};
		if((j < 0) || (j >= this->m_nCellX))
		{
			throw PGL_EXCEPT_OutOfBound("j index is too large, it was " + std::to_string(j)
					+ ", but only " + std::to_string(this->m_nCellX) + " are pressent.");
		};

		const Index_t k = i * (this->m_nCellX) + j;
			pgl_assert(        k  >= 0                        ,
				   uSize_t(k) <  this->m_cornerVels.size() );

		return this->m_cornerVels[k];
	}; //End: operator(i, j)


	/**
	 * \brief	This function returns a const reference to the corner
	 * 		 velocity object of cell (i, j).
	 *
	 * This is the constant versaion of the get function.
	 * However it is an alias of the operator(i, j) function.
	 * See the operator(i, j) function for a discription.
	 *
	 * \param  i		The y coordinate of the cell (rows).
	 * \param  j 		The x coordinate of the cell (columns).
	 */
	const CornerVelocity_t&
	get(
		const Index_t 		i,
		const Index_t 		j)
	 const
	{
		return this->operator()(i, j);
	};


	/**
	 * \brief	This is the explcit constant version of the get function.
	 *
	 * \param  i		The y coordinate of the cell (rows).
	 * \param  j 		The x coordinate of the cell (columns).
	 */
	const CornerVelocity_t&
	cget(
		const Index_t 		i,
		const Index_t 		j)
	 const
	{
		return this->operator()(i, j);
	};


	/*
	 * ========================
	 * Managing
	 */
public:
	/**
	 * \brief	This function reset the velocity information of *this.
	 *
	 * This function will set all velocity informations to NAN. *this is
	 * then equivalent as if it was constzructed. The size is not affected.
	 *
	 * A reference to *this is returned.
	 */
	egd_jmICellVel_t&
	resetCornerVel();


	/**
	 * \brief	This function clears the memeory.
	 *
	 * Note that this function is not guaranteed to free the memory.
	 * This function calls the clear function of teh underlying container,
	 * and thus no guarantee of releasing the memory is given.
	 * Also note that the size of the grid is not modified.
	 *
	 * Note that such an object is not consistent.
	 */
	egd_jmICellVel_t&
	clear();


	/**
	 * \brief	This function allows to resize the velocity container.
	 *
	 * This function allows in essence to redo construction, but with
	 * different size parameters. Thus it expects the nunmber of cells
	 * in y and x direction as input arguments.
	 * Note that this function only works if *this was cleared before.
	 * All corener velocities object will be invalid.
	 *
	 * \param  nBasicY	Number of cells in y direction.
	 * \param  nBasicX	Number of cells in x direction.
	 */
	void
	resize(
		const yNodeIdx_t 	nCellY,
		const xNodeIdx_t 	nCellX);



	/*
	 * =======================
	 * Private members
	 */
private:
	Index_t 		m_nCellY = -1;		//!< Numbers of cells in the y direction.
	Index_t 		m_nCellX = -1;		//!< Numbers of cells in the x direction.
	PropIdx_t 		m_prop;			//!< This is the propertyx that is stored here.
	CornerVelArray_t 	m_cornerVels;		//!< Array of corner velocities.
}; //End class(egd_jmICellVel_t):


PGL_NS_END(egd)


