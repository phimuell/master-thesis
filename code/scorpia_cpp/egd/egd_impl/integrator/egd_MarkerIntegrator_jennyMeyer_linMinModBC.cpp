/**
 * \brief	This file contains the code for the linear minmod limiting interpolation base.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include "./egd_MarkerIntegrator_jennyMeyer_cornerVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyer_cellVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyerBase.hpp"
#include "./egd_MarkerIntegrator_jennyMeyer_linMinModBC.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_set.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


/*
 * ================
 * Interface Hook
 */
std::string
egd_markerIntegrator_jmILinMinMod_t::print()
 const
{
	return (std::string("Leaner MinMod jm Integrator.")
			+ " VelX BC: " + this->getBCVelX().print() + ";"
			+ " VelY BC: " + this->getBCVelY().print() + ";"
		);
}; //End: print


/*
 * =====================
 * Jenny-Meyer-Hooks
 */
void
egd_markerIntegrator_jmILinMinMod_t::hook_computeCellVelocities(
	const MechSolverResult_t&	mechSol,
	const GridGeometry_t& 		gridGeo,
	const ApplyBC_t& 		applyBC)
{
	/*
	 * We will split this in two steps. First computing the x component and
	 * then computing the y component. This is a bit in efficient, but it
	 * makes things clearer
	 */
	if(mechSol.Nx() <= 3 ||
	   mechSol.Ny() <= 3   )
	{
		throw PGL_EXCEPT_InvArg("The grid is too small.");
	};

	/*
	 * Loading some parameters
	 */
	const Index_t nBasicX = mechSol.Nx();	//Basic nodal points
	const Index_t nBasicY = mechSol.Ny();
	const Index_t nCellX  = nBasicX - 1;	//Numbr of cells
	const Index_t nCellY  = nBasicY - 1;

	//
	//Load grid positions
	NodePositions_t VXPos_x_tmp, VXPos_y_tmp, VYPos_x_tmp, VYPos_y_tmp;
	std::tie(VXPos_x_tmp, VXPos_y_tmp) = gridGeo.getGridPoints(mechSol.getVelX().getType());	//it is not important if extended or regular
	std::tie(VYPos_x_tmp, VYPos_y_tmp) = gridGeo.getGridPoints(mechSol.getVelY().getType());
	const NodePositions_t VXPos_x = std::move(VXPos_x_tmp);
	const NodePositions_t VXPos_y = std::move(VXPos_y_tmp);
	const NodePositions_t VYPos_x = std::move(VYPos_x_tmp);
	const NodePositions_t VYPos_y = std::move(VYPos_y_tmp);

	const NodePositions_t BasicPosX = gridGeo.getXBasicPos();	//get the basic nodal points
	const NodePositions_t BasicPosY = gridGeo.getYBasicPos();

	const BoundaryCondition_t& bcVelX = this->getBCVelX();	pgl_assert(bcVelX.isValid() );		//Boundaries
	const BoundaryCondition_t& bcVelY = this->getBCVelY();  pgl_assert(bcVelY.isValid() );

	const GridProperty_t& VX = mechSol.getVelX();		//The velocities
	const GridProperty_t& VY = mechSol.getVelY();

	{
		/*
		 * Althought only some of the Vx nodes that lies outside the computational
		 * domain have no meaning, we will only use the nodes that are inside or
		 * at the boundary. The x BC is fulfiled exactly already, so we have only to
		 * care about the y condition. which we will use for filling the gaps.
		 *
		 * Because of the staggered grid, the coundary condition normal to *this
		 * is handled by the solver, we only have to consider the tangenial condition.
		 */

		//prepare the result container
		if(this->m_cellVelX.isVelX() == false)
		{
			throw PGL_EXCEPT_LOGIC("The cell container for the x component is not an x velocity but " + this->m_cellVelX.getProp());
		};
		if((this->m_cellVelX.nCellX() != nCellX) ||
		   (this->m_cellVelX.nCellY() != nCellY) ||
		   (this->m_cellVelX.isClear()         )   )
		{
			if(this->m_cellVelX.isClear() == false)		//if not clear clear it
			{
				this->m_cellVelX.clear();
			};
			this->m_cellVelX.resize(yNodeIdx_t(nCellY), xNodeIdx_t(nCellX) );	//perform the resizing
		};

		/*
		 * We will now process the cells from UP, low y values, to DOWN, larger y values.
		 */
		for(Index_t xCell = 0; xCell != nCellX; ++xCell)
		{
			for(Index_t yCell = 0; yCell != nCellY; ++yCell)
			{
				/*
				 * This is a general cell
				 *
				 *  o---------------> X/J
				 *  |
				 *  |  a x------------------x b
				 *  |    |                  |           D
				 *  |    |                  |           O
				 *  |    |                  |           W
				 *  |    O Vx_l             O Vx_r      N
				 *  |    |                  |
				 *  |    |                  |           |
				 *  |    |                  |           V
				 *  |  c x------------------x d
				 *  |
				 *  V Y/I
				 *
				 * 'O' are the VX nodes, we want now to compute the nodes in the corners,
				 * denoted by 'x'. Note that the arangemont of the naming is a bit different
				 * than in the papers, because of the special orioentation of the grid.
				 *
				 * First of all the pure math is the same on the left as it is on the right,
				 * but not the calculation, so we will focuse on a generic side.
				 *
				 * We want now estimate a reconstruction of the velocty in the cell where
				 * Vx_{i} ios located in, i.e. we want to calculate values at the cell interfaces.
				 * Note that we only deal with one cell, and in generall we have discontinuities.
				 *
				 *   A  Vx
				 *   |               a/b              c/d
				 *   |                |                |
				 *   |                |        o       |
				 *   |                |                |         o
				 *   |        o       |                |                    DOWN ->
				 *   |                |                |
				 *   o--------x-------|--------x-------|---------x------------------------> y
				 *          Vx_{i-1}         Vx_{i}            Vx_{i+1}
				 *                          Vx_{l/r}
				 *
				 * We want to perform a linear reconstruction, this is a linear function inside
				 * the cell. We have tow possibilities, first we could estimate a linear function
				 * with the slope determining by the values $(Vx_{i-1}, Vx_{i})$ or we could use.
				 * the values $(Vx_{i}, Vx_{i+1})$.
				 * We will use the minmod limiter to select an apropriate value.
				 * See figure 12 in JENNY 2001, for the minmod calculation we use equation
				 * (61) & (62) in the same paper.
				 *
				 *
				 * We have to translate the cell ID to the ID of the different nodes we need.
				 * By convention the cell ID always referes to the basic grid point in the
				 * top left corner, in the picture this would be 'a'. The j index is the same
				 * for all values at the left side and the same holds true for the right side.
				 * We also see from the picture above, that the Vx_l is associated to basic
				 * nodal point 'c' and Vx_r is associated to node 'd'.
				 */

				// INDEX COMPUTATION
				//	note that some of them may be not defined, here
				const Index_t i_VxL   = yCell + 1;	//The VX node Vx_l is assoiated to the basic nodal point 'c'
				const Index_t i_VxL_u = i_VxL - 1;	//This node that is in the upper cell, less y, this is the node that is accossiated to 'a'.
				const Index_t i_VxL_d = i_VxL + 1;	//The VX node that is down the node, larger y, bn is not shown on picture, but would be $Vx_{i+1}$
				const Index_t j_VxL   = xCell + 0;	//There is no modification, since the column is the same
				const Index_t j_VxL_d = j_VxL;		// is the same for all nodex on the left side
				const Index_t j_VxL_u = j_VxL;

				//For the right side it is the samelogic, but now j is different
				const Index_t i_VxR   = yCell + 1;	//As with the left case, we have to add one.
				const Index_t i_VxR_u = i_VxR - 1;	//We must move one up, so less because of y orientation
				const Index_t i_VxR_d = i_VxR + 1;	//we go down.
				const Index_t j_VxR   = xCell + 1;	//Now we have to add one
				const Index_t j_VxR_u = j_VxR;		// But is still the same for all on this side
				const Index_t j_VxR_d = j_VxR;


				//This are the result values
				Numeric_t U_a = NAN, U_b = NAN, U_c = NAN, U_d = NAN;

				if(yCell == 0)
				{
					/*
					 * We are in the top row, this means that 'a' and 'b' are directly
					 * determined by the boundary condition and the other two are
					 * influenced by it. Also note that the _u values does not exists, or
					 * are ghost nodes.
					 */
					const Numeric_t VxL   = VX(i_VxL  , j_VxL  );		//get velocities
					const Numeric_t VxL_d = VX(i_VxL_d, j_VxL_d);
					const Numeric_t VxR   = VX(i_VxR  , j_VxR  );
					const Numeric_t VxR_d = VX(i_VxR_d, j_VxR_d);

					const auto& bcObj  = bcVelX[eRandLoc::TOP];
					const auto  bcType = bcObj.getKind();
					if(bcType == eRandKind::Dirichlet)
					{
						throw PGL_EXCEPT_illMethod("This functionality is not implemented.");
					}
					else if(bcType == eRandKind::freeSlip)
					{
						/* It is free splip, this means the derivate at the boundary is zero
						 * and so is the slope from the _u side. Now what is the slope from
						 * the _d side? The answer is that it does not matter. If one looks
						 * at the definition on the min mod limiter we see that a zero slope
						 * estimate will always dominate, since it is _the_ smallest value
						 * in magnitude that is possible.
						 * So the slope is zero in the entiere cell and all corner values
						 * have the same values as the central nodes. */
						U_c = VxL;	//left
						U_a = VxL;
						U_d = VxR;	//right
						U_b = VxR;
					}
					else
					{
						throw PGL_EXCEPT_RUNTIME("Encountered an unkown boundary condition " + bcType);
					};
					(void)VxL_d;
					(void)VxR_d;
				//End: handling top row
				}
				else if(yCell == (nCellY - 1))
				{
					/*
					 * We are in the bottom row, this means that are 'c' and 'd' are directly
					 * determined by the boundary condition and the other twos are influenced
					 * by it. Also the uper nodes will exists and the down nodes do not
					 */
					const Numeric_t VxL   = VX(i_VxL  , j_VxL  );		//get velocities
					const Numeric_t VxL_u = VX(i_VxL_u, j_VxL_u);
					const Numeric_t VxR   = VX(i_VxR  , j_VxR  );
					const Numeric_t VxR_u = VX(i_VxR_u, j_VxR_u);

					const auto& bcObj  = bcVelX[eRandLoc::BOTTOM];
					const auto  bcType = bcObj.getKind();
					if(bcType == eRandKind::Dirichlet)
					{
						throw PGL_EXCEPT_illMethod("This functionality is not implemented.");
					}
					else if(bcType == eRandKind::freeSlip)
					{
						/* Here we have the same logic as in the top row case.
						 * The slope in the cell is zero, so all corner values
						 * are the same as the central value. */
						U_c = VxL;
						U_a = VxL;
						U_d = VxR;
						U_b = VxR;
					}
					else
					{
						throw PGL_EXCEPT_RUNTIME("Encountered an unkown boundary condition " + bcType);
					};
					(void)VxL_u;
					(void)VxR_u;
				//End: handling top row
				}
				else
				{
					/*
					 * Here we are not in the bottom row nor the top row.
					 * This means that we can do normal minmod interpolation.
					 * It also means that all the positions and nodes exists.
					 * In this setting only the y components are important.
					 */

					//load velocity
					const Numeric_t VxL   = VX(i_VxL  , j_VxL  );
					const Numeric_t VxL_d = VX(i_VxL_d, j_VxL_d);
					const Numeric_t VxL_u = VX(i_VxL_u, j_VxL_u);
					const Numeric_t VxR   = VX(i_VxR  , j_VxR  );
					const Numeric_t VxR_d = VX(i_VxR_d, j_VxR_d);
					const Numeric_t VxR_u = VX(i_VxR_u, j_VxR_u);

					//load position
					const Numeric_t yR    = VXPos_y[i_VxL  ];		//y-position of the velocity node in the current cell
					const Numeric_t yL    = yR;
					const Numeric_t yR_d  = VXPos_y[i_VxL_d];		//y-position of velocity node Vx_{i+1}
					const Numeric_t yL_d  = yR_d;
					const Numeric_t yR_u  = VXPos_y[i_VxL_u];		//y-position of velcoity node Vx_{i-1}
					const Numeric_t yL_u  = yR_u;
					const Numeric_t yB_ab = BasicPosY[yCell    ];		//y-position of corner a and b.
					const Numeric_t yB_cd = BasicPosY[yCell + 1];		//y position of corner c and d.
						pgl_assert(yR_u < yR   , yR    <= yR_d);
						pgl_assert(yL_u < yL   , yL    <= yL_d);
						pgl_assert(yL_u < yB_ab, yB_ab <  yL,
							   yL   < yB_cd, yB_cd <  yL_d);
						pgl_assert(yR_u < yB_ab, yB_ab <  yR,
							   yR   < yB_cd, yB_cd <  yR_d);

					/* Now we compute the slope */
					const Numeric_t slopeL_u = (VxL   - VxL_u) / (yL   - yL_u);	pgl_assert(::pgl::isValidFloat(slopeL_u));
					const Numeric_t slopeL_d = (VxL_d - VxL  ) / (yL_d - yL  );	pgl_assert(::pgl::isValidFloat(slopeL_d));
					const Numeric_t slopeL   = pgl::minmod(slopeL_d, slopeL_u);
					const Numeric_t slopeR_u = (VxR   - VxR_u) / (yR   - yR_u);	pgl_assert(::pgl::isValidFloat(slopeL_u));
					const Numeric_t slopeR_d = (VxR_d - VxR  ) / (yR_d - yR  );	pgl_assert(::pgl::isValidFloat(slopeL_d));
					const Numeric_t slopeR   = pgl::minmod(slopeR_d, slopeR_u);

					/* now we compute the value in the positions */
					U_a = VxL + slopeL * (yB_ab - yL);	//left side
					U_c = VxL + slopeL * (yB_cd - yL);
					U_b = VxR + slopeR * (yB_ab - yR);	//right side
					U_d = VxR + slopeR * (yB_cd - yR);
				}; //End: normal minmod


				/*
				 * Storing the value corner velocity
				 */
				CornerVelocities_t thisCell(U_a, U_b, U_c, U_d);	pgl_assert(thisCell.isValid());
				this->m_cellVelX.get(yCell, xCell) = thisCell;
			}; //End for(yCell): going through the rows
		}; //End for(xCell): going through the columns

		pgl_assert(this->m_cellVelX.isValid());
	}; //End scope: x component


	{
		/*
		 * Here we compue the y components of the velocity. This is fairly similar to the x case,
		 * but there are some differences in details. So the discussion will only highlight them.
		 *
		 * Because of the staggered grid, the coundary condition normal to *this is handled by
		 * the solver, we only have to consider the tangenial condition.
		 */

		//prepare the result container
		if(this->m_cellVelY.isVelY() == false)
		{
			throw PGL_EXCEPT_LOGIC("The cell container for the y component is not an y velocity but " + this->m_cellVelY.getProp());
		};
		if((this->m_cellVelY.nCellX() != nCellX) ||
		   (this->m_cellVelY.nCellY() != nCellY) ||
		   (this->m_cellVelY.isClear()         )   )
		{
			if(this->m_cellVelY.isClear() == false)		//if not clear clear it
			{
				this->m_cellVelY.clear();
			};
			this->m_cellVelY.resize(yNodeIdx_t(nCellY), xNodeIdx_t(nCellX) );	//perform the resizing
		};


		/*
		 * We now iterate through the CELLS. We will do so by processing
		 * along x, so we will first go to the RIGHT and then DOWN.
		 */
		for(Index_t yCell = 0; yCell != nCellY; ++yCell)
		{
			for(Index_t xCell = 0; xCell != nCellX; ++xCell)
			{
				/*
				 * This is a general cell
				 *
				 *  o---------------> X/J
				 *  |
				 *  |           Vy_u
				 *  |  a x--------O---------x b
				 *  |    |                  |
				 *  |    |                  |
				 *  |    |                  |
				 *  |    |                  |       RIGHT ->
				 *  |    |                  |
				 *  |    |                  |
				 *  |    |       Vy_d       |
				 *  |  c x--------O---------x d
				 *  |
				 *  V Y/I
				 *
				 * The 'O' are VY nodes. We first again note, that up and down, the same functional
				 * math applies. Also note the special orrientation of the corners, this is due to
				 * the orientation of the grid itself.
				 *
				 *
				 * Here s scetch of the situation, that we have.
				 *
				 *   A  Vy
				 *   |               a/c              b/d
				 *   |                |                |
				 *   |                |        o       |
				 *   |                |                |         o
				 *   |        o       |                |                    RIGHT ->
				 *   |                |                |
				 *   o--------x-------|--------x-------|---------x------------------------> x
				 *          Vy_{j-1}         Vy_{j}            Vy_{j+1}
				 *                          Vy_{u/d}
				 *
				 * We we can then estimate the values at the corner points, as we have done it in the
				 * other case, so please see there.
				 *
				 * Something that has to be done is the translation from the cell ID to grid node ID.
				 * As by convention, the cell ID does refere to the top left node, 'a', this is similar
				 * to the Vx case, we have handled before.
				 * It is so that Vy_u is assoicate to the basic nodal point 'b' and Vy_d is assoiated to
				 * node 'd'.
				 */
				// INDEX COMPUTATION
				//	note that some of them may be not defined, here
				const Index_t j_VyU   = xCell + 1;
				const Index_t j_VyU_l = j_VyU - 1;		//this is left of _u, in sketch this is Vy_{j-1}
				const Index_t j_VyU_r = j_VyU + 1;		//this is right of _u, in sketch this is Vy_{j+1}
				const Index_t i_VyU   = yCell;			//all have the same i coordninate
				const Index_t i_VyU_l = i_VyU;
				const Index_t i_VyU_r = i_VyU;

				//down side
				const Index_t j_VyD   = xCell + 1;		//is on deeper
				const Index_t j_VyD_l = j_VyD - 1;		//left to the node, this is Vy_{j-1}
				const Index_t j_VyD_r = j_VyD + 1;		//right to the node, this is Vy_{j+1}
				const Index_t i_VyD   = yCell + 1;		//we are one row deeper, but still the same
				const Index_t i_VyD_l = i_VyD; 			// for all nodes in the down row
				const Index_t i_VyD_r = i_VyD;

				//This are the result values
				Numeric_t U_a = NAN, U_b = NAN, U_c = NAN, U_d = NAN;

				if(xCell == 0)
				{
					/* We are at a cell that at the left boundary. So we have
					 * to use the initial condition. This is conseptually the
					 * same as it was for the x case, so we we refere to that
					 * code section for more information */

					const Numeric_t VyU = VY(i_VyU, j_VyU);		//load value of the velocity
					const Numeric_t VyD = VY(i_VyD, j_VyD);

					const auto& bcObj  = bcVelY[eRandLoc::LEFT];	//load the boundary condition
					const auto  bcType = bcObj.getKind();

					if(bcType == eRandKind::Dirichlet)
					{
						throw PGL_EXCEPT_illMethod("This functionality is not implemented.");
					}
					else if(bcType == eRandKind::freeSlip)
					{
						U_a = VyU;	//Upper
						U_b = VyU;
						U_c = VyD;	//down
						U_d = VyD;
					}
					else
					{
						throw PGL_EXCEPT_RUNTIME("Encountered an unkown boundary condition " + bcType);
					};
				//End: handling left boundary
				}
				else if(xCell == (nCellX - 1))
				{
					/* We are at the cell that is most right. Here the same reasoning appies
					 * as before (in Vx and in the other boundary, so we refere to that section */

					const Numeric_t VyU = VY(i_VyU, j_VyU);		//load value of the velocity
					const Numeric_t VyD = VY(i_VyD, j_VyD);

					const auto& bcObj  = bcVelY[eRandLoc::RIGHT];	//load the boundary condition
					const auto  bcType = bcObj.getKind();

					if(bcType == eRandKind::Dirichlet)
					{
						throw PGL_EXCEPT_illMethod("This functionality is not implemented.");
					}
					else if(bcType == eRandKind::freeSlip)
					{
						U_a = VyU;	//Upper
						U_b = VyU;
						U_c = VyD;	//down
						U_d = VyD;
					}
					else
					{
						throw PGL_EXCEPT_RUNTIME("Encountered an unkown boundary condition " + bcType);
					};
				//End: handling right boundary
				}
				else
				{
					/*
					 * We are in an incide cell, this means we can use minmod interpolation for
					 * reconstructing the boundary value. This is technically the same as it is
					 * for the VX case, so we refere to that section */

					//Loading values
					const Numeric_t VyU   = VY(i_VyU  , j_VyU  );	//this is Vy_{j}
					const Numeric_t VyU_l = VY(i_VyU_l, j_VyU_l);	//this is Vy_{j-1}
					const Numeric_t VyU_r = VY(i_VyU_r, j_VyU_r);	//this is Vy_{j+1}
					const Numeric_t VyD   = VY(i_VyD  , j_VyD  );
					const Numeric_t VyD_l = VY(i_VyD_l, j_VyD_l);
					const Numeric_t VyD_r = VY(i_VyD_r, j_VyD_r);

					//loading node positions
					const Numeric_t xVYU   = VYPos_x[j_VyU  ];	//node positions UP
					const Numeric_t xVYU_l = VYPos_x[j_VyU_l];
					const Numeric_t xVYU_r = VYPos_x[j_VyU_r];
					const Numeric_t xVYD   = VYPos_x[j_VyD  ];	//node positions DOWN
					const Numeric_t xVYD_l = VYPos_x[j_VyD_l];
					const Numeric_t xVYD_r = VYPos_x[j_VyD_r];
					const Numeric_t xBG_ac = BasicPosX[xCell    ];	//x-position of corner a & c
					const Numeric_t xBG_db = BasicPosX[xCell + 1];  //x-position of corner b & d
						pgl_assert(xVYD_l < xBG_ac, xBG_ac < xVYD,
							   xVYD   < xBG_db, xBG_db < xVYD_r);
						pgl_assert(xVYU_l < xBG_ac, xBG_ac < xVYU,
							   xVYU   < xBG_db, xBG_db < xVYU_r);

					/*  We now compute the slopes */
					const Numeric_t slopeU_l = (VyU   - VyU_l) / (xVYU   - xVYU_l);	//slope from the left
					const Numeric_t slopeU_r = (VyU_r - VyU  ) / (xVYU_r - xVYU  ); //slope from the left
					const Numeric_t slopeU   = pgl::minmod(slopeU_l, slopeU_r);	//select the slope
					const Numeric_t slopeD_l = (VyD   - VyD_l) / (xVYD   - xVYD_l);
					const Numeric_t slopeD_r = (VyD_r - VyD  ) / (xVYD_r - xVYD  );
					const Numeric_t slopeD   = pgl::minmod(slopeD_l, slopeD_r);

					/* Calculate the velocity */
					U_a = VyU + slopeU * (xBG_ac - xVYU);
					U_b = VyU + slopeU * (xBG_db - xVYU);
					U_c = VyD + slopeD * (xBG_ac - xVYD);
					U_d = VyD + slopeD * (xBG_db - xVYD);
				//End: interpolating with minmod
				};


				/*
				 * Storing the value corner velocity
				 */
				CornerVelocities_t thisCell(U_a, U_b, U_c, U_d);	pgl_assert(thisCell.isValid());
				this->m_cellVelY.get(yCell, xCell) = thisCell;
			}; //End for(xCell):
		}; //End for(yCell):

		pgl_assert(this->m_cellVelY.isValid());
	}; //End scope: computing y component

	return;
	(void)applyBC;
};//End interpolator



/*
 * ======================
 * Constructors
 */
egd_markerIntegrator_jmILinMinMod_t::egd_markerIntegrator_jmILinMinMod_t(
	const egd_confObj_t& 	confObj)
 :
  Base_t(confObj)
{};


egd_markerIntegrator_jmILinMinMod_t::egd_markerIntegrator_jmILinMinMod_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const BoundaryCondition_t& 	bcVelX,
	const BoundaryCondition_t& 	bcVelY)
 :
  Base_t(Ny, Nx, bcVelX, bcVelY)
{};


egd_markerIntegrator_jmILinMinMod_t::~egd_markerIntegrator_jmILinMinMod_t()
 = default;



egd_markerIntegrator_jmILinMinMod_t::egd_markerIntegrator_jmILinMinMod_t(
		const egd_markerIntegrator_jmILinMinMod_t&)
 = default;


egd_markerIntegrator_jmILinMinMod_t&
egd_markerIntegrator_jmILinMinMod_t::operator= (
	const egd_markerIntegrator_jmILinMinMod_t&)
 = default;


egd_markerIntegrator_jmILinMinMod_t::egd_markerIntegrator_jmILinMinMod_t(
	egd_markerIntegrator_jmILinMinMod_t&&)
 noexcept
 = default;


egd_markerIntegrator_jmILinMinMod_t&
egd_markerIntegrator_jmILinMinMod_t::operator= (
	egd_markerIntegrator_jmILinMinMod_t&&)
 = default;


PGL_NS_END(egd)


