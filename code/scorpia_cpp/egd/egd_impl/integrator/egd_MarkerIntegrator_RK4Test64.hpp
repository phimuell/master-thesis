#pragma once
/**
 * \brief	This file declares an integrator that uses Taras' new method, also known as test64.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include "./egd_MarkerIntegrator_RK4Base.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \class 	egd_markerIntegratorRK4Test64_t
 * \brief	This class implemnts the new integrator scheme.
 *
 * The Scheme is also known as Test64, no official name yet.
 * It basically works by interpolating derivatives to markers
 * and formaly integrates. It operates on the pressure grid.
 * On the pressure grid the derivative of velocity are knwon
 * exactly, because there continuity is solved. Assumeing to
 * be on a reference square element with width 1, we have
 *
 * 	dVx/dx[x, y] =   (1 - x) * (1 - y) * (dVx/dx)_{i    , j}
 * 		       + (    x) * (1 - y) * (dVx/dx)_{i    , j + 1}
 * 		       + (1 - x) * (    y) * (dVx/dx)_{i + 1, j    }
 * 		       + (    x) * (    y) * (dVx/dx)_{i + 1, j + 1}
 * Where {i, j} is the pressure point that is top left to the
 * marker. Note that x and y are coordinates within the pressure
 * cell. A similar equation can be found for dVy/dy[x, y].
 *
 * The equations are then integrated and the integration
 * constants are determined using the staggered grid nodes.
 *
 * NOTE:
 * I do not fully understand this scheme.
 */
class egd_markerIntegratorRK4Test64_t : public egd_markerIntegratorRK4Base_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t = egd_markerIntegratorRK4Base_t;		//This is the base class of *this
	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using Base_t::NodePositions_t;
	using Base_t::GridProperty_t;

	using Base_t::MarkerPosition_t;
	using Base_t::MarkerVelocity_t;
	using Base_t::MarkerProperty_t;
	using Base_t::ExtVelField_t;

	using Base_t::BoundaryCondition_t;
	using Base_t::ApplyBC_t;

	using Base_t::IgnoredTypeList_t;
	using Base_t::IndexVector_t;


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor will be forwarded to the explicit
	 * constructor of *this.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_markerIntegratorRK4Test64_t(
		const egd_confObj_t& 	confObj);

	/**
	 * \brief	This constructor is an explicit constructor.
	 *
	 * \param  Ny		Number of basic grid points in y direction.
	 * \param  Nx 		Number of basic grid points in x direction.
	 * \param  bcVelX	The boundary condition for the velocity field in x direction.
	 * \param  bcVelY	The boundary condition for the velocity field in y direction.
	 * \param  confObj	Pointer to a possible aviable configuration object.
	 */
	egd_markerIntegratorRK4Test64_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const BoundaryCondition_t& 	bcVelX,
		const BoundaryCondition_t& 	bcVelY,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markerIntegratorRK4Test64_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Test64_t(
		const egd_markerIntegratorRK4Test64_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Test64_t&
	operator= (
		const egd_markerIntegratorRK4Test64_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Test64_t(
		egd_markerIntegratorRK4Test64_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Test64_t&
	operator= (
		egd_markerIntegratorRK4Test64_t&&);


	/*
	 * ==========================
	 * Private Constructor
	 */
private:
	/**
	 * \brief	Default constructor
	 */
	egd_markerIntegratorRK4Test64_t()
	 = delete;



	/*
	 * ===========================
	 * Interface functions
	 */
public:
	using Base_t::moveMarkers;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function is abstract and does not provide an implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;



	/*
	 * Interface functions of the interface
	 */
	using Base_t::isIgnoredType;
	using Base_t::addIgnoredType;
	using Base_t::noIgnoredTypes;


	/*
	 * =====================
	 * Protected Constructors
	 *
	 * All other constructors are protected and defaulted.
	 * They are also marked as noexcept
	 */
protected:


	/*
	 * ======================
	 * RK4 Base Hooks.
	 *
	 * These functions are called by the driver code and allows
	 * to customize the codes.
	 */
protected:
	/**
	 * \brief	This function will set up the internals.
	 *
	 * It will copies the velocity fields from the mechanical
	 * solution into *this no further computations are done.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	hook_setupIntegrator(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function computes the velocity field
	 * 		 At the given positions.
	 *
	 * This function uses the new method, that is scetched above
	 * to compute the velocity, note that for this a loop is
	 * used. so no vecotorization are possible.
	 *
	 * \param  grid		This is the grid object.
	 * \param  xMPos	The x coordinates of the marker.
	 * \param  yMPos 	The y coordinates of the marker.
	 * \param  xMVel 	The x velocity of the marker that was interpolated.
	 * \param  yMVel 	The y velocity of the marker that was interpolated.
	 * \param  cTime	This is the current time, at which we evaluate the velocity field.
	 */
	virtual
	void
	hook_getMarkerVel(
		const GridContainer_t&		grid,
		const MarkerPosition_t& 	xMPos,
		const MarkerPosition_t& 	yMPos,
		      MarkerVelocity_t* const 	xMVel,
		      MarkerVelocity_t* const 	yMVel,
		const Numeric_t 		cTime)
	 const
	 override;


	/**
	 * \brief	This function will return true, if *this is set up.
	 *
	 * This function will check if the base is set up and then check
	 * if the velocity field is setted up.
	 */
	virtual
	bool
	isSetUp()
	 const
	 override;


	/*
	 * =====================
	 * Internal Query Functions
	 *
	 * These functions allows subclasses to access and query the internal
	 * status of the base class instance.
	 */
protected:
	using Base_t::getBCVelX;
	using Base_t::getBCVelY;


	/*
	 * ========================
	 * Private Members
	 *
	 * We need the velocity
	 */
public:
	GridProperty_t 		m_velX;		//!< The x velocity field defined at the staggered grid points.
	GridProperty_t 		m_velY;		//!< The y velocity field defined at the staggered grid points.
}; //End class(egd_markerIntegratorRK4Test64_t)


PGL_NS_END(egd)


