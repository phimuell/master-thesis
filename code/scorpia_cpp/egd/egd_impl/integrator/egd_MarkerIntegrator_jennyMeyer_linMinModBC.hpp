#pragma once
/**
 * \brief	This file implements a min mod limiter interpolation.
 *
 * It extends the base class of tzhe jm interpolation class.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include "./egd_MarkerIntegrator_jennyMeyer_cornerVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyer_cellVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyerBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_set.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \brief	This class computes the corner velocities with min mod limiter.
 * \class 	egd_markerIntegrator_jmILinMinMod_t
 *
 * This function basically overwrites a the abstaact functions of the base class.
 */
class egd_markerIntegrator_jmILinMinMod_t : public egd_markerIntegrator_jmIBase_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t 			= egd_markerIntegrator_jmIBase_t;	//!< This is the base class
	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using Base_t::NodePositions_t;
	using Base_t::GridProperty_t;

	using Base_t::MarkerPosition_t;
	using Base_t::MarkerVelocity_t;
	using Base_t::ExtVelField_t;

	using Base_t::BoundaryCondition_t;
	using Base_t::ApplyBC_t;

	using Base_t::IgnoredTypeList_t;
	using Base_t::IndexVector_t;

	using Base_t::CellVelocities_t;
	using Base_t::CornerVelocities_t;


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor will be forwarded to the base implementation.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_markerIntegrator_jmILinMinMod_t(
		const egd_confObj_t& 	confObj);


	/**
	 * \brief	This constructor is an explicit constructor.
	 *
	 * This constructor will be forwarded to the base implementation.
	 *
	 * \param  Ny		Number of basic grid points in y direction.
	 * \param  Nx 		Number of basic grid points in x direction.
	 * \param  bcVelX	The boundary condition for the velocity field in x direction.
	 * \param  bcVelY	The boundary condition for the velocity field in y direction.
	 */
	egd_markerIntegrator_jmILinMinMod_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const BoundaryCondition_t& 	bcVelX,
		const BoundaryCondition_t& 	bcVelY);


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markerIntegrator_jmILinMinMod_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegrator_jmILinMinMod_t(
		const egd_markerIntegrator_jmILinMinMod_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegrator_jmILinMinMod_t&
	operator= (
		const egd_markerIntegrator_jmILinMinMod_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegrator_jmILinMinMod_t(
		egd_markerIntegrator_jmILinMinMod_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegrator_jmILinMinMod_t&
	operator= (
		egd_markerIntegrator_jmILinMinMod_t&&);


	/*
	 * ==========================
	 * Private Constructor
	 */
private:
	/**
	 * \brief	Default constructor
	 *
	 */
	egd_markerIntegrator_jmILinMinMod_t()
	 = delete;


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	using Base_t::moveMarkers;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 */
	virtual
	std::string
	print()
	 const
	 override;


	/*
	 * =====================
	 * Jenny-Meyer-Hooks
	 *
	 * These hooks are needed for configuring the
	 * base class.
	 */
protected:
	/**
	 * \brief	This function computes the corner velocities
	 * 		 that are needed in the next step.
	 *
	 * The corner velocities will be interpolated by linear interpolation.
	 * As a limiting function minmod will be used. But the boundary condition
	 * is respected.
	 *
	 * \param  mechSol	The mechanical solution.
	 * \param  gridGeo 	The grid geometry.
	 * \param  applyBC	The object for applying the boundary conditions.
	 */
	virtual
	void
	hook_computeCellVelocities(
		const MechSolverResult_t&	mechSol,
		const GridGeometry_t& 		gridGeo,
		const ApplyBC_t& 		applyBC)
	 override;



	/*
	 * =====================
	 * Member Variable Access
	 *
	 * These functions are proviuded such that
	 * sub classes can access internal data
	 * structures of the base. Note that only
	 * access functions for private variables
	 * are provided.
	 */
protected:
	using Base_t::getBCVelX;
	using Base_t::getBCVelY;
	using Base_t::isIgnoredType;



	/*
	 * ======================
	 * Private Helper Functions
	 */
private:


	/*
	 * ========================
	 * Internal Members
	 *
	 * No variables are needed they are all
	 * inside the base class.
	 */
private:
}; //End class(egd_markerIntegrator_jmILinMinMod_t)


PGL_NS_END(egd)


