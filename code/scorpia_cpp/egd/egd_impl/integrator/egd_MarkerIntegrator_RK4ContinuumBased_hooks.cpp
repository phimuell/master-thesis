/**
 * \brief	This file contains the functions for the continuum based interpolator.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_RK4ContinuumBased.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

void
egd_markerIntegratorRK4Std_t::hook_setupIntegrator(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		gridCont,
		const MechSolverResult_t&	mechSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
{
	const GridGeometry_t&   gridGeo = gridCont.getGeometry();	//Load the geometry
	const ApplyBC_t&  	applyBC = gridCont.getApplyBC();

	if(gridGeo.isNewVersion() )
	{
		throw PGL_EXCEPT_illMethod("The integrator can not handle a changed geometry.");
	};
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set.");
	};
	if(mkCase(mechSol.getGridType()) != eGridType::StGrid)
	{
		throw PGL_EXCEPT_InvArg("The solution is not a stagered grid.");
	};

	if(m_extVx.isAllocated() == false ||
	   m_extVy.isAllocated() == false   )
	{
		throw PGL_EXCEPT_LOGIC("The extended velocity field is not allocated.");
	};
	if(m_extVx.hasValidGridType() == false ||
	   m_extVy.hasValidGridType() == false   )
	{
		throw PGL_EXCEPT_LOGIC("THhe extended grids needs to be allocated.");
	}; //End if: check if they are set up
		pgl_assert(m_extVx.getType() == mkExt(eGridType::CellCenter),
			   m_extVy.getType() == mkExt(eGridType::CellCenter) );


	//Get the number of grid points in x and y direction.
	//Remember the number of grid points is one more than cells in that direction.
	const Index_t Nx_gp = gridGeo.xNPoints();
	const Index_t Ny_gp = gridGeo.yNPoints();
		pgl_assert(Nx_gp > 2, Ny_gp > 2);	//Requierement that we have inner nodes

	//Now we allocated the matrix, if needed for the interpolation
	//Resize will only do sonmthing if the nunber of cells are different
	//at least the Eigen documentation claims that.
	m_extVx.resize(yNodeIdx_t(Ny_gp), xNodeIdx_t(Nx_gp));	//Size is directly adjusted
	m_extVy.resize(yNodeIdx_t(Ny_gp), xNodeIdx_t(Nx_gp));

	//Load the two velocities from the solution
	const auto& VX = mechSol.cgetVelX();
	const auto& VY = mechSol.cgetVelY();
		pgl_assert(VY.Ny() == Ny_gp, VY.Nx() == Nx_gp,
			   VX.Ny() == Ny_gp, VX.Nx() == Nx_gp );

	/*
	 * Perform the interpolation.
	 * For that we can use the naive interpolator.
	 * The boundary is not what we want, but we will later
	 * update it.
	 */
	egd_performNaiveTransform(&m_extVx, VX, gridGeo, false);
	egd_performNaiveTransform(&m_extVy, VY, gridGeo, false);


	/*
	 * ==========================================
	 * Now we have to apply the boundary conditions
	 * For that we use the boundary object.
	 */
	applyBC.apply(m_bcVelX, gridGeo, &m_extVx);
	applyBC.apply(m_bcVelY, gridGeo, &m_extVy);

	return;
	(void)mColl;
	(void)tSol;
	(void)thisDt;
	(void)currTime;
	(void)tStepIdx;
}; //End compute the field


void
egd_markerIntegratorRK4Std_t::hook_getMarkerVel(
	const GridContainer_t&		gridCont,
	const MarkerPosition_t& 	xMPos,
	const MarkerPosition_t& 	yMPos,
	      MarkerVelocity_t* const 	xMVel_,
	      MarkerVelocity_t* const 	yMVel_,
	const Numeric_t 		cTime)
 const
{
	pgl_assert( xMVel_ !=  nullptr,
		    yMVel_ !=  nullptr );
	pgl_assert( xMVel_ !=  yMVel_ ,
		   &xMPos  != &yMPos   );

	MarkerVelocity_t& xMVel = *xMVel_;	//Create alias for the pointers
	MarkerVelocity_t& yMVel = *yMVel_;

	const GridGeometry_t& grid = gridCont.getGeometry();	//Get the geometry

	if(grid.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set up.");
	};
	if(grid.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not constant.");
	};
	if(xMPos.size() != yMPos.size())
	{
		throw PGL_EXCEPT_InvArg("The marker positions are not the same."
				" There are " + std::to_string(xMPos.size()) + " x positions,"
				" but " + std::to_string(yMPos.size()) + " many y positions.");
	};
	if(m_extVx.isAllocated()        == false ||
	   m_extVx.isExtendedGrid()     == false ||
	   m_extVx.onCellCentrePoints() == false   )
	{
		throw PGL_EXCEPT_LOGIC("There is a problem with the extendec velocity field for the x component.");
	};
	if(m_extVy.isAllocated()        == false ||
	   m_extVy.isExtendedGrid()     == false ||
	   m_extVy.onCellCentrePoints() == false   )
	{
		throw PGL_EXCEPT_LOGIC("There is a problem with the extendec velocity field for the y component.");
	};

	//Get the number of markers
	const Index_t nMarkers = xMPos.size();
		pgl_assert(nMarkers > 0);

	//Load some properties
	const Index_t xNVelCell = m_extVx.cols();	//Get the size, nubers of cells of the extended velocity grid
	const Index_t yNVelCell = m_extVx.rows();
		pgl_assert(xNVelCell >= 3, xNVelCell == m_extVy.cols(),
			   yNVelCell >= 3, yNVelCell == m_extVy.rows() );

	//This is the size of the basic nodal grid
	const Index_t Nx_bg = grid.xNPoints();
	const Index_t Ny_bg = grid.yNPoints();
		pgl_assert(Nx_bg > 2, Nx_bg == (xNVelCell - 1),
			   Ny_bg > 2, Ny_bg == (yNVelCell - 1) );

	//Load geometrical properties
	const Numeric_t DeltaX = grid.getXSpacing();	//The spacing in x direction.
	const Numeric_t DeltaY = grid.getYSpacing();

	/**
	 * Compute the start point of the grid, this is needed to compute
	 * the index (coordinate) of the cell and the weight.
	 * We define the "origin" at the top left cell center point.
	 * This is node (0,0) in the extended grid.
	 * The location of that node is shifted by half a grid spaceing
	 * backwards of the origin of the basic nodal grid.
	 */
	const Numeric_t xStart = grid.xDomainStart() - 0.5 * DeltaX;
	const Numeric_t yStart = grid.yDomainStart() - 0.5 * DeltaY;

	//
	//These are the properties of the basic grid
	const Numeric_t bgDom_xEnd    = grid.xDomainEnd();	//End of domain
	const Numeric_t bgDom_yEnd    = grid.yDomainEnd();
	const Numeric_t bgDom_xStart  = grid.xDomainStart();	//Start of the domain
	const Numeric_t bgDom_yStart  = grid.yDomainStart();


	//Resize the output arguments, resize will not allocate except it is needed
	xMVel.resize(nMarkers);
	yMVel.resize(nMarkers);

	//Get aliases of the Velocities
	const auto& VX = m_extVx;
	const auto& VY = m_extVy;


	/*
	 * Now we iterate throuh all the markers and find the node where it belongs to.
	 * The node that owns a marker is the next cell center point on the extended
	 * grid that is left and top.
	 */
	for(Index_t m = 0; m != nMarkers; ++m)
	{
		//Load the position of the marker, note it is absolute and not ajusted from the frist node.
		const Numeric_t x_m_abs = xMPos[m];
		const Numeric_t y_m_abs = yMPos[m];
			pgl_assert(bgDom_xStart <= x_m_abs, x_m_abs <= bgDom_xEnd,
				   bgDom_yStart <= y_m_abs, y_m_abs <= bgDom_yEnd );

		//Now we shift the value such that it is relative to the new origin of the
		//extended velocity field
		const Numeric_t x_m = x_m_abs - xStart;
		const Numeric_t y_m = y_m_abs - yStart;

		//Now we calculate the fractional index of the marker.
		const Numeric_t x_m_fi = x_m / DeltaX;
		const Numeric_t y_m_fi = y_m / DeltaY;

		//Now we get the real index, we do this my casting them into an int.
		//The C++ standard requieres that the fractorial part is removed.
		const Index_t x_m_i = Index_t(x_m_fi);
		const Index_t y_m_i = Index_t(y_m_fi);

		/*
		 * It is extremel unrelaistic that we will find a marker that is outside the
		 * extended domain. So we will not do a suffisticated tests as we have done
		 * in the grid code. Note that we are no longer on the basic nodal grid.
		 * So we have one more valid index, this means that N{x,y}_bg is a valid index.
		 */
		pgl_assert(x_m_i >= 0, x_m_i < xNVelCell,
			   y_m_i >= 0, y_m_i < yNVelCell );

		const Index_t i = y_m_i;	//This is now the real index of the node
		const Index_t j = x_m_i;	//Note that here y is first.

		//Compute the position of the associated pressure node
		//We have not included the offset of the origin, because
		//we have moved into a reference frame, that acounts for this.
		const Numeric_t xNodePos = DeltaX * j;
		const Numeric_t yNodePos = DeltaY * i;

		//Now compute the difference to the associated node
		const Numeric_t xDiff_NodeMarker = x_m - xNodePos;	//x_m is the corrected marker position
		const Numeric_t yDiff_NodeMarker = y_m - yNodePos;
			pgl_assert(pgl::isValidFloat(xDiff_NodeMarker), pgl::isValidFloat(yDiff_NodeMarker) );
			//pgl_assert(xDiff_NodeMarker >= 0.0, yDiff_NodeMarker >= 0.0);	THIS IS HANDLED BY THE NORMALIZER


		//This are the component of the weights
		//This are the component of the weights
		const Numeric_t compX 	      = egd_normalizeWeightComp(xDiff_NodeMarker / DeltaX);
		const Numeric_t compY 	      = egd_normalizeWeightComp(yDiff_NodeMarker / DeltaY);
		const Numeric_t compYOneMinus = 1.0 - compY;
		const Numeric_t compXOneMinus = 1.0 - compX;


		/*
		 * X-----------------------------> x / j (second index)
		 * |  omega_ij        omega_ij1
		 * |    o----------------o
		 * |    |       	 |
		 * |    |	^   	 |
		 * |    |		 |
		 * |    |		 |	^ ~ Marker
		 * |    o----------------o
		 * |  omega_i1j       omega_i1j1
		 * |
		 * V  y / i (frist index)
		 *
		 */

		//Caluclate the different weights
		const Numeric_t omega_ij   = compXOneMinus * compYOneMinus;
		const Numeric_t omega_ij1  = compX         * compYOneMinus;
		const Numeric_t omega_i1j  = compXOneMinus * compY        ;
		const Numeric_t omega_i1j1 = compX 	   * compY        ;
			pgl_assert(egd_isValidWeight(omega_ij  ),
				   egd_isValidWeight(omega_i1j ),
				   egd_isValidWeight(omega_ij1 ),
				   egd_isValidWeight(omega_i1j1) );


		// Now load the Eight velocities, four of each direction
			pgl_assert(0 <= i, (i + 1) < VX.rows(),
				   0 <= j, (j + 1) < VX.cols() );
		const Numeric_t Vx_ij   = VX(i    , j    );
		const Numeric_t Vx_ij1  = VX(i    , j + 1);
		const Numeric_t Vx_i1j  = VX(i + 1, j    );
		const Numeric_t Vx_i1j1 = VX(i + 1, j + 1);
			pgl_assert(0 <= i, (i + 1) < VY.rows(),
				   0 <= j, (j + 1) < VY.cols() );

		const Numeric_t Vy_ii   = VY(i    , j    );
		const Numeric_t Vy_ij1  = VY(i    , j + 1);
		const Numeric_t Vy_i1j  = VY(i + 1, j    );
		const Numeric_t Vy_i1j1 = VY(i + 1, j + 1);

		//Compute the marker velocity
		const Numeric_t Vx_m    =   omega_ij   * Vx_ij
  					  + omega_i1j  * Vx_i1j
  					  + omega_ij1  * Vx_ij1
  					  + omega_i1j1 * Vx_i1j1;
  			pgl_assert(pgl::isValidFloat(Vx_m));

  		const Numeric_t Vy_m    =   omega_ij   * Vy_ii
  			 		  + omega_i1j  * Vy_i1j
  			 		  + omega_ij1  * Vy_ij1
  			 		  + omega_i1j1 * Vy_i1j1;
  			pgl_assert(pgl::isValidFloat(Vy_m));

  		//Write the reult back
  		xMVel[m] = Vx_m;
  		yMVel[m] = Vy_m;
	}; //End for(m):

	return;
	(void)cTime;
}; //End: interpolate velocity to marker.



/*
 * =======================
 */
bool
egd_markerIntegratorRK4Std_t::isSetUp()
 const
{
	if(this->Base_t::isSetUp() == false)
	{
		pgl_assert(false && "Uninitialized base.");
		return false;
	};

	if(m_extVx.hasValidGridType() == false ||
           m_extVy.hasValidGridType() == false   )
        {
                pgl_assert(false && "Velocity fields are not allocated.");
                return false;
        }; //End if: test ext vel field

        if(m_extVx.getType() != mkExt(eGridType::CellCenter) ||
           m_extVy.getType() != mkExt(eGridType::CellCenter)   )
        {
                pgl_assert(false && "Velocity fields are not on cell centres.");
                return false;
        }; //End if: ext field is not on right position

        if(m_extVx.getPropIdx() != PropIdx::VelXCC() ||
           m_extVy.getPropIdx() != PropIdx::VelYCC()   )
        {
                pgl_assert(false && "Velcoity fields have wrong property.");
                return false;
        };

        return true;
}; //End: isSetup




PGL_NS_END(egd)

