#pragma once
/**
 * \brief	This is an integrator that uses the conservative interpolation scheme that is described by Jenny and Mayer.
 *
 * See paper "Conservative Velocity Interpolation for PDF Methods", AMM · Proc. Appl. Math. Mech. 4, 466–467 (2004).
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include "./egd_MarkerIntegrator_jennyMeyer_cornerVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyer_cellVelocities.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_set.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \brief	This class enables a new integration scheme.
 * \class 	egd_markerIntegrator_jmIBase_t
 *
 * The implementation is based on "Conservative Velocity Interpolation for PDF Methods"
 * by P. Jenny and D. Meyer and to a smaller extend "A Hybrid Algorithm for the Joint
 * PDF Equation of Turbulent Reactive Flows" by P. Jenny, especially apendix A.
 * We also use "Advantages of a conservative velocity interpolation (CVI) scheme
 * for particle-in-cell methods with application in geodynamic modeling" by Wang et. al.
 * Here especially the supporting material.
 * Note that the paper of P. Jenny has a typo in (58) and D. Meyer's paper has a strange
 * linear term in the correction, which completly cancels.
 *
 * The idea is to compute a velocity in the four corners of each basic cell, that is
 * formed by the basic nodal grids, and then use these to perform interpolation.
 * The folowing fact is important,on each basic nodal points, at least the inner ones,
 * four cells interacts and four velocities are there, but they do not need to be the
 * same. It is possible that there are shocks and thus the velocities are different.
 *
 * This class moves the marker using RK4. This is done inside a cell. If the marker
 * would leave the cell the timestep is reduced to trigger a save crossing.
 * If this fails, forward Euler is used to perform the crossing.
 * See the moveOneMarker() function for an explanation.
 *
 * For interpolating the velocity a conservative scheme is used, that is outlined in
 * the first paper.
 *
 * This class provides acts as a base. It does not compute the corner velocities, this
 * is left to a concrete base.
 *
 * This class does not allow vectorization but allows to ignore, not move, sepecific
 * marker types.
 *
 * This class supports feel velocities. Ifsubstepping is in place, then a time weighted
 * average is computed and returned.
 */
class egd_markerIntegrator_jmIBase_t : public egd_MarkerIntegrator_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t = egd_MarkerIntegrator_i;		//This is the base class of *this
	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using GridGeometry_t 		= egd_gridGeometry_t;		  //!< This is teh grid geometry.
	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using NodePositions_t 		= GridGeometry_t::NodePosition_t; //!< This is the positions of the node.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;

	using MarkerPosition_t		= MarkerCollection_t::MarkerProperty_t;	//!< This is the type for the position of the markers
	using MarkerVelocity_t 		= MarkerCollection_t::MarkerProperty_t;	//!< This is the velocity of the markers.
	using ExtVelField_t 		= egd_gridProperty_t;			//!< This is the type for storing the extended velocity field.

	using BoundaryCondition_t 	= egd_boundaryConditions_t;		//!< This is a class for encoding a boundary conditin.
	using ApplyBC_t 		= GridContainer_t::ApplyBC_t;		//!< This is the type that is able to apply the boundary conditin.

	using Base_t::IgnoredTypeList_t;
	using IndexVector_t 		= MarkerCollection_t::IndexVector_t;	//!< List of indexes

	using CellVelocities_t		= egd_jmICellVel_t;			//!< This is the container for storing the cell velocities.
	using CornerVelocities_t 	= CellVelocities_t::CornerVelocity_t;	//!< This stores the velocities in one particular cell.



	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor extracts the the necessary information
	 * from the configuration object and forwards it to the
	 * building constructor.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_markerIntegrator_jmIBase_t(
		const egd_confObj_t& 	confObj);


	/**
	 * \brief	This constructor is an explicit constructor.
	 *
	 * This constructor will set up all internal structure.
	 * It will allocate the cell velocity container, with the
	 * basic grid cell.
	 * If this is not desiered, reallocate them in the compute
	 * function.
	 *
	 * \param  Ny		Number of basic grid points in y direction.
	 * \param  Nx 		Number of basic grid points in x direction.
	 * \param  bcVelX	The boundary condition for the velocity field in x direction.
	 * \param  bcVelY	The boundary condition for the velocity field in y direction.
	 */
	egd_markerIntegrator_jmIBase_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const BoundaryCondition_t& 	bcVelX,
		const BoundaryCondition_t& 	bcVelY);


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markerIntegrator_jmIBase_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegrator_jmIBase_t(
		const egd_markerIntegrator_jmIBase_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegrator_jmIBase_t&
	operator= (
		const egd_markerIntegrator_jmIBase_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegrator_jmIBase_t(
		egd_markerIntegrator_jmIBase_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegrator_jmIBase_t&
	operator= (
		egd_markerIntegrator_jmIBase_t&&);


	/*
	 * ==========================
	 * Private Constructor
	 */
private:
	/**
	 * \brief	Default constructor
	 *
	 */
	egd_markerIntegrator_jmIBase_t()
	 = delete;


	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function performs the interpolation.
	 *
	 * This function will advect the marker.
	 *
	 * It will not use the feel velocity to move the marker, but it will
	 * use the new positions that are stored inside *this.
	 *
	 * This function will use the time stepping information to determine,
	 * if the stored data is still accurate, if a missmatch was found
	 * the data will be recomputed.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	moveMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This fununction can be used to set feel velocities and
	 * 		 store them. This will NOT move the markers.
	 *
	 * This function will compute the feel velocities. The velocties are stored
	 * inside *this and in the marker collection. The time index, current time
	 * and time increment are stored and are used as key to idenetify the
	 * velocties, such that they could be reused upon moving.
	 * If a marker is ignored, its velocity will be set to zero.
	 *
	 * This function also stores a position vector of the new position.
	 * This is doe the subtime steppiong.
	 *
	 * The function will always return ture, indicating that the velocities
	 * are always recomputed upon calling this function.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	bool
	storeFeelVel(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function is not implemented because *this is
	 * a base class.
	 *
	 */
	virtual
	std::string
	print()
	 const
	 override
	 = 0;

	/**
	 * \brief	This function returns true, meaning that it supports the
	 * 		 feel velocity.
	 *
	 * If substepping was in use, a time weighted average of the velocity
	 * is computed and used.
	 */
	virtual
	bool
	computeFeelVel()
	 const
	 override;


	using Base_t::isIgnoredType;
	using Base_t::addIgnoredType;
	using Base_t::noIgnoredTypes;


	/*
	 * =====================
	 * Jenny-Meyer-Hooks
	 *
	 * These hooks are needed for configuring the
	 * base class.
	 */
protected:
	/**
	 * \brief	This function computes the corner velocities
	 * 		 that are needed in the next step.
	 *
	 * This function throws if it is not able to establish its post
	 * conditions. This function has no default.
	 *
	 * \param  mechSol	The mechanical solution.
	 * \param  gridGeo 	The grid geometry.
	 * \param  applyBC	The object for applying the boundary conditions.
	 */
	virtual
	void
	hook_computeCellVelocities(
		const MechSolverResult_t&	mechSol,
		const GridGeometry_t& 		gridGeo,
		const ApplyBC_t& 		applyBC)
	 = 0;



	/*
	 * =====================
	 * Member Variable Access
	 *
	 * These functions are proviuded such that
	 * sub classes can access internal data
	 * structures of the base. Note that only
	 * access functions for private variables
	 * are provided.
	 */
protected:
	/**
	 * \brief	This function retrns a reference to the boundary
	 * 		 condition for the x component of velocity.
	 */
	const BoundaryCondition_t&
	getBCVelX()
	 const;


	/**
	 * \brief	This function retrns a reference to the boundary
	 * 		 condition for the y component of velocity.
	 */
	const BoundaryCondition_t&
	getBCVelY()
	 const;



	/*
	 * ======================
	 * Private Helper Functions
	 */
private:
	/**
	 * \brief	This function moves a single marker to its new position.
	 *
	 * This function will move the marker from its location to the new position
	 * and will return the values in the pointer arguments.
	 *
	 * This function will respect the cells and apply subtimestepping. If the
	 * movement of the markers is completly contained inside the cell, this
	 * function will move the marker with the classical RK4 scheme.
	 * If the marker corsses a cell, subtimestepping is applied to resove the
	 * cell crossing. It will try to provoke a cell crossing with RK4 and
	 * a smaller timestepsize. If this is not successfull, it will employ
	 * forward Euler, to move the cell over the cell boundary, for numerical
	 * reason the marker will penetrate the other cell a bit.
	 *
	 * \param  yOld_m 	Current y position of the marker.
	 * \param  xOld_m	Current x position of the marker.
	 * \param  yNew_m	Pointer to the new y location of the marker.
	 * \param  xNew_m 	Pointer to the new x location of the marker.
	 * \param  feelVelY_m	Feel velocity in y direction.
	 * \param  feelVelX_m	Feel velocity in x direction.
	 * \param  yGridPos	y coordinates of the basic nodal grid points.
	 * \param  xGridPos	x coordinates of the basic nodal grid points.
	 * \param  dt 		Time step size.
	 *
	 * \note	This function considers a move, with RK4, to be successfull
	 * 		 if the RK4 routine can compute the final value, as described
	 * 		 there this does not imply true as return value.
	 */
	void
	moveOneMarker(
		const Numeric_t 		yOld_m,
		const Numeric_t 		xOld_m,
		      Numeric_t* const 		yNew_m,
		      Numeric_t* const 		xNew_m,
		      Numeric_t* const 		feelVelY_m,
		      Numeric_t* const 		feelVelX_m,
		const NodePositions_t& 		yGridPos,
		const NodePositions_t& 		xGridPos,
		const Numeric_t 		dt)
	 const;


	/**
	 * \brief	This function will compute the associated cell.
	 *
	 * This function uses the assumtion that the grid spacing is
	 * constant, it also assumes that the grid which is used is the
	 * basic nodal gread, meaing it will respect the fact, that mo
	 * marker can be associated to the last node.
	 *
	 * \param  posM		Marker position.
	 * \param  domStart	Start of the domain.
	 * \param  gSpacing	The grid spacing.
	 * \param  nBasicPos	The number of basic nodal points.
	 */
	static
	Index_t
	compCellId(
		const Numeric_t 	posM,
		const Numeric_t 	domStart,
		const Numeric_t 	gSpacing,
		const Index_t 		nBasicPos);


	/**
	 * \brief 	This function is able to compute the velocity at a given points.
	 *
	 * Note that this function expects its argument to be in {x, y} order, the same
	 * holds for its return value. This is different than for other routines in the
	 * code. This also holds especially for the index association.
	 *
	 * \param  xM		x coordinate of the marker.
	 * \param  yM		y coordinate of the marker.
	 * \param  j 		Association in j, towards x.
	 * \param  i 		Association in i, towards y.
	 * \param  xBasicPos	x coordinates of the basic nodal points.
	 * \param  yBasicPos	y coordinates of the basic nodal points.
	 * \param  xV		Computed marker velocity in x direction.
	 * \param  yV		Computed marker velocity in y direction.
	 */
	void
	computeMarkerVel(
		const Numeric_t 		xM,
		const Numeric_t 		yM,
		const Size_t 			j,	/* Different ordering than before */
		const Size_t 			i,
		const NodePositions_t& 		xBasicPos,
		const NodePositions_t& 		yBasicPos,
		      Numeric_t* const 		xV,
		      Numeric_t* const 		yV)
	 const;


	/**
	 * \brief	This function implements RK4 movement.
	 *
	 * It starts at the given position, note that the order is different
	 * than usual such that x comes before y. Note that depending on how
	 * the marker moves the return value will differ.
	 * The function will calculate the velocity at four different locations,
	 * {a, b, c, d}, but location a is the same as it initial position,
	 * which is assumed to be inside the designated cell.
	 * If any of the position lies outside the start cell it will return
	 * false and write NAN as result.
	 * If all intermediate seps lies inside the cell it will return true
	 * and the new position is returned by the pointer arguments.
	 * However, if all intermediate position lies inside the same cell,
	 * but the final marker position is not inside the cell the function
	 * will return false, and the computed results will be returned.
	 * Because the position is still "good".
	 *
	 * If the movement was preemptive exited, meaning that the final velocity
	 * was not computed because a cell crossing happened, then moveVel{X,Y}
	 * will be NAN.
	 *
	 * \param  dt 		The timestep that should be used.
	 * \param  xM		x coordinate of the marker.
	 * \param  yM		y coordinate of the marker.
	 * \param  j 		Association in j, towards x.
	 * \param  i 		Association in i, towards y.
	 * \param  moveVelX	Velocity in x that was computed.
	 * \param  moveLevY 	Velocity in y that was computed.
	 * \param  xBasicPos	x coordinates of the basic nodal points.
	 * \param  yBasicPos	y coordinates of the basic nodal points.
	 * \param  newPosX	The x coordinate of the marker after its movement.
	 * \param  newPosY	The y coordinate of the marker after its movement.
	 */
	bool
	moveMarkerRK4(
		const Numeric_t 		dt,
		const Numeric_t 		xM,
		const Numeric_t 		yM,
		const Size_t 			j,	/* Different ordering than before */
		const Size_t 			i,
		      Numeric_t* const 		moveVelX,
		      Numeric_t* const 		moveVelY,
		const NodePositions_t& 		xBasicPos,
		const NodePositions_t& 		yBasicPos,
		      Numeric_t* const 		newPosX,
		      Numeric_t* const 		newPosY)
	 const;


	/**
	 * \brief	This function will return true, if *this is set up.
	 *
	 * *this is set up, if the boundary values have a value other than NAN
	 * and if the velocity fields are allocated. It does not mean that the
	 * right values has been set up. It is more a test for "is allocated"
	 * and can it be used.
	 *
	 * This may change in the furtur.
	 */
	bool
	isSetUp()
	 const;



	/*
	 * ==================
	 * Protected base functions
	 */
protected:
	using Base_t::getIgnoredTypeList;


	/*
	 * ========================
	 * Internal Members
	 *  They are private and protected
	 */
private:
	BoundaryCondition_t 	m_bcVelX;		//!< Boundary condition for the velocity x field.
	BoundaryCondition_t 	m_bcVelY;		//!< Boundary condition for the velocity y field.

	//This is the precomputed feel velocity
	MarkerVelocity_t 	m_feelVelX;		//!< Feel Velocity in x direction.
	MarkerVelocity_t 	m_feelVelY;		//!< Feel velocity in y direction.
	MarkerPosition_t 	m_feelPosX;		//!< New position, is needed for this kind of integrator.
	MarkerPosition_t 	m_feelPosY;		//!< New position, is needed for this this of integrator.
	Numeric_t 		m_feelTime = NAN;	//!< Time at which the feel velocities where computed.
	Numeric_t   		m_feelDT   = NAN;	//!< Time increment that was used.
	Size_t 			m_feelIdx  = Size_t(-1);//!< Timestep index at which the feel velocity was computed.

protected:
	CellVelocities_t 	m_cellVelX;		//!< Cell velocities for the x component.
	CellVelocities_t 	m_cellVelY;		//!< Cell velocities for the y component.
}; //End class(egd_markerIntegrator_jmIBase_t)


PGL_NS_END(egd)


