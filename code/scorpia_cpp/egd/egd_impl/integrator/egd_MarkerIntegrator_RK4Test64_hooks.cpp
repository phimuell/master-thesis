/**
 * \brief	This file contains the hook functions of the Test64 integrator.
 *
 * The implementation is mostly based on the matlab file.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_RK4Test64.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

template<
	typename 	T
>
constexpr
T
clamp(
	const T& 	smallest,
	const T& 	x,
	const T& 	largest)
{
	return (x <= smallest)
		? (smallest)		//if below smalles, return smallest
		: ((largest <= x)	//if x is larger than largest, return largest
		   ? largest
		   : x           );	//if neither, then return x
};


template<
	typename 	T
>
constexpr
T
sq(
	const T& 	x)
{
	return (x * x);
};



/*
 * =======================
 * Real RK4 Hooks.
 *
 * These are the hooks that are needed by the RK4 base class.
 */
bool
egd_markerIntegratorRK4Test64_t::isSetUp()
 const
{
	if(this->Base_t::isSetUp() == false)
	{
		pgl_assert(false && "Uninitialized base.");
		return false;
	};

	if((this->m_velX.onStVxPoints()        == false) ||
	   (this->m_velX.getPropIdx().isVelX() == false) ||
	   (this->m_velX.isExtendedGrid()      == false)   )
	{
		pgl_assert(false && "velX is not valid.");
		return false;
	};

	if((this->m_velY.onStVyPoints()        == false) ||
	   (this->m_velY.getPropIdx().isVelY() == false) ||
	   (this->m_velY.isExtendedGrid()      == false)   )
	{
		pgl_assert(false && "velY is not valid.");
		return false;
	};

        return true;
}; //End: isSetup


void
egd_markerIntegratorRK4Test64_t::hook_setupIntegrator(
	const MarkerCollection_t& 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	if(grid.getGeometry().isConstantGrid() == false)
	{
		throw PGL_EXCEPT_LOGIC("The passed grid is not constant.");
	};

	/*
	 * We will copy the velocities from the
	 * mechanical solution and then cewch ourself
	 */
	this->m_velX = mSol.cgetVelX();		pgl_assert(this->m_velX.getType() == mSol.cgetVelX().getType(), mSol.cgetVelX().checkOnSameGrid(this->m_velX));
	this->m_velY = mSol.cgetVelY();		pgl_assert(this->m_velY.getType() == mSol.cgetVelY().getType(), mSol.cgetVelY().checkOnSameGrid(this->m_velY));

	return;
		PGL_UNUSED(mColl,
	   		   grid,
	   		   tSol,
			   thisDt,
			   tStepIdx,
			   currTime);
}; //End: setUp Integrator


void
egd_markerIntegratorRK4Test64_t::hook_getMarkerVel(
	const GridContainer_t&		grid,
	const MarkerPosition_t& 	xMPos,
	const MarkerPosition_t& 	yMPos,
	      MarkerVelocity_t* const 	xMVel_,
	      MarkerVelocity_t* const 	yMVel_,
	const Numeric_t 		cTime)
 const
{
using ::pgl::isValidFloat;
using ::std::trunc;	// is the same as matlab's FIX command


	pgl_assert(xMVel_ != nullptr, yMVel_ != nullptr);	//alias the valocity
	MarkerVelocity_t& xMVel = *xMVel_;
	MarkerVelocity_t& yMVel = *yMVel_;

	const GridGeometry_t&  gridGeo = grid.getGeometry();		//get the geometry
	const Index_t 	       nMarker = yMPos.size();			//get numbers of markers
		pgl_assert(nMarker > 0, nMarker == xMPos.size());

	//Resize the velocity container, according to the eigen docs
	// this will only cause an operation if the size is not ciorrect
	xMVel.resize(nMarker);
	yMVel.resize(nMarker);
	xMVel = NAN;	//set to dummy value
	yMVel = NAN;
		pgl_assert(xMVel.size() == nMarker);
		pgl_assert(yMVel.size() == nMarker);

	//get the node positions that are needed
	NodePositions_t PosVX_x, PosVX_y;	//Positions of the VX nodes
	NodePositions_t PosVY_x, PosVY_y;
	std::tie(PosVX_x, PosVX_y) = gridGeo.getGridPoints(this->m_velX );
	std::tie(PosVY_x, PosVY_y) = gridGeo.getGridPoints(this->m_velY );

	//where the grid starts
	const Numeric_t startVX_x = PosVX_x[0];
	const Numeric_t startVX_y = PosVX_y[0];
	const Numeric_t startVY_x = PosVY_x[0];
	const Numeric_t startVY_y = PosVY_y[0];

	//what is the last associated index
	const Index_t Ny       = gridGeo.yNPoints();
	const Index_t Nx       = gridGeo.xNPoints();
	const Index_t Zero     = 0;

	//the grid spacing, we assume constant grid spacing on all different grids
	const Numeric_t dx = PosVX_x[1] - PosVX_x[0];
	const Numeric_t dy = PosVX_y[1] - PosVX_y[0];


	/*
	 * Going through the markers and clcualting the velocity.
	 * The code is directly prorted from the matlab version.
	 * The difference is that the matlab code moves a marker,
	 * and this only computes the velocity.
	 */
	for(Index_t m = 0; m != nMarker; ++m)
	{
		//get some general values
		const Numeric_t xm = xMPos[m];		//The marker positions
		const Numeric_t ym = yMPos[m];

		//this are the result velocityies that then are used.
		Numeric_t xVelFin = NAN;
		Numeric_t yVelFin = NAN;


		/*
		 * Interpolating VX
		 */
		{
			//Computing the associated index
			const Index_t j_ = trunc((xm - startVX_x) / dx);	//no +1 needed
			const Index_t i_ = trunc((ym - startVX_y) / dy);

			//Adjusting the index
			const Index_t j = clamp(Zero, j_, Nx - 2);
			const Index_t i = clamp(Zero, i_, Ny - 1);

			//compuet distances
			const Numeric_t dxmjRaw = xm - PosVX_x[j];
			const Numeric_t dymiRaw = ym - PosVX_y[i];

			//making weights out of them, note that the matlab script does not do that, and always performs divisions
			//we will not do this.
			const Numeric_t dxmj    = egd_normalizeWeightComp(dxmjRaw / dx);
			const Numeric_t dymi    = egd_normalizeWeightComp(dymiRaw / dy);

			//compute the upper and lower velocity value
			Numeric_t vxm13 = m_velX(i    , j) * (1.0 - dxmj) + m_velX(i    , j + 1) * dxmj;
			Numeric_t vxm24 = m_velX(i + 1, j) * (1.0 - dxmj) + m_velX(i + 1, j + 1) * dxmj;

			//Correct the computations
			if(dxmj >= 0.5)
			{
				if(j < (Nx - 2))
				{
					vxm13 += 0.5 * sq(dxmj - 0.5) * (m_velX(i    , j) - 2.0 * m_velX(i    , j + 1) + m_velX(i    , j + 2) );
					vxm24 += 0.5 * sq(dxmj - 0.5) * (m_velX(i + 1, j) - 2.0 * m_velX(i + 1, j + 1) + m_velX(i + 1, j + 2) );
				}
				else
				{
					//DO NOTHING
				};
			}
			else
			{
				if(j > 0)
				{
					vxm13 += 0.5 * sq(dxmj - 0.5) * (m_velX(i    , j - 1) - 2.0 * m_velX(i    , j) + m_velX(i    , j + 1) );
					vxm24 += 0.5 * sq(dxmj - 0.5) * (m_velX(i + 1, j - 1) - 2.0 * m_velX(i + 1, j) + m_velX(i + 1, j + 1) );
				}
				else
				{
					//DO NOTHING
				};
			}; //End: applying correction

			//Compuet the vinal velocity
			xVelFin = (1.0 - dymi) * vxm13 + dymi * vxm24;
				pgl_assert(isValidFloat(xVelFin));
		}; //End scope(VX):


		/*
		 * Computing the velocity Y
		 */
		{
			//Computing the associated node
			const Index_t j_ = trunc((xm - startVY_x) / dx);	//no +1 needed
			const Index_t i_ = trunc((ym - startVY_y) / dy);

			//Constraint the index
			const Index_t j = clamp(Zero, j_, Nx - 1);
			const Index_t i = clamp(Zero, i_, Ny - 2);

			//this is teh raw distance, this is computed by the matlab code, but
			//it is not realy used
			const Numeric_t dxmjRaw = xm - PosVY_x[j];
			const Numeric_t dymiRaw = ym - PosVY_y[i];

			//We define the "distance" already by a normalized distance.
			//the reason is only this one is used, and we can avoid some divisions.
			//Note the matlab does not do that.
			const Numeric_t dxmj = egd_normalizeWeightComp(dxmjRaw / dx);
			const Numeric_t dymi = egd_normalizeWeightComp(dymiRaw / dy);

			//Compute velocities
			Numeric_t vym12 = m_velY(i, j    ) * (1.0 - dymi) + m_velY(i + 1, j    ) * dymi;
			Numeric_t vym34 = m_velY(i, j + 1) * (1.0 - dymi) + m_velY(i + 1, j + 1) * dymi;

			//Compute corrections
			if(dymi >= 0.5)
			{
				if(i < (Ny - 2))
				{
					vym12 += 0.5 * sq(dymi - 0.5) * (m_velY(i, j    ) - 2.0 * m_velY(i + 1, j    ) + m_velY(i + 2, j    ) );
					vym34 += 0.5 * sq(dymi - 0.5) * (m_velY(i, j + 1) - 2.0 * m_velY(i + 1, j + 1) + m_velY(i + 2, j + 1) );
				}
				else
				{
					//DO NOTHING
				}
			}
			else
			{
				if(i > 0)
				{
					vym12 += 0.5 * sq(dymi - 0.5) * (m_velY(i - 1, j    ) - 2.0 * m_velY(i, j    ) + m_velY(i + 1, j    ) );
					vym34 += 0.5 * sq(dymi - 0.5) * (m_velY(i - 1, j + 1) - 2.0 * m_velY(i, j + 1) + m_velY(i + 1, j + 1) );
				}
				else
				{
					//DO NOTHING
				};
			}; //End: correcting

			//computing the final velocity
			yVelFin = (1.0 - dxmj) * vym12 + dxmj * vym34;
				pgl_assert(isValidFloat(yVelFin));
		}; //end scope: VY

		/*
		 * FInal handling
		 */
			pgl_assert(isValidFloat(xVelFin), isValidFloat(yVelFin));
		yMVel[m] = yVelFin;
		xMVel[m] = xVelFin;
	}; //End for(m):

		pgl_assert(xMVel.isFinite().all(),
			   yMVel.isFinite().all() );
	return;
		PGL_UNUSED(cTime);
}; //End: compute the marker velocity


PGL_NS_END(egd)

