/**
 * \brief	This file contains the constructor from the jenny meyer marker interpolation base.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_jennyMeyerBase.hpp"

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

egd_markerIntegrator_jmIBase_t::egd_markerIntegrator_jmIBase_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const BoundaryCondition_t& 	bcVelX,
	const BoundaryCondition_t& 	bcVelY)
 :
  m_bcVelX(PropIdx::VelX() ),		//Construct bc objects only with component
  m_bcVelY(PropIdx::VelY() ),		// assignment happens later
  m_cellVelX(yNodeIdx_t(Ny - 1), xNodeIdx_t(Nx - 1), PropIdx::VelX()),
  m_cellVelY(yNodeIdx_t(Ny - 1), xNodeIdx_t(Nx - 1), PropIdx::VelY())
{
	//Test if the arguments are valid.
	if(bcVelX.isValid()    == false           ||
	   bcVelX.isFinal()    == false           ||
	   bcVelX.getPropIdx() != PropIdx::VelX()   )
	{
		throw PGL_EXCEPT_InvArg("The boundary condition for the velocity field in x direction was invalid.");
	};
	if(bcVelY.isValid()    == false           ||
	   bcVelY.isFinal()    == false           ||
	   bcVelY.getPropIdx() != PropIdx::VelY()   )
	{
		throw PGL_EXCEPT_InvArg("The boundary condition for the velocity field in y direction was invalid.");
	};
	if(m_cellVelX.isConsistent() == false)
	{
		throw PGL_EXCEPT_InvArg("The cell x velcoity container was not constructed correctly.");
	};
	if(m_cellVelY.isConsistent() == false)
	{
		throw PGL_EXCEPT_InvArg("The cell y velcoity container was not constructed correctly.");
	};

	//Overwrite the current ones
	m_bcVelX = bcVelX;
	m_bcVelY = bcVelY;
};//End: direct building constructor


egd_markerIntegrator_jmIBase_t::egd_markerIntegrator_jmIBase_t(
	const egd_confObj_t& 	confObj)
 :
  egd_markerIntegrator_jmIBase_t(
  		yNodeIdx_t(confObj.getNy()),
  		xNodeIdx_t(confObj.getNx()),
  		confObj.getBC(eRandProp::VelX),
  		confObj.getBC(eRandProp::VelY) )
{
}; //End: building constructor



egd_markerIntegrator_jmIBase_t::~egd_markerIntegrator_jmIBase_t()
 = default;


egd_markerIntegrator_jmIBase_t::egd_markerIntegrator_jmIBase_t(
	const egd_markerIntegrator_jmIBase_t&)
 = default;


egd_markerIntegrator_jmIBase_t&
egd_markerIntegrator_jmIBase_t::operator= (
	const egd_markerIntegrator_jmIBase_t&)
 = default;


egd_markerIntegrator_jmIBase_t::egd_markerIntegrator_jmIBase_t(
	egd_markerIntegrator_jmIBase_t&&)
 noexcept
 = default;


egd_markerIntegrator_jmIBase_t&
egd_markerIntegrator_jmIBase_t::operator= (
	egd_markerIntegrator_jmIBase_t&&)
 = default;


PGL_NS_END(egd)



