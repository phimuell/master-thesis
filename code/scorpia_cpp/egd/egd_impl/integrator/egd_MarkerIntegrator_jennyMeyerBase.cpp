/**
 * \brief	This file contains some functions from the jenny meyer bvase class that does not belong in a different file.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_jennyMeyerBase.hpp"

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

bool
egd_markerIntegrator_jmIBase_t::isSetUp()
 const
{
	//Test if the arguments are valid.
	if(m_bcVelX.isValid()    == false           ||
	   m_bcVelX.isFinal()    == false           ||
	   m_bcVelX.getPropIdx() != PropIdx::VelX()   )
	{
		pgl_assert(false && "The x boundary object is invalid.");
		return false;
	};
	if(m_bcVelY.isValid()    == false           ||
	   m_bcVelY.isFinal()    == false           ||
	   m_bcVelY.getPropIdx() != PropIdx::VelY()   )
	{
		pgl_assert(false && "The y boundary object is invalid.");
		return false;
	};


	//No error detected
	return true;
}; //End: isSetUp


bool
egd_markerIntegrator_jmIBase_t::computeFeelVel()
 const
{
	return true;
}; //End: compute feel vel


const egd_markerIntegrator_jmIBase_t::BoundaryCondition_t&
egd_markerIntegrator_jmIBase_t::getBCVelX()
 const
{
	pgl_assert(m_bcVelX.isValid(),
		   m_bcVelX.getPropIdx().isVelX());
	return m_bcVelX;
}; //End: get bc for velocity in x direction


const egd_markerIntegrator_jmIBase_t::BoundaryCondition_t&
egd_markerIntegrator_jmIBase_t::getBCVelY()
 const
{
	pgl_assert(m_bcVelY.isValid(),
		   m_bcVelY.getPropIdx().isVelY());
	return m_bcVelY;
}; //End: get bc for velocity in x direction



PGL_NS_END(egd)



