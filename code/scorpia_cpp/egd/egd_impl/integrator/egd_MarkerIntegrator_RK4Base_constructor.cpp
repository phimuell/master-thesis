/**
 * \brief	This file implements the constructor code for the RK4 interpolator.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_RK4Base.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

#define TO_STR(s) #s

egd_markerIntegratorRK4Base_t::egd_markerIntegratorRK4Base_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const BoundaryCondition_t& 	bcVelX,
	const BoundaryCondition_t& 	bcVelY,
	const egd_confObj_t* const 	confObj)
 :
  egd_markerIntegratorRK4Base_t(Ny, Nx)	//This will allocate the grids
{
	//Test if the arguments are valid.
	if(bcVelX.isValid()    == false           ||
	   bcVelX.isFinal()    == false           ||
	   bcVelX.getPropIdx() != PropIdx::VelX()   )
	{
		throw PGL_EXCEPT_InvArg("The boundary condition for the velocity field in x direction was invalid.");
	};
	if(bcVelY.isValid()    == false           ||
	   bcVelY.isFinal()    == false           ||
	   bcVelY.getPropIdx() != PropIdx::VelY()   )
	{
		throw PGL_EXCEPT_InvArg("The boundary condition for the velocity field in y direction was invalid.");
	};

	//Overwrite the current ones
	m_bcVelX = bcVelX;
	m_bcVelY = bcVelY;
	(void)confObj;	//not needed.
};//End: direct building constructor


egd_markerIntegratorRK4Base_t::egd_markerIntegratorRK4Base_t(
	const egd_confObj_t& 	confObj)
 :
  egd_markerIntegratorRK4Base_t(
  		yNodeIdx_t(confObj.getNy()),
  		xNodeIdx_t(confObj.getNx()),
  		confObj.getBC(eRandProp::VelX),
  		confObj.getBC(eRandProp::VelY),
  		&confObj)
{
	using pgl::isValidFloat;

	/*
	 * We have to test if the other boundaries are correct
	 * This is technically not needed since the we use the
	 * boundary applier.
	 */
#define TEST(prop, loc) if(confObj.getBoundaryType(prop, loc) != eRandKind::freeSlip) {throw PGL_EXCEPT_InvArg("The boundary of " TO_STR(prop) " at " TO_STR(loc) " was not free slip.");}
	TEST(eRandProp::VelX, eRandLoc::TOP   );
	TEST(eRandProp::VelX, eRandLoc::BOTTOM);
	TEST(eRandProp::VelY, eRandLoc::RIGHT );
	TEST(eRandProp::VelY, eRandLoc::LEFT  );
#undef TEST
}; //End: building constructor


egd_markerIntegratorRK4Base_t::egd_markerIntegratorRK4Base_t(
	const yNodeIdx_t 	Ny,
	const xNodeIdx_t 	Nx)
 :
  m_bcVelX(PropIdx::VelX() ),
  m_bcVelY(PropIdx::VelY() )
{
	/*
	 * Handle the grid
	 */
	if(Ny < Index_t(3) || Nx < Index_t(3))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points is wrong.");
	}; //End if: check the size
};


egd_markerIntegratorRK4Base_t::~egd_markerIntegratorRK4Base_t()
 = default;


egd_markerIntegratorRK4Base_t::egd_markerIntegratorRK4Base_t()
 :
  m_bcVelX(PropIdx::VelX() ),
  m_bcVelY(PropIdx::VelY() )
{};


egd_markerIntegratorRK4Base_t::egd_markerIntegratorRK4Base_t(
	const egd_markerIntegratorRK4Base_t&)
 = default;


egd_markerIntegratorRK4Base_t&
egd_markerIntegratorRK4Base_t::operator= (
	const egd_markerIntegratorRK4Base_t&)
 = default;


egd_markerIntegratorRK4Base_t::egd_markerIntegratorRK4Base_t(
	egd_markerIntegratorRK4Base_t&&)
 noexcept
 = default;


egd_markerIntegratorRK4Base_t&
egd_markerIntegratorRK4Base_t::operator= (
	egd_markerIntegratorRK4Base_t&&)
 = default;

PGL_NS_END(egd)

