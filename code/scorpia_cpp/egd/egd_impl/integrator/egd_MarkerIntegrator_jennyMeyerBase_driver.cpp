/**
 * \brief	This file contains the function that drives the movement.
 *
 * This function only contains the driver, no other function. The goal of this file
 * is to provide a very clean description of how the markers are moved.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include "./egd_MarkerIntegrator_jennyMeyer_cornerVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyer_cellVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyerBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

bool
egd_markerIntegrator_jmIBase_t::storeFeelVel(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		gridCont,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
using ::pgl::isValidFloat;

	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;		//Alias of the marker collection

	const Index_t nMarkers = mColl.nMarkers();

	const GridGeometry_t& grid = gridCont.getGeometry();	//Alias the grid geometry

	if(grid.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set up.");
	};
	if(grid.isConstantGrid() == false)	//Needed for the one marker move
	{
		throw PGL_EXCEPT_InvArg("The grid is not constant.");
	};
	if(mSol.isAllocated() == false)
	{
		throw PGL_EXCEPT_InvArg("The mechanical solution is not allocated.");
	};
	if(grid.xNPoints() != mSol.Nx())
	{
		throw PGL_EXCEPT_InvArg("The number of x points is not consitent.");
	};
	if(grid.yNPoints() != mSol.Ny())
	{
		throw PGL_EXCEPT_InvArg("The number of y points is inconsitent.");
	};
	if(pgl::isValidFloat(thisDt) == false || thisDt <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The timestep is invalid.");
	};
	if(::pgl::isValidFloat(currTime) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed current time is invalid.");
	};
	if(tStepIdx == (Size_t(-1)))
	{
		throw PGL_EXCEPT_InvArg("The time step index is invalid.");
	};
	if(!(pgl_implies(this->noIgnoredTypes() == false, mColl.hasIndexMap())))	//only if needed
	{
		throw PGL_EXCEPT_InvArg("No index map provided, but we need it.");
	};
	if(this->isSetUp() == false)
	{
		throw PGL_EXCEPT_LOGIC("*this is not setted up.");
	};
	if(nMarkers <= 0)
	{
		throw PGL_EXCEPT_InvArg("The marker collection is empty, with " + std::to_string(nMarkers) + " many markers");
	};

	const Index_t nBasicX = grid.xNPoints();		//get the size of the basic nodal grid
	const Index_t nBasicY = grid.yNPoints();

	/*
	 * This will allocate the feel velocity and also set it to zero
	 * The positions are allocated later
	 */
	this->m_feelVelX.setZero(nMarkers);
	this->m_feelVelY.setZero(nMarkers);


	/*
	 * Update the the corner velocities
	 */
	this->hook_computeCellVelocities(mSol, grid, gridCont.cgetApplyBC());


	/*
	 * Check if the values are correct
	 */
	if(this->m_cellVelX.isConsistent() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The cell velocities for X are not consistent.");
	};
	if(this->m_cellVelX.isVelX() == false)
	{
		throw PGL_EXCEPT_LOGIC("The x cell velocities are not a x velocity, but a " + m_cellVelX.getProp() + ".");
	};
	if(this->m_cellVelY.isConsistent() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The cell velocities for Y are not consistent.");
	};
	if(this->m_cellVelY.isVelY() == false)
	{
		throw PGL_EXCEPT_LOGIC("The y cell velocities are not a y velocity, but a " + m_cellVelY.getProp() + ".");
	};
	if(this->m_cellVelX.nCellX() + 1 != nBasicX)		//These checks assumes that we are on the basic cells
	{
		throw PGL_EXCEPT_RUNTIME("The cell velcoity for x component is wrong."
				" Expected " + std::to_string(nBasicX) + " nodes in x direction but got " + std::to_string(this->m_cellVelX.nCellX() + 1));
	};
	if(this->m_cellVelX.nCellY() + 1 != nBasicY)
	{
		throw PGL_EXCEPT_RUNTIME("The cell velcoity for x component is wrong."
				" Expected " + std::to_string(nBasicY) + " nodes in x direction but got " + std::to_string(this->m_cellVelX.nCellY() + 1));
	};
	if(this->m_cellVelY.nCellX() + 1 != nBasicX)
	{
		throw PGL_EXCEPT_RUNTIME("The cell velcoity for x component is wrong."
				" Expected " + std::to_string(nBasicX) + " nodes in x direction but got " + std::to_string(this->m_cellVelY.nCellX() + 1));
	};
	if(this->m_cellVelY.nCellY() + 1 != nBasicY)
	{
		throw PGL_EXCEPT_RUNTIME("The cell velcoity for x component is wrong."
				" Expected " + std::to_string(nBasicY) + " nodes in x direction but got " + std::to_string(this->m_cellVelY.nCellY() + 1));
	};
		pgl_assert(this->m_cellVelX.isValid(),		//Perform the expensive test only if needed.
			   this->m_cellVelY.isValid() );


	/*
	 * Load some parameter
	 */
	this->m_feelPosX                 = mColl.cgetXPos();		//copy the current locations of the markers in
	this->m_feelPosY                 = mColl.cgetYPos();		// the internal storage of *This; will also allocate
	const NodePositions_t&  xGridPos = grid.getXBasicPos();
	const NodePositions_t&  yGridPos = grid.getYBasicPos();
		pgl_assert(this->m_feelPosX.size() == nMarkers,
			   this->m_feelPosY.size() == nMarkers );


	/*
	 * Now we will perform teh actual marker movement.
	 */
	for(const auto& typeIt : mColl.indexes())
	{
		const Index_t         cType   = typeIt.first;	//unpack the iterator
		const IndexVector_t&  idxList = typeIt.second;

		if(this->isIgnoredType(cType) == true)	//Test if the type is ignored, and skip it if so.
		{
			continue;
		};


		/*
		 * Now we will iterate over all the markers in the associated list
		 */
		for(const Index_t& m : idxList)		//m is already the correct index
		{
				pgl_assert(m >= 0,
				   	   m <  this->m_feelPosX.size(),
				   	   m <  this->m_feelPosY.size() );

			const Numeric_t oldX_m  = this->m_feelPosX[m];	//current or old position of the marker
			const Numeric_t oldY_m  = this->m_feelPosY[m];
			      Numeric_t newX_m  = NAN;			//new position of the marker
			      Numeric_t newY_m  = NAN;
			      Numeric_t fVelX_m = NAN;			//feel velocity
			      Numeric_t fVelY_m = NAN;

			//Move one marker
			this->moveOneMarker(
					 oldY_m ,  oldX_m , 		//old position
					&newY_m , &newX_m , 		//new pos, as pointer
					&fVelY_m, &fVelX_m,		//feel velocity
					yGridPos, xGridPos,		//grid positions
					thisDt             );		//time step length

			//Check the computed marker position
				pgl_assert(isValidFloat(newX_m),
				   	   xGridPos[0] <= newX_m, newX_m <= xGridPos[xGridPos.size() - 1]);
				pgl_assert(isValidFloat(newY_m),
				   	   yGridPos[0] <= newY_m, newY_m <= yGridPos[yGridPos.size() - 1]);
				pgl_assert(isValidFloat(fVelX_m), isValidFloat(fVelY_m));

			//Store the new position in the internal vector
			this->m_feelPosX[m] = newX_m;
			this->m_feelPosY[m] = newY_m;

			//store the feel velocity, in the internal vector of this
			this->m_feelVelX[m] = fVelX_m;
			this->m_feelVelY[m] = fVelY_m;
		}; //End for(m): iterating over the index
	}; //End for(typeIt): iterating over all the indexes

	/*
	 * If the collection has a feel property,
	 * store the feel velocities in it
	 */
	if(mColl.hasFeelVel() == true)
	{
		mColl.getMarkerProperty(PropIdx::FeelVelX()) = this->m_feelVelX;
		mColl.getMarkerProperty(PropIdx::FeelVelY()) = this->m_feelVelY;
	}; //end: collection has feel prop

	/*
	 * Now we store the keys that we are using to identify the velocities
	 */
	this->m_feelDT   = thisDt;
	this->m_feelIdx  = tStepIdx;
	this->m_feelTime = currTime;

	return true;
}; //End: store feel velocity

void
egd_markerIntegrator_jmIBase_t::moveMarkers(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		gridCont,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
using ::pgl::isValidFloat;
	//Alias of the marker collection
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;

	const GridGeometry_t& grid = gridCont.getGeometry();	//Alias the grid geometry

	if(grid.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set up.");
	};
	if(grid.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not constant.");
	};
	if(mSol.isAllocated() == false)
	{
		throw PGL_EXCEPT_InvArg("The mechanical solution is not allocated.");
	};
	if(grid.xNPoints() != mSol.Nx())
	{
		throw PGL_EXCEPT_InvArg("The number of x points is not consitent.");
	};
	if(grid.yNPoints() != mSol.Ny())
	{
		throw PGL_EXCEPT_InvArg("The number of y points is inconsitent.");
	};
	if(pgl::isValidFloat(thisDt) == false || thisDt <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The timestep is invalid.");
	};
	if(::pgl::isValidFloat(currTime) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed current time is invalid.");
	};
	if(tStepIdx == (Size_t(-1)))
	{
		throw PGL_EXCEPT_InvArg("The time step index is invalid.");
	};
	if(this->isSetUp() == false)
	{
		throw PGL_EXCEPT_LOGIC("*this is not setup.");
	};
	if(!(pgl_implies(this->noIgnoredTypes() == false, mColl.hasIndexMap())))
	{
		throw PGL_EXCEPT_RUNTIME("Some materials are ignored, but collection does not provide an index map.");
	};

	//Get the number of markers
	const Index_t nMarkers = mColl.nMarkers();
		pgl_assert(nMarkers > 0);


	/*
	 * Test if we have to compute the feel position
	 */
	if(((!isValidFloat(this->m_feelDT  )) || (this->m_feelDT != thisDt    )) ||	//time increment
	   ((!isValidFloat(this->m_feelTime)) || (this->m_feelTime != currTime)) ||	//current time
	   ((this->m_feelIdx == (Size_t(-1))) || (this->m_feelIdx != tStepIdx ))   ) 	//index
	{
		/* We have to compute the feel velocity */
		const bool Res = this->storeFeelVel(mColl_, gridCont,
							mSol, tSol,
							thisDt, tStepIdx, currTime);

		if(Res == false)
		{
			throw PGL_EXCEPT_RUNTIME("FAiled to compute the feel velocities.");
		};
	}; //End if: need feel velocities to compute

	if(this->m_feelPosX.size() != nMarkers)
	{
		throw PGL_EXCEPT_RUNTIME("Failed to compute PosX, only " + std::to_string(this->m_feelPosX.size())
				+ " many markers, but a total of " + std::to_string(nMarkers) + " are expected.");
	};
	if(this->m_feelPosY.size() != nMarkers)
	{
		throw PGL_EXCEPT_RUNTIME("Failed to compute PosY, only " + std::to_string(this->m_feelPosY.size())
				+ " many markers, but a total of " + std::to_string(nMarkers) + " are expected.");
	};


	/*
	 * Copy the positions to the container
	 */
	mColl.getXPos() = this->m_feelPosX;
	mColl.getYPos() = this->m_feelPosY;

	return;
}; //End: move markers

PGL_NS_END(egd)


