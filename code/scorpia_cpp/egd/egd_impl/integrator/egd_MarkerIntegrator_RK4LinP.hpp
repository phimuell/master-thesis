#pragma once
/**
 * \brief	This is the concrete implementation of the LinP scheme, also known as (1/3)-(2/3) scheme.
 *
 * The scheme uses two velcoity sources.
 * The first one is given by the staggered grid points and the second source
 * is gioven by velocity that inpolated from teh pressure points.
 * See Pusok (2017) "On the Quality of Velocity Interpolation Schemes for Marker-in-Cell Method and Staggered Grids".
 * The scheme was originally developed by Taras Gerya, see "Introduction Numerical Geodynamical Modeling" Chap 8, section 6.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include "./egd_MarkerIntegrator_RK4ContinuumBased.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \brief	This class implements the empirical (1/3)-(2/3) scheme.
 * \class 	egd_markerIntegratorRK4LinP_t
 *
 * This class useses different velocity fields to compute the final used
 * field to move the markers. It Uses the staggered grid, that is computed
 * by the the solver and velocities that are present at the cell centre.
 * They can be combined to get the final velocity.
 *
 * This class is implmented by inherenting from the the CC class (continuum
 * basec interpolator) and overwritting some function.
 * This class is not very efficient, since it uses the provided interpolation
 * methods, which incure some memeory overhead. But it should be neglected
 * in this setting.
 */
class egd_markerIntegratorRK4LinP_t : public egd_markerIntegratorRK4Std_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t = egd_markerIntegratorRK4Std_t;		//This is the base class of *this
	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using Base_t::NodePositions_t;
	using Base_t::GridProperty_t;

	using Base_t::MarkerPosition_t;
	using Base_t::MarkerVelocity_t;
	using Base_t::MarkerProperty_t;
	using Base_t::ExtVelField_t;

	using Base_t::BoundaryCondition_t;
	using Base_t::ApplyBC_t;

	using Base_t::IgnoredTypeList_t;
	using Base_t::IndexVector_t;


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor will be forwarded to the explicit
	 * constructor of *this.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_markerIntegratorRK4LinP_t(
		const egd_confObj_t& 	confObj);

	/**
	 * \brief	This constructor is an explicit constructor.
	 *
	 * This function will set up the base by calling the explicit
	 * constructor of *this and then set up its other structures.
	 *
	 * \param  Ny		Number of basic grid points in y direction.
	 * \param  Nx 		Number of basic grid points in x direction.
	 * \param  bcVelX	The boundary condition for the velocity field in x direction.
	 * \param  bcVelY	The boundary condition for the velocity field in y direction.
	 * \param  ccWeight	The weight of the velocity of the cell centred scheme.
	 * \param  confObj	Pointer to a possible aviable configuration object.
	 */
	egd_markerIntegratorRK4LinP_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const BoundaryCondition_t& 	bcVelX,
		const BoundaryCondition_t& 	bcVelY,
		const Numeric_t 		ccWeight,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	This constructor is an explicit constructor.
	 *
	 * This constructor constructor uses 1/3 for the ccWeight, which is the
	 * recomended value. It will be forwarded to the full buiilding constructor
	 * no action will be done.
	 *
	 * \param  Ny		Number of basic grid points in y direction.
	 * \param  Nx 		Number of basic grid points in x direction.
	 * \param  bcVelX	The boundary condition for the velocity field in x direction.
	 * \param  bcVelY	The boundary condition for the velocity field in y direction.
	 * \param  confObj	Pointer to a possible aviable configuration object.
	 */
	egd_markerIntegratorRK4LinP_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const BoundaryCondition_t& 	bcVelX,
		const BoundaryCondition_t& 	bcVelY,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markerIntegratorRK4LinP_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4LinP_t(
		const egd_markerIntegratorRK4LinP_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4LinP_t&
	operator= (
		const egd_markerIntegratorRK4LinP_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4LinP_t(
		egd_markerIntegratorRK4LinP_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4LinP_t&
	operator= (
		egd_markerIntegratorRK4LinP_t&&);


	/*
	 * ==========================
	 * Private Constructor
	 */
private:
	/**
	 * \brief	Default constructor
	 */
	egd_markerIntegratorRK4LinP_t()
	 = delete;



	/*
	 * ===========================
	 * Interface functions
	 */
public:
	using Base_t::moveMarkers;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function is abstract and does not provide an implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;



	/*
	 * Interface functions of the interface
	 */
	using Base_t::isIgnoredType;
	using Base_t::addIgnoredType;
	using Base_t::noIgnoredTypes;


	/*
	 * =====================
	 * Protected Constructors
	 *
	 * All other constructors are protected and defaulted.
	 * They are also marked as noexcept
	 */
protected:


	/*
	 * ======================
	 * RK4 Base Hooks.
	 *
	 * These functions are called by the driver code and allows
	 * to customize the codes.
	 */
protected:
	/**
	 * \brief	This function will set up the internals.
	 *
	 * It will interpolate the velocity field from the staggered
	 * grid poinst to the pressure points on an extended grid.
	 * And apply the boundary conditions.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	hook_setupIntegrator(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function computes the velocity field
	 * 		 At the given positions.
	 *
	 * This function will use bininear interpolation to compute the velocity
	 * at the given points.
	 *
	 * \param  grid		This is the grid object.
	 * \param  xMPos	The x coordinates of the marker.
	 * \param  yMPos 	The y coordinates of the marker.
	 * \param  xMVel 	The x velocity of the marker that was interpolated.
	 * \param  yMVel 	The y velocity of the marker that was interpolated.
	 * \param  cTime	This is the current time, at which we evaluate the velocity field.
	 */
	virtual
	void
	hook_getMarkerVel(
		const GridContainer_t&		grid,
		const MarkerPosition_t& 	xMPos,
		const MarkerPosition_t& 	yMPos,
		      MarkerVelocity_t* const 	xMVel,
		      MarkerVelocity_t* const 	yMVel,
		const Numeric_t 		cTime)
	 const
	 override;


	/**
	 * \brief	This function will return true, if *this is set up.
	 *
	 * This function will check if the base is set up and then check
	 * if the velocity field is setted up.
	 */
	virtual
	bool
	isSetUp()
	 const
	 override;


	/*
	 * =====================
	 * Internal Query Functions
	 *
	 * These functions allows subclasses to access and query the internal
	 * status of the base class instance.
	 */
protected:
	using Base_t::getBCVelX;
	using Base_t::getBCVelY;
	using Base_t::getVelXCC;
	using Base_t::getVelYCC;


	/**
	 * \brief	This function sets up the cell centred velocity points.
	 *
	 * In essence this function forwards its request to the setup function
	 * of the base class, which uses the CC points.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	priv_setUpCCVel(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 final;


	/**
	 * \brief	This function sets up the staggered points.
	 *
	 * In essence this function copies the velocity fields from
	 * the solution object to internal memory.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	priv_setUpStaggVel(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 final;


	/**
	 * \brief	This function computes the velocity at the given points from
	 * 		 the CC field.
	 *
	 * In essence thsi function forwards the request to the base implementation.
	 *
	 * \param  grid		This is the grid object.
	 * \param  xMPos	The x coordinates of the marker.
	 * \param  yMPos 	The y coordinates of the marker.
	 * \param  xMVel 	The x velocity of the marker that was interpolated.
	 * \param  yMVel 	The y velocity of the marker that was interpolated.
	 * \param  cTime	This is the current time, at which we evaluate the velocity field.
	 */
	virtual
	void
	priv_getMarkerVelCC(
		const GridContainer_t&		grid,
		const MarkerPosition_t& 	xMPos,
		const MarkerPosition_t& 	yMPos,
		      MarkerVelocity_t* const 	xMVel,
		      MarkerVelocity_t* const 	yMVel,
		const Numeric_t 		cTime)
	 const
	 final;


	/**
	 * \brief	This function computes the velocity at the given points from
	 * 		 the staggered velocity points.
	 *
	 * The VelX component is calculated using the VX points and the VelY component
	 * is calculated using the VY points.
	 * Note that this function uses the interpolation routines that are provided by
	 * EGD, thus some memory overhead is incurred.
	 *
	 * \param  grid		This is the grid object.
	 * \param  xMPos	The x coordinates of the marker.
	 * \param  yMPos 	The y coordinates of the marker.
	 * \param  xMVel 	The x velocity of the marker that was interpolated.
	 * \param  yMVel 	The y velocity of the marker that was interpolated.
	 * \param  cTime	This is the current time, at which we evaluate the velocity field.
	 */
	virtual
	void
	priv_getMarkerVelStagg(
		const GridContainer_t&		grid,
		const MarkerPosition_t& 	xMPos,
		const MarkerPosition_t& 	yMPos,
		      MarkerVelocity_t* const 	xMVel,
		      MarkerVelocity_t* const 	yMVel,
		const Numeric_t 		cTime)
	 const
	 final;


	/*
	 * ========================
	 * Private Members
	 *
	 * We need the velocity
	 */
public:
	GridProperty_t 		m_velX;		//!< The x velocity field defined at the staggered grid points.
	GridProperty_t 		m_velY;		//!< The y velocity field defined at the staggered grid points.
	Numeric_t 		m_ccWeight;	//!< The weight of teh CC velocity field.
}; //End class(egd_markerIntegratorRK4LinP_t)


PGL_NS_END(egd)


