/**
 * \brief	This file contains the hook functions of the LinP integrator.
 *
 * They are split into functions that only operate on the CC grid and some that
 * are only operate on the staggered grid.
 * The true hooks will call them both and combine, if needed the resuilt and return it.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_RK4LinP.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


/*
 * =======================
 * Real RK4 Hooks.
 *
 * These are the hooks that are needed by the RK4 base class.
 */
bool
egd_markerIntegratorRK4LinP_t::isSetUp()
 const
{
	if(this->Base_t::isSetUp() == false)
	{
		pgl_assert(false && "Uninitialized base.");
		return false;
	};

	if((this->m_velX.onStVxPoints() == false           ) ||
	   (this->m_velX.getPropIdx().isVelX() == false    ) ||
	   (this->getVelXCC().checkOnSameGrid(this->m_velX))   )
	{
		pgl_assert(false && "velX is not valid.");
		return false;
	};

	if((this->m_velY.onStVyPoints() == false           ) ||
	   (this->m_velY.getPropIdx().isVelY() == false    ) ||
	   (this->getVelYCC().checkOnSameGrid(this->m_velY))   )
	{
		pgl_assert(false && "velY is not valid.");
		return false;
	};


        return true;
}; //End: isSetup


void
egd_markerIntegratorRK4LinP_t::hook_setupIntegrator(
	const MarkerCollection_t& 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	//First set up the base
	this->priv_setUpCCVel(mColl, grid,
			mSol, tSol,
			thisDt, tStepIdx, currTime);
		pgl_assert(this->Base_t::isSetUp());

	//now set up us
	this->priv_setUpStaggVel(mColl, grid,
			mSol, tSol,
			thisDt, tStepIdx, currTime);
		pgl_assert(this->isSetUp());

	return;
}; //End: setUp Integrator


void
egd_markerIntegratorRK4LinP_t::hook_getMarkerVel(
	const GridContainer_t&		grid,
	const MarkerPosition_t& 	xMPos,
	const MarkerPosition_t& 	yMPos,
	      MarkerVelocity_t* const 	xMVel_,
	      MarkerVelocity_t* const 	yMVel_,
	const Numeric_t 		cTime)
 const
{
using ::pgl::isValidFloat;
	pgl_assert(xMVel_ != nullptr, yMVel_ != nullptr);	//alias the valocity
	MarkerVelocity_t& xMVel = *xMVel_;
	MarkerVelocity_t& yMVel = *yMVel_;

		pgl_assert(this->isSetUp());

	//This are helper variables
	MarkerVelocity_t xMVel_stagg, yMVel_stagg,	//marker velocities on the staggered grid
			 xMVel_cc   , yMVel_cc;		//marker velocities on the cc grid

	//caluclate the velcoity on teh cc grid
	this->priv_getMarkerVelCC(grid, xMPos, yMPos, &xMVel_cc, &yMVel_cc, cTime);

	//caluclate the velocity on the staggered grid
	this->priv_getMarkerVelStagg(grid, xMPos, yMPos, &xMVel_stagg, &yMVel_stagg, cTime);

	/*
	 * Now combine the two velocities to one
	 */
	const Numeric_t weightCC =       this->m_ccWeight; 	pgl_assert(isValidFloat(weightCC), 0.0 <= weightCC, weightCC <= 1.0);
	const Numeric_t weightST = 1.0 - this->m_ccWeight; 	pgl_assert(isValidFloat(weightST), 0.0 <= weightST, weightST <= 1.0);

	xMVel = weightCC * xMVel_cc + weightST * xMVel_stagg;
	yMVel = weightCC * yMVel_cc + weightST * yMVel_stagg;
		pgl_assert(xMVel.isFinite().all(),
			   yMVel.isFinite().all() );

	return;
}; //End: compute the marker velocity



/*
 * ==================================
 * CC Operations
 */
void
egd_markerIntegratorRK4LinP_t::priv_setUpCCVel(
	const MarkerCollection_t& 	mColl,
	const GridContainer_t& 		grid,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	/*
	 * We only need to foirward the request to the standard implementation
	 */
	this->egd_markerIntegratorRK4Std_t::hook_setupIntegrator(
			mColl, grid, mSol, tSol,
			thisDt, tStepIdx, currTime);
	return;
}; //End: setup cc


void
egd_markerIntegratorRK4LinP_t::priv_getMarkerVelCC(
	const GridContainer_t&		grid,
	const MarkerPosition_t& 	xMPos,
	const MarkerPosition_t& 	yMPos,
	      MarkerVelocity_t* const 	xMVel,
	      MarkerVelocity_t* const 	yMVel,
	const Numeric_t 		cTime)
 const
{
	pgl_assert(xMVel != nullptr, yMVel != nullptr);
	/*
	 * We only need to forward the request
	 */
	this->egd_markerIntegratorRK4Std_t::hook_getMarkerVel(
		grid, xMPos, yMPos, xMVel, yMVel, cTime);
	return;
}; //End: compute velocity on CC


/*
 * =====================
 * Staggered grid
 */
void
egd_markerIntegratorRK4LinP_t::priv_setUpStaggVel(
	const MarkerCollection_t& 	mColl,
	const GridContainer_t& 		gridCont,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	/* We simply load the  */
	const GridGeometry_t&   gridGeo = gridCont.getGeometry();	//Load the geometry

	if(gridGeo.isNewVersion() )
	{
		throw PGL_EXCEPT_illMethod("The integrator can not handle a changed geometry.");
	};
	if(gridGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set.");
	};
	if(mkCase(mSol.getGridType()) != eGridType::StGrid)
	{
		throw PGL_EXCEPT_InvArg("The solution is not a stagered grid.");
	};


	/*
	 * SImply coping the the fields
	 */
	this->m_velX = mSol.cgetVelX();		pgl_assert(this->m_velX.getType() == mSol.cgetVelX().getType(), mSol.cgetVelX().checkOnSameGrid(this->m_velX));
	this->m_velY = mSol.cgetVelY();		pgl_assert(this->m_velY.getType() == mSol.cgetVelY().getType(), mSol.cgetVelY().checkOnSameGrid(this->m_velY));

	return;
	(void)mColl;
	(void)tSol;
	(void)thisDt;
	(void)tStepIdx;
	(void)currTime;
}; //End: setup the staggered grid


void
egd_markerIntegratorRK4LinP_t::priv_getMarkerVelStagg(
	const GridContainer_t&		grid,
	const MarkerPosition_t& 	xMPos,
	const MarkerPosition_t& 	yMPos,
	      MarkerVelocity_t* const 	xMVel,
	      MarkerVelocity_t* const 	yMVel,
	const Numeric_t 		cTime)
 const
{
	pgl_assert(xMVel != nullptr, yMVel != nullptr);

	//loading some values
	const GridGeometry_t& 	gridGeo = grid.getGeometry();

	//this is tehe interpolation state that we are using for the interpolation
	egd_interpolState_t intState;

	//Handle VX component
	{
		/* Set up the interpolation state */
		const bool noEmptyCell = egd_initInterpolation(
				yMPos, xMPos,
				gridGeo,
				this->m_velX.getGridType(),
				&intState,
				false,		//no processing, since only pull back is needed
				true,		//do disabeling of ghost node
				false);		// no local interpolation.

		/* Actually it is not needed that the celly are full, but we will check it anyway */
		if(noEmptyCell == false)
		{
			throw PGL_EXCEPT_RUNTIME("While setting up the interpolation state for the x velocity pull back, "
					"encountered en empty cell.");
		};

		/* Perform the pull back */
		egd_pullBackToMarker(
				xMVel,
				this->m_velX,
				intState,
				false);		//no update
			pgl_assert(xMVel->isFinite().all());
	}; //End scope: interpolating VX

	//Handle VY component
	{
		/* Set up the interpolation state */
		const bool noEmptyCell = egd_initInterpolation(
				yMPos, xMPos,
				gridGeo,
				this->m_velY.getGridType(),
				&intState,
				false,		//no processing, since only pull back is needed
				true,		//do disabeling of ghost node
				false);		// no local interpolation.

		/* Actually it is not needed that the celly are full, but we will check it anyway */
		if(noEmptyCell == false)
		{
			throw PGL_EXCEPT_RUNTIME("While setting up the interpolation state for the y velocity pull back, "
					"encountered en empty cell.");
		};

		/* Perform the pull back */
		egd_pullBackToMarker(
				yMVel,
				this->m_velY,
				intState,
				false);		//no update
			pgl_assert(yMVel->isFinite().all());
	}; //End scope: interpolating VX

	return;
	(void)cTime;
}; //End: compute velocity on CC

PGL_NS_END(egd)

