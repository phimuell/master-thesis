#pragma once
/**
 * \brief	This is the (old) standard interpolator.
 *
 * This file only is used to enable backwards compability.
 */

//Include the confg file
#include <egd_core.hpp>

//include the RK4 standard integrator
#include "./egd_MarkerIntegrator_RK4ContinuumBased.hpp"


//Include PGL
#include <pgl_core.hpp>

PGL_NS_START(egd)


/**
 * \brief	This is an alias that provides access to the standard integrator by its old name.
 * \typedef	egd_markerIntegratorStd_t
 *
 * The standard interpolar was refactored such that it will use the RK4 base class.
 * However for enabeling backwards compability an alias was created that allows
 * to access the integrator under its old name.
 */
using egd_markerIntegratorStd_t = egd_markerIntegratorRK4Std_t;

PGL_NS_END(egd)


