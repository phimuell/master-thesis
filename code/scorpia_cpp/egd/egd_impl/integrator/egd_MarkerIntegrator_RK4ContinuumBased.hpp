#pragma once
/**
 * \brief	This is the concrete implementation for the continuum based integrator.
 *
 * This was the original integrator.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include "./egd_MarkerIntegrator_RK4Base.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \brief	This is the standard integrator to move the marker.
 * \class 	egd_markerIntegratorRK4Std_t
 *
 * It interpolates the velocities to the pressure points and will
 * use that to compute the velocities of the markers.
 *
 * This is the orioginal integrator, but refitted into the
 * new integrator base class.
 */
class egd_markerIntegratorRK4Std_t : public egd_markerIntegratorRK4Base_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t = egd_markerIntegratorRK4Base_t;		//This is the base class of *this
	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using Base_t::GridGeometry_t;
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using Base_t::NodePositions_t;
	using Base_t::GridProperty_t;

	using Base_t::MarkerPosition_t;
	using Base_t::MarkerVelocity_t;
	using Base_t::MarkerProperty_t;
	using Base_t::ExtVelField_t;

	using Base_t::BoundaryCondition_t;
	using Base_t::ApplyBC_t;

	using Base_t::IgnoredTypeList_t;
	using Base_t::IndexVector_t;


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor will be forwarded to the explicit
	 * constructor of *this.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_markerIntegratorRK4Std_t(
		const egd_confObj_t& 	confObj);

	/**
	 * \brief	This constructor is an explicit constructor.
	 *
	 * This function will set up the base by calling the explicit
	 * constructor of *this and then set up its other structures.
	 *
	 * \param  Ny		Number of basic grid points in y direction.
	 * \param  Nx 		Number of basic grid points in x direction.
	 * \param  bcVelX	The boundary condition for the velocity field in x direction.
	 * \param  bcVelY	The boundary condition for the velocity field in y direction.
	 * \param  confObj	Pointer to a possible aviable configuration object.
	 */
	egd_markerIntegratorRK4Std_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const BoundaryCondition_t& 	bcVelX,
		const BoundaryCondition_t& 	bcVelY,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markerIntegratorRK4Std_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Std_t(
		const egd_markerIntegratorRK4Std_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Std_t&
	operator= (
		const egd_markerIntegratorRK4Std_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Std_t(
		egd_markerIntegratorRK4Std_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Std_t&
	operator= (
		egd_markerIntegratorRK4Std_t&&);


	/*
	 * ==========================
	 * Private Constructor
	 */
private:
	/**
	 * \brief	Default constructor
	 */
	egd_markerIntegratorRK4Std_t()
	 = delete;



	/*
	 * ===========================
	 * Interface functions
	 */
public:
	using Base_t::moveMarkers;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function is abstract and does not provide an implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override;



	/*
	 * Interface functions of the interface
	 */
	using Base_t::isIgnoredType;
	using Base_t::addIgnoredType;
	using Base_t::noIgnoredTypes;


	/*
	 * =====================
	 * Protected Constructors
	 *
	 * All other constructors are protected and defaulted.
	 * They are also marked as noexcept
	 */
protected:


	/*
	 * ======================
	 * RK4 Base Hooks.
	 *
	 * These functions are called by the driver code and allows
	 * to customize the codes.
	 */
protected:
	/**
	 * \brief	This function will set up the internals.
	 *
	 * It will interpolate the velocity field from the staggered
	 * grid poinst to the pressure points on an extended grid.
	 * And apply the boundary conditions.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	hook_setupIntegrator(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function computes the velocity field
	 * 		 At the given positions.
	 *
	 * This function will use bininear interpolation to compute the velocity
	 * at the given points.
	 *
	 * \param  grid		This is the grid object.
	 * \param  xMPos	The x coordinates of the marker.
	 * \param  yMPos 	The y coordinates of the marker.
	 * \param  xMVel 	The x velocity of the marker that was interpolated.
	 * \param  yMVel 	The y velocity of the marker that was interpolated.
	 * \param  cTime	This is the current time, at which we evaluate the velocity field.
	 *
	 * \note	This function does not use the interpolation routines that are provided
	 * 		 by EGD but uses internally coded (historical reasons).
	 */
	virtual
	void
	hook_getMarkerVel(
		const GridContainer_t&		grid,
		const MarkerPosition_t& 	xMPos,
		const MarkerPosition_t& 	yMPos,
		      MarkerVelocity_t* const 	xMVel,
		      MarkerVelocity_t* const 	yMVel,
		const Numeric_t 		cTime)
	 const
	 override;


	/**
	 * \brief	This function will return true, if *this is set up.
	 *
	 * This function will first call the test version that is implemented
	 * by the base and then verify the velocity points.
	 */
	virtual
	bool
	isSetUp()
	 const
	 override;


	/*
	 * =====================
	 * Internal Query Functions
	 *
	 * These functions allows subclasses to access and query the internal
	 * status of the base class instance.
	 */
protected:
	using Base_t::getBCVelX;
	using Base_t::getBCVelY;


	/**
	 * \brief	This function returns a constant reference to the y component of the
	 * 		 velocity defined on the CC grid.
	 */
	virtual
	const ExtVelField_t&
	getVelYCC()
	 const
	 final;


	/**
	 * \brief	This function returns a constant reference to the x component of the
	 * 		 velocity defined on the CC grid.
	 */
	virtual
	const ExtVelField_t&
	getVelXCC()
	 const
	 final;



	/*
	 * ========================
	 * Private Members
	 *
	 * We need the velocity
	 */
public:
	ExtVelField_t 		m_extVx;		//!< This is the extended velocity field for the x component.
	ExtVelField_t 		m_extVy;		//!< This is the extended Velocity field for the y component.
}; //End class(egd_markerIntegratorRK4Std_t)


PGL_NS_END(egd)


