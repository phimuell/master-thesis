/**
 * \brief	This file implements functions for the cell velcoity containers.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>

#include "./egd_MarkerIntegrator_jennyMeyer_cornerVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyer_cellVelocities.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


bool
egd_jmICellVel_t::isConsistent()
 const
 noexcept
{
	if(this->m_nCellX < 1)
	{
		pgl_assert(false && "m_nCellX too small.");
		return false;
	};
	if(this->m_nCellY < 1)
	{
		pgl_assert(false && "m_nCellY too small.");
		return false;
	};
	if(uSize_t(this->m_nCellX * this->m_nCellY) != this->m_cornerVels.size())
	{
		pgl_assert(false && "The array has the wrong size.");
		return false;
	};
	if(this->m_prop.isRepresentant() == false)
	{
		pgl_assert(false && "m_prop is not a representant");
		return false;
	};
	if(this->m_prop.isVelocity() == false)
	{
		pgl_assert(false && "m_prop is not a velcoity");
		return false;
	};


	/*
	 * No error detected
	 */
	return true;
}; //End: isConsistent


bool
egd_jmICellVel_t::isValid()
 const
 noexcept
{
	if(this->isConsistent() == false)
	{
		pgl_assert(false && "*this is not consistent.");
		return false;
	};

	for(const CornerVelocity_t& cv : this->m_cornerVels)
	{
		if(cv.isValid() == false)
		{
			pgl_assert(false && "Found an invalid corner value.");
			return false;
		};
	}; //End for(cv):

	/*
	 * No error detected
	 */
	return true;
}; //End: isValid


/*
 * ==========================
 */
egd_jmICellVel_t::egd_jmICellVel_t(
	const yNodeIdx_t 	nCellY,
	const xNodeIdx_t 	nCellX,
	const PropIdx_t 	velComp)
 :
  m_nCellY(nCellY),
  m_nCellX(nCellX),
  m_prop(velComp),
  m_cornerVels(nCellX * nCellY)
{
	/*
	 * *this must be consistent
	 */
	if(this->isConsistent() == false)
	{
		throw PGL_EXCEPT_LOGIC("Could not construct a consostent cell velocity object.");
	};
}; //End: building constructor



/*
 * ======================
 * Managing
 */
egd_jmICellVel_t&
egd_jmICellVel_t::resetCornerVel()
{
	if(this->isConsistent() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can only reset consistent containers.");
	};

	/*
	 * All the rest function on the velocities
	 */
	for(CornerVelocity_t& cv : this->m_cornerVels)
	{
		cv.resetVelocities();
	};//End for(cv):

	return *this;
}; //End: resetCornerVel


egd_jmICellVel_t&
egd_jmICellVel_t::clear()
{
	if(this->isConsistent() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not clear an inconsistent object.");
	};

	this->clear();	//call clear of the underlying container.
		pgl_assert(this->isClear());

	return *this;
}; //End: clear.


void
egd_jmICellVel_t::resize(
		const yNodeIdx_t 	nCellY,
		const xNodeIdx_t 	nCellX)
{
	if(this->isClear() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not resize a non clear container.");
	};
	if(nCellX < 2)
	{
		throw PGL_EXCEPT_InvArg("The number of basic nodal points in x was too small, it was " + std::to_string(nCellX));
	};
	if(nCellY < 2)
	{
		throw PGL_EXCEPT_InvArg("The number of basic nodal points in y was too small, it was " + std::to_string(nCellY));
	};
	pgl_assert(this->m_prop.isVelocity());


	/*
	 * Now we can update the internal members
	 */
	this->m_nCellX = nCellX;		//set the size
	this->m_nCellY = nCellY;

	this->m_cornerVels.resize(this->m_nCellX * this->m_nCellY);	//reallocate


	if(this->isConsistent() == false)	//check for success
	{
		throw PGL_EXCEPT_LOGIC("Failed to resize the cell velocity container.");
	};

	return;
}; //End: resize


/*
 * =======================
 * Default constructor
 */

egd_jmICellVel_t::egd_jmICellVel_t(
	const egd_jmICellVel_t&)
 = default;


egd_jmICellVel_t::egd_jmICellVel_t(
	egd_jmICellVel_t&&)
 noexcept
 = default;


egd_jmICellVel_t&
egd_jmICellVel_t::operator= (
	const egd_jmICellVel_t&)
 = default;


egd_jmICellVel_t&
egd_jmICellVel_t::operator= (
	egd_jmICellVel_t&&)
 = default;


egd_jmICellVel_t::~egd_jmICellVel_t()
 noexcept
 = default;


PGL_NS_END(egd)


