# Added the implementation of an integrator interface to the egd library

target_sources(
	egd
  PRIVATE
	# Standard RK4
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4Base.cpp"			# Base
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4Base_constructor.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4Base_driver.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4ContinuumBased.cpp"		# Continuum based
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4ContinuumBased_hooks.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4LinP.cpp" 			# LinP
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4LinP_hooks.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4Test64.cpp" 			# Test64
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4Test64_hooks.cpp"

	# Jenny-Meyer Integrator
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyerBase.cpp"		# Base
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyer_cellVelocities.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyerBase_constructor.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyerBase_oneMarker.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyerBase_driver.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyer_linMinModBC.cpp"	# actuall minmod interpolator


  PUBLIC
	# Standard RK4
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4Base.hpp"			# RK4
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4ContinuumBased.hpp"		# Continuum based
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_standard.hpp"			# Alias for compability
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4LinP.hpp" 			# LinP
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_RK4Test64.hpp" 			# Test64

	# Jenny-Meyer Integrator
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyerBase.hpp"			# Base
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyer_cornerVelocities.hpp"	# Helper
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyer_cellVelocities.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/egd_MarkerIntegrator_jennyMeyer_linMinModBC.hpp"		# minmod interpolator
)

