#pragma once
/**
 * \brief	This file implements some helper structure for the JennyMeyer integrator.
 */

//Include the confg file
#include <egd_core.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>
#include <pgl_preprocessor.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <string>
#include <initializer_list>


PGL_NS_START(egd)

//FWD the container of them
class egd_jmICellVel_t;


/**
 * \brief	This function stores the velocities of the corner points in one cell.
 * \class 	egd_jmICornerVel_t
 *
 * This function stores the velocities, one component, in one cell, It thus follows the paper.
 *
 *
 *    A y
 *    | U_c              U_d
 * 1 -|  x----------------x
 *    |  |                |
 *    |  |                |
 *    |  |                |
 *    |  |                |
 *    |  |                |
 *    |  |                |
 * 0 -|  x----------------x
 *    | U_a              U_b
 *    o---------------------------> x
 *       i                i
 *       0                1
 * Note that the method allows discontinuities, so U_b of one cell and U_a of the adiacent cell
 * must not be the same. This struct is a convenient wrapper arround the four values.
 *
 * This struct assumes that the square has size 1.
 */
class egd_jmICornerVel_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used


	/*
	 * =========================
	 * Constructor
	 */
public:
	/**
	 * \brief	The default constructo maes an invalid oobject.
	 *
	 * All velcoities are initialied with NAN.
	 */
	egd_jmICornerVel_t()
	 noexcept
	 = default;


	/**
	 * \brief	This is the building constructor
	 *
	 * It takes the velocity at the positions as arguments.
	 * This constructor throws if *this is invalid after constructon.
	 *
	 * \param  Ua		Velcoity at local node a ~ (0, 0)
	 * \param  Ub		Velcoity at local node b ~ (1, 0)
	 * \param  Uc 		Velcoity at local node c ~ (0, 1)
	 * \param  Ud 		Velocity at local node d ~ (1, 1)
	 */
	egd_jmICornerVel_t(
		const Numeric_t 	Ua,
		const Numeric_t 	Ub,
		const Numeric_t 	Uc,
		const Numeric_t 	Ud)
	 :
	  m_Ua(Ua),
	  m_Ub(Ub),
	  m_Uc(Uc),
	  m_Ud(Ud)
	{
		if(this->isInValid())
		{
			throw PGL_EXCEPT_InvArg("Passed invalid argument to a corner velocity object, values were: " + this->print());
		};
	};


	/**
	 * \brief	This function is also building constructor.
	 *
	 * It expects that the passed array has a length of four.
	 * the order is as you would expect it.
	 *
	 * \param  U		A list of the corner velocities.
	 */
	egd_jmICornerVel_t(
		const std::initializer_list<Numeric_t>& 	U)
	 :
	  egd_jmICornerVel_t()	//Will be invalid
	{
		if(U.size() != 4)
		{
			throw PGL_EXCEPT_InvArg("The passed inizalizer list has the wrong length, its length is " + std::to_string(U.size())
					+ ", but expected 4");
		};

		//Assigne the values
		this->m_Ua = *(U.begin()    );
		this->m_Ub = *(U.begin() + 1);
		this->m_Uc = *(U.begin() + 2);
		this->m_Ud = *(U.begin() + 3);

		if(this->isInValid())
		{
			throw PGL_EXCEPT_InvArg("Passed invalid argument to a corner velocity object, values were: " + this->print());
		};
	}; //End: constructz from list


	/**
	 * \brief	Copy constructor
	 */
	egd_jmICornerVel_t(
		const egd_jmICornerVel_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move constructor
	 */
	egd_jmICornerVel_t(
		egd_jmICornerVel_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignment
	 */
	egd_jmICornerVel_t&
	operator= (
		const egd_jmICornerVel_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment
	 */
	egd_jmICornerVel_t&
	operator= (
		egd_jmICornerVel_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Destructor
	 */
	~egd_jmICornerVel_t()
	 noexcept
	 = default;


	/*
	 * ==========================
	 * Status Functions
	 */
public:
	/**
	 * \brief	This function returns true if *this is valid.
	 *
	 * This is the case if all velocities are not NAN.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		return (::pgl::isValidFloat(this->m_Ua) &&
			::pgl::isValidFloat(this->m_Ub) &&
			::pgl::isValidFloat(this->m_Uc) &&
			::pgl::isValidFloat(this->m_Ud)    );
	}; //End: isValid


	/**
	 * \brief	This function returns true if *this is invalid.
	 */
	bool
	isInValid()
	 const
	 noexcept
	{
		return (this->isValid() == false);
	};//End: isInvalid


	/**
	 * \brief	This function returns a string that is a representation
	 * 		 of *this.
	 *
	 * This function can be used to generate an error text or so.
	 */
	std::string
	print()
	 const
	{
		std::string s;
		s += "{";
		s += "Ua = " + std::to_string(this->m_Ua) + ", ";
		s += "Ub = " + std::to_string(this->m_Ub) + ", ";
		s += "Uc = " + std::to_string(this->m_Uc) + ", ";
		s += "Ud = " + std::to_string(this->m_Ud)       ;
		s += "}";

		return s;
	}; //End print


	/**
	 * \brief	This function returns 4.
	 *
	 * This is for completing operator[].
	 */
	constexpr
	Index_t
	size()
	 const
	 noexcept
	{
		return 4;
	};



	/*
	 * ==========================
	 * Accessing
	 */
public:
	/**
	 * \brief	Returns the value in the corner point a.
	 */
	Numeric_t
	a()
	 const
	 noexcept
	{
		pgl_assert(::pgl::isValidFloat(this->m_Ua));
		return this->m_Ua;
	}; //End: a


	/**
	 * \brief	Returns the value in the corner point b.
	 */
	Numeric_t
	b()
	 const
	 noexcept
	{
		pgl_assert(::pgl::isValidFloat(this->m_Ub));
		return this->m_Ub;
	}; //End: b


	/**
	 * \brief	Returns the value in the corner point c.
	 */
	Numeric_t
	c()
	 const
	 noexcept
	{
		pgl_assert(::pgl::isValidFloat(this->m_Uc));
		return this->m_Uc;
	}; //End: c


	/**
	 * \brief	Returns the value in the corner point d.
	 */
	Numeric_t
	d()
	 const
	 noexcept
	{
		pgl_assert(::pgl::isValidFloat(this->m_Ud));
		return this->m_Ud;
	}; //End: d


	/**
	 * \brief	This function mimiks an array.
	 *
	 * The ordering is the same as the list initializer.
	 *
	 * \param  n		The id of the corner to access
	 */
	Numeric_t
	operator[] (
		const Index_t 		n)
	 const
	{
		if((n < 0) || (n >= 4))
		{
			throw PGL_EXCEPT_OutOfBound("Accessed the non existing corner " + std::to_string(n));
		};


		switch(n)
		{
		  case 0:
		  	return this->a(); break;

		  case 1:
			return this->b(); break;

		  case 2:
		  	return this->c(); break;

		  case 3:
		  	return this->d(); break;

		  default:
		  	/* Will never happen */
			pgl_assert(false && "reached unreachable code.");
			return NAN;
		}; //End switch(n)

		/* will never be here */
		pgl_assert(false && "reached unreachable code.");
		return NAN;
	}; //End: operator[]


	/*
	 * =========================
	 * Private Functions
	 */
private:
	/**
	 * \brief	This function sets all velocities to NAN.
	 *
	 * This is a managing function, that is only called in
	 * special situations.
	 */
	void
	resetVelocities()
	{
		this->m_Ua = NAN;
		this->m_Ub = NAN;
		this->m_Uc = NAN;
		this->m_Ud = NAN;

		return;
	};


	/*
	 * ======================
	 * Friends
	 */
private:
	/**
	 * \brief	Make the cell celocity container a friend.
	 */
	friend
	class egd_jmICellVel_t;


	/*
	 * ========================
	 * Private Members
	 */
private:
	Numeric_t 		m_Ua = NAN;	//!< Velocity at local node (0, 0)
	Numeric_t 		m_Ub = NAN;	//!< Velocity at local node (1, 0)
	Numeric_t 		m_Uc = NAN;	//!< Velcoity at local node (0, 1)
	Numeric_t 		m_Ud = NAN;	//!< Velocity at local node (1, 1)
}; //End class(egd_jmICornerVel_t):


/**
 * \brief	This macro can be used to unpack a corner velocity.
 * \define 	EGD_UNPACK_CORVEL
 *
 * This function creates variables of the form ${VAR}_{a, b, c, d}.
 * Where the variables are inizialized as you would expect it.
 *
 * \param  VAR		The base ob the variable that sbould be created.
 * \param  corVel	The corner velocity object name.
 */
#define EGD_UNPACK_CORVEL(VAR, corVel) const Numeric_t PGL_CAT(VAR, _a) = (corVel).a();		\
				       const Numeric_t PGL_CAT(VAR, _b) = (corVel).b();		\
				       const Numeric_t PGL_CAT(VAR, _c) = (corVel).c();		\
				       const Numeric_t PGL_CAT(VAR, _d) = (corVel).d();




PGL_NS_END(egd)


