/**
 * \brief	This file contains the implementation of the function to move one single marker.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>

#include "./egd_MarkerIntegrator_jennyMeyer_cornerVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyer_cellVelocities.hpp"
#include "./egd_MarkerIntegrator_jennyMeyerBase.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>
#if PGL_DEBUG_ON
#	include <iomanip>
#endif


PGL_NS_START(egd)


using NodePositions_t = egd_markerIntegrator_jmIBase_t::NodePositions_t;


/**
 * \brief	This function is able to test if a marker is still inside its
 * 		 originally associated cell.
 *
 * This function assumes that the marker is not associated to the last
 * cell in the passed array. it will respect some numerical glitches,
 * such as numerical cancelation. However it enforces that a patricel
 * is always inside the box.
 *
 * \param  asocIdx	ID of the associated cell
 * \param  markerPos	Position of the marker.
 * \param  nodePosArray	Array of the nodes.
 */
static
bool
isInsideCell(
	const Size_t		assocIdx,
	const Numeric_t 	markerPos,
	const NodePositions_t& 	nodePosArray);


/**
 * \brief	This function is able to compute the colision time.
 *
 * The is the time a marker has to travel until it encounters the
 * end of a cell. The used algorithm comes from computer graphic
 * where it is used for ray intersection. It operates on pre move
 * values.
 * Note that this function orders its argument as {x, y}.
 *
 * This function returns the time of the intersection.
 *
 * \param  X		Coordinate of the marker, before it was moved.
 * \param  V		Velcocity of marker
 * \param  CStart	Start of the cell, top left corner
 * \param  DX		Extension of the cell.
 */
static
Numeric_t
findIntersectionTime(
	const Numeric_t* 	X,
	const Numeric_t* 	V,
	const Numeric_t* 	CStart,
	const Numeric_t*	DX);




void
egd_markerIntegrator_jmIBase_t::moveOneMarker(
	const Numeric_t 		yOld_m,
	const Numeric_t 		xOld_m,
	      Numeric_t* const 		yNew_m,
	      Numeric_t* const 		xNew_m,
	      Numeric_t* const 		feelVelY_m,
	      Numeric_t* const 		feelVelX_m,
	const NodePositions_t& 		yGridPos,
	const NodePositions_t& 		xGridPos,
	const Numeric_t 		dtFull)	//this is interpreted as the time step size that is
 const
{
using pgl::isValidFloat;
using pgl::isVerySmall;
using pgl::lessOrEqualFP;
using std::fmin;
using std::fmax;
using std::fabs;
using std::numeric_limits;

	pgl_assert(xNew_m != nullptr,
		   yNew_m != nullptr );
	pgl_assert(feelVelX_m != nullptr,
		   feelVelY_m != nullptr );
	pgl_assert(::pgl::isValidFloat(dtFull),
		   dtFull > 0.0		       );

	//This is the lower bound for a psotive timestep
	//if a timestep is below this value, we will end the computation.
	constexpr static Numeric_t dtZERO = 10 * numeric_limits<Numeric_t>::epsilon();
		pgl_assert(dtFull > dtZERO);

	//The minimal timestep length is used to controle the probing for the cell crossing
	//If the cell is crossed, then the timestep is reduced. If the timestep falls bellow
	//this value then we will switch to different scheme that guarantees a clean crossing
	//at the expense of lower order
	constexpr static Index_t   DT_MAX_SPLITS = 8;	//empirical guess
	const            Numeric_t dtMinProb     = dtFull / Numeric_t(1 << DT_MAX_SPLITS);	//this is the minimal time we requiere to use RK

	/*
	 * Load some information
	 */
	const Index_t   nBasicX   = xGridPos.size();		//number of nodes
	const Index_t   nBasicY   = yGridPos.size();
	const Numeric_t xDomStart = xGridPos[0];		//start of the domain
	const Numeric_t yDomStart = yGridPos[0];
	const Numeric_t xDomEnd   = xGridPos[nBasicX - 1];	//end of the domain
	const Numeric_t yDomEnd   = yGridPos[nBasicY - 1];
	const Numeric_t dx        = xGridPos[1] - xGridPos[0];	//grid spacing
	const Numeric_t dy        = yGridPos[1] - yGridPos[0];

	/*
	 * These are running variables that are used during the iterations
	 */
	Numeric_t x_m    = xOld_m;		//These is the position of the marker, also during the marker movement.
	Numeric_t y_m    = yOld_m;
	Numeric_t dtLeft = dtFull;		//this is the time step size that has yet to be processed.
	Numeric_t dtDone = 0.0;			//this is is the time that is/was aalready processed.
	Numeric_t dtProp = dtLeft;		//this is the proposed time step for the next iteration
						// allows different iteration to compunicate with each other
						// DANGEROUS

	*feelVelX_m = 0.0;			//set the feel velocity to zero
	*feelVelY_m = 0.0;

	Numeric_t lastVelX = 0.0;		//This is the last experienced velocity
	Numeric_t lastVelY = 0.0;

	/*
	 * This loop allows subtime stepping.
	 */
	             Index_t nIntervals      =    0;	//Index for counting the numbers of intervals
	const static Index_t MAX_INTERATIONS = 1000;	//Maximal iterations we are perforoming, in theory we could exceed it.
	while(true)
	{
		pgl_assert(isValidFloat(dtLeft), dtLeft >  0.0,
			   isValidFloat(dtDone), dtDone >= 0.0, dtDone <= dtFull,
			   isValidFloat(dtProp), dtProp >= 0.0, dtProp <= dtLeft );
		pgl_assert(isValidFloat(x_m), xDomStart <= x_m, x_m <= xDomEnd,
			   isValidFloat(y_m), yDomStart <= y_m, y_m <= yDomEnd );

		if(nIntervals >= MAX_INTERATIONS)	//test if we have made to many iterations -> starvation
		{
			throw PGL_EXCEPT_RUNTIME("Detected starvation in the marker movement algorithm. Performed "
					+ std::to_string(nIntervals) + " iteration without progress.");
		};
		nIntervals += 1;	/* Increase the number of iterations, this positions makes it easy to work with it */
		/* ==================================== */


		const Numeric_t x_m1 = x_m - xDomStart;	//Adjust the offset
		const Numeric_t y_m1 = y_m - yDomStart;
			pgl_assert(x_m1 >= 0.0, y_m1 >= 0.0);

		const Numeric_t x_mc = x_m1 / dx;	//Caluclate the fractional index
		const Numeric_t y_mc = y_m1 / dy;

		//To get the index we must remove the fractional part.
		//We can do that by casting it into an ingeger.
		Size_t i = Size_t(y_mc);
		Size_t j = Size_t(x_mc);
			pgl_assert(0 <= i, i < nBasicY);	//This is a general test
			pgl_assert(0 <= j, j < nBasicX);
#			if PGL_DEBUG_ON
#			pragma message "Extended debugging."
			if(!(pgl::lessOrEqualFP(yGridPos[i], y_m)))
			{
				std::cout
					<< "==============================\n"
					<< "\t\tExtended Debugging Output\n"
					<< "\nStrange Bug in jm detected:\n"
					<< " i = " << i << "\n"
					<< " nBasicY = " << nBasicY << "\n"
					<< " yGrid[i] = " << std::setprecision(18) << std::fixed << yGridPos[i] << "\n"
					<< " y_m =  "     << std::setprecision(18) << std::fixed << y_m << "\n"
					<< " diff = "     << std::setprecision(18) << std::fixed << (y_m - yGridPos[i]) << "\n"
					<< " y_m1 = "     << std::setprecision(18) << std::fixed << y_m1 << "\n"
					<< " y_mc = "     << std::setprecision(18) << std::fixed << y_mc << "\n"
					<< "==============================\n"
					<< std::endl;
			}
#			endif
			pgl_assert(pgl::lessOrEqualFP(xGridPos[j], x_m),
				   pgl::lessOrEqualFP(yGridPos[i], y_m) );

		if(i == (nBasicY - 1))	//respect that the last index can not be associated with
			{ i -= 1; };
		if(j == (nBasicX - 1))
			{ j -= 1; };

		/* Use the RK4 mover to compute the next position */
		Numeric_t unRestMarkerMoveRaw_x = NAN,
			  unRestMarkerMoveRaw_y = NAN;
		const bool RK4Res = this->moveMarkerRK4(
				dtProp, 		//timestep
				x_m, y_m, 		//current position
				j, i, 			//cell association
				&lastVelX, &lastVelY,	//Velocity that was used to move
				xGridPos, yGridPos, 	//basic grid geometry
				&unRestMarkerMoveRaw_x, //return variables
				&unRestMarkerMoveRaw_y);

		/*
		 * Depending on the return value of RK4 res act appropriate
		 */
		if(RK4Res == true)
		{
			pgl_assert(isValidFloat(unRestMarkerMoveRaw_x), isInsideCell(j, unRestMarkerMoveRaw_x, xGridPos),
				   isValidFloat(unRestMarkerMoveRaw_y), isInsideCell(i, unRestMarkerMoveRaw_y, yGridPos) );

			/* The movement is completly contained inside the cell. */
			x_m = unRestMarkerMoveRaw_x;	//propagate the position
			y_m = unRestMarkerMoveRaw_y;
				pgl_assert(isValidFloat(x_m), xDomStart <= x_m, x_m <= xDomEnd,
					   isValidFloat(y_m), yDomStart <= y_m, y_m <= yDomEnd );


			//update the time.
			dtDone += dtProp;
			dtLeft -= dtProp;	pgl_assert(dtLeft >= 0.0 || isVerySmall(dtLeft));
			dtProp  = fmax(0.0, dtLeft);	//make sure that we have a nice zero

			//now we check if we have exhaused the resources
			if(dtProp <= dtZERO)
			{
				//We have exhaused our resources, so we decide to
				//exit the loop and call it a day
				break;
			}; //End if: handle the exit case

			/*
			 * We will now go to the next iteration
			 */
			continue;
		//End: movement is inside the cell
		}
		else
		{
			/*
			 * If we are here, then two things could have happened.
			 * 	- While using RK4, the marker crossed the cellm so RK4 exited preemtive.
			 * 	- RK4 was successfull, but the marker crossed the cell while we performed
			 * 	   the final movement.
			 */
			if(isValidFloat(unRestMarkerMoveRaw_x) == true)
			{
				/*
				 * If we are here, all 4 steps are inside the cell, but the
				 * final position was not. We will still count this as a success.
				 * The reason is that all four intermediate points where inside the
				 * same cell, so if the cell would not be limited, the result would
				 * not be different. Thus we considering it as success.
				 *
				 * Controll flow will not continue if entered this if-body.
				 * Either continue will restart the iteration or break will
				 * exit the loop
				 */

				x_m = unRestMarkerMoveRaw_x;	//propagate the position
				y_m = unRestMarkerMoveRaw_y;	pgl_assert(isValidFloat(unRestMarkerMoveRaw_y));  //Late test
					pgl_assert(isValidFloat(x_m), xDomStart <= x_m, x_m <= xDomEnd,
						   isValidFloat(y_m), yDomStart <= y_m, y_m <= yDomEnd );

				//update the time.
				dtDone += dtProp;
				dtLeft -= dtProp;	pgl_assert(dtLeft >= 0.0 || isVerySmall(dtLeft));
				dtProp  = fmax(0.0, dtLeft);	//make sure that we have a nice zero

				//now we check if we have exhaused the resources
				if(dtProp <= dtZERO)
				{
					//We have exhaused our resources, so we decide to
					//exit the loop and call it a day
					break;
				}; //End if: handle the exit case

				/*
				 * If we are here, then there is still work to do, so we
				 * will exit this iteration.
				 */
				continue;
			}; //End if: RK4 movement done successfully
				pgl_assert(isValidFloat(unRestMarkerMoveRaw_y) == false);


			/*
			 * If we are here then RK4 failed.
			 * This means we must reduce the timestep length and try anew.
			 */
			dtProp /= 2.0;	//half the probing step

			/* We must check if the probing time step is not to small
			 * if the timestep is good, we will restart the iteration with
			 * the smaller timestep */
			if(dtProp > dtMinProb)
				{ continue; };
			//End: try this probing step

			/*
			 * If we are here, then the probing timestep has got too small.
			 * So we will now fallback to a method which guarantees
			 * the crossing, at the expense of order, forward Euler
			 */

			//calculate the velocity at the current point
			Numeric_t mVelX = NAN,
				  mVelY = NAN;
			this->computeMarkerVel(x_m, y_m, j, i, xGridPos, yGridPos, &mVelX, &mVelY);


			/* Move the markers store them in temporarary variables
			 * use the timestep that is left not the probing step
			 * this will guarantee the crossing, hopefully */
			const Numeric_t unRestMarkerMoveEuler_x = x_m + dtLeft * mVelX;		//Simple linear Euler
			const Numeric_t unRestMarkerMoveEuler_y = y_m + dtLeft * mVelY;
				pgl_assert(isValidFloat(unRestMarkerMoveEuler_x), isValidFloat(unRestMarkerMoveEuler_y));

			/* Find out if the marker has left the cell */
			const bool iLeftCell = !isInsideCell(i, unRestMarkerMoveEuler_y, yGridPos);	//Left in i/y direction
			const bool jLeftCell = !isInsideCell(j, unRestMarkerMoveEuler_x, xGridPos);	//Left in j/x direction


			/* Test if the marker has left the building */
			if((iLeftCell == false) && (jLeftCell == false))
			{
				/* We are still inside the cell, but the marker should have left it.
				 * This is very unlikely, but I think it can happen, because of the
				 * simplicties of forward euler. We will consider it as a valid case */
				x_m = unRestMarkerMoveEuler_x;
				y_m = unRestMarkerMoveEuler_y;
					pgl_assert(isValidFloat(x_m), xDomStart <= x_m, x_m <= xDomEnd,
						   isValidFloat(y_m), yDomStart <= y_m, y_m <= yDomEnd );

				/* For the sake of completness we will now update the timestep
				* It is technically not necessary, but nicer when it is done */
				dtDone += dtLeft;	//we have used the full timestep budget
				dtLeft  = 0.0;		// no budget is left
				dtProp  = 0.0;

				/* exit the loop since we are done */
				break;
			//End: no cell crossing detected
			}
			else
			{
				/*
				 * We have detected cell crossing. We will now evaluate the time
				 * until the cell crossing occure and we will then
				 * move the markers to theier new position
				 */
				const Numeric_t X     [2] = { x_m        , y_m         };
				const Numeric_t V     [2] = { mVelX      , mVelY       };
				const Numeric_t CStart[2] = { xGridPos[j], yGridPos[i] };
				const Numeric_t DX    [2] = { dx         , dy          };

				const Numeric_t freeFlightTimeRaw2 = findIntersectionTime(X, V, CStart, DX);
					pgl_assert(isValidFloat(freeFlightTimeRaw2),
						   lessOrEqualFP(0.0, freeFlightTimeRaw2, 100));
					pgl_assert((freeFlightTimeRaw2 <= dtLeft) && "Maybe this test is a bit strong, it could happen in some situations.");

				/*
				 * Due to numerical instabilities, we have to ensure
				 * positivity of the free flight time.
				 * Note that the added time form bellow will ensure
				 * that the marker is moved away from teh boundary.
				 */
				const Numeric_t freeFlightTimeRaw = fmax(0.0, freeFlightTimeRaw2);


				/* We have to modify the free flight time a bit. The reason is
				 * that if we use the computed one, we can not be sure that we
				 * realy have left the cell completely. So we will increase the
				 * flight time by a tiny fraction. We neglect the movent of the
				 * marker that it does in the other time cell.
				 * Note that we are guaranteed to have a non zero velocity component.
				 * Because if it is none zero we wouldn't have movement. Also we
				 * could also use the movement in the other cell, but this would
				 * be a bit of an over kill */
				const Numeric_t freeFlightTime_addUp = 0.001 *
					fmax(
						((mVelX == 0.0)
					 	 ? 0.0
					 	 : fabs(dx / mVelX)),
					 	((mVelY == 0.0)
					 	 ? 0.0
					 	 : fabs(dy / mVelY))
						);
					pgl_assert(isValidFloat(freeFlightTime_addUp),
						   freeFlightTime_addUp > 0.0);
				const Numeric_t freeFlightTime = fmin(dtLeft, freeFlightTimeRaw + freeFlightTime_addUp);	//ensuring save movement

				/* Move the marker with the reff flight time */
				const Numeric_t ffMarkerMove_x = x_m + freeFlightTime * mVelX;		//Simple linear Euler
				const Numeric_t ffMarkerMove_y = y_m + freeFlightTime * mVelY;

				/* Now we set up everything for the next iteration */
				x_m = ffMarkerMove_x;	//transport marker
				y_m = ffMarkerMove_y;
					pgl_assert(isValidFloat(x_m), xDomStart <= x_m, x_m <= xDomEnd,
						   isValidFloat(y_m), yDomStart <= y_m, y_m <= yDomEnd );

				dtDone += freeFlightTime;	//update time
				dtLeft -= freeFlightTime;
				dtProp  = fmax(dtLeft, 0.0);	//try to move as far as possible next time

				/* We must have a marker transfer started */
#				if !defined(PGL_NDEBUG)
				const bool stillInsideI = isInsideCell(i, y_m, yGridPos);
				const bool stillInsideJ = isInsideCell(j, x_m, xGridPos);
				pgl_assert(!(stillInsideI && stillInsideJ));
#				endif

				/* Maybe it is possible to exit now, since only a small fraction of
				* time is left. This frees us from handling some corner cases in
				* the iteration that handles the next step. */
				if(dtLeft <= dtMinProb)
					{ break; };

				/*
				 * If we are here then we must restart the iteration
				 */
				continue;
			}; //End: cell crossing is detected
			pgl_assert(false && "reached unreachable code");
		}; //End: RK4 failed
		pgl_assert(false && "reached unreachable code");
	}; //End while(true): moving the markers

	//Perform some last checks
	pgl_assert(isValidFloat(x_m),
		   xGridPos[0] <= x_m, x_m <= xGridPos[xGridPos.size() - 1]);
	pgl_assert(isValidFloat(y_m),
		   yGridPos[0] <= y_m, y_m <= yGridPos[yGridPos.size() - 1]);

	/* Write the position in the output variables */
	*xNew_m = x_m;
	*yNew_m = y_m;

	/* The feel velocity that we compute can be the movement, or if
	 * we could it compute in one go, then we use it directly */
	if(nIntervals == 1)
	{
			pgl_assert(isValidFloat(lastVelX),
				   isValidFloat(lastVelY) );
		*feelVelX_m = lastVelX;
		*feelVelY_m = lastVelY;
	}
	else
	{
			pgl_assert(nIntervals > 1);
		/* compute the feel velocity, this will also compute
		 * the time weighted average */
		*feelVelX_m = (x_m - xOld_m) / dtFull;		pgl_assert(isValidFloat(*feelVelX_m));
		*feelVelY_m = (y_m - yOld_m) / dtFull;		pgl_assert(isValidFloat(*feelVelY_m));
	}; //End if: writting feel velocity

	return;
}; //End: move one marker



/*
 * ===============================
 * Helper functions
 */

egd_markerIntegrator_jmIBase_t::Index_t
egd_markerIntegrator_jmIBase_t::compCellId(
	const Numeric_t 	posM,
	const Numeric_t 	domStart,
	const Numeric_t 	gSpacing,
	const Index_t 		nBasicPos)
{
	using pgl::isValidFloat;
	pgl_assert(isValidFloat(posM    ),
		   isValidFloat(domStart), posM      >= domStart,
		   isValidFloat(gSpacing), gSpacing  >  0.0     ,
		   			   nBasicPos >= 2        );

	const Numeric_t relPos  = posM - domStart;	//get the relative position
	const Numeric_t fracIdx = relPos / gSpacing;	//get the fractional index
		pgl_assert(relPos >= 0.0);
		pgl_assert(isValidFloat(fracIdx));

	const Index_t idx = Index_t(fracIdx);		//We just need to remoive the fractional part.
							//This can be done by casting it into an int

	if(idx == (nBasicPos - 1))			//respect that last one, can not have an association
	{
		return (nBasicPos - 2);
	};

	//Return the normal one
	return idx;
}; //End: associated cell ID



/*
 * ==============================
 * Static In-File Helper functions
 */
bool
isInsideCell(
	const Size_t		assocIdx,
	const Numeric_t 	markerPos,
	const NodePositions_t& 	nodePosArray)
{
using pgl::isValidFloat;
using pgl::lessOrEqualFP;
	pgl_assert((assocIdx + 1) < (Size_t)nodePosArray.size());
	pgl_assert(isValidFloat(markerPos),
		   nodePosArray[0] <= markerPos,
		   markerPos       <= nodePosArray[nodePosArray.size() - 1]);

	//loding the positions suct that they can be used
	const Numeric_t assocNode  = nodePosArray[assocIdx    ];
	const Numeric_t assocNodeP = nodePosArray[assocIdx + 1];	//The past the end node

	/* Used for some numerical difficulties. */
	return (lessOrEqualFP(assocNode, markerPos ) &&
		lessOrEqualFP(markerPos, assocNodeP)   )
		? true
		: false;
}; //End: isInsideCell


Numeric_t
findIntersectionTime(
	const Numeric_t* 	X,
	const Numeric_t* 	V,
	const Numeric_t* 	CStart,
	const Numeric_t*	DX)
{
using std::swap;
using std::fmin;
using std::fmax;
using std::numeric_limits;
using pgl::isValidFloat;

	/*
	 * This implementation is based on the one of NORI
	 *   https://github.com/wjakob/nori/blob/master/include/nori/bbox.h
	 */
	Numeric_t nearT   = -numeric_limits<Numeric_t>::infinity();
	Numeric_t farT    =  numeric_limits<Numeric_t>::infinity();
	bool      zeroVel = true;	//indicates of zero valus where detected.

	for (int i = 0; i < 2; i++)
	{
		const Numeric_t origin = X[i];
		const Numeric_t minVal = CStart[i];
		const Numeric_t maxVal = minVal + DX[i];
		const Numeric_t vi     = V[i];
		const Numeric_t invVi  = 1.0 / vi;
			pgl_assert(minVal <= origin, origin <= maxVal);	//marker is contained

		if (V[i] == 0)
		{
			/* This check comes from nory, that operates
			 * on some different assumtion. It is here for reference */
			if (origin < minVal || origin > maxVal)
			{
				throw PGL_EXCEPT_RUNTIME("Detected a wrong intersection.");
			}
		}
		else
		{
			Numeric_t t1 = (minVal - origin) * invVi;
			Numeric_t t2 = (maxVal - origin) * invVi;

			if (t1 > t2)
				{ swap(t1, t2);	}

			nearT = fmax(t1, nearT);
			farT  = fmin(t2, farT );

			if (!(nearT <= farT))
			{
				throw PGL_EXCEPT_RUNTIME("Do not detected an intersection at all.");
			}

			//we have found a non zero value
			zeroVel = false;
		}
	}; //End for(i):

		/* We can currently not handle this case,
		 * so we check it that we see it. */
		pgl_assert(zeroVel == false);


	/*
	 * This is the interection time. Since we are guaranteed
	 * to be inside a box, we can make some strong conclusions.
	 * First, we will always have a positive and a negative
	 * solution. The negative solution is unphysical, since it
	 * means that we have to go backwards, which the particle
	 * is never doing, so we can just return the largest of
	 * the two.
	 */
		pgl_assert(isValidFloat(nearT), isValidFloat(farT));
		pgl_assert(nearT <= farT);
		pgl_assert((nearT < 0.0) != (farT < 0.0));
	return fmax(farT, nearT); // ~ max(max(farT, 0.0), max(nearT, 0.0));
}; //End: getIntersectionTime


void
egd_markerIntegrator_jmIBase_t::computeMarkerVel(
	const Numeric_t 		x_m,
	const Numeric_t 		y_m,
	const Size_t 			j,	/* Different ordering than before */
	const Size_t 			i,
	const NodePositions_t& 		xGridPos,
	const NodePositions_t& 		yGridPos,
	      Numeric_t* const 		xV,
	      Numeric_t* const 		yV)
 const
{
	using pgl::isValidFloat;
	using std::fmin;
	using std::fmax;
	using std::fabs;

		pgl_assert(xV != nullptr, yV != nullptr);
		pgl_assert(isInsideCell(j, x_m, xGridPos),
			   isInsideCell(i, y_m, yGridPos) );
	const Numeric_t xDomStart = xGridPos[0];		//start of the domain
	const Numeric_t yDomStart = yGridPos[0];
	const Numeric_t dx        = xGridPos[1] - xGridPos[0];	//grid spacing
	const Numeric_t dy        = yGridPos[1] - yGridPos[0];
	const Numeric_t x_m1 	  = x_m - xDomStart;		//Adjust the offset
	const Numeric_t y_m1  	  = y_m - yDomStart;
		pgl_assert(x_m1 >= 0.0, y_m1 >= 0.0);

	//now get the velocity in that cell
	const CornerVelocities_t& corVelX = this->m_cellVelX(i, j);
	const CornerVelocities_t& corVelY = this->m_cellVelY(i, j);

	//Now we compute the local coordinate of the marker. Formaly we transform the
	//rechtangular cell into a square cell. For that we will (ab)use the weight normalizer
	const Numeric_t xM_loc_raw = (x_m - xGridPos[j]) / dx;			//This are raw local coordinates
	const Numeric_t yM_loc_raw = (y_m - yGridPos[i]) / dy;
	const Numeric_t xM_loc     = egd_normalizeWeightComp(xM_loc_raw);	//Ensure that they are inside [0, 1]
	const Numeric_t yM_loc     = egd_normalizeWeightComp(yM_loc_raw);

	//compute the weights, for bilinear interpolation
	const Numeric_t compX   = xM_loc;	//components (helper variables, and for clarity(
	const Numeric_t compY   = yM_loc;
	const Numeric_t omega_a = (1.0 - compX) * (1.0 - compY);	//node a (x = 0, y = 0)
	const Numeric_t omega_b = (      compX) * (1.0 - compY);	//node b (x = 1, y = 0)
	const Numeric_t omega_c = (1.0 - compX) * (      compY);	//node c (x = 0, y = 1)
	const Numeric_t omega_d = (      compX) * (      compY);	//node d (x = 1, y = 1)

	//Unpack the velocities
	EGD_UNPACK_CORVEL(Ux, corVelX);
	EGD_UNPACK_CORVEL(Uy, corVelY);


	/*
	 * Compute the bilinear component
	 */
	const Numeric_t Ux_linPart =   omega_a * Ux_a
				     + omega_b * Ux_b
				     + omega_c * Ux_c
				     + omega_d * Ux_d;
	const Numeric_t Uy_linPart =   omega_a * Uy_a
				     + omega_b * Uy_b
				     + omega_c * Uy_c
				     + omega_d * Uy_d;


	/*
	 * Compute the correction terms.
	 * We assume here, as in Meyer/Jenny 2004, that $U_{1}^{e} == U_{1}^{f}$
	 * and $U_{2}^{g} == U_{2}^{h}$ is. This will also change the $\Delta U_{1, 2}$
	 * factors because the variation is removed.
	 */
	const Numeric_t Ux_ef = -(dx / dy) * ((Uy_a - Uy_b) + (-Uy_c + Uy_d));
	const Numeric_t Uy_gh = -(dy / dx) * ((Ux_a - Ux_b) + (-Ux_c + Ux_d));

	const Numeric_t deltaUx = xM_loc * 0.5 * (1.0 - xM_loc) * (-Ux_ef);	//Simplified last parametese from paper under
	const Numeric_t deltaUy = yM_loc * 0.5 * (1.0 - yM_loc) * (-Uy_gh);	// the assumption U_e == U_f also simplified


	/* now compute the velocity */
	const Numeric_t mVelX = Ux_linPart + deltaUx;
	const Numeric_t mVelY = Uy_linPart + deltaUy;
		pgl_assert(isValidFloat(mVelX),
			   isValidFloat(mVelY) );


	/* Write them back */
	*xV = mVelX;
	*yV = mVelY;

	return;
}; //End: marker celocity


bool
egd_markerIntegrator_jmIBase_t::moveMarkerRK4(
	const Numeric_t 		dt,
	const Numeric_t 		xM,
	const Numeric_t 		yM,
	const Size_t 			j,	/* Different ordering than before */
	const Size_t 			i,
	      Numeric_t* const 		moveVelX,
	      Numeric_t* const 		moveVelY,
	const NodePositions_t& 		xBasicPos,
	const NodePositions_t& 		yBasicPos,
	      Numeric_t* const 		newPosX,
	      Numeric_t* const 		newPosY)
 const
{
	using pgl::isValidFloat;
		pgl_assert(newPosX != nullptr,
			   newPosY != nullptr );
		pgl_assert(0 <= i, i < yBasicPos.size(),
			   0 <= j, j < xBasicPos.size(),
			   isValidFloat(dt), dt > 0.0   );
		pgl_assert(isInsideCell(i, yM, yBasicPos),
			   isInsideCell(j, xM, xBasicPos) );

	//Write NAN into the return variable, such that the correct value is there
	*newPosX = NAN;
	*newPosY = NAN;

	//Write NAN in the velocity variable if given
	if(moveVelX != nullptr)
	{
			pgl_assert(moveVelY != nullptr);
		*moveVelX = NAN;
		*moveVelY = NAN;
	}; //End if: write nan


	/*
	 * RK4:
	 *
	 * 	k_1 := f(\vec{y}                  )
	 * 	k_2 := f(\vec{y} + \frac{h}{2} k_1)
	 *	k_3 := f(\vec{y} + \frac{h}{2} k_2)
	 *	k_4 := f(\vec{y} + h * k_3    )
	 *  y^{(+1)} := \vec{y} + \frac{h}{6}(k_1 + 2 * k_2 + 2 * k_3 + k_4)
	 *
	 * f is the the function that calculates velocity at a specific location
	 * and k is that velocity.
	 */

	/* k_1 */
	Numeric_t vel1_x = NAN,
		  vel1_y = NAN;
	this->computeMarkerVel(xM, yM, j, i, xBasicPos, yBasicPos, &vel1_x, &vel1_y);
		pgl_assert(isValidFloat(vel1_x), isValidFloat(vel1_y));
	const Numeric_t k1_x = vel1_x;
	const Numeric_t k1_y = vel1_y;


	/* k_2 */
	const Numeric_t pos2_x = xM + 0.5 * dt * k1_x;	//move to the second position
	const Numeric_t pos2_y = yM + 0.5 * dt * k1_y;

	//test if we have left the cell
	if(!(isInsideCell(i, pos2_y, yBasicPos) &&
	     isInsideCell(j, pos2_x, xBasicPos)   ))
		{ return false; };

	Numeric_t vel2_x = NAN,
		  vel2_y = NAN;
	this->computeMarkerVel(pos2_x, pos2_y, j, i, xBasicPos, yBasicPos, &vel2_x, &vel2_y);
		pgl_assert(isValidFloat(vel2_x), isValidFloat(vel2_y));
	const Numeric_t k2_x = vel2_x;
	const Numeric_t k2_y = vel2_y;


	/* k_3 */
	const Numeric_t pos3_x = xM + 0.5 * dt * k2_x;
	const Numeric_t pos3_y = yM + 0.5 * dt * k2_y;

	//test if we have left the cell
	if(!(isInsideCell(i, pos3_y, yBasicPos) &&
	     isInsideCell(j, pos3_x, xBasicPos)   ))
		{ return false; };

	Numeric_t vel3_x = NAN,
		  vel3_y = NAN;
	this->computeMarkerVel(pos3_x, pos3_y, j, i, xBasicPos, yBasicPos, &vel3_x, &vel3_y);
		pgl_assert(isValidFloat(vel3_x), isValidFloat(vel3_y));
	const Numeric_t k3_x = vel3_x;
	const Numeric_t k3_y = vel3_y;


	/* k_4 */
	const Numeric_t pos4_x = xM + dt * k3_x;
	const Numeric_t pos4_y = yM + dt * k3_y;

	//test if we have left the cell
	if(!(isInsideCell(i, pos4_y, yBasicPos) &&
	     isInsideCell(j, pos4_x, xBasicPos)   ))
		{ return false; };

	Numeric_t vel4_x = NAN,
		  vel4_y = NAN;
	this->computeMarkerVel(pos4_x, pos4_y, j, i, xBasicPos, yBasicPos, &vel4_x, &vel4_y);
		pgl_assert(isValidFloat(vel4_x), isValidFloat(vel4_y));
	const Numeric_t k4_x = vel4_x;
	const Numeric_t k4_y = vel4_y;

	/* Get teh final velocity */
	const Numeric_t velFinal_x = ((k1_x + 2.0 * k2_x) + (2.0 * k3_x + k4_x)) / 6.0;
	const Numeric_t velFinal_y = ((k1_y + 2.0 * k2_y) + (2.0 * k3_y + k4_y)) / 6.0;

	/* get the final position */
	const Numeric_t fPosX = xM + dt * velFinal_x;
	const Numeric_t fPosY = yM + dt * velFinal_y;
		pgl_assert(isValidFloat(fPosX), isValidFloat(fPosY));

	/* Write the position in the output variables */
	*newPosX = fPosX;
	*newPosY = fPosY;

	/* write the final Velocity into the output variable
	 * however only if given */
	if(moveVelX != nullptr)
	{
		*moveVelX = velFinal_x;
		*moveVelY = velFinal_y;
	};//End if: write velocity back

	/* Now we must test if the marker is still inside the cell.
	 * This is requiered by the interface */
	if(isInsideCell(i, fPosY, yBasicPos) &&
	   isInsideCell(j, fPosX, xBasicPos)   )
	{
		/* we are inside the same cell as we have started */
		return false;
	}

	/* We are not in teh same cell as we have started
	 * we do not overwrite the computed position, because it
	 * is not bad, because it was found soly inside the cell. */
	return false;
}; //End: RK4 mover


PGL_NS_END(egd)

