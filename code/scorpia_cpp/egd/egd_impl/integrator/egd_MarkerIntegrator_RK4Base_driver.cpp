/**
 * \brief	This file contains the driver code for the RK4 integration.
 *
 * This file contains teh actuall RK4 method.
 *
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_RK4Base.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


bool
egd_markerIntegratorRK4Base_t::storeFeelVel(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		gridCont,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
	//Alias of the marker collection
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;

	const GridGeometry_t& grid = gridCont.getGeometry();	//Alias the grid geometry

	if(grid.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set up.");
	};
	if(grid.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not constant.");
	};
	if(mSol.isAllocated() == false)
	{
		throw PGL_EXCEPT_InvArg("The mechanical solution is not allocated.");
	};
	if(grid.xNPoints() != mSol.Nx())
	{
		throw PGL_EXCEPT_InvArg("The number of x points is not consitent.");
	};
	if(grid.yNPoints() != mSol.Ny())
	{
		throw PGL_EXCEPT_InvArg("The number of y points is inconsitent.");
	};
	if(pgl::isValidFloat(thisDt) == false || thisDt <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The timestep is invalid.");
	};
	if(::pgl::isValidFloat(currTime) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed current time is invalid.");
	};
	if(tStepIdx == (Size_t(-1)))
	{
		throw PGL_EXCEPT_InvArg("The time step index is invalid.");
	};
	if(this->isSetUp() == false)
	{
		throw PGL_EXCEPT_LOGIC("*this is not setup.");
	};
	if(!(pgl_implies(this->noIgnoredTypes() == false, mColl.hasIndexMap())))
	{
		throw PGL_EXCEPT_RUNTIME("Some materials are ignored, but collection does not provide an index map.");
	};


	//Get the number of markers
	const Index_t nMarkers = mColl.nMarkers();
		pgl_assert(nMarkers > 0);

	/*
	 * Set the feel velocity to zero.
	 * Note maybe not allocated yet.
	 */
	this->m_feelVelX.setZero();
	this->m_feelVelY.setZero();	pgl_assert(this->m_feelVelX.size() == this->m_feelVelY.size());

	/*
	 * Setup the internal structures
	 */
	this->hook_setupIntegrator(mColl, gridCont, mSol, tSol, thisDt, tStepIdx, currTime);


	/*
	 * Position  A
	 * Evaluate the markers where they are
	 */
	MarkerVelocity_t xMVel_a, yMVel_a;
	this->hook_getMarkerVel(gridCont, mColl.cgetXPos(), mColl.cgetYPos(), &xMVel_a, &yMVel_a, currTime);


	/*
	 * Position B
	 * This is:
	 * 	x_b = x_0 + 1/2 * dt * v_a
	 */
	const MarkerPosition_t xMPos_b = mColl.cgetXPos() + 0.5 * thisDt * xMVel_a;
	const MarkerPosition_t yMPos_b = mColl.cgetYPos() + 0.5 * thisDt * yMVel_a;

	MarkerVelocity_t xMVel_b, yMVel_b;
	this->hook_getMarkerVel(gridCont, xMPos_b, yMPos_b, &xMVel_b, &yMVel_b, currTime + 0.5 * thisDt);


	/*
	 * Position C
	 * This is:
	 * 	x_c = x_0 + 1/2 * dt * v_b
	 */
	const MarkerPosition_t xMPos_c = mColl.cgetXPos() + 0.5 * thisDt * xMVel_b;
	const MarkerPosition_t yMPos_c = mColl.cgetYPos() + 0.5 * thisDt * yMVel_b;

	MarkerVelocity_t xMVel_c, yMVel_c;
	this->hook_getMarkerVel(gridCont, xMPos_c, yMPos_c, &xMVel_c, &yMVel_c, currTime + 0.5 * thisDt);


	/*
	 * Position D
	 * This is:
	 * 	x_d = x_0 + dt * v_c
	 */
	const MarkerPosition_t xMPos_d = mColl.cgetXPos() + thisDt * xMVel_c;
	const MarkerPosition_t yMPos_d = mColl.cgetYPos() + thisDt * yMVel_c;

	MarkerVelocity_t xMVel_d, yMVel_d;
	this->hook_getMarkerVel(gridCont, xMPos_d, yMPos_d, &xMVel_d, &yMVel_d, currTime + thisDt);


	/*
	 * The effective velocity is now computed as
	 * 	v_eff = 1/6 * ( v_a + 2 * v_b + 2 * v_c + v_d)
	 * Belief in Eigen.
	 * Store them in the feel velocity functions.
	 */
	this->m_feelVelX = (xMVel_a + 2 * xMVel_b + 2 * xMVel_c + xMVel_d) / 6.0;
	this->m_feelVelY = (yMVel_a + 2 * yMVel_b + 2 * yMVel_c + yMVel_d) / 6.0;
		pgl_assert(this->m_feelVelX.size() == nMarkers,
			   this->m_feelVelY.size() == nMarkers );

#	if PGL_DEBUG_ON != 0
	//Loading the type property
	const MarkerProperty_t& mTypeColl = mColl.cgetMarkerProperty(PropIdx::Type());
#	endif

	/*
	 * Handling the ignored part
	 *  The feel velocity of an ignored marker is zero.
	 */
	for(const auto& it : mColl.indexes() )
	{
		const Index_t         mType = it.first;		//get the current material type
		const IndexVector_t&  mIdx  = it.second;	//get the associated indexes

		//Test if this is NOT an ignored type
		if(this->isIgnoredType(mType) == false)
		{
			/*
			 * This type is not ignored, so it will bew moved.
			 * this means the feel velocity is already correct.
			 */
				pgl_assert(mIdx.size() > 0);
			continue;
		};//End: is an ignred type

		/*
		 * If we are here, then we ignore this type.
		 * So we must set the feel velocity to zero.
		 */
			pgl_assert(mIdx.size() > 0);

		for(const Index_t m : mIdx)
		{
				pgl_assert(Index_t(mTypeColl[m]) == mType);	//check if type is correct

			//set the feel velocity to zero
			this->m_feelVelX[m] = 0.0;
			this->m_feelVelY[m] = 0.0;
		}; //End for(m): zero the ignored feel felocity
	}; //End for(it): going through all types


	/*
	 * Save the feel velocity if the map supports it
	 */
	if(mColl.hasFeelVel() == true)
	{
		mColl.getMarkerProperty(PropIdx::FeelVelX()) = this->m_feelVelX;
		mColl.getMarkerProperty(PropIdx::FeelVelY()) = this->m_feelVelY;
	}; //End if: set feel velocity

	/*
	 * Now we store the keys that we are using to identify the velocities
	 */
	this->m_feelDT   = thisDt;
	this->m_feelIdx  = tStepIdx;
	this->m_feelTime = currTime;


	/*
	 * We return true, to indicate that we have computed the feel velocity
	 */
	return true;
}; //ENd: compute the feel vel


void
egd_markerIntegratorRK4Base_t::moveMarkers(
	MarkerCollection_t* const 	mColl_,
	const GridContainer_t& 		gridCont,
	const MechSolverResult_t&	mSol,
	const TempSolverResult_t&	tSol,
	const Numeric_t 		thisDt,
	const Size_t 			tStepIdx,
	const Numeric_t 		currTime)
{
using ::pgl::isValidFloat;
	//Alias of the marker collection
	pgl_assert(mColl_ != nullptr);
	MarkerCollection_t& mColl = *mColl_;

	const GridGeometry_t& grid = gridCont.getGeometry();	//Alias the grid geometry

	if(grid.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not set up.");
	};
	if(grid.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("The grid is not constant.");
	};
	if(mSol.isAllocated() == false)
	{
		throw PGL_EXCEPT_InvArg("The mechanical solution is not allocated.");
	};
	if(grid.xNPoints() != mSol.Nx())
	{
		throw PGL_EXCEPT_InvArg("The number of x points is not consitent.");
	};
	if(grid.yNPoints() != mSol.Ny())
	{
		throw PGL_EXCEPT_InvArg("The number of y points is inconsitent.");
	};
	if(pgl::isValidFloat(thisDt) == false || thisDt <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The timestep is invalid.");
	};
	if(::pgl::isValidFloat(currTime) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed current time is invalid.");
	};
	if(tStepIdx == (Size_t(-1)))
	{
		throw PGL_EXCEPT_InvArg("The time step index is invalid.");
	};
	if(this->isSetUp() == false)
	{
		throw PGL_EXCEPT_LOGIC("*this is not setup.");
	};
	if(!(pgl_implies(this->noIgnoredTypes() == false, mColl.hasIndexMap())))
	{
		throw PGL_EXCEPT_RUNTIME("Some materials are ignored, but collection does not provide an index map.");
	};

	//Get the number of markers
	const Index_t nMarkers = mColl.nMarkers();
		pgl_assert(nMarkers > 0);


	/*
	 * Test if we have to compute the feel velocity
	 */
	if(((!isValidFloat(this->m_feelDT  )) || (this->m_feelDT != thisDt    )) ||	//time increment
	   ((!isValidFloat(this->m_feelTime)) || (this->m_feelTime != currTime)) ||	//current time
	   ((this->m_feelIdx == (Size_t(-1))) || (this->m_feelIdx != tStepIdx ))   ) 	//index
	{
		/* We have to compute the feel velocity */
		const bool Res = this->storeFeelVel(mColl_, gridCont,
							mSol, tSol,
							thisDt, tStepIdx, currTime);

		if(Res == false)
		{
			throw PGL_EXCEPT_RUNTIME("FAiled to compute the feel velocities.");
		};
	}; //End if: need feel velocities to compute

	if(this->m_feelVelX.size() != nMarkers)
	{
		throw PGL_EXCEPT_RUNTIME("Failed to compute VelX, only " + std::to_string(this->m_feelVelX.size())
				+ " many markers, but a total of " + std::to_string(nMarkers) + " are expected.");
	};
	if(this->m_feelVelY.size() != nMarkers)
	{
		throw PGL_EXCEPT_RUNTIME("Failed to compute VelY, only " + std::to_string(this->m_feelVelY.size())
				+ " many markers, but a total of " + std::to_string(nMarkers) + " are expected.");
	};


	/*
	 * Compute new positions, because of our requierement of the 0 feel velocity if ignore.
	 * The markers that are ignored will not move
	 */
	mColl.getXPos() += (this->m_feelVelX) * thisDt;
	mColl.getYPos() += (this->m_feelVelY) * thisDt;

	return;
}; //End: move markers


PGL_NS_END(egd)


