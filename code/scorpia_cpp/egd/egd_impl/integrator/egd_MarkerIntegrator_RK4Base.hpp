#pragma once
/**
 * \brief	This is the base class for RK4 integration.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

/**
 * \brief	This is a base class that is abel to move the markers with RK4 scheme.
 * \class 	egd_markerIntegratorRK4Base_t
 *
 * This class does not compute the velocity fields but will call a hook functionality.
 * This function only implements the mechanic to do RK4 integration, subclasses only
 * overwrite the compute and setup functions.
 *
 * This function will compute the movement of ALL markers, which allows for vectorization.
 * It will then only use the markers that are not ignored. Note that this scheme assumes
 * that ignored markers are not the majority.
 */
class egd_markerIntegratorRK4Base_t : public egd_MarkerIntegrator_i
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using Base_t = egd_MarkerIntegrator_i;		//This is the base class of *this
	using Base_t::Size_t;
	using Base_t::Index_t;
	using Base_t::Numeric_t;

	using Base_t::MarkerCollection_t;
	using Base_t::GridContainer_t;
	using GridGeometry_t 		= egd_gridGeometry_t;		  //!< This is teh grid geometry.
	using Base_t::TempSolverResult_t;
	using Base_t::MechSolverResult_t;
	using NodePositions_t 		= GridGeometry_t::NodePosition_t; //!< This is the positions of the node.
	using GridProperty_t 		= GridContainer_t::GridProperty_t;

	using MarkerPosition_t		= MarkerCollection_t::MarkerProperty_t;	//!< This is the type for the position of the markers
	using MarkerVelocity_t 		= MarkerCollection_t::MarkerProperty_t;	//!< This is the velocity of the markers.
	using MarkerProperty_t 		= MarkerCollection_t::MarkerProperty_t;	//!< This is the type of a property defined on marker.
	using ExtVelField_t 		= egd_gridProperty_t;			//!< This is the type for storing the extended velocity field.

	using BoundaryCondition_t 	= egd_boundaryConditions_t;		//!< This is a class for encoding a boundary conditin.
	using ApplyBC_t 		= GridContainer_t::ApplyBC_t;		//!< This is the type that is able to apply the boundary conditin.

	using Base_t::IgnoredTypeList_t;
	using IndexVector_t 		= MarkerCollection_t::IndexVector_t;	//!< List of indexes


	/*
	 * ======================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor uses the configuration object to
	 * determine the prescribed velocities. It also checks
	 * if the other boundary conditions are correctly set
	 * to free slip.
	 *
	 * \param  confObj	This is the configuration object.
	 */
	egd_markerIntegratorRK4Base_t(
		const egd_confObj_t& 	confObj);

	/**
	 * \brief	This constructor is an explicit constructor.
	 *
	 * This constructor assumes that the boundaries that are
	 * normal to the velocity direction are firced to a specific
	 * value. Boundaries that are tangenial have a free slip
	 * condition. The corner nodes are managed by the free
	 * splip condition.
	 *
	 * This constructor also accepts an optional fifth parameter.
	 * Which is a pointer to the config object. This argument
	 * my be the nullpointer, which indicates that no config object
	 * is aviable.
	 * This does not depend on that.
	 *
	 * \param  Ny		Number of basic grid points in y direction.
	 * \param  Nx 		Number of basic grid points in x direction.
	 * \param  bcVelX	The boundary condition for the velocity field in x direction.
	 * \param  bcVelY	The boundary condition for the velocity field in y direction.
	 */
	egd_markerIntegratorRK4Base_t(
		const yNodeIdx_t 		Ny,
		const xNodeIdx_t 		Nx,
		const BoundaryCondition_t& 	bcVelX,
		const BoundaryCondition_t& 	bcVelY,
		const egd_confObj_t* const 	confObj = nullptr);


	/**
	 * \brief	Destructor.
	 *
	 * The destructor is public and defaulted.
	 */
	virtual
	~egd_markerIntegratorRK4Base_t();



	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Base_t(
		const egd_markerIntegratorRK4Base_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Base_t&
	operator= (
		const egd_markerIntegratorRK4Base_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Base_t(
		egd_markerIntegratorRK4Base_t&&)
	 noexcept;


	/**
	 * \brief	Move assigment.
	 *
	 * Is defaulted and noexcept.
	 */
	egd_markerIntegratorRK4Base_t&
	operator= (
		egd_markerIntegratorRK4Base_t&&);


	/*
	 * ==========================
	 * Private Constructor
	 */
private:
	/**
	 * \brief	Default constructor
	 *
	 * It is used only for internal use.
	 * It will generate an invalide object.
	 */
	egd_markerIntegratorRK4Base_t();


	/**
	 * \brief	This is an internal constructor.
	 *
	 * It will initialize the two velocity container.
	 * It will set the boundaries to nonsensical values.
	 *
	 * \param  Ny		The number of basic nodal point in y.
	 * \param  Nx		The number of basic nodal point in x.
	 */
	egd_markerIntegratorRK4Base_t(
		const yNodeIdx_t 	Ny,
		const xNodeIdx_t 	Nx);



	/*
	 * ===========================
	 * Interface functions
	 */
public:
	/**
	 * \brief	This function performs the interpolation.
	 *
	 * This function will advect the marker.
	 * It will use the feel velocities that are previously computed.
	 * It will use the passed time to estimate if they are accurate or not.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	moveMarkers(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This fununction can be used to set feel velocities and
	 * 		 store them. This will NOT move the markers.
	 *
	 * This function will compute the feel velocities. The velocties are stored
	 * inside *this and in the marker collection. The time index, current time
	 * and time increment are stored and are used as key to idenetify the
	 * velocties, such that they could be reused upon moving.
	 * If a marker is ignored, its velocity will be set to zero.
	 *
	 * The function will always return ture, indicating that the velocities
	 * are always recomputed upon calling this function.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	bool
	storeFeelVel(
		MarkerCollection_t* const 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 override;


	/**
	 * \brief	This function returns a textual
	 * 		 representation of *this.
	 *
	 * This function is abstract and does not provide an implementation.
	 */
	virtual
	std::string
	print()
	 const
	 override
	 = 0;


	/**
	 * \brief	Thes function will return true, meaing that it computes
	 * 		 the feel velocities.
	 *
	 * The velocity that is stored is the one that was used to move the marker.
	 * In the vase of the RK4 scheme, the feel velocity is the effective velocity
	 * that was computed.
	 *
	 * This can be fully done on the base level, without the help of the
	 * concrete integrators.
	 */
	virtual
	bool
	computeFeelVel()
	 const
	 override;



	/*
	 * Interface functions of the interface
	 */
	using Base_t::isIgnoredType;
	using Base_t::addIgnoredType;
	using Base_t::noIgnoredTypes;


	/*
	 * =====================
	 * Protected Constructors
	 *
	 * All other constructors are protected and defaulted.
	 * They are also marked as noexcept
	 */
protected:


	/*
	 * ======================
	 * RK4 Base Hooks.
	 *
	 * These functions are called by the driver code and allows
	 * to customize the codes.
	 */
protected:
	/**
	 * \brief	This is the setup function of the driver.
	 *
	 * This function is called before any movement happens.
	 * It is provided such that subclasses can set up
	 * internal structures for the calculations.
	 *
	 * This function does not have a default implementation.
	 *
	 * \param  mColl	This is the marker collection that should be updated.
	 * \param  grid 	This is the grid that was optained from mapping the markers to the grid.
	 * \param  mSol 	This is the mechnaical solution that was computed.
	 * \param  tSol 	This is the temperature solution that was computed.
	 * \param  thisDt	This is the timestep that is done next.
	 * \param  tStepIdx 	This is the index of the tiomestep, starting at zero.
	 * \param  currTime	This is the current time.
	 */
	virtual
	void
	hook_setupIntegrator(
		const MarkerCollection_t& 	mColl,
		const GridContainer_t& 		grid,
		const MechSolverResult_t&	mSol,
		const TempSolverResult_t&	tSol,
		const Numeric_t 		thisDt,
		const Size_t 			tStepIdx,
		const Numeric_t 		currTime)
	 = 0;


	/**
	 * \brief	This function computes the velocity field
	 * 		 At the given positions.
	 *
	 * This function basically performs an interpolation of the
	 * extended velocity field.
	 *
	 * This function does not have a default implementation.
	 *
	 * \param  grid		This is the grid object.
	 * \param  xMPos	The x coordinates of the marker.
	 * \param  yMPos 	The y coordinates of the marker.
	 * \param  xMVel 	The x velocity of the marker that was interpolated.
	 * \param  yMVel 	The y velocity of the marker that was interpolated.
	 * \param  cTime	This is the current time, at which we evaluate the velocity field.
	 */
	virtual
	void
	hook_getMarkerVel(
		const GridContainer_t&		grid,
		const MarkerPosition_t& 	xMPos,
		const MarkerPosition_t& 	yMPos,
		      MarkerVelocity_t* const 	xMVel,
		      MarkerVelocity_t* const 	yMVel,
		const Numeric_t 		cTime)
	 const
	 = 0;


	/**
	 * \brief	This function will return true, if *this is set up.
	 *
	 * This function will test if the boundatry conditions are valid.
	 *
	 * It is intended to override this function by base class and
	 * before validating themself, validating their base.
	 *
	 * This file is implemented in the cpp file of this class.
	 */
	virtual
	bool
	isSetUp()
	 const;




	/*
	 * =====================
	 * Internal Query Functions
	 *
	 * These functions allows subclasses to access and query the internal
	 * status of the base class instance.
	 */
protected:
	/**
	 * \brief	This function retrns a reference to the boundary
	 * 		 condition for the x component of velocity.
	 */
	const BoundaryCondition_t&
	getBCVelX()
	 const;


	/**
	 * \brief	This function retrns a reference to the boundary
	 * 		 condition for the y component of velocity.
	 */
	const BoundaryCondition_t&
	getBCVelY()
	 const;



	/*
	 * ========================
	 * Private Members
	 */
public:
	BoundaryCondition_t 	m_bcVelX;		//!< Boundary condition for the velocity x field.
	BoundaryCondition_t 	m_bcVelY;		//!< Boundary condition for the velocity y field.

	//This is the precomputed feel velocity
	MarkerVelocity_t 	m_feelVelX;		//!< Feel Velocity in x direction.
	MarkerVelocity_t 	m_feelVelY;		//!< Feel velocity in y direction.
	Numeric_t 		m_feelTime = NAN;	//!< Time at which the feel velocities where computed.
	Numeric_t   		m_feelDT   = NAN;	//!< Time increment that was used.
	Size_t 			m_feelIdx  = Size_t(-1);//!< Timestep index at which the feel velocity was computed.
}; //End class(egd_markerIntegratorRK4Base_t)

PGL_NS_END(egd)

