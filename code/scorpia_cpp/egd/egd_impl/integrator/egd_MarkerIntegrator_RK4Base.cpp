/**
 * \brief	This file contains some functions of the RK4 base implementation.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>
#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_RK4Base.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)

bool
egd_markerIntegratorRK4Base_t::computeFeelVel()
 const
{
	return true;
};

const egd_markerIntegratorRK4Base_t::BoundaryCondition_t&
egd_markerIntegratorRK4Base_t::getBCVelX()
 const
{
	pgl_assert(m_bcVelX.isValid(),
		   m_bcVelX.getPropIdx().isVelX());
	return m_bcVelX;
}; //End: get bc for velocity in x direction


const egd_markerIntegratorRK4Base_t::BoundaryCondition_t&
egd_markerIntegratorRK4Base_t::getBCVelY()
 const
{
	pgl_assert(m_bcVelY.isValid(),
		   m_bcVelY.getPropIdx().isVelY());
	return m_bcVelY;
}; //End: get bc for velocity in x direction


bool
egd_markerIntegratorRK4Base_t::isSetUp()
 const
{
	//Test if the arguments are valid.
	if(m_bcVelX.isValid()    == false           ||
	   m_bcVelX.isFinal()    == false           ||
	   m_bcVelX.getPropIdx() != PropIdx::VelX()   )
	{
		pgl_assert(false && "The x boundary object is invalid.");
		return false;
	};
	if(m_bcVelY.isValid()    == false           ||
	   m_bcVelY.isFinal()    == false           ||
	   m_bcVelY.getPropIdx() != PropIdx::VelY()   )
	{
		pgl_assert(false && "The y boundary object is invalid.");
		return false;
	};


	//No error detected
	return true;
}; //End: isSetUp

PGL_NS_END(egd)

