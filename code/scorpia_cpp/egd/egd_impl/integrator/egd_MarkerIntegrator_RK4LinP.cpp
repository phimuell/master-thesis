/**
 * \brief	This file contains the code for the constructor and the non hook related functions.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_core/egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include "./egd_MarkerIntegrator_RK4LinP.hpp"
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <memory>
#include <string>


PGL_NS_START(egd)


/*
 * ==================
 * Integrator interface functions
 */
std::string
egd_markerIntegratorRK4LinP_t::print()
 const
{
	return (std::string("LinP RK4 Integrator, with ccWeight = " + std::to_string(this->m_ccWeight) + ".")
			+ " VelX BC: " + this->getBCVelX().print() + ";"
			+ " VelY BC: " + this->getBCVelY().print() + ";"
		);
}; //End: print


/*
 * =======================
 * Constructor
 */
egd_markerIntegratorRK4LinP_t::egd_markerIntegratorRK4LinP_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const BoundaryCondition_t& 	bcVelX,
	const BoundaryCondition_t& 	bcVelY,
	const Numeric_t 		ccWeight,
	const egd_confObj_t* const 	confObj)
 :
  Base_t(Ny, Nx, bcVelX, bcVelY, confObj),			//setup the base
  m_velX(Ny, Nx, mkExt(eGridType::StVx), PropIdx::VelX()),	//allocate the fields, use the extended grid as a default
  m_velY(Ny, Nx, mkExt(eGridType::StVy), PropIdx::VelY()),	// is another one is used, it will get overwrriten, in the settup process.
  m_ccWeight(ccWeight)
{
	/* Handle the grid */
	if(Ny < Index_t(3) || Nx < Index_t(3))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points is wrong.");
	}; //End if: check the size

	/* test if weight is valid */
	if((pgl::isValidFloat(ccWeight) == false) ||
	   (ccWeight < 0.0) || (ccWeight > 1.0) )
	{
		throw PGL_EXCEPT_InvArg("The ccWeight is invalid, it is " + std::to_string(ccWeight));
	};
}; //End: constructor


egd_markerIntegratorRK4LinP_t::egd_markerIntegratorRK4LinP_t(
	const yNodeIdx_t 		Ny,
	const xNodeIdx_t 		Nx,
	const BoundaryCondition_t& 	bcVelX,
	const BoundaryCondition_t& 	bcVelY,
	const egd_confObj_t* const 	confObj)
 :
  egd_markerIntegratorRK4LinP_t(
  		Ny, Nx,
  		bcVelX, bcVelY,
  		1.0 / 3.0,		//weight of cc velocity
  		confObj)
{};


egd_markerIntegratorRK4LinP_t::egd_markerIntegratorRK4LinP_t(
	const egd_confObj_t& 	confObj)
 :
  egd_markerIntegratorRK4LinP_t(
  		yNodeIdx_t(confObj.getNy()),
  		xNodeIdx_t(confObj.getNx()),
  		confObj.getBC(eRandProp::VelX),
  		confObj.getBC(eRandProp::VelY),
  		&confObj)
{};


egd_markerIntegratorRK4LinP_t::~egd_markerIntegratorRK4LinP_t()
 = default;


egd_markerIntegratorRK4LinP_t::egd_markerIntegratorRK4LinP_t(
	const egd_markerIntegratorRK4LinP_t&)
 = default;


egd_markerIntegratorRK4LinP_t&
egd_markerIntegratorRK4LinP_t::operator= (
	const egd_markerIntegratorRK4LinP_t&)
 = default;


egd_markerIntegratorRK4LinP_t::egd_markerIntegratorRK4LinP_t(
	egd_markerIntegratorRK4LinP_t&&)
 noexcept
 = default;


egd_markerIntegratorRK4LinP_t&
egd_markerIntegratorRK4LinP_t::operator= (
	egd_markerIntegratorRK4LinP_t&&)
 = default;


PGL_NS_END(egd)

