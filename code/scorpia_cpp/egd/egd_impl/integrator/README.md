# Info
This folder contains all implementations of the integrator interface that moves the markers.

## Std or Continuum Based
The standard implementation uses RK4 to move the markers.
However the velocity is nod updated as the marker moves.
It is always the computed solution used.

## Jenny-Meyer
This is an integrator that uses the techniques that from Meyer-Jenny.
There velocities from the staggered velocity points are interpolated
to the corner points of the basic nodal cells.
The trick here is, that the four velocities that meet in a corner,
are not necessarily the same.
This integrator performs sub grid time stepping. And uses RK4 to use
the marker within a cell.

## LinP
This is an empirical integrator that has two components.
One component is the velocity that is interpolated from the staggered
grid (VX and VY nodes) to the markers and the second component is
that velocity that is interpolated from the CC points to the markers.
CC velocity has a weight of 1/3 and the staggered grid is weighted by
2/3.
CC velocities are computed the same way as in the standard version.

## Test64
An empirical interpolation routine, that interpolates velocity derivatives
from the corner points to the marker position.
See MATLAB file in the folder.





