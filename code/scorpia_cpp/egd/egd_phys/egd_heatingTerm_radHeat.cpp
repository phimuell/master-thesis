/**
 * \brief	This file implements the computation of the radioactive contribution to
 * 		 the heating term.
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)


void
egd_heatingTerm_t::pric_computeRadioactiveHeating(
	const GridContainer_t& 		grid,
	Heating_t* const 		radHeating_)
{
	pgl_assert(radHeating_ != nullptr);
	Heating_t& radHeating = *radHeating_;	//Create an alias of the destination for better handling.

	if(radHeating.isAllocated() == false)
	{
		throw PGL_EXCEPT_InvArg("The radiooactive heating is not allocated.");
	};
	if(radHeating.hasValidGridType() == false)
	{
		throw PGL_EXCEPT_InvArg("The target grid property does not have a vaolid grid type.");
	};
	if(radHeating.getPropIdx() != PropIdx::RadioEnergy())
	{
		throw PGL_EXCEPT_InvArg("The target grid is not of type radioactive heating.");
	};

	//Set the target property to zero
	radHeating.setConstantAll(0.0);

	//load if it is an extended case or not
	const bool isExtended = radHeating.isExtendedGrid();

	//This is the property we have to look for
	const PropIdx_t radioProp = isExtended ? PropIdx::RadioEnergyCC() : PropIdx::RadioEnergy();

	//Test if the property is pressent if not we are done
	if(grid.hasProperty(radioProp) == false)
	{
		//No radioactive property, so we are done
		//we do not consider it ans an error if no is present.
		return;
	};

	// Get the grid property for the radoioactive heating
	const auto& RAD = grid.getProperty(radioProp);

	//Test if on same grid
	if(radHeating.checkOnSameGrid(RAD) == false)
	{
		throw PGL_EXCEPT_InvArg("The grid property of radioactive heating and the target property are nopt on the same grid."
				" The grid property is defined on " + RAD.getGridType() + ", but we need it on " + radHeating.getGridType());
	};

	/*
	 * We simply load the size of and then we will copy the inner part.
	 */
	const Index_t nRows = radHeating.rows();
	const Index_t nCols = radHeating.cols();

	//Copy the inner part
	radHeating.block(1, 1, nRows - 2, nCols - 2) = RAD.block(1, 1, nRows - 2, nCols - 2);

	return;
}; //End: radioactive heating term.

PGL_NS_END(egd)

