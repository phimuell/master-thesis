#pragma once
#ifndef EGD_IG_EGD_PHYS_BC
#define EGD_IG_EGD_PHYS_BC
/**
 * \brief	This file implements some very important concepts
 * 		 for working with BC.
 *
 * This file provides some functions that allows working with BC,
 * but also allows to access other parts of EGD that deals with BC.
 * One can see this file as the low level implementation of the BC
 * handling code.
 *
 * This file is guarded by an include guard, because it has problems otherwhise.
 * YCM COMPLETED
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)

//FWD of the condition for one side.
class egd_sideCondition_t;

//FWD of the condition for all
class egd_boundaryConditions_t;


/*
 * N O T E
 * The three enums that are here were previously located
 * in the core folder in the confObj header. They were
 * moved here because they are used in many different
 * situations. Note that some functions involving the
 * enums are still located there, but they have a more
 * private touch.
 */


/**
 * \brief	This enum is used to encode location of the boundary condition.
 * \enum 	eRandLoc
 *
 * It is used to encode if the boundary condition is on top, bottom,
 * right or left of the domain.
 *
 * "Rand" is German and stands for boundary, it was shorter so I used it.
 *
 * It also contains the special position PRESS, which is used for pressure.
 * This is a compability value, that stems from old version of this.
 *
 * The orderin is such that they are consecutive ordered, when sort is
 * applied.
 *
 */
enum class eRandLoc
{
	TOP,		//!< The top condition, these is at y == 0; meaning at the ground.
	RIGHT,		//!< Condition at the right, largest x value.
	BOTTOM,		//!< The condition at the bottom or at maximal depth.
	LEFT,		//!< Condition at the left, smalles x value.
	PRESS,		//!< This is a special location value, it is needed only for the pressure.
	INT_LOWSHEAR,	//!< Internal shearing condition, for the lower wedge.
	INT_UPSHEAR,	//!< Internal shearing confition, for the upper wedge.
}; //End enum(eRandLoc)


/**
 * \brief	This is used to encode the kind of condition we want to impose.
 * \enum 	eRandKind
 *
 * This enum encodes the kind of boundary condition that should be used.
 */
enum class eRandKind
{
	Dirichlet,	//!< A value is prescribed.
	freeSlip,	//!< derivative of normal component with respect to tangential space component is zero.
	Insulation,	//!< Only usefull for thermal condition; Temperature derivative in normal direction is zero.
}; //End enum(eRandKind)


/**
 * \brief	This function allows to create a moundary condition.
 *
 * This function allows to create a boundary condition in a very
 * elegant way. As first argument the function takes a property
 * index, which describes which property you want to create.
 * Note that the property has to be a representant.
 *
 * Then the function has a vardiac argument, which means
 * that it accepts any number arguments. You can use the
 * convinient single creation functions to generate
 * a single condition.
 *
 * All conditions are then passed to the constructor of
 * the boundary condition. The condition is then finalized
 * and returned.
 *
 * This fnnction is the prefered way of creating a boundary condition.
 */
template<
	typename... Args
>
egd_boundaryConditions_t
mkBC(
	const PropIdx 		pIdx,
	const Args... 		args);





/**
 * \brief	This function returns a human readable
 * 		 representation of the location enum.
 *
 * Thsi function basically prints it to a string,
 * but not all capital.
 *
 * \param  loc 		The location that should be printed.
 */
extern
std::string
printLoc(
	const eRandLoc 		loc);


/**
 * \brief	This function returns a human readable
 * 		 representation of the boundary kind.
 *
 * \param  kind		The kind that should be printed.
 */
extern
std::string
printKind(
	const eRandKind 	kind);


/*
 * ======================================
 * CONVINIENT SINGLE BOUNDARY CREATION
 *
 * These functions allows a convenient way to
 * create single boundary conditions.
 *
 * The name indicate the kind of the creeated
 * condition, so only the location and the
 * value, if needed, have to be specified.
 */


/**
 * \brief	This function creates a dirichlet condition.
 *
 * This is a short hand for the constructor of the single
 * condtion.
 *
 * \param  loc		The location where the condition should aply.
 * \param  val 		The value that we impose.
 */
extern
egd_sideCondition_t
mkDiri(
	const eRandLoc 		loc,
	const Numeric_t 	val);

/**
 * \brief	This function creates a dirichlet condition.
 *
 * This is a short hand for the constructor of the single
 * condtion. But swapped order.
 *
 * \param  val 		The value that we impose.
 * \param  loc		The location where the condition should aply.
 */
extern
egd_sideCondition_t
mkDiri(
	const Numeric_t 	val,
	const eRandLoc 		loc);


/**
 * \brief	This function generates a pressure condition.
 *
 * Thsi is a Dirichlet condition with location set to PRESS.
 * So only the value has to be specified.
 *
 * \param  val		The value to prescribe.
 */
extern
egd_sideCondition_t
mkPress(
	const Numeric_t 	val);


/**
 * \brief	This function creates an insulation condition.
 *
 * For this only the location is needed.
 *
 * \param  loc		The location where this should apply.
 */
extern
egd_sideCondition_t
mkInsu(
	const eRandLoc 		loc);


/**
 * \brief	This function allows to create a free slip condition.
 *
 * Note that currently the value must be NAN.
 *
 * \param  loc		The location.
 * \param  val		The value, must be nan.
 */
extern
egd_sideCondition_t
mkFS(
	const eRandLoc 		loc,
	const Numeric_t 	val = NAN);


/**
 * \brief	This condition makes an internal boundary condition.
 *
 * Note that you still has to pass the enum to indicate which internal
 * boundary you want to create. Also note that an internal boundary
 * is currently only a dirichlet condition. so the default value
 * of the third argument should not be changed.
 *
 * \param  loc		Location, denote which internal boundary you mean.
 * \param  val 		Value of the internal condition.
 * \param  kind		Kind of the internal boundary condition, currently
 * 			 must be Dirichlet.
 */
extern
egd_sideCondition_t
mkIntern(
	const eRandLoc 		loc,
	const Numeric_t 	val,
	const eRandKind 	kind = eRandKind::Dirichlet);



/**
 * \brief	This function allows to create a free slip condition.
 *
 * Note that the value must be set to NAN.
 *
 * \param  val		The value, must be nan.
 * \param  loc		The location.
 */
extern
egd_sideCondition_t
mkFS(
	const Numeric_t 	val,
	const eRandLoc 		loc);



/*
 * ====================================
 * Internal functions
 *
 * These functions have an internal touch
 */


/**
 * \brief	This function returns true if loc is a valid location.
 *
 * \param  loc		The location enum to test.
 */
inline
constexpr
bool
isValidRandLoc(
	const eRandLoc 		loc)
{
	switch(loc)
	{
	  case eRandLoc::TOP:
	  case eRandLoc::RIGHT:
	  case eRandLoc::BOTTOM:
	  case eRandLoc::LEFT:
	  case eRandLoc::PRESS:
	  case eRandLoc::INT_LOWSHEAR:
	  case eRandLoc::INT_UPSHEAR:
	  	return true;
	  break;

	  default:
	  break;
	}; //End switch(loc):

	return false;
}; //End: is valid rand location


/**
 * \brief	This function returns true if kind is a valid boundary kind.
 *
 * \param  kinbd	The kind of condition that is applied.
 */
inline
constexpr
bool
isValidRandKind(
	const eRandKind 	kind)
{
	switch(kind)
	{
	  case eRandKind::Dirichlet:
	  case eRandKind::Insulation:
	  case eRandKind::freeSlip:
	  	return true;
	  break;

	  default:
	  break;
	}; //End switch(kind):

	return false;
}; //End: is valid randkind


/**
 * \brief	This function returns true if loc is an internal boundary condition.
 *
 * Note this function does not distinguish between an invalid and a non internal
 * boundary condition. This means that if the location is itself is invalid this
 * function returns false.
 *
 * \param  loc		The location enum to test.
 */
inline
constexpr
bool
isInternalRandLoc(
	const eRandLoc 		loc)
{
	switch(loc)
	{
	  //this are internal boundary conditions.
	  case eRandLoc::INT_LOWSHEAR:
	  case eRandLoc::INT_UPSHEAR:
	  	return true;
	  break;

	  //these are valid non internal boundaries
	  case eRandLoc::TOP:
	  case eRandLoc::RIGHT:
	  case eRandLoc::BOTTOM:
	  case eRandLoc::LEFT:
	  case eRandLoc::PRESS:
	  	return false;

	  //we will exit the switch here, only if loc is invalid.
	  default:
	  break;
	}; //End switch(loc):

	return false;
}; //End: is valid rand location


inline
std::string
operator+ (
	const std::string& 	s,
	const eRandLoc 		loc)
{
	return s + printLoc(loc);
};

inline
std::string
operator+ (
	const eRandLoc 		loc,
	const std::string& 	s)
{
	return printLoc(loc) + s;
};

inline
std::string
operator+ (
	const std::string& 	s,
	const eRandKind 	kind)
{
	return s + printKind(kind);
};

inline
std::string
operator+ (
	const eRandKind 	kind,
	const std::string& 	s)
{
	return printKind(kind) + s;
};

PGL_NS_END(egd)
#endif	//End include guard 1


/*
 * Here we include the two class header for the boundary
 * objects, such that they are aviable.
 */
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>




#ifndef EGD_IG_EGD_PHYS_BC_2	//Begin include gurard 2
#define EGD_IG_EGD_PHYS_BC_2

PGL_NS_START(egd)

template<
	typename... Args
>
egd_boundaryConditions_t
mkBC(
	const PropIdx 		pIdx,
	const Args... 		args)
{
	return egd_boundaryConditions_t(pIdx, {args...}, true);
};


PGL_NS_END(egd)

#endif //ENd include guard 2

