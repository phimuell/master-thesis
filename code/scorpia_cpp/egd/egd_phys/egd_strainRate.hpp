#pragma once
/**
 * \brief	This function implements the functionality to compute and store the strain rate.
 *
 * For that the solution of the mechanical solver is needed.
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridType.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)

/**
 * \brief	This macro will instruct the strain rate to ensure
 * 		 that the deviatoric strain rate is computed.
 * \define 	EGD_ENSURE_DEVIATORIC_STRAIN_RATE
 *
 * If this macro is set to one (1), just defining it is not enough,
 * the class egd_deviatoricStrainRate_t will not assume that the
 * supplied velocity field is incompressible and thus the trace
 * does not need to be subttracted.
 * This macro will instruct the code base to do this.
 *
 * This macro is set to zero by default, but if EGD_INCOMPRESSIBLE_MODE
 * is activated it will be set to one.
 */
//Only doing it if needed
//TODO: add a parsed by doxygen switch
#ifndef EGD_ENSURE_DEVIATORIC_STRAIN_RATE
#	if defined(EGD_INCOMPRESSIBLE_MODE) && (EGD_INCOMPRESSIBLE_MODE == 1)
		//Incompressibility is in place
#		define EGD_ENSURE_DEVIATORIC_STRAIN_RATE 1
#	else
		// If not set the default set it to zero
#		define EGD_ENSURE_DEVIATORIC_STRAIN_RATE 0
#	endif
#endif


/**
 * \brief	Represents the strain rate of a grid or a configuaration.
 * \class 	egd_deviatoricStrainRate_t
 *
 * This class manages the deviatoric strain rate. It is computed out of the solution
 * of the mechanical solver. It contains three matrices, one for each starin rate
 * {xx, yy, xy == yx}. Because we have incompressibility, the deviatoric starin rate
 * and the normal starin rate are identical with each other, but *this stores explicitly
 * the deviatoric strain rate. Also the two normal componets are trivially deducted
 * form each other, however we will not take advantages of this.
 *
 * First the deviatoric strain rate of xy is defined at the basic nodal points.
 * But not all nodes are valid. Only the inner nodes have a maning.
 * Thus only indexes in range 0 < i, j < ((rows - 1, cols - 1)) are prpopper
 * defined, the other values are undefined.
 *
 * The {xx} and {yy} starin rates are defined in the center points.
 * They following the stagerred grid indexing, meaning that the first valid
 * index is (1, 1). Thus the furst row and first column are undefined.
 *
 * The nice effect of this choice is that all three matrices have the same size
 * and their valid index starts at (1, 1).
 *
 * The class guarantees that all entries of that matrix that refere to
 * positions that have no meaning ar set to zero.
 *
 * Note that this class contains different grid sizes.
 * The normal strain rate, {XX, YY}, are defined on the CC grid.
 * These grids can be extended or not. Howver the shear component,
 * {XY} is defined on the basic grid points, so it is always
 * regular and never extended.
 *
 * \note	This class currently computes the normal strain rate.
 * 		 It only computes the deviatoric strain rate, because
 * 		 the problem is incompressible, if the problem is extended
 * 		 to be compressible, this class must be addapted.
 */
class egd_deviatoricStrainRate_t
{
	/*
	 * =========================
	 * Typedef
	 */
public:
	using uSize_t 	= ::pgl::Size_t;		//!< Unsigned size type
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using PropIdx_t 	= PropIdx;			//!< This is the idex type for the properties
	using SolverResult_t 	= egd_mechSolverResult_t;	//!< Result of the mechanical solver.
	using StrainRate_t 	= egd_gridProperty_t;		//!< Type for represnting the strain. Note this type is only used for exposition.
	using GridGeometry_t 	= egd_gridGeometry_t;		//!< This is the grid geometry.


	/*
	 * ==========================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor will set up *this.
	 * It will use the grid and the result of the
	 * mechanical solver to set up itself.
	 *
	 * \param  grid		The grid that is used.
	 * \param  solRes 	The reuslt of the solver.
	 *
	 * \throw	If an inconsitency is detected.
	 */
	egd_deviatoricStrainRate_t(
		const GridGeometry_t& 		grid,
		const SolverResult_t& 		solRes);


	/**
	 * \brief	Copy constructor.
	 *
	 * Defaulted.
	 */
	egd_deviatoricStrainRate_t(
		const egd_deviatoricStrainRate_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Defaulted.
	 */
	egd_deviatoricStrainRate_t&
	operator= (
		const egd_deviatoricStrainRate_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Defaulted.
	 */
	egd_deviatoricStrainRate_t(
		egd_deviatoricStrainRate_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Defaulted.
	 */
	egd_deviatoricStrainRate_t&
	operator= (
		egd_deviatoricStrainRate_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Defaulted.
	 */
	~egd_deviatoricStrainRate_t()
	 = default;

private:
	/**
	 * \brief	Default constructor.
	 *
	 * Sets the properties.
	 */
	egd_deviatoricStrainRate_t();


	/*
	 * ====================
	 * Quering and status
	 */
public:
	/**
	 * \brief	This function returns the number of
	 * 		 basic nodal points in x direction.
	 */
	Size_t
	xNodalPoints()
	 const
	 noexcept
	{
		return m_dEpsilonXX.Nx();
	};

	/**
	 * \brief	This function returns the number of
	 * 		 basic nodal points in y direction.
	 */
	Size_t
	yNodalPoints()
	 const
	 noexcept
	{
		return m_dEpsilonXX.Ny();
	};


	/**
	 * \brief	Return true if *this is valid.
	 *
	 * This is an expensive test that will check if the
	 * sizes are correct and if the values are valid.
	 *
	 * Note This function will be removed in teh future,
	 * use the isFinite() function for it.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		return this->isFinite();
	};


	/**
	 * \brief	This function tests if the sizes of *this are correct.
	 *
	 * This will only check if the dimensions are plausible.
	 * It will not perform a check is the values are valid.
	 */
	bool
	checkSize()
	 const
	 noexcept;


	/**
	 * \brief	Return true if *this is valid.
	 *
	 * This function calls checkSize() and isFinite()
	 * on each component. Use it instead of the isValid()
	 * function.
	 */
	bool
	isFinite()
	 const
	 noexcept;


	/**
	 * \brief	Returns true if all components of *this are on the extended grid.
	 *
	 * Note that this only queries the normal component, {XX, YY}.
	 * The shear component is always a regular basic grid.
	 */
	bool
	isExtendedGrid()
	 const
	{
		return (m_dEpsilonXX.isExtendedGrid() && m_dEpsilonYY.isExtendedGrid())
			? true
			: false;
	};


	/**
	 * \brief	Returns true if all components of *this are on regular grid size.
	 *
	 * Note that this only queries the normal component, {XX, YY}.
	 * The shear component is always a regular basic grid.
	 */
	bool
	isRegularGrid()
	 const
	{
		return (m_dEpsilonXX.isRegularGrid() && m_dEpsilonYY.isRegularGrid())
			? true
			: false;
	};


	/**
	 * \brief	This function returns if *this is allocated.
	 */
	bool
	isAllocated()
	 const;




	/*
	 * ====================
	 * Access functions.
	 */
public:
	/**
	 * \brief	Returns the xx component of the deviatoric strain rate
	 * 		 tensor.
	 *
	 * The value is defined in the cell center of the basic nodal cells.
	 * Thus the numbering is such that it starts at the index (1, 1).
	 * Meaning that the first row and first column are invalid.
	 */
	const StrainRate_t&
	xx()
	 const
	 noexcept
	{
		return m_dEpsilonXX;
	};


	/**
	 * \brief	Returns the yy component of the deviatoric
	 * 		 strain rate tensor.
	 *
	 * The value is defined in the cell center of the basic nodal cells.
	 * Thus the numbering is such that it starts at the index (1, 1).
	 * Meaing that the first row and first column are invalid.
	 */
	const StrainRate_t&
	yy()
	 const
	 noexcept
	{
		return m_dEpsilonYY;
	};


	/**
	 * \brief	Returns the xy component of the deviatoric
	 * 		 strain rate tensor.
	 *
	 * The values is only defined on the interiour basic nodel grid points.
	 * Thus the fris and last row and column are invalid and caries no meaning.
	 * Thus the first value is located at (1, 1).
	 */
	const StrainRate_t&
	xy()
	 const
	 noexcept
	{
		return m_dEpsilonXY;
	};


	/**
	 * \brief	Returns the yx component of the deviatoric
	 * 		 strain rate tensor.
	 *
	 * This is an alias of xy().
	 */
	const StrainRate_t&
	yx()
	 const
	 noexcept
	{
		return m_dEpsilonXY;
	};


#if 0
	/**
	 * \brief	Returns the xx component of the deviatoric strain rate
	 * 		 tensor.
	 *
	 * The value is defined in the cell center of the basic nodal cells.
	 * Thus the numbering is such that it starts at the index (1, 1).
	 * Meaning that the first row and first column are invalid.
	 */
	StrainRate_t&
	xx()
	 noexcept
	{
		return m_dEpsilonXX;
	};


	/**
	 * \brief	Returns the yy component of the deviatoric
	 * 		 strain rate tensor.
	 *
	 * The value is defined in the cell center of the basic nodal cells.
	 * Thus the numbering is such that it starts at the index (1, 1).
	 * Meaing that the first row and first column are invalid.
	 */
	StrainRate_t&
	yy()
	 noexcept
	{
		return m_dEpsilonYY;
	};


	/**
	 * \brief	Returns the xy component of the deviatoric
	 * 		 strain rate tensor.
	 *
	 * The values is only defined on the interiour basic nodel grid points.
	 * Thus the fris and last row and column are invalid and caries no meaning.
	 * Thus the first value is located at (1, 1).
	 */
	StrainRate_t&
	xy()
	 noexcept
	{
		return m_dEpsilonXY;
	};


	/**
	 * \brief	Returns the yx component of the deviatoric
	 * 		 strain rate tensor.
	 *
	 * This is an alias of xy().
	 */
	StrainRate_t&
	yx()
	 noexcept
	{
		return m_dEpsilonXY;
	};
#endif

	/*
	 * ======================
	 * Managing Functions
	 */
public:
	/**
	 * \brief	This function allocates *this.
	 *
	 * It will allocate the underling properties and set them
	 * to zero, no calculation is done. It is an error to
	 * call this function twioce.
	 *
	 * \param  yBasicN	Number of basic nodal points in y direction.
	 * \param  xBasicN	Number of basic nodal points in x direction.
	 * \param  isExt 	Is the setting an extended one.
	 */
	void
	allocateGrid(
		const yNodeIdx_t 	yBasicN,
		const xNodeIdx_t 	xBasicN,
		const bool 		isExt);


	/**
	 * \brief	This function sets all the underlying values to zero.
	 *
	 * Note that this function must be called on an already allocated grid.
	 */
	void
	setZero();


	/*
	 * ===================
	 * Internal function
	 */
private:
	/**
	 * \brief	This is the building function.
	 *
	 * It will use the grid and the result of the
	 * mechanical solver to set up itself.
	 *
	 * \param  grid		The grid that is used.
	 * \param  solRes 	The reuslt of the solver.
	 *
	 * \throw	If an inconsitency is detected.
	 */
	void
	priv_compuetStrainRate(
		const GridGeometry_t&		grid,
		const SolverResult_t& 		solRes);



	/*
	 * ====================
	 * Private Members
	 */
public:
	StrainRate_t 		m_dEpsilonXX;	//!< This is the deviatoric strain rate in xx direction.
	StrainRate_t 		m_dEpsilonYY;	//!< This is the deviatoric strain rate in yy direction.
	StrainRate_t 		m_dEpsilonXY;	//!< This is the deviatoric strain rate in xy direction.
}; //End class(egd_deviatoricStrainRate_t)


PGL_NS_END(egd)



