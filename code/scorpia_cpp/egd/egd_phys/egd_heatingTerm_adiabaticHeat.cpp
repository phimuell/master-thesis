/**
 * \brief	This file contains the functions that computes the adiabatic heat.
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)


void
egd_heatingTerm_t::priv_computeAdiabaticHeat(
	const GridContainer_t& 		grid,
	const SolverResult_t& 		solRes,
	const GravForce_t& 		grav,
	Heating_t* const 		adiHeat_)
{
	pgl_assert(adiHeat_ != nullptr);
	Heating_t& adiHeat = *adiHeat_;	//Make an alias of the destination

	//Load the geometry
	const GridGeometry_t& gGeo = grid.getGeometry();

	if(gGeo.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("Adiabatic heat is only supported for constant grids.");
	};
	if(solRes.getVelY().onStVyPoints() == false)
	{
		throw PGL_EXCEPT_InvArg("The y velcoity is not defined on the y velocity points.");
	};
	if(adiHeat.isAllocated() == false)
	{
		throw PGL_EXCEPT_InvArg("The adiabatic heat is not allocated.");
	};

	if(grav.isFinite() == false)
	{
		throw PGL_EXCEPT_InvArg("The gravity term is invalid it was: " + grav);
	};
	if(grav.isZeroX() == false)
	{
		throw PGL_EXCEPT_InvArg("The x comonent of gravity is not zero, gravity was: " + grav);
	};

	//Test if the thermal expansion is pressent
	//In that case we do not compute the adiabatic heating
	if(grid.hasProperty(PropIdx::ThermExpansAlpha()) == false)
	{
		//Set the adiabatic heat to zero
		adiHeat.setConstantAll(0.0);

		return;
	}; //End if: no thermal expansion


	//Load the sizes
	const xNodeIdx_t Nx(gGeo.xNPoints());
	const yNodeIdx_t Ny(gGeo.yNPoints());
		pgl_assert(Nx > 2, Ny > 2);	//We need that for having internal points

	//This is the real size of the adiabatic heat grid
	const Index_t nRows = adiHeat.rows();
	const Index_t nCols = adiHeat.cols();

	//Load needed variables
	const auto& VY    = solRes.getVelY();
	const auto& ALPHA = grid.cgetProperty(PropIdx::ThermExpansAlpha() );	//This property is always at the gright igrid
	const auto& TEMP  = grid.cgetProperty(PropIdx::Temperature()      );
		pgl_assert(ALPHA.Ny() == Ny, ALPHA.Nx() == Nx);
		pgl_assert( TEMP.Ny() == Ny,  TEMP.Nx() == Nx);

	const Numeric_t gravY = grav.y();	//load the gravity
		pgl_assert(::pgl::isValidFloat(gravY));


	if(adiHeat.hasValidGridType() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed target grid property has no valid grid type.");
	};
	if(adiHeat.getPropIdx() != PropIdx::AdiabaticHeat())
	{
		throw PGL_EXCEPT_InvArg("The passed target grid property is not defined as a adiabatic heat.");
	};

	//Set the value to zero
	adiHeat.setConstantAll(0.0);

	/*
	 * Calcualte the adiabatic heat
	 */
	if(adiHeat.onBasicNodalPoints() == true)
	{
		//Load the density
		const auto& RHO   = grid.cgetProperty(PropIdx::Density() );
			pgl_assert(  RHO.Ny() == Ny,   RHO.Nx() == Nx);

		/*
		 * Calculate the adiabatic heating term in the basic nodal points.
		 */
		if(ALPHA.onBasicNodalPoints() == false)
		{
			throw PGL_EXCEPT_InvArg("ALPHA is not defined on the basic nodal points.");
		};
		if(RHO.onBasicNodalPoints() == false)
		{
			throw PGL_EXCEPT_InvArg("RHO is not in the basic nodal poinst.");
		};
		if(TEMP.onBasicNodalPoints() == false)
		{
			throw PGL_EXCEPT_InvArg("Temperature is not at the basic nodal points.");
		};
		if(gGeo.isConstantGrid() == false)
		{
			throw PGL_EXCEPT_InvArg("The implementation only works with constant grids.");
		};

		/*
		 * We will go through all the inner nodes and calaulates the
		 * values at the gird points.
		 *
		 * The formula to compute the Adiabatic heat is given by:
		 * 	H_a(i, j) := T^{(0)}(i, j) * \alpha * D_{t}[P]
		 * Where \alpha is the thermal expansion coefficient and D_{t}[P]
		 * is the material derivative of the pressure. Under the assumtion of
		 * incompressibility and that the pressure is domainaited by a hydro
		 * static effects it can be appsoximated as:
		 *     	H_a(i, j) := T^(i, j) * \alpha v_y * \rho * g_y
		 * See chapto 11.
		 *
		 *    0--------------------------------------------> I/X
		 *    |                     i
		 *    |      o--------------o--------------o
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      |              | H_a          |
		 *    |    j o------x-------o-------x------o
		 *    |      |     vy_1     |      vy_2    |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      o--------------o--------------o
		 *    V
		 *  J/Y
		 *
		 * We see that all quanties are given on the correct grid, with the
		 * exception of the velocity. In order to get them we have to average.
		 * This is done by by averaging vy_1 and vy_2.
		 * vy_1 is associated to node (i, j), velocity vy_2 is associated to
		 * (i, j+1).
		 *
		 */
		for(Index_t j = 1; j != (nCols - 1); ++j)
		{
			for(Index_t i = 1; i != (nRows - 1); ++i)
			{
				const Numeric_t Vy_1     = VY   (i, j    );	//Load variables
				const Numeric_t Vy_2     = VY   (i, j + 1);
				const Numeric_t rho_ij   = RHO  (i, j    );
				const Numeric_t temp_ij  = TEMP (i, j    );
				const Numeric_t alpha_ij = ALPHA(i, j    );

				const Numeric_t Vy_ij    = (Vy_1 + Vy_2) * 0.5;	//Average

				//Compute the value
				const Numeric_t adiHeat_ij = (temp_ij * alpha_ij) * ((Vy_ij * rho_ij) * gravY);

				//Save the value
				adiHeat(i, j) = adiHeat_ij;
			}; //End for(i):
		}; //End for(j):
	//End if: basic nodal points
	}
	else if(adiHeat.onCellCentrePoints() == true && adiHeat.isExtendedGrid() == true)
	{
		//Load the density
		const auto& RHO   = grid.cgetProperty(PropIdx::DensityCC() );
			pgl_assert(  RHO.Ny() == Ny,   RHO.Nx() == Nx);

		/*
		 * Calculate the adiabatic heating term in the basic nodal points.
		 */
		if(ALPHA.onCellCentrePoints() == false ||
		   ALPHA.isExtendedGrid()     == false   )
		{
			throw PGL_EXCEPT_InvArg("ALPHA is not defined on the CC grid, but on " + ALPHA.getGridType() );
		};
		if(RHO.onCellCentrePoints() == false ||
		   RHO.isExtendedGrid()     == false   )
		{
			throw PGL_EXCEPT_InvArg("RHO is not defined on the CC grid, but on " + RHO.getGridType() );
		};
		if(TEMP.onCellCentrePoints() == false ||
		   TEMP.isExtendedGrid()     == false   )
		{
			throw PGL_EXCEPT_InvArg("Temperature is not defined on the CC grid, but on " + TEMP.getGridType() );
		};

		/*
		 * The computation is basically the same as it is for the other case,
		 * but the average is done a bit differenly.
		 * Now we average in the horizontal, also in the fris component.
		 * Then we must add -1 to it, becase of the location of the indexing.
		 *
		 * The formula to compute the Adiabatic heat is given by:
		 * 	H_a(i, j) := T^{(0)}(i, j) * \alpha * D_{t}[P]
		 * Where \alpha is the thermal expansion coefficient and D_{t}[P]
		 * is the material derivative of the pressure. Under the assumtion of
		 * incompressibility and that the pressure is domainaited by a hydro
		 * static effects it can be appsoximated as:
		 *     	H_a(i, j) := T^(i, j) * \alpha v_y * \rho * g_y
		 * See chapto 11.
		 *
		 *    0--------------------------------------------> I/X
		 *    |            vy_1     i
		 *    |      o------x-------o--------------o
		 *    |      |              |              |
		 *    |      |     H_a      |              |
		 *    |      |      x       |              |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |    j o------x-------o--------------o
		 *    |      |     vy_2     |              |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      |              |              |
		 *    |      o--------------o--------------o
		 *    V
		 *  J/Y
		 *
		 *  All quantities are aviable on the CC grid, so we can simply
		 *  load them, with the exception of the velocity. We see that
		 *  vy_2 is associated to the node (i, j), but the velocity
		 *  at node vy_1 is associated to the index (i-1, j).
		 *  So it must be averaged to get the value.
		 */
		for(Index_t j = 1; j != (nCols - 1); ++j)
		{
			for(Index_t i = 1; i != (nRows - 1); ++i)
			{
				const Numeric_t Vy_1     = VY   (i - 1, j);	//Load variables
				const Numeric_t Vy_2     = VY   (i    , j);
				const Numeric_t rho_ij   = RHO  (i    , j);
				const Numeric_t temp_ij  = TEMP (i    , j);
				const Numeric_t alpha_ij = ALPHA(i    , j);

				const Numeric_t Vy_ij    = (Vy_1 + Vy_2) * 0.5;	//Compute average

				//Compute the value
				const Numeric_t adiHeat_ij = (temp_ij * alpha_ij) * ((Vy_ij * rho_ij) * gravY);

				//Save the value
				adiHeat(i, j) = adiHeat_ij;
			}; //End for(i):
		}; //End for(j):

	//End if: cell centre points
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The requested grid is not implemented.");
	};

	return;
}; //End: calcualte adiabatic heat


PGL_NS_END(egd)



