#pragma once
/**
 * \brief	This file implements a class that encodes the condition at one side.
 *
 * This is a building block of the boundary condtions.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <string>



PGL_NS_START(egd)

/**
 * \brief	This class encodes one boundary condition.
 * \class 	egd_sideCondition_t
 *
 * This condition does not store a full boundary condition, but
 * only the conditoion at one location, thus the name.
 * *this stores the location where it is in place, what kind
 * of boundary it is and if needed, the value that is prescribed.
 *
 * It also contains an indication if the left and right corner
 * point of the side is included or not.
 * However this has more a non binding caracter currently.
 *
 *
 * Note that the class supports an ordering scheme. However there
 * only the location/side is decisifnes for the comaprisson.
 */
class egd_sideCondition_t
{
	/*
	 * ===================
	 * Typedef
	 */
public:

	/*
	 * =====================
	 * Constructor
	 */
public:
	/**
	 * \brief	This is the full building constructor.
	 *
	 * This constructor constructs a one sided condition.
	 *
	 * \param  side		The location/side where *this is active.
	 * \param  kind 	The kind of the condition that is applied.
	 * \param  val		The prescribed values there, must be NAN in non Dirichlet cases.
	 */
	egd_sideCondition_t(
		const eRandLoc 		side,
		const eRandKind 	kind,
		const Numeric_t 	val)
	 :
	  m_side(side),
	  m_kind(kind),
	  m_val(val)
	{
		if(this->isValid() == false)	//consistency is enforced by this function.
		{
			throw PGL_EXCEPT_InvArg("An invalid object was created.");
		};
	}; //End: full buildind constzructor.


	/**
	 * \brief	This is a convenience constructor.
	 *
	 * It is meant for non dirichlet condition. It is equivalent in
	 * passing NAN as val to the full building constructor.
	 *
	 * \param  side		The side where the condition is applied to.
	 * \param  kind		The kind that is used.
	 */
	egd_sideCondition_t(
		const eRandLoc 		side,
		const eRandKind 	kind)
	 :
	  egd_sideCondition_t(side, kind, NAN)
	{}; //End: partially building constzructor


	/**
	 * \brief	Move constructor.
	 *
	 * Defaulted.
	 */
	egd_sideCondition_t(
		egd_sideCondition_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Defaulted.
	 */
	egd_sideCondition_t&
	operator= (
		egd_sideCondition_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_sideCondition_t(
		const egd_sideCondition_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_sideCondition_t&
	operator= (
		const egd_sideCondition_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_sideCondition_t()
	 noexcept
	 = default;


	/*
	 * =========================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	Default constructor.
	 *
	 * Is private and defaulted. Is needed for
	 * internal operations.
	 */
	egd_sideCondition_t()
	 noexcept
	 = default;



	/*
	 * ====================
	 * Query functions
	 */
public:
	/**
	 * \brief	This function returns the prescribed value we impose.
	 *
	 * Note that this function throws if *this does not have a value.
	 */
	Numeric_t
	getValue()
	 const
	{
		pgl_assert(this->isValid());

		if(this->hasValue() == false)	//Test if we have one
		{
			throw PGL_EXCEPT_LOGIC("Can not request the prescribed value of a side that does not have one.");
		};

		return m_val;
	}; //End: get value


	/**
	 * \brief	This function returns the location of *this.
	 */
	eRandLoc
	getSide()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return m_side;
	};


	/**
	 * \brief	This is an alias of the getSide() function.
	 */
	eRandLoc
	getLoc()
	 const
	 noexcept
	{
		return this->getSide();
	};


	/**
	 * \brief	This function returns the kind of *this.
	 */
	eRandKind
	getKind()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return m_kind;
	};


	/**
	 * \brief	This function prints out *this.
	 *
	 * This function will print out a string of the
	 * form:
	 * 	LOC -> COND_TYPE
	 */
	std::string
	print()
	 const;




	/*
	 * =====================
	 * Status functions
	 */
public:
	/**
	 * \brief	This function returns true if *this is valid.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		if(isValidRandLoc(m_side) == false)
		{
			pgl_assert(false && "Invalid location.");
			return false;
		};
		if(isValidRandKind(m_kind) == false)
		{
			pgl_assert(false && "Invalid boundary kind.");
			return false;
		};
		if(m_side == eRandLoc::PRESS)
		{
			if(m_kind != eRandKind::Dirichlet)
			{
				pgl_assert(false && "*this is pressure, but no dirichlet condition.");
				return false;
			};
		};
		if(isInternalRandLoc(m_side) == true)	//only dirichelt allowed in this cenario
		{
			if(m_kind != eRandKind::Dirichlet)
			{
				pgl_assert(false && "*this is an internal boundary condition, and thus only Dirichlet types are allowed.");
				return false;
			};
		};
		if(m_kind == eRandKind::Dirichlet)
		{
			//Inside a dirichlet condition we must have a valid value.
			if(pgl::isValidFloat(m_val) == false)
			{
				pgl_assert(false && "No value given in a direichlet condition.");
				return false;
			};
		}
		else
		{
			//All not dirichlet conditions, to not need to have a value
			if(pgl::isValidFloat(m_val) == true)
			{
				pgl_assert(false && "A non dirichlet condition has a valid value.");
				return false;
			};
		};


		//We did not found an error
		return true;
	}; //End: isValid


	/**
	 * \brief	This function returns true if *this has a
	 * 		 prescribed value.
	 */
	bool
	hasValue()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());

		return 	::pgl::isValidFloat(m_val)
			? true
			: false;
	};


	/**
	 * \brief	This function returns true if *this is an
	 * 		 internal boundary.
	 */
	bool
	isInternalBC()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return isInternalRandLoc(this->m_side);
	};


	/*
	 * ======================
	 * Make fast queries
	 */


#define MAKEIS(name, what) 			\
	bool 					\
	is ## name () 				\
	 const 					\
	 noexcept 				\
	{					\
		pgl_assert(this->isValid()); 	\
		return (this->T == what)	\
			? true			\
			: false;		\
	}

	//Make location queries
#define T m_side
	MAKEIS(Top     , eRandLoc::TOP   );
	MAKEIS(Left    , eRandLoc::LEFT  );
	MAKEIS(Right   , eRandLoc::RIGHT );
	MAKEIS(Bottom  , eRandLoc::BOTTOM);
	MAKEIS(Pressure, eRandLoc::PRESS );

	//Internal boundaries
	MAKEIS(LowerShearing, eRandLoc::INT_LOWSHEAR );
	MAKEIS(UpperShearing, eRandLoc::INT_UPSHEAR  );
#undef T



	//Make kind
#define T m_kind
	MAKEIS(FreeSlip  , eRandKind::freeSlip  );
	MAKEIS(Dirichlet , eRandKind::Dirichlet );
	MAKEIS(Insulation, eRandKind::Insulation);
#undef T
#undef MAKEIS



	/*
	 * =======================
	 * Operators
	 */
public:
	/**
	 * \brief	Returns true if *this and rhs are the same.
	 *
	 * Note that only the side is used.
	 *
	 * \param  rhs		The right hand side.
	 */
	bool
	operator== (
		const egd_sideCondition_t& 	rhs)
	 const
	 noexcept
	{
		pgl_assert(this->isValid(), rhs.isValid());
		return (this->getSide() == rhs.getSide());
	};


	/**
	 * \brief	This function returns true if *this and rhs
	 * 		 are unequal.
	 *
	 * \param  rhs		The right hand side.
	 */
	bool
	operator!= (
		const egd_sideCondition_t& 	rhs)
	 const
	 noexcept
	{
		pgl_assert(this->isValid(), rhs.isValid());
		return !(this->operator==(rhs));
	};


	/**
	 * \brief	This function tests if *this is less than rhs.
	 *
	 * For this test a comaprisson on the location is done.
	 *
	 * \param  rhs		The right hand side.
	 */
	bool
	operator< (
		const egd_sideCondition_t& 	rhs)
	 const
	 noexcept
	{
		pgl_assert(this->isValid(), rhs.isValid());
		return (this->getSide() < rhs.getSide());
	};


	/**
	 * \brief	Returns true if *this is the same as the locaton loc.
	 *
	 * \param  loc		The location enum that is compared
	 */
	bool
	operator== (
		const eRandLoc 	loc)
	 const
	 noexcept
	{
		pgl_assert(this->isValid(), isValidRandLoc(loc));
		return (this->getSide() == loc);
	};


	/**
	 * \brief	Returns true if *this is not the same as loc.
	 *
	 * \param  loc		The location enum that is compared
	 */
	bool
	operator!= (
		const eRandLoc 	loc)
	 const
	 noexcept
	{
		pgl_assert(this->isValid(), isValidRandLoc(loc));
		return !(this->operator==(loc));
	};


	/**
	 * \brief	Returns true if *this is the same as kind.
	 *
	 * \param  kind		The kind object we want.
	 */
	bool
	operator== (
		const eRandKind 	kind)
	 const
	 noexcept
	{
		pgl_assert(this->isValid(), isValidRandKind(kind));
		return (this->getKind() == kind);
	};

	/**
	 * \brief	Returns true if *this is not the same as kind.
	 *
	 * \param  kind		The kind object we want.
	 */
	bool
	operator!= (
		const eRandKind 	kind)
	 const
	 noexcept
	{
		pgl_assert(this->isValid(), isValidRandKind(kind));
		return !(this->operator==(kind));
	};


	/*
	 * =====================
	 * Private Members
	 */
private:
	eRandLoc 	m_side;		//!< The side or the location, where *this is used.
	eRandKind 	m_kind;		//!< The kind of the condition that is used.
	Numeric_t 	m_val;		//!< The value that it should have; must be nan in non dirichlet case.
}; //End class(egd_sideCondition_t):



/*
 * ====================
 * Free operators.
 */
inline
bool
operator== (
	const eRandLoc 			lhs,
	const egd_sideCondition_t& 	rhs)
 noexcept
{
	return (rhs == lhs);
};


inline
bool
operator!= (
	const eRandLoc 			lhs,
	const egd_sideCondition_t& 	rhs)
 noexcept
{
	return (rhs != lhs);
};


inline
bool
operator== (
	const eRandKind			lhs,
	const egd_sideCondition_t& 	rhs)
 noexcept
{
	return (rhs == lhs);
};


inline
bool
operator!= (
	const eRandKind			lhs,
	const egd_sideCondition_t& 	rhs)
 noexcept
{
	return (rhs != lhs);
};


PGL_NS_END(egd)


