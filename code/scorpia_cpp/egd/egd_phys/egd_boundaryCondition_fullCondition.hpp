#pragma once
/**
 * \brief	This file implements a class that represents some boundary conditions.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)

/**
 * \brief	This class encodes one boundary condition.
 * \class 	egd_boundaryConditions_t
 *
 * This class stores a full set of boundary conditions.
 * This means this stores information about all four sides
 * or the pressure condition.
 *
 * Such a condition is tied to a property, which is also needed
 * by the constructor. This property must be a representant.
 * It is also important that *this represents the boundary
 * conditions for all properties with the same representant.
 * Thus *This represents the BC in a grid agnostic way.
 *
 * *this is implemented as a list of side conditions, which
 * represents the conditons at a single side. But it is
 * enhanched with meaningfull functions.
 *
 * It is important that *this can be modiofied until it is
 * finalized, after that only read access is possible.
 */
class egd_boundaryConditions_t
{
	/*
	 * ===================
	 * Typedef
	 */
public:
	using PropIdx_t 		= egd_propertyIndex_t;			//!< This is the property index.
	using SideCondition_t		= egd_sideCondition_t;			//!< This is the class that encodes one condition.
	using ConditionList_t 		= ::pgl::pgl_vector_t<SideCondition_t>;	//!< This is a type that lists all the conditions.
	using const_iterator 		= ConditionList_t::const_iterator;	//!< The itertor type.
	using iterator 			= const_iterator;			//!< Alias of the constant iterator.

	/*
	 * =====================
	 * Constructor
	 */
public:
	/**
	 * \brief	This constructor builds a condition
	 * 		 for the given property.
	 *
	 * Note that a condition is not bound to a grid but only
	 * to a property, thus the passed property must be a
	 * representant.
	 * No condition will be added to this.
	 */
	egd_boundaryConditions_t(
		const PropIdx_t 	pIdx)
	 :
	  m_isFin(false),
	  m_pIdx(pIdx),
	  m_conds()
	{
		if(m_pIdx.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid index.");
		};
		if(m_pIdx.isRepresentant() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed index \"" + pIdx + "\" is not a representant.");
		};
		pgl_assert(this->isValid());
	}; //End: EMpty building constructor



	/**
	 * \brief	This function creates a boundary coondition
	 * 		 and adds the full range to *this.
	 *
	 * This is like constructing a condition and then call
	 * the add fucntion for ranges. Note that this constructor
	 * will automatically call finalize if not specified
	 * otherwise.
	 *
	 * \param  pIdx		The property index.
	 * \param  start	Iterator to the first condition.
	 * \param  plast 	Past the end iterator.
	 * \param  doFin	Should *this be finalize, defaults to true.
	 *
	 * \tparam  fwdIt_t 	The type of the range.
	 */
	template<
		class 	fwdIt_t
	>
	egd_boundaryConditions_t(
		const PropIdx_t 	pIdx,
		fwdIt_t 		start,
		fwdIt_t 		last,
		const bool 		doFin = true)
	 :
	  egd_boundaryConditions_t(pIdx)
	{
		this->addCond(start, last);	//Add the range

		if(doFin == true)
		{
			this->makeFinal();
		};
	}; //End: range building


	/**
	 * \brief	This constzructor takes a initializer list
	 * 		 as argument.
	 *
	 * This constructor is for convenience, that allows to
	 * bypass the creation of an explicit range.
	 * It is also possible to apply auto finalize.
	 *
	 * Note if just one argument is given, and this is a pressure
	 * conditoion, then no special behaviour is performed.
	 *
	 * \param  pIdx		The property index.
	 * \param  iList	The inizializer list.
	 * \param  doFin	Perform finialize; defaults to true.
	 */
	egd_boundaryConditions_t(
		const PropIdx_t 				pIdx,
		const std::initializer_list<SideCondition_t> 	iList,
		const bool 					doFin = true)
	 :
	  egd_boundaryConditions_t(pIdx,
	  		  iList.begin(), iList.end(),
	  		  doFin)
	{}; //End: auto range


	/**
	 * \brief	This function constructs *this with the given
	 * 		 side condition.
	 *
	 * This is just for one condition. Note that this constructor
	 * will not finalize *this automatically, it has to be
	 * requested explicitly.
	 *
	 * \param  pIdx		The property index.
	 * \param  sCond	The single condition.
	 * \param  doFin	Finalize *this.
	 */
	egd_boundaryConditions_t(
		const PropIdx_t 		pIdx,
		const SideCondition_t& 		sCond,
		const bool 			doFin)
	 :
	  egd_boundaryConditions_t(pIdx)
	{
		pgl_assert(sCond.isValid());	//Test if okay
		this->addCond(sCond);		//Add condition

		if(doFin == true)		//Test if we must make it final
		{
			this->makeFinal();
		};
	}; //End: build with one condition


	/**
	 * \brief	This constructor is a special version of the single
	 * 		 condition constructor.
	 *
	 * If the passed condition is a pressor condition this function
	 * will finalite *this automatically, if pIdx is not a pressure
	 * conndition then no finalize will be performed.
	 *
	 * \param  pIdx		The condition that should be used.
	 * \param  sCond 	The condition that should be added.
	 */
	egd_boundaryConditions_t(
		const PropIdx_t 		pIdx,
		const SideCondition_t& 		sCond)
	 :
	  egd_boundaryConditions_t(
	  		pIdx,
	  		sCond,
	  		pIdx.isPressure())	//Test if pressure
	{
	}; //End: pressure building


	/**
	 * \brief	This function allows to copy to condition,
	 * 		 but the result is not final.
	 *
	 * This function allows to copy *this, but with the final
	 * flag set to false.
	 */
	egd_boundaryConditions_t
	nonFinalCopy()
	 const;



	/**
	 * \brief	Move constructor.
	 *
	 * Defaulted.
	 */
	egd_boundaryConditions_t(
		egd_boundaryConditions_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Defaulted.
	 */
	egd_boundaryConditions_t&
	operator= (
		egd_boundaryConditions_t&&)
	 = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_boundaryConditions_t(
		const egd_boundaryConditions_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_boundaryConditions_t&
	operator= (
		const egd_boundaryConditions_t&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_boundaryConditions_t()
	 noexcept
	 = default;


	/*
	 * =========================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	Default constructor.
	 *
	 * Is private and defaulted.
	 */
	egd_boundaryConditions_t()
	 noexcept
	 = delete;



	/*
	 * ====================
	 * Query functions
	 *
	 * This functions fakes a part of the
	 * side conditioninterface, but extend
	 * it, such that they make sense for
	 * a full boundary.
	 */
public:

#define MAKEIS(name) 						\
	bool 							\
	is ## name (						\
		const eRandLoc 		loc)			\
	 const 							\
	{							\
		pgl_assert(this->isValid()); 			\
		return this->getSideCond(loc) . is ## name ();	\
	}

	MAKEIS(FreeSlip  );
	MAKEIS(Dirichlet );
	MAKEIS(Insulation);
#undef MAKEIS



	/**
	 * \brief	This function returns if the passed
	 * 		 location has a value.
	 *
	 * If *This does not have a condition at the location,
	 * then an error is generated.
	 *
	 * \param  loc		The location to search for.
	 */
	bool
	hasValue(
		const eRandLoc 		loc)
	 const
	{
		return this->getSideCond(loc).hasValue();
	};


	/**
	 * \brief	This function returns the value
	 * 		 of that condition.
	 *
	 * If *this does not have that location, and
	 * the location does not have a value,
	 * an exception is generated.
	 *
	 * \param  loc		The location to examine.
	 */
	Numeric_t
	getValue(
		const eRandLoc 		loc)
	 const
	{
		return this->getSideCond(loc).getValue();
	};


	/**
	 * \brief 	This function returns true if *this
	 * 		 has internal boundaries.
	 *
	 * This function will iterate through all the single
	 * conditions and check any of them is a internal
	 * condition. If at least one is found true is returned.
	 *
	 * \note	That only the conditions that are found
	 * 		 before the first internal one are checked.
	 */
	bool
	hasInternalBC()
	 const
	 noexcept
	{
		if(this->hasCondition() == false)
			{ return true; };

		for(const SideCondition_t& cond : this->m_conds)
		{
			pgl_assert(cond.isValid());
			if(cond.isInternalBC() == true)
				{ return true; };
		}; //End for(cond):

		return false;
	};//End: check if *this has internal bc


	/**
	 * \brief	This function returns the number of
	 * 		 internal boundary conditions inside *this.
	 *
	 * This function will scan, and verify, by assert,
	 * if all conditions are valid and returns the number
	 * of condiutions that returned true for internal BC
	 * test.
	 */
	Size_t
	nInternalBC()
	 const
	 noexcept
	{
		if(this->hasCondition() == false)
			{ return 0; };

		Size_t n = 0;
		for(const SideCondition_t& cond : this->m_conds)
		{
			pgl_assert(cond.isValid());
			if(cond.isInternalBC() == true)
				{ n += 1; };
		}; //End for(cond):

		return n;
	}; //End: number of internals


	/*
	 * =====================
	 * Status functions
	 */
public:
	/**
	 * \brief	This function returns true if *this is valid.
	 *
	 * Note that if *this does not have any conditions, then it is valid to.
	 */
	bool
	isValid()
	 const
	 noexcept;

	/**
	 * \brief	This function returns true if *this is apressure condition.
	 *
	 * This is done by checking if the property index of *this is pressure.
	 */
	bool
	isPressure()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());

		return this->m_pIdx.isPressure();
	};


	/**
	 * \brief	Returns true if *this has conditions.
	 *
	 * This function tests if teh list of conditions is empty or not.
	 */
	bool
	hasCondition()
	 const
	 noexcept
	{
		return (m_conds.empty())
			? false
			: true;
	};


	/**
	 * \brief	Returns true if *this has a condition at
	 * 		 the specified location.
	 *
	 * This function test if a side has a condition isnside this
	 *
	 * \param  loc		The location that is searched for.
	 */
	bool
	hasCondition(
		const eRandLoc 		loc)
	 const
	{
		if(isValidRandLoc(loc) == false)
		{
			throw PGL_EXCEPT_InvArg("The given location enum is invalid.");
		};

		return ::pgl::contains(this->cbegin(), this->cend(), loc);
	};


	/**
	 * \brief	Returns the numbers of conditions
	 */
	Index_t
	nConditions()
	 const
	 noexcept
	{
		return m_conds.size();
	};


	/**
	 * \brief	This function will print out *this.
	 *
	 * This function will produces a list like output
	 * that names all the different conditions that
	 * are stored indie this. The output is like:
	 * 	[pIdx: {COND_LIST} | FIN]
	 */
	std::string
	print()
	 const;



	/*
	 * =======================
	 * Access functions
	 */
public:
	/**
	 * \brief	Returns a constant reference to the ith condition.
	 *
	 * Note that an exception is generated if an out of bound access happens.
	 *
	 * \param  i	The condition that is requested.
	 */
	const SideCondition_t&
	operator[](
		const Index_t 		i)
	 const
	{
		pgl_assert(this->isValid(),
			   0 <= i, i < (Index_t)m_conds.size());
		return m_conds.at(i);
	};


	/**
	 * \brief	An alias of nConditions().
	 *
	 * Provided for synatctic reasons.
	 */
	Index_t
	size()
	 const
	 noexcept
	{
		return this->nConditions();
	};


	/**
	 * \brief	This function returns a reference to the
	 * 		 condition in location loc.
	 *
	 * This function searches for the condition and will return
	 * a constant reference to it. It the condition does not
	 * exist it will throw an exception.
	 *
	 * \param  loc		The location that is requested.
	 */
	const SideCondition_t&
	getSideCond(
		const eRandLoc 		loc)
	 const
	{
		pgl_assert(this->isValid());

		if(isValidRandLoc(loc) == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid location parameter.");
		};

		//Search for the condition
		const auto fLoc = ::std::find(this->cbegin(), this->cend(), loc);

		if(fLoc == this->cend())
		{
			throw PGL_EXCEPT_OutOfBound("The location " + loc + " is unkown.");
		};

		return (*fLoc);
	}; //End: ghetSide condition


	/**
	 * \brief	This is an ovberload for the getSideCond() function.
	 *
	 * \param  loc		The location to search for.
	 */
	const SideCondition_t&
	operator[] (
		const eRandLoc 		loc)
	 const
	{
		return this->getSideCond(loc);
	};


	/**
	 * \brief	Access the given location loc.
	 *
	 * \param  loc		The location that should be accessed.
	 */
	const SideCondition_t&
	at(
		const eRandLoc 		loc)
	 const
	{
		return this->getSideCond(loc);
	};


	/**
	 * \brief	This function returns the property index of *this.
	 *
	 */
	PropIdx_t
	getPropIdx()
	 const
	 noexcept
	{
		pgl_assert(m_pIdx.isValid(),
			   m_pIdx.isRepresentant());
		pgl_assert(this->isValid());

		return m_pIdx;
	};//End: get property index


	/*
	 * =====================
	 * Managing function.
	 */
public:
	/**
	 * \brief	This function returns true if *this is final.
	 *
	 * If *this is final, then no new side conditions can be added
	 * to this.
	 */
	bool
	isFinal()
	 const
	 noexcept
	{
		pgl_assert(this->isValid());
		return m_isFin;
	};


	/**
	 * \brief	This function makes *this final.
	 *
	 * Note that it performs some chacks before *this is final.
	 */
	void
	makeFinal()
	{
		//A final object can be turned final again. this is then a no ops.
		//One could argue that this should raise an error, but we will not
		//do that.
		if(this->isFinal() == true)
		{
			return;
		}; //End if: is already final

		//Test if *This is valid
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("Can not turn an invalid object into a final one.");
		};

		/*
		 * Make *this final
		 */
		this->m_isFin = true;

		return;
	}; //End: makeFinal


	/**
	 * \brief	This function adds the new condition inside *this.
	 *
	 * Note that *this must not be final.
	 * Also it is tested if the condition is inserted again.
	 *
	 * \param  newCond	The new condition that should be added.
	 */
	egd_boundaryConditions_t&
	addCond(
		const SideCondition_t& 	newCond);


	/**
	 * \brief	This function allows to add a range of side conditions.
	 *
	 * This function iterates through the provided range and calls
	 * the add function on each element. The iterator must be at least
	 * a forward iterator.
	 *
	 * \param  start	The first object to add.
	 * \param  last 	Iterator to the past the end condition.
	 *
	 * \tparam fwdIt_t	The type of the forward iteration.
	 */
	template<
		class 		fwdIt_t
	>
	egd_boundaryConditions_t&
	addCond(
		fwdIt_t 	start,
		fwdIt_t 	last)
	{
		//Going through the range
		for(fwdIt_t it = start; it != last; ++it)
		{
			this->addCond(*it);
		};//End for(it):
			pgl_assert(this->isValid());

		return *this;
	}; //End: add a range of conditions


	/**
	 * \brief	Adds newCond to *this.
	 *
	 * This allows a nice creation.
	 *
	 * \param  newCond	The condition that should be added.
	 */
	egd_boundaryConditions_t&
	operator<< (
		const SideCondition_t& 		newCond)
	{
		return this->addCond(newCond);
	};


	/**
	 * \brief	This is yet again an alias for add condition.
	 *
	 * \param  newCond	This is the new condition that is added.
	 */
	egd_boundaryConditions_t&
	operator()(
		const SideCondition_t& 		newCond)
	{
		return this->addCond(newCond);
	};


	/**
	 * \brief	This functions adds the conditions that are inside
	 * 		 the inizializer list to *this.
	 *
	 * \param  iList	The list that should be added.
	 */
	egd_boundaryConditions_t&
	addCond(
		const std::initializer_list<SideCondition_t> 	iList)
	{
		return this->addCond(iList.begin(), iList.end());
	};


	/**
	 * \brief	This function allows to remove a
	 * 		 single condition from *this.
	 *
	 * *this must not be final, otherwhise an exception will
	 * be generated. The contion that should be removed is
	 * identified by a location enum.
	 * If the location does not exists an error is generated.
	 *
	 * Note that a removal will invalidate all references
	 * iterators and so on to single conditions. But the function
	 * allows to remove an invalid single condition.
	 *
	 * \param  loc		The location that should be removed.
	 */
	void
	removeCond(
		const eRandLoc 		loc);



	/*
	 * ======================
	 * Iterator functions
	 *
	 * Only constant iterators are supported
	 */
public:
	/**
	 * \brief	Iterator to the first condition of *this.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return m_conds.cbegin();
	};


	/**
	 * \brief	Iterator to the first condition of *this.
	 *
	 * Explicit constant version.
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		return m_conds.cbegin();
	};


	/**
	 * \brief	Past the end iterator.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return m_conds.cend();
	};


	/**
	 * \brief	Past the end iterator.
	 *
	 * Explicit constant version.
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		return m_conds.cend();
	};





	/*
	 * =====================
	 * Private Members
	 */
private:
	bool 			m_isFin = false;  //!< Indicates if *this is final.
	PropIdx_t 		m_pIdx;		  //!< This is the property that *this applies to.
	ConditionList_t 	m_conds;		  //!< This is a list of conditions that are managed by this.
}; //End class(egd_boundaryConditions_t):



/**
 * \brief	This typedef is used to shorten the type name
 * 		 for a condition.
 */
using egdBC_t = egd_boundaryConditions_t;



PGL_NS_END(egd)


