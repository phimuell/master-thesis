#pragma once
/**
 * \brief	This file contains functions and classes that deals with gravity.
 *
 * Currently only a uniform gravity is supported. This could also be used to
 * expres the gravity at one point.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <string>


PGL_NS_START(egd)


/**
 * \brief	This class is used to represent the gravity at a single point.
 * \class 	egd_gravForce_t
 *
 * This is the gravitational force that is acting at one point.
 * Note that this class can also be used to express the graviatational form
 * everywhere in the case of uniform gravity.
 *
 * Note that due to definition of the coordinate system in EGD, the y coordinate
 * does not indicates the force that it is pulled upwards, but the force pulled
 * downwards.
 */
class egd_gravForce_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Numeric_t 	= ::pgl::Numeric_t;		//!< Underlying type
	using value_type 	= Numeric_t;			//!< For compability with std
	using Scalar		= Numeric_t;			//!< For compability with eigen
	using iterator 		= Numeric_t*;			//!< Iterator type.
	using const_iterator 	= const Numeric_t*; 		//!< Const Iterator type.

	using EigenMap_t 	= Eigen::Map<Eigen::Vector2d>;		//!< Type for a map
	using constEigenMap_t 	= Eigen::Map<const Eigen::Vector2d>;	//!< Constant Eigen map.



	/*
	 * ========================
	 * Constructors
	 */
public:
	/**
	 * \brief	This constructor allows to set the x component of *this to x.
	 *
	 * The second component y is set to zero.
	 *
	 * \param  x	The value of the x, first, component.
	 */
	egd_gravForce_t(
		const Numeric_t 	x)
	 noexcept
	 :
	  m_grav{x, 0.0}
	{
		pgl_assert(::pgl::isValidFloat(x));
	};


	/**
	 * \brief	This function allows to set the x and y components at construction.
	 *
	 * \param  x	The value of the x, first, component.
	 * \param  y	The value of the y, second, component.
	 */
	egd_gravForce_t(
		const Numeric_t 	x,
		const Numeric_t 	y)
	 noexcept
	 :
	  m_grav{x, y}
	{
		pgl_assert(::pgl::isValidFloat(x),
			   ::pgl::isValidFloat(y) );
	};


	/**
	 * \breief	Default constructor.
	 *
	 * Will initaize to zero.
	 */
	egd_gravForce_t()
	 noexcept
	 = default;


	/**
	 * \brief	Copy constructor.
	 */
	egd_gravForce_t(
		const egd_gravForce_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignment
	 */
	egd_gravForce_t&
	operator= (
		const egd_gravForce_t&)
	 noexcept
	 = default;


	/**
	 * \brief	Move constructor.
	 */
	egd_gravForce_t(
		egd_gravForce_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 */
	egd_gravForce_t&
	operator= (
		egd_gravForce_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Destructor
	 */
	~egd_gravForce_t()
	 noexcept
	 = default;


	/*
	 * =======================
	 * Status and Quering Functions
	 */
public:
	/**
	 * \brief	Get the number of dimensions.
	 */
	constexpr
	Index_t
	nDims()
	 const
	 noexcept
	{
		return NDIMS;
	};


	/**
	 * \brief	Get the number of dimensions.
	 */
	constexpr
	Index_t
	size()
	 const
	 noexcept
	{
		return NDIMS;
	};


	/**
	 * \brief	Returns true if *this is finite.
	 *
	 * This applies std::isfinite() to any member of *this.
	 */
	bool
	isFinite()
	 const
	 noexcept
	{
		for(const Numeric_t& v : m_grav)
		{
			if(::pgl::isValidFloat(v) == false)
			{
				return false;
			}; //End if: is not finite
		};//End for(v):

		return true;
	}; //End: isFinite


	/**
	 * \brief	Test if the x component of *this is zero.
	 *
	 * I know that this is very dubious.
	 */
	bool
	isZeroX()
	 const
	 noexcept
	{
		return (m_grav[0] == 0.0)
			? true
			: false;
	};


	/**
	 * \brief	Test if the y component of *this is zero.
	 *
	 * I know that this is very dubious.
	 */
	bool
	isZeroY()
	 const
	 noexcept
	{
		return (m_grav[1] == 0.0)
			? true
			: false;
	};



	/*
	 * ========================
	 * Accessing functions
	 *
	 * Supported are several versions.
	 */
public:
	/**
	 * \brief	Returns a mutable reference to the ith component.
	 *
	 * Note that an out of bound check is tested by an assert.
	 *
	 * \param  i 	The component to access.
	 */
	Numeric_t&
	operator[] (
		const Index_t 		i)
	 noexcept
	{
		pgl_assert(0 <= i, i < NDIMS);
		return m_grav[i];
	};


	/**
	 * \brief	This function returns a scalar to the ith component.
	 *
	 * This function tests with assert.
	 *
	 * \param  i	The component to access.
	 */
	Numeric_t
	operator[] (
		const Index_t 		i)
	 const
	 noexcept
	{
		pgl_assert(0 <= i, i < NDIMS);
		return m_grav[i];
	};


	/**
	 * \brief	Returns a mutable reference to the ith component.
	 *
	 * \param  i 	The index of the component to access.
	 */
	Numeric_t&
	at(
		const Index_t 		i)
	{
		if(!((0 <= i) && (i < NDIMS)))
		{
			throw PGL_EXCEPT_OutOfBound("Performed an out of bound access; i = " + std::to_string(i) + ", size = " + std::to_string(NDIMS));
		};

		return m_grav[i];
	};


	/**
	 * \brief	Returns the ith component.
	 *
	 * \param  i 	The index of the component to access.
	 */
	Numeric_t
	at(
		const Index_t 		i)
	 const
	{
		if(!((0 <= i) && (i < NDIMS)))
		{
			throw PGL_EXCEPT_OutOfBound("Performed an out of bound access; i = " + std::to_string(i) + ", size = " + std::to_string(NDIMS));
		};

		return m_grav[i];
	};


	/*
	 * ===========================
	 * Special Access
	 *
	 * This functions allows to access the component directly.
	 */
public:
	/**
	 * \brief	Returns access to the first component.
	 */
	Numeric_t&
	x()
	 noexcept
	{
		return m_grav[0];
	};


	Numeric_t
	x()
	 const
	 noexcept
	{
		return m_grav[0];
	};


	/**
	 * \brief	This function gives access to the second component.
	 */
	Numeric_t&
	y()
	 noexcept
	{
		return m_grav[1];
	};


	Numeric_t
	y()
	 const
	 noexcept
	{
		return m_grav[1];
	};


	/*
	 * =======================
	 * Eigen Interface
	 */
public:
	/**
	 * \brief	Returns a map object, that mimics a Vector2d.
	 */
	EigenMap_t
	getMap()
	 noexcept
	{
		return EigenMap_t(m_grav);
	};


	/**
	 * \brief	Returns a map object, that mimics a const Vector2d
	 */
	constEigenMap_t
	getMap()
	 const
	 noexcept
	{
		return constEigenMap_t(m_grav);
	};


	/**
	 * \brief	Returns a map object, that mimics a const Vector2d
	 *
	 * Explicit version.
	 */
	constEigenMap_t
	cgetMap()
	 const
	 noexcept
	{
		return constEigenMap_t(m_grav);
	};


	/*
	 * ==========================
	 * Outputting
	 */
public:
	/**
	 * \brief	This function prints out *this.
	 *
	 * The string that is generated has the form [x(), y()].
	 */
	std::string
	print()
	 const;



	/*
	 * =========================
	 * Private Memers
	 */
private:
	static const Index_t 	NDIMS = 2;
	Numeric_t		m_grav[2] = {0.0, 0.0};		//!< Components of the graviational force.
}; //End class(egd_gravForce_t):



/**
 * \brief	Allows to output the gravity to a ostream.
 *
 * \param  o 		The output stream.
 * \param  grav		The gravity object.
 */
inline
std::ostream&
operator<< (
	std::ostream& 		o,
	const egd_gravForce_t&	grav)
{
	o << grav.print();
	return o;
};


inline
std::string
operator+ (
	const std::string& 	s,
	const egd_gravForce_t&	grav)
{
	return s + grav.print();
};


inline
std::string
operator+ (
	const egd_gravForce_t&	grav,
	const std::string& 	s)
{
	return grav.print() + s;
};







PGL_NS_END(egd)


