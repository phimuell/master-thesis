#pragma once
/**
 * \brief	This function implements the functionality to compute and store the stress (deviatoric).
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_phys/egd_strainRate.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)


/**
 * \brief	Represents the dress of a grid or a configuaration.
 * \class 	egd_deviatoricStress_t
 *
 * This class manages the deviatoric stress of configuration.
 * It stores three matrices that contains \sigma_{xx}, \sigma_{yy}
 * and \sigma_{xy} respectively.
 * The three stresses are not defined at the same location.
 * The {xx, yy} stresses are defined in the cell center of the basic nodal
 * grid. The {xy} stress is defined at the internal grid points of the
 * basic nodal grid, see the description of teh egd_deviatoricStrainRate_t
 * for more information on that.
 *
 * For computation the stress the strain is used. The formula
 * $\sigma_{ij}^{\prime} = 2 \eta \dot{\epsilon}}_{ij}^{\prime}$ is
 * used for that.
 * Since the deviatoric strain rate is used, *this is also the
 * deviatoric stress.
 *
 * The class guarantees that all entries of that matrix that refere to
 * positions that have no meaning ar set to zero.
 *
 * Note that this class stores different grids.
 * The normal components, {XX, YY}, are defined on a CC grid, this grid
 * can be extended or not. However the shhear component, {XY}, is
 * always defined on a regular basic grid.
 */
class egd_deviatoricStress_t
{
	/*
	 * =========================
	 * Typedef
	 */
public:
	using uSize_t 	= ::pgl::Size_t;		//!< Unsigned size type
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using PropIdx_t 	= PropIdx;			//!< This is the idex type for the properties
	using SolverResult_t 	= egd_mechSolverResult_t;	//!< Result of the mechanical solver.
	using GridContainer_t 	= egd_gridContainer_t;		//!< This is the container for the properties.
	using DeviStrainRate_t 	= egd_deviatoricStrainRate_t;	//!< This is the deviatorix strain rate.
	using Stress_t 		= egd_gridProperty_t;		//!< Matrix type for representing the stress. Is used only for exposition, do not use it directly.


	/*
	 * ==========================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * It will use the passed arguiments to compute the stress.
	 *
	 * \param  gridProps	The grid property container.
	 * \param  dEps		The deviatoric strain rate.
	 *
	 * \throw	If an inconsitency is detected.
	 */
	egd_deviatoricStress_t(
		const GridContainer_t& 		gridProps,
		const DeviStrainRate_t&		dEps);


	/**
	 * \brief	Copy constructor.
	 *
	 * Defaulted.
	 */
	egd_deviatoricStress_t(
		const egd_deviatoricStress_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Defaulted.
	 */
	egd_deviatoricStress_t&
	operator= (
		const egd_deviatoricStress_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Defaulted.
	 */
	egd_deviatoricStress_t(
		egd_deviatoricStress_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Defaulted.
	 */
	egd_deviatoricStress_t&
	operator= (
		egd_deviatoricStress_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Defaulted.
	 */
	~egd_deviatoricStress_t()
	 = default;

private:
	/**
	 * \brief	Default constructor.
	 *
	 * Will initialize the member property
	 * with an appropriate index.
	 */
	egd_deviatoricStress_t();


	/*
	 * ====================
	 * Quering and status
	 */
public:
	/**
	 * \brief	This function returns the number of
	 * 		 basic nodal points in x direction.
	 */
	Size_t
	xNodalPoints()
	 const
	 noexcept
	{
		return m_sigmaXX.Nx();
	};

	/**
	 * \brief	This function returns the number of
	 * 		 basic nodal points in y direction.
	 */
	Size_t
	yNodalPoints()
	 const
	 noexcept
	{
		return m_sigmaXX.Ny();
	};


	/**
	 * \brief	Return true if *this is valid.
	 *
	 * Note this function is badly named and will be
	 * removed in the future. It is only an alias for
	 * the isFinite() function.
	 */
	bool
	isValid()
	 const
	 noexcept
	{
		return this->isFinite();
	};


	/**
	 * \brief	This function returns true if *this
	 * 		 has valid sizes.
	 *
	 * This function does some consictency checks but does
	 * not scan the stored values.
	 */
	bool
	checkSize()
	 const
	 noexcept;


	/**
	 * \brief	This function returns ture if *this
	 * 		 is finite.
	 *
	 * This function check the size, by calling checkSize(),
	 * and verifies that the values are finite, by calling
	 * isFinite() on each component.
	 */
	bool
	isFinite()
	 const
	 noexcept;


	/**
	 * \brief	Returns true if all components of *this are on the extended grid.
	 *
	 * This function only queries the normal components, since they can be extended or not.
	 * The shear component is always a regular basic grid.
	 */
	bool
	isExtendedGrid()
	 const
	{
		return (m_sigmaXX.isExtendedGrid() && m_sigmaYY.isExtendedGrid())
			? true
			: false;
	};


	/**
	 * \brief	Returns true if all components of *this are on regular grid size.
	 *
	 * This function only queries the normal components, since they can be extended or not.
	 * The shear component is always a regular basic grid.
	 */
	bool
	isRegularGrid()
	 const
	{
		return (m_sigmaXX.isRegularGrid() && m_sigmaYY.isRegularGrid())
			? true
			: false;
	};


	/**
	 * \brief	This function returns true if *this is allocated.
	 */
	bool
	isAllocated()
	 const;


	/*
	 * ===================
	 * Managing Function.
	 */
public:
	/**
	 * \brief	This function allocates *this.
	 *
	 * This function will allocate the underling grid properties
	 * and set them to zero, but it will not compute the values.
	 *
	 * \param  yBasicN	Number of basic nodal points in y direction.
	 * \param  xBasicN	Number of basic nodal points in x direction.
	 * \param  isExt 	Is the setting an extended one.
	 */
	void
	allocateGrid(
		const yNodeIdx_t 	yBasicN,
		const xNodeIdx_t 	xBasicN,
		const bool 		isExt);


	/**
	 * \brief	This function sets all components of *this to zero.
	 *
	 * This function requieres that *this is allocated.
	 */
	void
	setZero();



	/*
	 * ====================
	 * Access functions.
	 */
public:
	/**
	 * \brief	Returns the xx component of the
	 * 		 deviatoric stress tensor.
	 *
	 * The value is defined in the cell center of the basic nodal cells.
	 * Thus the numbering is such that it starts at the index (1, 1).
	 * Meaning that the first row and first column are invalid.
	 */
	const Stress_t&
	xx()
	 const
	 noexcept
	{
		return m_sigmaXX;
	};


	/**
	 * \brief	Returns the yy component of the deviatoric
	 * 		 strain rate tensor.
	 *
	 * The value is defined in the cell center of the basic nodal cells.
	 * Thus the numbering is such that it starts at the index (1, 1).
	 * Meaing that the first row and first column are invalid.
	 */
	const Stress_t&
	yy()
	 const
	 noexcept
	{
		return m_sigmaYY;
	};


	/**
	 * \brief	Returns the xy component of the
	 * 		 deviatoric stress tensor.
	 *
	 * The values is only defined on the interiour basic nodel grid points.
	 * Thus the fris and last row and column are invalid and caries no meaning.
	 * Thus the first value is located at (1, 1).
	 */
	const Stress_t&
	xy()
	 const
	 noexcept
	{
		return m_sigmaXY;
	};


	/**
	 * \brief	Returns the yx component of the
	 * 		 deviatoric stress tensor.
	 *
	 * This is an alias of xy().
	 */
	const Stress_t&
	yx()
	 const
	 noexcept
	{
		return m_sigmaXY;
	};


	/*
	 * ===================
	 * Internal function
	 */
private:
	/**
	 * \brief	This is the building function.
	 *
	 * From the viscosity property of the grid and the
	 * previously computed deviatoric strain rate
	 * the stress is computed.
	 *
	 * \param  gridProps	The grid property container.
	 * \param  dEps		The deviatoric strain rate.
	 *
	 * \throw	If an inconsitency is detected.
	 */
	void
	priv_compuetStress(
		const GridContainer_t& 		gridProps,
		const DeviStrainRate_t&		dEps);



	/*
	 * ====================
	 * Private Members
	 */
public:
	Stress_t 		m_sigmaXX;	//!< This is the deviatoric strain rate in xx direction.
	Stress_t 		m_sigmaYY;	//!< This is the deviatoric strain rate in yy direction.
	Stress_t 		m_sigmaXY;	//!< This is the deviatoric strain rate in xy direction.
}; //End class(egd_deviatoricStress_t)


PGL_NS_END(egd)



