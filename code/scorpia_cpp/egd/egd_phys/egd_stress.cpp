/**
 * \brief	This function implements some functions for the stress calculations.
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_util.hpp>
#include <egd_phys/egd_stress.hpp>
#include <egd_phys/egd_strainRate.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)

bool
egd_deviatoricStress_t::checkSize()
 const
 noexcept
{
	if(this->isAllocated() == false)
	{
		pgl_assert(false && "*this is not allocated.");
		return false;
	};

	// Test if the sizes are the same
	if(m_sigmaXX.checkOnSameGrid(m_sigmaYY) == false ||
	   m_sigmaXX.Nx() != m_sigmaXY.Nx()	       ||
	   m_sigmaXX.Ny() != m_sigmaXY.Ny()		 )
	{
		pgl_assert(false && "Equal test has failed.");
		return false;
	}; //End if: size

	if(m_sigmaXX.checkSize() == false ||
	   m_sigmaXY.checkSize() == false ||
	   m_sigmaYY.checkSize() == false   )
	{
		pgl_assert(false && "The size has a problem.");
		return false;
	};

	if(m_sigmaXX.onCellCentrePoints() == false)	//YY is the same because of the check above
	{
		pgl_assert(false && "{XX, YY} are on the wrong grid.");
		return false;
	};
	if(m_sigmaXY.onBasicNodalPoints() == false)
	{
		pgl_assert(false && "{XY} is on the wrong grid.");
		return false;
	};

	return true;
}; //End: checkSize




bool
egd_deviatoricStress_t::isFinite()
 const
 noexcept
{
	//Check if the sizes are correct
	if(this->checkSize() == false)
	{
		return false;
	};

	// The tests will also check the other values that are undefined
	if(m_sigmaXX.isFinite() == false)
	{
		pgl_assert(false && "XX has invalid numbers.");
		return false;
	};
	if(m_sigmaXY.isFinite() == false)
	{
		pgl_assert(false && "XY has invalid values.");
		return false;
	};
	if(m_sigmaYY.isFinite() == false)
	{
		pgl_assert(false && "YY has invalid values.");
		return false;
	};

	/*
	 * If we are here, we have not found any inconsistency, so
	 * we are valid, at least as the tests are concerned.
	 */
	return true;
}; //End: isValid


void
egd_deviatoricStress_t::setZero()
{
	pgl_assert(this->isAllocated());

	//Set the components to zero
	m_sigmaXX.setConstantAll(0.0);
	m_sigmaXY.setConstantAll(0.0);
	m_sigmaYY.setConstantAll(0.0);

	return;
}; //End: setZero


bool
egd_deviatoricStress_t::isAllocated()
 const
{
	pgl_assert((m_sigmaXX.isAllocated() || m_sigmaXY.isAllocated() || m_sigmaYY.isAllocated())
			==
		   (m_sigmaXX.isAllocated() && m_sigmaXY.isAllocated() && m_sigmaYY.isAllocated()) );

	return m_sigmaXX.isAllocated();
};


void
egd_deviatoricStress_t::allocateGrid(
	const yNodeIdx_t 	yBasicN,
	const xNodeIdx_t 	xBasicN,
	const bool 		isExt)
{
	//Only allow one allocation
	if(this->isAllocated() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate the grid twice.");
	};

	//Test input sizes
	if(yBasicN <= Index_t(2))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points is to small in y direction.");
	};
	if(xBasicN <= Index_t(2))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points is to small in x direction.");
	};

	//Get the correct grid type
	const eGridType diagType  = isExt ? mkExt(eGridType::CellCenter) : mkReg(eGridType::CellCenter);
	const eGridType shearType = mkReg(eGridType::BasicNode);

	//Allocate the matrices, note that the grid differs edepending on the component.
	m_sigmaXX.allocateGrid(yNodeIdx_t(yBasicN), xNodeIdx_t(xBasicN), diagType,  PropIdx::StressXX() );
	m_sigmaYY.allocateGrid(yNodeIdx_t(yBasicN), xNodeIdx_t(xBasicN), diagType,  PropIdx::StressYY() );
	m_sigmaXY.allocateGrid(yNodeIdx_t(yBasicN), xNodeIdx_t(xBasicN), shearType, PropIdx::StressXY() );

	//Set everything to zero
	this->setZero();

	//tets if all went well
	pgl_assert(this->isAllocated());

	return;
}; //End: allocate



void
egd_deviatoricStress_t::priv_compuetStress(
	const GridContainer_t& 		gridProps,
	const DeviStrainRate_t&		dEps)
{
	using pgl::isValidFloat;

	/*
	 * NOTE
	 * This implementation will compute the deviatoric stress
	 * as long as the deviatoric strain is passed to it.
	 */
	if(this->isAllocated() == true)
	{
		throw PGL_EXCEPT_LOGIC("The grid was already allocated.");
	};
	pgl_assert(dEps.isRegularGrid() != dEps.isExtendedGrid());

	//Load the variables
	const auto& ETA_BG  = gridProps.cgetProperty(PropIdx::Viscosity()   );	//Viscosity at the basic nodal points
	const auto& ETA_P   = gridProps.cgetProperty(PropIdx::ViscosityCC() );	//Viscosity at the cell centre points

	//Check if the viscosities are on the correct grid
	if(ETA_BG.onBasicNodalPoints() == false)
	{
		throw PGL_EXCEPT_InvArg("The ETA_BG property is not on the expected grid it is on grid " + ETA_BG.getGridType());
	};
	if(ETA_P.onCellCentrePoints() == false)
	{
		throw PGL_EXCEPT_InvArg("The ETA_P property is not on the expected grid it is on grid " + ETA_P.getGridType());
	};
	if(dEps.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The dEps object is invalid.");
	};

	//load the size of the basic grid
	const xNodeIdx_t Nx(ETA_P.Nx() );
	const yNodeIdx_t Ny(ETA_P.Ny() );
		pgl_assert(Nx >  Index_t(0)         , Ny >  Index_t(0),
			   Nx == dEps.xNodalPoints(), Ny == dEps.yNodalPoints() );

	//Perform the allocation
	this->allocateGrid(Ny, Nx, dEps.isExtendedGrid());


	/*
	 * Compute 	\sigma_{xy}
	 *
	 * This is defined at the girtd points where we have already
	 * the viscosity, so we can just compute
	 * 	\sigma_{xy}^{\prime} = 2 \cdot \eta \cdot \dot{\epsilon}_{xy}^{\prime}
	 */
	m_sigmaXY = 2.0 * dEps.xy().array() * ETA_BG.array();
		pgl_assert(m_sigmaXY.Nx() == Nx,
			   m_sigmaXY.Ny() == Ny );

	/*
	 * Compute 	\sigma{xx, yy}
	 *
	 * This is defined at the cell center points. Note in previous
	 * versions, this function has interpolated the viscossity to
	 * that location manually. But this is not needed anymore.
	 *
	 * Note that the assignment operator will also check if the
	 * is correct.
	 */
	m_sigmaXX = 2.0 * dEps.xx().array() * ETA_P.array();
	m_sigmaYY = 2.0 * dEps.yy().array() * ETA_P.array();
		pgl_assert(m_sigmaXX.Nx() == Nx, m_sigmaYY.Nx() == Nx,
			   m_sigmaXX.Ny() == Ny, m_sigmaYY.Ny() == Ny );

	return;
}; //End: clculate the stress



/*
 * ===========================
 * Constructors
 */

egd_deviatoricStress_t::egd_deviatoricStress_t()
 :
  m_sigmaXX(PropIdx::StressXX()),
  m_sigmaYY(PropIdx::StressYY()),
  m_sigmaXY(PropIdx::StressXY())
{};

egd_deviatoricStress_t::egd_deviatoricStress_t(
	const GridContainer_t& 		gridProps,
	const DeviStrainRate_t&		dEps)
 :
  egd_deviatoricStress_t()
{
	this->priv_compuetStress(gridProps, dEps);
}; //End building constructor

PGL_NS_END(egd)

