/**
 * \brief	This file implements a class that represents some boundary conditions.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_algorithm.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)

bool
egd_boundaryConditions_t::isValid()
 const
 noexcept
{
	//Test if the property indiex is valid
	if(m_pIdx.isValid() == false)
	{
		pgl_assert(false && "Property index is not valid.");
		return false;
	};
	if(m_pIdx.isRepresentant() == false)
	{
		pgl_assert(false && "pIdx is not a representant.");
		return false;
	};

	//Test if all conditions are valid
	for(const auto& it : (*this))
	{
		if(it.isValid() == false)	//Check if valid
		{
			return false;
		};
	}; //End for(it):


	if(m_pIdx.isPressure() == false)
	{
		/*
		 * Here we are in the non pressure condition tests.
		 */

		//Test if they are sorted
		if(::std::is_sorted(this->cbegin(), this->cend()) == false)
		{
			pgl_assert(false && "Is not sorted.");
			return false;
		};

		//Test if they are unique
		if(::pgl::is_unique(this->cbegin(), this->cend()) == false)
		{
			pgl_assert(false && "Is not unique.");
			return false;
		};
		//End if: no pressure condition
	}
	else
	{
		/*
		 * In the pressure tests we have other conditions.
		 */
		switch(m_conds.size())
		{
			case 0:
				//Everything is okay, since no condition was added
				break;

			case 1:
				//We much check if the single condition is an error
				if(m_conds.at(0).isPressure() == false)
				{
					pgl_assert(false && "No pressure condition.");
					return false;
				};

				//if we are here, the single condition we have is a pressure and we can go
				break;

			default:
				pgl_assert(false && "No many single condition for a pressure.");
				return false;
				break;

		}; //End switch
	};//End else: pressure condition is applied

	//We did not found an error
	return true;
}; //End: isValid


egd_boundaryConditions_t&
egd_boundaryConditions_t::addCond(
	const SideCondition_t& 	newCond)
{
	if(this->isFinal() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to add a condition to a final object.");
	};
	if(newCond.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid condition to *this.");
	};
	if(this->hasCondition(newCond.getSide()) == true)
	{
		throw PGL_EXCEPT_InvArg("Tried to add the same location twice.");
	};
		pgl_assert(this->isValid());

	//If *this is pressure, then we must make special tests
	if(this->isPressure() == true)
	{
		if(newCond.isPressure() == false)
		{
			throw PGL_EXCEPT_InvArg("Passes a non pressure condition to a pressure boundary.");
		};

		if(m_conds.size() != 0)
		{
			throw PGL_EXCEPT_InvArg("*this already contains a pressure condition.");
		};
	//End if: make some additional checks in the case of pressure.
	}
	else
	{
		//*this is not a pressure, so the new condition must not be one
		if(newCond.isPressure() == true)
		{
			throw PGL_EXCEPT_InvArg("Passed a pressure condition to the boundary, but *this is not one, this is a condition for " + m_pIdx);
		};
	}; //End else:

	//We will now simply add it at the end of the condition list
	this->m_conds.push_back(newCond);

	//Now sort the list
	::std::sort(m_conds.begin(), m_conds.end());
		pgl_assert(this->isValid());

	//Return a reference to *this
	return *this;
}; //End: add new condition


void
egd_boundaryConditions_t::removeCond(
	const eRandLoc 		loc)
{
	if(isValidRandLoc(loc) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid location.");
	};
	if(m_pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("*this has an invalid property index.");
	};
	if(this->hasCondition() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to delete a condition from an empty boundary conditon list.");
	};
	if(m_pIdx.isPressure() == true)
	{
		if(m_conds.size() == 1)
		{
			if(m_conds.at(0).isPressure() == false)
			{
				throw PGL_EXCEPT_LOGIC("Detected a non pressure condtion in a pressure condition.");
			};
		}
		else
		{
			throw PGL_EXCEPT_LOGIC("The number of single condition is wrong it was " + std::to_string(m_conds.size()) + ", but should be 0.");
		};
	}; //End if: handle pressure case
	if(this->isFinal() == true)
	{
		throw PGL_EXCEPT_LOGIC("Can not erase a condition from a final boundary condition.");
	};

	//This is the location where we found it
	Index_t foundIdx = -1;	//Is an index

	/*
	 * Go through the list of conditions to search
	 * for the location we need
	 */
	for(Index_t i = 0; m_conds.size(); ++i)
	{
		const auto& c = m_conds.at(i);	//Load the condtion and make a reference

		if(c.getLoc() == loc)	//Test if we found the correct one
		{
			if(foundIdx != -1)	//Test if first one
			{
				throw PGL_EXCEPT_LOGIC("Found at least two occurance for the location " + loc);
			};

			//Store the index
			foundIdx = i;
		//End if: found the location
		}
		else
		{
			//We enforce consistency
			if(c.isValid() == false)
			{
				throw PGL_EXCEPT_LOGIC("Found an invalid single condition in the list.");
			};
		}; //End else: not the one we are looking for
	}; //End for(i): searching

	if(foundIdx <  0             	      ||
	   foundIdx >= (Index_t)m_conds.size()   )
	{
		throw PGL_EXCEPT_LOGIC("Did not found the location " + loc + ", the index was " + std::to_string(foundIdx));
	};

	//Now we remove the one we need
	m_conds.erase(m_conds.cbegin() + foundIdx);

	return;
}; //End: remove condtitio


egd_boundaryConditions_t
egd_boundaryConditions_t::nonFinalCopy()
 const
{
	if(this->isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not copy an invalid object.");
	};

	egd_boundaryConditions_t dolly(*this);	//Make a copy of *This

	//Manipulate the final set
	dolly.m_isFin = false;

	return dolly;	//Return the copied condition
}; //End: non final coping



std::string
egd_boundaryConditions_t::print()
 const
{
	pgl_assert(this->isValid());

	//Make the beginning
	std::string ret = "[pIdx(" + m_pIdx + "): ";

	//Add the list of all conditions
	ret += "{";

	if(hasCondition() == true)
	{
		for(Size_t i = 0; i != m_conds.size() - 1; ++i)
		{
			ret += m_conds.at(i).print() + ", ";
		}; //ENd for(i): goiing through all but the last condition

		//Add the last condition
		ret += m_conds.back().print();
	};//End if: has conditions

	// End of condition listing
	ret += "}";

	//Beginning of the fin state
	ret += " FIN(" + (this->isFinal() ? std::string("True") : std::string("False")) + ")";

	//End of listing
	ret += "]";

	//Return
	return ret;
}; //End: print


PGL_NS_END(egd)


