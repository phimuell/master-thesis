#pragma once
/**
 * \brief	This implements a container for boundary condition.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridProperty.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)

/**
 * \class 	egd_BCMap_t
 * \brief	Container for simplifing the working of boundaries.
 *
 * This container is provided to ease the use of boundary condition.
 * It implements a map, who maps properties to bopundary conditions.
 * It is intended to be used by many other classes as building bloks.
 *
 * It is only able to hold boundary conditions that are final.
 */
class egd_BCMap_t
{
	/*
	 * ===================
	 * Typedef
	 */
public:
	using PropIdx_t 		= egd_propertyIndex_t;			//!< This is the property index.
	using GridProperty_t 		= egd_gridProperty_t;			//!< This is a grid property.
	using SideCondition_t		= egd_sideCondition_t;			//!< This is the class that encodes one condition.
	using BoundaryCondition_t	= egd_boundaryConditions_t;		//!< This is a full boundary condition.
	using ConditionList_t 		= ::pgl::pgl_vector_t<BoundaryCondition_t>;	//!< This is a type that lists all the conditions.
	using const_iterator 		= ConditionList_t::const_iterator;	//!< The itertor type.
	using iterator 			= const_iterator;			//!< Alias of the constant iterator.

	/*
	 * =====================
	 * Constructor
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * Builds an empty map.
	 * Is defaulted.
	 */
	egd_BCMap_t()
	 noexcept
	 = default;


	/**
	 * \brief	This function allows to create a map
	 * 		 and add one condition.
	 *
	 * \param  bCond		The condition we want to add
	 */
	egd_BCMap_t(
		const BoundaryCondition_t& 	bCond)
	 :
	  egd_BCMap_t()
	{
		this->addCond(bCond);
	};


	/**
	 * \brief	This constructir allows to add two
	 * 		 conditions to the map.
	 *
	 * \param  bCond1	The first condition.
	 * \param  bCond2	The second condition.
	 */
	egd_BCMap_t(
		const BoundaryCondition_t& 	bCond1,
		const BoundaryCondition_t& 	bCond2)
	 :
	  egd_BCMap_t({bCond1, bCond2})
	{};


	/**
	 * \brief	This constructir allows to add three
	 * 		 conditions to the map.
	 *
	 * \param  bCond1	The first condition.
	 * \param  bCond2	The second condition.
	 * \param  bCond3 	The third condition.
	 */
	egd_BCMap_t(
		const BoundaryCondition_t& 	bCond1,
		const BoundaryCondition_t& 	bCond2,
		const BoundaryCondition_t& 	bCond3)
	 :
	  egd_BCMap_t({bCond1, bCond2, bCond3})
	{};


	/**
	 * \brief	Allows to initailaize from an initaalizer list.
	 *
	 * \param  iList	The list.
	 */
	egd_BCMap_t(
		const std::initializer_list<BoundaryCondition_t>& 	iList)
	 :
	  egd_BCMap_t(iList.begin(), iList.end())
	{};


	/**
	 * \brief	Initailaize it from any ranged object.
	 *
	 * \param  cont
	 */
	template<
		typename 	T
	>
	egd_BCMap_t(
		const T& 	cont)
	 :
	  egd_BCMap_t(std::begin(cont), std::end(cont))
	{};


	/**
	 * \brief	This is a constructor that is
	 * 		 able to process a range.
	 *
	 * Thsi constructor takes a range of forward iterators
	 * and adds all conditions that are given.
	 *
	 * \param  start 	Start of the range.
	 * \param  last 	Past the end itertaor to the range.
	 *
	 * \tparam  fwdIt_t	Type of the forward iterator
	 */
	template<
		typename  	fwdIt_t
	>
	egd_BCMap_t(
		fwdIt_t 	start,
		fwdIt_t 	last)
	 :
	  egd_BCMap_t()
	{
		this->addCond(start, last);
	};



	/**
	 * \brief	Move constructor.
	 *
	 * Defaulted.
	 */
	egd_BCMap_t(
		egd_BCMap_t&&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Defaulted.
	 */
	egd_BCMap_t&
	operator= (
		egd_BCMap_t&&)
	 = default;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_BCMap_t(
		const egd_BCMap_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_BCMap_t&
	operator= (
		const egd_BCMap_t&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_BCMap_t()
	 noexcept
	 = default;


	/*
	 * =======================
	 * Quering function
	 *
	 * This function gives information
	 * about the state of *this.
	 */
public:
	/**
	 * \brief	This function tests if *this has a condition for
	 * 		 the passed boundary.
	 *
	 * Note that actually the representant of pIdx is searched.
	 *
	 * \param  pIdx		The property that should be loaded.
	 */
	bool
	hasConditionFor(
		const PropIdx_t& 	pIdx)
	 const
	{
		pgl_assert(pIdx.isValid());

		//Get the iterator to the point
		const auto it = this->priv_searchFor(pIdx);

		if(it == this->cend())	//End means that we do not have it
		{
			return false;
		};

		return true;
	}; //End: has Condition for


	/**
	 * \brief	Over load for a grid property.
	 *
	 * This function will extract the index of the
	 * grid property and call the underlying function.
	 *
	 * \param  gProp	The grid property.
	 */
	bool
	hasConditionFor(
		const GridProperty_t& 		gProp)
	 const
	{
		return this->hasConditionFor(gProp.getPropIdx());
	}; //End: has condit for grid property


	/**
	 * \brief	This function returns teh number of conditions
	 * 		 that are stored inside *this.
	 */
	Index_t
	nConditions()
	 const
	 noexcept
	{
		return m_cond.size();
	};


	/**
	 * \brief	This function returns true if *this does not have
	 * 		 any conditions.
	 */
	bool
	noConditions()
	 const
	 noexcept
	{
		return m_cond.empty();
	};


	/**
	 * \brief	Returns true if *this is valid.
	 *
	 * *this is valid if all added boundary condtions are
	 * valid. The case were *this is empty is defined as
	 * valid.
	 */
	bool
	isValid()
	 const
	{
		for(const auto& it : m_cond)
		{
			if(it.isValid() == false)
			{
				return false;
			};//End if: memeber is not valid
		};//End for(it):

		return true;
	};//End: isValid


	/*
	 * ==================
	 * Accessing Function
	 *
	 * These function allows to access a
	 * specific condition.
	 */
public:
	/**
	 * \brief	This function returns a reference to the
	 * 		 appropriate condition.
	 *
	 * If the condition is not found then an error is generated.
	 *
	 * \param  pIdx		For which proeprty we want the condition.
	 */
	const BoundaryCondition_t&
	getConditionFor(
		const PropIdx_t& 		pIdx)
	 const
	{
		pgl_assert(pIdx.isValid());

		//look for the position
		const auto it = this->priv_searchFor(pIdx);

		if(it == this->cend())	//Test if not found
		{
			throw PGL_EXCEPT_OutOfBound("No condition for " + pIdx + " is known.");
		};

		return (*it);
	}; //End: getConditionFor


	/**
	 * \brief	Overload for grid property objects.
	 *
	 * \param  gProp	The grid property that we want to restrict.
	 */
	const BoundaryCondition_t&
	getConditionFor(
		const GridProperty_t& 		gProp)
	 const
	{
		return this->getConditionFor(gProp.getPropIdx());
	};


	/**
	 * \brief	Overload of teh getConditionFor() function.
	 *
	 * \param  pIdx		The index we want to query.
	 */
	const BoundaryCondition_t&
	operator[](
		const PropIdx_t& 	pIdx)
	 const
	{
		return this->getConditionFor(pIdx);
	};


	/**
	 * \brief	Overload of the operator[] for grid proeprties.
	 *
	 * \param  gProp	The grid property
	 */
	const BoundaryCondition_t&
	operator[](
		const GridProperty_t& 	gProp)
	 const
	{
		return this->getConditionFor(gProp.getPropIdx());
	};


	/*
	 * =========================
	 * Managing Function.
	 *
	 * These functions allows to manipulate the
	 * underlying arraqy that stores the conditions.
	 */
public:
	/**
	 * \brief	This function adds a condition to *this.
	 *
	 * This function will add the passed condition bCond to the
	 * underlying storage, a copy is made.
	 *
	 * Note that it is not allowed to pass an invalid, non final
	 * condtion to this function, nor a condition, property, that
	 * is already known.
	 *
	 * \param  bCond		The condition to add.
	 */
	egd_BCMap_t&
	addCond(
		const BoundaryCondition_t& 		bCond)
	{
		if(bCond.isFinal() == false)
		{
			throw PGL_EXCEPT_InvArg("The boundary condition for " + bCond.getPropIdx() + " is not final.");
		};
		if(bCond.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed boundary condition is invalid.");
		};

		//Now look for the position
		const auto it = this->priv_searchFor(bCond.getPropIdx());

		if(it != this->cend())
		{
			throw PGL_EXCEPT_LOGIC("Already has a condition for " + bCond.getPropIdx());
		};//End if: prop already known

		//Now we can add it at the end
		m_cond.push_back(bCond);


		//REtrun reference to ourself
		return *this;
	}; //End: add condition


	/**
	 * \brief	This function allows to add a range of conditions.
	 *
	 * This function iterates through the provided range and calls
	 * the add function on each element. The iterator must be at least
	 * a forward iterator.
	 *
	 * \param  start	The first object to add.
	 * \param  last 	Iterator to the past the end condition.
	 *
	 * \tparam fwdIt_t	The type of the forward iteration.
	 */
	template<
		class 		fwdIt_t
	>
	egd_BCMap_t&
	addCond(
		fwdIt_t 	start,
		fwdIt_t 	last)
	{
		//Going through the range
		for(fwdIt_t it = start; it != last; ++it)
		{
			this->addCond(*it);
		};//End for(it):

		return *this;
	}; //End: add a range of conditions




	/*
	 * ======================
	 * Iterator functions
	 *
	 * Only constant iterators are supported
	 */
public:
	/**
	 * \brief	Iterator to the first condition of *this.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return m_cond.cbegin();
	};


	/**
	 * \brief	Iterator to the first condition of *this.
	 *
	 * Explicit constant version.
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		return m_cond.cbegin();
	};


	/**
	 * \brief	Past the end iterator.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return m_cond.cend();
	};


	/**
	 * \brief	Past the end iterator.
	 *
	 * Explicit constant version.
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		return m_cond.cend();
	};



	/*
	 * =====================
	 * Private functions
	 */
public:
	/**
	 * \brief	This function returns an iterator that points to
	 * 		 the requzested pIdx.
	 *
	 * Note that the representant of pIdx is searched for and
	 * that also the end iterator could be returned, if pIdx is
	 * not inside *this.
	 *
	 * \param  pIdx		The property index.
	 */
	const_iterator
	priv_searchFor(
		const PropIdx_t& 	pIdx)
	 const
	{
		if(pIdx.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
		};

		if(m_cond.empty() == true)	//Shortcut for empty containers
		{
			return this->cend();
		};//End if: container is empty

		const PropIdx_t pRepIdx = pIdx.getRepresentant();	//Get the representant

		/*
		 * Go through all the conditions that are stored
		 */
		for(auto it = this->cbegin(); it != this->cend(); ++it)
		{
			pgl_assert(it->isValid());

			if(it->getPropIdx() == pRepIdx)	//Test iof we have it
			{
				return it;
			};
		};//End for(it)

		//We did not found anything, so we return the end pointer
		return this->cend();
	}; //End: search for



	/*
	 * =====================
	 * Private Members
	 */
private:
	ConditionList_t 	m_cond;		  //!< This is a list of conditions that are managed by this.
}; //End class(egd_BCMap_t):


PGL_NS_END(egd)


