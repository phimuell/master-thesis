/**
 * \brief	This file contains implementations for the heating term code.
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>
#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)

egd_heatingTerm_t::egd_heatingTerm_t()
 :
  m_gType(eGridType::INVALID),
  m_adiabaticHeating(PropIdx::AdiabaticHeat() ),
  m_radioHeating(    PropIdx::RadioEnergy()   ),
  m_shearHeating(    PropIdx::ShearHeat()     )
{};


egd_heatingTerm_t::egd_heatingTerm_t(
	const GridContainer_t&		grid,
	const StrainRate_t&		dEps,
	const Stress_t&			sig,
	const SolverResult_t&		solRes,
	const GravForce_t& 		grav,
	const bool 			onExt)
 :
  egd_heatingTerm_t()
{
	//Call the implementation
	this->priv_compuetHeatingTerms(grid, dEps, sig, solRes, grav, onExt);
}; //End: building constructor


void
egd_heatingTerm_t::allocateGrid(
	const yNodeIdx_t 	yBasicN,
	const xNodeIdx_t 	xBasicN,
	const bool 		isExt)
{
	if(this->isAllocated() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate an already allocated heating term.");
	};
	if(m_gType != eGridType::INVALID)
	{
		throw PGL_EXCEPT_LOGIC("The heating term is not allocated, but the grid type is not invalid, it is " + m_gType);
	};

	//Set the grid type
	m_gType = (isExt == true)			//Store the grid type in the variable.
		   ? mkExt(eGridType::CellCenter)
		   : mkReg(eGridType::BasicNode );

	//Check the imput size
	if(xBasicN <= Index_t(2))
	{
		throw PGL_EXCEPT_InvArg("The number of gird points in x direction was " + std::to_string(xBasicN) + ", need at least 3 to have an inner grid point.");
	};
	if(yBasicN <= Index_t(2))
	{
		throw PGL_EXCEPT_InvArg("The number of gird points in y direction was " + std::to_string(yBasicN) + ", need at least 3 to have an inner grid point.");
	};


	/*
	 * Allocate the Grids
	 */
	m_adiabaticHeating.allocateGrid(yBasicN, xBasicN, m_gType, PropIdx::AdiabaticHeat());
	m_radioHeating.allocateGrid(    yBasicN, xBasicN, m_gType, PropIdx::RadioEnergy()  );
	m_shearHeating.allocateGrid(    yBasicN, xBasicN, m_gType, PropIdx::ShearHeat()    );

	/*
	 * Set the value to zero
	 */
	this->setZero();

	//Test if everything went well
	pgl_assert(this->isAllocated());

	return;
}; //End: allocateGrid


void
egd_heatingTerm_t::priv_compuetHeatingTerms(
	const GridContainer_t&		grid,
	const StrainRate_t&		dEps,
	const Stress_t&			sig,
	const SolverResult_t&		solRes,
	const GravForce_t& 		grav,
	const bool 			onExt)
{
	//Get the geometry
	const GridGeometry_t& gGeo = grid.getGeometry();

	//This is teh size that we expect
	const xNodeIdx_t Nx(gGeo.xNPoints());
	const yNodeIdx_t Ny(gGeo.yNPoints());

	if(grav.isZeroX() == false)
	{
		throw PGL_EXCEPT_InvArg("The gravity is wrong, an x component was passed, the gravity was " + grav);
	};
	if(gGeo.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid geometry is not set.");
	};

	/*
	 * Allocating the terms
	 */
	this->allocateGrid(Ny, Nx, onExt);

	//
	//Shear Heating
	priv_computeShearHeat(dEps, sig, &m_shearHeating);
		pgl_assert(m_shearHeating.Ny() == Ny, m_shearHeating.Nx() == Nx,
			   m_shearHeating.isFinite() );

	//
	//Adiabatic Heating
	priv_computeAdiabaticHeat(grid, solRes, grav, &m_adiabaticHeating);
		pgl_assert(m_adiabaticHeating.Ny() == Ny, m_adiabaticHeating.Nx() == Nx,
			   m_adiabaticHeating.isFinite() );

	//
	//Radioactive Heating
	pric_computeRadioactiveHeating(grid, &m_radioHeating);
		pgl_assert(m_radioHeating.Ny() == Ny, m_radioHeating.Nx() == Nx,
			   m_radioHeating.isFinite() );

	pgl_assert(this->isValid());

	return;
}; //End calculate heating term.


bool
egd_heatingTerm_t::isValid()
 const
 noexcept
{
	if(m_radioHeating.hasValidGridType() == false)
	{
		pgl_assert(false);
		return false;
	};
	if(m_shearHeating.hasValidGridType() == false)
	{
		pgl_assert(false);
		return false;
	};
	if(m_adiabaticHeating.hasValidGridType() == false)
	{
		pgl_assert(false);
		return false;
	};
	if(::egd::isValidGridType(this->m_gType) == false)
	{
		pgl_assert(false);
		return false;
	};

	if(m_adiabaticHeating.checkOnSameGrid(m_radioHeating) == false ||
	   m_adiabaticHeating.checkOnSameGrid(m_shearHeating) == false   )
	{
		pgl_assert(false);
		return false;
	};

	if(m_shearHeating.getType() != m_gType)
	{
		pgl_assert(false);
		return false;
	};

	if(false ==     m_radioHeating.checkSize() ||
	   false ==     m_shearHeating.checkSize() ||
	   false == m_adiabaticHeating.checkSize()   )
	{
		return false;
	};

	if(false ==     m_radioHeating.isFinite() ||
	   false ==     m_shearHeating.isFinite() ||
	   false == m_adiabaticHeating.isFinite()   )
	{
		return false;
	};

	return true;
}; //End is valid:


bool
egd_heatingTerm_t::isAllocated()
 const
{
	//Test if all are allocated in a very strange way.
	pgl_assert((m_radioHeating.isAllocated() || m_shearHeating.isAllocated() || m_adiabaticHeating.isAllocated())
			==
	 	   (m_radioHeating.isAllocated() && m_shearHeating.isAllocated() && m_adiabaticHeating.isAllocated()) );
	pgl_assert(pgl_ifteTest(m_shearHeating.isAllocated(), ::egd::isValidGridType(m_gType), m_gType == eGridType::INVALID));

	return (m_shearHeating.isAllocated())
		? true
		: false;
}; //End: isAllocated


void
egd_heatingTerm_t::setZero()
{
	if(this->isAllocated() == false)
	{
		throw PGL_EXCEPT_InvArg("Heating term is not allocated.");
	};

	//Set the values to zero
	m_radioHeating.setConstantAll(0.0);
	m_shearHeating.setConstantAll(0.0);
	m_adiabaticHeating.setConstantAll(0.0);

	return;
};//End: setZero



PGL_NS_END(egd)


