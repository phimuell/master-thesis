/**
 * \brief	This file contains the implementation of several functions from teh gravity header.
 */

//Include the confg file
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_phys/egd_gravitation.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <string>


PGL_NS_START(egd)


std::string
egd_gravForce_t::print()
 const
{
	return std::string("[") + std::to_string(x()) + ", " + std::to_string(y()) + "]";
};




PGL_NS_END(egd)


