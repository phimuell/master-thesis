/**
 * \brief	This file contains the implementation of some functions that are defined
 * 		 in the boundary file, but not implemented there.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>
#include <egd_phys/egd_boundaryCondition_fullCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers
#include <Eigen/Core>


//Include STD


PGL_NS_START(egd)

std::string
printLoc(
	const eRandLoc 		loc)
{
	pgl_assert(isValidRandLoc(loc));
#define CASE(l, s) case l : return std::string(s); break
	switch(loc)
	{
	  CASE(eRandLoc::TOP,    	"Top"          );
	  CASE(eRandLoc::LEFT,   	"Left"         );
	  CASE(eRandLoc::BOTTOM, 	"Bottom"       );
	  CASE(eRandLoc::RIGHT,  	"Right"        );
	  CASE(eRandLoc::PRESS,  	"Pressure"     );
	  CASE(eRandLoc::INT_LOWSHEAR,	"LowerShearing");
	  CASE(eRandLoc::INT_UPSHEAR,	"UpperShearing");

	  default:
	  	return std::string("INVALID");
	  break;
	}; //End switch(prop):
#undef CASE
}; //End: printLoc


std::string
printKind(
	const eRandKind 	kind)
{
	pgl_assert(isValidRandKind(kind));
#define CASE(l, s) case l : return std::string(s); break
	switch(kind)
	{
	  CASE(eRandKind::freeSlip,   "FreeSlip"  );
	  CASE(eRandKind::Dirichlet,  "Dirichlet" );
	  CASE(eRandKind::Insulation, "Insulation");

	  default:
	  	return std::string("INVALID");
	  break;
	}; //End switch(prop):
#undef CASE
}; //End: printLoc


egd_sideCondition_t
mkDiri(
	const eRandLoc 		loc,
	const Numeric_t 	val)
{
	pgl_assert(::pgl::isValidFloat(val));
	return egd_boundaryConditions_t::SideCondition_t(loc, eRandKind::Dirichlet, val);
};


egd_sideCondition_t
mkPress(
	const Numeric_t 	val)
{
	pgl_assert(::pgl::isValidFloat(val));
	return egd_boundaryConditions_t::SideCondition_t(eRandLoc::PRESS, eRandKind::Dirichlet, val);
};


egd_sideCondition_t
mkDiri(
	const Numeric_t 	val,
	const eRandLoc 		loc)
{
	return mkDiri(loc, val);
};


egd_sideCondition_t
mkInsu(
	const eRandLoc 		loc)
{
	return egd_boundaryConditions_t::SideCondition_t(loc, eRandKind::Insulation);
};


egd_sideCondition_t
mkFS(
	const eRandLoc 		loc,
	const Numeric_t 	val)
{
	pgl_assert(::pgl::isValidFloat(val) == false);
	return egd_boundaryConditions_t::SideCondition_t(loc, eRandKind::freeSlip, val);
};


egd_sideCondition_t
mkFS(
	const Numeric_t 	val,
	const eRandLoc 		loc)
{
	return mkFS(loc, val);
};


egd_sideCondition_t
mkIntern(
	const eRandLoc 		loc,
	const Numeric_t 	val,
	const eRandKind 	kind)
{
	if(isInternalRandLoc(loc) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed a non internal boundary location to the function, passed " + loc);
	};
	//it is good practice to check such things right here
	pgl_assert(kind == eRandKind::Dirichlet, 	//this may change in the furture
		   ::pgl::isValidFloat(val));

	//Every other error will be erforced in the constructor
	return egd_boundaryConditions_t::SideCondition_t(loc, kind, val);
}; //End: make internal shoirthand


PGL_NS_END(egd)


