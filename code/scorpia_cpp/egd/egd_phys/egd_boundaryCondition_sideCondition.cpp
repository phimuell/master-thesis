/**
 * \brief	This fille contains the implementation of the side condition functions.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_phys/egd_boundaryCondition.hpp>
#include <egd_phys/egd_boundaryCondition_sideCondition.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include the Eigen Headers


//Include STD
#include <string>


PGL_NS_START(egd)

std::string
egd_sideCondition_t::print()
 const
{
	pgl_assert(this->isValid());

	//Print location
	std::string ret = printLoc(m_side);

	//add the arrow
	ret += " -> ";

	//Add the type
	ret = ret + m_kind;

	//Modify the output
	switch(m_kind)
	{
	  case eRandKind::Dirichlet:
	  	ret += "(" + std::to_string(m_val) + ")";
	  break;

	  case eRandKind::Insulation:
	  break;

	  case eRandKind::freeSlip:
	  	if(this->hasValue() == true)
		{
	  		ret += "(" + std::to_string(m_val) + ")";
		};
	  break;

	  default:
	  	throw PGL_EXCEPT_RUNTIME("SOmething is very fishy.");
	  break;
	}; //End swiotch(kind):

	return ret;
}; //End: printing


PGL_NS_END(egd)


