/**
 * \brief	This file contains implementations for the shear heat component of the heating term.
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>
#include <egd_phys/egd_heatingTerm.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)


void
egd_heatingTerm_t::priv_computeShearHeat(
	const StrainRate_t& 		dEps,
	const Stress_t& 		sig,
	Heating_t* const 		shearHeat_)
{
	pgl_assert(shearHeat_ != nullptr);
	Heating_t& shearHeat = *shearHeat_;	//Alias the imput argument for better handling.

	pgl_assert(dEps.xNodalPoints() == sig.xNodalPoints(), sig.xNodalPoints() == shearHeat.Nx(),
		   dEps.yNodalPoints() == sig.yNodalPoints(), sig.yNodalPoints() == shearHeat.Ny(),
		   shearHeat.Nx()      >   2                , shearHeat.Ny()     >  2       	   );
	pgl_assert(dEps.xx().onCellCentrePoints(), sig.xx().onCellCentrePoints(),
		   dEps.xy().onBasicNodalPoints(), sig.xy().onBasicNodalPoints(),
		   dEps.yy().onCellCentrePoints(), sig.yy().onCellCentrePoints() );
	pgl_assert(pgl_ifteTest(shearHeat.isExtendedGrid(),
				dEps.isExtendedGrid() && sig.isExtendedGrid(),
				dEps.isRegularGrid()  && sig.isRegularGrid()  ));

	//load the size
	const auto Nx = xNodeIdx_t(dEps.xNodalPoints());
	const auto Ny = yNodeIdx_t( sig.yNodalPoints());
		pgl_assert(Nx > 2, Ny > 2);	//Two grid points would mean no inner point.

	//This are the real dimenions of the underlying grid
	const Index_t nRows = shearHeat.rows();
	const Index_t nCols = shearHeat.cols();

	if(shearHeat.getPropIdx() != PropIdx::ShearHeat())
	{
		throw PGL_EXCEPT_InvArg("The target grid property is not a shear heat.");
	};

	if(shearHeat.hasValidGridType() == false)
	{
		throw PGL_EXCEPT_InvArg("The shear heat property was not set.");
	}
	else
	{
		//perform a resize operation
		shearHeat.resize(Ny, Nx);
	};

	//Load the parameter we need
	const auto& dEPS_XX = dEps.xx();	//Load the deviatoric strain rate
	const auto& dEPS_XY = dEps.xy();
	const auto& dEPS_YY = dEps.yy();
	const auto& SIG_XX  = sig.xx();		//Load the deviatoric stress
	const auto& SIG_XY  = sig.xy();
	const auto& SIG_YY  = sig.yy();
		pgl_assert(dEPS_XX.Nx() == Nx, dEPS_XX.Ny() == Ny,
			    SIG_XX.Nx() == Nx,  SIG_XX.Ny() == Ny );

	//Set the shear heat to zero
	shearHeat.setConstantAll(0.0);

	/*
	 * Start calculating the shear heat
	 */
	if(shearHeat.onBasicNodalPoints() == true)
	{
		/*
		 * We now calculate the shear heat at the basic nodal points,
		 * but only in the interour ones.
		 * As it was stated in the header, the {xx, yy} heat component
		 * is calculated on the four surrounding points and then averaged,
		 * to get the value at the nodal point.
		 *
		 * TODO: find an Eigen expression.
		 */
		for(Index_t j = 1; j != (nCols - 1); ++j)
		{
			for(Index_t i = 1; i != (nRows - 1); ++i)
			{
				//Load the variables
				const Numeric_t dEPS_XY_ij   = dEPS_XY(i    , j    );
				const Numeric_t dEPS_XX_ij   = dEPS_XX(i    , j    );
				const Numeric_t dEPS_XX_i1j  = dEPS_XX(i + 1, j    );
				const Numeric_t dEPS_XX_ij1  = dEPS_XX(i    , j + 1);
				const Numeric_t dEPS_XX_i1j1 = dEPS_XX(i + 1, j + 1);
				const Numeric_t dEPS_YY_ij   = dEPS_YY(i    , j    );
				const Numeric_t dEPS_YY_i1j  = dEPS_YY(i + 1, j    );
				const Numeric_t dEPS_YY_ij1  = dEPS_YY(i    , j + 1);
				const Numeric_t dEPS_YY_i1j1 = dEPS_YY(i + 1, j + 1);

				const Numeric_t SIG_XY_ij    =  SIG_XY(i    , j    );
				const Numeric_t SIG_XX_ij    =  SIG_XX(i    , j    );
				const Numeric_t SIG_XX_i1j   =  SIG_XX(i + 1, j    );
				const Numeric_t SIG_XX_ij1   =  SIG_XX(i    , j + 1);
				const Numeric_t SIG_XX_i1j1  =  SIG_XX(i + 1, j + 1);
				const Numeric_t SIG_YY_ij    =  SIG_YY(i    , j    );
				const Numeric_t SIG_YY_i1j   =  SIG_YY(i + 1, j    );
				const Numeric_t SIG_YY_ij1   =  SIG_YY(i    , j + 1);
				const Numeric_t SIG_YY_i1j1  =  SIG_YY(i + 1, j + 1);


				/*
				 * 	XY
				 * This is already at the right position.
				 *
				 * Note that this term apears twiche in the final sum.
				 * Beacuae wre also have XY.
				 */
				const Numeric_t HEATING_XY = SIG_XY_ij * dEPS_XY_ij;


				/*
				 * 	XX
				 * First calculated at the four surounding pressure points
				 * then averaged to the pressure point.
				 */
				const Numeric_t HEATING_XX_ij   = SIG_XX_ij   * dEPS_XX_ij  ;	//The heating at the coriner points
				const Numeric_t HEATING_XX_i1j  = SIG_XX_i1j  * dEPS_XX_i1j ;
				const Numeric_t HEATING_XX_ij1  = SIG_XX_ij1  * dEPS_XX_ij1 ;
				const Numeric_t HEATING_XX_i1j1 = SIG_XX_i1j1 * dEPS_XX_i1j1;

				//Calculate the average and thus the final result.
				const Numeric_t HEATING_XX  = 0.25 * ((HEATING_XX_ij + HEATING_XX_i1j) + (HEATING_XX_ij1 + HEATING_XX_i1j1));


				/*
				 * 	YY
				 * First calculated at the four surounding pressure points
				 * then averaged to the pressure point.
				 */
				const Numeric_t HEATING_YY_ij   = SIG_YY_ij   * dEPS_YY_ij  ;	//The heating at the coriner points
				const Numeric_t HEATING_YY_i1j  = SIG_YY_i1j  * dEPS_YY_i1j ;
				const Numeric_t HEATING_YY_ij1  = SIG_YY_ij1  * dEPS_YY_ij1 ;
				const Numeric_t HEATING_YY_i1j1 = SIG_YY_i1j1 * dEPS_YY_i1j1;

				//Calculate the average and thus the final result.
				const Numeric_t HEATING_YY  = 0.25 * ((HEATING_YY_ij + HEATING_YY_i1j) + (HEATING_YY_ij1 + HEATING_YY_i1j1));


				/*
				 * Final heating
				 *
				 * The XY tearms apears twiche, and here we account for it.
				 */
				const Numeric_t HEATING = HEATING_XX + HEATING_YY + 2.0 * HEATING_XY;

				//Write the result back
				shearHeat(i, j) = HEATING;
			}; //End for(i):
		}; //End for(j):
	//End if: basic nodal point grid
	}
	else if(shearHeat.onCellCentrePoints() == true && shearHeat.isExtendedGrid() == true)
	{
		/*
		 * Here we calculate the shear heat on the Cell centre grid.
		 * The handling is basically the same as it was in the basic
		 * grid but a bit swapped. Now the {xx, yy} components are
		 * at the right position and the {xy} component has to be
		 * interpolated to the CC point. for that the four
		 * sourounding values are averaged. Here it is different.
		 * Inseatd of +1 we have to add -1, beacuse the nodes are
		 * behind the CC point.
		 *
		 * It should be clear, why it does not start at zero.
		 * But why does it not goes up to Nx?
		 * We are on the extended CC grid, so we have $Ni + 1$
		 * many grid points, sincew we are zero based, the last
		 * valid index to access would be $Ni$, but this one
		 * is already outside the domain, since we only calculate
		 * the inner ones, this must not be accessed. So the last
		 * node we have to compute a value for is $Ni - 1$.
		 * So we have to iterate only over the innder part.
		 */
		for(Index_t j = 1; j != (nCols - 1); ++j)
		{
			for(Index_t i = 1; i != (nRows - 1); ++i)
			{
				//
				//Load the variables

				//Strain rate
				const Numeric_t dEPS_XX_ij   = dEPS_XX(i    , j    );
				const Numeric_t dEPS_YY_ij   = dEPS_YY(i    , j    );
				const Numeric_t dEPS_XY_ij   = dEPS_XY(i    , j    );
				const Numeric_t dEPS_XY_ij1  = dEPS_XY(i    , j - 1);
				const Numeric_t dEPS_XY_i1j  = dEPS_XY(i - 1, j    );
				const Numeric_t dEPS_XY_i1j1 = dEPS_XY(i - 1, j - 1);

				//Stress
				const Numeric_t SIG_XX_ij    =  SIG_XX(i    , j    );
				const Numeric_t SIG_YY_ij    =  SIG_YY(i    , j    );
				const Numeric_t SIG_XY_ij    =  SIG_XY(i    , j    );
				const Numeric_t SIG_XY_ij1   =  SIG_XY(i    , j - 1);
				const Numeric_t SIG_XY_i1j   =  SIG_XY(i - 1, j    );
				const Numeric_t SIG_XY_i1j1  =  SIG_XY(i - 1, j - 1);


				/*
				 * 	XY
				 * Not at the right position, we first calculate the
				 * shear heat component and will then perform the
				 * averaging.
				 */
				const Numeric_t HEATING_XY_ij   = SIG_XY_ij   * dEPS_XY_ij  ;
				const Numeric_t HEATING_XY_i1j  = SIG_XY_i1j  * dEPS_XY_i1j ;
				const Numeric_t HEATING_XY_ij1  = SIG_XY_ij1  * dEPS_XY_ij1 ;
				const Numeric_t HEATING_XY_i1j1 = SIG_XY_i1j1 * dEPS_XY_i1j1;

				//Now calculate the heating
				const Numeric_t HEATING_XY      = 0.25 * (HEATING_XY_ij + HEATING_XY_i1j + HEATING_XY_ij1 + HEATING_XY_i1j1);


				/*
				 * 	XX
				 * Already at the right position
				 */
				const Numeric_t HEATING_XX = SIG_XX_ij * dEPS_XX_ij;


				/*
				 * 	YY
				 * Already at the right position
				 */
				const Numeric_t HEATING_YY = SIG_YY_ij * dEPS_YY_ij;


				/*
				 * Final heating
				 *
				 * The XY tearms apears twiche, and here we account for it.
				 */
				const Numeric_t HEATING = HEATING_XX + HEATING_YY + 2.0 * HEATING_XY;

				//Write the result back
				shearHeat(i, j) = HEATING;
			}; //End for(i):
		}; //End for(j):

	//End if: shear heat at CC Points
	}
	else
	{
		throw PGL_EXCEPT_InvArg("Unkown grid.");
	}; //End else: no known grid

	return;
}; //End: claculating the shear heat.



PGL_NS_END(egd)



