#pragma once
/**
 * \brief	This functin implements the heating term.
 *
 * The heating term is composed out of many components.
 * The class defined here is used to collect them.
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>
#include <egd_phys/egd_gravitation.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)


/**
 * \class 	egd_heatingTerm_t
 * \class 	This class represents the heating term.
 *
 * The heating term can be seen as the source term that
 * appears inside the temperature equation. It is composed
 * of many different terms. Currently three terms are
 * implemented. Radioactive heating (is optional),
 * adiabatic heating and sear heating.
 *
 * This class is used to compose them.
 *
 * The heating is defined at the grid points of the basic
 * nodal grid. For several reasons only the inner grid points
 * have a meaning, the other grid points are zero.
 * However the Matrices still have the same size as the grid.
 * Thus the first and last row/column does not carry a meaning
 * but are pressent.
 *
 * The implementation is such that it is detected if it can be
 * computed or not. For example if no radioscative
 * heating is inside the grid container than the heating will
 * be zero or no themal expansion alpha means zero adiabatic heat.
 */
class egd_heatingTerm_t
{
	/*
	 * =========================
	 * Typedef
	 */
public:
	using uSize_t 	= ::pgl::Size_t;		//!< Unsigned size type
	using Size_t 	= ::pgl::Index_t;	//!< This is the size type of *this. Note that it is not pgl::Size_t.
						//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;	//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;	//!< This is the float type that is used

	using PropIdx_t 	= PropIdx;			//!< This is the idex type for the properties
	using SolverResult_t 	= egd_mechSolverResult_t;	//!< Result of the mechanical solver.
	using Heating_t 	= egd_gridProperty_t;		//!< Matrix type for representing the heating. For exposition only.
	using GridContainer_t 	= egd_gridContainer_t;		//!< This is the container for the properties.
	using GridGeometry_t 	= egd_gridGeometry_t;		//!< This is the grid geometry class.
	using StrainRate_t 	= egd_deviatoricStrainRate_t;	//!< This is the type for the deviatoric strain rate.
	using Stress_t 		= egd_deviatoricStress_t;	//!< This is the type for the deviatoric stress.
	using GravForce_t	= egd_gravForce_t;		//!< Type for expressing gravity.


	/*
	 * ==========================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor will set up *this. It will use
	 * the grid and the result of the mechanical solver
	 * to set up itself.
	 * Note that x gravity is not supported.
	 *
	 * \param  grid		The grid that is used.
	 * \param  dEps		The deviatoric strain rate.
	 * \param  sig 		The deviatoric stress.
	 * \param  solRes 	The reuslt of the solver.
	 * \param  grav 	The gravity force.
	 * \param  onExt	Is on extended grid.
	 *
	 * \throw	If an inconsitency is detected.
	 */
	egd_heatingTerm_t(
		const GridContainer_t&		grid,
		const StrainRate_t&		dEps,
		const Stress_t&			sig,
		const SolverResult_t&		solRes,
		const GravForce_t& 		grav,
		const bool			onExt);


	/**
	 * \brief	Copy constructor.
	 *
	 * Defaulted.
	 */
	egd_heatingTerm_t(
		const egd_heatingTerm_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Defaulted.
	 */
	egd_heatingTerm_t&
	operator= (
		const egd_heatingTerm_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Defaulted.
	 */
	egd_heatingTerm_t(
		egd_heatingTerm_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Defaulted.
	 */
	egd_heatingTerm_t&
	operator= (
		egd_heatingTerm_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Defaulted.
	 */
	~egd_heatingTerm_t()
	 = default;

	/**
	 * \brief	Default constructor.
	 *
	 * Will make an invalid grid type and not allocated
	 * grid property, but the property index will be set
	 * to the correct value.
	 */
	egd_heatingTerm_t();


	/*
	 * ====================
	 * Quering and status
	 */
public:
	/**
	 * \brief	This function returns the number of
	 * 		 basic nodal points in x direction.
	 */
	Size_t
	xNodalPoints()
	 const
	 noexcept
	{
		return m_shearHeating.cols();
	};


	/**
	 * \brief	This function returns the number of
	 * 		 basic nodal points in y direction.
	 */
	Size_t
	yNodalPoints()
	 const
	 noexcept
	{
		return m_shearHeating.rows();
	};


	/**
	 * \brief	Return true if *this is valid.
	 *
	 * This is an expensive test that will check if the
	 * sizes are correct and if the values are valid.
	 */
	bool
	isValid()
	 const
	 noexcept;

	/**
	 * \brief	Returns true if *this is allocated.
	 *
	 * This function triggers an assert if an inconsistency
	 * is detected.
	 */
	bool
	isAllocated()
	 const;


	/**
	 * \brief	This function returns the number of rows
	 * 		 of the internal properties.
	 *
	 * This function is also provided to fake an Eigen type.
	 */
	Index_t
	rows()
	 const
	 noexcept
	{
		pgl_assert(m_shearHeating.rows() > 0);
		return m_shearHeating.rows();
	};


	/**
	 * \brief	This function returns the number of columns
	 * 		 of the internal property.
	 *
	 * This function is provided to fake an Eigen type.
	 */
	Index_t
	cols()
	 const
	 noexcept
	{
		pgl_assert(m_shearHeating.cols() > 0);
		return m_shearHeating.cols();
	};



	/*
	 * ======================
	 * Managing Function.
	 */
public:
	/**
	 * \brief	This function allocates *this.
	 *
	 * This function will allocate the grid properties of *this
	 * and and will set them to zero, but no computation of *this
	 * happens. Note that it is an error to call this function twice.
	 *
	 * This function will also set teh grid type.
	 *
	 * \param  yBasicN	Number of basic nodal points in y diretion.
	 * \param  xBasicN	Number of basic nodal points in x direction.
	 * \param  isExt	Indicates if teh extended grid case should be used.
	 */
	void
	allocateGrid(
		const yNodeIdx_t 	yBasicN,
		const xNodeIdx_t 	xBasicN,
		const bool 		isExt);


	/**
	 * \brief	This function sets all compponents of *this to zero.
	 *
	 * Note that an exception is generated if *this is not allocated.
	 */
	void
	setZero();


	/*
	 * ====================
	 * Access functions.
	 */
public:
	/**
	 * \brief	Returns radioactive heating term.
	 *
	 * This is the heating that is caused due to the radioactive
	 * decay of materials.
	 * The value is defined only at the inner basic nodal points
	 * of the original grid.
	 */
	const Heating_t&
	radioHeating()
	 const
	 noexcept
	{
		return m_radioHeating;
	};


	/**
	 * \brief	Returns the shear heating.
	 *
	 * This is the heating caused by friction, lously speaked.
	 * The value is defined only at the inner basic nodal points
	 * of the original grid.
	 */
	const Heating_t&
	shearHeating()
	 const
	 noexcept
	{
		return m_shearHeating;
	};


	/**
	 * \brief	Returns the adiabatic heating.
	 *
	 * The adiabatic heating is caused by the compression
	 * of material (can also be a cooling if the material
	 * is expanded).
	 * This property is only defined at the inner grid points
	 * of the basic nodal grid.
	 */
	const Heating_t&
	adiabaticHeating()
	 const
	 noexcept
	{
		return m_adiabaticHeating;
	};


	/**
	 * \brief	This function returns a matrix of the full heating term.
	 *
	 * Note that matirix is constructed on demand and not stored explicitly
	 * inside *this.
	 */
	Heating_t
	getSource()
	 const
	{
		const xNodeIdx_t Nx(this->xNodalPoints() );
		const yNodeIdx_t Ny(this->yNodalPoints() );

		Heating_t fullHeatingTerm(Ny, Nx, mkReg(m_gType), PropIdx::FullHeatingTerm());	//Creating a heating term object
		fullHeatingTerm = m_adiabaticHeating.array() + m_radioHeating.array() + m_shearHeating.array();
			pgl_assert(fullHeatingTerm.isFinite());

		return fullHeatingTerm;
	}; //End: compute the full heating term


	/**
	 * \brief	Returns the complete source term.
	 *
	 * This function allows only inner points.
	 * This is checked by an assert.
	 *
	 * \param  i	The first index (y) of the node.
	 * \param  j	The second index (x) of the node.
	 */
	Numeric_t
	getSource(
		const Index_t 		i,
		const Index_t 		j)
	 const
	 noexcept
	{
		//TODO: Think about a good check for stupid access.
		const Numeric_t thisTerm = (m_shearHeating(i, j) + m_adiabaticHeating(i, j) + m_radioHeating(i, j));
			pgl_assert(pgl::isValidFloat(thisTerm));

		return thisTerm;
	};


	/**
	 * \brief	This function returns the grid type of *this.
	 *
	 * The grid type should be understood as the point where the
	 * source terms are lcoated. Note taht all terms are located
	 * at the same point and that they are defined only at the
	 * inner nodes.
	 */
	eGridType
	getGridType()
	 const
	 noexcept
	{
		pgl_assert(::egd::isValidGridType(m_gType));
		return m_gType;
	};



	/**
	 * \brief	This function is an alias of getSource(i, j).
	 *
	 * This function is provided tsuch that *this can mimik a matrix.
	 *
	 * \param  i	The first index (y) of the node.
	 * \param  j	The second index (x) of the node.
	 */
	Numeric_t
	operator() (
		const Index_t 		i,
		const Index_t 		j)
	 const
	 noexcept
	{
		return this->getSource(i, j);
	};





	/*
	 * ===================
	 * Internal function
	 */
private:
	/**
	 * \brief	This is the building function.
	 *
	 * It will use the grid and the result of the mechanical
	 * solver to set up itself. If the extended case is selected
	 * then the heating terms are computed in the CC points on
	 * the extended grid, not that due to lack of sufficent data,
	 * only the inner poinst are unequal zero.
	 * In the regular case the values are calculated on the basic
	 * nodal grids, again only at the inner ones.
	 * Note that x gravity is not supported.
	 *
	 * \param  grid		The grid that is used.
	 * \param  dEps		The deviatoric strain rate.
	 * \param  sig 		The deviatoric stress.
	 * \param  solRes 	The reuslt of the solver.
	 * \param  grav		The gravity force.
	 * \param  onExt	Indicates if the values should be computed
	 * 			 for the extended case.
	 *
	 * \throw	If an inconsitency is detected.
	 */
	void
	priv_compuetHeatingTerms(
		const GridContainer_t&		grid,
		const StrainRate_t&		dEps,
		const Stress_t&			sig,
		const SolverResult_t&		solRes,
		const GravForce_t& 		grav,
		const bool 			onExt);



	/**
	 * \brief	This function calculates the sear heating term.
	 *
	 * The seahr heating term is defined as:
	 * 	\sigma^\prime : \dot{\epsilon}^{\prime}
	 *
	 * Where : is the double dot operator, which performs a component wise
	 * multiplication and the sums up the resulting four values.
	 *
	 * This function expects that the target grid property has a valid type
	 * and stores a shear heat property.
	 * This function will call the resitze function, which is expected to
	 * set all values to zero.
	 *
	 * Depending on the actuall grid the computation differs. Currently
	 * two grids are supported, the basic nodal grid and the CC grid,
	 * both in regular sizes. In each case the kind of interpolation
	 * does not differ, first the product of the strain and stress is
	 * computed and then the average is done. In the case of a basic
	 * nodal grid, {xx, yy} are averaged and in case of a CC only
	 * {xy} is averaged.
	 *
	 * \param  dEps 	The deviatoric strain rate.
	 * \param  sig		The deviatoric stress.
	 * \param  shearHeat	This is the result value
	 */
	static
	void
	priv_computeShearHeat(
		const StrainRate_t& 		dEps,
		const Stress_t& 		sig,
		Heating_t* const 		shearHeat);


	/**
	 * \brief	This function calculates the radioactive contribution
	 * 		 to the heating term.
	 *
	 * How this function works, also depends on the target grid property.
	 * Note that allways only the inner values are copied.
	 *
	 * \param  grid		The grid.
	 * \param  radHeating	The destination for the radioactive heating.
	 */
	static
	void
	pric_computeRadioactiveHeating(
		const GridContainer_t& 		grid,
		Heating_t* const 		radHeating);


	/**
	 * \brief	This function computes the adiabatic heat.
	 *
	 * Adiabatic heat is caused by the expansion and compression
	 * of material.
	 * Here it is calculated by the following formula:
	 * 	T_0 \cdot \alpha \cdot v_y \cdot \rho \cdot g_y
	 *
	 * This is an approximation, that substitutes the change in pressure
	 * by an expression that is related to the hydrostatic pressure.
	 *
	 * This function can accept two different target grid properties.
	 * The first is the basic nodal point, then the velocity is averaged
	 * in a horizontal way. If requested in the CC points, then a vertical
	 * averaging is used.
	 *
	 * Note that onyl a gravity in y direction is accepted.
	 *
	 * \param  grid		The grid that contains the needed properties.
	 * \param  solRes	The solution of the mechanical solver.
	 * \param  gravY 	The gravity of the problem.
	 * \param  adiHeat	The adiabatic heat variable.
	 *
	 * \note	If the grid container does not have a thermal expanson
	 * 		 property the adiabatic heat is set to zero.
	 */
	static
	void
	priv_computeAdiabaticHeat(
		const GridContainer_t& 		grid,
		const SolverResult_t& 		solRes,
		const GravForce_t& 		grav,
		Heating_t* const 		adiHeat);



	/*
	 * ====================
	 * Private Members
	 */
public:
	eGridType 	m_gType;			//!< This is the grid type, where we are defined on.
	Heating_t 	m_adiabaticHeating;		//!< The adiabatic heating.
	Heating_t 	m_radioHeating;			//!< Heating due to radiaoactive decay.
	Heating_t 	m_shearHeating;			//!< Heating due to shearing.
}; //End class(egd_heatingTerm_t)


PGL_NS_END(egd)



