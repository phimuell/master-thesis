/**
 * \brief	This function implements the some functions for the deviatoric strain rate class.
 */
#include <egd_core.hpp>
#include <egd_eigen.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_interfaces/egd_MechSolverResult.hpp>

#include <egd_phys/egd_util.hpp>
#include <egd_phys/egd_strainRate.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


//Tets if there are incompatibles
#if defined(EGD_INCOMPRESSIBLE_MODE) && (EGD_INCOMPRESSIBLE_MODE == 1)
	//Incompressible mode is selected, so ensure deviatoric strain rate must be enabled.
#	if defined(EGD_ENSURE_DEVIATORIC_STRAIN_RATE) && (EGD_ENSURE_DEVIATORIC_STRAIN_RATE == 1)
		//Do nothing
#	else
		//Not enabled, this is an error
#		pragma error "Incompressible mode is enabled in EGD, but no deviatoric strain rate is enabled."
#	endif
#endif


PGL_NS_START(egd)

bool
egd_deviatoricStrainRate_t::checkSize()
 const
 noexcept
{
	if(this->isAllocated() == false)
	{
		pgl_assert(false && "*this is not allocated.");
		return false;
	};

	// Test if the sizes are the same
	if(m_dEpsilonXX.checkOnSameGrid(m_dEpsilonYY) == false ||
	   m_dEpsilonXX.Nx() != m_dEpsilonXY.Nx()	       ||
	   m_dEpsilonXX.Ny() != m_dEpsilonXY.Ny()		 )
	{
		pgl_assert(false && "Equal test has failed.");
		return false;
	}; //End if: size

	if(m_dEpsilonXX.checkSize() == false ||
	   m_dEpsilonXY.checkSize() == false ||
	   m_dEpsilonYY.checkSize() == false   )
	{
		pgl_assert(false && "The size has a problem.");
		return false;
	};

	if(m_dEpsilonXX.onCellCentrePoints() == false)	//YY is the same because of the check above
	{
		pgl_assert(false && "{XX, YY} are on the wrong grid.");
		return false;
	};
	if(m_dEpsilonXY.onBasicNodalPoints() == false)
	{
		pgl_assert(false && "{XY} is on the wrong grid.");
		return false;
	};

	return true;
};//End: checkSize


bool
egd_deviatoricStrainRate_t::isFinite()
 const
 noexcept
{

	//Check if the size is valid
	if(this->checkSize() == false)
	{
		return false;
	};

	// The tests will also check the other values that are undefined
	if(m_dEpsilonXX.isFinite() == false)
	{
		pgl_assert(false && "XX has invalid numbers.");
		return false;
	};
	if(m_dEpsilonXY.isFinite() == false)
	{
		pgl_assert(false && "XY has invalid values.");
		return false;
	};
	if(m_dEpsilonYY.isFinite() == false)
	{
		pgl_assert(false && "YY has invalid values.");
		return false;
	};

	/*
	 * If we are here, we have not found any inconsistency, so
	 * we are valid, at least as the tests are concerned.
	 */
	return true;
}; //End: isValid


bool
egd_deviatoricStrainRate_t::isAllocated()
 const
{
	pgl_assert((m_dEpsilonXX.isAllocated() || m_dEpsilonXY.isAllocated() || m_dEpsilonYY.isAllocated())
			==
		   (m_dEpsilonXX.isAllocated() && m_dEpsilonXY.isAllocated() && m_dEpsilonYY.isAllocated()) );

	return m_dEpsilonXX.isAllocated();
};


void
egd_deviatoricStrainRate_t::setZero()
{
	pgl_assert(this->isAllocated());

	//Set the components to zero
	m_dEpsilonXX.setConstantAll(0.0);
	m_dEpsilonXY.setConstantAll(0.0);
	m_dEpsilonYY.setConstantAll(0.0);

	return;
};


void
egd_deviatoricStrainRate_t::allocateGrid(
	const yNodeIdx_t 	yBasicN,
	const xNodeIdx_t 	xBasicN,
	const bool 		isExt)
{
	//Only allow one allocation
	if(this->isAllocated() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to allocate the grid twice.");
	};

	//Test input sizes
	if(yBasicN <= Index_t(2))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points is to small in y direction.");
	};
	if(xBasicN <= Index_t(2))
	{
		throw PGL_EXCEPT_InvArg("The number of grid points is to small in x direction.");
	};

	//Get the correct grid type
	const eGridType diagType  = isExt ? mkExt(eGridType::CellCenter) : mkReg(eGridType::CellCenter);
	const eGridType shearType = mkReg(eGridType::BasicNode);

	//Allocate the matrices, note that the grid differs edepending on the component.
	m_dEpsilonXX.allocateGrid(yNodeIdx_t(yBasicN), xNodeIdx_t(xBasicN), diagType,  PropIdx::StrainRateXX() );
	m_dEpsilonYY.allocateGrid(yNodeIdx_t(yBasicN), xNodeIdx_t(xBasicN), diagType,  PropIdx::StrainRateYY() );
	m_dEpsilonXY.allocateGrid(yNodeIdx_t(yBasicN), xNodeIdx_t(xBasicN), shearType, PropIdx::StrainRateXY() );

	//Set everything to zero
	this->setZero();

	//tets if all went well
	pgl_assert(this->isAllocated());

	return;
}; //End: allocate


void
egd_deviatoricStrainRate_t::priv_compuetStrainRate(
	const GridGeometry_t& 		grid,
	const SolverResult_t& 		solRes)
{
	using pgl::isValidFloat;

	/*
	 * NOTE
	 * This implementation does currently conpute the normal
	 * strain rate and not the deviatoric strain rate.
	 * It only computes the deviatoric strain rate, because
	 * the problem is incompressible, thus deviatoric and
	 * normal strain rate are the same.
	 */
	if(this->isAllocated() == true)
	{
		throw PGL_EXCEPT_LOGIC("The grid is allready allocated, this function only works if this is not the case.");
	};
	if(isFullyStaggeredGrid(solRes.getGridType()) == false)
	{
		throw PGL_EXCEPT_InvArg("Deviatoric strain rate can only be computed with a normal stagered grid.");
	};
	if(grid.isGridSet() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed grid geometry is not set.");
	};
	if(grid.isConstantGrid() == false)
	{
		throw PGL_EXCEPT_InvArg("Deviatoric strain rate can only be computed with a constant grid.");
	};
	pgl_assert(solRes.isExtendedGrid() != solRes.isRegularSize());

	//load the size of the basic grid
	const Size_t Nx = grid.xNPoints();
	const Size_t Ny = grid.yNPoints();
		pgl_assert(Nx > 0, Ny > 0);

		//This is for the derivative
	const Numeric_t iDx = 1.0 / grid.getXSpacing();
	const Numeric_t iDy = 1.0 / grid.getYSpacing();
		pgl_assert(isValidFloat(iDx), iDx > 0.0,
			   isValidFloat(iDy), iDy > 0.0 );

	//Load the Velocities
	const auto& VX = solRes.getVelX();
	const auto& VY = solRes.getVelY();


	//Perform the allocation, will also set
	//the grid to zero.
	this->allocateGrid(
		yNodeIdx_t(Ny), xNodeIdx_t(Nx),
		solRes.isExtendedGrid() );

	/*
	 * This is not very efficient
	 *
	 * TODO: Use Eigen
	 */

	/*
	 * Compute dEpsilon_{xy}
	 *
	 * This is defined in the basic nodal points and also
	 * only on the inner points too.
	 *
	 *
	 *  0--------------------------------------> X/J
	 *  |                 j
	 *  |   o-------------o-------------o
	 *  |   |             |             |
	 *  |   |             |             |
	 *  |   |             X             |
	 *  |   |             | Vx1         |
	 *  |   |     Vy1     |             |
	 *  | i o------Y------O------Y------o
	 *  |   |         dExy|      Vy2    |
	 *  |   |             |             |
	 *  |   |             X             |
	 *  |   |             | Vx2         |
	 *  |   |             |             |
	 *  |   o-------------o-------------o
	 *  |
	 *  V Y/I
	 *
	 * The deviatoric strain rate dExx who is defined at the node
	 * "O" which is a node of the basic grid at coordinate (i,j),
	 * has to be computed. To the same node the velocities "Vx1"
	 * and "Vy1" are assicated (have the same coordinates, because
	 * of the staggered grid).
	 *
	 * The property "Vx2" is associated to the node (i+1, j) and
	 * the property "Vy2" is associated to (i, j+1).
	 *
	 * Form the picture it is clear, that the first valid node index
	 * that can be used is (2, 2). It is further clear that no
	 * problem arrises,s ince we will never access ghost node.
	 * We also see that only internal grid points are accessed.
	 * Also we iterate over $1 < i < Nx-1$ and $1 < j < Ny-1$.
	 *
	 * This means the deviatoric strain rate can be computed by
	 *
	 * 	dE_xy = \frac{1}{2} ( \frac{Vx2 - Vx1}{\Delta y} + \frac{Vy2 - Vy1}{\Delta x} )
	 */
	for(Index_t j = 1; j != (Nx - 1); ++j)
	{
		for(Index_t i = 1; i != (Ny - 1); ++i)
		{
			//load the values
			const Numeric_t Vx1 = VX(i    , j    );
			const Numeric_t Vx2 = VX(i + 1, j    );
			const Numeric_t Vy1 = VY(i    , j    );
			const Numeric_t Vy2 = VY(i    , j + 1);
				pgl_assert(isValidFloat(Vx1), isValidFloat(Vx2),
					   isValidFloat(Vy1), isValidFloat(Vy2) );

			//Caculate the derivative
			const Numeric_t dVx_dy = (Vx2 - Vx1) * iDy;
			const Numeric_t dVy_dx = (Vy2 - Vy1) * iDx;

			//Calöculate the strain
			const Numeric_t dE_xy  = 0.5 * (dVx_dy + dVy_dx);
				pgl_assert(isValidFloat(dE_xy));

			//Write result back
			m_dEpsilonXY(i, j) = dE_xy;
		}; //End for(i):
	};//End for(j):


	/*
	 * Now we calculate the dE_{xx, yy} deviatoric strain rate.
	 *
	 *  0-------------------------------> X/J
	 *  |                      i
	 *  |   o--------X---------o
	 *  |   |        Vy1       |
	 *  |   |                  |
	 *  |   |                  |
	 *  |   X      dEii        X
	 *  |   | Vx1              | Vx2
	 *  |   |                  |
	 *  |   |                  |
	 *  | j o--------X---------O
	 *  |            Vy2
	 *  V Y/I
	 *
	 * We want to compute the the strain rate at the middle of the cell.
	 * We are currently at node (i,j), which is marked as "O".
	 * From the staggered grid we know that dE_ii (a short hand for
	 * dE_xx or dE_yy; not to confuse with the invariante) is located
	 * at the pressure point thus is assoicated to the node "O",
	 * also linked to that node is "Vx2" and "Vy2".
	 *
	 * The value "Vy1" is associated to (i-1, j).
	 * The node "Vx1" is associated to (i, j-1).
	 *
	 * The formula to compute the strain rate is,
	 *
	 *  dE_ii = \frac{Vi2 - Vi1}{\Delta i}
	 *
	 * Where i is x or y.
	 *
	 * We also see that now the index ov valid indexes is larger than
	 * before, now we have $1 < i < Nx$, $1 < j < Nx$, meaning that now
	 * the last row/column is valid to and only the first row/column
	 * is undefined.
	 */
	for(Index_t j = 1; j != Nx; ++j)
	{
		for(Index_t i = 1; i != Ny; ++i)
		{
			//Load the variables
			const Numeric_t Vx2 = VX(i    , j    );
			const Numeric_t Vx1 = VX(i    , j - 1);
			const Numeric_t Vy2 = VY(i    , j    );
			const Numeric_t Vy1 = VY(i - 1, j    );
				pgl_assert(isValidFloat(Vx1), isValidFloat(Vx2),
					   isValidFloat(Vy1), isValidFloat(Vy2) );

			//Compute
			const Numeric_t dE_xx = (Vx2 - Vx1) * iDx;
			const Numeric_t dE_yy = (Vy2 - Vy1) * iDy;
				pgl_assert(isValidFloat(dE_xx), isValidFloat(dE_yy));

#if !(defined(EGD_ENSURE_DEVIATORIC_STRAIN_RATE) && (EGD_ENSURE_DEVIATORIC_STRAIN_RATE == 1))
			// No deviatoric strain rate is enforced
#			pragma message "Devoiatoric strain rate is not ensured."

			//Write back
			m_dEpsilonXX(i, j) = dE_xx;
			m_dEpsilonYY(i, j) = dE_yy;

#else
			// Deviatoric strain rate should be ensured.
#			pragma message "Devoiatoric strain rate is ensured."

			/*
			 * In order to compute the deviatoric strain rate,
			 * we have to substract the mean from the ordinary
			 * strain rate
			 */
			const Numeric_t trace 		= dE_xx + dE_yy;		//The trace.
			const Numeric_t staticStrain 	= trace * 0.5;			//The mean of the diagonal values.

			//Correct for the trace
			const Numeric_t dE_prime_xx 	= dE_xx - staticStrain;
			const Numeric_t dE_prime_yy 	= dE_xx - staticStrain;

			//Write back
			m_dEpsilonXX(i, j) = dE_prime_xx;
			m_dEpsilonYY(i, j) = dE_prime_yy;
#endif
		}; //End for(i):
	}; //End for(j):

	return;
}; //End: clculate the strain rate



/*
 * ===============================
 * Constructors
 */

egd_deviatoricStrainRate_t::egd_deviatoricStrainRate_t(
	const GridGeometry_t& 		grid,
	const SolverResult_t& 		solRes)
 :
  egd_deviatoricStrainRate_t()
{
	this->priv_compuetStrainRate(grid, solRes);
}; //End building constructor


egd_deviatoricStrainRate_t::egd_deviatoricStrainRate_t()
 :
  m_dEpsilonXX(PropIdx::StrainRateXX()),
  m_dEpsilonYY(PropIdx::StrainRateYY()),
  m_dEpsilonXY(PropIdx::StrainRateXY())
{};


PGL_NS_END(egd)

