#pragma once
/**
 * \brief	This file contains some small functions that are used in teh whole codebase.
 *
 * The functions that are defined here have more a physical touch than anything else.
 */
#include <egd_core.hpp>
#include <egd_core/egd_eigen.hpp>

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>


//Include Eigen


//Include STD


PGL_NS_START(egd)

/**
 * \brief	This function interpolates the the physical property
 * 	 	 from the basic nodal points to the presure point.
 * 	 	 Harmonic averaging is used.
 *
 * This function assumes that the property, matrix A, is defined
 * at the basic nodal points, it will then (harmonic) average
 * the four corner properties of this cell.
 *
 * Note that the indexing is a bit strage. The first valid index
 * is (i = 1, j = 1), this is the value of the first, top, left
 * corner point.
 *
 * \param  A	The property matrix.
 * \param  i	The first (y) index of the pessure point.
 * \param  j	The second (x) index of the pressure point.
 *
 * \tparam  T	The type of the proery matrix, assumed to be
 * 		 an Eigen compatible object.
 */
template<
	class 	T
>
static
typename T::Scalar
egd_harmonicProperty(
	const T&	A,
	const Index_t 	i,
	const Index_t 	j)
{
	pgl_assert(i > 0, i < A.rows());
	pgl_assert(j > 0, j < A.cols());
	using ::pgl::isValidFloat;

	//load the data
	const auto a_1 = A(i - 1, j - 1);
	const auto a_2 = A(i    , j - 1);
	const auto a_3 = A(i - 1, j    );
	const auto a_4 = A(i    , j    );

	//Compute the sum of the inverse
	const auto sum_ia = ( (1.0 / a_1) + (1.0 / a_2) ) + ( (1.0 / a_3) + (1.0 / a_4) );
	const auto pP = 4.0 / sum_ia;
		pgl_assert(isValidFloat(pP));

	return pP;
}; //End egd_pProperty

PGL_NS_END(egd)



