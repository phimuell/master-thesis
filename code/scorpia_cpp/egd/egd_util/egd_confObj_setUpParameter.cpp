/**
 * \brief	This file contains the implementation of teh setup functions.
 */

//Include EGD
#include <egd_core/egd_core.hpp>
#include "./egd_confObj.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_string.hpp>
#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)

using pgl::isValidFloat;

Numeric_t
egd_confObj_t::setUpPar(
	const std::string& 	par)
 const
{
	const Numeric_t w = this->m_ini.GetReal("SetUp", par, NAN);
	if(isValidFloat(w) == false)
	{
		throw PGL_EXCEPT_InvArg("The key " + par + ", was not found in the SetUp section.");
	};

	return w;
};


Numeric_t
egd_confObj_t::setUpParNAN(
	const std::string& 	par)
 const
{
	//Just try to load the value, use NAN as default.
	const Numeric_t w = this->m_ini.GetReal("SetUp", par, NAN);

	//we do not care if it is NAN or not, so we return it
	return w;
}; //End: setUpParNAN


Index_t
egd_confObj_t::setUpParInt(
	const std::string& 	par)
 const
{
	if(this->m_ini.hasKey("SetUp", par) == false)
	{
		throw PGL_EXCEPT_InvArg("The key " + par + ", was not found in the SetUp section.");
	};
	return this->m_ini.GetInteger("SetUp", par, 0);
};


Numeric_t
egd_confObj_t::setUpPar(
	const std::string& 	par,
	const Numeric_t& 	d)
 const
{
		pgl_assert(isValidFloat(d));
	const auto w = this->m_ini.GetReal("SetUp", par, d);
	if(isValidFloat(w) == false)
	{
		throw PGL_EXCEPT_InvArg("The value that was read in for key " + par + " was invalid.");
	};
	return w;
};


Index_t
egd_confObj_t::setUpPar(
	const std::string& 	par,
	const Index_t& 		d)
 const
{
	const auto w = this->m_ini.GetInteger("SetUp", par, d);
	return w;
};


Numeric_t
egd_confObj_t::setUpGravY()
 const
	{return this->setUpGravY(9.81);};


Numeric_t
egd_confObj_t::setUpGravY(
	const Numeric_t&	d)
 const
	{return this->setUpPar("gravY", d);};



PGL_NS_END(egd)


