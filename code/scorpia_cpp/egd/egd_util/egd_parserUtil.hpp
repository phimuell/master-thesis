#pragma once
/**
 * \brief	This file contains functions that deals with the parsing of the ini file.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include the Eigen Headers


//Include STD
#include <string>


PGL_NS_START(egd)

/**
 * \brief	This function is able to parse a time duration
 * 		 and returns the result in seconds.
 *
 * This function parses the passed string and transforms the found
 * duration in seconds and returns that value. Note that the identifier
 * of the time duration can come at the beginning or the end. If no
 * identifier is found, it is assumed that the value is in second.
 * The identifier is treated case insensitive.
 *
 * The identifieres are:
 * 	s    -> seconds
 * 	mi   -> minutes
 * 	h    -> hours
 * 	w    -> weeks
 * 	mo   -> months
 * 	y|yr -> years
 *
 * \param  s 	The string that should be parsed
 */
Numeric_t
egd_parseTime(
	std::string 	s);






PGL_NS_END(egd)

