/**
 * \brief	This file implements the short hand functions of the configuration object.
 */

//Include EGD
#include <egd_core/egd_core.hpp>
#include "./egd_confObj.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_string.hpp>
#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)

using pgl::isValidFloat;

/*
 * ========================
 * Interface selection
 */
std::string
egd_confObj_t::getMarkerToGridImp()
 const
{
	std::string type = m_ini.Get("MarkerToGrid", "type", "");
	if(type.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No implementation for MarkerToGrid specified.");
	};

	pgl::pgl_strToLower(&type);

	return type;
};


eJobType
egd_confObj_t::getGtoMJobType(
	const eJobType 		defType,
	bool* const 		wasFound)
 const
{
	if(wasFound != nullptr)	//Set the falg to zero if given
	{
		*wasFound = false;
	};

	//Load the string
	const std::string s = this->m_ini.Get("GridToMarker", "JobType", "");

	if(s.empty() == true)	//Test if empty, i.e. not found
	{
		return defType;
	}; //End if: was found

	if(wasFound != nullptr)		//Report that we have found if
	{
		*wasFound = true;
	};

	return parseJobType(s);
}; //ENd: job type


std::string
egd_confObj_t::getStokesSolverImpl()
 const
{
	std::string type = m_ini.Get("StokesSolver", "type", "");
	if(type.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No implementation for StokesSolver specified.");
	};

	pgl::pgl_strToLower(&type);

	return type;
};


std::string
egd_confObj_t::getTemperatureSolverImpl()
 const
{
	std::string type = m_ini.Get("TemperatureSolver", "type", "");
	if(type.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No implementation for TemperatureSolver specified.");
	};

	pgl::pgl_strToLower(&type);

	return type;
};


std::string
egd_confObj_t::getIntegratorImpl()
 const
{
	std::string type = m_ini.Get("Integrator", "type", "");
	if(type.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No implementation for Integrator specified.");
	};

	pgl::pgl_strToLower(&type);

	return type;
};


std::string
egd_confObj_t::getRheologyImpl()
 const
{
	std::string type = m_ini.Get("Rheology", "type", "");
	if(type.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No implementation for Rheology specified.");
	};

	pgl::pgl_strToLower(&type);

	return type;
};


std::string
egd_confObj_t::getGridToMarkerImpl()
 const
{
	std::string type = m_ini.Get("GridToMarker", "type", "");
	if(type.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No implementation for GridToMarker specified.");
	};

	pgl::pgl_strToLower(&type);

	return type;
};


std::string
egd_confObj_t::getSetUpImpl()
 const
{
	std::string type = m_ini.Get("SetUp", "type", "");
	if(type.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No implementation for setup specified.");
	};

	pgl::pgl_strToLower(&type);

	return type;
};


std::string
egd_confObj_t::getApplyBC()
 const
{
	std::string type = m_ini.Get("ApplyBC", "type", "");
	if(type.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No implementation for the boundary is given.");
	};

	pgl::pgl_strToLower(&type);

	return type;
};


PGL_NS_END(egd)


