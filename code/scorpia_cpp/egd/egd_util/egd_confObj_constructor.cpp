/**
 * \brief	This file implements the constructor of the configuration object.
 */

//Include EGD
#include <egd_core/egd_core.hpp>
#include "./egd_confObj.hpp"


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_string.hpp>
#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


egd_confObj_t::egd_confObj_t(
	const Path_t& 	filePath)
 :
  egd_confObj_t()
{
	//Test if filepath exists
	if(::boost::filesystem::exists(filePath) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed file path, " + filePath.string() + ", does not exist.");
	};

	//Load the path
	this->m_path = filePath;

	//Load the configuration
	this->m_ini = IniReader_t(filePath.string());

	if(this->m_ini.hasKeys() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed configuration file, " + filePath.string() + ", did not contain any values.");
	};
}; //End: building constructor.




PGL_NS_END(egd)






