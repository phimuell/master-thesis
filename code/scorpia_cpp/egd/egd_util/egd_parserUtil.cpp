/**
 * \brief	This file contains the implementation of the parser functions.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridType.hpp>

#include "./egd_parserUtil.hpp"


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_string.hpp>


//Include the Eigen Headers


//Include STD
#include <string>


//Include BOOST
#include <boost/algorithm/algorithm.hpp>
#include <boost/algorithm/string/classification.hpp>

using boost::algorithm::is_alpha;
using boost::algorithm::is_digit;


PGL_NS_START(egd)

/**
 * \brief	This function is able to parse a time duration
 * 		 and returns the result in seconds.
 *
 * This function parses the passed string and transforms the found
 * duration in seconds and returns that value. Note that the identifier
 * of the time duration can come at the beginning or the end. If no
 * identifier is found, it is assumed that the value is in second.
 * The identifier is treated case insensitive.
 *
 *
 * \param  s 	The string that should be parsed
 */
Numeric_t
egd_parseTime(
	std::string 	s)
{
	if(s.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("Passed an empty string.");
	};
	if(::std::any_of(s.cbegin(), s.cend(), is_digit()) == false)
	{
		throw PGL_EXCEPT_InvArg("The passed string \"" + s + "\" did not contain any number.");
	};

	//Make lower case out of the numbers
	::pgl::pgl_strToLower(&s);


	/*
	 * Now we scan the file and look for an identifier.
	 * We assume that a number will blok us.
	 */
	std::string tt;					//This is the time tag that we use
	auto isAlpha = ::boost::algorithm::is_alpha();	//Is used to test if it is a letter or not

	if(isAlpha(s.front()) == true)
	{
		//
		//Tag is at the beginning
		while(isAlpha(s.front()) == true)
		{
			tt.push_back(s.front() );	//Copy the first char to the tag
			s.erase(s.cbegin() );		//Remove the copied char from the source

			if(s.empty() == true)
			{
				//If s was fully extracted, we have a
				//problem, so we throw an exception.
				throw PGL_EXCEPT_InvArg("No number found in the passed strig \"" + s + "\""
						" The tag is \"" + tt + "\".");
			};
		}; //ENd while: extracting

	}
	else if(isAlpha(s.back()) == true)
	{
		//
		//Tag is at the end
		while(isAlpha(s.back()) == true)
		{
			tt.push_back(s.back());		//Copy the last char into the tag
			s.erase(s.cend() - 1);		//Remove the  copied chanracter

			if(s.empty() == true)
			{
				//If s was fully extracted, we have a
				//problem, so we throw an exception.
				throw PGL_EXCEPT_InvArg("No number found in the passed strig \"" + s + "\""
						" The tag is \"" + tt + "\".");
			};
		}; //End while: extracting

		//The tag has the wrong order, it is in reverse, so we must change it
		::std::reverse(tt.begin(), tt.end());
	}
	else
	{
		//
		//No tag was given, so it is seconds
		tt = "s";
	};


	//Now we can translate the string into a number
	const Numeric_t sN = std::stod(s);


	/*
	 * New we check if we have to modify the value
	 * The identifieres are:
	 * 	us   -> micro seconds
	 * 	ms   -> mili seconds
	 * 	s    -> seconds
	 * 	mi   -> minutes
	 * 	h    -> hours
	 * 	w    -> weeks
	 * 	mo   -> months
	 * 	y|yr -> years
	 */
	if(tt == "us" || tt == "micro")
	{
		return (sN / (1000.0 * 1000.0));
	}
	else if(tt == "ms" || tt == "mili")
	{
		return (sN / 1000.0);
	}
	else if(tt == "s" || tt == "sec")
	{
		//Just pass it through
		return sN;
	}
	else if(tt == "mi" || tt == "min")
	{
		return (60.0 * sN);
	}
	else if(tt == "h")
	{
		return (3600.0 * sN);
	}
	else if(tt == "w")
	{
		return (7 * 24 * 3600.0 * sN);
	}
	else if(tt == "mo" || tt == "month")
	{
		return (604800 * sN);
	}
	else if(tt == "y" || tt == "yr" || tt == "a")
	{
		return (31556952.0 * sN);
	}
	else
	{
		throw PGL_EXCEPT_InvArg("The extracted time tag \"" + tt + "\" is unkonwn."
				" The passed string was \"" + s + "\".");
	};

		pgl_assert(false && "Reached unreacable code");
	return NAN;
}; //End parse function



PGL_NS_END(egd)

