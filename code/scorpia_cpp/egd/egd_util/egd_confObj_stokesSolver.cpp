/**
 * \brief	Here the functions related to the stokes solver are readed.
 */

//Include EGD
#include <egd_core/egd_core.hpp>

#include "./egd_confObj.hpp"
#include "./egd_parserUtil.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>



//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_string.hpp>
#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>
#include <limits>


PGL_NS_START(egd)

using pgl::isValidFloat;


bool
egd_confObj_t::solverUseFullCell()
 const
{
	return this->m_ini.GetBoolean("StokesSolver", "fullCell", true);
};


PGL_NS_END(egd)


