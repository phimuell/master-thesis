#pragma once
/**
 * \brief	This file implements a class that is able to map grid properties to grid types.
 *
 * This class was developed from a typedef.
 * And is used in the construction phase of the grid.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridType.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_hashmap.hpp>
#include <pgl_checkedAssignment.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)



/**
 * \brief	With this macro it is possible to request that the mapp supports
 * 		 a writting operator[] function.
 * \define 	EGD_PROP_TO_GRID_MAP_BRACKET
 *
 * By default it is deactivated. Note that it is not enough to define the macro.
 * Its wvalue must be set to 1 (one) to activate it.
 */
#if !defined(EGD_PROP_TO_GRID_MAP_BRACKET)
#	define EGD_PROP_TO_GRID_MAP_BRACKET 0
#endif


/**
 * \class 	egd_propToGridMap_t
 * \brief	This class represents the mapping from grid properties to grid types.
 *
 * This mapping is class is used, in the construction phase of the grid.
 * It is is basically a wrapper around a map. That maps grid properties to
 * grid types. It is used to instruct the set up code, which property should
 * be inserted into the grid container, and on which grid this property is
 * defined on.
 * However it offers some validity methods.
 *
 * Note that this class was previously just a typedef of a map. Now it
 * is dedicated class, the reason for that is, that more complex operations
 * are now needed, and the type should be able to themn on its own.
 */
class egd_propToGridMap_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using Size_t 	= ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
							//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 	= ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t = ::pgl::Numeric_t;		//!< This is the float type that is used
	using PropIdx_t = ::egd::egd_propertyIndex_t;	//!< This is the type for encodeing a grid property.


	/**
	 * \brief	This is a property map.
	 * \typedef	BaseMap_t
	 *
	 * This is the underlying type of the map class.
	 * It maps property indexes to grid types
	 */
	using BaseMap_t = ::pgl::pgl_hashMap_t<PropIdx_t, eGridType>;

	using iterator		= BaseMap_t::iterator;		//!< This is the iterator type for the map.
	using const_iterator 	= BaseMap_t::const_iterator;	//!< This is the constant iterator type.

	// These are typedefs for compability
	using value_type 	= BaseMap_t::value_type;
	using key_type 		= BaseMap_t::key_type;
	using mapped_type 	= BaseMap_t::mapped_type;

	using CheckFkt_t 	= ::std::function<bool(eGridType&, const eGridType&)>;
	using ChkAssignment_t	= ::pgl::pgl_checkedAssignment_t<eGridType, CheckFkt_t>;


	/*
	 * ====================
	 * Constructors
	 *
	 * The construction can happens directly, by calling
	 * a building constructor, or later and constzructing
	 * *this by calling the default constructor.
	 */
public:
	/**
	 * \brief	Default constructor.
	 *
	 * Will construct an empyt map.
	 */
	egd_propToGridMap_t();


	/**
	 * \brief	Copy constructor.
	 *
	 * Is default.
	 */
	egd_propToGridMap_t(
		const egd_propToGridMap_t&);


	/**
	 * \brief	Copy assignment.
	 *
	 * Is default.
	 */
	egd_propToGridMap_t&
	operator= (
		const egd_propToGridMap_t&);


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted and marked as no except.
	 */
	egd_propToGridMap_t(
		egd_propToGridMap_t&&)
	 noexcept;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_propToGridMap_t&
	operator= (
		egd_propToGridMap_t&&);


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_propToGridMap_t();



	/*
	 * ============================
	 * Quering Functions
	 *
	 * Here are some small status functions.
	 */
public:
	/**
	 * \brief	Returns the size of the map, i.e
	 * 		 the number of known grid properties.
	 */
	Size_t
	nProps()
	 const
	 noexcept
	{
		return m_map.size();
	};


	/**
	 * \brief	Returns the size of the map.
	 *
	 * This function is an alias of nProps(), that is
	 * provided for compability.
	 */
	Size_t
	size()
	 const
	 noexcept
	{
		return this->nProps();
	};


	/**
	 * \brief	This function returns true if
	 * 		 *this is empty.
	 *
	 * Meaning that nProps() returns 0.
	 */
	bool
	empty()
	 const
	 noexcept
	{
		return this->m_map.empty();
	};


	/**
	 * \brief	This function returns true if
	 * 		 *this is valid.
	 *
	 * This is the case if no invalid values are
	 * stored inside *this.
	 */
	bool
	isValid()
	 const
	 noexcept;


	/**
	 * \brief	This function returns true if *this
	 * 		 maps pIdx.
	 *
	 * This function basically tests if the map exists.
	 *
	 * \param  pIdx		The property index to search for.
	 */
	bool
	hasMappingFor(
		const PropIdx_t 	pIdx)
	 const
	 noexcept;


	/**
	 * \brief	This function is an alias for the
	 * 		 hasMapFor() function.
	 *
	 * \param  pIdx		The property index.
	 */
	bool
	hasProperty(
		const PropIdx_t 	pIdx)
	 const
	 noexcept
	{
		return this->hasMappingFor(pIdx);
	};


	/*
	 * ============================
	 * Map Managing
	 *
	 * These functions allows to manage the
	 * underlying map.
	 */
public:
	/**
	 * \brief	This function allows to \e add
	 * 		 a new mapping.
	 *
	 * The new maping consists of pIdx is ampped
	 * to gType. This function requeres that that
	 * pIdx is not already inside the map.
	 *
	 * \param  pIdx	    The property that should be recorded.
	 * \param  gType    The grid type that is being used.
	 */
	egd_propToGridMap_t&
	addMapping(
		const PropIdx_t 	pIdx,
		const eGridType 	gType);


	/**
	 * \brief	This function allows to add a new mapping.
	 *
	 * Note that this function differes from the other, such
	 * that the size is added my the function. So gType
	 * must have no size bit, the size bit is controlled by
	 * the flag.
	 *
	 * \param  pIdx	 	The property index to add.
	 * \param  gType	The grid type.
	 * \param  isExt	Is the grid extended.
	 */
	egd_propToGridMap_t&
	addMapping(
		const PropIdx_t 	pIdx,
		const eGridType 	gType,
		const bool 		isExt);


	/**
	 * \brief	This function allows to update the mapping.
	 *
	 * This function works like the add function, but it requeres
	 * that the property pIdx is already inside *this, such that
	 * an update of the mapped grid type is performed.
	 *
	 * \param  pIdx	    The property that should be recorded.
	 * \param  gType    The grid type that is being used.
	 */
	egd_propToGridMap_t&
	updateMapping(
		const PropIdx_t 	pIdx,
		const eGridType 	gType);


	/**
	 * \brief	This function allows to update the mapping.
	 *
	 * This function is like the other update function.
	 * But it allows to select if extended or not.
	 * Also note that the grid must not have any size information.
	 *
	 * \param  pIdx	    The property that should be recorded.
	 * \param  gType    The grid type that is being used.
	 * \param  isExt    Is the new grid extended.
	 */
	egd_propToGridMap_t&
	updateMapping(
		const PropIdx_t 	pIdx,
		const eGridType 	gType,
		const bool 		isExt);


	/**
	 * \brief	This function is provided to ensure that a certain
	 * 		 mapping is pressent.
	 *
	 * This function is similar to addMapping() function. If the mapping
	 * is not pressent it will simply add it and return. However if the
	 * mapping is pressent this function checks if the grid is the same.
	 * If this is the case, then it will return. But in the case the
	 * grids are different, this function will generate an error.
	 *
	 * \param  pIdx		The property index.
	 * \param  gType 	The grid type.
	 */
	egd_propToGridMap_t&
	ensureMapping(
		const PropIdx_t 	pIdx,
		const eGridType 	gType);


	/**
	 * \brief	Thsi function ensures that a certain
	 * 		 mapping is pressent.
	 *
	 * This function is like the other function of the
	 * same name with the difference that this function
	 * allows to specify if the grid is extended or not
	 *
	 * \param  pIdx	    The property that should be recorded.
	 * \param  gType    The grid type that is being used.
	 * \param  isExt    Is the new grid extended.
	 */
	egd_propToGridMap_t&
	ensureMapping(
		const PropIdx_t 	pIdx,
		const eGridType 	gType,
		const bool 		isExt);


	/**
	 * \brief	This function performs safe merge operations.
	 *
	 * This function will call ensure mapping on each mapping
	 * that is storred inside source.
	 *
	 * \param  src		The source of the adding.
	 */
	egd_propToGridMap_t&
	saveAdding(
		const egd_propToGridMap_t& 	src);


	/**
	 * \brief	This function returns a reference to the underlying grid.
	 *
	 * Note that this function is considered dangerous, since it does not
	 * check if the grid is valid or not. It is provided for compability.
	 * Depending on the state of EGD_PROP_TO_GRID_MAP_BRACKET, this function
	 * throws or does what you want.
	 *
	 * \param  pIdx		The grid property.
	 */
	ChkAssignment_t
	operator[](
		const PropIdx_t 	pIdx)
	{
		//Test if pIdx is valid
		if(pIdx.isValid() == false)
		{
			throw PGL_EXCEPT_InvArg("The passed property index was invalid.");
		};

#		if !(defined(EGD_PROP_TO_GRID_MAP_BRACKET) && (EGD_PROP_TO_GRID_MAP_BRACKET == 1))
		throw PGL_EXCEPT_illMethod("The method operator[](pIdx) is not implemeneted.");
#		endif

		return ChkAssignment_t(m_map[pIdx], [](eGridType& lhs, const eGridType& rhs) -> bool
				{  if(::egd::isValidGridType(rhs) == false) {return false;};	//Check
				   lhs = rhs;   return true;					//Perform assigment
				}  );
	}; //End: operator[]




	/*
	 * ==========================
	 * Accessing Functions
	 *
	 * These functions allows to access the mapping.
	 */
public:
	/**
	 * \brief	returns the grid type that pIdx maps to.
	 *
	 * This function basically allows to access the map.
	 * It requieres that pIdx is inside *this.
	 *
	 * \param  pIdx		The property index to query.
	 */
	eGridType
	getGridOf(
		const PropIdx_t 	pIdx)
	 const;


	/**
	 * \brief	This function is an alias of getGridOf().
	 *
	 * It is provided for compability with the map.
	 *
	 * \param  pIdx		The property index to query.
	 */
	eGridType
	at(
		const PropIdx_t 	pIdx)
	 const
	{
		return this->getGridOf(pIdx);
	};



	/*
	 * ==========================
	 * Iterator functions
	 *
	 * These are the iterator functions.
	 * It allows iteration over the properties.
	 * Note that the position is not included.
	 */
public:
	/**
	 * \brief	Return an iterator to the begining of the property range.
	 */
	iterator
	begin()
	 noexcept
	{
		return m_map.begin();
	};


	/**
	 * \brief	Return a constant iterator to the begining of the property range.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return m_map.cbegin();
	};


	/**
	 * \brief	Return a constant iterator to the begining of the property range.
	 * 		 Explicit version.
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		return m_map.cbegin();
	};


	/**
	 * \brief	Returns the past the end iterator of the property range.
	 */
	iterator
	end()
	 noexcept
	{
		return m_map.end();
	};


	/**
	 * \brief	Returns the constant past the end iterator of the property range.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return m_map.cend();
	};


	/**
	 * \brief	Returns the constant past the end iterator of the property range.
	 * 		 Explicit version.
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		return m_map.cend();
	}



	/*
	 * ===================
	 * Private memeber
	 */
private:
	BaseMap_t 	m_map;		//!< This is the underlying map
}; //End: class(egd_propToGridMap_t)

PGL_NS_END(egd)

