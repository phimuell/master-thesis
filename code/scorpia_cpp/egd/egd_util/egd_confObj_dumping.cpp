/**
 * \brief	This file contains the functions of the configuration file that are related
 * 		 to the dumping of markers.
 */

//Include EGD
#include <egd_core/egd_core.hpp>

#include "./egd_confObj.hpp"
#include "./egd_parserUtil.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>



//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_string.hpp>
#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>
#include <limits>


PGL_NS_START(egd)

using pgl::isValidFloat;


std::string
egd_confObj_t::getDumpFile()
 const
{
	//Load the dump file
	const std::string s = m_ini.Get("Misc", "hdf5_file", "");

	if(s.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("No Dumpfile was given.");
	};

	return s;
}; //End: getDumpFile


egd_confObj_t::Numeric_t
egd_confObj_t::getMinimalDumpIntervalTime()
 const
{
	//load the value as string such that we can parse it
	const std::string dI_s = m_ini.Get("Misc", "MinDumpTimeInterval", "");

	if(dI_s.empty() )	//Not specified, so large negative number
	{
		return Numeric_t(-9999999);
	}; //End if:

	//now parse the time
	const Numeric_t dI = egd_parseTime(dI_s);

	//Check value for consistency, note that we will accept negative values
	//to express no time seperation requiered, it is not so usefull.
	if(isValidFloat(dI) == false)
	{
		throw PGL_EXCEPT_InvArg("The value for the minimal time that seperates dumps is invalid.");
	};

	return dI;
}; //End: minimal dump time


bool
egd_confObj_t::useTimeBasedDumping()
 const
{
	return m_ini.GetBoolean("Misc", "TimeBasedDumping", false);
}; //End: useTimeBasedDumping


uInt_t
egd_confObj_t::getDumpingStepInterval()
 const
{
	//load the value, we pass zero as defaults
	const auto dI = m_ini.GetInteger("Misc", "DumpingStepInterval", 0);	//Is signed

	if(dI < 0)
	{
		throw PGL_EXCEPT_InvArg("Specified a negative number of steps between the dumping.");
	};

	return uInt_t(dI);	//Note overflow could occure, but WHAT THE HELL ARE YOU DOING????
}; //End: dump step interval


bool
egd_confObj_t::useStepBasedDumping()
 const
{
	return m_ini.GetBoolean("Misc", "StepBasedDumping", true);
};


bool
egd_confObj_t::typeOnlyFirst()
 const
{
	return m_ini.GetBoolean("Misc", "typeOnlyFirst", false);
};


bool
egd_confObj_t::useDensityStabilization()
 const
{
	return m_ini.GetBoolean("Misc", "RhoStab", true);
};


bool
egd_confObj_t::onlyFullHeatingTerm()
 const
{
	return m_ini.GetBoolean("Misc", "WriteOnlyFullHeatTerm", false);
};


bool
egd_confObj_t::noStrainRateDump()
 const
{
	return m_ini.GetBoolean("Misc", "NoStrainRateDump", false);
};


bool
egd_confObj_t::noStressDump()
 const
{
	return m_ini.GetBoolean("Misc", "NoStressDump", false);
};


bool
egd_confObj_t::dumpOnlyRep()
 const
{
	return m_ini.GetBoolean("Misc", "DumpOnlyRep", false);
};


bool
egd_confObj_t::noTempSolDump()
 const
{
	return !m_ini.GetBoolean("TemperatureSolver", "dumpSol", true);
};


egd_confObj_t::Numeric_t
egd_confObj_t::markerDumpProb()
 const
{
	const Numeric_t f = m_ini.GetReal("Misc", "MarkerSelectionProb", 1.0);
	if(::pgl::isValidFloat(f) == false)
	{
		throw PGL_EXCEPT_InvArg("The value read as dumping probability is no real value.");
	};
	if(f <= 0)
	{
		throw PGL_EXCEPT_InvArg("The dump probability was negative, it was " + std::to_string(f));
	};

	return f;
};//End: get marker selection probably


bool
egd_confObj_t::dumpFeelVel()
 const
{
	return m_ini.GetBoolean("Misc", "DumpFeelVel", true);
};


bool
egd_confObj_t::dumpMarkerVel()
 const
{
	return m_ini.GetBoolean("Misc", "DumpMarkerVel", false);
}; //End: dumpMarkerVel


PGL_NS_END(egd)


