/**
 * \brief	This file contains teh functions to read the parameters
 * 		 that are related to the grid, marker and geometry
 * 		 of the simulation.
 */

//Include EGD
#include <egd_core/egd_core.hpp>
#include "./egd_confObj.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_string.hpp>
#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)

using pgl::isValidFloat;

/*
 * ========================
 * Geometry, Grid and Marker
 */
Index_t
egd_confObj_t::getNx()
 const
{
	const Index_t Nx = m_ini.GetInteger("Domain", "Nx", 0);

	if(Nx <= 0)
	{
		throw PGL_EXCEPT_InvArg("The number of gridpoints in x direction, " + std::to_string(Nx) + ", is invalid.");
	};

	return Nx;
}; //End: getNx


Index_t
egd_confObj_t::getNy()
 const
{
	const Index_t Ny = m_ini.GetInteger("Domain", "Ny", 0);

	if(Ny <= 0)
	{
		throw PGL_EXCEPT_InvArg("The number of gridpoints in y direction, " + std::to_string(Ny) + ", is invalid.");
	};

	return Ny;
}; //End: getNx


uSize_t
egd_confObj_t::getMarkerDensityX()
 const
{
	const auto markRhoX = m_ini.GetInteger("Domain", "nMarkerX", 0);

	if(markRhoX <= 0)
	{
		throw PGL_EXCEPT_InvArg("The marker density for the x direction that was sepcified, " + std::to_string(markRhoX) + ", is invalid.");
	};

	return markRhoX;
}; //End: marker density X


uSize_t
egd_confObj_t::getMarkerDensityY()
 const
{
	const auto markRhoY = m_ini.GetInteger("Domain", "nMarkerY", 0);

	if(markRhoY <= 0)
	{
		throw PGL_EXCEPT_InvArg("The marker density for the y direction that was sepcified, " + std::to_string(markRhoY) + ", is invalid.");
	};

	return markRhoY;
}; //End: marker density Y


bool
egd_confObj_t::randomlyDisturbeMarkers()
 const
{
	return m_ini.GetBoolean("Domain", "MarkerRandDisplace", false);
};


egd_confObj_t::Numeric_t
egd_confObj_t::domainLengthY(
	const Numeric_t 	defValue)
 const
{
	if(m_ini.Get("Domain", "yLength_m").empty() == false)
	{
		//We found an empyt value so we look for the meter value
		const Numeric_t yLengthM = m_ini.GetReal("Domain", "yLength_m", -2.0);

		//Meter is only considered if found otherwise the KM is used
		if(yLengthM > 0.0)
		{
			return yLengthM;
		};
	};//End if: hasKM

	const Numeric_t yLengthKM = m_ini.GetReal("Domain", "yLength_km", defValue);

	if(yLengthKM <= 0.0)
	{
		throw PGL_EXCEPT_RUNTIME("No length for y specified, read " + std::to_string(yLengthKM) + ", default value was " + std::to_string(defValue));
	};

	return (yLengthKM * 1000.0);
}; //End: domainLengthY


egd_confObj_t::Numeric_t
egd_confObj_t::domainLengthX(
	const Numeric_t 	defValue)
 const
{
	if(m_ini.Get("Domain", "xLength_m").empty() == false)
	{
		//We found an empyt value so we look for the meter value
		const Numeric_t xLengthM = m_ini.GetReal("Domain", "xLength_m", -2.0);

		//Meter is only considered if found otherwise the KM is used
		if(xLengthM > 0.0)
		{
			return xLengthM;
		};
	};//End if: hasKM

	//Read the KM distance, this many be empty.
	const Numeric_t xLengthKM = m_ini.GetReal("Domain", "xLength_km", defValue);

	if(xLengthKM <= 0.0)
	{
		throw PGL_EXCEPT_RUNTIME("No length for x specified, read " + std::to_string(xLengthKM) + ", default value was " + std::to_string(defValue));
	};

	return (xLengthKM * 1000.0);
}; //End: domainLengthX


PGL_NS_END(egd)


