/**
 * \brief	This file contains a signal handler managing utility.
 *
 * It allows to catch several signals and set a boolean instead.
 * Note that this is different from the abilities that are offered by GUCKY.
 * So it only sets a flag and exits then.
 */

//Include the confg file
#include <egd_core.hpp>

#include "./egd_signalHandler.hpp"


//Include PGL
#include <pgl_core.hpp>


//Include the Eigen Headers


//Include STD
#include <atomic>
#include <csignal>


/**
 * \brief	This is the acctual signal handler.
 *
 * It is called by the implementation.
 * It does not performs the dump, but inisiates the dumping.
 * It does also sets the signal to ignore.
 */
extern "C"
{
static
void
egdInternal_SignalHandler(int sig)
{
	//Ignore the siganle
	std::signal(sig, egdInternal_SignalHandler);

	//Set the flag to ture
	::egd::internal::glob_flagWasSet.store(true, ::std::memory_order_seq_cst);

	return;
};//ENd: handler
};//End: extern



PGL_NS_START(egd)

/*
 * \brief	This namespace contains all the variables, that are needed for it.
 */
PGL_NS_START(internal)

std::atomic<bool> 	glob_flagWasSet;

PGL_NS_END(internal)



void
egd_startSignalHandling()
{
	//Set the flag to zero, ignore its status
	internal::glob_flagWasSet.store(false, ::std::memory_order_seq_cst);

	//Install all the handlers
	const int sigs[] = {SIGTERM, SIGINT, SIGUSR1, SIGUSR2, SIGTSTP, SIGQUIT};

	//Go through the signals and install the handler
	for(const int sig : sigs)
	{
		::std::signal(sig, egdInternal_SignalHandler);
	}; //End for(sig):

	return;
}; //End: start handling function


bool
egd_signalWasRaised()
{
	return internal::glob_flagWasSet.load(::std::memory_order_seq_cst);
};//End: was raised


PGL_NS_END(egd)

