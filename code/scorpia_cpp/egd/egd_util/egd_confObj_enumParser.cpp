/**
 * \brief	This file contains teh functions that convert strings
 * 		 to enum that are used in the configuration object.
 */

//Include EGD
#include <egd_core/egd_core.hpp>
#include "./egd_confObj.hpp"


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>
#include <pgl/pgl_string.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


eRandKind
egd_confObj_t::Rand_parseKind(
	const std::string& 	s_)
{
	//Make lower case
	const std::string s = pgl::pgl_strToLower(s_);

	if(s == "dirichlet" || s == "d")
	{
		return eRandKind::Dirichlet;
	}
	else if(s == "freeslip" || s == "f")
	{
		return eRandKind::freeSlip;
	}
	else if(s == "insulation" || s == "vonneumann")
	{
		return eRandKind::Insulation;
	}

	throw PGL_EXCEPT_InvArg("The token \"" + s_ + "\" (" + s + ") can not be transformed into a boundary type.");
}; //End: parse Kind

std::string
egd_confObj_t::Rand_getLocStr(
	const eRandLoc 		loc)
{
#define CASE(l, s) case l : return std::string(s); break
	switch(loc)
	{
	  CASE(eRandLoc::TOP,    "top");
	  CASE(eRandLoc::BOTTOM, "bot");
	  CASE(eRandLoc::LEFT,   "lef");
	  CASE(eRandLoc::RIGHT,  "rig");

	  //internal
	  CASE(eRandLoc::INT_LOWSHEAR, "lowshear");
	  CASE(eRandLoc::INT_UPSHEAR , "upshear" );

	  case eRandLoc::PRESS:
		throw PGL_EXCEPT_InvArg("Tried to get the location string of the pressure location. But this is only a virtual location, this is an error in your handling.");
	  break;

	  default:
		throw PGL_EXCEPT_InvArg("The location enum is invalid.");
		break;
	}; //End switch(prop):
#undef CASE
}; //End: getLocStr


std::string
egd_confObj_t::Rand_getPropStr(
	const eRandProp 	prop)
{
#define CASE(l, s) case l : return std::string(s); break
	switch(prop)
	{
	  CASE(eRandProp::VelX,     "vel_x"   );
	  CASE(eRandProp::VelY,     "vel_y"   );
	  CASE(eRandProp::Pressure, "pressure");
	  CASE(eRandProp::Temp,     "temp"    );

	  default:
		throw PGL_EXCEPT_InvArg("Property unkown.");
		break;
	}; //End switch(prop)
#undef CASE
}; //End: getPropStr


std::string
egd_confObj_t::Rand_getStectionStr(
	const eRandProp 	prop)
{
#define CASE(l, s) case l : return std::string(s); break
	switch(prop)
	{
	  CASE(eRandProp::VelX,     "StokesBoundary"     );
	  CASE(eRandProp::VelY,     "StokesBoundary"     );
	  CASE(eRandProp::Pressure, "StokesBoundary"     );
	  CASE(eRandProp::Temp,     "TemperatureBoundary");

	  default:
		throw PGL_EXCEPT_InvArg("Invalid property.");
	}; //End switch
#undef CASE
}; //End: get Section


PropIdx
egd_confObj_t::Rand_toPropIdx(
	const eRandProp 	prop)
{
	switch(prop)
	{
	  case eRandProp::VelX:
	  	return PropIdx::VelX().getRepresentant();
	  break;

	  case eRandProp::VelY:
		return PropIdx::VelY().getRepresentant();
	  break;

	  case eRandProp::Temp:
	  	return PropIdx::Temperature().getRepresentant();
	  break;

	  case eRandProp::Pressure:
	  	return PropIdx::Pressure().getRepresentant();
	  break;

	  default:
	  	throw PGL_EXCEPT_InvArg("Passed an invalid value.");
	  break;
	};

	return eMarkerProp::NONE;	//Will lead to an exception
}; //End: toPropIdx


std::string
printRandProp(
	const eRandProp 	prop)
{
	switch(prop)
	{
	  case eRandProp::VelX:     return std::string("VelX"    );
	  case eRandProp::VelY:     return std::string("VelY"    );
	  case eRandProp::Temp:     return std::string("Temp"    );
	  case eRandProp::Pressure: return std::string("Pressure");

	  default:
		throw PGL_EXCEPT_InvArg("Encountered an invalid eRandProp enum, its numeric value was "
				+ std::to_string(static_cast<sSize_t>(prop)));
	}; //End switch(prop)

	pgl_assert(false && "Reached unreachable code.");
	return std::string("");
}; //End: print rand property

PGL_NS_END(egd)


