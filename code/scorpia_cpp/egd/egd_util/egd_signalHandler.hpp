#pragma once
/**
 * \brief	This file contains a signal handler managing utility.
 *
 * It allows to catch several signals and set a boolean instead.
 * Note that this is different from the abilities that are offered by GUCKY.
 * So it only sets a flag and exits then.
 */

//Include the confg file
#include <egd_core.hpp>


//Include PGL
#include <pgl_core.hpp>


//Include the Eigen Headers


//Include STD
#include <atomic>


PGL_NS_START(egd)

/*
 * \brief	This namespace contains all the variables, that are needed for it.
 */
PGL_NS_START(internal)

/**
 * \brief	This flag is used to store if the flag was raised.
 * \var 	glob_flagWasSet
 *
 * This flag is set by the interrupt routine. No not access this
 * flag directly use the provided testing routines
 */
extern std::atomic<bool> 	glob_flagWasSet;

PGL_NS_END(internal)


/**
 * \brief	This function will start the monitoring process.
 *
 * It will set the global state to false, which indicates that
 * the interrupt has not happened yet, note that the state is
 * ignored. It will also register handlers for the managed flags.
 */
void
egd_startSignalHandling();


/**
 * \brief	This function returns true if a signal was detected.
 *
 * This function can be used to detect if the signal was raised.
 * It is the safe way of testing the state of the flag variable.
 */
bool
egd_signalWasRaised();


PGL_NS_END(egd)

