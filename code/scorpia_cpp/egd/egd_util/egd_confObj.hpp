#pragma once
/**
 * \brief	This file implements a configuration object for the program.
 *
 * This object is used by all parts of the code for configuration.
 * It is the single sincronyzation point.
 */

//Include EGD
#include <egd_core/egd_core.hpp>

//Needed for the other enums
#include <egd_phys/egd_boundaryCondition.hpp>

//Needed for the job selection
#include <egd_impl/grid_to_marker/egd_mappingJobType.hpp>


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_INIreader/pgl_INIReader.hpp>
#include <pgl_vector.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)


/*
 * N O T E
 * In previous versions of this header three enums where here that
 * were used to encode positions and boundaries and so on.
 * They were moved to the boundary condition file in the egd_phys folder.
 * One remained here, becuase it was not needed there.
 */


/**
 * \brief	This is used to encode the quantity we want to controll.
 * \enum 	eRandProp
 *
 * This is used to encode what we want to restrict in the boundary.
 * Such as the velocity, pressure temperature and so on.
 *
 * \note	The virtual property BEGIN, must be the first one
 * 		 and ENDE must be the last one. They are used internally.
 * 		 Also the numbering must be done automatically.
 */
enum class eRandProp
{
	BEGIN,
	VelX,		//!< Velocity in x direction.
	VelY,		//!< Velocity in y direction.
	Pressure,	//!< Presure
	Temp, 		//!< Temperature
	ENDE,
}; //End enum(eRandWhat)


/**
 * \brief	This function allows to print the rand properties.
 *
 * Note that this function is only used for debugging output.
 * There are a set of overload functions that allows to access this
 * function by operator+ involvin std::strings.
 *
 * \param  prop		This is the property.
 */
extern
std::string
printRandProp(
	const eRandProp 	prop);



/**
 * \brief	This class implements a configuration file for egd.
 * \class 	egd_confObj_t
 *
 * This configuration file is used, as the name says, to configure EGD.
 * It us used to select which implementation should be build.
 * It is also used to link different part of the codes, for example
 * it stores information that are needed at several location.
 * So hardcoding can be avoided.
 *
 * The class is basically a warper around an Ini parser object
 * offered by pgl. However some helper functions are provided
 * to perform some commenly tasks.
 *
 * Note that this class is a work in progress.
 */
class egd_confObj_t
{
	/*
	 * ======================
	 * Typedefs
	 */
public:
	using Size_t 		= ::pgl::Index_t;		//!< This is the size type of *this. Note that it is not pgl::Size_t.
								//!<  but the pgl::Index_t type, the reason is because Eigen uses them and we use Eigen.
	using Index_t 		= ::pgl::Index_t;		//!< This is the index type we use, this is an alias of pgl::Index_t.
	using Numeric_t 	= ::pgl::Numeric_t;		//!< This is the float type that is used

	using Path_t 		= ::boost::filesystem::path;	//!< The path object.

	using IniReader_t	= ::pgl::pgl_INIReader_t;	//!< This is the ini reader.

	using BoundCondition_t  = egd_boundaryConditions_t;	//!< This is the boundary condition object.

	using ePropList_t 	= ::pgl::pgl_vector_t<eRandProp>;	//!< This is a lsit of all boundaries that are known.


	/*
	 * ========================
	 * Constructor
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This constructor is the only way to construct
	 * an configuration object. As argument it takes
	 * a path to an ini file. This file is parsed.
	 *
	 * \param  filePath 	File path to the ini file.
	 */
	egd_confObj_t(
		const Path_t& 		filePath);


	/**
	 * \brief	Copy constructor.
	 *
	 * Is defaulted.
	 */
	egd_confObj_t(
		const egd_confObj_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * Is defaulted.
	 */
	egd_confObj_t&
	operator= (
		const egd_confObj_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	egd_confObj_t(
		egd_confObj_t&&)
	 = default;


	/**
	 * \brief	Move assignment.
	 *
	 * Is defaulted.
	 */
	egd_confObj_t&
	operator= (
		egd_confObj_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~egd_confObj_t()
	 = default;


	/*
	 * ===========================
	 * INI-Accesser
	 */
public:
	/**
	 * \brief	This function returns the ini reader
	 * 		 object that is associated with *this.
	 *
	 * This function returns a constant reference to the
	 * internal ini reader, this allows direct manipulations
	 *
	 * The access is guarded with an assert that test if the
	 * reader has any keys.
	 */
	const IniReader_t&
	getINI()
	 const
	 noexcept
	{
		pgl_assert(m_ini.hasKeys());
		return m_ini;
	};


	/**
	 * \brief	This function allows a conversion
	 * 		 to an ini reader object.
	 */
	operator const IniReader_t& ()
	 const
	 noexcept
	{
		return this->getINI();
	};


	/*
	 * ========================
	 * INI Short Hand
	 *
	 * These functions allows to access some aspect of
	 * the configuration file directly.
	 */
public:
	/**
	 * \brief	This function retuens a complete boundary.
	 *
	 * This function queries the ini file and builds a full
	 * boundary condition for a specific property.
	 *
	 * \param  prop		The property that we want.
	 */
	BoundCondition_t
	getBC(
		const eRandProp 	prop)
	 const;


	/**
	 * \brief	This function returns true if *this has a boundary
	 * 		 for the given property.
	 *
	 * This function operates in a very crude way. it will check if any
	 * condition exists for said boundary.
	 *
	 * \param  prop		For which boundary we want to make the test.
	 */
	bool
	hasBC(
		const eRandProp 	prop)
	 const;


	/**
	 * \brief	This function returns a list of all known
	 * 		 boundaries that are set.
	 *
	 * This file returns a list that constains all eRandProp
	 * values that are specified inside the configuration file.
	 * This is hasBC() returns true for them.
	 */
	ePropList_t
	getAllProps()
	 const;


	/**
	 * \brief	This function returns the type of the condition
	 * 		 in the specified location.
	 *
	 * This function reads the configuration file for the condition to apply.
	 * The location and property that is requested is encoded with enums.
	 * If the requested boundary type does not exists an error will be
	 * generated. If this is not desired, use the other version of this
	 * function.
	 *
	 * \param  prop 	The property that should be read.
	 * \param  loc 		Where the boundary condition is quirried.
	 */
	eRandKind
	getBoundaryType(
		const eRandProp 	prop,
		const eRandLoc 		loc)
	 const;


	/**
	 * \brief	This function is able to read the type of the specified
	 * 		 condition, but can handle "not pressent" cases.
	 *
	 * This function will return true, if the request was able to succeeed.
	 * If the value was not found, then false is returned. Note that error
	 * will still generate an exception.
	 * This function will return the kind by its third argument, this value
	 * is only modified if true was returned.
	 *
	 * The optional fourth argument, allows to load the key value that was
	 * used, it is written as soon as it is computed. Intendet for error
	 * messages.
	 *
	 * \param  prop 	The property that should be read.
	 * \param  loc 		Where the boundary condition is quirried.
	 * \param  kind		The kind that was found.
	 * \param  keyToken	The key that was used too look up the file.
	 */
	bool
	getBoundaryType(
		const eRandProp 		prop,
		const eRandLoc 			loc,
		      eRandKind* const 		kind,
		      std::string* const 	keyToken = nullptr)
	 const;


	/**
	 * \brief	Returns the dirichlet value for the specified location.
	 *
	 * The location and property that is requested is encoded with enums.
	 *
	 * \param  prop		The property that is quried.
	 * \param  loc		The location of the boundary.
	 *
	 * \throw 	If the condition specified is not a dirichlet condition.
	 */
	Numeric_t
	getDirichletValue(
		const eRandProp 	prop,
		const eRandLoc 		loc)
	 const;


	/*
	 * ===================
	 * Grid, Marker and Geometry
	 */
public:
	/**
	 * \brief	This function returns the number
	 * 		 of grid points in x direction.
	 *
	 * This is the number of basic nodal grid points.
	 */
	Index_t
	getNx()
	 const;


	/**
	 * \brief	Returns the number of basic grid points
	 * 		 in y direction.
	 */
	Index_t
	getNy()
	 const;


	/**
	 * \brief	This returns the marker density per cell in x direction.
	 */
	uSize_t
	getMarkerDensityX()
	 const;


	/**
	 * \brief	Get the marker density per cell in y direction .
	 */
	uSize_t
	getMarkerDensityY()
	 const;


	/**
	 * \brief	This function returns true, if markers should be
	 * 		 randomly displaced.
	 *
	 * This is usefull to counter some anomalies, but is bad
	 * for debuging. How the displacement is done is left to
	 * the implementation.
	 *
	 * \note 	If the key "MarkerRandDisplace" is not specified
	 * 		 in the "Domain" section of the config file,
	 * 		 false is returned.
	 */
	bool
	randomlyDisturbeMarkers()
	 const;


	/**
	 * \brief	This function returns if boundary stabilization was requested or not.
	 *
	 * This function will query section "Misc:RhoStab" in the configuration file.
	 * If this is not found true is returned.
	 */
	bool
	useDensityStabilization()
	 const;


	/**
	 * \brief	This function returns the extension of the domain in x.
	 *
	 * This function will query Domain:xLength_km parameter in the file.
	 * If the parameter is not pressent and no default is given, then
	 * an error is generated.
	 * Note that this function will read in the value in kilo meters,
	 * but return it in meters.
	 *
	 * Note that if this value is not found the _m value is queried.
	 * This is the length specified in meters. Meter takes precedence.
	 * If the value was found in the file, otherwise the KM mode is
	 * resumed.
	 *
	 * \param  defValue	The default value that should be used, in KM.
	 */
	Numeric_t
	domainLengthX(
		const Numeric_t 	defValue = -1.0)
	 const;


	/**
	 * \brief	This function returns the extension of the domain in y.
	 *
	 * This function will query Domain:yLength_km parameter in the file.
	 * If the parameter is not pressent and no default is given, then
	 * an error is generated.
	 * Note that this function will read in the value in kilo meters,
	 * but return it in meters.
	 *
	 * Note that if this value is not found the _m value is queried.
	 * This is the length specified in meters. Meter takes precedence.
	 * If the value was found in the file, otherwise the KM mode is
	 * resumed.
	 *
	 * \param  defValue	The default value that should be used, in KM.
	 */
	Numeric_t
	domainLengthY(
		const Numeric_t 	defValue = -1.0)
	 const;


	/**
	 * \brief	This function returns the type of the mapping that
	 * 		 the grid to marker should use.
	 *
	 * Note that this may not be used by the concrete marker.
	 * It is also possible to specify a default that is used if not found.
	 * This function will read the key "GridToMarker:JobType" key.
	 * Note by anm optional bool you can get if it was found or not.
	 *
	 * \param  defType	The default job that should be used.
	 * \param  wasFound	If given will be set to false if it was found.
	 */
	eJobType
	getGtoMJobType(
		const eJobType 		defType,
		bool* const 		wasFound = nullptr)
	 const;


	/*
	 * ======================
	 * SetUp specialization
	 *
	 * These functions allows to read in the setup section.
	 * Which can contain model parameters. They should be
	 * seen as convenient wrapper arround the ini class.
	 */
public:
	/**
	 * \brief	This function reads in the model parameter par.
	 *
	 * This function will return the value of the key "SetUp:par"
	 * and return the value as Numeric_t.
	 * If the value is not found, or NAN an error is generated.
	 *
	 * \param  par	The name of the key that should be loaded.
	 */
	Numeric_t
	setUpPar(
		const std::string& 	par)
	 const;


	/**
	 * \brief	This function is a special version of the setUpPar function,
	 * 		 that returns NAN if the requested value is not found.
	 *
	 * This function has certain, very limited applications, and should be
	 * used with care.
	 *
	 * \param  par	The name of the key that should be loaded.
	 */
	Numeric_t
	setUpParNAN(
		const std::string& 	par)
	 const;


	/**
	 * \brief	This function is the same as setUpPar(), but
	 * 		 returns an integer.
	 *
	 * \param  par	The name of the parameter that should be read in.
	 */
	Index_t
	setUpParInt(
		const std::string& 	par)
	 const;


	/**
	 * \brief	This function is like setUpPar(), but will return
	 * 		 the second argument if par is not found.
	 *
	 * \param  par	The name of the parameter to look for.
	 * \param  d	Second argument.
	 */
	Numeric_t
	setUpPar(
		const std::string& 	par,
		const Numeric_t& 	d)
	 const;


	/**
	 * \brief	This function is like setUpPar(), but will return
	 * 		 the second argument if par is not found.
	 *
	 * This is the integer version.
	 *
	 * \param  par	The name of the parameter to look for.
	 * \param  d	Second argument.
	 */
	Index_t
	setUpPar(
		const std::string& 	par,
		const Index_t& 		d)
	 const;


	/**
	 * \brief	This function is like setUpPar(), but will return
	 * 		 the second argument if par is not found.
	 *
	 * Convenient function for name sake.
	 *
	 * \param  par	The name of the parameter to look for.
	 * \param  d	Second argument.
	 */
	Index_t
	setUpParInt(
		const std::string& 	par,
		const Index_t& 		d)
	 const
		{return this->setUpPar(par, d);};



	/**
	 * \brief	This function will return the gravity accelartion in y direction.
	 *
	 * Note that this will account for the coordinate system, meaning a positive value
	 * means that the force is pulling downwartds.
	 * In the end this unction reads "SetUp:gravY" and returns the value.
	 * if the key is not pressent, it will returns (9.81).
	 * The value is expressed in meter per second square.
	 */
	Numeric_t
	setUpGravY()
	 const;


	/**
	 * \brief	This function will return the gravity in y direction.
	 * 		 But this one allows a defautl value.
	 *
	 * This function is the same as the other version, without arguments.
	 * This one allows to specifies the gravity that is used if the key
	 * is not pressent.
	 *
	 * \param  d	Default value.
	 */
	Numeric_t
	setUpGravY(
		const Numeric_t&	d)
	 const;



	/*
	 * ==========================
	 * Dumping Controll
	 *
	 * This section contains teh functions that
	 * deals with the dumping controll
	 */
public:
	/**
	 * \brief	This function returns the file path to the file
	 * 		 (HDF5) that is created by the dunmer.
	 */
	std::string
	getDumpFile()
	 const;


	/**
	 * \brief	This function returns the minimal duration between two dumps.
	 *
	 * The returned value is a time in seconds that can be used to express the
	 * minimal times between two dumps. Note that this is not enforced by the
	 * dumper, but must be done on calling code. This will basically limit
	 * the number of dumps that are performed.
	 *
	 * This function reads in the "Misc:MinDumpTimeInterval" key in the
	 * ini file. If this value is not specified a very large negative
	 * number will be returned, such that every time will be accepted.
	 */
	Numeric_t
	getMinimalDumpIntervalTime()
	 const;


	/**
	 * \brief	This function can be used to check if time based dumping
	 * 		 is disabled.
	 *
	 * This function returns true if this is the case. In the end it will
	 * read the "Misc:TimeBasedDumping" key. If this key is not specified,
	 * false will be returned.
	 */
	bool
	useTimeBasedDumping()
	 const;


	/**
	 * \brief	This function returns the number of steps that should be
	 * 		 performed between two dumps.
	 *
	 * Also note that this is not enforced by the dumper itself but by calling
	 * code. This method is an adaptive version of the fixed time above.
	 *
	 * This function will read in the "Misc:DumpingStepInterval" key in the
	 * ini file. If the key is not present, zero will be returned.
	 */
	uInt_t
	getDumpingStepInterval()
	 const;


	/**
	 * \brief	This function returns true if step based dumping is enabled.
	 *
	 * This function will read the "Misc:StepBasedDumping" key in the ini file.
	 * If this key does not exist it will return false.
	 */
	bool
	useStepBasedDumping()
	 const;


	/**
	 * \brief	Returns true if the type information on the markers should
	 * 		 only be stored in the first iteration.
	 *
	 * If not specified the default "false" isused.
	 */
	bool
	typeOnlyFirst()
	 const;


	/**
	 * \brief	This function returns true if the ini file requests that only
	 * 		 the full heating term to be dumped, the components will be ignored.
	 *
	 * This function checks for "Misc:WriteOnlyFullHeatTerm", the default is false.
	 */
	bool
	onlyFullHeatingTerm()
	 const;


	/**
	 * \brief	This function returns true if strain rate should not be dumped.
	 *
	 * This function check Misc:NoStrainRateDump", default is false.
	 */
	bool
	noStrainRateDump()
	 const;


	/**
	 * \brief	This function returns true if strain rate should not be dumped.
	 *
	 * This function check Misc:NoStressDump", default is false.
	 */
	bool
	noStressDump()
	 const;


	/**
	 * \brief	This returns true if only representants should be dumped.
	 *
	 * This function checks "Misc:DumpOnlyRep", the default is false.
	 */
	bool
	dumpOnlyRep()
	 const;


	/**
	 * \brief	Returns true if the temperature solution should not be dumped.
	 *
	 * This function will read the key "TemperatureSolver:dumpSol", the default is yes.
	 * Also false.
	 */
	bool
	noTempSolDump()
	 const;


	/**
	 * \brief	Returns the probability for dumping a marker.
	 *
	 * This value is used by the dumper to determine what the probability is
	 * to dump a marker or not.
	 *
	 * This functions reads the "Misc:MarkerSelectionProb".
	 * If not specified 1.0 is returned.
	 */
	Numeric_t
	markerDumpProb()
	 const;


	/**
	 * \brief	This function returns true if the feel velocity
	 * 		 should be dumped.
	 *
	 * This function will read "Misc:DumpFeelVel" parameter.
	 * If the property is not found it will return true.
	 *
	 * \note	Before this property was introduces the feel
	 * 		 Vel did not existed. See also the dumpMarkerVel()
	 * 		 function.
	 */
	bool
	dumpFeelVel()
	 const;


	/**
	 * \brief	This function returns true if marker
	 * 		 velocity should be dumped.
	 *
	 * This function reads the state of "Misc:DumpMarkerVel".
	 * If the property was not found then the function will
	 * return false.
	 * Note that this is a different behaviour from the old
	 * one were the marker velocity was always dumped.
	 * Insped use the feel velocity.
	 */
	bool
	dumpMarkerVel()
	 const;




	/*
	 * ======================
	 * Timestepping Method
	 *
	 * All these functions operate on values that are defined in
	 * the TimeStepping section. The name is historically.
	 * However this section contains all functions that deals with
	 * the restriction, so also movement.
	 */
public:
	/**
	 * \brief	This function returns teh number of seconds that
	 * 		 should be simulated.
	 *
	 * This is an alternative to select the number of time steps that
	 * should be performed. the value is in second, if it is not found
	 * a negative value is returned.
	 * It will read the "TimeStepping:FinalTime" key.
	 */
	Numeric_t
	getFinalSimTime()
	 const;



	/**
	 * \brief	Returns the number of timesteps that should be performed.
	 *
	 * If not specified or an invalid value is detected, an error is generated.
	 *
	 * \note	Due to historical reasons, this function will examine two
	 * 		 keys in the TimeStepping section. The first one is
	 * 		 "nTimeSteps", if this value is not specified it will look
	 * 		 for the key "nTimeStpes", this is done to enable combatibility.
	 */
	Size_t
	nTimeSteps()
	 const;


	/**
	 * \brief	This function returns the length of the smalles
	 * 		 allowed time step in second.
	 */
	Numeric_t
	getMinTimeStepSize()
	 const;


	/**
	 * \brief	This function returns the length of the largest
	 * 		 allowed time step in second.
	 */
	Numeric_t
	getMaxTimeStepSize()
	 const;


	/**
	 * \brief	This function returns the the fictive -1 time step.
	 *
	 * This function is important to characterize the time that sperates
	 * the initial condition from the very first time step that is
	 * executed.
	 *
	 * If not found the minimal time step is used.
	 */
	Numeric_t
	getInitialInertiaTimeStep()
 	 const;


	/**
	 * \brief	This function returns the number of super
	 * 		 restricted time steps.
	 *
	 * This is the number of time steps, where only the minimum
	 * time step is allowed.
	 */
	Size_t
	nSuperResTimeSteps()
	 const;


	/**
	 * \brief	This function returns the number of steps,
	 * 		 where the timestep is successifly increased.
	 *
	 * Note that the returned number does not include the restricted
	 * time steps!
	 */
	Size_t
	nRampSteps()
	 const;


	/**
	 * \brief	This function returns the maximal movemnt a marker
	 * 		 is allowed to do in x direction, during a time step.
	 *
	 * The value is measured in the grid size in x direction. Meaning
	 * 0.5 means it is allowed to move half a cell or $0.5 * \Delta x$
	 * during one time step. The default is 0.4.
	 * This function will perform checks on the value.
	 *
	 * It reas the maxDisplacementX in the time stepping section.
	 */
	Numeric_t
	getMaxDisplacementX()
	 const;


	/**
	 * \brief	This function returns the maximal movemnt a marker
	 * 		 is allowed to do in y direction, during a time step.
	 *
	 * See the getMaxDisplacementX() function for more information.
	 * If the value is not specified it will forward the call to the
	 * displacement function for the X direction.
	 *
	 * It reas the maxDisplacementY in the time stepping section.
	 */
	Numeric_t
	getMaxDisplacementY()
	 const;


	/*
	 * =====================
	 * Rehology
	 */
public:
	/**
	 * \brief	This function returns true if in the [Rheology] section of the
	 * 		 ini file the key, "imposeVel" was set to true.
	 *
	 * This flag is used by the pulled ball settings. It will instruct the marker
	 * rheology to impose the velocity or not impose the velocity. In order for
	 * backwards compability, if the key was not found, true is returned.
	 */
	bool
	rheologyImposeVel()
	 const;


	/*
	 * =============================
	 * Solver
	 */
public:
	/**
	 * \brief	This function returns true if the solver boundary,
	 * 		 should use all four velocities for prescribing the velocity.
	 *
	 * This value is important for the powered pulled ball settings. In effect
	 * this function will read key "fullCell" in section "StokesSolver" of the ini
	 * file. If this key is not found, false will be returned.
	 *
	 * Note that if true is returned, the pressure equation must be artificially
	 * resolved. For this two ways are supported, that are determined on compile
	 * time.
	 */
	bool
	solverUseFullCell()
	 const;


	/*
	 * =====================
	 * Impl Short Hands
	 */
public:
	/**
	 * \brief	This function returns the identifier for the
	 * 		 implementation of the marker to grid interpolator.
	 *
	 * The returned value is all lower case.
	 */
	std::string
	getMarkerToGridImp()
	 const;


	/**
	 * \brief	This function returns the identifier for the
	 * 		 Stokes solver.
	 *
	 * The returned value is all lower case.
	 */
	std::string
	getStokesSolverImpl()
	 const;


	/**
	 * \brief	This function returns the identifier for the
	 * 		 temperature solver.
	 *
	 * The returned value is all lower case.
	 */
	std::string
	getTemperatureSolverImpl()
	 const;


	/**
	 * \brief	This function returns the identifier for the
	 * 		 Integrator; marker mover.
	 *
	 * The returned value is all lower case.
	 */
	std::string
	getIntegratorImpl()
	 const;


	/**
	 * \brief	This function returns the identifier for the
	 * 		 rheology.
	 *
	 * The returned value is all lower case.
	 */
	std::string
	getRheologyImpl()
	 const;


	/**
	 * \brief	This function returns the identifier for the
	 * 		 grid to marker interpolator.
	 *
	 * The returned value is all lower case.
	 */
	std::string
	getGridToMarkerImpl()
	 const;


	/**
	 * \brief	Thsi function returns the identifier for the
	 * 		 setup.
	 *
	 * The setup is the part of teh code that distributes the markers
	 * on the grid and assignes them the initial values.
	 */
	std::string
	getSetUpImpl()
	 const;


	/**
	 * \brief	This function returns the identifier for the
	 * 		 boundary implementation.
	 *
	 * The boundary object, implements the egd_applyBoundary_i interface.
	 * It is responsible for setting up the boundary to property.
	 * Note that this option, will not affect the boundary used
	 * by the solvers.
	 */
	std::string
	getApplyBC()
	 const;




	/*
	 * =======================
	 * Private Constructors
	 */
private:
	/**
	 * \brief	The default constructor.
	 *
	 * Is private and defaulted.
	 */
	egd_confObj_t()
	 = default;


	/*
	 * =========================
	 * Private Helper Functions
	 */
private:
	/**
	 * \brief	This function parses a string and converts
	 * 		 it into a boundary type.
	 *
	 * This function constructs a eRandKind from a string.
	 * The string is interpreted case insensitive
	 *
	 * \param  s		The string that should be parsed.
	 */
	static
	eRandKind
	Rand_parseKind(
		const std::string& 	s);


	/**
	 * \brief	This function transform the location enum into the token
	 * 		 that is used inside the config file.
	 *
	 * This function allows to reconstruct the key name of a property.
	 *
	 * \param  loc 	The location we need.
	 */
	static
	std::string
	Rand_getLocStr(
		const eRandLoc 		loc);


	/**
	 * \brief	This function transforms a eRandProp enum into the string
	 * 		 that is used to name the property.
	 *
	 * \param  prop 	The property that should be used.
	 */
	static
	std::string
	Rand_getPropStr(
		const eRandProp 	prop);


	/**
	 * \brief	This function returns the section where we have
	 * 		 to look for a propperty.
	 *
	 * \param  prop 	The property we are interested.
	 */
	static
	std::string
	Rand_getStectionStr(
		const eRandProp 	prop);


	/**
	 * \brief	This function translates a eRandProp into an property index.
	 *
	 * \param  prop		The rand property that we want to translate.
	 */
	static
	PropIdx
	Rand_toPropIdx(
		const eRandProp 	prop);


	/**
	 * \brief	This function is used to extract the
	 * 		 string of a bounday type.
	 *
	 * Note that this is an internal function, so it is
	 * quite tricky to use it. First of all this function
	 * can return an empty string, if the condition is not
	 * found. Calling code has to handle this case.
	 *
	 * This function will return teh used key as first and
	 * the read value as second. Note that the value can be
	 * empty. Also the key is prefixed fith the section token
	 * a colon is usedto seperate them.
	 *
	 * \param  prop		The property that should be checked.
	 * \param  loc		The location where the condition is applied.
	 */
	std::pair<std::string, std::string>
	priv_getTypeString(
		const eRandProp 	prop,
		const eRandLoc 		loc)
	 const;

	/*
	 * =============================
	 * Private Members
	 */
private:
	Path_t 		m_path;		//!< Location of the configuration file.
	IniReader_t 	m_ini;		//!< The ini file object.
}; //End class(egd_confObj_t)



/*
 * ======================
 * String interacting functions
 */
inline
std::string
operator+ (
	const std::string& 	s,
	const eRandProp 	p)
{
	return s + printRandProp(p);
};


inline
std::string
operator+ (
	const eRandProp 	p,
	const std::string& 	s)
{
	return printRandProp(p) + s;
};


PGL_NS_END(egd)


