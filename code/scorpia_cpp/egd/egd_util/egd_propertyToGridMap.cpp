/**
 * \brief	This file contains some of the implementation of the property to gid map class.
 */

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_gridType.hpp>

#include <egd_util/egd_propertyToGridMap.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_hashmap.hpp>


//Include the Eigen Headers


//Include STD


PGL_NS_START(egd)

#if defined(EGD_PROP_TO_GRID_MAP_BRACKET) && (EGD_PROP_TO_GRID_MAP_BRACKET == 1)
#	pragma message "Compiled the property to grid map, with enabled operator[](pIdx)"
#endif


egd_propToGridMap_t::egd_propToGridMap_t()
 = default;


egd_propToGridMap_t::egd_propToGridMap_t(
	const egd_propToGridMap_t&)
 = default;


egd_propToGridMap_t&
egd_propToGridMap_t::operator= (
	const egd_propToGridMap_t&)
 = default;


egd_propToGridMap_t::egd_propToGridMap_t(
	egd_propToGridMap_t&&)
 noexcept
 = default;


egd_propToGridMap_t&
egd_propToGridMap_t::operator= (
	egd_propToGridMap_t&&)
 = default;


egd_propToGridMap_t::~egd_propToGridMap_t()
 = default;



bool
egd_propToGridMap_t::isValid()
 const
 noexcept
{
	for(const auto it : m_map)
	{
		if(it.first.isValid() == false)
		{
			pgl_assert(false && "Property index is not valid.");
			return false;
		};
		if(::egd::isValidGridType(it.second) == false)
		{
			pgl_assert(false && "Grid type is invalid.");
			return false;
		};
		if(::egd::hasSizeBit(it.second) == false)
		{
			pgl_assert(false && "No size bit.");
			return false;
		};
	}; //End for(it):

	return true;
}; //End: isValid



bool
egd_propToGridMap_t::hasMappingFor(
	const PropIdx_t 	pIdx)
 const
 noexcept
{
	if(pIdx.isValid() == false)
	{
		//Can not thowo an exception. callterminate manually.
		std::cerr
			<< PGL_EXCEPT_InvArg("Passed an invbalid property index.").what()
			<< std::endl;
		std::terminate();
	};// End if: no valid index

	const auto c = m_map.count(pIdx);
		pgl_assert(c == 0 || c == 1);

	//We do not have it
	if(c == 0)
	{
		return false;
	};//End if: we do not have it

	//We have found it
		pgl_assert(::egd::isValidGridType(m_map.at(pIdx)));
	return true;
};//End: hasProperty


egd_propToGridMap_t&
egd_propToGridMap_t::addMapping(
	const PropIdx_t 	pIdx,
	const eGridType 	gType)
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(::egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};

	//Test if it is here
	if(this->hasProperty(pIdx) == true)
	{
		throw PGL_EXCEPT_InvArg("Tried to add the mapping {" + pIdx + " -> " + gType + "} to the map, but the mapping is already pressent."
				" The recorded mapping is {" + pIdx + " -> " + m_map.at(pIdx) + "}");
	};

	//Now add it
	m_map[pIdx] = gType;

	//Return a reference to this
	return *this;
}; //End: addMapping


egd_propToGridMap_t&
egd_propToGridMap_t::addMapping(
	const PropIdx_t 	pIdx,
	const eGridType 	gType,
	const bool 		isExt)
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	//We can not check gType for validity, since it is by definition invalid, currently
	if(egd::hasSizeBit(gType) == true)
	{
		throw PGL_EXCEPT_InvArg("The passed grid type has an size it is " + gType);
	};

	return this->addMapping(
			pIdx,
			isExt ? mkExt(gType) : mkReg(gType) );
};//End: add mapping


egd_propToGridMap_t&
egd_propToGridMap_t::updateMapping(
	const PropIdx_t 	pIdx,
	const eGridType 	gType)
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	if(::egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid grid type.");
	};

	//Test if it is here
	if(this->hasProperty(pIdx) == false)
	{
		throw PGL_EXCEPT_InvArg("Tried to update the mapping for " + pIdx + ", with {" + pIdx + " -> " + gType + "}."
				" But the mapping is not pressent.");
	};

	//Now add it
	m_map.at(pIdx) = gType;

	return *this;
};//End update the mapping


egd_propToGridMap_t&
egd_propToGridMap_t::updateMapping(
	const PropIdx_t 	pIdx,
	const eGridType 	gType,
	const bool 		isExt)
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid property index.");
	};
	//We can not check gType for validity, since it is by definition invalid, currently
	if(egd::hasSizeBit(gType) == true)
	{
		throw PGL_EXCEPT_InvArg("The passed grid type has an size it is " + gType);
	};

	return this->updateMapping(
			pIdx,
			isExt ? mkExt(gType) : mkReg(gType) );
}; //End: update the map



eGridType
egd_propToGridMap_t::getGridOf(
	const PropIdx_t 	pIdx)
 const
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed property index is invalid.");
	};
	if(this->hasProperty(pIdx) == false)
	{
		throw PGL_EXCEPT_OutOfBound("No mapping for property " + pIdx + " is known.");
	};

	return this->m_map.at(pIdx);
}; //End, get grid


egd_propToGridMap_t&
egd_propToGridMap_t::ensureMapping(
	const PropIdx_t 	pIdx,
	const eGridType 	gType,
	const bool 		isExt)
{
	return this->ensureMapping(pIdx, isExt
					? mkExt(gType)
					: mkReg(gType) );
};


egd_propToGridMap_t&
egd_propToGridMap_t::ensureMapping(
	const PropIdx_t 	pIdx,
	const eGridType 	gType)
{
	if(pIdx.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("Passed an invalid argument to the ensuring.");
	};
	if(egd::isValidGridType(gType) == false)
	{
		throw PGL_EXCEPT_InvArg("The grid type for the mapping of " + pIdx + " was invalid.");
	};


	//Test if do not know the mapping
	if(this->hasMappingFor(pIdx) == false)
	{
		/*
		 * The mapping is un known, so we can
		 * simply add it
		 */
		return this->addMapping(pIdx, gType);
	}; //End if: not known

	/*
	 * If we are here, we must test if the
	 * two grids are the same.
	 */
	const eGridType storredGrid = m_map.at(pIdx);	//Load the grid

	//Test if different
	if(storredGrid != gType)
	{
		throw PGL_EXCEPT_LOGIC("For the property " + pIdx + ", the mapping is already known, it is " + storredGrid
				+ ", but was asked to set it to " + gType);
	}; //End if: grids are different

	//We habe the same grid, so we can end here
	return *this;
}; //ENd: ensure


egd_propToGridMap_t&
egd_propToGridMap_t::saveAdding(
	const egd_propToGridMap_t& 	src)
{
	if(src.isValid() == false)
	{
		throw PGL_EXCEPT_InvArg("The passed src is not valid.");
	};
	if(this->isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("*this is not valid.");
	};


	/*
	 * GOing through the list and add
	 */
	for(const auto& it : src)
	{
		const PropIdx_t pIdx 	= it.first;		//Unpack
		const eGridType gType 	= it.second;

		//Ensure the existence of *this into the mapping
		this->ensureMapping(pIdx, gType);

		//
		//If we are here then everything is right
	}; //End for(it):

	return *this;
}; //End: saveAdding


PGL_NS_END(egd)

