/**
 * \brief	This file contains the functions to read out the boundary condition state given by the ini file.
 */

//Include EGD
#include <egd_core/egd_core.hpp>
#include "./egd_confObj.hpp"

#include <egd_phys/egd_boundaryCondition.hpp>


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_string.hpp>
#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)

using pgl::isValidFloat;


/*
 * ======================
 * Boundary Conditions
 */
egd_confObj_t::BoundCondition_t
egd_confObj_t::getBC(
	const eRandProp 	prop)
 const
{
	//get the correct indx, the translator will ensure that it is correct
	const PropIdx pIdx = Rand_toPropIdx(prop);

	//This is teh BC that we will return
	BoundCondition_t BC(pIdx);

	//This is a list of all sides that we have to scan
	pgl::pgl_vector_t<eRandLoc> rLocs;

	//Fill the vector
	if(BC.isPressure() == false)
	{
		rLocs = {eRandLoc::TOP, eRandLoc::RIGHT, eRandLoc::BOTTOM, eRandLoc::LEFT,	//classical boundary
			 eRandLoc::INT_LOWSHEAR, eRandLoc::INT_UPSHEAR                    };	//internals
	}
	else
	{
		rLocs = {eRandLoc::PRESS};
	};


	for(const eRandLoc loc : rLocs)
	{
		std::string key;
		eRandKind kind;
		const bool wasLocFound = this->getBoundaryType(prop, loc, &kind, &key);

		//Handling a missing location
		if(wasLocFound == false)
		{
			//in the case of a internal boundary, we will ignore them
			if(isInternalRandLoc(loc) )
				{ continue; };

			//In the case of a non internal boundary, we will error
			throw PGL_EXCEPT_RUNTIME("Did not found locaton " + loc
					+ " for property " + pIdx + " in the configuration file.");
		}; //End if: location was not found

		//Get the value if it is a dirichlet condition
		//  is NAN in case of non dirichlet conditions
		const Numeric_t dirichletValue = (kind == eRandKind::Dirichlet)
					          ? this->getDirichletValue(prop, loc)
					          : NAN;

		//Construct an object
		BC.addCond(egd_boundaryConditions_t::SideCondition_t(loc, kind, dirichletValue));
	}; //End for(loc):

	if(BC.hasCondition() == false)
	{
		throw PGL_EXCEPT_RUNTIME("No conditions found.");
	};
	if(BC.isValid() == false)
	{
		throw PGL_EXCEPT_RUNTIME("Did not construct a valid BC.");
	};

	//Make the BC final
	BC.makeFinal();

	if(BC.isFinal() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The BC could not be finalized.");
	};

	//Return the boundary object
	return BC;
}; //End: compose a BC


bool
egd_confObj_t::hasBC(
	const eRandProp 	prop)
 const
{
	//get the correct indx, the translator will ensure that it is correct
	const PropIdx pIdx = Rand_toPropIdx(prop);

	//This is teh BC that we will return
	BoundCondition_t BC(pIdx);

	//This is a list of all sides that we have to scan
	pgl::pgl_vector_t<eRandLoc> rLocs;

	//Fill the vector
	if(BC.isPressure() == false)
	{
		rLocs = {eRandLoc::TOP, eRandLoc::RIGHT, eRandLoc::BOTTOM, eRandLoc::LEFT,	//classical boundary
			 eRandLoc::INT_LOWSHEAR, eRandLoc::INT_UPSHEAR                    };	//internals
	}
	else
	{
		rLocs = {eRandLoc::PRESS};
	};

	// Go through all locations
	for(const eRandLoc loc : rLocs)
	{
		std::string value, key;		//Load the key/values
		std::tie(key, value) = this->priv_getTypeString(prop, loc);

		//If we found a non empty value, then we know,
		//that there is something
		if(value.empty() == false)
		{
			return true;
		}; //End if: we found something
	};//End for(loc): cecked all locations

	return false;
}; //End: hasBC


egd_confObj_t::ePropList_t
egd_confObj_t::getAllProps()
 const
{
	ePropList_t propList;	//Return value

	/*
	 * Go through all known eRandKind tags.
	 * Thsi is very ugly, and not so efficient.
	 */
	for(eRandProp it = static_cast<eRandProp>(static_cast<int>(eRandProp::BEGIN) + 1);
	    it != eRandProp::ENDE;
	    it = static_cast<eRandProp>(static_cast<int>(it) + 1) )
	{
		if(this->hasBC(it) == true)	//If we had it, add it
		{
			propList.push_back(it);
		};
	}; //End for(it):

	//Return the list
	return propList;
};//End: get all different kinds



eRandKind
egd_confObj_t::getBoundaryType(
	const eRandProp 	prop,
	const eRandLoc 		loc)
 const
{
	std::string key;	//this is the key that was used, only needed in case of an error.
	eRandKind kind;		//this is the kind, will be set by the call

	if(this->getBoundaryType(prop, loc, &kind, &key) == false)
	{
		throw PGL_EXCEPT_RUNTIME("Could not find the boundary condition of "
				+ prop + " at " + loc + ", key was " + key);
	}; //End if: did not found the property
		pgl_assert(isValidRandKind(kind));

	return kind;
}; //End: get boundary type, with safty


bool
egd_confObj_t::getBoundaryType(
	const eRandProp 		prop,
	const eRandLoc 			loc,
	      eRandKind* const 		kind,
	      std::string* const 	keyToken)
 const
{
	pgl_assert(kind != nullptr);

	//Get the value string
	std::string value, key;
	std::tie(key, value) = this->priv_getTypeString(prop, loc);

	//write the key token if given, moves KEY
	if(keyToken != nullptr)
		{ *keyToken = std::move(key); };

	//if empty then it was not found, so we have to return false
	if(value.empty() == true)
	{
		return false;
	};

	//Convert the value
	const eRandKind tmpKind = Rand_parseKind(value);
		pgl_assert(isValidRandKind(tmpKind));

	//now we write the value back
	*kind = tmpKind;

	return true;
}; //End: get Boundary type


std::pair<std::string, std::string>
egd_confObj_t::priv_getTypeString(
	const eRandProp 	prop,
	const eRandLoc 		loc)
 const
{
	/*
	 * If we have pressure then the location must
	 * be PRESS, also only the kind of dirichlet is
	 * supported for pressure.
	 */
	if(prop == eRandProp::Pressure)
	{
		if(loc != eRandLoc::PRESS)	//Test if the pressure location is given.
		{
			throw PGL_EXCEPT_InvArg("Tried to access the pressure type, without using the special pressure local value tag.");
		};

		return std::make_pair(
			std::string("PRESS_SPECIAL:PRESS_SPECIAL"),	//Fake key
			std::string("d") );				//Will be recognized as dirichlet
	}
	else
	{
		if(loc == eRandLoc::PRESS)
		{
			throw PGL_EXCEPT_InvArg("Tried to access a non pressure kind at the pressure location.");
		};
	};

	//Load the values we need
	const std::string propToken = Rand_getPropStr(prop);		pgl_assert(!propToken.empty());
	const std::string locToken  = Rand_getLocStr(loc);		pgl_assert(!locToken.empty());
	const std::string sectToken = Rand_getStectionStr(prop);	pgl_assert(!sectToken.empty());

	//Compose the key we need
	const std::string key = propToken + "_" + locToken + "_type";

	//Now we query the ini reader
	const std::string value = m_ini.Get(sectToken, key, "");

	//Now return the string for the value
	return std::make_pair(key + ":" + sectToken, value);
}; // End: get kind of BC string


egd_confObj_t::Numeric_t
egd_confObj_t::getDirichletValue(
	const eRandProp 	prop,
	const eRandLoc 		loc)
 const
{
	/*
	 * This handles the pressure offset value.
	 * This is always a dirichlet condition and
	 * has a different format, so it can not be
	 * handled by the regular implementation.
	 */
	if(prop == eRandProp::Pressure)
	{
		if(loc != eRandLoc::PRESS)
		{
			throw PGL_EXCEPT_InvArg("Tried to access the pressure type, without using the special pressure local value tag.");
		};

		//Parse the pressure value
		const std::string propToken = Rand_getPropStr(prop);
		const std::string sectToken = Rand_getStectionStr(prop);

		//Compose the key
		const std::string key = propToken + "_value";

		//Now load the value
		const Numeric_t value = m_ini.GetReal(sectToken, key, NAN);

		if(::pgl::isValidFloat(value) == false)
		{
			throw PGL_EXCEPT_InvArg("The value requested for \"" + key + "\" does not exist.");
		};

		//
		//Return the value
		return value;
	}; //End if: handle the pressure case
		pgl_assert(loc != eRandLoc::PRESS);


	//Load the values we need
	const std::string propToken = Rand_getPropStr(prop);
	const std::string locToken  = Rand_getLocStr(loc);
	const std::string sectToken = Rand_getStectionStr(prop);

	//Compose the key we need
	const std::string key = propToken + "_" + locToken + "_type";

	//Now we query the ini reader
	const std::string value = m_ini.Get(sectToken, key, "");
	if(value.empty() == true)
	{
		throw PGL_EXCEPT_InvArg("The key \"" + key + "\" in section \"" + sectToken + "\" does not exist.");
	};

	//Now parse the enum
	const eRandKind kind = Rand_parseKind(value);

	if(kind != eRandKind::Dirichlet)
	{
		throw PGL_EXCEPT_InvArg("The key \"" + key + "\" does not refere to a dirichlet condition. its value was \"" + value + "\"");
	};

	//Now load the value
	const std::string key2 = propToken + "_" + locToken + "_value";

	const Numeric_t diriValue = m_ini.GetReal(sectToken, key2, NAN);
	if(isValidFloat(diriValue) == false)
	{
		throw PGL_EXCEPT_InvArg("The value that was read in for the dirichlet condition of " + key2 + " was an invalid number.");
	};

	return diriValue;
}; //End: get Dirichlet value


PGL_NS_END(egd)


