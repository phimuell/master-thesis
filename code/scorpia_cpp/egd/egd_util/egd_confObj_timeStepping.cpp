/**
 * \brief	This file contains all functions for the configuration oibject.
 */

//Include EGD
#include <egd_core/egd_core.hpp>
#include "./egd_confObj.hpp"

#include "./egd_parserUtil.hpp"



//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_string.hpp>
#include <pgl/pgl_INIreader/pgl_INIReader.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <string>


PGL_NS_START(egd)

using pgl::isValidFloat;


egd_confObj_t::Numeric_t
egd_confObj_t::getFinalSimTime()
 const
{
	if(this->m_ini.hasKey("TimeStepping", "FinalTime") == false)
	{
		return Numeric_t(-1.0);
	};

	const std::string dt_s = this->m_ini.Get("TimeStepping", "FinalTime", "");
		pgl_assert(dt_s.empty() == false);

	const Numeric_t dt = egd_parseTime(dt_s);

	if(isValidFloat(dt) == false ||
	   dt <= 0.0                   )
	{
		throw PGL_EXCEPT_InvArg("The final time step value is not valid. String was \"" + dt_s
				+ "\" and parsed time " + std::to_string(dt));
	};

	return dt;
};


egd_confObj_t::Numeric_t
egd_confObj_t::getMaxTimeStepSize()
 const
{
	const std::string dt_s = m_ini.Get("TimeStepping", "maxDeltaTime", "");
	if(dt_s.empty() )
	{
		throw PGL_EXCEPT_InvArg("The value for the maximum time step was not specified.");
	};

	const Numeric_t dt = egd_parseTime(dt_s);

	if(isValidFloat(dt) == false ||
	   dt <= 0.0                   )
	{
		throw PGL_EXCEPT_InvArg("The maximum time step value is not valid. String was \"" + dt_s
				+ "\" and parsed time " + std::to_string(dt));
	};

	return dt;
};


egd_confObj_t::Numeric_t
egd_confObj_t::getMinTimeStepSize()
 const
{
	const std::string dt_s = m_ini.Get("TimeStepping", "minDeltaTime", "");
	if(dt_s.empty() )
	{
		throw PGL_EXCEPT_InvArg("The value for the minimum time step was not specified.");
	};

	const Numeric_t dt = egd_parseTime(dt_s);

	if(isValidFloat(dt) == false ||
	                dt  <= 0.0     )
	{
		throw PGL_EXCEPT_InvArg("The minimum time step value is not valid. String was \"" + dt_s
				+ "\" and parsed time " + std::to_string(dt));
	};

	return dt;
};


egd_confObj_t::Numeric_t
egd_confObj_t::getInitialInertiaTimeStep()
 const
{
	const std::string dt_s = m_ini.Get("TimeStepping", "initInertiaTimeStep", "");
	if(dt_s.empty() )
	{
		//Did not specify the time, so we use the minimal one
		return this->getMinTimeStepSize();
	};

	const Numeric_t dt = egd_parseTime(dt_s);

	if(isValidFloat(dt) == false ||
	                dt  <= 0.0     )
	{
		throw PGL_EXCEPT_InvArg("The initial inertia time step value is not valid. String was \"" + dt_s
				+ "\" and parsed time " + std::to_string(dt));
	};

	return dt;
};



egd_confObj_t::Size_t
egd_confObj_t::nTimeSteps()
 const
{
	//This is a list of all known name token. They are processed from left to right.
	const std::string names[] = {"nTimeSteps", "nTimeStpes"};

	//iterate through the token
	for(const std::string& name : names)
	{
		//Test if a token with an associated value is given
		if(m_ini.hasValue("TimeStepping", name) == true)
		{
			//the value was given
			const sSize_t N = m_ini.GetInteger("TimeStepping", name, -1);	//third argument is unneccessary

			//test if the value is illegal
			if(N <= 0)
			{
				throw PGL_EXCEPT_InvArg("The specified timesteps " + std::to_string(N)
						+ " is invalid, the used key was " "TimeStepping" ":" + name);
			};

			//return the value
			return N;
		}; //end: test if the key was given.
	}; //End for(name):

	throw PGL_EXCEPT_InvArg("The number of timesteps  (TimeStepping:nTimeSteps) was not specified.");
	return Size_t(-1);
}; //End: nTimesteps


egd_confObj_t::Size_t
egd_confObj_t::nRampSteps()
 const
{
	const Size_t N = m_ini.GetInteger("TimeStepping", "nRampSteps", -1);

	if(N == -1)
	{
		throw PGL_EXCEPT_InvArg("The number of ramp steps  (TimeStepping:nRampSteps) was not specified.");
	}
	if(N <= 0)
	{
		throw PGL_EXCEPT_InvArg("Illegal numbers of timesteps. TimeStepping:nRampSteps was " + std::to_string(N) );
	};

	return N;
}; //End: nRampStep


egd_confObj_t::Size_t
egd_confObj_t::nSuperResTimeSteps()
 const
{
	const Size_t N = m_ini.GetInteger("TimeStepping", "nSuperSmallTimeSteps", -1);

	if(N == -1)
	{
		throw PGL_EXCEPT_InvArg("The number of super small time steps was not specified.");
	};
	if(N <= 0)
	{
		throw PGL_EXCEPT_InvArg("Illegal number of super restricted time steps, the value was " + std::to_string(N) );
	};

	return N;
};//End: super restricted



egd_confObj_t::Numeric_t
egd_confObj_t::getMaxDisplacementX()
 const
{
	const Numeric_t disX = m_ini.GetReal("TimeStepping", "maxDisplacementX", 0.40);

	if(pgl::isValidFloat(disX) == false)
	{
		throw PGL_EXCEPT_InvArg("Read in a non finite value for the x displacement.");
	};
	if(disX <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The value for the maximal x displacement is invalid."
				" It is too small, the read value was " + std::to_string(disX));
	};
	if(disX >= 1.0)
	{
		throw PGL_EXCEPT_InvArg("The value for the maximal x displacement is invalid."
				" It is too large, the read value was " + std::to_string(disX));
	};
	return disX;
}; //End: getDisplacement in X


egd_confObj_t::Numeric_t
egd_confObj_t::getMaxDisplacementY()
 const
{
	const std::string disY_s = m_ini.Get("TimeStepping", "maxDisplacementY", "");

	if(disY_s.empty() )	//Not specified, so we forward the request to Y
	{
		return this->getMaxDisplacementX();
	};

	//Read in the value
	const Numeric_t disY = std::stod(disY_s);


	if(pgl::isValidFloat(disY) == false)
	{
		throw PGL_EXCEPT_InvArg("Read in a non finite value for the y displacement.");
	};
	if(disY <= 0.0)
	{
		throw PGL_EXCEPT_InvArg("The value for the maximal y displacement is invalid."
				" It is too small, the read value was " + std::to_string(disY));
	};
	if(disY >= 1.0)
	{
		throw PGL_EXCEPT_InvArg("The value for the maximal y displacement is invalid."
				" It is too large, the read value was " + std::to_string(disY));
	};
	return disY;
}; //End: getDisplacement in Y



PGL_NS_END(egd)

