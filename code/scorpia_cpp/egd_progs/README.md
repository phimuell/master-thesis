# Info
This folder contains the programs that were written for the thesis.
Each of the sub folder contains a separate program. It is tried to
keep them to a minimum. Actually only one was written.


## Folders /Programs
Here is a list of folders and programs that reside in this folder.

### CORELLO
This is the main program that is written. It uses configuration
files to control itself. It is named after "Ribald Corello" a
fictional character from the Perry Rhodan science fiction series,
see "https://www.perrypedia.proc.org/wiki/Ribald_Corello" for
more informations.
I mean the 'E' in EGD also stands for "Ellert" which is also
from the series.

It will store the simulation results in HDF5 files.

