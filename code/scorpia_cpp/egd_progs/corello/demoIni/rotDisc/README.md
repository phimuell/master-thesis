# Info
In this folder the test configurations for the rotating disc scenario are located.

# The General Setting
The setting is that a disc is rotating in mid air, with a certain angular velocity.
The angular momentum should be conserved. There is no gravity thou, but the surrounding
media is viscose, but very low. So the angular momentum should reduce over time, but
should be conserved at the beginning.

## Slow
In this setting the disc is considered to rotate with a low speed. It rotates
with a speed of 10 rad per seconds, meaning a full rotation all 0.6 seconds.
We will simulate two rotations of the movement.

## Fast
Here the disc rotates faster, its initial velocity is 30 rad per seconds. This
means that one rotation will last for 0.2 seconds, we will also simulate two
rotations as well.

## Viscosity of the Media
We have three different fluid viscosity.
In the normal case, it is very low, in the order of air.
Then we have a setting where the viscosity is zero and one
where the viscosity is very large, similar to honey.

### Real Viscosity in Zero Case
Due to the final resolution and the interpolation, the ball will experience some kind
of viscosity in its vicinity, even in the case of zero viscosity.
Since the air viscosity is already very small, the differences should not be large.

## Coarse
The system that have a C in their name are coarse systems.
In these setting we have each grid cell has a side that is twice as large as as in the other models.
But the number of markers stay the same.
This will increase the artificial viscosity in the surrounding of the ball.


## Technical Considerations/Details

### Time Stepping
Note that in both scenarios the used time stepping is the same, meaning that we will
allow for a maximum displacement in each direction of 0.06, which means that the CFL,
condition is limited to 0.12 at most.

### smTime
These versions only allow for a maximum displacement of of 0.02.
Which means that the time step is tree times as small as in the other systems.
In absolute terms it allows for the same displacement as in the Coarse systems.

We would also point out that we consider this experiments as the true normal settings.
However this is not reflected in the naming.


### Rotating Media
It is recommended that the fluid should also be set in motion, such that the fluid
has not to be accelerated and consume momentum from the disc.
This can not be done by configuration file, this is a compile parameter.
The fluid is initialized using the Taylor-Courette flow.
Which is a rotating cylinder inside a hollow one, that is at rest.
There is no real outer cylinder, instead there is just fluid that is at rest.

Previously the angular velocity was quadratically reduced.


### Integrator
For the normal case we will use all four integrators.
However for some settings we are using only one integrator.
This is most often jm, since it has become the default one.

### Dumping Interval
We try to dump the simulation "often", without generating too much data. We choose
the interval such that the disc has rotated approximately 2 degrees. This means
180 dumps per rotation. Note that at the beginning there is a finer dumping.
For the slow setting this means all 3.3ms a dump and for the fast disc all 1.1ms
a dump has to be performed.

Note these numbers where retrofit from "what is easy to analyze".


# Old Version
There were old versions of this settings, which were deemed not so good.
They can be examined, for example, under the commit "d05e740832ce6977e6",
in the folder "code/scorpia_cpp/egd_progs/corello/demoIni/rotDisc".


