# Info
The configuration in this file will setup a scenario to evaluate
how good linear momentum is conserved.

## Setting
The setting is quite simple. A ball moves towards the left in a
media with low viscosity, no gravity is acting on it.
This means that the linear momentum should be preserved, however
some slow down is expected.
The Reynolds number is in the region of 10'000.

The ball will move for 600 milliseconds, then the ball has
crossed the domain.

The configuration settings are based on the ones from the
simple ball scenario.

### Versions
We have two versions of them. That differs in the ball density.
In linMom the ball is very light and thus strongly influenced
by the surrounding. In linMom2 the ball is very heavy and is
thus able to sustain some effects that tries to slow it down.





