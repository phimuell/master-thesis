# Info
This folder contains settings that are designed to compare the different integration schemes.
The setting is quite simplistic. It is the setting where a ball is thrown straight up.

These settings are not were only studied in an early phase of the project.
They could be the source for more experiments, thus we decided to keep them.


# What to Expect
It is quite difficult to say, what is to expect from the simulation. Since we have viscosity
in the system, even if the surrounding media is invicid, because of the mapping from the
markers top the grid, trajectories predicted by Newtonian mechanic are off.
There are a few workarounds for that problem, but neither is perfect.

## Using a Zero Viscosity Media
Here we basically hope that the effect of the ghost viscosity is small.

## Incorporate the Drag Coefficient
According to Wikipedia, (https://de.wikipedia.org/wiki/Str%C3%B6mungswiderstandskoeffizient#cw-Werte_von_typischen_K%C3%B6rperformen)
the drag coefficient of a long rod, which is our setting, is given as 1.2, if the
Reynolds number below 1.9 * 10^5.
In the normal setting the Reynolds number is 1'037, so we could use the value.
However should we really use an approximation as our analytical solution?

## Analyzing the X Component
Even if we can not compute the analytical evolution of the y component, we can analyze
the evolution of the x component, which is known. Since all movement is directed upwards
no movement in x should happen and marker positions should stay the same.


# Concrete Settings
Here is a list of all settings that are implemented.

## The "ball1" Family
We will use the normal viscosity setting, for now, this is a very small but non zero
viscosity. In the analyzing we will focuses on the x component and see how the y
component is behaved. Since it is possible to say, what should happen, from a
qualitative point of view.

## The "ballZeroVisco" Family
This are settings which are basically copies of the ball1 family. However here we have
used a different medium. The viscosity of the medium is set to zero.
It is important to realize that this does not mean that the ball will not exhibit viscosity
from the medium. This viscosity is an artifact that is caused by the interpolation of viscosity.
Nodes that are outside the ball, but have ball markers near by, will have a viscosity because
of that.

### TODO
Think about solution. Other interpolation scheme.

## The "BallHighVisco" Family
This setting is the same as the other twos conceptional, but the surrounding media is
different. Instead of very low or zero, as the other two, it is large. In the normal
setting the viscosity is somewhere the one of air, here it is in the region of honey.
The net effect of that the ball will not ascend as high as in the other case, because
the media will consume its energy much quicker, and the falling phase will take longer.


## Note about Timestep
The time step is not directly controlled. Configuration files only enforces some limits.
The time step is mostly controlled by limiting the movement of markers. This settings
limits that movement to 0.1 grid wide in each dimension. This means that the simulation
does not have a CFL number larger than 0.2, which is deemed low enough.


# NOTE - Old Configuration
Note the configurations that are here where heavily modified. See commit 3966939e81d51
for the old version of the configurations. In the old versions the focuses lies on the
sub time stepping, so there were two versions, a "fine" and a "normal", for each setting.
Now we have focuses on comparing the integrator under fair conditions, with respect to
the time stepping.



