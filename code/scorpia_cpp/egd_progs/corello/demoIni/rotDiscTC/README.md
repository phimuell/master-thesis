# Info
This is a special setting, it is similar to the rotating disc case.
However the disc is powered here, and there is a second cylinder, which encapsulating some fluid.

Since the inner disc is rotating at a constant speed, the simulation should reach a steady state.
This state can be calculated analytically and is known as Taylor--Couette flow.

## Disc
We have again two disc speed.
Fast is 30 rad per seconds and slow 10.


## Fluid
The fluid is partially rotating, but does not start in the steady state.
And it has a larger viscosity, such that the coupling works better.

### Low Eta
There is a second version of the system, the low eta version. In this version the viscosity of the
fluid is reduced, currently by the factor of four compared to the normal one. Technically this will
reduce the ability of the fluid to transmit velocity. Thus it will increase the time needed for the
system to reach the steady state.


## Rheology
A special Rheology is used to power the rotation.


## NAME TC
Some time ago we also used attached the name TC to the normal rotating disc settings.
However this only reefed to the initial conditions that that where used for the fluid.
And has nothing to to with this experiment.

