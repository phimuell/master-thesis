; This is a demo configuration file of CORELLO.
;
; This is the demo configuration for the simple ball experiment.
; It is an experiment where a ball has a prescribed velocity
; upwards and gravity pulls him down. This is also a test to
; see if the solver works at all.
; SOURCE: 99f452887eb9a5c1662f384c3c1dc4b0a6a33096:./demo_corello_conf_simpleBall1.ini

[Misc]
; This is the misc section, it contains parameter/options
; that does not fit nicely into the other sections.

; This option specifies where the dump file, the HDF5 file should be
; written to.
hdf5_file=corello_ball1.hdf5

; Set this option to yes if the type information sould only be dumped
; in the first iteration.
typeOnlyFirst=yes

; If yes the train rate will not be dumped.
NoStrainRateDump=no

; If yes, then no stress will be dumped
NoStressDump=yes

; THis flag controles how many markers are dumped.
; If the index of the marker modulo this number is
; zero then the marlker will be dumped. Note it should be
; smaller than the two marker densities.
; setting this value to one or zero will dump all markers.
MarkerSelectionMod=2

; If this is set to yes only representants will be dumped
; on the grid
DumpOnlyRep=yes

; Select if density stabilization should be activated
RhoStab=false


[TimeStepping]
; This section will contain all configurations about the time stepping
; Note that this was previosly located in the Misc section.

; This options controles the numnber of timesteps that should be performed.
;  (Was previously in Misc)
nTimeStpes=2

; This is the final time that should be simulated. The value is
; interpreted in seconds but can be augmented by chars to indicate
; the unit. Note that this value takes precednece over the raw
; time step argument
FinalTime=1

; This is the minimum type step length that should be performed.
minDeltaTime=0.001

; This is the maximum time step length that is allowed.
maxDeltaTime=0.01

; This is the number of supper restricted time step.
; During this time, the timestep is forced to the minimum time step length.
nSuperSmallTimeSteps=5

; This is the number timestep in which the time limit is learly increased
; until the time step maximum reaches the specified maximum. the counting
; starts after the super restricted phase.
nRampSteps=40

; This is the initial inertia time step. This time step is needed when
; inertia is activated. In the first step one needs a previuous time
; step and this value provides one with it.
initInertiaTimeStep=0.001


[Domain]
; In this section you can controll some basic things about the domain
; and the discretization. Note that the setup implementations, that is
; the code that reads in this section is not requiered to read all and
; accept them. For example that standard implementation, which sets
; up the original problem, does not read the length option.

; This is the number of basic nodal grid points in x direction.
Nx=101

; This is the number of basic nodal grid points in y direction.
Ny=201

; This is the mareker density measuered in cell for the x direction.
; To get the number of markers that are placed in x direction
; the number of cells is muliplied by this number.
nMarkerX=5

; This is the marker density for the y direction, see x direction
; for more information.
nMarkerY=5

; This value controles, if the marker are randomly scattered
; around their regular position, this feature may not be
; imlemented.
MarkerRandDisplace=yes

; This value allows to control how the dumper handles temperature.
; By setting it toyes will cause CORELLO to instruct the dumper to
; only dump the full heating term, the components of it will not be
; written by the dumper; Default is no
WriteOnylFullHeatTerm=yes

; This variable allows to change the extension of the domain in x
; direction. Note that there is no synchronization with the
; number of grid points, so you also have to addapt that value.
; Note that the value is interpreted in Kilometers.
xLength_m=1


; This variable allows to change the extension of the domain in x
; direction. Note that there is no synchronization with the
; number of grid points, so you also have to addapt that value.
; Note that the value is interpreted in Kilometers.
; And it si seen as the depth.
yLength_m=2



[SetUp]
; In this section the initaitor or the set uper can be configured.
; These are the part of the code that set everything up, distribute
; the markers and assigne them the correct values.
type=ball1



[StokesSolver]
; Here you can select the solver that should be used to compute
; the velocities and pressure. You could also configure the
; solver with further parmeters.
type=nsext



[TemperatureSolver]
; Here you can select the solver for the temperature.
; YOu could also define some more option here if the
; solver supports them.
type=nullext

; With this falg you can controll if the temperature
; solution is dumped or not. This is only usefull
; and recomended if the null temperature solver
; is used.
dumpSol=no



[StokesBoundary]
; In this section the condition for the stokes boundary conditions
; this are velocities (x & y) and pressure are configured.
; Note that not all options are aviable.

; Here the conditions on the top can be configured.
; This sets the x velocity condition at the top to
; free slip.
vel_x_top_type=freeSlip
;vel_x_top_value=

; Here we can controll the bottom condition for the x velocity..
; We also set it to free slip.
vel_x_bot_type=freeSlip
;vel_x_bot_value=

; Here we can controll the x velocity at the right boundary,
; these are lower value of x. It is set to dirichlet.
; We force it to zero.
vel_x_rig_type=dirichlet
vel_x_rig_value=0

; Here we controll the left side of the domain, these are larger x values.
vel_x_lef_type=dirichlet
vel_x_lef_value=0


; Here the conditions on the top can be configured.
; This sets the y velocity condition at the top to
; dirichlet and force it to zero.
vel_y_top_type=dirichlet
vel_y_top_value=0

; Here we can controll the bottom condition for the y velocity..
; We also set it to dirichlet with zero velocity.
vel_y_bot_type=dirichlet
vel_y_bot_value=0

; Here we can controll the y velocity at the right boundary,
; these are lower value of x. It is set to free slip.
; We force it to zero.
vel_y_rig_type=freeSlip
;vel_y_rig_value=

; Here we controll the y velocity at the left side of the domain,
; these are larger x values.
vel_y_lef_type=freeSlip
;vel_y_lef_value=


; Here we controll the pressure value. This is the linear offset.
; This value is needed since we can only solve for the derivative
; and we need an integration constant.
pressure_value=1.0



[TemperatureBoundary]
; These section controles the temperature boundary of
; the problem.

; Here we control the temperature condition that is applied
; at the top. We use a dirichlet condition andforce the value to 273K.
; The value here is interpreted in Kelvin.
temp_top_type=dirichlet
temp_top_value=273

; Here we controll the temperature that is applied to the
; bottom of the domain. We use a dirichlet condition, the
; value that is used is interpreted as Kelvin.
temp_bot_type=dirichlet
temp_bot_value=1573

; Here we controll the temperature condition that is applied
; to the left boundary, these is smaller x values.
; It is set to insulation, this means that the derivation in
; x direction is set to zero, and no temperature flux happens.
temp_lef_type=insulation
;temp_lef_value=

; Here we controll the temperature condition that is applied
; to the roight boundary. It is set to insulation meaning that
; no temperature flux is allowed.
temp_rig_type=insulation
;temp_rig_value=



[Rheology]
; Here the rheology is controlled.
; The rheology is used to controll how the material properties
; are depending on the environment.

; Here you specify the type that should be used.
type=null

; Here vou could specify further configuration options.



[Integrator]
; Here you can select which integrator, the part
; that moves the marker is used.

; Use the std version which is RK4 with continuum based
; interpolation of velocities.
type=std



[MarkerToGrid]
; Here you select how the interpolation from the markers
; to the grid points works.

; Use the std interpolator, which performs bilinear interpolation
; and uses a conservative scheme for temperature.
type=full



[GridToMarker]
; Here the implementation for mapping the properties from the grid
; back to the markers is selected.

; The standard version only handles the temperature, it performs
; subgrid diffusion on it.
type=velo

; With thsi you can control which scheme is used for the mapping.
; Suported are subgrid diffusion, absolute interpolation and change
; interpolation. Note that depending on the implementation this
; value meight by ignored.
JobType=s


[ApplyBC]
; Here the implementation of the Boundary Condition Applier has to be set.
; This is the code that allows to manipulate grid properties such that
; they fullfill certain conditions.

; The standard implementation uses the original conditions.
type=pipe


