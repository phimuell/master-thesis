# INFO
This setting, or family of setting, can be seen as a blend between the linear momentum
setting and the pulled ball setting. The geometry is taken from the linear momentum setting,
meaning it is much smaller than the pulled ball,  but the ball is also powered, meaning
that we impose a velocity on it. Another difference to the pulled ball setting is that,
as its name is implying, the ball moves at a constant speed, the velocity can be configured
in the configuration file. But it is usually set to 4 m/s, actually -4 m/s because of direction.

## Integrator
Normally we use the jm integrator.

## Variations
We will now explain the different variations of the experiment.

#### Note about the density
Previous versions used a variable density. In these settings the density of the ball was
always 7 times larger as the one of the media. In the new setting all balls have the same
density of 72 kg/m^3.

### ConstBall
This is the normal setting.
The density of the fluid is chosen, such that the Reynolds number is roughly 100.

### ConstBallTurb
In this scenario, the density of the fluid is larger and its viscosity is smaller.
The density is increased by a factor of 100 and the viscosity is reduced by a factor of 10.
This means that its Reynolds number is large, here its value is in the region of 100’000.

### ConstBallHighEta
This scenario is very similar to the normal one, but the viscosity of the fluid is 10 times larger.
Meaning that its Reynolds number is smaller too.

### ConstBallHighEta2
This is a setting that is very similar to the other high eta setting.
It also has a viscosity that is ten times larger than the normal one.
But it also has a density that is larger by the same factor of ten.
This means that the Reynolds number is the same as in the original case.

#### ERROR
I made an error previously.
I was confused and thought that I had to lower the density to get the same Reynolds number.
But I actually have to increase the density by the same factor than I previously increased
the density.
I have corrected that later, but it may be possible that there is still this error, in the
description.


### Stokes
This versions are quite strange, because they use the Stokes solver instead of the Navier-Stokes solver.
In order to work at all the internal condition must be used to move the ball.
Thus the Rheology will be completely useless.
Only some configuration are simulated with this mode.


## Imposing the Velocity
We have two different ways of imposing a velocity, that can be combined too.

### NoBC
In this method we  manipulate the intrinsic velocity of the markers.
This is done by using a special kind of material model.

### NoR
In this method we impose an internal condition.
We will manipulate the grid velocity that surround the centre of the ball.
We also manipulate the pressure condition, we define pressure in that cell as the average of the
four surrounding cells.

### Full
Here we use both methods at once to impose the condition.

## See also
More information can be found in the linMomentum tests, the pulled ball versions and the ini file.

