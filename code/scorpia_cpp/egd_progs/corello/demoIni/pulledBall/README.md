# Info
The pulled ball setting is a very special setting. It is a long pipe that has a small ball in it.
In the beginning the ball is at rest. It will then be pulled trough the fluid, the velocity is
described by a special function, see below. We must accelerate the ball, but also the fluid
must be moved and accelerated as well. This is similar to increasing the mass of the object
and this mass is known as “added mass”. Under certain conditions, potential theory can be
used to compute the force analytically that is needed to move the ball.
The pressure that is excreted on the ball, is the force that is needed to accelerate the ball.


## Concrete Settings
Currently there are two settings implemented for this scenario.
However they are both related to each other.

### SIN
In this setting the velocity is given as the following function.
\begin{align}
	u_{x}(t) = \alpha * ( \Sin{\frac{2 \pi}{\omega} * (t - t_0) } + 1)
\end{align}

In the above equation $\alpha$ and $\omega$ are parameter that describes the movement.
The choices for the parameters are arbitrarily, the main goal was to “see some kind of movement”.
We have set $\alpha$ to $2.3$.
We have two different choices for $\omega$.

The default one is $\omega = 0.5$, which means that two periods are done by the ball velocity.
The second choice, that is denoted by a “2”, is $\omega = 0.25$, which makes the ball a bit more oscillating.

Density and viscosity of the fluid are chosen, such that the Reynolds number of the system is roughly 10.
The density of the ball is chosen such that it is 3.2 times larger than the fluid density.
The viscosity of the ball is rather large, 10e+8, which markers is for the time scales we considering a right object.


### SinRe3
Here we use the same velocity equation than in the original sin setting.
But here the Reynolds number is larger, it is in the region of 100.
This is achieved by increasing the density of the fluid, note that the density contrast between the fluid and the ball is the same as in the other case.

### Org
This setting is the original version, in it the parameters of the fluid are real world.
This means that the Reynolds number is in the order of 1.6Mio.
However the density contrast is still the same as in the other settings.

### Stokes
In these settings we are using a Stokes Solver, that is equipped with the boundary, to impose the velocity.
But no inertia will be considered.


## Imposing Velocity
We are using all three methods of imposing the velocity. We also studying how they affect the result.


### Measuring the Additional Mass
Measuring the additional mass, which is caused by the fluid that must be accelerated is tricky.
The analyzing framework supports the integration of pressure over the surface.
The surface is reconstructed by computing the convex hull of the markers that forms the ball.
However the results are mixed, because the surface does not really exists, we have a continuum.
But the markers does not form the object.
Also due to the finite resolution we can get problems.

Instead we measuring it indirectly.
At the beginning the fluid is at rest, this means that all fluid motion that is generated,
is caused by the acceleration of the ball. This means by measuring the force that is
exerted on the fluid, we measuring the force that is needed to accelerate. However there
is one trick here, the sign. Potential theory says that the acceleration of fluid will
cause a pressure at the surface of the ball, that has to be compensated to move the
ball with the desired speed (for simplicity we consider acceleration here). Pressure does
not have a direction in acts in all direction at the same time. Assume we accelerating at
you are at the nose of the ball, and there is pressure. The pressure there opposes the
movement of the ball, this means that it is positive. Thus it will press the fluid away
from the ball. This means that the force that acts on fluid and is caused by the pressure,
has the opposite singe than the force that acts on the ball.


### Notes on Quality
We also have used the convex hull to integrate pressure over the surface. We have mixed,
but not very good results, with it. Results that are not accurate, but that are
qualitative correct are obtained by using only the Rheology to impose velocity.
But the pressure line has not the correct hights and starts too late and also decreases too much.
If however the solver is used, then the results are very bad.


## Note to Dumping
Due to the large size of the domain and the number of markers, this simulation generates a lot of data.
The setting does configure the dumper to lower the amount, but it will still generate data in the range of 60GB.

