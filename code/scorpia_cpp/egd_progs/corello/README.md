# Info
This folder contains the code written for CORELLO.
CORELLO is the main program that was written for the thesis.
It is named after a fictional character from the Perry Rhodan
series, see "https://www.perrypedia.proc.org/wiki/Ribald_Corello".

## Description
This program simulates different processes. It can be configured by
editing configuration files. It basically performs the same
computation as the MATLAB code does, but by means of the EGD library.


## Usage
Its basic usage is described in the appendix of the thesis. so we refer to
it.


## KNOWN BUGS
This is a list of known bugs that CORELLE has, but are not fixed, because they are minor importance.

### Error in the phased counter
If CORELLO is run in timestep mode, meaning that a certain number of steps are preformed,
and the number of step, that is requested is a multiply of 100, e.g. 100 or 300, an
error is generated.
This is most likely caused by an obscure corner case in the phased counter.


## DEMOINI
This folder contains all configuration files that were written for the thesis.


