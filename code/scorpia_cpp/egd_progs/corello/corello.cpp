/**
 * \brief	This file contains the CORELLO program.
 *
 * The CORELLO program is basically a prot of the Matlab script.
 * It allows to easy simulate the process.
 *
 * It uses an INI file that is read in to configure everything.
 */


//Include the confg file
#include <egd_core.hpp>
#include <egd_util/egd_confObj.hpp>
#include <egd_util/egd_signalHandler.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include <egd_interfaces/egd_GridToMarkerInterpol_interface.hpp>
#include <egd_interfaces/egd_MarkToGridInterpol_interface.hpp>
#include <egd_interfaces/egd_MarkerIntegrator_interface.hpp>
#include <egd_interfaces/egd_MarkerRehology_interface.hpp>
#include <egd_interfaces/egd_MarkerSetUp_interface.hpp>
#include <egd_interfaces/egd_StokesSolver_interface.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_TemperatureSolver_interface.hpp>
#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_applyBoundary_interface.hpp>

#include <egd_phys/egd_strainRate.hpp>
#include <egd_phys/egd_stress.hpp>
#include <egd_phys/egd_heatingTerm.hpp>
#include <egd_phys/egd_gravitation.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_simple_timer.hpp>
#include <pgl/pgl_time.hpp>

#include <pgl_util/pgl_format_util.hpp>
#include <pgl_util/pgl_phasedEventCounter.hpp>

#include <pgl_random/pgl_seedRNG.hpp>


//Include BOOST
#include <boost/filesystem.hpp>


//Include STD
#include <iterator>
#include <string>
#include <iostream>
#include <thread>

//Include SYSTEM
#include <getopt.h>


/*
 * Importing the egd namespace into this file.
 */
using namespace egd;

//Import the time namespace
using namespace ::std::chrono;

//year is only aviale in C++20
#if __cplusplus <= 201703L
using years = ::std::chrono::duration<seconds::rep, std::ratio<31556952> >;
#endif


/*
 * Type aliases
 */
using Path_t = ::boost::filesystem::path;


/*
 * Type importing
 */
PGL_IMPORT_TYPE(MarkerIntegrator_ptr, egd_MarkerIntegrator_i);
PGL_IMPORT_TYPE(  MarkerRehology_ptr, egd_MarkerRehology_i);
PGL_IMPORT_TYPE(   GridToMarkers_ptr, egd_GridToMarkers_i);
PGL_IMPORT_TYPE(   MarkersToGrid_ptr, egd_MarkersToGrid_i);
PGL_IMPORT_TYPE(    StokesSolver_ptr, egd_StokesSolver_i);
PGL_IMPORT_TYPE(     MarkerSetUp_ptr, egd_MarkerSetUp_i);
PGL_IMPORT_TYPE(      TempSolver_ptr, egd_TempSolver_i);
PGL_IMPORT_TYPE(         ApplyBC_ptr, egd_applyBoundary_i);

PGL_IMPORT_TYPE(GridContainer_t   , egd_MarkerSetUp_i);
PGL_IMPORT_TYPE(MarkerCollection_t, egd_MarkerSetUp_i);


#define outputTime(x) ::pgl::pgl_prettyPrintTime(x)


int
main(
	int 	argc,
	char**	argv)
{
	using pgl::isValidFloat;

	//Seed the generator if enabled
#	if defined(CORELLO_SEED_RNG) && (CORELLO_SEED_RNG != 0)
#	pragma message "Seeding the RNG with time is enabled."
	const auto pglSeedValue = ::std::time(nullptr);
	::pgl::pgl_seedRNG.seedByUser_strong(pglSeedValue);
	std::cout
		<< "Seeding the PGL seed generator with: " << pglSeedValue
		<< std::endl;
#	endif

	//Install the signal handling
	egd_startSignalHandling();

	/*
	 * Arguments for GETOPS
	 */

	//Long options
	struct option corelloOps[] =
	{
		{"conf" ,	required_argument,	nullptr,	'c'},	//Configuration file
		{"force",	no_argument      ,	nullptr,	'f'},	//Delete file if exists
		{"dir"  ,	required_argument,	nullptr, 	'd'},	//dump folder
		{0, 0, 0, 0}		//Indicates the end
	}; //End: corelloOps

	//Short options
	char corelloShortOps[] = ":c:d:";		//Starting with a ':' changes the behaviour
							//https://www.informit.com/articles/article.aspx?p=175771&seqNum=3


	/*
	 * Variables for the program
	 */

	//Path to the configuration file, that is read in
	Path_t ConfigurationFile;

	//This is the path to the HDF5 file
	Path_t DumpFile;

	//This variable indicates if an old dump file
	//should be deleted.
	bool deleteOldDumpFile = false;

	//This is the folder where we sould dump to, relative to the config file
	Path_t dumpDir_tmp;

	/*
	 * Parsing the arguments
	 */
	int c;
	while((c = getopt_long(argc, argv, corelloShortOps, corelloOps, nullptr)) != -1)
	{
		switch(c)
		{
		  case 'c':
		  	pgl_assert(optarg != nullptr);
		  	ConfigurationFile = optarg;
		  break;

		  case 'f':
		  	deleteOldDumpFile = true;
		  break;

		  case 'd':
		  	pgl_assert(optarg != nullptr);
		  	dumpDir_tmp = optarg;
		  break;


		  /*
		   * ERRORS
		   */
		  case ':':	//Missing option
			std::cerr
				<< "ERROR:\n"
				<< "The option \'" << ((char)optopt) << "\' needs an argument!\n"
				<< "ABORT."
				<< std::endl;
			std::abort();
		  break;

		  case '?':	//Invalid command
			std::cerr
				<< "ERROR:\n"
				<< "The option \"" << argv[optind] << "\" is unknown!\n"
				<< "ABORT."
				<< std::endl;
			std::abort();
		  break;
		}; //End switch: parsing
	}; //End while: parsing

	/*
	 * Handle the dumping directory
	 */
	const Path_t dumpDir(dumpDir_tmp);	//Make a copy of the dump ir to ensure constant
	if(dumpDir.empty() == false)
	{
		if(::boost::filesystem::exists(dumpDir) == true)
		{
			// the dump path exists so we must check if it is an error
			if(::boost::filesystem::is_directory(dumpDir) == true)
			{
				std::cout
					<< "Using non standard dump directory: " << dumpDir
					<< std::endl;
			}
			else
			{
				std::cerr
					<< "Given the non default dumping directory " << dumpDir
					<< ", but this path does not aprears to be a directory."
					<< std::endl;
				std::abort();
			};
		} //End if: folder already exists
		else
		{
			/*
			* Create all missing paths to meet the need
			*/
			(void)::boost::filesystem::create_directories(dumpDir);
		}; //End else: folder does not exists
	}; //ENd if: there was a dump dir given


	/*
	 * Creating the configuration object
	 */
	const egd_confObj_t confObject(ConfigurationFile);

	std::cout
		<< "Using the configuration file: " << ConfigurationFile
		<< std::endl;


	/*
	 * Creating the objects needed
	 */
	MarkerSetUp_ptr 	MarkerSetUp 	 = egd_MarkerSetUp_i::BUILD(confObject);
	GridToMarkers_ptr 	GridToMarkers 	 = egd_GridToMarkers_i::BUILD(confObject);
	MarkerIntegrator_ptr 	MarkerIntegrator = egd_MarkerIntegrator_i::BUILD(confObject);
	MarkersToGrid_ptr 	MarkersToGrid 	 = egd_MarkersToGrid_i::BUILD(confObject);
	MarkerRehology_ptr 	MarkerRehology 	 = egd_MarkerRehology_i::BUILD(confObject);
	StokesSolver_ptr 	StokesSolver 	 = egd_StokesSolver_i::BUILD(confObject);
	TempSolver_ptr  	TempSolver 	 = egd_TempSolver_i::BUILD(confObject);
	ApplyBC_ptr             ApplyBC          = egd_applyBoundary_i::BUILD(confObject);

	/* Inspecting */
	MarkerSetUp->inspectSolver(StokesSolver);		//inspect the solvers
	MarkerSetUp->inspectSolver(TempSolver  );
	MarkerSetUp->inspectRehology(MarkerRehology);		//Inspecting the material model
	MarkerSetUp->inspectIntegrator(MarkerIntegrator);	//inspect the integrator

	//Create the result containers (they are unique pointers)
	auto StokesResult = StokesSolver->creatResultContainer();
	auto   TempResult =   TempSolver->creatResultContainer();

	//Create the arguments
	auto StokesArg    = StokesSolver->createArgument();
	auto   TempArg    =   TempSolver->createArgument();

	/*
	 * Dumper
	 */
	const Path_t dumpFilePath_ = confObject.getDumpFile();	  //This is the real file
	const Path_t dumpFilePath  = dumpDir.empty()
				     ? dumpFilePath_		  //No dump dir was given
				     : (dumpDir / dumpFilePath_); //Dump dir was given

	//Test if the (HDF5) file exists, if so complain since this will
	//lead to errors
	if(::boost::filesystem::exists(dumpFilePath) == true)
	{
		if(deleteOldDumpFile == true)
		{
			//Output a message
			std::cerr
				<< "The file " << dumpFilePath << " already exists and --force was given. "
				<< "The file will be deleted!"
				<< std::endl;

			//Give the user time to react
			std::this_thread::sleep_for(std::chrono::seconds(5));

			//Now we delete the file
			::boost::filesystem::remove(dumpFilePath);
		}
		else
		{
			//The file exists, but no force was in place
			std::cerr
				<< "The file " << dumpFilePath << " already exists and --force was not given. "
				<< "Rename the file first."
				<< std::endl;
			std::exit(EXIT_FAILURE);
		};
	}; //End if: dump file path exists


	/*
	 * D U M P E R
	 */
	egd_dumperFile_t dumper(dumpFilePath);		//Create the dump file

	/* I G N O R I N G     P R O P E R T I E S  */

	//Allow the setup to inspect the dumper
	MarkerSetUp->inspectDumper(dumper);

	//Allow the rheology to inspect the dumper
	MarkerRehology->inspectDumper(dumper);


	if(confObject.typeOnlyFirst() == true)
	{
		std::cout
			<< "You have requested to dump the type only once.\n"
			<< "This option is depricated, and not needed.\n"
			<< "The type is autmatically added to the constant properties.";
	}; //End if: type only first

	if(confObject.onlyFullHeatingTerm() == true)
	{
		std::cout
			<< "Only the full heating term will be dumped."
			<< std::endl;

		dumper.onlyFullHeatingTerm();
	};

	if(confObject.noStrainRateDump() == true)
	{
		std::cout
			<< "The strain rate will not be dumped."
			<< std::endl;

		dumper.noStrainRateDump();
	};

	if(confObject.noStressDump() == true)
	{
		std::cout
			<< "The stress will not be dumped."
			<< std::endl;

		dumper.noStressDump();
	};

	if(confObject.dumpOnlyRep() == true)
	{
		std::cout
			<< "Only grid properties that are representants will be dumped."
			<< std::endl;

		dumper.onlyDumpRepOnGrid();
	};

	if(confObject.noTempSolDump() == true)
	{
		std::cout
			<< "Dumping of temperature solution is disabled."
			<< std::endl;

		dumper.noTempSol();
	};

	if(confObject.markerDumpProb() != 1)
	{
		const auto df = confObject.markerDumpProb();
		std::cout
			<< "A non standard dump probability is used, it is " << df
			<< std::endl;

		dumper.markerDumpProbability(df);
	};

	if(confObject.dumpMarkerVel() == false)
	{
		std::cout
			<< "Ignoring of the (intrinsic) marker velocities."
			<< std::endl;
		dumper.ignoreMarkerVel();
	}
	else
	{
		if(dumper.markerVelAreDumped() == false)
		{
			std::cout
				<< "Requested to dump the intrinsic marker velocities. "
				<< "But their dumping is deactivated!";
		}
		else
		{
			std::cout
				<< "Dumping of the (intrinsic) marker velocities.";
		}

		std::cout
			<< std::endl;
	};

	if(confObject.dumpFeelVel() == true)
	{
		std::cout
			<< "Dumping of the feel velocity."
			<< std::endl;
		dumper.dumpFeelVelocity();
	}
	else
	{
		if(dumper.feelVelAreDumped() == true)
		{
			std::cout
				<< "The ini file disabled the dumping of the feel velocity, but it will be dumped.";
		}
		else
		{
			std::cout
				<< "The feel velocities are not dumped.";
		};
		std::cout
			<< std::endl;
	};



	/*
	 * ===========================
	 * Simulation constraints
	 * These are constraints fo the simulation and other options
	 */


	/* Length of the different Phases, counted in steps */

	//Number of setup steps, the maximal allowed timestep will increase
	//linearly towards the maximum; this is after the tiny step phase.
	//This is also called the ramp time/phase.
	const Size_t  nSetupSteps =  confObject.nRampSteps();

	//This is the number of supper tiny moves; included in the normal tiny moves
	//During these steps the timestep is extremly small
	//Note that this phase is the merge that results when we have merged the
	//tiny step phase and the super tiny step phase.
	const Size_t nSuperTinySteps = confObject.nSuperResTimeSteps();

	//This checks are needed for the dump control.
	if(nSuperTinySteps <= 0)
	{
		throw PGL_EXCEPT_InvArg("At leas one super restricted step must be performd.");
	};
	if(nSetupSteps <= 0)
	{
		throw PGL_EXCEPT_InvArg("At least one ramp step must be performed.");
	};


	/* Tiome step length ionformation */

	//Maximal length of a time step
	const Numeric_t maxTimeStepLength    = confObject.getMaxTimeStepSize();

	//The minimal allowed timestep length
	const Numeric_t minTimeStepLength    = confObject.getMinTimeStepSize();

	//This is the maximal timestep length during the super tiny steps. in SECONDS
	//This is an alias of the minimum time.
	const Numeric_t maxSuperTinyTimeStepLength = minTimeStepLength;

	//this is the initial intertial time step length.
	const Numeric_t initialInertialTimeStepLength = confObject.getInitialInertiaTimeStep();


	/* Handle the end of teh simulation times */

	//this is a variable that is used as a dumy
	Size_t nTimeSteps_SETUPTEMP = std::numeric_limits<Size_t>::max();

	//this is the tentative final time that is used
	Numeric_t finalTimeTenative = confObject.getFinalSimTime();

	//this bool indicates if time controlled looping is used
	bool useTCLooping = false;

	//Now we make the distinction if we found it or not
	if(finalTimeTenative <= 0.0)
	{
		//We have not found the final time, so we will
		//use the timestep limit; Note that this will generate an error if not given
		nTimeSteps_SETUPTEMP = confObject.nTimeSteps();
		useTCLooping         = false;
		finalTimeTenative    = std::numeric_limits<Numeric_t>::max();
	}
	else
	{
		//We are here to use the final time
		useTCLooping         = true;
	};

	//
	//Now we load everything into constant variables
	const Size_t    nTimeSteps         = nTimeSteps_SETUPTEMP;
	const Numeric_t finalTime          = finalTimeTenative;
	const bool      useTimeCtrlLooping = useTCLooping;



	/* Restrictions */

	//Maximal change in temperature; expressed in Kelvin
	const Numeric_t maxTempChnage        = 20.0;

	//Maximal movement of a marker; expressed in grid size fraction
	const Numeric_t maxMarkerMoveX 		= confObject.getMaxDisplacementX();
	const Numeric_t maxMarkerMoveY 		= confObject.getMaxDisplacementY();

	//This is the maximal movement of markers in during the tiny step phace
	const Numeric_t maxTinyMarkerMove = 0.0001;

	/* External dump control */
	const bool      useTimeDumping      = confObject.useTimeBasedDumping();
	const bool      useStepDumping      = confObject.useStepBasedDumping();
	const Numeric_t minimalDumpSepTime  = confObject.getMinimalDumpIntervalTime();	//Time between two dumps
	const Size_t	minimalDumpSepSteps = confObject.getDumpingStepInterval();	//Min number of steps between dump
	Numeric_t lastDumpTime 		    = -100.0;					//Last time we performed a dump
	Size_t   lastDumpStep               = 0;					//Last step idx where we performed a dump

	if(!(useTimeDumping || useStepDumping))
	{
		std::cout
			<< "Neither activated time or step based dumping.\n"
			<< "Thus every step will be dumped.\n\n";
	}; //End if: output warning about dumping


	/* Physical settings */

	//This is the gravity in y direction, expressed in [m / s^2]
	//Note it is positive because y axis points downwards
	const Numeric_t gravY = confObject.setUpGravY();	pgl_assert(isValidFloat(gravY));
	const egd_gravForce_t grav(0.0, gravY);
		pgl_assert(grav.isZeroX());

	//DO we use density stabilization
	const bool useDensityStabilization = confObject.useDensityStabilization();



	/*
	 * Print out the variables
	 */
	std::cout
		<< "SetUp:\n"
		<< MarkerSetUp->print()
		<< "\n\n"

		<< "Integrator:\n"
		<< MarkerIntegrator->print()
		<< "\n\n"

		<< "MarkerToGrid:\n"
		<< MarkersToGrid->print()
		<< "\n\n"

		<< "StokesSolver:\n"
		<< StokesSolver->print()
		<< "\n\n"

		<< "TemperatureSolver:\n"
		<< TempSolver->print()
		<< "\n\n"

		<< "GridToMarkers:\n"
		<< GridToMarkers->print()
		<< "\n\n"

		<< "Rheology:\n"
		<< MarkerRehology->print()
		<< "\n\n"

		<< "Integrator:\n"
		<< MarkerIntegrator->print()
		<< "\n\n"

		<< "ApplyBC:\n"
		<< ApplyBC->print() 	/* note will later be moved into the grid container */
		<< "\n\n"

		<< std::flush;


	/* Outputing timestepping information */
	std::cout
		<< "Timestepping and Restriction information:\n"
		<< "\t" << "nTinySteps:             " <<            nSuperTinySteps                << "\n"
		<< "\t" << "nSetupSteps:            " <<            nSetupSteps                    << "\n"
		<< "\t" << "maxMarkerMoveX:          " <<           maxMarkerMoveX                 << " h_{x}\n"
		<< "\t" << "maxMarkerMoveY:          " <<           maxMarkerMoveY                 << " h_{y}\n"
		<< "\t" << "maxTinyMarkerMove:      " <<            maxTinyMarkerMove              << " h_{x,y}\n"
		<< "\t" << "maxTempChnage:          " <<            maxTempChnage                  << " K\n"
		<< "\t" << "minTimeStep:            " << outputTime(minTimeStepLength            ) << "\n"
		<< "\t" << "maxTimeStep:            " << outputTime(maxTimeStepLength            ) << "\n"
		<< "\t" << "tinyTimeStep:           " << outputTime(maxSuperTinyTimeStepLength   ) << "\n"
		<< "\t" << "initialInertiaTimeStep: " << outputTime(initialInertialTimeStepLength) << "\n";

	if(useTimeCtrlLooping == true)
	{
	std::cout
		<< "\t" << "finalTime:              " << outputTime(finalTime                    ) << "\n";
	}
	else
	{
	std::cout
		<< "\t" << "nTimeStep:              " <<            nSetupSteps                    << "\n";
	};
	std::cout
		<< std::endl;




	/*
	 * Simulation controll variables
	 */

	//The current simulation time
	Numeric_t currentTime = 0.0;

	//This is the time step, is set to the super tyiny timestep
	Numeric_t timeStep = 0.0;

	//this is the last time step, note that it is only needed for inertia
	Numeric_t lastIntertiaTimeStep = initialInertialTimeStepLength;

	//This is the grid
	GridContainer_t grid;

	//This is the marker collection
	MarkerCollection_t  markerColl;

	//Create the setting
	std::tie(grid, markerColl) = MarkerSetUp->creatProblem();

	//Load the boundary object into the grid
	grid.loadApplyBC(std::move(ApplyBC));

	//Load the geometry
	const GridContainer_t::GridGeometry_t& gridGeo = grid.getGeometry();

	//Enable density stailization in the argument
	if(useDensityStabilization)
	{
		StokesArg->enableDensityStab();
			pgl_assert(StokesArg->isDensityStabOn(),
				   StokesArg->getDensityStabTime() == 0.0);
	};// End if: enable density stabilization


	//
	//Load some grid properties
	const Size_t    nMarkers = markerColl.nMarkers();	//How many markers do we have.
	const Size_t    Nx       = gridGeo.xNPoints();		//Number of grid points in x direction.
	const Size_t    Ny       = gridGeo.yNPoints();		//Numbers of grid points in y direction.
	const Numeric_t DeltaX   = gridGeo.getXSpacing();	//Grid spacing in x direction.
	const Numeric_t DeltaY   = gridGeo.getYSpacing();	//Grid spacing in y direction.

	std::cout
		<< "Grid was created with the following properties:\n"
		<< " Nx       = " << Nx       << "\n"
		<< " Ny       = " << Ny       << "\n"
		<< " nMarkers = " << nMarkers << "\n"
		<< " DeltaX   = " << DeltaX   << "\n"
		<< " DeltaY   = " << DeltaY   << "\n"
		<< std::endl;

	/*
	 * Pefrom a first mapping from the markers to the
	 * grid. This is needed such that the init state
	 * has usefull values on the grid.
	 */
	MarkersToGrid->mapToGrid(markerColl, &grid);


	/*
	 * Map the markers to the grid, before the initial dump.
	 * This is needed to make sure that the initial state is
	 * not all zero.
	 */
	MarkersToGrid->mapToGrid(markerColl, &grid);


	/*
	 * Write the initial data to the dump file
	 */
	dumper.dumpInit(grid, markerColl);


	//Start main timer
	pgl::pgl_simple_timer_t TIMER_GLOBAL;
	TIMER_GLOBAL.start();


	//This is a phased event counter object.
	//It is used to count the number of times the minimal time steps has to be enforced.
	//Meaning tha timestep was löarger than other restrictions would suggests
	pgl::pgl_phasedEventCounter_t minTimeExtensionCounter(100);

	//This is the event counter for the dumping
	pgl::pgl_phasedEventCounter_t dumpingEventCounter(100);


	/*
	 * The simulation loop
	 */
	Size_t timeStepIdx      = 0;
	bool   probeStepWasDone = false;	//indicated if the probe step was done
	while(true)
	{
		//This timer is used for measuring the time spend for jobs
		pgl::pgl_simple_timer_t jobTimer;

		//Only update the inertia time, if we are not in the first run
		//Thus we will not override the initial value
		if(timeStepIdx > 0)
		{
			//This is currently the last one
			lastIntertiaTimeStep = timeStep;
		}; //End if: not first one, so we have to update it
			pgl_assert(lastIntertiaTimeStep > 0.0);

		std::cout
			<< "\n\n=============================\n";
		if(useTCLooping == false)
		{
			std::cout
			<< "Timestep:     " << timeStepIdx             << " of " << (nTimeSteps - 1) 	  << "\n"
			<< "Current time: " << outputTime(currentTime) 					  << "\n";
		}
		else
		{
			std::cout
			<< "Timestep:     " << timeStepIdx                                                << "\n"
			<< "Current time: " << outputTime(currentTime) << " of " << outputTime(finalTime) << "\n";
		}
		std::cout
			<< "Time step:    " << outputTime(timeStep)    << " (old time step)" 		  << "\n"
			<< "InertiaTime:  " << outputTime(lastIntertiaTimeStep) 			  << "\n"
			<< "Density Stab: " << (useDensityStabilization ? "ON" : "OFF")      		  << "\n"
			<< std::endl;



		/*
		 * Map the Markers to the grid
		 */
			jobTimer.start();
		MarkersToGrid->mapToGrid(markerColl, &grid);
			jobTimer.stop();
		const Numeric_t thisMaxDensity   = grid.cgetProperty(PropIdx::Density()  ).maxCoeff();
		const Numeric_t thisMinDensity   = grid.cgetProperty(PropIdx::Density()  ).minCoeff();
		const Numeric_t thisMaxViscosity = grid.cgetProperty(PropIdx::Viscosity()).maxCoeff();
		const Numeric_t thisMinViscosity = grid.cgetProperty(PropIdx::Viscosity()).minCoeff();
		std::cout
			<< "Mapping the markers to the grid took: " << outputTime(jobTimer.get_timing()) << "\n"
			<< "RHO_{max} = " << thisMaxDensity   << " kg/m^3" << "\n"
			<< "RHO_{min} = " << thisMinDensity   << " kg/m^3" << "\n"
			<< "ETA_{max} = " << thisMaxViscosity << " Pa s" << "\n"
			<< "ETA_{min} = " << thisMinViscosity << " Pa s" << "\n"
			<< std::endl;


		/*
		 * Solve the stokes equation
		 */
		//Onyl update the density time if enabled
		if(useDensityStabilization == true)
			{StokesArg->setDensityStabTime(timeStep);};
		StokesArg->g_x(  0.0);				//update stokes solver argument.
		StokesArg->g_y(gravY);
		StokesArg->setStateIdx(timeStepIdx);
		StokesArg->setCurrentTime(currentTime);

		//if it is an inertial solver, update the time step
		if(StokesSolver->isInertiaSolver() == true)
			{(void)StokesArg->setPrevDT(lastIntertiaTimeStep);};

			jobTimer.start();
		StokesSolver->solve(grid, StokesArg, StokesResult);
			jobTimer.stop();
			pgl_assert(StokesResult->isFinite());

		std::cout
			<< "Stokes solver needed: " << outputTime(jobTimer.get_timing())
			<< std::endl;


		/*
		 * Here we adjust the timestep.
		 * This timestep asjustman is purely done on the basis of
		 * the Stokes solution, temperature will be incooperated
		 * later. This is also used to bootstrap the solution.
		 *
		 * !!!!! We will override the timestep here!
		 */
		timeStep = maxTimeStepLength;	//Set the timesetp to the maximum allowed value, we will reduce it later


		/*
		 * Here we will reduce the timestep
		 * We will do this by checking how much the markers
		 * will be moved, if they are displaced to much, then
		 * we will reduce the timestep.
		 */

		Numeric_t thisMaxChangeX = 0;	//This is the variable, we use for storing the current maximum
		Numeric_t thisMaxChangeY = 0;	//	Marker movement in this iteration.


		//
		//Depending on the Phase we will use different restrictions.
		if(timeStepIdx <= nSuperTinySteps)
		{
			std::cout
				<< "Super restricted timestepping is in place, adjusted the time step.\n"
				<< "\t" << outputTime(timeStep) << " -> " << outputTime(maxSuperTinyTimeStepLength)
				<< std::endl;
			timeStep = maxSuperTinyTimeStepLength;	//Note this will be done below again. because of the minimum time

			thisMaxChangeX = maxTinyMarkerMove;		//Set the maximal movement
			thisMaxChangeY = maxTinyMarkerMove;
		}
		else if(timeStepIdx <= (nSuperTinySteps + nSetupSteps))
		{
			//We are in the setup step phase
			std::cout
				<< "We are in the addaptive phase of the simulation."
				<< std::endl;

			//Calculate the linear addapting
			thisMaxChangeX = ((maxMarkerMoveX - maxTinyMarkerMove) / nSetupSteps) * (timeStepIdx - nSuperTinySteps) + maxTinyMarkerMove;
			thisMaxChangeY = ((maxMarkerMoveY - maxTinyMarkerMove) / nSetupSteps) * (timeStepIdx - nSuperTinySteps) + maxTinyMarkerMove;

			//Ensure that we are not too large
			thisMaxChangeX = std::min(maxMarkerMoveX, thisMaxChangeX);
			thisMaxChangeY = std::min(maxMarkerMoveY, thisMaxChangeY);
		}
		else
		{
			//We are in the normal phase, so just the default
			thisMaxChangeX = maxMarkerMoveX;
			thisMaxChangeY = maxMarkerMoveY;
		}; //End: no tine move

		//
		//Finding the maximal velocities
		const Numeric_t maxVx = StokesResult->getMaxAbsVx();
		const Numeric_t maxVy = StokesResult->getMaxAbsVy();

		std::cout
			<< "  VX(MAX) = " << maxVx << " m/s" << "\n"
			<< "  VY(MAX) = " << maxVy << " m/s"
			<< std::endl;

		//
		//Check the X direction for movements
		const Numeric_t maxMoveX = maxVx * timeStep / DeltaX;		//Note timestep is still the maximu or super restricted.
			pgl_assert(isValidFloat(maxMoveX), maxMoveX >= 0.0);
			//Y movement must be computed after the x restrction was applied

		//
		//Addapting the timestep depending on the move the markers do
		if(maxMoveX >= thisMaxChangeX)
		{
			const Numeric_t oldDT = timeStep;			//Store the old timesetp
			timeStep = (thisMaxChangeX * DeltaX / maxVx) * 0.95;	//Adjust the timestep
				pgl_assert(oldDT >= timeStep);

			std::cout
				<< "Movent in x is too fast, it was " << maxMoveX << "h_{x}, but maximum is " << thisMaxChangeX << "h_{x}; modify timestep " << "\n"
				<< "\t" << outputTime(oldDT) << " -> " << outputTime(timeStep)
				<< std::endl;
		}; //End if: addapting x
			pgl_assert(timeStep >= 0.0);


		//
		//Check the Y direction for movements
		const Numeric_t maxMoveY = maxVy * timeStep / DeltaY;	//Note we have already restricted the timestep
			pgl_assert(isValidFloat(maxMoveY), maxMoveY >= 0.0);

		//Test if y forces us to reduce the timestepping further
		if(maxMoveY >= thisMaxChangeY)
		{
			const Numeric_t oldDT = timeStep;			//Store the old timesetp
			timeStep = (thisMaxChangeY * DeltaY / maxVy) * 0.95;	//Adjust the timestep
				pgl_assert(oldDT >= timeStep);

			std::cout
				<< "Movent in y is too fast, it was " << maxMoveY << "h_{y}, but maximum is " << thisMaxChangeY << "h_{y}; modify timestep " << "\n"
				<< "\t" << outputTime(oldDT) << " -> " << outputTime(timeStep)
				<< std::endl;
		}; //End if: addapting for y
			pgl_assert(timeStep >= 0.0);


		//
		//Test if the timestep was too small, we only apply this in the case we are not in the setup phase
		if((timeStepIdx > (nSuperTinySteps + nSetupSteps)  ) &&    //Only take action if we not in the super rest phase or setup phase
		   (timeStep    <  minTimeStepLength               )    )  //Prevent too small time steps.
		{
			std::cout
				<< ">>> !The timestep has become too small, so we will set it to the minimum:\n"
				<< "\t" << outputTime(timeStep) << " -> " << outputTime(minTimeStepLength)
				<< std::endl;

			timeStep = minTimeStepLength;

			minTimeExtensionCounter.recordEvent();
		} //End if: handling too small timestep
		else
		{
			minTimeExtensionCounter.recordNothing();
		};

		//End of timesteping restriction from momentum equation.
		//Note that the temperature equation also can lead to a change in timestep.
		//But it will only shorten it.


		/*
		 * Calculate stess and strain rate
		 */
		const egd_deviatoricStrainRate_t StrainRate(grid.getGeometry(), *StokesResult);
		const egd_deviatoricStress_t     Stress(grid, StrainRate);

		/*
		 * Calculate the heating term
		 */
		const egd_heatingTerm_t HeatingTerm(
				grid,				//Grid properties
				StrainRate, Stress,		//Stress, Strain
				*StokesResult,			//Velocity
				grav,				//Gravitation
				TempSolver->onExtendedGrid() );	//Is the grid extended or not.

		/*
		 * Solve the temperature equation
		 */
		TempArg->setDT(timeStep);		//Updating the argument
		TempArg->setCurrentTime(currentTime);
		TempArg->setStateIdx(timeStepIdx);

			jobTimer.start();
		TempSolver->solve(grid, HeatingTerm, TempArg, TempResult);
			jobTimer.stop();
			pgl_assert(TempResult->isFinite());

		std::cout
			<< "The temperature equation took: " << outputTime(jobTimer.get_timing())
			<< std::endl;


		/*
		 * Test if we have to addapt the timestep again
		 * to meet the temperature restriction
		 */

		const Numeric_t thisMaxTempChange = TempResult->getLargestChange();	//Get the largest change

		if(thisMaxTempChange > maxTempChnage)
		{
			const Numeric_t preChangeTimeStep = timeStep;

			//Compute a new timestep, assume linear change
			timeStep = 0.95 * timeStep * maxTempChnage / thisMaxTempChange;

			std::cout
				<< "Measured a too large temperature change of " << thisMaxTempChange << "K, adapted timestep\n"
				<< "\t" << outputTime(preChangeTimeStep) << " -> " << outputTime(timeStep)
				<< std::endl;

			//
			//We have to recompute the temperature solution
			//heating term is not affected, but update the argument
			TempArg->setDT(timeStep);
			TempArg->setStateIdx(timeStepIdx);	//technically not needed, but nicer.
			TempArg->setCurrentTime(currentTime);

				jobTimer.start();
			TempSolver->solve(grid, HeatingTerm, TempArg, TempResult);
				jobTimer.stop();
				pgl_assert(TempResult->isFinite());

			const Numeric_t newMaxTempChange = TempResult->getLargestChange();
			std::cout
				<< "New temperature change is: " << newMaxTempChange << "K." << "\n"
				<< "Solving the temperature again took: " << outputTime(jobTimer.get_timing())
				<< std::endl;
		}; //End if: addapting because of maximal temp change

		/*
		 * We now output the final decission about the time step
		 *
		 * We do this here after the temperature equation was solved.
		 * This is because this equation can also lead to a reduction
		 * in timestep, but only a reduction!
		 */
		{
			std::cout
				<< "\n"
				<< "Final time step size is: " << outputTime(timeStep) << "\n"
				<< "\t" << "Maximal X movement: " << pgl::pgl_cutFloat(timeStep * maxVx / DeltaX, 3) << "h_{x}\n"
				<< "\t" << "Maximal Y movement: " << pgl::pgl_cutFloat(timeStep * maxVy / DeltaY, 3) << "h_{y}"
				<< "\n"	<< std::endl;
		}; //End: printing the timestep and movement


		/*
		 * Probe step
		 *
		 * The prope step is only needed when we have an inertial problem.
		 * It is quite hard to find a time step, wo we use the simulation
		 * itself to find one. We will start with the minimal time step
		 * and do one step. Then we will test if we are an interial solver.
		 * If so we will finish teh iteration here without increasing the
		 * timestep. Thus we have a restricted timestep and will redo the
		 * first iteration.
		 */
		if((timeStepIdx == 0) && (probeStepWasDone == false))
		{
			//
			//We are in the first loop, so this will be considered as probing step, regardless what we do
			probeStepWasDone = true;

			if(StokesSolver->isInertiaSolver() == true)
			{
				//
				//We have an inertia problem so lets exit the loop here
				std::cout
					<< "\n\n"
					<< "P R O B I N G  S T E P \n"
					<< "This iteration is a probe iteration. It will now be stoped without dumping"
					   " or moving the markers or state.\n"
					   " This iteration has never existed." "\n\n"
					<< std::endl;

				continue;	//without increasing the index
			};
		}; //End if: first loop

		/*
		 * DECIDE To exit the loop
		 *
		 * We only make the decision if this is the last iteration
		 * or not. This allows us to perform certain cleaup tasks.
		 * However we will break at the end of the loop.
		 */
		bool isLastLoop = false;	//currently not the last

		/*
		 * Test if a signal was raised and we have to exit
		 */
		if(egd_signalWasRaised() == true)
		{
			std::cout
				<< "\n\n"
				<< "P R E M T I V E   Exit\n"
				<< "was detected. The iteration ends now, final clean up state will be performed."
				<< std::endl;
			isLastLoop = true;
		}; //End if: sig raised


		/*
		 * Test if ewe have reached the timecontrolled looping end
		 */
		if(currentTime >= finalTime)
		{
				pgl_assert(useTimeCtrlLooping == true);	(void)useTimeCtrlLooping;
			isLastLoop = true;
		};//End break out from the loop

		/*
		 * Test if all iterations are done
		 */
		if(timeStepIdx == nTimeSteps)
		{
			isLastLoop = true;
		};//end if: is last time step


		/*
		 * Compute the feel velocity
		 * This is needed such that the velocity is aviable upon dumping.
		 * Since we have mass less markers there is no problem
		 * with a more complexed material model
		 */
		if(MarkerIntegrator->computeFeelVel() == true)
		{
			jobTimer.start();
			MarkerIntegrator->storeFeelVel(
					&markerColl,
					grid,
					*StokesResult,
					*TempResult,
					timeStep,
					timeStepIdx,
					currentTime);
			jobTimer.stop();

			std::cout
				<< "Computiung the feel velocity took: " << outputTime(jobTimer.get_timing())
				<< std::endl;
		}; //End scope: computing the feel velocities


		/*
		 * Dump the state
		 *
		 * We do this before we move the markers arround.
		 * This function will also class flush, which should write to disc.
		 *
		 * Note that we do not unconditionally dump. During the super restricted phase
		 * and the adaptive/ramp phase we dump any state, but afterwards we do not.
		 * We only dump the if a certain time between the two state has passed or
		 * a certain number of steps has passed between then.
		 *
		 * If a new version was created, we must perform a dump.
		 * Also if *this is the last loop.
		 */
		if(                   (timeStepIdx <= (nSuperTinySteps + nSetupSteps)        )           ||	//We must not be in teh addaptive phase
                   ( useTimeDumping ? ((currentTime - lastDumpTime) >= minimalDumpSepTime    ) : false ) ||
                   ( useStepDumping ? ((timeStepIdx - lastDumpStep) >= minimalDumpSepSteps   ) : false ) ||
                                      ((useStepDumping == false) && (useTimeDumping == false))           ||
                   ((grid.isNewVersion() == true) || (markerColl.isNewVersion() == true)     )		 ||	//New version
                   (isLastLoop == true                                                       )			//last iteration
		  )
                {
                	//
                	//Perform the dump
			jobTimer.start();
			dumper.dumpState(
				grid,
				markerColl,
				Stress,
				StrainRate,
				*StokesResult,
				*TempResult,
				HeatingTerm,
				timeStepIdx,
				currentTime,
				timeStep);
			jobTimer.stop();

			//How long did it took
			std::cout
				<< "The dumper took: " << outputTime(jobTimer.get_timing())
				<< "\n";

			//update dump counters
			lastDumpStep = timeStepIdx;
			lastDumpTime = currentTime;
			dumpingEventCounter.recordEvent();
		//End if: dump has happened
		}
		else
		{
				pgl_assert(useTimeDumping || useStepDumping);
			std::cout
				<< "No dump happened in this iteration.\n";
			if(useTimeDumping)
			{
				std::cout
					<< "\t" << "Time since the last dump:  " << outputTime(currentTime - lastDumpTime)
						<< ";   minimal time: " << outputTime(minimalDumpSepTime) << "\n";
			}
			if(useStepDumping)
			{
				std::cout
					<< "\t" << "Steps since the last dump: " << (timeStepIdx - lastDumpStep)
						<< ";   minimal step: " << minimalDumpSepSteps << "\n";
			};

			//Record the event.
			dumpingEventCounter.recordNothing();
		}; //End else, dump controll
		std::cout << std::endl;


		/*
		 * Map the grid properties back to the markers
		 */
		GridToMarkers->mapBackToMarkers(
				&markerColl,
				grid,
				*StokesResult,
				*TempResult,
				timeStep,
				timeStepIdx,
				currentTime);


		/*
		 * Apply the non linear rheology
		 */
		MarkerRehology->modifyMarkers(
				&markerColl,
				grid,
				*StokesResult,
				*TempResult,
				Stress,
				StrainRate,
				timeStep,
				timeStepIdx,
				currentTime);


		/*
		 * Move markers
		 */
		jobTimer.start();
		MarkerIntegrator->moveMarkers(
				&markerColl,
				grid,
				*StokesResult,
				*TempResult,
				timeStep,
				timeStepIdx,
				currentTime);
		jobTimer.stop();

		std::cout
			<< "Moving the markers took: " << outputTime(jobTimer.get_timing())
			<< std::endl;


		/*
		 * Update time and iteration index
		 */
		{
			const Numeric_t oldCurrentTime = currentTime;
			currentTime += timeStep;
			timeStepIdx += 1;
				pgl_assert(currentTime > oldCurrentTime);
				pgl_onlyForDebugging(oldCurrentTime);
		}; //End scope: updateing time


		/*
		 * Instruct the containrs of the end of the iteration.
		 */
		grid.endOfIteration();
		markerColl.endOfIteration();


		/*
		 * Now we exit the loop if we need to
		 */
		if(isLastLoop == true)
		{
			break;
		}; //End if: exit loop
	}; //End while(true): simulation loop


	/*
	 * Now we have to dump the final state
	 */
	dumper.dumpFinal(markerColl, currentTime, timeStepIdx);


	//Stop the global timer
	TIMER_GLOBAL.stop();

	//Close the file if it was open
	if(dumper.isOpen() == true)
	{
		dumper.closeFile();
	};


	/*
	 * Print the timestep extension
	 */
	std::cout
		<< "\n\n"
		<< "Statistc about the extension of the timestep length, such that the minimum was used."
		<< minTimeExtensionCounter
		<< std::endl;

	/*
	 * Print the dumping counter
	 */
	if((useStepDumping || useTimeDumping           ) &&
	   (useStepDumping && (minimalDumpSepSteps > 1))   )
	{
		std::cout
			<< "\n"
			<< "Statistic about the dumping:\n"
			<< dumpingEventCounter
			<< std::endl;
	}; //End if:


	/*
	 * Print an end message
	 */
	std::cout
		<< "\n\n\n\n=============================\n"
		<< "Simulation is finisched after " << timeStepIdx << " steps." << "\n"
		<< "The time is " << outputTime(currentTime) << "\n"
		<< "The simulation took " << outputTime(TIMER_GLOBAL.get_timing()) << " to complete."
		<< std::endl;

	return 0;
}; //End main



