/**
 * \brief	This program tests the correct working of the grid container.
 */


//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/stringize.hpp>


//Include STD
#include <iterator>
#include <string>
#include <iostream>


//We use the namespace
using namespace egd;

int
main(
	int 	argc,
	char**	argv)
{
#if 0
	//Create a small property list
	using PropList = egd_markerCollection_t::PropList_t;
	using PropIdx  = egd_markerCollection_t::PropIdx_t;
	PropList propList = {PropIdx::Type(), PropIdx::PosX(), PropIdx::PosY()};
	const auto NM = 4;
	const auto NX = 2;	//Grid points, so we have one cells
	const auto NY = 3;	//Grid points, so we have two cells
	const auto NCell = 2;
	const Numeric_t xStart = 1, xEnd = 2, yStart = 0, yEnd = 2;

	//Create a collection
	egd_markerCollection_t mColl(NM, propList);

	//create the grid, this will create a regular basic nodal grid.
	egd_gridContainer_t grid(NX, NY, mColl.getDefaultPropToGridMap() );

	if(grid.nProperties() != 1)
	{
		std::cerr << "Wrong number of properties." << std::endl;
		pgl_assert(false);
		return 1;
	};

	if(grid.begin()->first != propList.at(0))
	{
		std::cerr << "Wrong property was imported." << std::endl;
		pgl_assert(false);
		return 1;
	};

	//Create a grid
	grid.makeConstantGrid(xStart, xEnd, yStart, yEnd);

	//Try if a mutable reference can be obtained
	{
		bool throwed = false;
		try
		{
			(void)(grid.getMutableXPoints());
		}
		catch(...)
		{
			throwed = true;
		};

		if(throwed == false)
		{
			std::cerr << "Access to mutable reference is still possible." << std::endl;
			pgl_assert(false);
			return 1;
		};
	}; //End

	if(grid.isConstantGrid() == false)
	{
		std::cerr << "Grid is not constant." << std::endl;
		pgl_assert(false);
		return 1;
	};
	if(grid.isGridSet() == true)
	{
		std::cerr << "Grid is already set." << std::endl;
		pgl_assert(false);
		return 1;
	};

	//Finalize
	grid.finalizeGrids();
	if(grid.isGridSet() == false)
	{
		std::cerr << "Grid was not finalized." << std::endl;
		pgl_assert(false);
		return 1;
	};

#define P(x) #x << " = " << x << "\n"
	std::cout
		<< "Geometry of domain:\n"
		<< P(xStart)
		<< P(xEnd)
		<< P(yStart)
		<< P(yEnd)
		<< std::endl;
#undef P

	std::cout << "Domain:\n";
	std::cout
		<< "xGrid = " << grid.getXPoints().transpose() << "\n"
		<< "yGrid = " << grid.getYPoints().transpose() << "\n"
		<< "xDelta: " << grid.getXSpacing() << "\n"
		<< "yDelta: " << grid.getYSpacing()
		<< std::endl;




	std::cout
		<< "\n\n"
		<< "All tests in this file has been passed."
		<< std::endl;
#else
	std::cerr
		<< "The test is deactivated." << "\n"
		<< "Since most of the complexity was just moved to the geometry and the container is now basically an ordinary container, this test will probably not be backported."
		<< std::endl;
#endif
	return 0;
}; //End main



