/**
 * \brief	 This is a test program to test the regular temperature solver.
 *
 * This prgram uses the function that is provided by the
 * temperature tester implementation. See that file for
 * more information.
 */

/*
 * We want to test the regular solver, so we need to define it,
 * before we include the file.
 */
#define IS_REG

#include "./testImpl_tempSolver.hpp"

int
main(
	int 	argc,
	char**	argv)
{
	/*
	 * This is the driver code that performs several experiments.
	 * In all experiments the full set is used meaning the default
	 * values are used.
	 */

	/*
	 * COMMENT ON THE ADIABATIC HEATING
	 * The adiabatic heating is very strange and also sometimes missleading.
	 * Despite the fact that that it depends on VY only it crutialy depends
	 * on the number of X points! The reason is that the velocity at the
	 * basic nodal point is needed, which is computed by averaging the
	 * two VY nodes that are right and left of the node. The more nodes
	 * in x direction are present, the better the approximation.
	 *
	 * Thus some experiments are done with and without adiabatic heat enabled.
	 */


	/*
	 * 	STANDARD SIZE
	 *
	 * This is teh standard experimental size.
	 * Since Y is twice as long as X this reflects
	 * in the number of grid points.
	 * The number of grid points is increased exponentially.
	 */

	//Number of experiments
	const Size_t 	nExperiments 		= 8;
	const Size_t 	nMinGridX 		= 1 << 2;

	//This is the number of grid points that we want in X
	const intVec_t stdNx_vec = [nMinGridX, nExperiments]() -> intVec_t
			{  intVec_t Nx; Nx.push_back(nMinGridX);
			   for(Size_t i = 1; i != nExperiments; ++i)	//We have to start at one, for accounting the added one
				{ Nx.push_back(Nx.back() * 2 ); };
			   return Nx; (void)nExperiments;
			}();
	const intVec_t stdNy_vec = [](intVec_t Nx) -> intVec_t
			{  for(Size_t& i : Nx)
				{ i *= 2; };
			   return Nx;
			}(stdNx_vec);

	/*
	 * =============================
	 * 	EXPERIMENT 1
	 *
	 * In this test we have only a linear temperature profile.
	 * Also everything is constant or deactivated.
	 * This means we sould be able to solve the grid up to
	 * machine precision.
	 */
	{
		//This is the filename for dumping the experiment
		const std::string DUMP_FILE = "regularTempTest_linProfConst.hdf5";
		const std::string COMMENT   = "Linear profile, everything constant";

		performTest(stdNy_vec, stdNx_vec,
				DUMP_FILE, COMMENT,
				 0.0, 	//ETA             ~ shearheat
				 0.0,	//Therm expansion ~ adiabatic heat
				 0.0, 	//alphaS
				 0.0, 	//alphaK
				10.0,	//betaK  ~ Value of the thermal conductivity
				 0  );	//Hills in temp curve.
	}; //End: experiment 1


	/*
	 * =============================
	 * 	EXPERIMENT 2
	 *
	 * In this test we have a linear temnperature profiles
	 * with a sinus wave on top, everything is still constant
	 * or deactivated. The solver is not al good as before,
	 * but should solve it very good, one should observe the
	 * order.
	 */
	{
		//This is the filename for dumping the experiment
		const std::string DUMP_FILE = "regularTempTest_waveProfConst.hdf5";
		const std::string COMMENT   = "Wave temperature profile, everything constant";

		performTest(stdNy_vec, stdNx_vec,
				DUMP_FILE, COMMENT,
				 0.0, 	//ETA             ~ shearheat
				 0.0,	//Therm expansion ~ adiabatic heat
				 2.0, 	//alphaS
				 0.0, 	//alphaK
				10.0,	//betaK  ~ Value of the thermal conductivity
				31  );	//Hills in temp curve.
	}; //End: experiment 2


	/*
	 * =============================
	 * 	EXPERIMENT 3
	 *
	 * In this test we have a wave temperature profile.
	 * And the thermal conductivity is variable now.
	 * The rest is still deactivated.
	 * We should observe a nice dcreasing.
	 */
	{
		//This is the filename for dumping the experiment
		const std::string DUMP_FILE = "regularTempTest_waveProfVariing.hdf5";
		const std::string COMMENT   = "Wave temperature profile, varing k";

		performTest(stdNy_vec, stdNx_vec,
				DUMP_FILE, COMMENT,
				 0.0, 		//ETA             ~ shearheat
				 0.0,		//Therm expansion ~ adiabatic heat
				 2.0, 		//alphaS
				 8.539734, 	//alphaK
				 0.0,		//betaK  ~ Value of the thermal conductivity
				31  );		//Hills in temp curve.
	}; //End: experiment 3


	/*
	 * =============================
	 * 	EXPERIMENT 4
	 *
	 * In this experiment we have a everything variable,
	 * we also have shear heating. We sould see decreasing.
	 */
	{
		//This is the filename for dumping the experiment
		const std::string DUMP_FILE = "regularTempTest_waveProfVariingShear.hdf5";
		const std::string COMMENT   = "Wave temperature profile, varing k, with shear heat";

		performTest(stdNy_vec, stdNx_vec,
				DUMP_FILE, COMMENT,
				 4.0, 		//ETA             ~ shearheat
				 0.0,		//Therm expansion ~ adiabatic heat
				 2.0, 		//alphaS
				 8.539734, 	//alphaK
				 0.0,		//betaK  ~ Value of the thermal conductivity
				31  );		//Hills in temp curve.
	}; //End: experiment 4


	/*
	 * =============================
	 * 	EXPERIMENT 5
	 *
	 * This experiment performs a full test.
	 * Meaning that everything is activated.
	 */
	{
		//This is the filename for dumping the experiment
		const std::string DUMP_FILE = "regularTempTest_full.hdf5";
		const std::string COMMENT   = "Extended solver test, everything is activated.";

		performTest(stdNy_vec, stdNx_vec,
				DUMP_FILE, COMMENT,
				 4.0, 		//ETA             ~ shearheat
				 4.0,		//Therm expansion ~ adiabatic heat
				 2.0, 		//alphaS
				 8.539734, 	//alphaK
				 0.0,		//betaK  ~ Value of the thermal conductivity
				31  );		//Hills in temp curve.
	}; //End: experiment 5



	/*
	 * =============================
	 * 	EXPERIMENT 6
	 *
	 * This experiment performs a only x refinement.
	 * Since the solution has no x dependency, nothing
	 * should happens, however the adiabatic heating
	 * is influenced, since we use the x points to average.
	 * So if their number is increased, they get closer
	 * and the error decreased.
	 * The heating is deactivated and thus we will not see
	 * anything.
	 *
	 * It is such that everything is dominated by teh error in y.
	 * One can see that by making it constant.
	 */
	{
		//This is the filename for dumping the experiment
		const std::string DUMP_FILE = "regularTempTest_xRefine.hdf5";
		const std::string COMMENT   = "X Refinement.";

		performTest(intVec_t(nExperiments, stdNy_vec.back() ), stdNx_vec,
				DUMP_FILE, COMMENT,
				 0.0, 		//ETA             ~ shearheat
				 0.0,		//Therm expansion ~ adiabatic heat
				 2.0, 		//alphaS
				 8.539734, 	//alphaK
				 0.0,		//betaK  ~ Value of the thermal conductivity
				31  );		//Hills in temp curve.
	}; //End: experiment 6


	/*
	 * =============================
	 * 	EXPERIMENT 7
	 *
	 * This is y refinement, meaning that we keep the number of
	 * x points constant and increas the number of y points.
	 * This should also lead to a decreasing since the solution
	 * is approximnated better
	 *
	 */
	{
		//This is the filename for dumping the experiment
		const std::string DUMP_FILE = "regularTempTest_yRefine.hdf5";
		const std::string COMMENT   = "Y Refinement.";

		performTest(stdNy_vec, intVec_t(nExperiments, stdNx_vec.back() ),
				DUMP_FILE, COMMENT,
				 0.0, 		//ETA             ~ shearheat
				 0.0,		//Therm expansion ~ adiabatic heat
				 2.0, 		//alphaS
				 8.539734, 	//alphaK
				 0.0,		//betaK  ~ Value of the thermal conductivity
				31  );		//Hills in temp curve.
	}; //End: experiment 7


	return 0;
}; //End main


