/**
 * \brief	This file implements a function that is able to test the
 * 		 Solver.
 *
 * The method of manufactured solution assumes a solution, in order to make the
 * assumed solution a real solution of teh problem, the problem is extended
 * with a specialy designed additionaly source term that will make that work.
 *
 * We assumes the following solutions:
 *
 * 	u_x := A_{x} \cdot \Sin{\frac{x}{l_x} }
 * 	u_y := A_{y} \cdot \Sin{\frac{y}{l_y} }
 * 	p   := A_{p} \cdot \Sin{ \frac{x}{w_x} + \frac{y}{w_y} } + \alpha_{p}
 *
 * In the above equations $l_x$ and $l_y$ are scalling constants, such that the
 * Dirichlet constant is satisfied, thus it holds $k \cdot \pi \cdot l_i == L_i$
 * where $ L_i $ is the spatial extension of the domain in direction $i$.
 * $w_x$ and $w_y$ does not have to fullfill a condition, but should be not too
 * large, such that the pressure fluctuations can be resolved.
 * The Neumann conditions are satisified by this choice for all constants.
 * A bit trikier is the conditions for the pressure.
 *
 * A very nice effect of the choice of the assumed solution is that,
 * $\partial_{x_i} u_j == 0$, if $i \not= j$, thus making the stress tensor
 * diagonal.
 *
 * Thus we have the following source terms:
 * 	S_x := -A_p \Cos{ \frac{x}{w_x} + \frac{y}{w_y} } * \frac{1}{w_x}
 * 	       + A_x \Cos{ \frac{x}{l_x} } * \frac{1}{l_x} * \partial_{x}[ \eta ] * 2
 * 	       - A_x \Sin{ \frac{x}{l_x} } * \frac{1}{{l_x}^2} * \eta * 2
 * 	       + \rho \cdot g_x
 * 	S_y := -A_p \Cos{ \frac{x}{w_x} + \frac{y}{w_y} } \cdot \frac{1}{w_y}
 * 	       + A_y \Cos{ \frac{y}{l_y} } * \frac{1}{l_y} * \partial_{y}[ \eta ] * 2
 * 	       - A_y \Sin{ \frac{y}{l_y} } * \frac{1}{{l_x}^2} * \eta * 2
 * 	       + \rho \cdot g_y
 * 	S_p :=   A_x \cdot \Cos{ \frac{x}{l_x} } \cdot \frac{1}{l_x}
 * 	       + A_y \cdot \Cos{ \frac{y}{l_y} } \cdot \frac{1}{l_y}
 *
 * The viscosity is described by the following formula:
 * 	\eta := a * (ArcTan[(x - mx)/cx] + bx) * (ArcTan[(y - my)/cy] + by)
 * There are also constants that are used.
 *
 * So the derivative can be written as:
 * 	\partial_{x}[\eta] := (a*(by + ArcTan[(-my + y)/cy]))/(cx*(1 + (-mx + x)^2/cx^2))
 * 	\partial_{y}[\eta] := (a*(bx + ArcTan[(-mx + x)/cx]))/(cy*(1 + (-my + y)^2/cy^2))
 *
 * Note that since we have movement, the old timestep is set to zero and thus the
 * density stabilization is not enabled.
 * Also the solver requires us to set g_x == 0
 *
 * The density is given by:
 * 	\rho(x, y) :=	\rho_0 \cdot \Exp{-a_{rho} \cdot ( (\frac{x - mx}{Lx})^2 + (\frac{y - my}{Ly})^2 )  } + b_\rho
 */


#if defined(EGD_PROP_TO_GRID_MAP_BRACKET) && (EGD_PROP_TO_GRID_MAP_BRACKET != 1)
#	pragma error "You did not activate EGD_PROP_TO_GRID_MAP_BRACKET, but it is needed for this test."
#elif !defined(EGD_PROP_TO_GRID_MAP_BRACKET)
#define 	EGD_PROP_TO_GRID_MAP_BRACKET 1
#else
#	pragma error "Problem with macro EGD_PROP_TO_GRID_MAP_BRACKET"
#endif

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty_helper.hpp>

#include <egd_impl/mech_solver/egd_MechSolverImpl.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include <egd_interfaces/egd_MechSolverSourceTerm.hpp>
#include <egd_interfaces/egd_MechSolverResult.hpp>
#include <egd_interfaces/egd_MechSolverArgument.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>
#include <pgl/pgl_string_prettyPrint.cpp>

//Include BOOST


//Include STD
#include <iostream>
#include <iomanip>


//We use the namespace
using namespace egd;
using intVec_t = ::pgl::pgl_vector_t<Size_t>;

//This functions are for generating some expressions
#define ArcTan(x)   ((x).atan())
#define Cos(x)      ((x).cos() )
#define Sin(x)      ((x).sin() )

template<
	class		Derive
>
static
egd_Array_t<Numeric_t>
Power(
	const Eigen::ArrayBase<Derive>& 	x,
	const Numeric_t 			e)
{
	return x.pow(e);
};

static
Numeric_t
Power(
	const Numeric_t 			x,
	const Numeric_t 			e)
{
	return ::std::pow(x, e);
};


template<
	class		Derive
>
static
egd_Array_t<Numeric_t>
Exp(
	const Eigen::ArrayBase<Derive>& 	x)
{
	return x.exp();
};


static
Numeric_t
Exp(
	const Numeric_t 			x)
{
	return ::std::exp(x);
};


template<
	class 	T
>
std::string
generateErrOut(
	const T& 	errSer,
	const Index_t 	i);


/*
 * \define IS_REG
 *
 * If this macro is defined, it will test the regular solver.
 */

/**
 * \brief	This function is able to test the Stokes solver.
 *
 * Depending on the macro IS_REG the regular solver is tested or
 * or the extended one.
 */

static
int
performTest(
	const intVec_t& 	Ny_vec,
	const intVec_t& 	Nx_vec,
	const std::string& 	dumpFile,
	const std::string& 	comment)
{
	const std::string DUMP_FILE(dumpFile);

	/*
	 * Creating the dumper.
	 */
	egd_dumperFile_t dumper(DUMP_FILE);

	/*
	 * =====================
	 * Parameters
	 */

	//
	//Geometrical properties
	const Numeric_t DomainExtensionX 	= 100.0;
	const Numeric_t DomainExtensionY 	= DomainExtensionX * 2;

	const Numeric_t gravX 			= 0.0;
	const Numeric_t gravY 			= 0.0;
	const Numeric_t fictiveOldTimestep	= 0.0;	//This is the length of the old timestep, Zero will deactivate the stabilization.

	//
	//Parameterization of the solution

	//VelX
	const Numeric_t A_x 			= 2.0;
	const Int_t 	kx 			= 5;
	const Numeric_t lx 			= DomainExtensionX / (kx * M_PI);

	//VelY
	const Numeric_t A_y 			= 2.5;
	const Int_t 	ky 			= 6;
	const Numeric_t ly 			= DomainExtensionY / (ky * M_PI);

	//Pressure
	const Numeric_t A_p 			= 3.0;
	const Numeric_t wx 			= 5.0;
	const Numeric_t wy 			= 5.0;
	const Numeric_t alpha_p 		= 7.5;

	//Viscosity
	const Numeric_t AEta 			= 3.0;
	const Numeric_t mxEta 			= DomainExtensionX / 2.0;
	const Numeric_t myEta 			= DomainExtensionY / 2.0;
	const Numeric_t bxEta 			= (M_PI / 2.0) * 1.20;
	const Numeric_t byEta 			= (M_PI / 2.0) * 1.21;
	const Numeric_t cxEta 			= 20.0;
	const Numeric_t cyEta 			= 23.0;

	//Parameterize the grid container.
	egd_gridContainer_t::PropToGridMap_t pMap;
#ifdef IS_REG
		pMap[PropIdx::Density()    ] = mkReg(eGridType::BasicNode );	//Needed by the solver
		pMap[PropIdx::Viscosity()  ] = mkReg(eGridType::BasicNode );
		pMap[PropIdx::DensityCC()  ] = mkReg(eGridType::CellCenter);
		pMap[PropIdx::ViscosityCC()] = mkReg(eGridType::CellCenter);
		pMap[PropIdx::DensityVY()  ] = mkReg(eGridType::StVy      );
		pMap[PropIdx::ViscosityVX()] = mkReg(eGridType::StVx      );	//Needed for the source
		pMap[PropIdx::ViscosityVY()] = mkReg(eGridType::StVy      );
#else
		pMap[PropIdx::Density()    ] = mkReg(eGridType::BasicNode );	//Needed by the solver
		pMap[PropIdx::Viscosity()  ] = mkReg(eGridType::BasicNode );
		pMap[PropIdx::DensityCC()  ] = mkExt(eGridType::CellCenter);
		pMap[PropIdx::ViscosityCC()] = mkExt(eGridType::CellCenter);
		pMap[PropIdx::DensityVY()  ] = mkExt(eGridType::StVy      );
		pMap[PropIdx::ViscosityVX()] = mkExt(eGridType::StVx      );	//Needed for the source
		pMap[PropIdx::ViscosityVY()] = mkExt(eGridType::StVy      );
#endif


	//Values of the proeprties
	const Numeric_t RHO_0 		= 5.0;
	const Numeric_t a_rho 		= 8.0 * std::log(2);
	const Numeric_t b_rho 		= 1;
#define COMP_RHO(x, y) (RHO_0 * Exp(a_rho * (  Power((x - (DomainExtensionX * 0.5)) / DomainExtensionX, 2.0) 	\
					     + Power((y - (DomainExtensionY * 0.5)) / DomainExtensionY, 2.0)	\
					    )									\
				   ) + b_rho )


	//This is for the errors
	::pgl::pgl_vector_t<Numeric_t> ellTwoErrorsVx(Nx_vec.size(), 0.0);
	::pgl::pgl_vector_t<Numeric_t> ellTwoErrorsVy(Nx_vec.size(), 0.0);
	::pgl::pgl_vector_t<Numeric_t> ellTwoErrorsP (Nx_vec.size(), 0.0);

	//This is for the maximum
	::pgl::pgl_vector_t<Numeric_t> maxErrorsVx(Nx_vec.size(), 0.0);
	::pgl::pgl_vector_t<Numeric_t> maxErrorsVy(Nx_vec.size(), 0.0);
	::pgl::pgl_vector_t<Numeric_t> maxErrorsP (Nx_vec.size(), 0.0);

	/*
	 * This loop will iterate through the size list and perfom the experiment
	 */
	for(Size_t i = 0; i != Nx_vec.size(); ++i)
	{
		/*
		 * Load the size Parametrer and generate a grid geometry
		 */
		const xNodeIdx_t Nx(Nx_vec.at(i));
		const yNodeIdx_t Ny(Ny_vec.at(i));

		std::cout << i << ") Start processing of " << Ny << "x" << Nx << std::endl;

		egd_gridGeometry_t gridGeo(Ny, Nx);
		gridGeo.makeConstantGrid(0.0, DomainExtensionX, 0.0, DomainExtensionY);
		gridGeo.finalizeGrids();


		/*
		 * Create a Grid Container
		 */
		egd_gridContainer_t gridCont(Ny, Nx, gridGeo, pMap);

		/*
		 * Set the value of the properties
		 */
		for(auto& it : gridCont)
		{
			//Get the positrions on the grid
			egd_Array_t<Numeric_t> X, Y;	//Load the grid positions
			std::tie(X, Y) = egd_meshgrid(gridGeo.getGridPoints(it.second.getType()) );

			if(it.first.isRepresentedBy(PropIdx::Density().getRepresentant()))
			{
				//
				//Density
				it.second = COMP_RHO(X, Y);
			}
			else if(it.first.isRepresentedBy(PropIdx::Viscosity().getRepresentant()))
			{
				//
				//Viscosity
				it.second = AEta*(bxEta + ArcTan((-mxEta + X)/cxEta))*(byEta + ArcTan((-myEta + Y)/cyEta));
			}
			else
			{
				throw PGL_EXCEPT_RUNTIME("Encountered an unexpected property " + it.first);
			};

			const Numeric_t minVal = it.second.minCoeff();
			if(minVal < 0.0)
			{
				throw PGL_EXCEPT_RUNTIME("Found negative value of " + std::to_string(minVal) + ", for " + it.first);
			};
		}; //End for(it): set values


		/*
		 * Create the source Terms
		 */
#ifdef IS_REG
		egd_gridProperty_t sourceVelX    (Ny, Nx, mkReg(eGridType::StVx      ), PropIdx::VelX()     );
		egd_gridProperty_t sourceVelY    (Ny, Nx, mkReg(eGridType::StVy      ), PropIdx::VelY()     );
		egd_gridProperty_t sourcePressure(Ny, Nx, mkReg(eGridType::StPressure), PropIdx::Pressure() );
#else
		egd_gridProperty_t sourceVelX    (Ny, Nx, mkExt(eGridType::StVx      ), PropIdx::VelX()     );
		egd_gridProperty_t sourceVelY    (Ny, Nx, mkExt(eGridType::StVy      ), PropIdx::VelY()     );
		egd_gridProperty_t sourcePressure(Ny, Nx, mkExt(eGridType::StPressure), PropIdx::Pressure() );
#endif

		/*
		 * These is the correct solution
		 */
#ifdef IS_REG
		egd_gridProperty_t solVelX    (Ny, Nx, mkReg(eGridType::StVx      ), PropIdx::VelX()     );
		egd_gridProperty_t solVelY    (Ny, Nx, mkReg(eGridType::StVy      ), PropIdx::VelY()     );
		egd_gridProperty_t solPressure(Ny, Nx, mkReg(eGridType::StPressure), PropIdx::Pressure() );
#else
		egd_gridProperty_t solVelX    (Ny, Nx, mkExt(eGridType::StVx      ), PropIdx::VelX()     );
		egd_gridProperty_t solVelY    (Ny, Nx, mkExt(eGridType::StVy      ), PropIdx::VelY()     );
		egd_gridProperty_t solPressure(Ny, Nx, mkExt(eGridType::StPressure), PropIdx::Pressure() );
#endif

		Numeric_t pressOffset = NAN;	//This is the pressure offset we have


		//Calculating the VX Source
		{
			egd_Array_t<Numeric_t> X, Y, dx_Eta;
			std::tie(X, Y) = egd_meshgrid(gridGeo.getGridPoints(sourceVelX.getType()) );

			if(gravX != 0.0)
				{ throw PGL_EXCEPT_RUNTIME("This test only works for gravY = 0, but it was set to " + std::to_string(gravX)); };

			const auto& ETA = gridCont.getProperty(PropIdx::ViscosityVX());
			dx_Eta = (AEta*(byEta + ArcTan((-myEta + Y)/cyEta)))/(cxEta*(1 + Power(-mxEta + X,2)/::std::pow(cxEta,2)));

			sourceVelX = - A_p * (X / wx + Y / wy).cos() * (1.0 / (wx    ))
				     + A_x * (X / lx         ).cos() * (1.0 / (lx     )) * dx_Eta      * 2
				     - A_x * (X / lx         ).sin() * (1.0 / (lx * lx)) * ETA.array() * 2
				     /* NO GRAVITY */ ;

			solVelX = A_x * (X / lx).sin();
			egd_setGhostNodesToZero(&solVelX);
		}; //End scope: caluclating the VX source

		//Calculating the VY sources
		{
			egd_Array_t<Numeric_t> X, Y, dy_Eta;
			std::tie(X, Y) = egd_meshgrid(gridGeo.getGridPoints(sourceVelY.getType()) );

			const auto& ETA = gridCont.getProperty(PropIdx::ViscosityVY() );
			const auto& RHO = gridCont.getProperty(PropIdx::DensityVY()   );

			dy_Eta = (AEta*(bxEta + ArcTan((-mxEta + X)/cxEta)))/(cyEta*(1 + Power(-myEta + Y,2)/::std::pow(cyEta,2)));

			sourceVelY = - A_p * (X / wx + Y / wy).cos() * (1.0 / (wy     ))
				     + A_y * (Y / ly         ).cos() * (1.0 / (ly     )) * dy_Eta      * 2
				     - A_y * (Y / ly         ).sin() * (1.0 / (ly * ly)) * ETA.array() * 2
				     + gravY * RHO.array();

			solVelY = A_y * (Y / ly).sin();
			egd_setGhostNodesToZero(&solVelY);
		}; //End: calculating VY

		//Calculating the Pressure source
		{
			egd_Array_t<Numeric_t> X, Y;
			std::tie(X, Y) = egd_meshgrid(gridGeo.getGridPoints(sourcePressure.getType()) );

			sourcePressure = A_x * (X / lx).cos() / lx + A_y * (Y / ly).cos() / ly;

			solPressure = A_p * (X / wx + Y / wy).sin() + alpha_p;
#ifdef IS_REG
			pressOffset = solPressure(1, 2);
#else
			pressOffset = solPressure(1, 1);	//Different location when compared with the regular case.
#endif

			const Numeric_t smallestPress = solPressure.minCoeff();
			if(smallestPress < 0.0)
			{
				throw PGL_EXCEPT_RUNTIME("The solution contains a negative pressure of " + std::to_string(smallestPress)
						+ " the solver does not like this.");
			};

			//remove the ghost nodes
			egd_setGhostNodesToZero(&solPressure);
		}; //End: calculating Pressure


		/*
		 * Creating the solver
		 */
#ifdef IS_REG
		egd_StokesSolverStdReg_t solver(
#else
		egd_StokesSolverStdExt_t solver(
#endif
				Ny, Nx,
				pressOffset,
				0.0, 0.0,
				0.0, 0.0 );

		egd_StokesSolverStdReg_t::SolverArg_ptr    solArg    = solver.createArgument();
		egd_StokesSolverStdReg_t::SourceTerm_ptr   solSource = solver.creatSourceTerm();
		egd_StokesSolverStdReg_t::SolverResult_ptr solRes    = solver.creatResultContainer();

		//Fill the argument
		solArg->g_x(gravX);
		solArg->g_y(gravY);
		solArg->enableDensityStab().setDensityStabTime(fictiveOldTimestep);

		//Fill the source term
		solSource->getSource(PropIdx::VelX()     ) = sourceVelX;
		solSource->getSource(PropIdx::VelY()     ) = sourceVelY;
		solSource->getSource(PropIdx::Pressure() ) = sourcePressure;

		//Load the source term into the argument
		solArg->loadSourceTerm(std::move(solSource) );
			pgl_assert(solArg->hasSourceTerm() == true);


		/*
		 * Call the solver
		 */
		solver.solve(gridCont, solArg.get(), solRes.get());


		/*
		 * Writting the file to disc
		 */
		{
			//Base folder
			const std::string BaseFolder = "/exp_" + std::to_string(i) + "/";

#ifdef IS_REG
			egd_gridProperty_t diffVelX    (Ny, Nx, mkReg(eGridType::StVx      ), PropIdx::VelX()     );
			egd_gridProperty_t diffVelY    (Ny, Nx, mkReg(eGridType::StVy      ), PropIdx::VelY()     );
			egd_gridProperty_t diffPressure(Ny, Nx, mkReg(eGridType::StPressure), PropIdx::Pressure() );
#else
			egd_gridProperty_t diffVelX    (Ny, Nx, mkExt(eGridType::StVx      ), PropIdx::VelX()     );
			egd_gridProperty_t diffVelY    (Ny, Nx, mkExt(eGridType::StVy      ), PropIdx::VelY()     );
			egd_gridProperty_t diffPressure(Ny, Nx, mkExt(eGridType::StPressure), PropIdx::Pressure() );
#endif
			diffVelX     =     solVelX.array() - solRes->cgetVelX().array();
			diffVelY     =     solVelY.array() - solRes->cgetVelY().array();
			diffPressure = solPressure.array() - solRes->cgetPressure().array();

			//open the file
			dumper.openFile();

#if 0
			//Write the exact solution
			dumper.dump(solVelX    , BaseFolder + "exSolVx"   );
			dumper.dump(solVelY    , BaseFolder + "exSolVy"   );
			dumper.dump(solPressure, BaseFolder + "exSolPress");
#endif

			//Write the difference
			dumper.dumpGridProperty(diffVelX    , BaseFolder + "diffVx"   , true);
			dumper.dumpGridProperty(diffVelY    , BaseFolder + "diffVy"   , true);
			dumper.dumpGridProperty(diffPressure, BaseFolder + "diffPress", true);

			//Write the approximation
			dumper.dumpGridProperty(solRes->cgetVelX(),     BaseFolder + "approxVx"   , true);
			dumper.dumpGridProperty(solRes->cgetVelY(),     BaseFolder + "approxVy"   , true);
			dumper.dumpGridProperty(solRes->cgetPressure(), BaseFolder + "approxPress", true);

			//Close the file
			dumper.closeFile();
		}; //End dump


		/*
		 * Calculate a simple error statistic
		 */
		{
			ellTwoErrorsVx.at(i) = std::sqrt((solVelX.array() - solRes->cgetVelX().array() ).pow(2.0).sum() / solVelX.size());
			   maxErrorsVx.at(i) = (solVelX.array() - solRes->cgetVelX().array()).abs().maxCoeff();

			ellTwoErrorsVy.at(i) = std::sqrt((solVelY.array() - solRes->cgetVelY().array() ).pow(2.0).sum() / solVelY.size());
			   maxErrorsVy.at(i) = (solVelY.array() - solRes->cgetVelY().array()).abs().maxCoeff();

			ellTwoErrorsP.at(i) = std::sqrt((solPressure.array() - solRes->cgetPressure().array() ).pow(2.0).sum() / solPressure.size());
			   maxErrorsP.at(i) = (solPressure.array() - solRes->cgetPressure().array()).abs().maxCoeff();
		}; //End scope: computing errors


		std::cout << "\tStop processing of " << Ny << "x" << Nx << std::endl;
	}; //End for(i): performing the experiment
	std::cout << std::endl;


	/*
	 * Output the error statistic
	 */
	const auto myWidth = 12;
	const auto moreErr = 6;
	std::cout.precision(6);

	/*
	 * Writing some information about the parameter
	 */
	if(comment.empty() == false)
	{
		std::cout
			<< "Result of experiment:\n"
			<< " " << comment << "\n\n";
	};

	/*
	 * Writting the header
	 */
	std::cout << std::setw(myWidth) << "No";
	std::cout << std::setw(myWidth) << "Ny x Nx";

	std::cout << std::setw(myWidth) << "ell2_VX";
	std::cout << std::setw(moreErr) << " x";
	std::cout << std::setw(myWidth) << "max(VX)";
	std::cout << std::setw(moreErr) << " x";

	std::cout << std::setw(myWidth) << "ell2_VY";
	std::cout << std::setw(moreErr) << " x";
	std::cout << std::setw(myWidth) << "max(VY)";
	std::cout << std::setw(moreErr) << " x";

	std::cout << std::setw(myWidth) << "ell2_P";
	std::cout << std::setw(moreErr) << " x";
	std::cout << std::setw(myWidth) << "max(P)";
	std::cout << std::setw(moreErr) << " x";

	std::cout << std::endl;


	//Output the errors
	for(Size_t i = 0; i != ellTwoErrorsP.size(); ++i)
	{
		std::cout << std::setw(myWidth) << i;
		std::cout << std::setw(myWidth) << std::string(std::to_string(Ny_vec.at(i)) + "x" + std::to_string(Nx_vec.at(i)));

		std::cout << std::setw(myWidth) << ellTwoErrorsVx.at(i);
		std::cout << std::setw(moreErr) << generateErrOut(ellTwoErrorsVx, i);
		std::cout << std::setw(myWidth) <<    maxErrorsVx.at(i);
		std::cout << std::setw(moreErr) << generateErrOut(maxErrorsVx, i);

		std::cout << std::setw(myWidth) << ellTwoErrorsVy.at(i);
		std::cout << std::setw(moreErr) << generateErrOut(ellTwoErrorsVy, i);
		std::cout << std::setw(myWidth) <<    maxErrorsVy.at(i);
		std::cout << std::setw(moreErr) << generateErrOut(maxErrorsVy, i);

		std::cout << std::setw(myWidth) << ellTwoErrorsP .at(i);
		std::cout << std::setw(moreErr) << generateErrOut(ellTwoErrorsP, i);
		std::cout << std::setw(myWidth) <<    maxErrorsP .at(i);
		std::cout << std::setw(moreErr) << generateErrOut(maxErrorsP, i);

		std::cout << std::endl;
	}; //End for(i):
	std::cout << std::endl;

	return 0;
}; //End main


template<
	class 	T
>
std::string
generateErrOut(
	const T& 	errSer,
	const Index_t 	i)
{
	if(i == 0)
	{
		return std::string();
	};

	return "x" + ::pgl::pgl_prettyString(errSer.at(i-1) / errSer.at(i), 2, 2);
};
