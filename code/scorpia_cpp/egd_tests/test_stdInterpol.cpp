/**
 * \brief	This program tests the standard interpolation routine.
 *
 * This program is not intended to test if the interpolation routines works.
 * For them a dedicated test exists. This function is primarly implemented
 * to test the buissness logic.
 *
 * For that a special version of the interpolation is used that overwrites
 * some functions.
 *
 * It will handle interpolation on different grids, but only using the density
 * and viscosity property. It will set uop an interpolation plan that contains
 * only the basic grids. All other properties will be filled by the post processing
 *
 * The output has to be inspected manually.
 */

#if defined(EGD_PROP_TO_GRID_MAP_BRACKET) && (EGD_PROP_TO_GRID_MAP_BRACKET != 1)
#	pragma error "You did not activate EGD_PROP_TO_GRID_MAP_BRACKET, but it is needed for this test."
#elif !defined(EGD_PROP_TO_GRID_MAP_BRACKET)
#define 	EGD_PROP_TO_GRID_MAP_BRACKET 1
#else
#	pragma error "Problem with macro EGD_PROP_TO_GRID_MAP_BRACKET"
#endif

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_impl/marker_to_grid/egd_MarkToGridInterpol_TEST.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include BOOST


//Include STD
#include <iostream>


//We use the namespace
using namespace egd;



int
main(
	int 	argc,
	char**	argv)
{
	//Set some basic setings about the markers
	const Index_t   nXMarker  = 100;			//Markers in x direction
	const Index_t   nYMarker  = 100;			//Markers in y direction
	const Index_t   nMarkers  = nXMarker * nYMarker;
	const Numeric_t testVal_1 = ::std::exp(1);
	const Numeric_t testVal_2 = ::std::atan(1) * 4;
	const PropIdx   testProp1 = PropIdx::Viscosity();
	const PropIdx   testProp2 = PropIdx::Density();	//This is the property we use for testing

	//How is the grid defined
	const Index_t 	nBasicX   = 10;		//Basic grid points
	const Index_t   nBasicY   = 10;
	const Numeric_t domXStart =  1.0;	//Startpoint in x direction
	const Numeric_t domXLen   = 10.0;	//Extension of domain in x direction.
	const Numeric_t domYStart =  1.5;	//Start in y direction
	const Numeric_t domYLen   =  9.0;	//Domain extension in y direction.


	/*
	 * Create a marker collection
	 */

	//The property list we need
	const egd_markerCollection_t::PropList_t propList =
	 	{ PropIdx::PosX(), PropIdx::PosY(), testProp1, testProp2 };

	//Create a marker collection
	egd_markerCollection_t markerColl(nMarkers, propList);


	/*
	 * Now we set the markers to values we can use
	 */
	{
		//Get positions of markers
		auto& PosX = markerColl.getXPos();
		auto& PosY = markerColl.getYPos();
			pgl_assert(PosX.size() == nMarkers,
				   PosY.size() == nMarkers );

		//Set the test property to a certain value
		markerColl.getMarkerProperty(testProp1).setConstant(testVal_1);
		markerColl.getMarkerProperty(testProp2).setConstant(testVal_2);
			pgl_assert(markerColl.getMarkerProperty(testProp1).size() == nMarkers,
				   markerColl.getMarkerProperty(testProp1).size() == nMarkers );

		//this is the marker spacing
		const Numeric_t mSpacingX = domXLen / nXMarker;
		const Numeric_t mSpacingY = domYLen / nYMarker;

		Index_t m = 0;		//Indexing the marker

		for(Index_t iY = 0; iY != nXMarker; ++iY)
		{
			for(Index_t jX = 0; jX != nYMarker; ++jX)
			{
				//COmpute the position
				PosX[m] = domXStart + (jX + 0.5) * mSpacingX;
				PosY[m] = domYStart + (iY + 0.5) * mSpacingY;
				pgl_assert(domXStart < PosX[m], PosX[m] < (domXStart + domXLen),
					   domYStart < PosY[m], PosY[m] < (domYStart + domYLen) );

				m += 1;		//Increment the index
			}; //End for(iX):
		}; //End for(iY):
		pgl_assert(m == nMarkers);
	}; //End scope: setting positions of markers

	/*
	 * Create a grid geometry
	 */
	egd_gridGeometry_t gridGeo((yNodeIdx_t(nBasicY)), xNodeIdx_t(nBasicX));
	gridGeo.makeConstantGrid(domXStart, domXStart + domXLen,
			         domYStart, domYStart + domYLen );
	gridGeo.finalizeGrids();
	pgl_assert(gridGeo.isGridSet());


	/*
	 * Create A grid Container
	 */
	egd_gridContainer_t::PropToGridMap_t pMap;

	//fill the grid
	pMap[testProp1              ] = mkReg(eGridType::BasicNode );
	pMap[PropIdx::ViscosityVX() ] = mkReg(eGridType::StVx      );
	pMap[PropIdx::ViscosityVY() ] = mkReg(eGridType::StVy      );
	pMap[PropIdx::ViscosityCC() ] = mkReg(eGridType::CellCenter);

	pMap[testProp2           ] = mkReg(eGridType::BasicNode );
	pMap[PropIdx::DensityP() ] = mkReg(eGridType::StPressure);

	egd_gridContainer_t gridCont(
			yNodeIdx_t(nBasicY), xNodeIdx_t(nBasicX),
			gridGeo,
			pMap);
		pgl_assert(gridCont.getGeometry().isGridSet());

	/*
	 * Perform tests
	 */

	//Inspecting markers
	std::cout
		<< "Marker Collection:\n";
	for(auto& mProp : markerColl)
	{
		std::cout
			<< "\t" << "Property: " << mProp.first << "\n";

		if(mProp.first.isPosition() == true)
		{
			continue;	//ignore positions
		}
		if(mProp.first == testProp1)
		{
			mProp.second.setConstant(testVal_1);
				pgl_assert(mProp.second.size() == nMarkers);
		}
		else if(mProp.first == testProp2)
		{
			mProp.second.setConstant(testVal_2);
				pgl_assert(mProp.second.size() == nMarkers);
		}
		else
		{
			throw PGL_EXCEPT_RUNTIME("Found unknown property \"" + mProp.first.print() + "\"");
		};
	}; //End for(mProp): inspecting the marker properties
	std::cout << std::endl;


	//Inspecting grid
	std::cout
		<< "Grid Container:\n";
	for(const auto& gProp : gridCont)
	{
		pgl_assert(gProp.first == gProp.second.getPropIdx());

		std::cout
			<< "\t" << "Property " << gProp.second.getPropIdx() << " on " << printGridType(gProp.second.getType()) << "\n";
	}; //End for(gProp): inspecting the grid
	std::cout << std::endl;


	/*
	 * Create the interpolator
	 */
	egd_markersToGridTestImpl_t testInterpol;


	/*
	 * Perform the interpolator
	 */
	testInterpol.mapToGrid(markerColl, &gridCont);


	/*
	 * Print out the grid properties
	 */
	std::cout
		<< "Interpolated grid properties\n"
		<< "\t" << testProp1.print() << " -> " << testVal_1 << "\n"
		<< "\t" << testProp2.print() << " -> " << testVal_2 << "\n";
	for(const auto& gProp : gridCont)
	{
		std::cout
			<< "Property: " << gProp.first << " on " << printGridType(gProp.second.getType()) << "\n"
			<< gProp.second.matrix()
			<< "\n"
			<< std::endl;
	}; //End for(gProp): inspecting the grid
	std::cout << std::endl;





	return 0;
}; //End main


