/**
 * \brief	This program tests the correct working of marker collection.
 */


//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/stringize.hpp>


//Include STD
#include <iterator>
#include <string>
#include <iostream>


//We use the namespace
using namespace egd;

int
main(
	int 	argc,
	char**	argv)
{
	//Create a small property list
	using PropList = egd_markerCollection_t::PropList_t;
	using PropIdx  = egd_markerCollection_t::PropIdx_t;
	PropList propList = {PropIdx::Type(), PropIdx::PosX(), PropIdx::PosY()};
	const auto NM = 5;

	//Create a collection
	egd_markerCollection_t mColl(NM, propList);

	if(propList.size() != mColl.nProperties())
	{
		std::cerr << "The number of properties in the list and the collection is not the same." << std::endl;
		return 1;
	};
	if(mColl.nMarkers() != NM)
	{
		std::cerr << "The number of markers inside the collection is wrong." << std::endl;
		return 1;
	};

	/*
	 * Test if all property joinged the collectgion
	 */
	double a = 1.0;
	for(const auto p : propList)
	{
		if(mColl.hasProperty(p) == false)
		{
			std::cerr << "The collection does not have propery \"" << p.print() << "\"" << std::endl;
			pgl_assert(false);
			return 1;
		};
		mColl.getMarkerProperty(p).setConstant(a);
		a += 1;

		std::cout << p.print() << " => " << mColl.getMarkerProperty(p).data() << "\n";
	}; //End for(p):


	std::cout << "The properties of the collections are:\n";
	for(const auto& it : mColl)
	{
		std::cout << "\t" << it.first.print() << " => " << it.second.size() << "  (" << it.second[0] << "@" << it.second.data() << ")" << "\n";
		if(it.second.size() != NM)
		{
			std::cerr << "\t\t" << "Vector has the wrong size." << std::endl;
			return 1;
		};

		//Query the property
		egd_markerCollection_t::MarkerProperty_cref qP = mColl.cgetMarkerProperty(it.first);
		if(qP.data() != it.second.data())
		{
			std::cerr << "Could not retreve property " << it.first.print()<< std::endl
				<< "\tqP[0] = " << qP[0] << " @" << qP.data()  << "\n"
				<< "\tit.second[0] = " << it.second[0] << " @" << it.second.data() << "\n";

			pgl_assert(false);
			return 1;
		};

	}; //End for(it): printing all properties
	std::cerr << std::endl;



	std::cout
		<< "\n\n"
		<< "All tests in this file has been passed."
		<< std::endl;
	return 0;
}; //End main



