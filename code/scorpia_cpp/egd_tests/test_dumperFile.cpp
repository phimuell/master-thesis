/**
 * \brief	This is a simple test program for the dumper.
 *
 * This file tests the new dumping process. Since we know
 * from before that the code works and only some small changes
 * were done, that are tested with this code, we can be
 * pretty sure that the code works as a whole.
 *
 * If you which to see the old version of this test, that does
 * not work with the new environment, then use git.
 *
 * 	git log -p -- code/scorpia_cpp/egd_tests/test_dumperFile.cpp
 *
 * To get a list of all recorded version of this file.
 */


//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_propertyIndex.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridContainer.hpp>

#include <egd_dumper/egd_dumperFile.hpp>


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>


//Include BOOST
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/stringize.hpp>


//Include STD
#include <iterator>
#include <string>
#include <iostream>


//We use the namespace
using namespace egd;

int
main(
	int 	argc,
	char**	argv)
{
	const auto    NX   = 2;				//Grid points, so we have one cells
	const auto    NY   = 3;				//Grid points, so we have two cells
	Numeric_t     v    = 1.1234;			//this is used for a value of the property
	const PropIdx pIdx = PropIdx::Density();	//This is property we use.

	const std::string FINAL_DESTINATION  = "/test/";
	const std::string DUMP_FILE          = "./dumperTestFile.hdf5";

	//This is a list of all supported grids
	pgl::pgl_vector_t<eGridType> gTypes =
	 { mkReg(eGridType::BasicNode), mkReg(eGridType::StVx), mkReg(eGridType::StVy), mkReg(eGridType::StPressure), mkReg(eGridType::CellCenter),
	 				mkExt(eGridType::StVx), mkExt(eGridType::StVy), mkExt(eGridType::StPressure), mkExt(eGridType::CellCenter) };


	/*
	 * Creating the dumper.
	 */
	egd_dumperFile_t dumper(DUMP_FILE);

	dumper.openFile();


	/*
	 * Perform the test
	 */
	for(Size_t i = 0; i != gTypes.size(); ++i)
	{
		const std::string D = FINAL_DESTINATION + pIdx.print() + "_" + std::to_string(i);

		egd_gridProperty_t gProp(
				yNodeIdx_t(NY), xNodeIdx_t(NX),
				gTypes.at(i), pIdx);
		gProp.setConstant(v);

		dumper.dumpGridProperty(gProp, D, true);

		dumper.flushFile();
	}; // End for

	dumper.closeFile();

	std::cerr
		<< "\n"
		<< "The dump file was generated, please check its content."
		<< std::endl;
	return 0;
}; //End main



