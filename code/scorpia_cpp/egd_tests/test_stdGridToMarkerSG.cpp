/**
 * \brief	This program tests the grid to marker interpolator object.
 *
 * The test program builds on the idea, that the cahnge is predictable in
 * some way, if the change at all markers is the same!
 * We also know from the other test programs, that the interpolation routines,
 * which are used here are correct.
 *
 * So for the pure change and the absolute interpolation we should get the same
 * value for all markers.
 *
 * The subgrid diffusion is a bit secial. There we set the diffusion constant,
 * this is the argument of the exponential to zero. This does technically
 * disable subgrid diffusion, but the code will still take that path.
 * This means we sould get the same value as we had become before.
 *
 * What is not tested by this program is if the diffusion constant for the
 * temperature calculated correctly? This has to be checked by a direct
 * inspection of the code.
 *
 * Also this program requieres that the output is inspected manually.
 *
 */

#if defined(EGD_PROP_TO_GRID_MAP_BRACKET) && (EGD_PROP_TO_GRID_MAP_BRACKET != 1)
#	pragma error "You did not activate EGD_PROP_TO_GRID_MAP_BRACKET, but it is needed for this test."
#elif !defined(EGD_PROP_TO_GRID_MAP_BRACKET)
#define 	EGD_PROP_TO_GRID_MAP_BRACKET 1
#else
#	pragma error "Problem with macro EGD_PROP_TO_GRID_MAP_BRACKET"
#endif

//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridGeometry.hpp>

#include <egd_impl/grid_to_marker/egd_GridToMarkInterpol_standard.hpp>
#include <egd_impl/grid_to_marker/egd_GridToMarkInterpolBase.hpp>
#include <egd_impl/grid_to_marker/egd_GridToMarkInterpol_TEST.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>
#include <pgl/pgl_statistics.hpp>


//Include BOOST


//Include STD
#include <iostream>


//We use the namespace
using namespace egd;

/**
 * \brief	This function analizes the marklers and prints its findings.
 *
 * \param  mProp	The marker property.
 * \param  mPropIdx	The marker property index.
 * \param  out		Stream to write to.
 */
static
std::ostream&
analyzeMarker(
	const egd_markerProperty_t& 		mProp,
	const PropIdx 				mPropIdx,
	std::ostream& 				out);




int
main(
	int 	argc,
	char**	argv)
{
	//Set some basic setings about the markers
	const Index_t   nXMarker  = 100;			//Markers in x direction
	const Index_t   nYMarker  = 100;			//Markers in y direction
	const Index_t   nMarkers  = nXMarker * nYMarker;
	const Numeric_t testChVal = ::std::exp(1);		//This is the taarget we all want
	const PropIdx   testProp1 = PropIdx::Viscosity();	//Absolute
	const PropIdx   testProp2 = PropIdx::Density();		//PureChange
	const PropIdx 	testProp3 = PropIdx::Temperature();	//Subgrid
	const PropIdx 	wProp3    = PropIdx::ThermConduct();	//Weight modifier for subgrid interpolation of prop3

	//How is the grid defined
	const Index_t 	nBasicX   = 10;		//Basic grid points
	const Index_t   nBasicY   = 10;
	const Numeric_t domXStart =  1.0;	//Startpoint in x direction
	const Numeric_t domXLen   = 10.0;	//Extension of domain in x direction.
	const Numeric_t domYStart =  1.5;	//Start in y direction
	const Numeric_t domYLen   =  9.0;	//Domain extension in y direction.


	/*
	 * Create a marker collection
	 */

	//The property list we need
	const egd_markerCollection_t::PropList_t propList =
	 	{ PropIdx::PosX(), PropIdx::PosY(), testProp1, testProp2, testProp3, wProp3 };

	//Create a marker collection
	egd_markerCollection_t markerColl(nMarkers, propList);


	/*
	 * Now we set the markers to values we can use
	 */
	{
		//Get positions of markers
		auto& PosX = markerColl.getXPos();
		auto& PosY = markerColl.getYPos();
			pgl_assert(PosX.size() == nMarkers,
				   PosY.size() == nMarkers );

		//Set the marker property
		for(auto& it : markerColl)
		{
			if(it.first.isPosition() == true)
			{
				continue;	//Ignoring positions
			};

			it.second.setConstant(0.0);
		}; //End for(it):

		if(markerColl.checkConsistency() == false)
		{
			throw PGL_EXCEPT_RUNTIME("Detected an inconsistent marker collection.");
		};


		//this is the marker spacing
		const Numeric_t mSpacingX = domXLen / nXMarker;
		const Numeric_t mSpacingY = domYLen / nYMarker;

		Index_t m = 0;		//Indexing the marker

		for(Index_t iY = 0; iY != nXMarker; ++iY)
		{
			for(Index_t jX = 0; jX != nYMarker; ++jX)
			{
				//COmpute the position
				PosX[m] = domXStart + (jX + 0.5) * mSpacingX;
				PosY[m] = domYStart + (iY + 0.5) * mSpacingY;
				pgl_assert(domXStart < PosX[m], PosX[m] < (domXStart + domXLen),
					   domYStart < PosY[m], PosY[m] < (domYStart + domYLen) );

				m += 1;		//Increment the index
			}; //End for(iX):
		}; //End for(iY):
		pgl_assert(m == nMarkers);
	}; //End scope: setting positions of markers

	/*
	 * Create a grid geometry
	 */
	egd_gridGeometry_t gridGeo((yNodeIdx_t(nBasicY)), xNodeIdx_t(nBasicX));
	gridGeo.makeConstantGrid(domXStart, domXStart + domXLen,
			         domYStart, domYStart + domYLen );
	gridGeo.finalizeGrids();
	pgl_assert(gridGeo.isGridSet());


	/*
	 * Create A grid Container
	 */
	egd_gridContainer_t::PropToGridMap_t pMap;

	//fill the grid
	pMap[testProp1              ] = mkReg(eGridType::BasicNode );
	pMap[testProp2              ] = mkReg(eGridType::BasicNode );
	pMap[testProp3              ] = mkReg(eGridType::BasicNode );	//This is the old field
	pMap[wProp3                 ] = mkReg(eGridType::BasicNode );

	egd_gridContainer_t gridCont(
			yNodeIdx_t(nBasicY), xNodeIdx_t(nBasicX),
			gridGeo,
			pMap);
		pgl_assert(gridCont.getGeometry().isGridSet());


	/*
	 * Create the job plan
	 */
	using ComputePlan_t  = egd_gridToMarkersStandard_t::ComputePlan_t;
	using ComputeJob_t   = ComputePlan_t::ComputeJob_t;
	using DiffConstant_t = ComputePlan_t::DiffConstant_t;

	ComputePlan_t compPlan;	//This is the compute plan that we use

	//
	//ABSOLUTE INTERPOLATION
	{
		auto absField = std::ref(gridCont.getProperty(testProp1));
		ComputeJob_t absJob(
				testProp1,
				testProp1,		//Will deactivate conservative interpolation
				DiffConstant_t(),	//No subgrid diffusion
				absField,		//The change we want
				nullptr,		//No old field
				true);			//Abs interpolation
			pgl_assert(absJob.isValid());

		//We set the markers to a negative value to see that they are realy overwritten
		markerColl.getMarkerProperty(testProp1).setConstant(-1.0);

		//Set the marker to the uniform change
		absField.get().setConstantAll(testChVal);

		//Insert into the plan
		compPlan.addJob(std::move(absJob));
	}; //End scope: absolute interpolation


	//
	//Pure change interpolation
	{
		auto chField = std::ref(gridCont.getProperty(testProp2));
		ComputeJob_t chJob(
				testProp2,
				testProp2,		//Will deactivate conservative interpolation
				DiffConstant_t(),	//No subgrid diffusion
				chField,		//The change we want
				nullptr,		//No old field
				false);			//Cha interpolation interpolation
			pgl_assert(chJob.isValid());

		//Set the marker property to zero, this way we will have the same value as
		//it is for the absolute interpolation
		markerColl.getMarkerProperty(testProp2).setZero();

		//Set the grid change to the test value
		chField.get().setConstantAll(testChVal);

		//Insert into the plan
		compPlan.addJob(std::move(chJob));
	}; //End scope: pure change


	//
	//Subgrid diffusion
	{
		//We do subgrid diffusion here, but the value is set to zero
		//which will deactivate subgrid diffusion, but will instruct
		//the code to use the subgrid diffusion code path
		DiffConstant_t sgCoeff(nMarkers);
		sgCoeff.setZero();

		//this is the old field, we will set it to zero
		auto& oldField = gridCont.getProperty(testProp3);
		oldField.setZero();

		//this is the markers for the cinservative interpolation
		auto& wPropMarker = markerColl.getMarkerProperty(wProp3);
		wPropMarker.setConstant(0);

		//Also the markers are zero
		auto& propMarker = markerColl.getMarkerProperty(testProp3);
		propMarker.setZero();

		//We will also pass the change field to job jobject
		::std::unique_ptr<egd_gridProperty_t> sgField
			= std::make_unique<egd_gridProperty_t>(
					yNodeIdx_t(nBasicY), xNodeIdx_t(nBasicX),
					mkReg(eGridType::BasicNode), testProp3);
		//
		//Set the cahange value to the same change value as the others.
		sgField->setConstantAll(testChVal);

		ComputeJob_t sgJob(
				testProp3,
				wProp3, 		//the property for the conservative interpolation
				std::move(sgCoeff),	//Subgrid diffusion constants
				sgField,		//The change we want
				&oldField,		//old field
				false);			//Cha interpolation interpolation
			pgl_assert(sgJob.isValid());
			pgl_assert(sgField == nullptr);

		compPlan.addJob(std::move(sgJob));


	}; //End scope: subgrid diffusion


	/*
	 * Perform tests
	 */

	//now we will breate a test instance
	egd_gridToMarkersTestImpl_t<egd_gridToMarkersStandard_t> mapper(std::move(compPlan));

	//Now activate the verbose part.
	ComputeJob_t::BE_VERBOSE();

	//Call the mapping
	mapper.testBase(gridCont, &markerColl);

	/*
	 * Output the result
	 */
	for(const auto& mPropIt : markerColl)
	{
		if(mPropIt.first.isPosition() == true)
		{
			continue;	//Ignoring position
		};

		analyzeMarker(mPropIt.second, mPropIt.first,
				std::cerr) << std::endl;
	}; //End for(mPropIt): end outputting


	std::cerr
		<< "\n\n"
		<< "==============================" << "\n"
		<< "PLEASE CHECK THE RESULT"
		<< std::endl;

	return 0;
}; //End main


/*
 * ===============================
 * Helper info
 */
std::ostream&
analyzeMarker(
	const egd_markerProperty_t& 		mProp,
	const PropIdx 				mPropIdx,
	std::ostream& 				out)
{
	pgl::pgl_statistic_t stat;
	const Index_t N = mProp.size();

	for(Index_t m = 0; m != N; ++m)
	{
		const Numeric_t v = mProp[m];

		if(::pgl::isValidFloat(v) == false)
		{
			throw PGL_EXCEPT_RUNTIME("Found an invalid value for " + mPropIdx.print());
		};

		stat.add_Record(v);
	}; //End for(m):

	//
	//Print output
	out
		<< "Statistic of: " << mPropIdx.print() << "\n"
		<< " Mean: " << stat.get_Mean() 	<< "\n"
		<< " std:  " << stat.get_StdDeviation() << "\n"
		<< " Min: " << stat.get_Min() 		<< "\n"
		<< " Max: " << stat.get_Max()
		<< std::endl;

	return out;
};//Ena anayze markers






