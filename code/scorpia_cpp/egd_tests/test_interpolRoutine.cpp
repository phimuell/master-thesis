/**
 * \brief	This file implements a test for the implementation routines.
 *
 * Note that this test builds on an operation. If the marker property of all
 * markers have the same value, grid nodes will all have the same value and
 * this value is the same as the markers'.
 * The same is true for the inverse operation, the pulling back operation.
 * That maps grid nodes to markers.
 * But also note that due the finite interpolation, we will get values
 * that are sligthly different.
 *
 * The first observation is always true and also holds for near ghost nodes,
 * if they were _not_ disabled, then they are zero.
 * On some grids ghost nodes will be zero, even if they are not disabled,
 * ext(StV{x, y}), because they are too far away.
 *
 * The second observation is only true, if the marker is sourounded by
 * four nodes that are not zero, this is the case on all extended grids.
 * for regular grid, it is only true for the basic grid.
 *
 * This program will go through all grid and perform the interpolation.
 * Note that the interpolation state is created with disabeled ghost
 * nodes, but the actuall interpolation is done with modifing weights
 * and non disabled ghost nodes.
 *
 * The program 	will then output the grid weights and the property that was
 * implemented. it will also output the max and the min of the grid propery
 * and the marker property.
 *
 * The user must manually inspect the validity of the output.
 */


//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridGeometry.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include BOOST


//Include STD
#include <iostream>


//We use the namespace
using namespace egd;


/**
 * \brief	This function performs the interpolation.
 *
 * It will interpolate the property to the given grid
 * and return the  property.
 */
static
std::pair<egd_gridProperty_t, egd_markerProperty_t>
performTest(
	const egd_markerCollection_t& 		mColl,
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const PropIdx 				prop,
	const Numeric_t 			testValue);



int
main(
	int 	argc,
	char**	argv)
{
	//Set some basic setings about the markers
	const Index_t nXMarker  = 100;			//Markers in x direction
	const Index_t nYMarker  = 100;			//Markers in y direction
	const Index_t nMarkers  = nXMarker * nYMarker;
	const PropIdx testProp  = PropIdx::Density();	//This is the property we use for testing
	const Numeric_t testVal = ::std::exp(1) * std::atan(1) * 4;

	//How is the grid defined
	const Index_t 	nBasicX   = 10;		//Basic grid points
	const Index_t   nBasicY   = 11;
	const Numeric_t domXStart =  1.0;	//Startpoint in x direction
	const Numeric_t domXLen   = 10.0;	//Extension of domain in x direction.
	const Numeric_t domYStart =  1.5;	//Start in y direction
	const Numeric_t domYLen   =  9.0;	//Domain extension in y direction.


	/*
	 * Create a marker collection
	 */

	//The property list we need
	const egd_markerCollection_t::PropList_t propList =
	 	{ PropIdx::PosX(), PropIdx::PosY(), testProp };

	//Create a marker collection
	egd_markerCollection_t markerColl(nMarkers, propList);


	/*
	 * Now we set the markers to values we can use
	 */
	{
		//Get positions of markers
		auto& PosX = markerColl.getXPos();
		auto& PosY = markerColl.getYPos();
			pgl_assert(PosX.size() == nMarkers,
				   PosY.size() == nMarkers );

		//Set the test property to a certain value
		markerColl.getMarkerProperty(testProp).setConstant(testVal);
			pgl_assert(markerColl.getMarkerProperty(testProp).size() == nMarkers);

		//this is the marker spacing
		const Numeric_t mSpacingX = domXLen / nXMarker;
		const Numeric_t mSpacingY = domYLen / nYMarker;

		Index_t m = 0;		//Indexing the marker

		for(Index_t iY = 0; iY != nXMarker; ++iY)
		{
			for(Index_t jX = 0; jX != nYMarker; ++jX)
			{
				//COmpute the position
				PosX[m] = domXStart + (jX + 0.5) * mSpacingX;
				PosY[m] = domYStart + (iY + 0.5) * mSpacingY;
				pgl_assert(domXStart < PosX[m], PosX[m] < (domXStart + domXLen),
					   domYStart < PosY[m], PosY[m] < (domYStart + domYLen) );

				m += 1;		//Increment the index
			}; //End for(iX):
		}; //End for(iY):
		pgl_assert(m == nMarkers);
	}; //End scope: setting positions of markers


	/*
	 * Create a grid geometry
	 */
	egd_gridGeometry_t gridGeo((yNodeIdx_t(nBasicY)), xNodeIdx_t(nBasicX));
	gridGeo.makeConstantGrid(domXStart, domXStart + domXLen,
			         domYStart, domYStart + domYLen );
	gridGeo.finalizeGrids();


	/*
	 * Perform tests
	 */

	//This is a list of all supported grids
	pgl::pgl_vector_t<eGridType> gTypes =
	 { mkReg(eGridType::BasicNode), mkReg(eGridType::StVx), mkReg(eGridType::StVy), mkReg(eGridType::StPressure), mkReg(eGridType::CellCenter),
	 				mkExt(eGridType::StVx), mkExt(eGridType::StVy), mkExt(eGridType::StPressure), mkExt(eGridType::CellCenter) };

	Index_t i = 0;
	for(const eGridType gType : gTypes)
	{
		i += 1;
		std::cout
			<< "gType = " << printGridType(gType) << "\n";

		const auto px = performTest(markerColl, gridGeo, gType, testProp, testVal);
		const auto x = px.first;
		const auto m = px.second;

		std::cout
			<< "TEST VAL: " << testVal << "\n"
			<< "MAX (G):  " << x.maxCoeff() << "\n"
			<< "MIN (G):  " << x.minCoeff() << "\n"
			<< "MAX (M):  " << m.maxCoeff() << "\n"
			<< "MIN (M):  " << m.minCoeff()
			<< "\n" << std::endl;
	};//End for(gTypes): go through all grid types


	std::cout
		<< "\n\n"
		<< "PLEASE CHECK THE OUTPUT!"
		<< std::endl;

	return 0;
}; //End main


/*
 * ====================================
 * Helper functions
 */

std::pair<egd_gridProperty_t, egd_markerProperty_t>
performTest(
	const egd_markerCollection_t& 		mColl,
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const PropIdx 				prop,
	const Numeric_t 			testValue)
{
	//This is the return value
	egd_gridProperty_t gProp(
			yNodeIdx_t(gridGeo.yNPoints()), xNodeIdx_t(gridGeo.xNPoints()),
			gType,
			prop);

	//perform alsu use the the modifing feature
	egd_markerProperty_t modWeights(mColl.nMarkers());
		modWeights.setConstant(1234.5678);

	//This is the interpolation state object
	egd_interpolState_t interpolState;

	//fill the interpolation state object
	const bool noEmptyCell = egd_initInterpolation(mColl, gridGeo, gType, &interpolState,
			true,		//Do processing
			true);		//Do disabeling, but will not mater,s icne we use the modifiyt weights.

	if(noEmptyCell == false)
	{
		throw PGL_EXCEPT_RUNTIME("Detected an empty cell for grid " + gType);
	};

	//Perform the interpolation
	egd_mapToGrid(interpolState, mColl.getMarkerProperty(prop),
			&gProp,
			&modWeights,
			false);		//No disableing of the ghost node

	std::cout << "Interpolated grid property, ignoring ghost nodes:\n" << gProp.matrix() << std::endl;

	std::cout
		<< "\n" << "Grid weights with disabeled ghost nodes:\n" << interpolState.gWeights << std::endl;


	//Now we map back
	egd_markerProperty_t mappedBack(mColl.nMarkers());
		mappedBack.setConstant(-1.0);

	//Now interpolate
	egd_pullBackToMarker(&mappedBack, gProp, interpolState);


	return std::make_pair(gProp, mappedBack);
	(void)testValue;
}; //End: perforrm test











