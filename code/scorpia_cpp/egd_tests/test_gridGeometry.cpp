/**
 * \brief	This file implements a test for the grid geometry.
 *
 * The program will generate a very simple constant grid and then
 * output it.
 * Since everything is done with the same function, it should
 * be sufficent to run it only with constant grids.
 */


//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_gridGeometry.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>


//Include BOOST


//Include STD
#include <iostream>


//We use the namespace
using namespace egd;





int
main(
	int 	argc,
	char**	argv)
{
	//How is the grid defined
	const Index_t 	nBasicX   = 5;		//Basic grid points
	const Index_t   nBasicY   = 5;
	const Numeric_t domXStart = 1.0;	//Startpoint in x direction
	const Numeric_t domXLen   = 4.0;	//Extension of domain in x direction.
	const Numeric_t domYStart = 1.0;	//Start in y direction
	const Numeric_t domYLen   = 4.0;	//Domain extension in y direction.

	std::cout
		<< "The grid has an extension in (x, y)"
		<< " ~ "
		<< "[" << domXStart << ", " << (domXStart + domXLen) << "]"
		<< " x "
		<< "[" << domYStart << ", " << (domYStart + domYLen) << "]"
		<< ".\n"
		<< "It has " << nBasicX << " points in x direction and " << nBasicY << " points in y direction. "
		<< "it has thus a spcing of " << (domXLen / (nBasicX - 1.0)) << " in x direction and, "
		<< (domYLen / (nBasicY - 1)) << " in y direction."
		<< "\n"
		<< std::endl;


	/*
	 * Create a grid geometry
	 */
	egd_gridGeometry_t gridGeo((yNodeIdx_t(nBasicY)), xNodeIdx_t(nBasicX));
	gridGeo.makeConstantGrid(domXStart, domXStart + domXLen,
			         domYStart, domYStart + domYLen );
	gridGeo.finalizeGrids();


	/*
	 * Perform tests
	 */

	//List of all supported grids
	pgl::pgl_vector_t<eGridType> gTypes =
	 { mkReg(eGridType::BasicNode), mkReg(eGridType::StVx), mkReg(eGridType::StVy), mkReg(eGridType::StPressure), mkReg(eGridType::CellCenter),
	 				mkExt(eGridType::StVx), mkExt(eGridType::StVy), mkExt(eGridType::StPressure), mkExt(eGridType::CellCenter) };

	Index_t i = 0;
	for(const eGridType gType : gTypes)
	{
		i += 1;		//Is for debugging
		std::cout
			<< "gType = " << printGridType(gType) << "\n";

		egd_gridGeometry_t::NodePosition_t xPos, yPos;	//Position of the grid
		std::tie(xPos, yPos) = gridGeo.getGridPoints(gType);

		std::cout
			<< "xPos: " << xPos.transpose() << "\n"
			<< "yPos: " << yPos.transpose() << "\n"
			<< std::endl;
	};//End for(gTypes): go through all grid types


	std::cout
		<< "\n\n"
		<< "PLEASE CHECK THE OUTPUT!"
		<< std::endl;

	return 0;
}; //End main



