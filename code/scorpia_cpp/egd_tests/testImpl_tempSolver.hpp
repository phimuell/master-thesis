#pragma once
/**
 * \brief	This file implements a test for the temperature solver
 * 		 employing the method of manufactured solution.
 *
 * This file only provides a function to do that.
 *
 * The equation we want to test is
 * 	\RhoCp * D_{t}[T] + \div[\vec{q}] = H
 *
 * In the above equation, $D_{t}[T]$ is the material derivative of the temperature.
 * that we will interprete as a time derivative, sincethe advection is handled by the
 * markers. $\vec{q} := -k * \grad[T]$, where k is the thermal conductivity.
 * H is the heating term, mathematically it is a source term, but we call it the
 * heating term, our source term is different and is used to make our equation a
 * solution.
 *
 * We now assume that the following equation is a solution.
 * 	T_{m}(x, y) := alphaY + (betaY - alphaY) * (y / Ly) + alphaS * \Sin{gammaS * y}
 * 	\gammaS    := \frac{\pi * k}{Ly}, for k an integer
 * As we can see the equation satisfies the boundary conditions.
 * With alphaY beaing the top temperature and betaY the bottom temperature.
 *
 * We assume that the thermal conductivity is given by the following formula:
 * 	k(x, y) := alphaK * (ArcTan[(x - mx)/cx] + bx) * (ArcTan[(y - my)/cy] + by) + betaK
 *
 * Now we have to compute the source term.
 * The source term S is given by:
 *
 * S := -(  \partial_{x}[k] * \partial_{x}[T] + k * \partial_{xx}[T]
 *        + \partial_{y}[k] * \partial_{y}[T] + k * \partial_{yy}[T]
 *       )
 *        - H_{analytical} )
 * H_{analytical} is the value that is analytically computed for the heating term.
 * 	u_x := AVelX * \Sin{X * gamma_{velX} }
 * 	u_y := AVelY * \Sin{Y * gamma_{velY} }
 */

#if defined(EGD_PROP_TO_GRID_MAP_BRACKET) && (EGD_PROP_TO_GRID_MAP_BRACKET != 1)
#	pragma error "You did not activate EGD_PROP_TO_GRID_MAP_BRACKET, but it is needed for this test."
#elif !defined(EGD_PROP_TO_GRID_MAP_BRACKET)
#define 	EGD_PROP_TO_GRID_MAP_BRACKET 1
#else
#	pragma error "Problem with macro EGD_PROP_TO_GRID_MAP_BRACKET"
#endif



//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_gridGeometry.hpp>
#include <egd_grid/egd_gridContainer.hpp>
#include <egd_grid/egd_gridProperty_helper.hpp>

#include <egd_dumper/egd_dumperFile.hpp>

#include <egd_interfaces/egd_TempSolverResult.hpp>
#include <egd_interfaces/egd_TempSolverArgument.hpp>
#include <egd_interfaces/egd_TempSolverSourceTerm.hpp>

#include <egd_impl/temp_solver/egd_stdTempSolverRegularGrid.hpp>
#include <egd_impl/temp_solver/egd_stdTempSolverExtendedGrid.hpp>

#include <egd_phys/egd_heatingTerm.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_string_prettyPrint.cpp>


#if defined(EGD_ENSURE_DEVIATORIC_STRAIN_RATE) && (EGD_ENSURE_DEVIATORIC_STRAIN_RATE == 1)
#	pragma error "You selected a real deviatoric strain rate object. But this will break the analytic source term for the shear heating."
#endif



//Include BOOST


//Include STD
#include <iostream>
#include <iomanip>


//We use the namespace
using namespace egd;
using intVec_t = ::pgl::pgl_vector_t<Size_t>;

//This functions are for generating some expressions
#define ArcTan(x)   ((x).atan())
#define Cos(x)      ((x).cos() )
#define Sin(x)      ((x).sin() )

template<
	class		Derive
>
static
egd_Array_t<Numeric_t>
Power(
	const Eigen::ArrayBase<Derive>& 	x,
	const Numeric_t 			e)
{
	return x.pow(e);
};

static
Numeric_t
Power(
	const Numeric_t 			x,
	const Numeric_t 			e)
{
	return ::std::pow(x, e);
};

template<
	class 	T
>
std::string
generateErrOut(
	const T& 	errSer,
	const Index_t 	i);


/**
 * \brief	This function allows to perform several tests for the temperature solver.
 *
 * This function allows to test the temperature solver with the method of manufactured
 * solution, see description above for more detail. It allows to perform a scalling study.
 * This function takes the number of gird points and and several parameter to select the
 * behaviour. By defining the macro IS_REG you test the regular solver. If that macro is
 * not defined then the extended solver is tested.
 *
 * \param  Ny_vec	Number of grid points in y direction.
 * \param  Nx_vec	Number of grid points in x direction.
 * \param  dumpFile	Where to save the file.
 * \param  comment	Comment that can be printed.
 * \param  etaV		Viscosity paramter, set to zero dto deactivate shear heating.
 * \param  thermExp	Themrmal expansion, set to zero to deactivate adiabatic heat.
 * \param  alphaS	Strength of the temperature fluctuation.
 * \param  alphaK 	Strength of variable thermal conductivity
 * \param  betaK 	Offset of the thermal conductivity.
 * \param  kS		The number of hills in the temperature fluctuations.
 * \param  topTemp	Temperature at the top (y == 0).
 * \param  botTemp	Temperature at the bottom.
 */
static
int
performTest(
	const intVec_t& 	Ny_vec,
	const intVec_t& 	Nx_vec,
	const std::string& 	dumpFile,
	const std::string& 	comment,
	const Numeric_t 	etaV     = 4.0,
	const Numeric_t		thermExp = 4.0,
	const Numeric_t 	alphaS   = 2.0,
	const Numeric_t 	alphaK   = 8.5397342226,
	const Numeric_t 	betaK 	 = 0.0,
	const Int_t 		kS       = 10,
	const Numeric_t 	topTemp  = 40.0,
	const Numeric_t 	botTemp  = 60.0)
{
	//This is the filename for dumping the experiment
	const std::string DUMP_FILE = dumpFile;

	/*
	 * Creating the dumper.
	 */
	egd_dumperFile_t dumper(DUMP_FILE);

	/*
	 * =====================
	 * Parameters
	 */

	//
	//Geometrical properties
	const Numeric_t DomainExtensionX 	= 100.0;
	const Numeric_t DomainExtensionY 	= DomainExtensionX * 2;
	const Numeric_t gravX 			= 0.0;
	const Numeric_t gravY 			= 9.81;
	const egd_gravForce_t grav(gravX, gravY);
		pgl_assert(grav.isZeroX());

	const Numeric_t timeStep 		= 0.01;

	//
	//Parameterization of the solution

	//Thermal conductivity
	//const Numeric_t alphaK 			= ::std::exp(1) * std::atan(1) * 4.0;	<- ARGUMENT
	//const Numeric_t betaK 			= 0;					<- ARGUMENT
	const Numeric_t bxK 			= (M_PI / 2.0) * 1.20;
	const Numeric_t byK 			= (M_PI / 2.0) * 1.21;
	const Numeric_t cxK 			= 20.0;
	const Numeric_t cyK 			= 23.0;
	const Numeric_t mxK 			= DomainExtensionX / 2.0;
	const Numeric_t myK 			= DomainExtensionY / 2.0;

	//Temperature
	const Numeric_t alphaY 			= topTemp;		//Top
	const Numeric_t betaY 			= botTemp;		//Bottom
	//const Numeric_t alphaS 			=  3.0;	<- ARGUMENT
	const Numeric_t gammaS 			= kS * M_PI / DomainExtensionY;

	if(std::abs(alphaS) > std::min(std::abs(betaY), std::abs(alphaY)))
	{
		throw PGL_EXCEPT_InvArg("The arguments for the tmperature parameterization are not valid.");
	};

	//VelY
	const Numeric_t AVelY 			= 1.0;
	const Int_t 	ky_x 			= 4;
	const Int_t 	ky_y 			= 3;
	const Numeric_t gXVelY 			= ky_x * M_PI / DomainExtensionX;
	const Numeric_t gYVelY 			= ky_y * M_PI / DomainExtensionY;

	//VelX
	const Numeric_t AVelX 			= 1.0;
	const Int_t 	kx_x 			= 4;
	const Int_t 	kx_y 			= 3;
	const Numeric_t gXVelX 			= kx_x * M_PI / DomainExtensionX;
	const Numeric_t gYVelX 			= kx_y * M_PI / DomainExtensionY;

	//Heatingterm
	const Numeric_t thermExpansion_value 	= thermExp;
	const Numeric_t eta_value 		= etaV;
	const Numeric_t rho_value 		= 2.4;



	//Parameterize the grid container.
	egd_gridContainer_t::PropToGridMap_t pMap;
#ifdef IS_REG
	  pMap[PropIdx::RhoCp()            ] = mkReg(eGridType::BasicNode );
	  pMap[PropIdx::Density()          ] = mkReg(eGridType::BasicNode );
	  pMap[PropIdx::Viscosity()        ] = mkReg(eGridType::BasicNode );
	  pMap[PropIdx::ViscosityCC()      ] = mkReg(eGridType::CellCenter);
	  pMap[PropIdx::Temperature()      ] = mkReg(eGridType::BasicNode );
	  pMap[PropIdx::ThermConductVX()   ] = mkReg(eGridType::StVx      );
	  pMap[PropIdx::ThermConductVY()   ] = mkReg(eGridType::StVy      );
	  pMap[PropIdx::ThermExpansAlpha() ] = mkReg(eGridType::BasicNode );
	const bool isExt = false;
#else
	  pMap[PropIdx::RhoCpCC()          ] = mkExt(eGridType::CellCenter);
	  pMap[PropIdx::DensityCC()        ] = mkExt(eGridType::CellCenter);
	  pMap[PropIdx::Viscosity()        ] = mkReg(eGridType::BasicNode );
	  pMap[PropIdx::ViscosityCC()      ] = mkExt(eGridType::CellCenter);
	  pMap[PropIdx::Temperature()      ] = mkExt(eGridType::CellCenter);	//By convention this is in the correct grid
	  pMap[PropIdx::ThermConductVX()   ] = mkExt(eGridType::StVx     );
	  pMap[PropIdx::ThermConductVY()   ] = mkExt(eGridType::StVy     );
	  pMap[PropIdx::ThermExpansAlpha() ] = mkExt(eGridType::CellCenter);
	const bool isExt = true;
#endif

	//Values of the proeprties
	const Numeric_t RhoCp_val = ::std::exp(1.0);


	//This is for the errors
	::pgl::pgl_vector_t<Numeric_t> ellTwoErrorsT(Nx_vec.size(), 0.0);

	//This is for the maximum
	::pgl::pgl_vector_t<Numeric_t> maxErrorsT(Nx_vec.size(), 0.0);

	/*
	 * This loop will iterate through the size list and perfom the experiment
	 */
	for(Size_t i = 0; i != Nx_vec.size(); ++i)
	{
		/*
		 * Load the size Parametrer and generate a grid geometry
		 */
		const xNodeIdx_t Nx(Nx_vec.at(i));
		const yNodeIdx_t Ny(Ny_vec.at(i));

		std::cout << i << ") Start processing of " << Ny << "x" << Nx << std::endl;

		egd_gridGeometry_t gridGeo(Ny, Nx);
		gridGeo.makeConstantGrid(0.0, DomainExtensionX, 0.0, DomainExtensionY);
		gridGeo.finalizeGrids();


		/*
		 * Create a Grid Container
		 */
		egd_gridContainer_t gridCont(Ny, Nx, gridGeo, pMap);

		/*
		 * Set the value of the properties
		 */
		for(auto& it : gridCont)
		{
			egd_Array_t<Numeric_t> X, Y;	//Load the grid positions
			std::tie(X, Y) = egd_meshgrid(gridGeo.getGridPoints(it.second.getType()) );

			/*
			 * Set up the properties in the container
			 */
			if(it.first.getRepresentant() == PropIdx::RhoCp().getRepresentant())
			{
				//
				//RhoCp
				it.second.setConstantAll(RhoCp_val);
			}
			else if(it.first.getRepresentant() == PropIdx::Density().getRepresentant())
			{
				//
				//Density
				it.second.setConstantAll(rho_value);
			}
			else if(it.first.getRepresentant().getRepresentant() == PropIdx::Temperature().getRepresentant())
			{
				//
				//Temperature
				it.second =   (alphaY + ((betaY - alphaY) / DomainExtensionY) * Y)
					    + (alphaS * (gammaS * Y).sin());
			}
			else if(it.first.getRepresentant().getRepresentant() == PropIdx::ThermConduct().getRepresentant())
			{
				//
				//Thermal conductivity
				it.second = alphaK*(bxK + ArcTan((-mxK + X)/cxK))*(byK + ArcTan((-myK + Y)/cyK)) + betaK;
			}
			else if(it.first.getRepresentant().getRepresentant() == PropIdx::ThermExpansAlpha().getRepresentant())
			{
				//
				//Thermal expansion
				it.second.setConstantAll(thermExpansion_value);
			}
			else if(it.first.getRepresentant().getRepresentant() == PropIdx::Viscosity().getRepresentant())
			{
				//
				//Viscosity
				it.second.setConstantAll(eta_value);
			}
			else
			{
				throw PGL_EXCEPT_RUNTIME("Encountered an unkown property " + it.first);
			};

#ifdef IS_REG
			Index_t r, c;
			const Numeric_t minVal = it.second.array().minCoeff(&r, &c);
			if(minVal < 0.0)
			{
				throw PGL_EXCEPT_RUNTIME("Found negative value of " + std::to_string(minVal) + ", for " + it.first
						+ " At position (" + std::to_string(r) + ", " + std::to_string(c) + ")");
			};
#endif
		}; //End for(it): set values


		/*
		 * Create Velocity fields
		 */
		egd_mechSolverResult_t mechSolRes;
		mechSolRes.allocateSize(Ny, Nx, isExt);
		mechSolRes.setZero();
		{
			egd_Array_t<Numeric_t> X, Y;

			// X Velocity field
			std::tie(X, Y) = egd_meshgrid(gridGeo.getGridPoints(mechSolRes.getVelX().getType()) );
			mechSolRes.getVelX() = (AVelX * (X * gXVelX + Y * gYVelX).sin());

			// Y Velocity field
			// !!! NOTE ALSO ADAPT IT AT THE ADIABATIC PRESSURE
			std::tie(X, Y) = egd_meshgrid(gridGeo.getGridPoints(mechSolRes.getVelY().getType()) );
			mechSolRes.getVelY() = (AVelY * (X * gXVelY + Y * gYVelY).sin());
			pgl_assert(mechSolRes.getVelY().onStVyPoints());

		};//End creating a velocity


		/*
		 * This is teh heating term
		 */
		egd_deviatoricStrainRate_t dEps(gridGeo, mechSolRes);
		egd_deviatoricStress_t     Stress(gridCont, dEps);

		egd_heatingTerm_t heatingTerm(
				gridCont,
				dEps, Stress,
				mechSolRes,
				grav,
				isExt);


		/*
		 * Create the source Terms
		 */
#ifdef IS_REG
		egd_gridProperty_t sourceT(Ny, Nx, mkReg(eGridType::BasicNode), PropIdx::Temperature() );
#else
		egd_gridProperty_t sourceT(Ny, Nx, mkExt(eGridType::CellCenter), PropIdx::Temperature() );
#endif

		//Calculating the T
		{
			egd_Array_t<Numeric_t> X, Y, etaValue, rhoValue;
			std::tie(X, Y) = egd_meshgrid(gridGeo.getGridPoints(sourceT.getType()) );
#ifdef IS_REG
			const auto& RHO = gridCont.getProperty(PropIdx::Density());
			const auto& ETA = gridCont.getProperty(PropIdx::Viscosity());
#else
			const auto& RHO = gridCont.getProperty(PropIdx::DensityCC());
			const auto& ETA = gridCont.getProperty(PropIdx::ViscosityCC());
#endif


			if(RHO.getType() != sourceT.getType())
			{
				throw PGL_EXCEPT_RUNTIME("density on wrong grid.");
			};
			if(ETA.getType() != sourceT.getType())
			{
				throw PGL_EXCEPT_RUNTIME("Viscosity on wrong grid.");
			};
			pgl_assert(gridCont.getProperty(PropIdx::Temperature()).checkOnSameGrid(sourceT));
			pgl_assert(gridCont.getProperty(PropIdx::ThermExpansAlpha()).checkOnSameGrid(sourceT));

			etaValue = ETA.array();
			rhoValue = RHO.array();

			sourceT =
				//Manufactured source term
				 (-((alphaK*(bxK + ArcTan((-mxK + X)/cxK))*((-alphaY + betaY)/DomainExtensionY + alphaS*gammaS*Cos(gammaS*Y)))/(cyK*(1 + Power(-myK + Y,2)/Power(cyK,2))))
				  -  alphaS*Power(gammaS,2)*(-betaK - alphaK*(bxK + ArcTan((-mxK + X)/cxK))*(byK + ArcTan((-myK + Y)/cyK)))*Sin(gammaS*Y)
				 )

				//Adiabatic heating
				-(  gridCont.getProperty(PropIdx::ThermExpansAlpha() ).array()
				  * gridCont.getProperty(PropIdx::Temperature()      ).array()
				  * rhoValue
				  * gravY
				     //DO NOT LOAD THE VELOCITY FROM THE SOLUTION, DIFFERENT GRIDS!!!
				  * (AVelY * (X * gXVelY + Y * gYVelY).sin())
				 )
#if 1
				//Shear heating
				-(  2*Power(AVelX,2)*etaValue*Power(gXVelX,2)*Power(Cos(gXVelX*X + gYVelX*Y),2)
				  + 2*Power(AVelY,2)*etaValue*Power(gYVelY,2)*Power(Cos(gXVelY*X + gYVelY*Y),2)
				  + etaValue*Power(AVelX*gYVelX*Cos(gXVelX*X + gYVelX*Y) + AVelY*gXVelY*Cos(gXVelY*X + gYVelY*Y),2)
				 )
#endif
				;
		}; //End scope: caluclating the VX source


		/*
		 * Creating the solver
		 */
#ifdef IS_REG
		egd_StdTempSolverRegGrid_t solver(
#else
		egd_StdTempSolverExtGrid_t solver(
#endif
				Ny, Nx,
				alphaY,	//Temp Top
				betaY);

		egd_StdTempSolverRegGrid_t::SolverArg_ptr      solArg    = solver.createArgument();
		egd_StdTempSolverRegGrid_t::TempSourceTerm_ptr solSource = solver.creatSourceTerm();
		egd_StdTempSolverRegGrid_t::SolverResult_ptr   solRes    = solver.creatResultContainer();

		//Fill the argument
		solArg->setDT(timeStep);

		//Fill the source term
		solSource->getSource(PropIdx::Temperature() ) = sourceT;

		//Load the source term into the argument
		solArg->loadSourceTerm(std::move(solSource) );
			pgl_assert(solArg->hasSourceTerm() == true);


		/*
		 * Call the solver
		 */
		solver.solve(gridCont, heatingTerm, solArg.get(), solRes.get());


		/*
		 * Writting the file to disc
		 */
		{
			//Base folder
			const std::string BaseFolder = "/exp_" + std::to_string(i) + "/";

			//open the file
			dumper.openFile();

			//Write the exact solution
			dumper.dumpGridProperty(gridCont.getProperty(PropIdx::Temperature() ), BaseFolder + "exTemp", true);

			//Write the approximation
			dumper.dumpGridProperty(solRes->cgetTempDiff(), BaseFolder + "diffTemp", true);

			//Close the file
			dumper.closeFile();
		}; //End dump


		/*
		 * Calculate a simple error statistic
		 */
		{
			ellTwoErrorsT.at(i) = std::sqrt(solRes->getTempDiff().array().pow(2.0).sum() / solRes->getTempDiff().size());
			maxErrorsT   .at(i) = solRes->getTempDiff().array().abs().maxCoeff();
		}; //End scope: computing errors


		std::cout << "\tStop processing of " << Ny << "x" << Nx << std::endl;
	}; //End for(i): performing the experiment
	std::cout << std::endl;


	/*
	 * Output the error statistic
	 */

	//Setting some parameters to maje outputting a bit better
	const auto myWidth = 13;
	const auto moreErr = 6;
	std::cout.precision(6);


	/*
	 * Writing some information about the parameter
	 */
	if(comment.empty() == false)
	{
		std::cout
			<< "Result of experiment:\n"
			<< " " << comment << "\n\n";
	};

	std::cout
		<< " DeltaT = "  << std::setw(myWidth) << timeStep  << "\n"
		<< " alphaS = "  << std::setw(myWidth) << alphaS    << " (== 0 -> pure linear profile)\n"
		<< " alphaK = "  << std::setw(myWidth) << alphaK    << " (== 0 -> constant thermal conductivity of : " << betaK << ")\n"
		<< " eta    = "  << std::setw(myWidth) << eta_value << " (== 0 -> no shear heating)\n"
		<< "  AVelX = "  << std::setw(myWidth) << AVelX     << " (== 0 -> no x velocity)\n"
		<< "  AVelY = "  << std::setw(myWidth) << AVelY     << " (== 0 -> no y velocity)\n"
		<< " \\alpha = " << std::setw(myWidth) << thermExpansion_value << " (== 0 -> no adiabatic heating)\n";


	/*
	 * Writting the header
	 */
	std::cout << std::setw(myWidth) << "No";
	std::cout << std::setw(myWidth) << "Ny x Nx";

	std::cout << std::setw(myWidth) << "ell2_DT";
	std::cout << std::setw(moreErr) << " x";
	std::cout << std::setw(myWidth) << "max(DT)";
	std::cout << std::setw(moreErr) << " x";

	std::cout << std::endl;


	//Output the errors
	for(Size_t i = 0; i != ellTwoErrorsT.size(); ++i)
	{
		std::cout << std::setw(myWidth) << i;
		std::cout << std::setw(myWidth) << std::string(std::to_string(Ny_vec.at(i)) + "x" + std::to_string(Nx_vec.at(i)));

		std::cout << std::setw(myWidth) << ellTwoErrorsT.at(i);
		std::cout << std::setw(moreErr) << generateErrOut(ellTwoErrorsT, i);
		std::cout << std::setw(myWidth) <<    maxErrorsT.at(i);
		std::cout << std::setw(moreErr) << generateErrOut(maxErrorsT, i);

		std::cout << std::endl;
	}; //End for(i):
	std::cout << std::endl;

	return 0;
}; //End: perform test



template<
	class 	T
>
std::string
generateErrOut(
	const T& 	errSer,
	const Index_t 	i)
{
	if(i == 0)
	{
		return std::string();
	};

	return "x" + ::pgl::pgl_prettyString(errSer.at(i-1) / errSer.at(i), 2, 2);
};




