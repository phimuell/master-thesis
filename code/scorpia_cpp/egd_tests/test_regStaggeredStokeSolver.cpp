/**
 * \brief	This file implements a test for the staggered grid Stokes solver.
 *
 * It uses the testing component that this probioded by the implementation.
 */


//We are a regular grid solver, so define the macro
#define IS_REG

//Include the testing code
#include "./testImpl_StaggeredStokeSolver.hpp"

int
main(
	int 	argc,
	char**	argv)
{
	//Number of experiments
	const Size_t 	nExperiments 		= 6;
	const Size_t 	nMinGridX 		= 1 << 4;

	//This is the number of grid points that we want in X
	const intVec_t stdNx_vec = [nMinGridX, nExperiments]() -> intVec_t
			{  intVec_t Nx; Nx.push_back(nMinGridX);
			   for(Size_t i = 1; i != nExperiments; ++i)	//We have to start at one, for accounting the added one
				{ Nx.push_back(Nx.back() * 2 ); };
			   return Nx; (void)nExperiments;
			}();
	const intVec_t stdNy_vec = [](intVec_t Nx) -> intVec_t
			{  for(Size_t& i : Nx)
				{ i *= 2; };
			   return Nx;
			}(stdNx_vec);

	/*
	 * 	EXPERIMENT 1
	 *
	 * In this experiment we refine the grid further and further.
	 */
	{
		const std::string dumpFile = "regStokes_stdExperiment.hdf5";
		const std::string comm     = "Normal scaling experiment";

		performTest(stdNy_vec, stdNx_vec,
				dumpFile, comm);
	}; //End: experiment 1


	/*
	 * 	EXPERIMENT 2
	 *
	 * In this experiment we do y refinement.
	 * This means we use a large number of x grid points and consecutivly
	 * increase the number of grid points that are present.
	 */
	{
		const std::string dumpFile = "regStokes_yRefinement.hdf5";
		const std::string comm     = "Y Refinement";

		performTest(stdNy_vec, intVec_t(nExperiments, stdNx_vec.back() ),
				dumpFile, comm);
	}; //End: experiment 2


	/*
	 * 	EXPERIMENT 3
	 *
	 * In this experiment we do x refinement.
	 * This means we use a large number of y grid points and consecutivly
	 * increase the number of grid points that are present.
	 */
	{
		const std::string dumpFile = "regStokes_xRefinement.hdf5";
		const std::string comm     = "X Refinement";

		performTest(intVec_t(nExperiments, stdNy_vec.back() ), stdNx_vec,
				dumpFile, comm);
	}; //End: experiment 3

	return 0;
}; //End main

