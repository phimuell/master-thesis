/**
 * \brief	This file implements a test for the implementation routines,
 * 		 for the local interpolation.
 *
 * This test builds on a very simple observation, that is in place when local
 * interpolation is done on the regular CC grid.
 * Markers in a node will only contribute to the CC node that is in the centre
 * of the cell and to no other cell. So we have to place a few markers in each
 * cell, all markers in a given cell have the same weight. This will then
 * cause the CC node to have the same value as well.
 *
 * This program will also runn the other tests. But since the result of the
 * interpolation is hard to predict there we set all markers to the same values
 * The result is not that interesting.
 */


//Include the confg file
#include <egd_core.hpp>

#include <egd_grid/egd_util.hpp>
#include <egd_grid/egd_util_interpolState.hpp>
#include <egd_grid/egd_markerCollection.hpp>
#include <egd_grid/egd_gridGeometry.hpp>



//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include BOOST


//Include STD
#include <iostream>
#include <random>


//We use the namespace
using namespace egd;

/**
 * \brief	This function performs the interpolation.
 *
 * It will return the maximal and minimal values of the interpolated
 * values.
 *
 */
static
std::pair<Numeric_t, Numeric_t>
performTest(
	const egd_markerCollection_t& 		mColl,
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const PropIdx 				prop,
	const bool 				withWeight,
	const Numeric_t 			testValue);


int
main(
	int 	argc,
	char**	argv)
{
	//Set some basic setings about the markers
	const PropIdx   testProp  = PropIdx::Density();	//This is the property we use for testing
	const eGridType gType     = mkReg(eGridType::CellCenter);

	//How is the grid defined, we have rechtangular cells
	const Index_t 	nBasicX   =  6  ;	//Basic grid points
	const Index_t   nBasicY   =  5  ;
	const Numeric_t domXStart =  1.0;	//Startpoint in x direction
	const Numeric_t domXLen   = 10.0;	//Extension of domain in x direction.
	const Numeric_t domYStart =  1.5;	//Start in y direction
	const Numeric_t domYLen   =  4.0;	//Domain extension in y direction.

	//Set the marker properties
	const Index_t nMarkersPerCell  = 30;
	const Index_t nMarkers         = nMarkersPerCell * (nBasicX - 1) * (nBasicY - 1);


	/*
	 * Create a marker collection
	 */

	//The property list we need
	const egd_markerCollection_t::PropList_t propList =
	 	{ PropIdx::PosX(), PropIdx::PosY(), testProp };

	//Create a marker collection
	egd_markerCollection_t markerColl(nMarkers, propList);


	/*
	 * Now we set the markers to values we can use
	 */
	{
		//Get positions of markers
		auto& PosX = markerColl.getXPos();
		auto& PosY = markerColl.getYPos();
			pgl_assert(PosX.size() == nMarkers,
				   PosY.size() == nMarkers );

		//Set the test property to a certain value
		auto& P = markerColl.getMarkerProperty(testProp);
			pgl_assert(P.size() == nMarkers);

		//This is teh random generator we use to distribute the markers
		std::mt19937_64 geni(42);

		//this is the cell spacing
		const Numeric_t cSpacingX = domXLen / (nBasicX - 1);
		const Numeric_t cSpacingY = domYLen / (nBasicY - 1);

		Index_t m = 0;		//Indexing the marker

		//Iterating through the cells
		for(Index_t i = 0; i != (nBasicY - 1); ++i)
		{
			for(Index_t j = 0; j != (nBasicX - 1); ++j)
			{
				const Numeric_t mVals[] = {100.0, -200.0, -100.0, 200.0};
				const Index_t   k       = (j % 2) * 2 + (i % 2); 	pgl_assert(k < 4);
				const Numeric_t mVal    = mVals[k];

				//Get the start location of the cell
				const Numeric_t xStart_k = j * cSpacingX + domXStart;
				const Numeric_t yStart_k = i * cSpacingY + domYStart;

				std::uniform_real_distribution<Numeric_t> distX(xStart_k + 0.001, xStart_k + cSpacingX - 0.001);
				std::uniform_real_distribution<Numeric_t> distY(yStart_k + 0.001, yStart_k + cSpacingY - 0.001);

				for(Index_t q = 0; q != nMarkersPerCell; ++q)
				{
					const Numeric_t x = distX(geni);
					const Numeric_t y = distY(geni);

					PosX[m] = x;
					PosY[m] = y;
					P   [m] = mVal;

					m += 1;
				}; //End for(q):
			}; //End for(j):
		}; //End for(i):
		pgl_assert(m == nMarkers);
	}; //End scope: setting positions of markers


	/*
	 * Create a grid geometry
	 */
	egd_gridGeometry_t gridGeo((yNodeIdx_t(nBasicY)), xNodeIdx_t(nBasicX));
	gridGeo.makeConstantGrid(domXStart, domXStart + domXLen,
			         domYStart, domYStart + domYLen );
	gridGeo.finalizeGrids();


	/*
	 * Perform tests
	 */
	//This is the return value
	egd_gridProperty_t gProp(
			yNodeIdx_t(gridGeo.yNPoints()), xNodeIdx_t(gridGeo.xNPoints()),
			gType,
			testProp);

	//perform alsu use the the modifing feature
	egd_markerProperty_t modWeights(markerColl.nMarkers());
		modWeights.setConstant(1234.5678);

	//This is the interpolation state object
	egd_interpolState_t interpolState;

	//fill the interpolation state object
	egd_initInterpolation(markerColl, gridGeo, gType, &interpolState,
			true,		//Do processing
			true,		//Do disabeling
			true);		//Use local interpolation

	if(interpolState.hasLocalAssoc() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The interpol state is not local.");
	};
	if(interpolState.isFinite() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The interpol state is not finite.");
	};

	//Perform the interpolation
	egd_mapToGrid(interpolState, markerColl.getMarkerProperty(testProp),
			&gProp,
			&modWeights,
			false,		//Disabeling of ghost nodes
			true);		//use local interol

	std::cout << "Interpolated grid property, ignoring ghost nodes:\n" << gProp.matrix() << std::endl;

	std::cout
		<< "\n" << "Grid weights with disabeled ghost nodes:\n" << interpolState.gWeights << std::endl;


	std::cout
		<< "\n"
		<< "PLEASE CHECK THE OUTPUT!"
		<< "\n"
		<< std::endl;


	/*
	 * Perform the all grid types test
	 */
	{

		std::cout
			<< "\n"
			<< "Now we will test all grids. Here all markers have the same values.\n"
			<< "This means also the nodes must have the same values.\n"
			<< "Once the interpolation will be done with weights and once without."
			<< "\n\n";

		//This is a list of all supported grids
		pgl::pgl_vector_t<eGridType> gTypes =
		{ mkReg(eGridType::BasicNode), mkReg(eGridType::StVx), mkReg(eGridType::StVy), mkReg(eGridType::StPressure), mkReg(eGridType::CellCenter),
						mkExt(eGridType::StVx), mkExt(eGridType::StVy), mkExt(eGridType::StPressure), mkExt(eGridType::CellCenter) };

		const Numeric_t testValue = std::exp(1) * std::atan(1) * 4;	//This is the test value that is used

		Index_t i = 0;
		for(const eGridType gType : gTypes)
		{
			i += 1;
			std::cout
				<< "gType = " << gType << "\n";

			//set the property to the value we need
			markerColl.getMarkerProperty(testProp).setConstant(testValue);

			const auto px   = performTest(markerColl, gridGeo, gType, testProp, true , testValue);
			const auto pw   = performTest(markerColl, gridGeo, gType, testProp, false, testValue);
			const auto mi_x = px.first;
			const auto ma_x = px.second;
			const auto mi_w = pw.first;
			const auto ma_w = pw.second;

			std::cout
				<< "\t" << "min ~ " << mi_x      << ", without ~ " << mi_w << "\n"
				<< "\t" << "max ~ " << ma_x      << ", without ~ " << ma_w << "\n"
				<< "\t" << "tVa ~ " << testValue << "\n"
				<< std::endl;
		}; //End for(i):
	}; //End scope: test all grid types

	return 0;
}; //End main


/*
 * ====================================
 * Helper functions
 */

std::pair<Numeric_t, Numeric_t>
performTest(
	const egd_markerCollection_t& 		mColl,
	const egd_gridGeometry_t& 		gridGeo,
	const eGridType 			gType,
	const PropIdx 				prop,
	const bool 				withWeight,
	const Numeric_t 			testValue)
{
	//This is the return value
	egd_gridProperty_t gProp(
			yNodeIdx_t(gridGeo.yNPoints()), xNodeIdx_t(gridGeo.xNPoints()),
			gType,
			prop);

	//perform alsu use the the modifing feature
	egd_markerProperty_t modWeights(mColl.nMarkers());
		modWeights.setConstant(1234.5678);

	//This is the interpolation state object
	egd_interpolState_t interpolState;

	//fill the interpolation state object
	egd_initInterpolation(mColl, gridGeo, gType, &interpolState,
			true,		//Do processing
			true,		//Do disabeling, but will not mater,s icne we use the modifiyt weights.
			true);		//Local interpolation

	//Perform the interpolation
	egd_mapToGrid(interpolState, mColl.getMarkerProperty(prop),
			&gProp,
			withWeight ? &modWeights : nullptr,
			true);		//Disable the ghost node

	return std::make_pair(gProp.maxView().minCoeff(), gProp.maxView().maxCoeff());
	(void)testValue;
}; //End: perforrm test



