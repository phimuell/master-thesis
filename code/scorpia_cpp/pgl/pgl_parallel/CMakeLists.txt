# This CMake file will add all the files in the current folder
# to the PGL library.
 
target_sources(
	pgl
  PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/ppgl_mutex.cpp"
  PUBLIC
	"${CMAKE_CURRENT_LIST_DIR}/ppgl_core.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/ppgl_mutex.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/ppgl_openMP.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/ppgl_synced_buff_io.hpp"
)
