#pragma once
#ifndef PGL_PARALLEL_IG_PARALLEL_MUTEX_HPP
#define PGL_PARALLEL_IG_PARALLEL_MUTEX_HPP
/**
 * \brief	This file provides mutex for pgl
 *
 * This header provides mutex.
 * It also provides some locks gurds
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>


//Include std
#include <mutex>



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(parallel)


/**
 * \brief	Casic mutex
 * \class 	ppgl_mutex_t
 *
 * This is a basic mutex.
 * It can be locked and unlocked.
 * It is not wise to use it directly, instead a lock guard should be used.
 */
using ppgl_mutex_t = ::std::mutex;


/**
 * \brief	This is a timed mutex.
 * \class 	ppgl_timedMutex_t
 *
 * One can set a time limit for waiting until the try for accessing the ownership is given up.
 */
using ppgl_timedMutex_t = ::std::timed_mutex;

/**
 * \brief	This is a normal lock gurad.
 * \class 	ppgl_lock_t
 *
 * On construction it aquires ownership of the mutex and in the destructor the mutex is unlocked.
 */
template<typename Mutex_t>
using ppgl_lockGuard_t = ::std::lock_guard<Mutex_t>;


/**
 * \brief	This is a unique lock
 * \class 	ppgl_uniqueLock_t
 *
 * It is like the lock guard but it allows some more interaction with the underling mutex
 */
template<typename Mutex_t>
using ppgl_uniqueLock_t = ::std::unique_lock<Mutex_t>;



/*
 * These variables are needed for controling the unique lock
 */
constexpr std::defer_lock_t defer_lock {};
constexpr std::try_to_lock_t try_to_lock {};
constexpr std::adopt_lock_t adopt_lock {};



/**
 * \brief	This namespace holds global locks.
 * \namespace 	globalLocks
 */
PGL_NS_BEGIN(globalLock)

/**
 * \brief	This lock is associated with the output stream
 *
 * It shall be used if one want to synchronizes access to stdout
 */
extern
ppgl_mutex_t STDOUT_LOCK;


/**
 * \brief	This lock is associated with the time functions.
 *
 * It is many used in the header 'pgl/pgl_time.hpp'
 */
extern
ppgl_mutex_t CTIME_LOCK;



PGL_NS_END(globalLock)





PGL_NS_END(parallel)
PGL_NS_END(pgl)



#endif
// End include guards




