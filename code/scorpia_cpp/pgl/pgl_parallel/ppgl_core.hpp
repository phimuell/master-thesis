#pragma once
#ifndef PGL_PARALLEL_IG_PARALLEL_CORE_HPP
#define PGL_PARALLEL_IG_PARALLEL_CORE_HPP
/**
 * \brief	This file is the core file of the parallel part of pgl.
 *
 * It defines some usefull quantities.
 * It is also included by every asspect of the pgl library that uses parallelism.
 *
 * All parallel functions of pgl lives in the parallel namespace inside the pgl namespace itself.
 * please also note that this header defines an nam,espace alias ppgl
 *
 *
 */

//Include PGL
#include <pgl_core.hpp>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(parallel)

PGL_NS_END(parallel)
PGL_NS_END(pgl)


/**
 * \namespace 	ppgl
 *
 * This is an alias for the pgl::parallel namespace
 */
namespace ppgl = ::pgl::parallel;




 #endif





