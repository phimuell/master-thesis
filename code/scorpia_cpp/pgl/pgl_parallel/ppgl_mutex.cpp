/**
 * \brief	This file provides mutex for pgl
 *
 * This header provides mutex.
 * It also provides some locks gurds
 *
 * Thsi files defines the global lock variables
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_parallel/ppgl_mutex.hpp>



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(parallel)


/*
 * \brief	This namespace holds global locks.
 * \namespace 	globalLocks
 */
PGL_NS_BEGIN(globalLock)

/*
 * \brief	This lock is associated with the output stream
 *
 * It shall be used if one want to synchronizes access to stdout
 */
ppgl_mutex_t STDOUT_LOCK;



/*
 * \brief	This lock is associated with the time functions.
 *
 * It is many used in the header 'pgl/pgl_time.hpp'
 */
ppgl_mutex_t CTIME_LOCK;


PGL_NS_END(globalLock)
PGL_NS_END(parallel)
PGL_NS_END(pgl)





