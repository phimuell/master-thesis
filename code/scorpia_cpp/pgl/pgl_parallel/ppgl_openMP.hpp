#pragma once
#ifndef PGL_PARALLEL_IG_PARALLEL_OPENMP_HPP
#define PGL_PARALLEL_IG_PARALLEL_OPENMP_HPP
/**
 * \brief	This file is the openMP header substitute offered by pgl.
 *
 * The problem with openMP is, that if it is disabled, the functions the interface defines are not declared.
 * This is a problem, because the user has to disable them by many ifdefs.
 * For that very reason, this header is defined.
 * It declares aliases for the functions, that emulates a usefull behaviour.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>

#include <pgl_parallel/ppgl_core.hpp>



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(parallel)

/*
 * I am aware that the Namespace is useless here, but this is more
 * cleaner in an idiological sense.
 */


/**
 * \brief	This macro constant is an alias of the _OPENMP macro.
 *
 * if defined openMP is enabled.
 * Its value, if defined, is the version of the openMP.
 *
 * This Macro is not managed by the user but by PGL.
 */
#undef _PGL_OMP_ENABLED
#ifdef _OPENMP
	#define _PGL_OMP_ENABLED _OPENMP
#endif

PGL_NS_END(parallel)
PGL_NS_END(pgl)

/*
 * Test if we have to include the openMP header.
 */
#ifdef _PGL_OMP_ENABLED
#	include <omp.h>
#endif




PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(parallel)


/**
 * \brief	Define a constant that indicates if openMP is enabled
 */
#ifdef _PGL_OMP_ENABLED
static constexpr bool OPENMP_IS_ENABLED = true;
#else
static constexpr bool OPENMP_IS_ENABLED = false;
#endif


/**
 * \brief	Retruns true the number of of threads in the current team.
 *
 * Returns the number of threads in the current team.
 * The binding region for an pgl_openMP_get_threads region is the innermost
 * enclosing parallel region.
 *
 * If openMP is disabled, this function returns 1.
 *
 * This function is equivalent with: omp_get_num_threads [3.2.2] [3.2.2]
 */
inline
Int_t
ppgl_openMP_get_nThreads()
{
	#ifdef _PGL_OMP_ENABLED
	return omp_get_num_threads();
	#else
	return 1;
	#endif
}; //End get team size


/**
 * \brief	This function sets the size for parallel regions.
 *
 * Notice that this function only affects subsequent parallel regions,
 * which does not specify a num_threads clause.
 *
 * This function does noting if openMP is disabled.
 *
 * This fucntion is equivalent to: omp_set_num_threads  [3.2.1] [3.2.1]
 */
inline
void
ppgl_openMP_set_nThreads(
	const Int_t 	num_threads)
{
	#ifdef _PGL_OMP_ENABLED
	return omp_set_num_threads(num_threads);
	#else
	return;
	(void)num_threads;
	#endif
}; //End set team size


/**
 * \brief	Retruns the thread ID of the calling thread.
 *
 * This function returns the ID of the tread in the current team.
 * If openMP is disabled it returns zero.
 *
 * This function is equaivalöent to: omp_get_thread_num [3.2.4] [3.2.4]
 */
inline
Int_t
ppgl_openMP_get_threadID()
{
	#ifdef _PGL_OMP_ENABLED
	return omp_get_thread_num();
	#else
	return 0;
	#endif
}; //End get thread ID


/**
 * \brief	This function returns true if openMP is enabled.
 *
 * This function returns the OPENMP_IS_ENABLED variable
 */
inline
bool
ppgl_openMP_isEnabled()
{
	return OPENMP_IS_ENABLED;
}; //End test if enable




PGL_NS_END(parallel)
PGL_NS_END(pgl)



#endif
// End include guards




