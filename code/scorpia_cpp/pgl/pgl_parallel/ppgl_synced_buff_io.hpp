#pragma once
#ifndef PPGL_HEADER_IG_PPGL_SYNCED_BUFF_IO_HPP
#define PPGL_HEADER_IG_PPGL_SYNCED_BUFF_IO_HPP
/**
 * \brief	This file implements a buffered and synced IO stream.
 */


// INclude PGL
#include <pgl_core.hpp>

#include <pgl_parallel/ppgl_core.hpp>
#include <pgl_parallel/ppgl_mutex.hpp>

//INclude STD
#include <sstream>
#include <iostream>
#include <utility>
#include <functional>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(parallel)


/**
 * \brief	This class is a buffered output stream
 * \class 	ppgl_buffered_ostream_t
 *
 * This class can be associated with any output stream and any lock that is associated with it.
 * It internaly holds a streing stream.
 * In the destructor this stream is written to the associated ostream.
 *
 */
class ppgl_buffered_ostream_t
{
	/*
	 * ========================
	 * Typedef
	 */
public:
	using Buffer_t 		= ::std::stringstream;		//!< The internaly used buffer
	using Mutex_t 		= ppgl_mutex_t;		//!< The used Mutex
	using OutStream_t 	= ::std::ostream;	//!< The stream we want to syncronize

	// Private typedefs
private:
	using Lock_t 		= ppgl_uniqueLock_t<Mutex_t>;	//!< The used lock
	using Mutex_ref 	= ::std::reference_wrapper<Mutex_t>;
	using Stream_ref 	= ::std::reference_wrapper<::std::ostream>;	//!< A reference to the associated stream


	/*
	 * =========================
	 * Constructors all constructores beside the destructor are deleted
	 * There is one building constructor
	 */
public:
	ppgl_buffered_ostream_t() = delete;
	ppgl_buffered_ostream_t(const ppgl_buffered_ostream_t&) = delete;
	ppgl_buffered_ostream_t(ppgl_buffered_ostream_t&&) = delete;
	ppgl_buffered_ostream_t& operator= (const ppgl_buffered_ostream_t&) = delete;
	ppgl_buffered_ostream_t& operator= (ppgl_buffered_ostream_t&&) = delete;

	/**
	 * \brief	This is the building constructor.
	 *
	 * It associates this with a stream
	 *
	 * \param 	Stream 		The stream we wahnt to write to
	 * \param 	Lock 		The mutex that is assigned to that stream
	 */
	ppgl_buffered_ostream_t(
		OutStream_t& 		Stream,
		Mutex_t& 		Mutex) :
	  m_stream(Stream),
	  m_mutex(Mutex),
	  m_buff()
	{
	};

	/**
	 * \brief 	The destructor writes to the associated stream
	 */
	~ppgl_buffered_ostream_t()
	{
		this->flush();
	};


	/**
	 * \brief	This is a templated outstream operator.
	 *
	 * It passes everything to the buffer
	 */
	template<typename T>
	ppgl_buffered_ostream_t&
	operator<< (
		const T& 	a)
	{
		m_buff << a;

		return *this;
	};

	ppgl_buffered_ostream_t&
	operator<< (
		::std::ostream&(*func)(::std::ostream&))
	{
		m_buff << func;

		return *this;
	};


	/**
	 * \brief	Convertion to an output stream is possible
	 *
	 * It will return the buffer
	 */
	explicit
	operator ::std::ostream&()
	{
		return m_buff;
	};


	/*
	 * ========================
	 * Private function
	 */
private:

	/**
	 * \brief	Tis is the flusch function
	 *
	 * It will aquire the lock and then write the buffer to the string
	 */
	void
	flush()
	{
		const std::string sout = m_buff.str();

		Lock_t Lok(m_mutex.get());

		m_stream.get() << sout << std::flush;

		return;
	};


	/*
	 * ========================
	 * Private Varioables
	 */
private:
	Stream_ref 		m_stream;
	Mutex_ref 		m_mutex;
	Buffer_t 		m_buff;
}; //End class(ppgl_buffered_ostream_t)


/**
 * \brief	This is a macro makes the cout synced and buffered
 */
#define PPGL_COUT ::pgl::parallel::ppgl_buffered_ostream_t(::std::cout, ::pgl::parallel::globalLock::STDOUT_LOCK)
#define PPGL_CERR ::pgl::parallel::ppgl_buffered_ostream_t(::std::cerr, ::pgl::parallel::globalLock::STDOUT_LOCK)



PGL_NS_END(parallel)
PGL_NS_END(pgl)


#endif
// End IG
