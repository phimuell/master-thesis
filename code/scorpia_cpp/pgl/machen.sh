machen()
{
	NHeader="$(ls -1 . | grep -P '.*\.hpp' | wc -l)"
	NSource="$(ls -1 . | grep -P '.*\.cpp' | wc -l)"
	O="CMakeLists.txt"

	echo "# This CMake file will add all the files in the current folder" > CMakeLists.txt
	echo "# to the PGL library." >> CMakeLists.txt
	echo " " >> $O

	echo "target_sources(" >> $O
	echo -e "\tpgl" >> $O
	echo "  PRIVATE" >> $O

	if [ ${NSource} -gt 0 ]
	then
		ls -1 . | grep -P '.*\.cpp' | while read D
		do
			echo -e "\t\"\${CMAKE_CURRENT_LIST_DIR}/${D}\"" >> $O
		done
	fi

	echo "  PUBLIC" >> $O

	if [ ${NHeader} -gt 0 ]
	then
		ls -1 . | grep -P '.*\.hpp' | while read D
		do
			echo -e "\t\"\${CMAKE_CURRENT_LIST_DIR}/${D}\"" >> $O
		done
	fi

	echo ")" >> $O

	return 0
}
