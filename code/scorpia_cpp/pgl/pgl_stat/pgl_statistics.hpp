#pragma once
/**
 * \file	pgl/pgl/pgl_statistic.hpp
 * \brief	Statistic helper class.
 *
 * This is a simple statistic class
 * It is able to collect a seareas of measurements.
 *
 * To calculate the mean and the standard derivation Welford's algorithm is used.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>


//Include std
#include <limits>
#include <utility>
#include <type_traits>


PGL_NS_START(pgl)


/**
 * \class 	pgl_statistic_t
 * \brief	Provides a statistic object
 *
 * This class implements an online algorithm to compute the mean, variance, min and max of a series.
 *
 * If not engouh samples where recorded the variances retunr -1.
 * The mean is zero.
 * Min and max are unspecific
 */
class pgl_statistic_t
{
public:

	/*
	 * Constructors
	 */
	pgl_statistic_t() noexcept;
	inline pgl_statistic_t(const pgl_statistic_t&) noexcept = default;
	inline pgl_statistic_t(pgl_statistic_t&&) noexcept = default;

	inline
	pgl_statistic_t&
	operator= (
		const pgl_statistic_t&) noexcept = default;

	inline
	pgl_statistic_t&
	operator= (
		pgl_statistic_t&&) noexcept = default;

	inline
	~pgl_statistic_t() noexcept = default;



	/*
	 * Getter
	 */

	//PRE:  -
	//POST: The mean oh the records value is returned, if zero values are recorded zero is returned
	Numeric_t
	get_Mean() const;

	//PRE:  At least two records were added, otherwhise -1 is returned
	//POST:	The Variance is returned (Square of the standard derivation)
	Numeric_t
	get_Variance() const;


	//PRE:  At least two records were added, otherwhise -1 is returned
	//POST:	The empirical Variance is returned (Square of the standard derivation)
	Numeric_t
	get_eVariance() const;


	//PRE:  -
	//POST: Returns the numbers of records
	Size_t
	get_nRecords() const;


	//PRE:  -
	//POST: Resets everything to the begining
	pgl_statistic_t&
	reset();


	//PRE:  At least one result
	//POST: Retunrs the maximum value
	Numeric_t
	get_Max() const;

	//PRE:  At least one recorded sample
	//POST: Returns the minimum spaple seen
	Numeric_t
	get_Min() const;


	//Returns the std deviation of the recorded samples
	Numeric_t
	get_StdDeviation() const;

	//Does that that you are expecting
	void
	swap(
		pgl_statistic_t& other);
	/*
	 * Add a new record
	 */
	inline
	pgl_statistic_t&
	add_Record(
		const Numeric_t& newRecord) noexcept
	{

		if(m_n == 0)
		{
			m_min = newRecord;
			m_max = newRecord;
		};

		if(newRecord < m_min)
		{
			m_min = newRecord;
		};

		if(m_max < newRecord)
		{
			m_max = newRecord;
		};


		m_n += 1;
		const Real_t delta = newRecord - m_mu;
		m_mu += delta / m_n;
		const Real_t delta2 = newRecord - m_mu;
		m_M2 += delta * delta2;

		return *this;
	};





private:
	Real_t 		m_mu;
	Real_t 		m_M2;
	Numeric_t 	m_max;
	Numeric_t 	m_min;
	Size_t 		m_n;
}; //End class(statistic_t)


PGL_NS_END(pgl)


