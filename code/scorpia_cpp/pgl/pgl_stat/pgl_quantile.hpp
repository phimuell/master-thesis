#pragma once
/**
 * \brief	This is a class for optinaing the quantiles form a histogram.
 *
 * This class can be builded using a histogram.
 * One could then use it to compute a quantile an so on.
 * It is also capabel of be used as CDF function object.
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl_stat/pgl_statistics.hpp>





PGL_NS_BEGIN(pgl)


//Forward declaration of the hystogram
class pgl_histogram_t;
class pgl_sparseHistogram_t;


/**
 * \brief	The quiantile object of pgl
 * \class 	pgl_quantile_t
 *
 * This class is the quantile object of pgl.
 * It can be used to compute quantile.
 * It is constructed from a histogram object, it is not possible to construct it from a sparse histogram, as of now.
 *
 * This histogram is used to compute the CDF of the distribution, that is described by the histogram.
 * This CDF is also stored inside the quantile, and can be accessed.
 * Also a statistic object is accessed.
 *
 * Notice that it operates on index positions, that is it starts counting at zero.
 * Firther that the CDF is descrete.
 */
class pgl_quantile_t
{
	/*
	 * =============================
	 * Typedefs
	 */
public:
	using Histogram_t 	= ::pgl::pgl_histogram_t;		//!< The type for representing a histogram
	using SparseHistogram_t = ::pgl::pgl_sparseHistogram_t;		//!< The type of the sparse histogram
	using CDF_t 		= ::pgl::pgl_vector_t<Numeric_t>;	//!< This is the type for representing CDF
	using Statistc_t 	= ::pgl::pgl_statistic_t;		//!< This is a statistsic


	/*
	 * ==================================
	 * Constructors
	 */
public:
	/**
	 * brief	The default constructor is not allowed
	 */
	pgl_quantile_t() noexcept;

	/**
	 * \brief	Copyconstructor is default
	 */
	pgl_quantile_t(
		const pgl_quantile_t&);


	/**
	 * \brief	Moveconstructor is defaulted
	 */
	pgl_quantile_t(
		pgl_quantile_t&&) noexcept;


	/**
	 * \brief	Copyassignement is defaulted
	 */
	pgl_quantile_t&
	operator= (
		const pgl_quantile_t&);


	/**
	 * \brief	Moveassignement is defaulted
	 */
	pgl_quantile_t&
	operator= (
		pgl_quantile_t&&) noexcept;


	/**
	 * \brief	Construct a quantile object from a histogram.
	 *
	 * This is the only way for generating a usefull quantile object.
	 * It takes the histogram and computes a CDF.
	 * The different quantiles can then be queried.
	 *
	 * \param histogram	The histogram that is used
	 */
	pgl_quantile_t(
		const Histogram_t& histogram);


	/**
	 * \brief	This constructor constructs a quantile form a sparse histogram.
	 */
	pgl_quantile_t(
		const SparseHistogram_t& 	histogram);


	/*
	 * =================================
	 * Functions
	 */
public:
	/**
	 * \brief	Tis function returns true if this si empty.
	 *
	 * An empty quantile is unusual and should not happens.
	 */
	inline
	bool
	isEmpty() const noexcept
	{
		return m_cdf.size() == 0 ? true : false;
	};


	/**
	 * \brief	This function returns the number of descrete values that are stored.
	 *
	 * This is essentuialy the size of the underling array of the cdf.
	 */
	inline
	Size_t
	size() const noexcept
	{
		return m_cdf.size();
	};


	/**
	 * \brief	This function returns the CDF value at the given index position.
	 *
	 * \param 	i 	The index position
	 */
	inline
	Numeric_t
	CDFat(
		const Size_t 	i) const noexcept(false)
	{
		return m_cdf.at(i);
	};


	/**
	 * \brief	This returns the quantile level
	 *
	 * The returned value corespnds to an index in the histogram, form which this was constructed.
	 * If the return value is i, for a certen level l, then this means that all the observations recoreded in [0, i] represents l percent of the observations.
	 *
	 * There are some rules for finding i
	 * - if l is 50 then both bounds are computed and the midel is computed
	 * - if it is not 50 only the lower bound is computed
	 *
	 * \param level 	The level, a number in the region [0, 100]
	 *
	 * If teh level is bellow 0 ore above 100 an exception is generated.
	 */
	Size_t
	computeQuantile(
		const Size_t level) const noexcept(false);


	/**
	 * \brief	This function is for computing the median.
	 *
	 * It is equivalent as if computeQuantile is called with an argument equal to 50.
	 *
	 * This method is declared noexcept and calls a function that is not.
	 * But this is not a problem, because the exception is trigered only if the argument is out of bound, and this won't happen here.
	 */
	inline
	Size_t
	median() const noexcept
	{
		return this->computeQuantile(50);
	};


	/**
	 * \brief	This function computes the 25 quantile.
	 *
	 * It is equivalent as if we would call the compute quantile function with an argument of 25
	 * This method is declared noexcept and calls a function that is not.
	 * But this is not a problem, because the exception is trigered only if the argument is out of bound, and this won't happen here.
	 */
	inline
	Size_t
	quantile25() const noexcept
	{
		return this->computeQuantile(25);
	};


	/**
	 * \brief	This function computes the 5 percent quantile
	 *
	 * This method is declared noexcept and calls a function that is not.
	 * But this is not a problem, because the exception is trigered only if the argument is out of bound, and this won't happen here.
	 */
	inline
	Size_t
	quantile5() const noexcept
	{
		return this->computeQuantile(5);
	};


	/**
	 * \brief	This function computes the 75 quantile
	 *
	 * This method is declared noexcept and calls a function that is not.
	 * But this is not a problem, because the exception is trigered only if the argument is out of bound, and this won't happen here.
	 */
	inline
	Size_t
	quantile75() const noexcept
	{
		return this->computeQuantile(75);
	};


	/**
	 * \brief	This function computes the 95 quantile
	 *
	 * This method is declared noexcept and calls a function that is not.
	 * But this is not a problem, because the exception is trigered only if the argument is out of bound, and this won't happen here.
	 */
	inline
	Size_t
	quantile95() const noexcept
	{
		return this->computeQuantile(95);
	};


	/**
	 * \brief	Retrun the mean of the data series
	 */
	inline
	Numeric_t
	getMean() const
	{
		return m_statistic.get_Mean();
	};




	/**
	 * \brief	Returns the variance of the data
	 */
	inline
	Numeric_t
	getVariance() const
	{
		return m_statistic.get_Variance();
	};


	/**
	 * \brief	Retrun the mean of the data series
	 *
	 * An alias for compability reasons
	 */
	inline
	Numeric_t
	get_Mean() const
	{
		return m_statistic.get_Mean();
	};


	/**
	 * \brief	Returns the variance of the data
	 *
	 * An alias for compability reasons
	 */
	inline
	Numeric_t
	get_Variance() const
	{
		return m_statistic.get_Variance();
	};



	/**
	 * \brief	Returns a copy of the statistic object
	 */
	inline
	Statistc_t
	getStatistic() const
	{
		return m_statistic;
	};





	/*
	 * =========================
	 * Private Function
	 */
private:
	/**
	 * \brief	This function takes the histogram and computes the CDF of it
	 *
	 * The quantile must be empty.
	 *
	 * \param histogram 	The histogram that is analyzed.
	 */
	void
	analyzeHistogram(
		const Histogram_t& 	histogram);

	/**
	 * \brief	This function analyizes a sparse histogram
	 */
	void
	analyzeHistogram(
		const SparseHistogram_t& 	histogram);


	/**
	 * \brief	This is a reset function.
	 *
	 * This function sould not be used by the user
	 */
	void
	Reset();



	/*
	 * ==============
	 * Private Member
	 */
private:
	CDF_t 		m_cdf;		//!< This variable stores the cdf
	Statistc_t 	m_statistic;	//!< This is a statistic that stores, mean and variance of the data series
}; //End class(pgl_quantile_t)





PGL_NS_END(pgl)









