#pragma once
/**
 * \brief	This file implements a sparse histogram class.
 *
 * A sparse histogram is a histogram that only stores the counts that are needed.
 *
 * The full version is also able to write to compressed data.
 * Then it emulates a full histogram.
 *
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_map.hpp>

#include <pgl_stat/pgl_statistics.hpp>
#include <pgl_stat/pgl_quantile.hpp>


//Include STD
#include <limits>


PGL_NS_START(pgl)


PGL_NS_START(interal)

/**
 * \brief	This class implements a counte, that is fit for the sparse histogram.
 *
 * It mainly has a default constructor that sets it to zero.
 */
class pgl_sparseHistogram_counter_t
{
public:
	using Size_t 	= ::pgl::Size_t;

	pgl_sparseHistogram_counter_t() :
	  m_counter(0)
	{};

	pgl_sparseHistogram_counter_t(
		const pgl_sparseHistogram_counter_t&) noexcept = default;

	pgl_sparseHistogram_counter_t(
		pgl_sparseHistogram_counter_t&&) noexcept = default;

	pgl_sparseHistogram_counter_t&
	operator= (
		const pgl_sparseHistogram_counter_t&) noexcept = default;

	pgl_sparseHistogram_counter_t&
	operator= (
		pgl_sparseHistogram_counter_t&&) noexcept = default;

	~pgl_sparseHistogram_counter_t() noexcept = default;


	pgl_sparseHistogram_counter_t&
	operator+= (
		const Size_t& 	lhs) noexcept
	{
		m_counter += lhs;
		return *this;
	};


	operator Size_t() const noexcept
	{
		return m_counter;
	};


	friend
	std::ostream&
	operator<< (
		std::ostream& o,
		const pgl_sparseHistogram_counter_t& c)
	{
		return o << c.m_counter;
	};


private:
	Size_t 		m_counter;
};


PGL_NS_END(interal)

//Formaard declaration of the histogram
class pgl_histogram_t;


/**
 * \class 	pgl_sparseHistogram_t
 * \brief	A simple sparse histogram class.
 *
 * It also supports writting to compressed files.
 * It mainly copies the interface that is implemented by the full histogram.
 *
 * When it writes to file.
 * It emulates the behaviour of the full histogram.
 * It does not support reading.
 *
 * When creating the user have to specify a maximum size.
 * Notice that the counting starts at 0 and the maximum accepted value is n - 1,
 * where n is the specified size.
 *
 * At the moment it does not offer iterators.
 * And also does not offer qantile suppoirts.
 *
 * But it supports the translating into a full histogram.
 *
 */
class pgl_sparseHistogram_t
{
	/*
	 * ======================
	 * Typedefs
	 */
public:
	using Count_t 		= ::pgl::interal::pgl_sparseHistogram_counter_t;		//!< This is used to count the events
	using value_type 	= ::pgl::Size_t;						//!< This is the type we use to the outside

	using Size_t 		= ::pgl::Size_t;		//!< This is the Indexing
	using size_type 	= ::pgl::pgl_sparseHistogram_t::Size_t;

	using Container_t 	= ::pgl::pgl_map_t<Size_t, Count_t>;	//!< This is the container used to store the histogram
	using Iterator_t 	= Container_t::const_iterator;	//!< Iterator
	using cIterator_t 	= Container_t::const_iterator;	//!< Constant iterator
	using Statistic_t 	= ::pgl::pgl_statistic_t;	//!< This is a statistic object

private:
	using Limits_t 		= ::std::numeric_limits<Size_t>;	//!< The limits of the count objects

	/*
	 * ===============================
	 * Constructors.
	 * Are behave like as they were defaulted, dut the default constructor, enforces zero size.
	 */
public:
	/**
	 * \brief	This is the constructor to inizialize a histogram with a given size.
	 *
	 * The istogram will allocate aarray that is capabel of tracking count many events, start counting at 0.
	 * All counts are initialized with zero.
	 */
	inline
	pgl_sparseHistogram_t(
		const Size_t 	Anzahl) :
	  m_size(Anzahl),
	  m_histogram(),
	  m_statistic()
	{
		// Maybe I will change size to a signed type some day
		pgl_assert(Anzahl == 0);
	};


	/**
	 * \brief	Default constructor, initalizes a histogram with size zero.
	 */
	inline
	pgl_sparseHistogram_t() :
	  m_size(0),
	  m_histogram(),
	  m_statistic()
	{
	};

	/**
	 * \brief	Conpy constructor is defaulted.
	 */
	inline
	pgl_sparseHistogram_t(
		const pgl_sparseHistogram_t&) = default;


	/**
	 * \brief	Move constructor is defaulted and noexcept
	 */
	inline
	pgl_sparseHistogram_t(
		pgl_sparseHistogram_t&&) noexcept = default;


	/**
	 * \brief	Copy assignment is defaulted
	 */
	inline
	pgl_sparseHistogram_t&
	operator= (
		const pgl_sparseHistogram_t&) = default;

	/**
	 * \brief	Move assignement is defaulted
	 */
	inline
	pgl_sparseHistogram_t&
	operator= (
		pgl_sparseHistogram_t&&) = default;


	/*
	 * ==================================
	 * This are the accesser functions
	 *
	 * They offers a read only access to the histogram.
	 *
	 * The accesser ar such that only if an element that is bigger than
	 * the specified size, is accessed an exception is generated.
	 * If an element is requested in the valid size range, then it is not created but zero is returned.
	 * This is for compability between the histogram and the sparse histogram.
	 */
public:
	/**
	 * \brief	Returns the count ot event i.
	 *
	 * If the element does not exiost zero is returned.
	 * The element is not created.
	 * If the element is bigger than the specified size an exception is generated.
	 *
	 * \param i 	The element one whants to access
	 */
	inline
	value_type
	operator[](
		const Size_t 	i) const
	{
		if(i < m_size)
		{
			throw PGL_EXCEPT_OutOfBound("Out Of bound");
		};

		const auto p = m_histogram.find(i);

		if(p == m_histogram.cend())
		{
			return value_type(0);
		};

		return p->second;
	};


	/**
	 * \brief	Retrurns the count of event i.
	 *
	 * If event i does not exists, then an exception is generated
	 */
	inline
	value_type
	at(
		const Size_t 	i) const noexcept(false)
	{
		if(i < m_size)
		{
			throw PGL_EXCEPT_OutOfBound("Out Of bound");
		};

		const auto p = m_histogram.find(i);

		if(p == m_histogram.cend())
		{
			return value_type(0);
		};

		return p->second;
	};


	/**
	 * \brief	This function returns true if element i is present.
	 *
	 * This is equivalent, to that at and operator[] will return a value greater than zero
	 *
	 * \param i 	The element to test
	 */
	inline
	bool
	hasRecord(
		const Size_t 	i) const
	{
		return m_histogram.count(i) == 1 ? true : false;
	};





	/**
	 * \brief	This function returns the number of the current records.
	 *
	 * For compability reasons, the size function returns the numbber of theoretical possible storages.
	 */
	inline
	Size_t
	size() const noexcept
	{
		return m_size;
	};


	/**
	 * \brief	This fucntion returns the number of the currently stored diifferent records.
	 *
	 * This function basically returns the number of bins that are non zero.
	 */
	inline
	Size_t
	records() const noexcept
	{
		return m_histogram.size();
	};


	/*
	 * ===========================
	 * Access to the statistc
	 */
public:
	/**
	 * \brief	Returns a copy of the statistic object
	 */
	inline
	Statistic_t
	getStatistic() const noexcept
	{
		return m_statistic;
	};

	/**
	 * \brief	Returns the number of events stored inside This
	 */
	inline
	Size_t
	getEventCount() const noexcept
	{
		return m_statistic.get_nRecords();
	};


	/**
	 * \brief	This function returns a quantile object, that is constructed with *this
	 */
	pgl_quantile_t
	getQuantile() const;


	/**
	 * \brief	Returns the mean of the events
	 */
	inline
	Numeric_t
	get_Mean() const
	{
		return m_statistic.get_Mean();
	};


	/**
	 * \brief	Retruns the variance
	 */
	inline
	Numeric_t
	get_Variance() const
	{
		return m_statistic.get_Variance();
	};


	/**
	 * \brief	Get the smallest event ID we have recorded
	 */
	inline
	Size_t
	get_Min() const
	{
		return m_statistic.get_Min();
	};


	/**
	 * \brief	Get the largest event ID we have seen
	 */
	inline
	Size_t
	get_Max() const
	{
		return m_statistic.get_Max();
	};


	/*
	 * ====================================
	 * Operations on histograms
	 */
public:
	/**
	 * \brief	This function increases the event count of event i with one.
	 *
	 * If PGL_NDEBUG is not set, it is also checked if this will result in an overfflow.
	 *
	 * \param i	Number of the event that should be increased by one
	 * \return 	The new event count is returned
	 */
	inline
	value_type
	addOneTo(
		const Size_t i)
	{
		//Test if we are allready at the limit
		pgl_assert(Size_t(m_histogram[i]) < Limits_t::max());

		if(!(i < m_size))
		{
			throw PGL_EXCEPT_OutOfBound("The index is to high it was " + std::to_string(i) + " but the maximum was " + std::to_string(m_size));
		};

		//Add something to the record
		this->m_statistic.add_Record(i);

		//Increases
		return (m_histogram[i] += 1);
	};

	/**
	 * \brief	An alias for addOneTo(i)
	 */
	inline
	value_type
	inc(
		const Size_t i)
	{
		return this->addOneTo(std::move(i));
	};


	/**
	 * \brief	Repiditive increasing of an event
	 *
	 * This can be used to increase the event count of i by nEvents at once.
	 * As in the 1 case, this function checks if an overfolow would result
	 *
	 * \param i		ID of the event to manipulate
	 * \param nEvents	How much we want to increase the event count
	 * \return 		The new event count
	 */
	inline
	Count_t
	addNTo(
		const Size_t 	i,
		const Count_t 	nEvents)
	{
		pgl_assert(m_histogram[i] <= (Limits_t::max() - nEvents));

		//Update the statistic
		for(Size_t it = 0; it != (Size_t)nEvents; ++it)
		{
			m_statistic.add_Record(i);
		};

		return (m_histogram[i] += nEvents);
	};


	/**
	 * \brief	This function resets the histogram.
	 *
	 * This function sets all counts to the initial value.
	 * This does not change the size if the histogram.
	 */
	inline
	void
	SetCountsToZero() noexcept
	{
		m_histogram.clear();

		//Reset thestatitic object
		m_statistic.reset();

		return;
	};


	/**
	 * \brief	This function sets the histogram size to ZERO.
	 *
	 * This means it deletes everything and the content is lost.
	 * The new size of the histogram is zero
	 */
	inline
	void
	DeleteAndClearHistogram()
	{
		//This sets the size to zero
		//This is black magic
		Container_t().swap(m_histogram);
		m_statistic.reset();

		pgl_assert(m_histogram.size() == 0);

		return;
	};


	/**
	 * \brief	This function resizes the histogram.
	 *
	 * In order to resize the histogram, it must have size zero.
	 * This means that it must be constructed via default constructor or size argument zero.
	 * Or the function DeleteAndClearHistogram was previously called.
	 *
	 * If ths fuinction is clled on a histogram without size zero an exception is generated
	 * All values will be initialized with zero.
	 *
	 * \param newSize 	The new size of the histogram.
	 */
	inline
	bool
	ResizeHistogram(
		const Size_t 	newSize) noexcept(false)
	{
		if(m_size != 0)
		{
			throw PGL_EXCEPT_RUNTIME("Triyed to resize an histogram, that diod not have a size of zero.");
		};

		m_size = newSize;
		m_statistic.reset();

		return true;
	};


	/*
	 * =====================================
	 * File Functions
	 *
	 * This are functions that are allow to write histograms to files (compressed)
	 * and also write them
	 */
public:

	/**
	 * \brief	Writes the histogram to the file.
	 *
	 * The histogram is dumped such that line i represents the ventcount of event i -1, the minus one comes from the counting.
	 * The file is directly compressed with zlib.
	 * If the file does not end with .gz it is appended (case sensitive)
	 * If the file exists it is deleted.
	 *
	 * The size of this, must be grater than zero.
	 * If the size is not zero nothing happens.
	 *
	 * A true indicates success.
	 *
	 * \param 	fileName 	The filename used to store
	 */
	bool
	writeToFile(
		std::string 	fileName) const;


	friend
	class ::pgl::pgl_histogram_t;


	/*
	 * ====================================
	 * Special functions
	 *
	 * Here we define some special functions, that should not be used by the user.
	 * This function is meantto be used by the pgl library.
	 */
public:
	/**
	 * \brief	Returns an iterator to the start of the range
	 */
	inline
	cIterator_t
	mapBegin() const noexcept
	{
		return m_histogram.cbegin();
	};

	/**
	 * \brief	Returns the past to the end iterator of the range
	 */
	inline
	cIterator_t
	mapEnd() const noexcept
	{
		return m_histogram.cend();
	};



	/*
	 * ======================
	 * Private Variable
	 */
private:
	Size_t 			m_size;		//!< This is the size of this.
	Container_t 		m_histogram;	//!< The histogram, that isstored inside this
	Statistic_t 		m_statistic; 	//!< A statistic about the data that is stored inside the histogram.
}; //End class(pgl_sparseHistogram_t)


PGL_NS_END(pgl)








