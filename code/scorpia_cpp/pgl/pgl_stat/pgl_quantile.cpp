/**
 * \brief 	Implementation of some quantile functions
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_algorithm.hpp>

#include <pgl_stat/pgl_quantile.hpp>
#include <pgl_stat/pgl_histogram.hpp>
#include <pgl_stat/pgl_sparse_histogram.hpp>
#include <pgl_stat/pgl_statistics.hpp>


//Include STD
#include <numeric>


PGL_NS_BEGIN(pgl)

pgl_quantile_t::pgl_quantile_t() noexcept = default;

pgl_quantile_t::pgl_quantile_t(
	const pgl_quantile_t&) = default;


pgl_quantile_t::pgl_quantile_t(
	pgl_quantile_t&&) noexcept = default;


pgl_quantile_t&
pgl_quantile_t::operator= (
	const pgl_quantile_t&) = default;


pgl_quantile_t&
pgl_quantile_t::operator= (
	pgl_quantile_t&&) noexcept = default;


pgl_quantile_t::pgl_quantile_t(
	const Histogram_t& histogram) :
  m_cdf(),
  m_statistic()
{
	if(histogram.size() == 0)
	{
		throw PGL_EXCEPT_InvArg("The histogram has size zero.");
	};

	//Compute a histogram
	this->analyzeHistogram(histogram);
};

pgl_quantile_t::pgl_quantile_t(
		const SparseHistogram_t& 	histogram) :
  m_cdf(),
  m_statistic()
{
	if(histogram.size() == 0)
	{
		throw PGL_EXCEPT_InvArg("The histogram has size zero.");
	};

	//Compute a histogram
	this->analyzeHistogram(histogram);
};



Size_t
pgl_quantile_t::computeQuantile(
	const Size_t level) const noexcept(false)
{
	if(level > 100)
	{
		throw PGL_EXCEPT_OutOfBound("Level was too high, it was " + std::to_string(level));
	};

	if(((Numeric_t)level) < 0.0)	//I maight change the type
	{
		throw PGL_EXCEPT_OutOfBound("Level was too small, it was " + std::to_string(level));
	};


	/*
	 * This should be unneccessary but to be safe
	 */
	try
	{
		/**
		 * \TODO:
		 * There is a small error in the code.
		 * Here it does not matter much.
		 * The error is the usage of the lower_bound function.
		 * The return value of that function is the one that is "NOT LESS THAN ARG".
		 * Meaning that if the value we are looking for does not exist
		 * The next higher is used.
		 * However we do assume that it is the smaller one.
		 *
		 * This prolem is not so big, because the error is one unit.
		 *
		 * For a possible fix please see the quantile function in the caccell framework of sibyl."
		 */

		const Numeric_t valueToSearchFor = ((Numeric_t)level) / 100.0;

		if(level != 50)
		{
			//Search value not 50
			const auto foundPosition = std::lower_bound(m_cdf.cbegin(), m_cdf.cend(), valueToSearchFor);

			//Compute distance
			return std::distance(m_cdf.cbegin(), foundPosition);
		}
		else
		{
			//Search for the median
			const auto lowerPos = std::lower_bound(m_cdf.cbegin(), m_cdf.cend(), 0.5);
			const auto upperPos = std::upper_bound(m_cdf.cbegin(), m_cdf.cend(), 0.5);

			return ((std::distance(m_cdf.cbegin(), lowerPos) + std::distance(m_cdf.cbegin(), upperPos)) / 2);
		};
	}
	catch(...)
	{
		std::abort();
	};
}; //End compute quantile


void
pgl_quantile_t::analyzeHistogram(
	const Histogram_t& 	histogram)
{
	if(this->isEmpty() == false)
	{
		throw PGL_EXCEPT_RUNTIME("Tried to initialize a quantile that is not empty.");
	};

	//Compute the total number of events is also used as normalizing constant
	const Size_t TotalSum = std::accumulate(histogram.cbegin(), histogram.cend(), Size_t(0));
	const Real_t invNormConst = Real_t(1.0) / Real_t(TotalSum);

	//Make space
	m_cdf.resize(histogram.size());
	const Size_t ende = histogram.size();

	//Now compute the cdf
	Size_t cumSum = 0; //Used for the cumulative sum
	for(Size_t it = 0; it != ende; ++it)
	{
		//Update the cumulative sum
		cumSum += histogram.at(it);

		//Write the cdf
		m_cdf.at(it) = Real_t(cumSum) * invNormConst;
	}; //End for(it): Computing the cdf

	if(TotalSum != cumSum)
	{
		throw PGL_EXCEPT_RUNTIME("Error in constructing quantile.");
	};

	if(std::abs(m_cdf.back() - 1.0) > std::numeric_limits<Numeric_t>::epsilon() * 1000)
	{
		throw PGL_EXCEPT_RUNTIME("Error in constructing the cdf");
	};

	if(cumSum != histogram.getStatistic().get_nRecords())
	{
		throw PGL_EXCEPT_RUNTIME("Error, in agreement with statistic.");
	};

	//Copy the statistic
	m_statistic = histogram.getStatistic();

	return;
}; //Ende analyizing normal hiogram



void
pgl_quantile_t::analyzeHistogram(
		const SparseHistogram_t& 	histogram)
{
	if(this->isEmpty() == false)
	{
		throw PGL_EXCEPT_LOGIC("Can not analize a histogram when the quantile is not empty.");
	};

	//Resize the vector and set enything to zero
	m_cdf.resize(histogram.size(), 0);

	const Size_t SizeHist = histogram.size();
	Size_t cumSum = 0;
	SparseHistogram_t::cIterator_t currPosInHist = histogram.mapBegin();
	SparseHistogram_t::cIterator_t endeInHist    = histogram.mapEnd();

	//Iterating through the histogram to set the cummulative sum
	for(Size_t it = 0; it != SizeHist; ++it)
	{
		pgl_assert(pgl_implies(currPosInHist != endeInHist, it <= currPosInHist->first));

		//Test if the histogram is complytly scanned
		if((currPosInHist != endeInHist))
		{
			const Size_t currID = currPosInHist->first;
			if(currID == it)
			{
				//The currentPosInHistogram poins to the it location
				cumSum += (Size_t)(currPosInHist->second);
				++currPosInHist;
			}
			else
			{
				if(currID < it)
				{
					throw PGL_EXCEPT_RUNTIME("Problem with scanning the sparse histogram.");
				};
			}; //End if: same id
		}; //End if: not end iterator

		//Assigning it to the array possition
		m_cdf.at(it) = cumSum;
	}; //End for(it):


	/*
	 * Now we will invert the values to get the cdf
	 */
	const Real_t invCumSum = Real_t(1.0) / Real_t(cumSum);

	const auto ende = m_cdf.end();
	for(auto it = m_cdf.begin(); it != ende; ++it)
	{
		*it = (*it) * invCumSum;
	};

	m_statistic = histogram.getStatistic();

	return;
}; //End analyzing sparse histogram



void
pgl_quantile_t::Reset()
{
	m_statistic.reset();
	CDF_t(0).swap(m_cdf);

	return;
};



PGL_NS_END(pgl)


