/**
 * \brief	This file contains implementations for the histogram that are not suited to implemented in the header.
 *
 * This is mostly zlib.
 *
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>
#include <pgl_stat/pgl_sparse_histogram.hpp>

#include <pgl_vector.hpp>


//Include STD
#include <limits>
#include <string>
#include <sstream>


// Include the zlib for compressing
#include <zlib.h>


//Include boost
#include <boost/filesystem.hpp>
namespace File = ::boost::filesystem;


PGL_NS_START(pgl)


bool
pgl_sparseHistogram_t::writeToFile(
	std::string 	stdFileName) const
{
	//Test if consitions are meet
	//This is the maximal allowed size and not the number of recods
	if(this->size() == 0)
	{
		//Nothing to write
		pgl_assert(false);
		return false;
	};

	//Make a Pathobject
	File::path fileName(stdFileName);

	if(fileName.has_filename() == false)
	{
		pgl_assert(false);
		return false;
	};

	if(File::is_directory(fileName) == true)
	{
		pgl_assert(false);
		return false;
	};

	//Test if the filetype is correct
	if(fileName.extension().string() != ".gz")
	{
		//Fileextinsion is not the same
		fileName = File::path(stdFileName + (stdFileName.back() == '.' ? "" : ".") + "gz");
	};


	//Test if file exists and delete it
	if(File::exists(fileName) == true)
	{
		File::remove(fileName);
	};

	//Generating a string stream to store it
	//There may be better aproaches, but this one works
	std::string strOutPut;
	{
		std::stringstream s;
		using It_t = Container_t::const_iterator;
		const It_t ende = m_histogram.cend();
		It_t currentHistEntry = m_histogram.cbegin();

		for(Size_t it = 0; it != m_size; ++ it)
		{
			//test if the current
			if(currentHistEntry == ende || (currentHistEntry->first != it))
			{
				//The current index is not in the histogram
				s << "0\n";
			}
			else
			{
				//This is the current entry
				if(currentHistEntry->first != it)
				{
					throw PGL_EXCEPT_RUNTIME("JKJ");
				};

				//Write the entry
				s << currentHistEntry->second << "\n";

				//Increment the iterator
				++currentHistEntry;
			}; //End if: test if complete
		};//End for)it): Wrioting

		//Save to the string
		strOutPut = s.str();
	};



	// Now we can start compressing

	//Open the compressed file
	gzFile compOut = gzopen(fileName.c_str(), "wb");

        if(compOut == nullptr)
        {
		pgl_assert(false);
        	return false;
        };

        // Set the buffer
        if(gzbuffer(compOut, 1 << 16))
        {
		pgl_assert(false);
		gzclose(compOut);
        	return false;
        };

        if(Z_OK != gzsetparams(compOut, Z_BEST_COMPRESSION, Z_DEFAULT_STRATEGY))
        {
		pgl_assert(false);
        	gzclose(compOut);
        	return false;
        };

        //Write to the file
        if(0 == gzwrite(compOut, (voidpc)strOutPut.c_str(), strOutPut.size() ))
        {
                std::cout << "Error while writing.\n";
                std::exit(1);
        };

	//Everything has gone smothly, so we exiting now
	if(Z_OK != gzclose(compOut))
	{
		throw PGL_EXCEPT_RUNTIME("Could not close the gzFile.");
	};

	return true;
};


pgl_quantile_t
pgl_sparseHistogram_t::getQuantile() const
{
	return pgl_quantile_t(*this);
};





PGL_NS_END(pgl)


