/**
 * \brief	This file contains implementations for the histogram that are not suited to implemented in the header.
 *
 * This is mostly zlib.
 *
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl_stat/pgl_histogram.hpp>
#include <pgl_stat/pgl_sparse_histogram.hpp>
#include <pgl_stat/pgl_quantile.hpp>


//Include STD
#include <limits>
#include <string>
#include <sstream>


// Include the zlib for compressing
#include <zlib.h>


//Include boost
#include <boost/filesystem.hpp>
namespace File = ::boost::filesystem;


PGL_NS_START(pgl)


pgl_quantile_t
pgl_histogram_t::getQuantile() const
{
	return pgl_quantile_t(*this);
};


pgl_histogram_t::pgl_histogram_t(
	const pgl_sparseHistogram_t& 	sHist) :
  m_histogram(sHist.m_size),
  m_statistic(sHist.m_statistic)
{
	const auto ende = sHist.m_histogram.cend();
	for(auto it = sHist.m_histogram.cbegin(); it != ende; ++it)
	{
		m_histogram.at(it->first) = Size_t(it->second);
	}; //End for(it)
};

pgl_histogram_t::pgl_histogram_t(
	const Container_t& 	Array) :
  pgl_histogram_t(Array.size())	//Call the cinstructor with size
{
	const Size_t ende = Array.size();
	for(Size_t it = 0; it != ende; ++it)
	{
		if(Array[it] != 0)
		{
			this->addNTo(it, Array[it]);
		}; //End if: Add events

	}; //End for(it): Filling the internall array
};


bool
pgl_histogram_t::writeToFile(
	std::string 	stdFileName) const
{
	//Test if consitions are meet
	if(m_histogram.size() == 0)
	{
		//Nothing to write
		return false;
	};

	//Make a Pathobject
	File::path fileName(stdFileName);

	if(fileName.has_filename() == false)
	{
		return false;
	};

	if(File::is_directory(fileName) == true)
	{
		return false;
	};

	//Test if the filetype is correct
	if(fileName.extension().string() != ".gz")
	{
		//Fileextinsion is not the same
		fileName = File::path(stdFileName + (stdFileName.back() == '.' ? "" : ".") + "gz");
	};


	//Test if file exists and delete it
	if(File::exists(fileName) == true)
	{
		File::remove(fileName);
	};

	//Generating a string stream to store it
	//There may be better aproaches, but this one works
	std::string strOutPut;
	{
		std::stringstream s;
		for(const Count_t& c : m_histogram)
		{
			s << c << '\n';
		};

		strOutPut = s.str();
	};



	// Now we can start compressing

	//Open the compressed file
	gzFile compOut = gzopen(fileName.c_str(), "wb");

        if(compOut == nullptr)
        {
        	return false;
        };

        // Set the buffer
        if(gzbuffer(compOut, 1 << 16))
        {
		gzclose(compOut);
        	return false;
        };

        if(Z_OK != gzsetparams(compOut, Z_BEST_COMPRESSION, Z_DEFAULT_STRATEGY))
        {
        	gzclose(compOut);
        	return false;
        };

        //Write to the file
        if(0 == gzwrite(compOut, (voidpc)strOutPut.c_str(), strOutPut.size() ))
        {
                std::cout << "Error while writing.\n";
                std::exit(1);
        };

	//Everything has gone smothly, so we exiting now
	if(Z_OK != gzclose(compOut))
	{
		throw PGL_EXCEPT_RUNTIME("Could not close the gzFile.");
	};

	return true;
};




bool
pgl_histogram_t::loadFromFile(
	const std::string& 	fileName)
{
	if(m_histogram.size() != 0)
	{
		return false;
	};

	File::path fileNamePath(fileName);
	if(File::exists(fileNamePath) == false)
	{
		throw PGL_EXCEPT_RUNTIME("File does not exists.");
	};


	gzFile inFile = gzopen(fileName.c_str(), "rb");
	if(inFile == nullptr)
	{
		throw PGL_EXCEPT_RUNTIME("Could not open the file.");
	};

	const Size_t len = 1024;
	char* const line = new char[len];

	//Reset statistic
	m_statistic.reset();

	do
	{
		char* readLine = gzgets(inFile, line, len);

		// Test if we have readched the end of line
		if(readLine != line)
		{
			break;
		};

		m_histogram.push_back(std::stoull(std::string(readLine)));

		//Update statistic
		const Size_t ende = m_histogram.back(); //This many entries are new
		const Size_t i 	  = m_histogram.size() - 1; //This is the position in the array
		for(Size_t it = 0; it !=ende; ++it)
		{
			m_statistic.add_Record(i);
		}; //End for(it): updating the statistic

	} while(true);


	// Test if the end of file was reached
	if(gzeof(inFile) == false)
	{
		delete[] line;
		throw PGL_EXCEPT_RUNTIME("Loop was exited until eof was established.");
	};

	//Close
	if(Z_OK != gzclose(inFile))
	{
		delete[] line;
		throw PGL_EXCEPT_RUNTIME("Could not close the file.");
	};

	inFile = nullptr;

	delete[] line;

	m_histogram.shrink_to_fit();

	return true;
};




PGL_NS_END(pgl)


