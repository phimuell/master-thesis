#pragma once
/**
 * \brief	This file implements a histogram class.
 *
 * This file offers a histogram functionality.
 * It is relatively simple, but a cleaner solution as using plain arraies.
 *
 * It is also able to write to file.
 * This writing is only the count number
 * The line number represents the index of the event, that is counted.
 *
 * This class is a header only library, but some pars are in a seperate cpp file.
 * This is done to avoid including the zlib.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl_stat/pgl_statistics.hpp>
#include <pgl_stat/pgl_quantile.hpp>


//Include STD
#include <limits>


PGL_NS_START(pgl)

//Predeclaration of a sparse histogram
class pgl_sparseHistogram_t;



/**
 * \class 	pgl_histogram_t
 * \brief	A simple histogram class.
 *
 * It also supports writting.
 *
 * As of now, this class only supports a trivial iterator.
 * This is that the iterator does not know, at whicts location in the histogram it is.
 * Also for the time beeing they only offer read access
 *
 * The container is avare of its own size.
 */
class pgl_histogram_t
{
	/*
	 * ======================
	 * Typedefs
	 */
public:
	using Count_t 		= ::pgl::Size_t;		//!< This is used to count the events
	using value_type 	= ::pgl::pgl_histogram_t::Count_t;

	using Size_t 		= ::pgl::Size_t;		//!< This is the Indexing
	using size_type 	= ::pgl::pgl_histogram_t::Size_t;

	using Container_t 	= ::pgl::pgl_vector_t<Count_t>;	//!< This is the container used to store the histogram
	using Iterator_t 	= Container_t::const_iterator;	//!< Iterator
	using cIterator_t 	= Container_t::const_iterator;	//!< Constant iterator
	using Statistic_t 	= ::pgl::pgl_statistic_t;	//!< This is a statistic object

private:
	using Limits_t 		= ::std::numeric_limits<Count_t>;	//!< The limits of the count objects

	/*
	 * ===============================
	 * Constructors.
	 * Are behave like as they were defaulted, dut the default constructor, enforces zero size.
	 */
public:
	/**
	 * \brief	This is the constructor to inizialize a histogram with a given size.
	 *
	 * The istogram will allocate aarray that is capabel of tracking count many events, start counting at 0.
	 * All counts are initialized with zero.
	 */
	inline
	pgl_histogram_t(
		const Size_t 	Anzahl) :
	  m_histogram(Anzahl, Count_t(0)),
	  m_statistic()
	{
		// Maybe I will change size to a signed type some day
		pgl_assert(Anzahl == 0);
	};


	/**
	 * \brief	Default constructor, initalizes a histogram with size zero.
	 */
	inline
	pgl_histogram_t() :
	  m_histogram(0),
	  m_statistic()
	{
	};

	/**
	 * \brief	Construct a histogram from an array.
	 *
	 * This function allows to construct a histogram out of an array.
	 *
	 * The array shall not have negative entries.
	 * Since the underling type is an unsigned int, this can not be detected.
	 *
	 * \param 	Array 		An array
	 */
	pgl_histogram_t(
		const Container_t& 	Array);


	/**
	 * \brief	Construct a full histogram from a sparse hsitogram
	 *
	 * \param 	sHist 	A sparse hsitogram
	 */
	pgl_histogram_t(
		const pgl_sparseHistogram_t& 	sHist);

	/**
	 * \brief	Conpy constructor is defaulted.
	 */
	inline
	pgl_histogram_t(
		const pgl_histogram_t&) = default;


	/**
	 * \brief	Move constructor is defaulted and noexcept
	 */
	inline
	pgl_histogram_t(
		pgl_histogram_t&&) noexcept = default;


	/**
	 * \brief	Copy assignment is defaulted
	 */
	inline
	pgl_histogram_t&
	operator= (
		const pgl_histogram_t&) = default;

	/**
	 * \brief	Move assignement is defaulted
	 */
	inline
	pgl_histogram_t&
	operator= (
		pgl_histogram_t&&) = default;


	/*
	 * ==================================
	 * This are the accesser functions
	 *
	 * They offers a read only access to the histogram
	 */
public:
	/**
	 * \brief	Returns the count ot event i.
	 *
	 * The access is tested with an assert.
	 */
	inline
	Count_t
	operator[](
		const Size_t 	i) const noexcept
	{
		pgl_assert(i < m_histogram.size());

		return m_histogram[i];
	};


	/**
	 * \brief	Retrurns the count of event i.
	 *
	 * If event i does not exists, then an exception is generated
	 */
	inline
	Count_t
	at(
		const Size_t 	i) const noexcept(false)
	{
		if(!(i < m_histogram.size()))
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access an event that did not exist.");
		};

		return m_histogram[i];
	};


	/**
	 * \brief	Returns an iterator to the start of the range
	 */
	inline
	cIterator_t
	cbegin() const noexcept
	{
		return m_histogram.cbegin();
	};

	/**
	 * \brief	Returns the past to the end iterator of the range
	 */
	inline
	cIterator_t
	cend() const noexcept
	{
		return m_histogram.cend();
	};



	/**
	 * \brief	This function returns the size of this.
	 *
	 * This is also the numbers of events that the histogram keeps track of
	 */
	inline
	Size_t
	size() const noexcept
	{
		return m_histogram.size();
	};

	/**
	 * \brief 	Returns a pointer to the start of the histogram.
	 *
	 * Use this function with care.
	 */
	inline
	const Size_t*
	data() const
	{
		if(m_histogram.size() == 0)
		{
			throw PGL_EXCEPT_LOGIC("The histogram is empty.");
		};

		return m_histogram.data();
	};


	/*
	 * ===========================
	 * Access to the statistc
	 */
public:
	/**
	 * \brief	Returns a copy of the statistic object
	 */
	inline
	Statistic_t
	getStatistic() const noexcept
	{
		return m_statistic;
	};

	/**
	 * \brief	Returns the number of events stored inside This
	 */
	inline
	Size_t
	getEventCount() const noexcept
	{
		return m_statistic.get_nRecords();
	};


	/**
	 * \brief	Returns the mean of the events
	 */
	inline
	Numeric_t
	get_Mean() const
	{
		return m_statistic.get_Mean();
	};


	/**
	 * \brief	Retruns the variance
	 */
	inline
	Numeric_t
	get_Variance() const
	{
		return m_statistic.get_Variance();
	};


	/**
	 * \brief	Get the smallest event ID we have recorded
	 */
	inline
	Size_t
	get_Min() const
	{
		return m_statistic.get_Min();
	};


	/**
	 * \brief	Get the largest event ID we have seen
	 */
	inline
	Size_t
	get_Max() const
	{
		return m_statistic.get_Max();
	};

	/**
	 * \brief	This function returns a quantile object, that is constructed with *this
	 */
	pgl_quantile_t
	getQuantile() const;



	/*
	 * ====================================
	 * Operations on histograms
	 */
public:
	/**
	 * \brief	This function increases the event count of event i with one.
	 *
	 * If PGL_NDEBUG is not set, it is also checked if this will result in an overfflow.
	 *
	 * \param i	Number of the event that should be increased by one
	 * \return 	The new event count is returned
	 */
	inline
	Count_t
	addOneTo(
		const Size_t i)
	{
		//Test if we are allready at the limit
		pgl_assert(m_histogram.at(i) < Limits_t::max());

		//Add something to the record
		this->m_statistic.add_Record(i);

		//Increases
		return (m_histogram.at(i) += Count_t(1));
	};

	/**
	 * \brief	An alias for addOneTo(i)
	 */
	inline
	Count_t
	inc(
		const Size_t i)
	{
		return this->addOneTo(std::move(i));
	};


	/**
	 * \brief	Repiditive increasing of an event
	 *
	 * This can be used to increase the event count of i by nEvents at once.
	 * As in the 1 case, this function checks if an overfolow would result
	 *
	 * \param i		ID of the event to manipulate
	 * \param nEvents	How much we want to increase the event count
	 * \return 		The new event count
	 */
	inline
	Count_t
	addNTo(
		const Size_t 	i,
		const Count_t 	nEvents)
	{
		pgl_assert(m_histogram.at(i) <= (Limits_t::max() - nEvents));

		//Update the statistic
		for(Size_t it = 0; it != (Size_t)nEvents; ++it)
		{
			m_statistic.add_Record(i);
		};

		return (m_histogram.at(i) += nEvents);
	};


	/**
	 * \brief	This function resets the histogram.
	 *
	 * This function sets all counts to the initial value.
	 * This does not change the size if the histogram.
	 */
	inline
	void
	SetCountsToZero() noexcept
	{
		//Set all to zero
		std::fill(m_histogram.begin(), m_histogram.end(), Count_t(0));

		//Reset thestatitic object
		m_statistic.reset();

		return;
	};


	/**
	 * \brief	This function sets the histogram size to ZERO.
	 *
	 * This means it deletes everything and the content is lost.
	 * The new size of the histogram is zero
	 */
	inline
	void
	DeleteAndClearHistogram()
	{
		//This sets the size to zero
		//This is black magic
		Container_t(0).swap(m_histogram);
		m_statistic.reset();

		pgl_assert(m_histogram.size() == 0);

		return;
	};


	/**
	 * \brief	This function resizes the histogram.
	 *
	 * In order to resize the histogram, it must have size zero.
	 * This means that it must be constructed via default constructor or size argument zero.
	 * Or the function DeleteAndClearHistogram was previously called.
	 *
	 * If ths fuinction is clled on a histogram without size zero an exception is generated
	 * All values will be initialized with zero.
	 *
	 * \param newSize 	The new size of the histogram.
	 */
	inline
	bool
	ResizeHistogram(
		const Size_t 	newSize) noexcept(false)
	{
		if(m_histogram.size() != 0)
		{
			throw PGL_EXCEPT_RUNTIME("Triyed to resize an histogram, that diod not have a size of zero.");
		};

		m_histogram.assign(newSize, Count_t(0));
		m_statistic.reset();

		return m_histogram.size() == newSize ? true : false;
	};


	/*
	 * =====================================
	 * File Functions
	 *
	 * This are functions that are allow to write histograms to files (compressed)
	 * and also write them
	 */
public:

	/**
	 * \brief	Writes the histogram to the file.
	 *
	 * The histogram is dumped such that line i represents the ventcount of event i -1, the minus one comes from the counting.
	 * The file is directly compressed with zlib.
	 * If the file does not end with .gz it is appended (case sensitive)
	 * If the file exists it is deleted.
	 *
	 * The size of this, must be grater than zero.
	 * If the size is not zero nothing happens.
	 *
	 * A true indicates success.
	 *
	 * \param 	fileName 	The filename used to store
	 */
	bool
	writeToFile(
		std::string 	fileName) const;


	/**
	 * \brief	This reads in a histogram from file.
	 *
	 * *this must have size zero and the histogram must be compressed used zlib.
	 * The only garantee that exists is, that a histogram dumped by this histogram class can be read correctly.
	 *
	 * If this does not have size zero, the operation false and the function has no effects.
	 *
	 * A true indicates success.
	 *
	 * \param 	fileName 	The filename that contains the histogram
	 */
	bool
	loadFromFile(
		const std::string& 	fileName);






	/*
	 * ======================
	 * Private Variable
	 */
private:
	Container_t 		m_histogram;	//!< The histogram, that isstored inside this
	Statistic_t 		m_statistic; 	//!< A statistic about the data that is stored inside the histogram.
}; //End class(pgl_histogram_t)


PGL_NS_END(pgl)








