/*
 * This files implements the statistic class
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>

#include "pgl_statistics.hpp"


//Include std
#include <limits>
#include <cmath>
#include <utility>
#include <type_traits>


PGL_NS_START(pgl)



pgl_statistic_t::pgl_statistic_t() noexcept :
	m_mu(0.0),
	m_M2(0.0),
	m_n(0)
{
};
//End constructor


void
pgl_statistic_t::swap(
	pgl_statistic_t& other)
{
	std::swap(this->m_mu, other.m_mu);
	std::swap(this->m_n, other.m_n);
	std::swap(this->m_M2, other.m_M2);
	std::swap(this->m_max, other.m_max);
	std::swap(this->m_min, other.m_min);

	return;
};


Numeric_t
pgl_statistic_t::get_Min() const
{
	//pgl_assert(m_n > 0);
	return m_min;
};


Numeric_t
pgl_statistic_t::get_Max() const
{
	//pgl_assert(m_n > 0);
	return m_max;
};



pgl_statistic_t&
pgl_statistic_t::reset()
{
	m_mu = 0.0;
	m_n = 0;
	m_M2 = 0.0;

	return *this;
};

Numeric_t
pgl_statistic_t::get_Mean() const
{
	return this->m_mu;
};


Numeric_t
pgl_statistic_t::get_eVariance() const
{
	if(m_n == 1 || m_n == 0)
	{
		// In the case of one sample, we can't do anything, so we return -1
		return Numeric_t(-1);
	}
	else
	{
		pgl_assert(m_n >= 2);
		return (this->m_M2 / ((Numeric_t)(m_n -1)));
	};
};


Numeric_t
pgl_statistic_t::get_Variance() const
{
	if(m_n == 1 || m_n == 0)
	{
		return Numeric_t(-1);
	}
	else
	{
		pgl_assert(m_n >= 2);
		return (this->m_M2 / (Numeric_t)m_n);
	};
};

Size_t
pgl_statistic_t::get_nRecords() const
{
	return m_n;
};

Numeric_t
pgl_statistic_t::get_StdDeviation() const
{
	if(m_n == 1 || m_n == 0)
	{
		return Numeric_t(-1);
	}
	else
	{
		pgl_assert(m_n >= 2);
		return std::sqrt(this->get_Variance() );
	};
};







PGL_NS_END(pgl)
