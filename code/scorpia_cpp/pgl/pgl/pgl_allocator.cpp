/**
 * \file	code/pgl/pgl/pgl_allocator.cpp
 *
 * This file implements some functionality of the allocator interface.
 */

//INclude pgl
#include <pgl_core.hpp>


#if defined(PGL_USE_EXTENDED_ALLOCATOR) && (PGL_USE_EXTENDED_ALLOCATOR != 0)
#	pragma message "The usage of the extended allocators is enabled."
#else
#	pragma message "The usage of the extended allocators is disabled."
#endif

