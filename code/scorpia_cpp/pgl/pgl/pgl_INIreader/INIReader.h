#pragma once
/**
 * \file 	pgl/pgl_INIreader/INIReader.h
 * \brief 	Defines a class for reading ini-files
 *
 * This file defines a class that is able to read ini-files.
 * This class was orginaly written by Ben Hoyt (https://github.com/benhoyt/inih).
 * It is/was modyfied by Philip Müller and Stefano Weidmann.
 *
 */

#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_map.hpp>
#include <pgl_vector.hpp>
#include <string>

PGL_NS_BEGIN(pgl)


/**
 * \class 	pgl_INIReader_t
 *
 * This file defines an ini reader.
 */
class pgl_INIReader_t
{
public:

	/**
	 * \brief	Construct a pgl_INIReader_t and parse a given Filename.
	 *
	 * See ini.h for more info about the parsing.
	 */
	pgl_INIReader_t(std::string filename);


	/**
	 * \brief	Default constructor.
	 *
	 * This is the default constructor of *this.
	 * It will construct an empty ini object.
	 * Is usage is discuraged.
	 */
	pgl_INIReader_t()
	 = default;


	/**
	 * \brief	Coppy constructor.
	 *
	 * This function providesd a copy constructor.
	 */
	pgl_INIReader_t(
		const pgl_INIReader_t&)
	 = default;


	/**
	 * \brief	Copy assignment.
	 *
	 * This function is defaulted.
	 */
	pgl_INIReader_t&
	operator= (
		const pgl_INIReader_t&)
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Is defaulted.
	 */
	pgl_INIReader_t(
		pgl_INIReader_t&&)
	 = default;


	/**
	 * \brief	Move assignment
	 *
	 * Is defaulted.
	 */
	pgl_INIReader_t&
	operator= (
		pgl_INIReader_t&&)
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Is defaulted.
	 */
	~pgl_INIReader_t()
	 = default;


	/**
	 * \brief 	Return the result of ini_parse(), i.e., 0 on success, line number of
	 * 		first error on parse error, or -1 on file open error.
	 */
	int
	ParseError() const;

	/**
	 * \brief 	Return the string of the specific target.
	 *
	 * \param section	The section in the inifile
	 * \param name 		The name of the property (key)
	 * \param default_value Returned if not found, default to the empty sting)
	 */
	std::string
	Get(
		std::string section,
		std::string name,
		std::string default_value = std::string()) const;



	/**
	 * \brief 	Return the integer value of the given string
	 * 		is of type pgl::sSize_t
	 *
	 * \param section	The section in the inifile
	 * \param name 		The name of the property (key)
	 * \param default_value Returned if not found
	 */
	pgl::sSize_t
	GetInteger(
		std::string section,
		std::string name,
		const long default_value) const;

	/**
	 * \brief	This function reads int that are comma seperated.
	 *
	 * Assume you have a comma seperated list of values of integer in a siongle target.
	 * Then this function is used to exctract them and seperate them also
	 *
	 * \param section	The section in the inifile
	 * \param name 		The name of the property (key)
	 */
	Vector_t<sSize_t>
	getIntegerList(
		std::string section,
		std::string name) const;

	/**
	 * \brief 	Return the double value of the given string
	 * 		is of type pgl::Numeric_t
	 *
	 * \param section	The section in the inifile
	 * \param name 		The name of the property (key)
	 * \param default_value Returned if not found
	 */
	double
	GetReal(
		std::string section,
		std::string name,
		double default_value) const;


	/**
	 * \brief	This function reads floats that are comma seperated.
	 *
	 * Assume you have a comma seperated list of values of doubles in a siongle target.
	 * Then this function is used to exctract them and seperate them also
	 *
	 * \param section	The section in the inifile
	 * \param name 		The name of the property (key)
	 */
	Vector_t<double>
	getRealList(
		std::string section,
		std::string name) const;


	/**
	 * \brief 	Return the boolean value of the given string
	 *
	 * \param section	The section in the inifile
	 * \param name 		The name of the property (key)
	 * \param default_value Returned if not found
	 */
	bool
	GetBoolean(
		std::string section,
		std::string name,
		bool default_value) const;


	/**
	 * \brief	This function tests if the key exists or not.
	 *
	 * This function will test if a value with key "section:name"
	 * can be found. Note that if the key was given, but no value
	 * was given, then this function will return true and not false.
	 * Previous versions of this function relied on the Get function
	 * and thus eonsidered this sinuation asn that case.
	 *
	 * Use the function hasValue to test if a certain key also has
	 * a value, which is also equivalent to the old behaviour.
	 *
	 * \param  section	The secontion to test.
	 * \param  name		The name to test.
	 */
	bool
	hasKey(
		const std::string& 		section,
		const std::string& 		name)
	 const;


	/**
	 * \brief	This function tests if a key has a value or not.
	 *
	 * This function is simmilar to first calling Get, and testing if
	 * the returned value is empty or not. However this function is
	 * more efficient.
	 * Note that a main difference to the hasKey function is, that this
	 * function considers the empty value as not pressent.
	 *
	 * \param  section	The secontion to test.
	 * \param  name		The name to test.
	 *
	 * \note	This function is equivalent to the old behaviour of
	 * 		 the hasKey function.
	 */
	bool
	hasValue(
		const std::string& 		section,
		const std::string& 		name)
	 const;


	/**
	 * \brief	This function returns true if *this has any keys.
	 *
	 * Then no keys where found. This implies that _all_ calls to
	 * hasKey will return false, or generate an error if the arguments
	 * were illegal.
	 */
	bool
	hasKeys()
	 const
	 noexcept;


	/*
	 * ===========================
	 * Private member function$
	 */

	/*
	 * \brief 	Splits the given string into tokens, using commatas as seperation.
	 * 		Strings are trimmed and empty strings are eliminated.
	 */
	static
	Vector_t<std::string>
	splitIntoTokens(
		std::string toSplit);



	/*
	 * ==========================================
	 * Private Member variable
	 */
private:
	int _error;
	pgl::Map_t<std::string, std::string> _values;
	static std::string MakeKey(std::string section, std::string name);
	static int ValueHandler(void* user, const char* section, const char* name,
		const char* value);
}; //End class(pgl_INIReader_t)



PGL_NS_END(pgl)
