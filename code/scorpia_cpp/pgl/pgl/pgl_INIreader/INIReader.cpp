/**
 * \brief	Implementation of the ini reader.
 *
 * Originaly written by Ben Hoyt.
 */

#include <pgl_core.hpp>

#include <algorithm>
#include <cctype>
#include <cstdlib>
#include "ini.h"
#include "INIReader.h"
#include <pgl_int.hpp>
#include <pgl_map.hpp>
#include <pgl_vector.hpp>
#include <pgl_exception.hpp>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>

using std::string;

PGL_NS_BEGIN(pgl)

Vector_t<std::string>
pgl_INIReader_t::splitIntoTokens(
	std::string toSplit)
{
	Vector_t<std::string> parts;
	::boost::algorithm::split(parts, toSplit, [] (const char c) -> bool {return std::isspace(c) || c == ',';});

	const auto ende = parts.end();
	auto it = parts.begin();
	while(it != ende)
	{
		//Trim
		boost::algorithm::trim(*it);

		//Remove if empty
		if(it->empty() == true)
		{
			//Sting is empty not needen anymore
			it = parts.erase(it);
		}
		else
		{
			++it;
		};
	};

	return parts;
}; //End split into

bool
pgl_INIReader_t::hasKeys()
 const
 noexcept
{
	return (_values.size() > 1);
};


bool
pgl_INIReader_t::hasKey(
	const std::string& 	section,
	const std::string& 	name)
 const
{
	if(section.empty() )
	{
		throw PGL_EXCEPT_InvArg("The section token is empty.");
	};
	if(name.empty() )
	{
		throw PGL_EXCEPT_InvArg("The passed key token is empty:");
	};

    	//Transform section and name token into the key that is used for storing them
	const std::string key = MakeKey(section, name);

    	//Test if they are known to the map.
    	if(_values.count(key) != 0)
	{
		return true;
	}
	else
	{
		return false;
	};
}; //End: has a specific key


bool
pgl_INIReader_t::hasValue(
	const std::string& 	section,
	const std::string& 	name)
 const
{
	if(section.empty() )
	{
		throw PGL_EXCEPT_InvArg("The section token is empty.");
	};
	if(name.empty() )
	{
		throw PGL_EXCEPT_InvArg("The passed key token is empty:");
	};

    	//Transform section and name token into the key that is used for storing them
	const std::string key = MakeKey(section, name);

	//Search for the iterator of that key
	const auto it = this->_values.find(key);

	//Test if the key is stored inside the map.
    	if(this->_values.end() == it)
	{
		//The key is not known to the map, so we return true, since
		//it will never have a value.
		return true;
	}

	//We have found the key, so we must now check if the associated value
	//is not the empty string.
	return (it->second.size() == 0)
		? false 	//it is the empty string, so no value stored
		: true;		//we have strings so it is has a value.

}; //End: has a specific key



pgl_INIReader_t::pgl_INIReader_t(string filename)
{
    _error = ini_parse(filename.c_str(), ValueHandler, this);
}

int pgl_INIReader_t::ParseError() const
{
    return _error;
}


/*
 * \brief	This function reads int that are comma seperated.
 *
 * Assume you have a comma seperated list of values of integer in a siongle target.
 * Then this function is used to exctract them and seperate them also
 *
 * \param section	The section in the inifile
 * \param name 		The name of the property (key)
 */
Vector_t<sSize_t>
pgl_INIReader_t::getIntegerList(
	std::string section,
	std::string name) const
{
	std::string result = this->Get(std::move(section), std::move(name), "");

	if(result.empty() == true)
	{
		return Vector_t<sSize_t>();
	};

	auto Parts = splitIntoTokens(result);
	Vector_t<sSize_t> Values;

	for(auto& token : Parts)
	{
		Values.push_back(std::stoll(token));
	};

	return Values;
};




/*
 * \brief	This function reads floats that are comma seperated.
 *
 * Assume you have a comma seperated list of values of doubles in a siongle target.
 * Then this function is used to exctract them and seperate them also
 *
 * \param section	The section in the inifile
 * \param name 		The name of the property (key)
 */
Vector_t<double>
pgl_INIReader_t::getRealList(
	std::string section,
	std::string name) const
{
	std::string result = this->Get(std::move(section), std::move(name), "");

	if(result.empty() == true)
	{
		return Vector_t<double>();
	};

	auto Parts = splitIntoTokens(result);
	Vector_t<double> Values;

	for(auto& token : Parts)
	{
		Values.push_back(std::stod(token));
	};

	return Values;
};



string pgl_INIReader_t::Get(string section, string name, string default_value) const
{
    string key = MakeKey(section, name);
    return _values.count(key) ? _values.at(key) : default_value;
}

long pgl_INIReader_t::GetInteger(string section, string name, long default_value) const
{
    string valstr = Get(section, name, "");
    const char* value = valstr.c_str();
    char* end;
    // This parses "1234" (decimal) and also "0x4D2" (hex)
    long n = strtol(value, &end, 0);
    return end > value ? n : default_value;
}

double pgl_INIReader_t::GetReal(string section, string name, double default_value) const
{
    string valstr = Get(section, name, "");
    const char* value = valstr.c_str();
    char* end;
    double n = strtod(value, &end);
    return end > value ? n : default_value;
}

bool pgl_INIReader_t::GetBoolean(string section, string name, bool default_value) const
{
    string valstr = Get(section, name, "");
    // Convert to lower case to make string comparisons case-insensitive
    std::transform(valstr.begin(), valstr.end(), valstr.begin(), ::tolower);
    if (valstr == "true" || valstr == "yes" || valstr == "on" || valstr == "1")
        return true;
    else if (valstr == "false" || valstr == "no" || valstr == "off" || valstr == "0")
        return false;
    else
        return default_value;
}

string pgl_INIReader_t::MakeKey(string section, string name)
{
    string key = section + "=" + name;
    // Convert to lower case to make section/name lookups case-insensitive
    std::transform(key.begin(), key.end(), key.begin(), ::tolower);
    return key;
}

int pgl_INIReader_t::ValueHandler(void* user, const char* section, const char* name,
                            const char* value)
{
    pgl_INIReader_t* reader = (pgl_INIReader_t*)user;
    string key = MakeKey(section, name);
    if (reader->_values[key].size() > 0)
        reader->_values[key] += "\n";
    reader->_values[key] += value;
    return 1;
}


PGL_NS_END(pgl)


