# INIReader
The original code was written by Benn Hyot (https://github.com/benhoyt/inih).
The code is distributed under a BSD Lincece, see LICENCE.txt.

The code was modyfied by Stefano Weidmann (stefanow@ethz.ch).
And again modyfied by Philip Müller (phimuell@ethz.ch).
