#pragma once
#ifndef PGL__PGL_UNION_FINF_HPP
#define PGL__PGL_UNION_FINF_HPP
/**
 * \file	pgl/pgl_union_find.hpp
 * \brief	This file implements a union find structure.
 *
 * It is a very simple structure.
 * It is able to maintain sets, and answer queries like, is element x and y in the same set.
 * The elements must be convertible to uInt, this convertion must e unique.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_assert.hpp>

#include <pgl_container/pgl_vector.hpp>


//Include STD




PGL_NS_BEGIN(pgl)

/**
 * \brief A union find strucure
 * \class pgl_union_find_t
 *
 * This class implements a union find structure.
 * the types of the keys are fix uInt.
 *
 * To balance the structure the the method "Vereinigung nach Höhe" (DnA bBook page 435, Ask Philip)
 */
class pgl_union_find_t
{
	/*
	 * ======================
	 * Some typedefs
	 */
public:
	using Key_t 		= uInt_t;
	/**
	 * \brief 	This is the internaly stored type
	 *
	 * first stores the key and second the height.
	 */
	using value_type 	= std::pair<Key_t, uInt_t>;
	using internalSet_t	= ::pgl::Vector_t<value_type>;


	/*
	 * ======================
	 * Constructors
	 */
public:
	/// The default constructor is deleted
	pgl_union_find_t() = delete;

	/// Defaulted copy
	pgl_union_find_t(
		const pgl_union_find_t&) = default;

	/// Defaulted copy assignment
	pgl_union_find_t&
	operator= (
		const pgl_union_find_t&) = default;

	/// Defaulted move
	pgl_union_find_t(
		pgl_union_find_t&&) = default;

	/// Defaulted move assignment
	pgl_union_find_t&
	operator= (
		pgl_union_find_t&&) = default;

	/// Defaulted destructor
	~pgl_union_find_t() = default;


	/**
	 * \brief 	This is the intended build contructor
	 * \param n	The size of the structure
	 */
	inline
	pgl_union_find_t(
		uInt_t 		sizeOfSet):
	  m_set(sizeOfSet)
	{
		//Set the set in the default state
		int_make_defaute_state();
	};


	/*
	 * ===========================
	 * Public member functions
	 */
public:

	/**
	 * \brief	Retrurn the size of *this
	 */
	inline
	Size_t
	size() const
	{
		return m_set.size();
	};




	/*
	 * ==========================
	 * Set Operations
	 *
	 * Here are fucntions that deals with the set functionality.
	 */
public:

	/**
	 * \brief 	Returns the numbers of sets
	 *
	 * This is the numbers of sets, that are still in the structure.
	 * Essentialy, if you merges two sets, that are different, this number goes done, by one.
	 */
	inline
	Size_t
	countDiffSets() const
	{
		return this->m_diffSets;
	};


	/**
	 * \brief	Returns true if there is only one set left, that contains all elements
	 */
	inline
	bool
	onlyOneSet() const
	{
		return m_diffSets == 1;
	};


	/**
	 * \brief	Find Set of x
	 * \param x	The key of the element
	 *
	 * Returns the key of the element, to which x belongs to.
	 */
	inline
	Key_t
	findElement(
		const uInt_t&	x) const
	{
		pgl_assert(x < m_set.size());

		uInt_t searchKey = x;
		while(m_set[searchKey].first != searchKey)
		{
			searchKey = m_set[searchKey].first;
		}; //End while(searching for key)

		pgl_assert(m_set[searchKey].first == searchKey);
		pgl_assert(m_set[searchKey].second != 0);

		return searchKey;
	};



	/**
	 * \brief 	Returns true if y and x are in the same set
	 */
	inline
	bool
	testSameSet(
		const uInt_t& 		x,
		const uInt_t&		y) const
	{
		return findElement(x) == findElement(y);
	};

	/**
	 * \brief	Retrunrs true if x and y are in DIFFERENT sets.
	 */
	inline
	bool
	testDifferentSet(
		const uInt_t&		x,
		const uInt_t&		y) const
	{
		return !(this->testSameSet(x, y));
	};


	/**
	 * \brief	Merges two sets
	 *
	 * \param x	An element in a set
	 * \param y	An element in a set
	 *
	 * This functions merges the sets that contains x and y.
	 * They must not be the root element.
	 * If they are in the same set nothing is done
	 *
	 * \returns	True if some action was performt, false if nothing was done (like x and y are in the same set)
	 */
	inline
	bool
	mergeSets(
		const uInt_t& 		x,
		const uInt_t&		y)
	{
		const uInt_t SetX = findElement(x);
		const uInt_t SetY = findElement(y);

		if(SetX == SetY)
		{
			return false;
		};

		value_type& keyX = m_set[SetX];
		value_type& keyY = m_set[SetY];

		const uInt_t maxHight 	= std::max(keyX.second, keyY.second);
		const bool   xIsHighest	= maxHight == keyX.second;

		//Now comes the union
		if(xIsHighest == true)
		{
			//Union according to x
			keyX.second = std::max(keyX.second, keyY.second + 1);	//Calculate and set the new hight
			keyY.first  = keyX.first;				//Set the new key the first
			keyY.second = 0;					//Delete the old hight information
			pgl_assert(findElement(keyY.first) == keyX.first);
			pgl_assert(testSameSet(x, y) == true);
		}
		else
		{
			//Union acorinding to y
			keyY.second = std::max(keyY.second, keyX.second + 1);	//Calculate and set the new hight
			keyX.first  = keyY.first;				//Maxe x pointing to y
			keyX.second = 0;					//Delete the old hight information
			pgl_assert(findElement(keyX.first) == keyY.first);
			pgl_assert(testSameSet(x, y));
		};

		/*
		 * bookkeeping for the set counts
		 */
		m_diffSets -= 1;

		return true;
	};

#ifndef PGL_NDEBUG
	/**
	 * \brief	Give access to the underlying structure
	 */
	const value_type&
	at(
		const uInt_t n) const
	{
		return m_set.at(n);
	};
#endif



	/**
	 * \brief	Retruns this in a default state
	 */
	inline
	void
	resetStructure()
	{
		this->int_make_defaute_state();
		return;
	};



	/*
	 * ================================
	 * Private memberfunctions
	 */
private:

	/**
	 * \brief	Set the set into a default state
	 */
	inline
	void
	int_make_defaute_state()
	{
		//Make default values
		uInt_t i = 0;
		for(auto& element : m_set)
		{
			element.first 	= i++;
			element.second 	= 1;
		};

		//Reset the counter for the sets
		m_diffSets = m_set.size();

#ifndef PGL_NDEBUG
		for(Size_t it = 0; it != m_set.size(); ++it)
		{
			pgl_assert(m_set.at(it).first == it);
		};
#endif

		return;
	};







	/*
	 * ============================
	 * Private Memebrs
	 */
private:
	internalSet_t 		m_set;	//!< This is the set, that sotores everything
	Size_t 			m_diffSets;	//!< The numbers of different sets


}; //End class(pgl_union_find_t)





PGL_NS_END(pgl)




#endif 	//End include guard
