#pragma once
/**
 * \brief	This is a simple bitmap class
 * \file 	pgl/pgl_bitmap.hpp
 *
 * This file is a simple implementation of a bitmap file.
 * It can be created in memeory and be saved to file.
 *
 * The class to represent the colour is used from the graph subset.
 *
 * The code was taken form the course "Informatik I" at D-ITET, held by Prof F. Friedrich in AS2014.
 *
 */

// PGL include
#include <pgl_core.hpp>
#include <pgl_assert.hpp>

#include <pgl_vector.hpp>

#include <pgl_graph_utility/pgl_graph_colour.hpp>

// STD include
#include <iostream>
#include <string>

PGL_NS_START(pgl)


class pgl_bitmap_t
{
	/*
	 * ================================
	 * Typedefs
	 */
public:
	using Colour_t 		= ::pgl::graph::pgl_colour_t;		//!< This is the class for the colour
	using Size_t 		= ::pgl::Size_t;			//!< This is the size type


private:
	using internalStorage_t = ::pgl::pgl_vector_t<Colour_t>;	//!< Internal represetation of the bitmap


	/*
	 * ======================
	 * Constructors
	 *
	 * The default constructor is deleted.
	 * but the other constructors are defaulted
	 */
public:
	/**
	 * \brief 	Construct a bitmap
	 *
	 * The bitmap has the the sizes.
	 *
	 * \param width 	The width of the picture
	 * \param hight 	The hight of the picture
	 */
	pgl_bitmap_t(
		const Size_t 		width,
		const Size_t 		hight);


	inline
	pgl_bitmap_t() noexcept = delete;


	inline
	pgl_bitmap_t(
		const pgl_bitmap_t&) = default;

	inline
	pgl_bitmap_t&
	operator= (
		const pgl_bitmap_t&) = default;

	inline
	pgl_bitmap_t(
		pgl_bitmap_t&&) = default;

	inline
	pgl_bitmap_t&
	operator= (
		pgl_bitmap_t&&) = default;

	inline
	~pgl_bitmap_t() = default;



	/*
	 * ================================
	 * Accessor
	 */


	/**
	 * \brief	Return a non const reference to the given pixel.
	 *
	 * This function throws if the an out of bound excess happens.
	 *
	 * \param x	The x coordinate of the pixel
	 * \param y 	The y coordinate of the pixel
	 */
	inline
	Colour_t&
	operator () (
		const Size_t& 	x,
		const Size_t& 	y)
	{
		if(x >= m_width)
		{
			throw PGL_EXCEPT_OutOfBound("x Exceeded width");
		};

		if(y >= m_height)
		{
			throw PGL_EXCEPT_OutOfBound("y exceeded height");
		};

		return m_pixels.at(y * m_width + x);
	};

	inline
	const Colour_t&
	operator () (
		const Size_t& 	x,
		const Size_t& 	y) const
	{
		if(x >= m_width)
		{
			throw PGL_EXCEPT_OutOfBound("x Exceeded width");
		};

		if(y >= m_height)
		{
			throw PGL_EXCEPT_OutOfBound("y exceeded height");
		};

		return m_pixels.at(y * m_width + x);
	};


	/**
	 * \brief	This function sets the pixel (x, y) to the given colour
	 *
	 * \param x 	The x coordinate of the pixel
	 * \param y 	The y coordinate of the pixel
	 * \param c 	The colour the pixel should have
	 */
	inline
	pgl_bitmap_t&
	set(
		const Size_t& 		x,
		const Size_t& 		y,
		const Colour_t& 	c)
	{
		this->operator()(x, y) = c;

		return *this;
	};


	/**
	 * \brief	Return the hight of the picture
	 */
	inline
	Size_t
	GetWidth() const noexcept
	{
		return m_width;
	};

	/**
	 * \brief	Return the width of the picture
	 */
	inline
	Size_t
	GetHeight() const noexcept
	{
		return m_height;
	};


	/**
	 * \brief	Fill the picture with the colour fillCol
	 *
	 * The whole picture is filled with the colour.
	 * Everything is overwritten.
	 */
	void
	Fill(
		const Colour_t 		fillColour) noexcept;


	/**
	 * \brief	Save the picture to the file
	 *
	 * \param fileName 	The file name
	 */
	void
	Save(
		const char* fileName) const;

	inline
	void
	Save(
		const std::string& fileName) const
	{
		this->Save(fileName.c_str());
		return;
	};



	/*
	 * ===========================
	 * Private member function
	 */
private:
	internalStorage_t 		m_pixels;	//!< Internal representation of the picture
	Size_t 				m_width;	//!< Width of the picture
	Size_t 				m_height;	//!< Hight of the picture


	/*
	 * Internal function
	 */
	static void Write (std::ostream&, float);
	static void Write (std::ostream&, unsigned, int);
};








PGL_NS_END(pgl)



