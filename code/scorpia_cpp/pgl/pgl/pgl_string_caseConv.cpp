/**
 * \brief	This file contains the functions that changes the case of
 * 		 a string.
 *
 */

//PGL-Includes
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_string.hpp>


//Include BOOST
#include <boost/algorithm/string/case_conv.hpp>

//Include the std
#include <string>
#include <locale>

PGL_NS_START(pgl)

pgl_string_t
pgl_strToLower(
	const pgl_string_t& 	s,
	const std::locale& 	loc)
{
	std::string s_ret(s);				//Copy the imput argument
	::boost::algorithm::to_lower(s_ret, loc);	//Transform the case
	return s_ret;
}; //End: string to lower cases


pgl_string_t&
pgl_strToLower(
	pgl_string_t* const 	s_,
	const std::locale& 	loc)
{
	if(s_ == nullptr)
	{
		throw PGL_EXCEPT_NULL("The string passed to the function is the null pointer.");
	};

	pgl_string_t& s = *s_;			//Alias the argument
	::boost::algorithm::to_lower(s, loc);	//Make lower cases

	return s;				//Return the reference to the imput argument
}; //End converte string


PGL_NS_END(pgl)

