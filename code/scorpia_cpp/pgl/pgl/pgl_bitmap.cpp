/**
 * \brief	This is a simple bitmap class
 * \file 	pgl/pgl_bitmap.cpp
 *
 * This file contains an implementation of some function from the bitmap class
 *
 */

// PGL include
#include <pgl_core.hpp>
#include <pgl_assert.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_bitmap.hpp>

#include <pgl_graph_utility/pgl_graph_colour.hpp>

// STD include
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

PGL_NS_START(pgl)




pgl_bitmap_t::pgl_bitmap_t(
	const Size_t w,
	const Size_t h) :
  m_pixels(w * h),
  m_width(w),
  m_height(h)
{
	pgl_assert(m_width < 32767);
	pgl_assert(m_height < 32767);
};


void
pgl_bitmap_t::Fill(
		Colour_t color) noexcept
{
	std::fill(m_pixels.begin(), m_pixels.end(), color);
	return;
}

void
pgl_bitmap_t::Save(
	const char* filename) const
{
	assert(filename);
	std::ofstream file(filename, std::ios::binary);

	enum {WORD = 2, DWORD = 4, LONG = 4};
	const unsigned padding = (4 - m_width * 3 % 4) % 4;

	// BITMAPFILEHEADER
	Write (file, WORD, 0x4d42);	// bfType
	Write (file, DWORD, 14 + 40 + (m_width * 3 + padding) * m_height);	// bfSize
	Write (file, WORD, 0);		// bfReserved1
	Write (file, WORD, 0);		// bfReserved2
	Write (file, DWORD, 14 + 40);	// bfOffBits

	// BITMAPINFOHEADER
	Write (file, DWORD, 40);	// biSize
	Write (file, LONG, m_width);	// biWidth
	Write (file, LONG, -m_height);	// biHeight
	Write (file, WORD, 1);		// biPlanes
	Write (file, WORD, 24);		// biBitCount
	Write (file, DWORD, 0);		// biCompression
	Write (file, DWORD, 0);		// biSizeImage
	Write (file, LONG, 0);		// biXPelsPerMeter
	Write (file, LONG, 0);		// biYPelsPerMeter
	Write (file, DWORD, 0);		// biClrUsed
	Write (file, DWORD, 0);		// biClrImportant

	for(Size_t y = 0; y != m_height; ++y)
	{
		for(Size_t x = 0; x != m_width; ++x)
		{
			const Colour_t& color = m_pixels.at(y * m_width + x);
			Write(file, color.getBlueFraction() );
			Write(file, color.getGreenFraction());
			Write(file, color.getRedFraction()  );
		}

		for(Size_t x = 0; x != padding; ++x)
		{
			Write(file, 0);
		};
	}

	return;
};

void
pgl_bitmap_t::Write(
	std::ostream& os,
	float value)
{
	int intValue = 256 * (value < 0 ? 0 : value > 1 ? 1 : value);
	os.put(static_cast<unsigned char>(intValue < 256 ? intValue : 255));
	return;
}

void
pgl_bitmap_t::Write(
	std::ostream& os,
	unsigned length,
	int value)
{
	pgl_assert(length < 5);
	while (length--) os.put (value & 0xff), value >>= 8;
	return;
}








PGL_NS_END(pgl)



