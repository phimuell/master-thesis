#pragma once
/**
 * \file	pgl/pgl/pgl_timer.hpp
 * \brief 	Timers in pgl.
 *
 * This files includes all the file that providing timing functionality in pgl.
 */


#include <pgl_core.hpp>

/*
 * Including the single headers.
 */

#include "pgl_simple_timer.hpp"




