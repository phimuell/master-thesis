#pragma once
/**
 * \file	pgl/pgl/pgl_simple_pgl_simple_timer_t.hpp
 * \brief	A simple pgl_simple_timer_t.
 *
 * This class implements a simple pgl_simple_timer_t.
 * It was taken from the HPCSE16 course.
 * I do not know who wrote it.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>


// Include std



//Include sytsem
#include <sys/time.h>

PGL_NS_BEGIN(pgl)


class pgl_simple_timer_t
{
	/*
	 * =====================
	 * Constructor
	 */
public:
	/**
	 * \brief 	Default constructor
	 */
	inline
	pgl_simple_timer_t() noexcept
	{
		m_state = false;
		start_time.tv_sec  = 0;
		start_time.tv_usec = 0;
		stop_time.tv_sec   = 0;
		stop_time.tv_usec  = 0;
	}

	inline
	pgl_simple_timer_t(
		const pgl_simple_timer_t&) noexcept = default;

	inline
	pgl_simple_timer_t&
	operator= (
		const pgl_simple_timer_t&) noexcept = default;


	/*
	 * ==========================
	 * Functions
	 */

	/**
	 * \brief	Starts the timing.
	 *
	 * The pgl_simple_timer_t must not run in order to work
	 */
	inline
	void
	start() noexcept
	{
		pgl_assert(m_state == false);
		m_state = true;

		gettimeofday(&start_time, NULL);

		return;
	}

	/**
	 * \brief	Stops the timing.
	 *
	 * This requieres that the time is running.
	 * Aka, there was a preciding call to start.
	 */
	inline
	void
	stop() noexcept
	{
		pgl_assert(m_state == true);
		gettimeofday(&stop_time, NULL);
		m_state = false;

		return;
	};

	/**
	 * \brief	Get the time difference between start and stop in seconds.
	 *
	 * This must stopped.
	 */
	inline
	double
	get_timing() const noexcept
	{
		pgl_assert(m_state == false);
		return (stop_time.tv_sec - start_time.tv_sec) + (stop_time.tv_usec - start_time.tv_usec)*1e-6;
	}

	/**
	 * \brief	Return true if the clock is running
	 */
	inline
	bool
	isRunning() const noexcept
	{
		return m_state == true ? true : false;
	};


	/**
	 * \brief	Retruns true if the clock is not running
	 */
	inline
	bool
	isStoped() const noexcept
	{
		return m_state == false ? true : false;
	};

private:
	struct timeval start_time, stop_time;
	bool 	m_state;	//!< A true indicating, that the time is running
};



PGL_NS_END(pgl)


//for compability
#ifdef _ALLOW_GUCKY_HACK

using pgl::pgl_simple_timer_t;

#endif


