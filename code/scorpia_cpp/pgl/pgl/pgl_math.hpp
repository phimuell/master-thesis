#pragma once
/**
 * \file	code/pgl/pgl/pgl_math.hpp
 * \brief	This file provides mathe functions
 *
 * This file provides the function form cmath header, in the std namespace.
 * Ans some more usefull functions.
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <cmath>
#include <type_traits>
#include <limits>




PGL_NS_START(pgl)



template<
	typename 	Arg_t
>
inline
typename std::enable_if<std::is_floating_point<Arg_t>::value, bool>::type
isValidFloat(
	const Arg_t& 	arg);


PGL_NS_START(internal)
/*
 * =========================
 * Internal definitions
 */

/**
 * \brief	This function converts a value that is expressed
 * 		 in degrees into radiants.
 *
 * This is the implementation does perform the checks if
 * its argument is NAN.
 *
 * \param  deg		The degree value.
 *
 * \tparam  T 		The type of the argument.
 */
template<
	typename 	T
>
inline
T
toRad(
	const T		deg)
{
	static_assert(std::is_floating_point<T>::value, "Only supports floating point operations.");

	//Perform the check
	if(isValidFloat(deg) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed NAN to the transform function.");
	};

	//perform the transformation.
	return (deg * (T(M_PI) / T(180.0)));
}; //End: toDeg


/**
 * \brief	This function converts a value that is expressed
 * 		 in radiants into degrees.
 *
 * This is the implementation does perform the checks if
 * its argument is NAN.
 *
 * \param  rad		The value of the angle in radiant.
 *
 * \tparam  T 		The type of the argument.
 */
template<
	typename 	T
>
inline
T
toDeg(
	const T		rad)
{
	static_assert(std::is_floating_point<T>::value, "Only supports floating point operations.");

	//Perform the check
	if(isValidFloat(rad) == false)
	{
		throw PGL_EXCEPT_InvArg("Passed NAN to the transform function.");
	};

	//perform the transformation.
	return (rad * (T(180.0) / T(M_PI)));
}; //End: toDeg


PGL_NS_END(internal)




/*
 * =======================
 * Actuall math functions
 */

/**
 * \brief	This function calculates the logaritme, with respect to another basis
 *
 * \param x	We wahnt logarithm of it
 * \param base  to this basis
 */
template<typename T>
inline
T
Log_to(
	const T& 	x,
	const T&	base)
{
	return (std::log(x) / std::log(base));
};


/**
 * \brief	This function returns true if arg is a valid float number.
 *
 * A valid float number is a number that is not infinite nor nan.
 * This means it can be subnormal, normal or zero.
 *
 * \param  arg		The number to check.
 */
template<
	typename 	Arg_t
>
inline
typename std::enable_if<std::is_floating_point<Arg_t>::value, bool>::type
isValidFloat(
	const Arg_t& 	arg)
{
	return ::std::isfinite(arg);
};


/**
 * \brief	This is an average function for two numbers.
 *
 * This function will calculate the average of two numbers.
 *
 * \param  arg1		First number.
 * \param  arg2		Second number.
 *
 * \tparam  Arg_t 	The type of the arguments
 */
template<
	typename 	Arg_t
>
inline
typename std::enable_if<std::is_floating_point<Arg_t>::value, Arg_t>::type
avg(
	const Arg_t&	arg1,
	const Arg_t&	arg2)
{
	pgl_assert(isValidFloat(arg1), isValidFloat(arg2));
	return (Arg_t(0.5) * (arg1 + arg2));
}; //End Average function for two numbers


/**
 * \brief	This function tests if a passed value is
 * 		 below a certain treshold.
 *
 * \param  x 	Value to test.
 * \param  sf	Safty factor.
 *
 * \tparam  T 	The float time
 */
template<
	class	T
>
bool
isVerySmall(
	const T& 	x,
	const uInt_t	sf = 10)
 noexcept
{
	using std::abs;
	return (abs(x) < sf * std::numeric_limits<T>::epsilon())
		? true
		: false;
};


/**
 * \brief	This function tests if a passed value is
 * 		 below a certain treshold.
 *
 * This function is a specialization for double.
 *
 * \param  x 	Value to test.
 * \param  sf	Safty factor.
 */
constexpr
bool
isVerySmall(
	const Numeric_t& 	x,
	const uInt_t		sf = 10)
 noexcept
{
	return (((x < 0.0) ? (-x) : (x)) < sf * std::numeric_limits<Numeric_t>::epsilon())
		? true
		: false;
};


/**
 * \brief	This function tests if two values are approximatly the same.
 *
 * It basically tests if their difference is very small.
 *
 * \param  x1	The first number.
 * \param  x2	The second number.
 * \param  sf	Safty factor.
 *
 * \tparam  T1	Type of first argument.
 * \tparam  T2	Type of second argument.
 */
template<
	class 	T1,
	class 	T2
>
bool
approxTheSame(
	const T1& 	x1,
	const T2& 	x2,
	const uInt_t 	sf  = 10)
 noexcept
{
	return isVerySmall<decltype(x1 - x2)>(x1 - x2, sf);
};


/**
 * \brief	This function tests if two values are approximatly the same.
 *
 * Specialization for double.
 *
 *
 * \param  x1	The first number.
 * \param  x2	The second number.
 * \param  sf	Safty factor.
 *
 */
constexpr
bool
approxTheSame(
	const Numeric_t& 	x1,
	const Numeric_t& 	x2,
	const uInt_t 		sf  = 10)
 noexcept
{
	return isVerySmall(x1 - x2, sf);
};


/**
 * \brief	This function is a safe version of <= for floatingpoints.
 *
 * It compares if lhs is smaller or equal rhs, if this is not the case
 * then it will be checked if the two are the approciamtely the same.
 *
 * \param  x1	The first number.
 * \param  x2	The second number.
 * \param  sf	Safty factor.
 *
 * \tparam  T 		The numeric type.
 *
 */
template<
	typename 	T
>
bool
lessOrEqualFP(
	const T& 		lhs,
	const T& 		rhs,
	const uInt_t 		sf  = 10)
 noexcept
{
	return ((lhs <= rhs) || approxTheSame(lhs, rhs, sf));
};


/**
 * \brief	This is the minmod function.
 *
 * If the sign of both differ, then it returns zero.
 * If a is smaller, in modulus, than b it will return
 * a.
 *
 *
 * \param  a	first argument.
 * \param  b 	second argument.
 */
template<
	class 		T
>
::std::enable_if_t<::std::is_floating_point<T>::value, T>
minmod(
	const T& 		a,
	const T&		b)
{
	using std::signbit;
	using std::fmax;
	using std::fmin;
	pgl_assert(::pgl::isValidFloat(a), ::pgl::isValidFloat(b));
	const bool aNeg = signbit(a);	//get the sign of both numbers, true if negative
	const bool bNeg = signbit(b);

	/* Both have a different sign
	 * so return 0 */
	if(aNeg != bNeg)
	{
		return T(0.0);
	};

	if(aNeg)
	{
		//The two numbers are negative so return
		//the largest, because it is the less negative
		//and thus the smalles in modulus
		return fmax(a, b);
	};

	//The two numbers are both positive, so we return the
	//smallest one, because modulus has no effect
	return fmin(a, b);
}; //End: minmod


/**
 * \brief	This function converts a value that is expressed
 * 		 in degrees into radiants.
 *
 * If its argument is NAN, then an error is generated.
 * If the argument is not a floting point type, it will be
 * casted to the the Numeric type.
 * See also the comments at the toDeg function.
 *
 * \param  deg		The degree value.
 *
 * \tparam  T 		The type of the argument.
 */
template<
	typename 	T
>
inline
std::conditional_t<std::is_floating_point<T>::value, T, Numeric_t>
toRad(
	const T& 	deg)
{
	//Conmpute the type that is used
	using UsedType_t = std::conditional_t<std::is_floating_point<T>::value, T, Numeric_t>;	//this is the type that is actually used.

	//Call the implementation.
	return internal::toRad<UsedType_t>(deg);
};//End: toRad


/**
 * \brief	This function transforms an angle that is interpeted
 * 		 in radiant to degrees.
 *
 * This function is somehow the inverse operation of the toRad function.
 * Also its specifications are the same, meaning it will use Numeric_t
 * if a non float is passed.
 * Concerning the fact that this operation is the inverse operation
 * of the toRAd function, this is guaranteed conceptionally, but
 * due to floting point errors not guaranteed numerically.
 *
 * \param  rad		The angle expressed in radients
 *
 * \tparam  T 		The type of the argument.
 */
template<
	typename 	T
>
inline
std::conditional_t<std::is_floating_point<T>::value, T, Numeric_t>
toDeg(
	const T& 	rad)
{
	//Conmpute the type that is used
	using UsedType_t = std::conditional_t<std::is_floating_point<T>::value, T, Numeric_t>;	//this is the type that is actually used.

	//Call the implementation.
	return internal::toDeg<UsedType_t>(rad);
};//End: toRad


PGL_NS_END(pgl)

