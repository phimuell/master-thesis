#pragma once
/**
 * \file	pgl/pgl_algorithm.hpp
 * \brief 	This files contains some algorithms, that I deem usefull, but are missing in the std.
 *
 * Beside defining some algorithm, this file also includes the algorithm header from the standard.
 */

//PGL-Includes
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

//Include the std
#include <algorithm>
#include <iterator>

PGL_NS_START(pgl)


/**
 * \brief this 		function is like find, but is able to find the n-th occurance of something.
 *
 * if n is one, this function is equivalent, to std::any_of.
 *
 * \param start		Iterator to the start
 * \param ende		Iterator to the past the end position
 * \param Pred		The unary predicate that sould be applied
 * \param n		The occurance counter starts with 1.
 *
 * \pre   n is smaller than '2^{64} - 2'
 *
 * \throw if Pred throws, but also if n is zero
 *
 * \note 	In earlier versions, the counting started with zero.
 * 		  This was a stupid missdesigne. And this was changed, now it starts with 1.
 * 		  If zero is passed an assert is generated.
 */
template<class ForwardIt, class Pred_T>
ForwardIt
find_if_n(
	ForwardIt 		start,
	const ForwardIt&	ende,
	Pred_T 			Pred,
	Size_t			n)
{
	if(n == 0)
	{
		throw PGL_EXCEPT_InvArg("n shall not be zero.");
	};


	while(start != ende)
	{
		//The predicate returns true, so we have found one
		if(Pred(*start))
		{
			--n;
		};

		//If we have zero, then we have found the one requested
		if(n == 0)
		{
			break;
		};

		++start;
	}; //End whileloop

	return start;
};


/**
 * \brief	This function finds the last occurance where a predicate returned true.
 *
 * This function checks each element in the range and will return the last position
 * where the predicate has returned true. If no position was found that satisfied
 * the predicate, then the end iterator is generated.
 *
 * Note that this function currently is not so efficient, but it also accepts
 * forward iteratiors. This is something to change for the future, such that
 * also higher level iterators will be accepted.
 *
 * \param  start	The beginning of the range.
 * \param  ende		The past the end interpor of the sequence.
 * \param  pred		The predicate that is evaluated.
 *
 * \tparam  FwdIt_t 	The type of the forward iterator.
 * \tparam  Pred_t 	The predictae that is used.
 *
 * The predicate is not allowed to change the value of the elemnt that is passed to it.
 * It is recomended that it has the signature bool(*pred)(const typename std::iterator_traits::value_type&).
 */
template<
	typename 	FwdIt_t,
	typename 	Pred_t
>
FwdIt_t
find_last_if(
	FwdIt_t 	start,
	const FwdIt_t   ende,
	Pred_t 		pred)
{
	FwdIt_t ret = ende;	//Such the last one will always be retuurned.

	while(start != ende)
	{
		//Call the predicate
		if(pred(*start) == true)
		{
			//We found something, update the return value.
			ret = start;
		};

		//Increment the iterator
		++start;
	}; //End while:

	//Return the found position
	return ret;
}; //End. find last of




/**
 * \brief	This function tests if the sorted range is unique.
 *
 * The range must be sorted.
 * Will check if all consecutive elements are different.
 *
 * \param start 	Iterator to the begining
 * \param ende 		Past the end iterator
 * \param Pred	 	Used for equality.
 *
 * \tparam ForwardIt 	The iterator must be a forward iterator
 * \tparam Pred_T 	Must return true if two arguments are equal, an application of them dhall not alter thevalue there
 */
template<class ForwardIt, class Pred_T >
bool
is_unique(
	ForwardIt 		start,
	ForwardIt 		ende,
	Pred_T 			Pred) noexcept(noexcept(Pred(*start, *start)))
{

	/*
	 * We now use the standard.
	 */

	return (std::adjacent_find(std::move(start), ende, std::move(Pred)) == ende) ? true : false;
};


template<class ForwardIt>
bool
is_unique(
	ForwardIt 		start,
	ForwardIt 		ende) noexcept(noexcept(::pgl::is_unique<ForwardIt, std::equal_to<typename std::iterator_traits<ForwardIt>::value_type> >(start, ende, std::equal_to<typename std::iterator_traits<ForwardIt>::value_type>())))
{
	return ::pgl::is_unique(std::move(start), std::move(ende), std::equal_to<typename std::iterator_traits<ForwardIt>::value_type>() );
};


/**
 * \brief	This is the return type for the is unique and sorted function.
 * \enum 	retValueUniqueSorted_e
 *
 * Please notice that the return value only reflects the reason why the function has returned.
 * This means that non unique does not implies that the range is sorted.
 * It only say that the range was sorted until two elements compared equal where encountered.
 * the only value that has a real meaning is, sorted and unique, since this means that all elements where compared less
 * and not eaual.
 */
enum class retValueUniqueSorted_e
{
	nonUnique,
	notSorted,
	sortedAndUnique
};



/**
 * \brief 	This function is used to check in one go if the range is unique and sorted.
 *
 * The stl provides some function that does something similar, but it is not so efficient.
 * So this function is used to do it.
 * It iterates through the range and checks if two consecutive elements are smaller and not equal.
 * For efficient reasons the less and the equality predicate can be specified seperatly.
 * The range must be at least a forward iterter.
 * If the size of the range is one, then true is allways returned.
 *
 * For a description of the return value, please see the enum retValueUniqueSorted_e.
 *
 * \param 	start		The beginning of the range
 * \param 	ende 		The end of the range
 * \param 	LessPred	The binary function, that is used for the less comparision
 * \param 	EqualPred	The binary functuion that is used for the equal predicate
 *
 */
template<
	class ForwardIt,
	class LessPred_t,
	class EqualPred_t>
retValueUniqueSorted_e
is_sorted_unique(
	ForwardIt 	start,
	ForwardIt 	ende,
	LessPred_t 	LessPred,
	EqualPred_t 	EqualPred)
{
	ForwardIt head 	= start;	//This is the head,
	++head;

	//Test if the range has size 1
	if(head == ende)
	{
		return retValueUniqueSorted_e::sortedAndUnique;
	};

	ForwardIt prevHead = start;	//This is the element imediatly after the head

	//Iterating through the range
	while(head != ende)
	{
		//Test if less
		if(LessPred(*prevHead, *head) == false)
		{
			//THey are not lesser
			return retValueUniqueSorted_e::notSorted;
		};

		//Test if not the same
		if(EqualPred(*prevHead, *head) == true)
		{
			//They are equal
			return retValueUniqueSorted_e::nonUnique;
		};

		//Increment both
		++prevHead;
		++head;
	}; //End while

	//We reached the end, so, they must be sorted and unique
	return retValueUniqueSorted_e::sortedAndUnique;
};


/**
 * \brief	This is a specialization of the unique and sorted function.
 *
 * They use the std::less and std::equal_to predicate to perform the test of uniqueness and lessness.
 *
 * \param	start 		Beginning of the range to check
 * \param	ende 		End of the range
 */
template<
	class ForwardIt>
retValueUniqueSorted_e
is_sorted_unique(
	ForwardIt 	start,
	ForwardIt 	ende)
{
	using value_type = decltype(*start);
	return is_sorted_unique<ForwardIt, std::less<value_type>, std::equal_to<value_type> >(std::move(start), std::move(ende), std::less<value_type>(), std::equal_to<value_type>() );
};


/**
 * \brief	This function is also a specialization of the unique and sorted function, but the user can specifiy, its own Less function.
 *
 * This function only needs a less predicate.
 * The equal predicate is modelated by "!comp(a, b) && !comp(b, a)" as in the std::map.
 */
template<
	class ForwardIt,
	class LessPred_t>
retValueUniqueSorted_e
is_sorted_unique(
	ForwardIt 	start,
	ForwardIt 	ende,
	LessPred_t 	LessPred)
{
	auto EqualPred = [LessPred](const auto& a, const auto& b){ return (!LessPred(a, b) && !LessPred(b, a));};
	return is_sorted_unique<ForwardIt, LessPred_t, decltype(EqualPred) >(std::move(start), std::move(ende), LessPred, EqualPred );
};



/**
 * \brief	This function tests if a given value is inside the range.
 *
 * This function cals the find function and will then test if the itertaor
 * that is found is the end iterator. If this is not the case, true id returned.
 *
 * \param  start	The begining of the range.
 * \param  last		Past the end iterator.
 * \param  val 		The value that is searched for.
 *
 * \tparam  forwardIt		Iterator type.
 * \tparam  T 			Type we are searchin.
 */
template<
	class 	InputIt,
	class 	T
>
bool
contains(
	InputIt 	start,
	InputIt 	last,
	const T& 	val)
{
	using std::find;

	if(find(start, last, val) != last)
	{
		return true;
	};

	return false;
}; //End contain





PGL_NS_END(pgl)

