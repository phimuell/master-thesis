#pragma once
/**
 * \brief	This file contains some string processing functions.
 *
 * Tjis files declares an alias of std::string as pgl::pgl_string_t.
 * It also provides functions that helps dealing with problems.
 * They are mostly wrapper arround boost functions.
 *
 */

//PGL-Includes
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>

//Include the std
#include <string>
#include <locale>

PGL_NS_START(pgl)

/**
 * \brief	Alias of std::string.
 * \typedef 	pgl_string_t
 *
 * This is an alias of the standard string.
 * Someday it will be replaced by a real class.
 */
using pgl_string_t = ::std::string;


/**
 * \brief	This function converts the string into lower cases.
 * 		 A copy is returned.
 *
 * This function performs a copy of its input argument and returns a copy
 * of it but all charachters are lower case.
 *
 * \param  s	The string that should be converted.
 * \param  loc 	The locale object, the default one is used if not specified.
 */
pgl_string_t
pgl_strToLower(
	const pgl_string_t& 	s,
	const std::locale& 	loc = std::locale());


/**
 * \brief	This function converts the string to lower cases.
 * 		 Its argument is modified.
 *
 * This function converts the string s into all lower cases.
 * The imput argument s is modified.
 * A reference to s is returned.
 *
 * \param  s 		String to modify to lower case.
 * \param  loc		The locale object.
 */
pgl_string_t&
pgl_strToLower(
	pgl_string_t* const 	s,
	const std::locale& 	loc = std::locale());


/**
 * \brief	This function performs a pretty printing output.
 *
 * It will groop the number into groups of n digit and output it.
 * It will also limit the number of digits after the decimal point
 * to a specicied number. Currently it is onl working for doubles.
 *
 * \param a 	The number to print.
 * \param nK	The number of digits after the decimal point.
 * \param nG 	The number of digits that should be grouped; defaulted 3.
 */
pgl_string_t
pgl_prettyString(
	const Numeric_t 	a,
	const Size_t 		nK,
	const Size_t 		nG = 3);







PGL_NS_END(pgl)

