#pragma once
#ifndef PGL_PGL_TIME_FUNCTIONS
#define PGL_PGL_TIME_FUNCTIONS
/**
 * \brief	This header defines some of function that makes it easy to work with times.
 *
 * In the C-library the time object are shared, at least some of them.
 * PGL decouples this.
 * It protects the access to such objects with a lock.
 * This lock is only used in PGL.
 *
 * The lock is defined in the ppgl_mutex header.
 * All these function access the global lock directly or indirectly.
 *
 * If those function have an error they trigger a time exception.
 *
 */

//Inlcue PGL
#include <pgl_core.hpp>

#include <pgl/pgl_string.hpp>


//Include Standard
#include <ctime>
#include <string>



PGL_NS_BEGIN(pgl)


/**
 * \brief	This is a callender object.
 * \typedef 	pgl_calendarTime_t
 *
 * This is basically a typedef of the tm object from the c library
 */
using pgl_calendarTime_t = std::tm;


/**
 * \brief	This is a typedef of the std::time_t object.
 * \typedef 	pgl_time_t
 *
 * This is an aretmetic type that is used to store the time on the system.
 * This function represents the time since epoc
 */
using pgl_time_t = std::time_t;

/**
 * \brief	This is a global variable that indicates an invalid time since epoc
 */
const static pgl_time_t PGL_INVALID_CTIME = (std::time_t)(-1);


/**
 * \brief	This function returns the current time.
 *
 * This function is equivalent to the c function 'std::time()', but inproves semantic.
 * It returns the current time, but it also takes an optional argument.
 * This is a pointer to a time object.
 * If it is the nullpointer, nothing is done.
 * If not then the current time is also stored there.
 *
 * If the function failed then an exception is generated.
 *
 * \return 	The urrent time
 *
 */
pgl_time_t
pgl_getTime(
	pgl_time_t* const 	Arg = nullptr) noexcept(false);



/**
 * \brief	This function returns a struct tm object. The convention is UTC
 *
 * The c library only gives a pointer back, this object is shared and can be overwritten any time.
 * PGL gioves back the real thing, meaning it copies the obect.
 *
 * This is function returns the calendar in UTC.
 * To get the local time version use the pgl_getCalendarTimeL function.
 *
 * \param currTime 	This time is copied into the strict
 *
 * \return 		A non shared callendar time
 * \note 		This function aquires the lock
 */
pgl_calendarTime_t
pgl_getCalendarTime(
	const pgl_time_t) noexcept(false);

/**
 * \brief	This function returns a struct tm object. For that Local time is used
 *
 * The c library only gives a pointer back, this object is shared and can be overwritten any time.
 * PGL gioves back the real thing, meaning it copies the obect.
 *
 * This is function returns the calendar in local time
 *
 * \param currTime 	This time is copied into the strict
 *
 * \return 		A non shared callendar time
 * \note 		This function aquires the lock
 */
pgl_calendarTime_t
pgl_getCalendarTimeL(
	const pgl_time_t) noexcept(false);

/**
 * \brief	This function returns a textual representation of the passed time.
 *
 * This function internaly calls the std::ctime function.
 * It aquires a lock.
 * The ctime function does appand a new line character to the string.
 * This function does not!
 *
 * \param zeit 		The time sice epoch
 */
std::string
pgl_timeToString(
	const pgl_time_t&	zeit) noexcept(false);


/**
 * \brief	This function returns the pgl_calendarTime_t object, that is not shared.
 *
 * The time since epoc is generated from a call to pgl_getTime.
 *
 * \return 		A non shared pgl_calendarTime_t object
 * \note 		This function aquieres the lock two times indirectly
 */
pgl_calendarTime_t
pgl_getCalendarTime() noexcept(false);


/**
 * \brief	This function converts a time span, that is given in seconds
 * 		 in an appropriate format.
 *
 * This function converts an uncomprehensible amount of seconds into something
 * that one can graps, like 2139Y 3M 4D.
 * The format that is returned demends on the amount that is passed.
 * The function will try to estimate the best possible choices.
 * However some parts may be omitted. This function assumes hughes values.
 * So it will only output miliseconds.
 *
 * Note this function only accepts positive values.
 *
 * \param  spannSec	The amount of second that should be pretty printed.
 */
pgl_string_t
pgl_prettyPrintTime(
	const Numeric_t 	spannSec);





PGL_NS_END(pgl)




#endif
