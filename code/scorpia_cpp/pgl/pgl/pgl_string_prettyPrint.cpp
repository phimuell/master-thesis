/**
 * \brief	This functions contains the code to pretty print the
 * 		 numbers.
 */

//PGL-Includes
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_assert.hpp>

#include <pgl/pgl_string.hpp>
#include <pgl/pgl_math.hpp>


//Include BOOST
#include <boost/algorithm/string/case_conv.hpp>

//Include the std
#include <string>
#include <locale>

PGL_NS_START(pgl)

pgl_string_t
pgl_prettyString(
	const Numeric_t 	a,
	const Size_t 		nK,
	const Size_t 		nG)
{
	if(std::isnan(a) == true)
	{
		pgl_string_t("NAN");
	};
	if(std::isinf(a) == true)
	{
		return pgl_string_t((a < 0.0) ? "-Inf" : "Inf");
	};

	//This is the mask for grouping
	const sSize_t gMask = std::pow(10, nG);

	//Store if a is negative
	const bool isNegative = (a < 0.0);

	//First make an int out of the imput.
	const Size_t a_full = sSize_t(std::abs(a));

	//this is the string we will return
	pgl_string_t outString;

	//This is our working value
	Size_t workA = a_full;

	while(workA > 0)
	{
		//This is the current part that we want to add
		const Size_t currAdd = workA % gMask;

		//This is the next workA that we handle
		const Size_t nextWorkA = workA / gMask;

		//Transform the cirrent addings to a string
		pgl_string_t currAddStr = std::to_string(currAdd);

		//Padd the number with zeros if needed
		//and also handle the sepration token
		if(nextWorkA > 0)
		{
			pgl_assert(currAddStr.size() <= nG);
			while(currAddStr.size() != nG)
			{
				//Padding at the left
				currAddStr = '0' + currAddStr;
			}; //End while: padding

			// Handle the seperation token
			currAddStr = '\'' + currAddStr;
		}; //End if: padding is needed

		//Add the new part
		outString = currAddStr + outString;

		//Propagate the new work A
		workA = nextWorkA;
	}; //End while

	//Handle the case that we have zero
	if(outString.size() == 0)
	{
		outString = '0';
	}; //End if: handle zero integer part


	//Now handle the fractional part
	if(nK > 0)
	{
		//Get the fractional part as an int
		const Size_t fracPart = (std::abs(a) - a_full) * std::pow(10, nK);

		//Make an string out of it and add it to output
		outString = outString + "." + std::to_string(fracPart);
	}; //End if: handle fractional part

	/*
	 * We have to consider if we need a minus
	 */
	if(isNegative == true)
	{
		outString = "-" + outString;
	}; //End if: negative

	//return the string
	return outString;
}; //End: pretty printing




PGL_NS_END(pgl)

