#pragma once
/**
 * \file	code/pgl/pgl/pgl_allocator.hpp
 * \brief	A collection of the allocators that are used in pgl.
 *
 * This file mainly includes the allocators that are provided within the boos container library.
 * It does include all of them and makes an aliace of them.
 *
 * pgl provedes the macro PGL_USE_EXTENDED_ALLOCATOR.
 * If this macro is defined, then the allocators are defined.
 * If this macro is not defined, all allocators are equivalent to the stlAllocator.
 *
 * Only the newAllocator is provided.
 */

//INclude pgl
#include <pgl_core.hpp>


#if defined(PGL_USE_EXTENDED_ALLOCATOR) && (PGL_USE_EXTENDED_ALLOCATOR != 0)
//Include boost
#	include <boost/container/allocator.hpp>
#	include <boost/container/adaptive_pool.hpp>
#	include <boost/container/scoped_allocator_fwd.hpp>
#	include <boost/container/node_allocator.hpp>
#endif

#include <boost/container/allocator_traits.hpp>
#include <boost/container/new_allocator.hpp>

//Include std
#include <memory>


PGL_NS_START(pgl)


/**
 * \class 	pgl_stlAllocator_t
 * \brief	This is the allocator provided by the standard.
 */
template<typename T>
using pgl_stlAllocator_t = ::std::allocator<T>;

/**
 * \class	stl_allocator
 * \brief	This is a typedef of pgl_stlAllocator_t
 */
template<typename T>
using stl_allocator = ::pgl::pgl_stlAllocator_t<T>;


/**
 * \class 	pgl_newAllocator_t
 * \brief	This is an allocator, that uses the ::operator new, to allocate memory.
 *
 * This allocator is taken form the boost library.
 * It does not offer the full functionality of the allocator, but is the default inside, the boost conatiner lib.
 * Not in pgl.
 */
template<typename T>
using pgl_newAllocator_t = ::boost::container::new_allocator<T>;


/**
 * \class	pgl_allocator_t
 * \brief	The allocator used inside pgl.
 *
 * This is the allocator that is used inside pgl.
 * It is taken from the boost library. Ther it is called simply allocator.
 * The allocator is configurable trough its template parameters.
 *
 * \tparam T		The type that should be allocated
 * \tparam Version 	The version that should be used, can be 1 or 2
 * \tparam Mask 	This mask can disable certain features of the allocator.
 *
 *
 * If Version is 1, then the allocator conformes to the functions that are required by the standart.
 * If it is 2, then it provides some more functionality, see the boost documentation for more (if you find a comprehensive
 * documentation, please drop me a mail.)
 * The default is set to 2.
 * If the mask, one can deactivate some feature, but I ave no clue how this is realy done.
 */
template<typename T,
	unsigned Version = 2,
	unsigned Mask = 0>
#if defined(PGL_USE_EXTENDED_ALLOCATOR) && (PGL_USE_EXTENDED_ALLOCATOR != 0)
using pgl_allocator_t = ::boost::container::allocator<T, Version, Mask>;
#else
using pgl_allocator_t = ::pgl::pgl_stlAllocator_t<T>;
#endif

/**
 * \class	allocator
 * \brief	This is only a typedef of pgl_allocator_t
 */
template<typename T,
	unsigned Version = 2,
	unsigned Mask = 0>
#if defined(PGL_USE_EXTENDED_ALLOCATOR) && (PGL_USE_EXTENDED_ALLOCATOR != 0)
using allocator = ::pgl::pgl_allocator_t<T, Version, Mask>;
#else
using allocator = ::pgl::pgl_stlAllocator_t<T>;
#endif


/**
 * \class 	pgl_nodeAllocator_t
 * \brief	This is an allocator, that is optimized for nodes.
 *
 * This allocator is taken form the boost container library.
 * There it is called 'node_allocator'.
 * It emulates a singeltone.
 * It maintains a pool between all allocates, of the same type.
 * It does its allocation once if it run out of free nodes.
 *
 * There are severall parameters used
 * \tparam T			The type we allocate
 * \tparam Version		The version that should be used
 * \tapram NodesPerBlock 	Determmes how many nodes, should be allocated, if the allocater runs out of free nodes.
 *
 * If the 'Version' is 1, then all functions are standard.
 * If 'Version' is 2, more advance functions are suported.
 */
template<typename T,
	::std::size_t 	NodesPerBlock,
	unsigned 	Version = 2>
#if defined(PGL_USE_EXTENDED_ALLOCATOR) && (PGL_USE_EXTENDED_ALLOCATOR != 0)
using pgl_nodeAllocator_t = ::boost::container::node_allocator<T, NodesPerBlock, Version>;
#else
using pgl_nodeAllocator_t = ::pgl::pgl_stlAllocator_t<T>;
#endif


/**
 * \class 	pgl_adaptiveAllocator_t
 * \brief	An adaptive allocator for nodes.
 *
 * It is also taken form the boost library.
 * The documentation stats that:
 * An STL node allocator that uses a modified DLMalloc as memory source.
 *
 * This node allocator shares a segregated storage (singelton) between all instances of adaptive_pool with equal sizeof(T).
 * NodesPerBlock is the number of nodes allocated at once when the allocator needs runs out of nodes.
 * MaxFreeBlocks is the maximum number of totally free blocks that the adaptive node pool will hold.
 * The rest of the totally free blocks will be deallocated to the memory manager.
 * OverheadPercent is the (approximated) maximum size overhead (1-20%) of the allocator: (memory usable for nodes / total memory allocated from the memory allocator)
 * The allocator, also supports a version.
 *
 * If version is 1, then the allocator only offers the functionalitoes required by the standard,
 * if version os 2, then more features are enabled.
 */
template<typename T,
	std::size_t NodesPerBlock,
        std::size_t MaxFreeBlocks,
        std::size_t OverheadPercent,
        unsigned 	Version = 2>
#if defined(PGL_USE_EXTENDED_ALLOCATOR) && (PGL_USE_EXTENDED_ALLOCATOR != 0)
using pgl_adpAllocator_t = ::boost::container::adaptive_pool<T, NodesPerBlock, MaxFreeBlocks, OverheadPercent, Version>;
#else
using pgl_adpAllocator_t = ::pgl::pgl_stlAllocator_t<T>;
#endif



/**
 * \class 	pgl_allocator_traits
 * \brief	Offer traits for the allocators.
 *
 * This class also comes form boost.
 */
template<typename Allocator>
using pgl_allocator_traits = ::boost::container::allocator_traits<Allocator>;





PGL_NS_END(pgl)




