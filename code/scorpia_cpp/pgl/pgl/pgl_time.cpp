/**
 * \brief	This header defines some of function that makes it easy to work with times.
 *
 * In the C-library the time object are shared, at least some of them.
 * PGL decouples this.
 * It protects the access to such objects with a lock.
 * This lock is only used in PGL.
 *
 * The lock is defined in the ppgl_mutex header.
 * All these function access the global lock directly or indirectly.
 *
 * If those function have an error they trigger a time exception.
 *
 */

//Inlcue PGL
#include <pgl_core.hpp>
#include <pgl_exception.hpp>
#include <pgl_assert.hpp>

#include <pgl_parallel/ppgl_mutex.hpp>
#include <pgl/pgl_time.hpp>
#include <pgl/pgl_string.hpp>


//Include Standard
#include <ctime>
#include <cerrno>
#include <chrono>



PGL_NS_BEGIN(pgl)

// This is the definition of the lock
using TimeLock_t = ::pgl::parallel::ppgl_uniqueLock_t<::pgl::parallel::ppgl_mutex_t>;




/*
 * \brief	This function returns the current time.
 *
 * This function is equivalent to the c function 'std::time()', but inproves semantic.
 * It returns the current time, but it also takes an optional argument.
 * This is a pointer to a time object.
 * If it is the nullpointer, nothing is done.
 * If not then the current time is also stored there.
 *
 * If the function failed then an exception is generated.
 *
 * \return 	The urrent time
 */
pgl_time_t
pgl_getTime(
	pgl_time_t* const 	Arg) noexcept(false)
{
	//TimeLock_t lock(::pgl::parallel::globalLock::CTIME_LOCK);

	//C all the curent time protected by a lock
	pgl_time_t currTime = std::time(Arg == nullptr ? nullptr : Arg);

	if(currTime == PGL_INVALID_CTIME)
	{
		throw PGL_EXCEPT_TIME("Falied to get the Time form the system.");
	};

	return currTime;
};


std::string
pgl_timeToString(
	const pgl_time_t&	zeit) noexcept(false)
{
	if(zeit == PGL_INVALID_CTIME)
	{
		throw PGL_EXCEPT_TIME("The current time passed to the calendar function was invalid.");
	};

	std::string Res;
	{
		//Get the lock
		TimeLock_t Lock(::pgl::parallel::globalLock::CTIME_LOCK);

		Res = ::std::ctime(&zeit);
	}; //ENd lock scope

	//THe string seams to contain a newline
	//And this I do not like
	Res.pop_back();

	return Res;
};


pgl_calendarTime_t
pgl_getCalendarTime(
	const pgl_time_t currTime) noexcept(false)
{
	if(currTime == PGL_INVALID_CTIME)
	{
		throw PGL_EXCEPT_TIME("The current time passed to the calendar function was invalid.");
	};

	//This is the return value
	pgl_calendarTime_t calTime;

	//Get the date
	{
		struct tm* tmpCal = nullptr;
		TimeLock_t Lock(::pgl::parallel::globalLock::CTIME_LOCK);

		tmpCal = std::gmtime(&currTime);

		if(tmpCal == nullptr)
		{
			throw PGL_EXCEPT_TIME("std::gmtime returned a nullpointer.");
		};

		//According to cppref
		//http://en.cppreference.com/w/cpp/chrono/c/gmtime
		if(errno == EOVERFLOW)
		{
			throw PGL_EXCEPT_TIME("std::gmtime sets errno.");
		};

		//Copy the result
		calTime = *tmpCal;
	}; //End lock


	//Retrun the value
	return calTime;
};



pgl_calendarTime_t
pgl_getCalendarTimeL(
	const pgl_time_t currTime) noexcept(false)
{
	if(currTime == PGL_INVALID_CTIME)
	{
		throw PGL_EXCEPT_TIME("The current time passed to the calendar function was invalid.");
	};

	//This is the return value
	pgl_calendarTime_t calTime;

	//Get the date
	{
		struct tm* tmpCal = nullptr;
		TimeLock_t Lock(::pgl::parallel::globalLock::CTIME_LOCK);

		tmpCal = std::localtime(&currTime);

		if(tmpCal == nullptr)
		{
			throw PGL_EXCEPT_TIME("std::localtime returned a nullpointer.");
		};

		//According to cppref
		//http://en.cppreference.com/w/cpp/chrono/c/gmtime
		if(errno == EOVERFLOW)
		{
			throw PGL_EXCEPT_TIME("std::localtime sets errno.");
		};

		//Copy the result
		calTime = *tmpCal;
	}; //End lock


	//Retrun the value
	return calTime;
};

/*
 * \brief	This function returns the pgl_calendarTime_t object, that is not shared.
 *
 * The time since epoc is generated from a call to pgl_getTime.
 *
 * \return 		A non shared pgl_calendarTime_t object
 * \note 		This function aquieres the lock two times indirectly
 */
pgl_calendarTime_t
pgl_getCalendarTime() noexcept(false)
{
	//First get the time
	pgl_time_t currTime = PGL_INVALID_CTIME;

	try
	{
		currTime = pgl_getTime(nullptr);
	}
	catch(::pgl::pgl_runtime_error& exept)
	{
		throw PGL_EXCEPT_TIME("Could not determine the time.");
	};


	//Now return the struct
	return pgl_getCalendarTime(currTime);
};



pgl_string_t
pgl_prettyPrintTime(
	const Numeric_t 	spannSec)
{
	//Use the chrono namespace in thsi function
	using namespace ::std::chrono;

	if(spannSec < 0.0)
	{
		throw PGL_EXCEPT_TIME("Can not pretty print negative values.");
	};

	if(spannSec < 0.001) //this will only be happening if we are less than one milisecond
	{
		/*
		 * Belwo one mili second seconds, we will now go to
		 * one microseconds and then prety pring the result.
		 */

		//We must make Numeric baised durations
		using Second_t      = duration<Numeric_t, seconds::period>;
		using MicroSecond_t = duration<Numeric_t, microseconds::period>;

		//Convert into miliseconds
		const Numeric_t timeInUS = duration_cast<MicroSecond_t>(Second_t(spannSec)).count();

		//Get the string
		const pgl_string_t pretyString = pgl_prettyString(timeInUS, 1, 3);

		return (pretyString + "us");
	};//Handle values below one miilisecond


	if(spannSec < 1.0)  //Only below one second.
	{
		/*
		 * Belwo one seconds, we will now go to
		 * one miliseconds and then prety pring the result.
		 */

		//We must make Numeric baised durations
		using Second_t     = duration<Numeric_t, seconds::period>;
		using MiliSecond_t = duration<Numeric_t, milliseconds::period>;

		//Convert into miliseconds
		const Numeric_t timeInMS = duration_cast<MiliSecond_t>(Second_t(spannSec)).count();

		//Get the string
		const pgl_string_t pretyString = pgl_prettyString(timeInMS, 1, 3);

		return (pretyString + "ms");
	}; //Handle values bellow one seconds


	//Here some variables, that stores the length of a time period in seconds
	const Size_t SEC_MINUTE = 60;
	const Size_t SEC_HOUR   = 3600;
	const Size_t SEC_DAY    = 24 * SEC_HOUR;
	const Size_t SEC_WEEK   = 7 * SEC_DAY; 		//Will not be used
	const Size_t SEC_YEAR   = 365.2425 * SEC_DAY;	// Gregorian year, respects all rule; comes from the C++ stanrad
	const Size_t SEC_MONTH  = SEC_YEAR / 12;

	if(spannSec < SEC_MINUTE)
	{
		/*
		 * Handle less than a minute
		 */

		return pgl_prettyString(spannSec, 2, 3) + "s";
	}; //End if: less than a minute

	if(spannSec < SEC_HOUR)
	{
		/*
		 * Hanlde less than an hour.
		 * convert to hour and full minute.
		 * ignore the seconds
		 */

		//Get the full minutes
		const Size_t fullMinutes = spannSec / SEC_MINUTE;

		//Get the full minute
		const Size_t fullSeconds = Size_t(spannSec) % SEC_MINUTE;

		return (std::to_string(fullMinutes) + "min " + std::to_string(fullSeconds) + "s");
	}; //End if: less than an hour

	if(spannSec < SEC_DAY)
	{
		/*
		 * Handle less than a day.
		 * Output only hours, minutes and seconds
		 */

		const Size_t fullHours = spannSec / SEC_HOUR;

		//Now get the full minutes
		const Size_t fullMinutes = (Size_t(spannSec) % SEC_HOUR) / SEC_MINUTE;

		//Get the full seconds
		const Size_t fullSeconds = Size_t(spannSec) % SEC_MINUTE;

		return (std::to_string(fullHours) + "h " + std::to_string(fullMinutes) + "min " + std::to_string(fullSeconds) + "s");
	}; //End if: less than a day

	if(spannSec < SEC_MONTH)
	{
		/*
		 * Handle less than a month
		 * output only days and hours
		 */
		const Size_t fullDays = spannSec / SEC_DAY;

		const Size_t fullHours = (Size_t(spannSec) % SEC_DAY) / SEC_HOUR;

		return (std::to_string(fullDays) + "d " + std::to_string(fullHours) + "min");
	}; //End if: less than a month

	if(spannSec < SEC_YEAR)
	{
		/*
		 * Less than a year
		 * Output month and days
		 */

		const Size_t fullMonths = spannSec / SEC_MONTH;

		const Size_t fullDays = (Size_t(spannSec) % SEC_MONTH) / SEC_DAY;

		return (std::to_string(fullMonths) + "m " + std::to_string(fullDays) + "d");
	}; //end if: less than a year.

	if(spannSec < (100 * SEC_YEAR))
	{
		/*
		 * Less than hunderd years
		 * we will output years months and days
		 */

		const Size_t fullYears = spannSec / SEC_YEAR;

		const Size_t fullMonths = (Size_t(spannSec) % SEC_YEAR) / SEC_MONTH;

		const Size_t fullDays = (Size_t(spannSec) % SEC_MONTH) / SEC_DAY;

		return (std::to_string(fullYears) + "y " + std::to_string(fullMonths) + "m " + std::to_string(fullDays) + "d");
	}; //End if: less than 100 years

	/*
	 * If we are here we have more than a hundered years,
	 * so we will output ounly the years, we will
	 * use the pretty printer for that.
	 * We will allow for two after digits
	 */
	return (pgl_prettyString(spannSec / SEC_YEAR, 2, 3) + "y");
	(void)SEC_WEEK;
}; //End: prety print time

PGL_NS_END(pgl)




