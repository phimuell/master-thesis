# This CMake file will add all the files in the current folder
# to the PGL library.

target_sources(
	pgl
  PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/pgl_exception.cpp"

	# Only for output
	"${CMAKE_CURRENT_LIST_DIR}/pgl_assert.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_core.cpp"
  PUBLIC
	"${CMAKE_CURRENT_LIST_DIR}/pgl_assert.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_core.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_exception.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_int.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_preprocessor.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_utility.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_strongUsing.hpp"
)


