#pragma once
/**
 * \brief	This file provides a strong using, that is basically a wrapper from
 * 		 The boost serialization library.
 */

// Include the corefile
#include <pgl_core.hpp>


// Include BOOST
#include <boost/serialization/strong_typedef.hpp>


/*
 * \brief	This macro creates a strong using statement.
 * \define 	PGL_STRONG_USING
 *
 * A strong using is similar to a using, but also very different.
 * A using statement introduces an alias for a type, but
 * not a new type, meaning that one can not trigger function
 * overloading with them.
 *
 * The strong using solving the problem, by introducing a new
 * type. This is done by creating a class with that name.
 *
 * Note that this is in a way limited, and should
 * only be used where function resultion is enforeced, like
 * ensuring that the order of severall integer parameter is
 * typesafe.
 *
 * Note that inmplicit convertion in the underling type is
 * supported, the other way is marked explicitly.
 *
 * Note that this macro is an alias of the strong typedef that
 * is provided by the boost serializer library. But the arguments
 * are swapped since we now have a USING statement instead of a
 *
 * \param  NEW_TYPE_NAME	This is the name of the type alias.
 * \param  BASE_TYPE		This is the underling type.
 */
#define PGL_STRONG_USING(NEW_TYPE_NAME, BASE_TYPE) BOOST_STRONG_TYPEDEF(BASE_TYPE, NEW_TYPE_NAME)




