/**
 * \brief	This is the cpp file of the core header.
 *
 * This file is provided to output some compile time messages that are related to the
 * configuration of PGL.
 */

//We must do the following before we include the core header.
//This is needed to see the true value of PGL_NDEBUG
#if defined(PGL_NDEBUG) && ((PGL_NDEBUG) == 0)
#	pragma GCC warning "The PGL_NDEBUG macro is set to the special value zero. This means that, althought it is defined, it is considered disabled, and will be undefined."
#endif


//Now we can include the core header
// Note that this will also modify PGL_NDEBUG if it has the special value zero
#include "./pgl_core.hpp"

#if (PGL_DEBUG_ON == 1) && (PGL_DEBUG_OFF == 0)
#	pragma message "Debugging is enabled."
#elif (PGL_DEBUG_ON == 0) && (PGL_DEBUG_OFF == 1)
#	pragma message "Debugging is disabled."
#else
#	pragma GCC error "Error with the debug ON OFF flags."
#endif


/*
 * ==========================
 * Enforces some constraints
 *
 * See: https://gcc.gnu.org/onlinedocs/cpp/Common-Predefined-Macros.html
 */

#if !defined(__cplusplus)
#	error "PGL needs C++"
#endif


#if __cplusplus < 201402L
#	error "PGL needs at least C++14 enabled."
#endif





