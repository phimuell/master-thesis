#pragma once
#ifndef PGL_CORE__PGL_ASSERT_HPP
#define PGL_CORE__PGL_ASSERT_HPP
/**
 * \file 	pgl_core/pgl_assert.hpp
 * \brief	This file defines a custom assert.
 *
 * This code was originally written by Stefano Weidmann (stefanow@student.ethz.ch).
 * It was modified to fit into the PGL framework.
 *
 * This file will define the macro pgl_assert. This assert is similar to the
 * standard assert, but allows a vardiac number of arguments. Each argument
 * is a condition that must hold in order to pass the test.
 * The arguments are tested individually.
 *
 * Note that the standard assert is also overwritten to refer to the PGL version.
 * This file will respect PGL_NDEBUG
 */

// Include the corefile
#include <pgl_core.hpp>

//Include the preprocessor file
#include <pgl_preprocessor.hpp>


//Some C stuff
#include <cassert>
#include <cstdlib>

#include <iostream>


//Featuretest
#if !defined(__cplusplus) || (__cplusplus < 201103L)
#	error "pgl_assert needs at least C++11!"
#endif


/**
 * \brief	This is the custom assert iomplementation.
 * \define 	PGL_INT_MY_ASSERT_IMPLEMENTATION
 *
 * This is an internal macro, but it is the body that is copied
 * into the assert location. The expansion is later done by boost.
 *
 */
#define PGL_INT_MY_ASSERT_IMPLEMENTATION(r, name, elem)	 			\
if (!(elem)) 									\
{ 										\
	std::cerr 								\
		<< "\n" << (name) << " FAILED\n"				\
		<< "--------------------------------------------------------" 	\
		<< "\nfile: " 	<< __FILE__ 					\
		<< "\nfunction: " << __func__ 					\
		<< "\nline: " << __LINE__ 					\
	     /* << "\nthread: " << getMyThreadNumber() */ 			\
		<< "\ncondition which failed: " << BOOST_PP_STRINGIZE(elem) 	\
		<< "\n"; 							\
	std::cerr.flush(); 							\
	std::abort(); 								\
}


/**
 * \brief	This is the ensure implementation
 * \define 	PGL_INT_ENSURE_IMPLEMENTATION_BASE
 *
 * This implementation is execues its argument and casts them to void.
 *
 * \param  r		Boost internal variable, not used.
 * \param  name		Constant data, not used.
 * \param  elem		Iteration element, condition.
 */
#define PGL_INT_ENSURE_IMPLEMENTATION_BASE(r, name, elem) ((void)(elem));


/**
 * \brief	This is a level of indirectionality that allows a multistatement macro.
 * \define 	PGL_INT_MY_ASSERT_IMPLEMENTATION_FORCE
 *
 * In this macro the iteration over the conditions happens.
 * It expects a name, which a name, for example "assert" or "precondition",
 * and a boost preprocessor sequence, each element of that sequence is a
 * condition that should be tested and for each condition the above
 * macro PGL_INT_MY_ASSERT_IMPLEMENTATION is expanded.
 *
 * Putting it into a do-while(0) loop is an idiom that allows a multistatement
 * macro, such as the above one, to be used in places where a single statement
 * macro would be used, see "http://www.bruceblinn.com/linuxinfo/DoWhile.html".
 *
 * \note	This is an internal macro that is always defined.
 *
 * \param  name		The name of the test that this is the base of.
 * \param  seq		A sequences of tests.
 */
#define PGL_INT_MY_ASSERT_IMPLEMENTATION_FORCE(name, seq) \
do { \
	BOOST_PP_SEQ_FOR_EACH(PGL_INT_MY_ASSERT_IMPLEMENTATION, name, seq); \
} while(false)


/**
 * \brief	This allows to iterate over the sequence and still provide a single
 * 		 statement operation
 * \define 	PGL_INT_ENSURE_IMPLEMENTATION
 *
 * This function expects a boost preprocessor sequence as its second argument.
 * Note that the first argument, is ignored and provided for compability with
 * the the assert construct.
 *
 * The passed sequence will be executed and casted to void.
 * This macro will expand to a single statement.
 *
 * \param  name 	is ignored, provided for compaility.
 * \param  seq		Sequences of code to execute.
 */
#define PGL_INT_ENSURE_IMPLEMENTATION(name, seq) \
do { \
	BOOST_PP_SEQ_FOR_EACH(PGL_INT_ENSURE_IMPLEMENTATION_BASE, name, seq); \
} while(false)



/**
 * \brief	This macro is jet another wrapper around the the implementation.
 * \define	PGL_INT_MY_ASSERT_CHECK
 *
 * This macro is the same as the PGL_INT_MY_ASSERT_IMPLEMENTATION_FORCE macro.
 * But it will respect the PGL_NDEBUG state.
 * If debuging is disabled, it will expand to the Implementation, and if
 * debugging is disabled it will expand to an empty statement.
 *
 * \param  name		The name of the test that this is the base of.
 * \param  seq		A sequences of tests.
 */
#if PGL_DEBUG_ON != 0
	//Define it to the force implementation
	#define PGL_INT_MY_ASSERT_CHECK(name, seq) PGL_INT_MY_ASSERT_IMPLEMENTATION_FORCE(name, seq)
#else
	//define it to the empty statement
	#define PGL_INT_MY_ASSERT_CHECK(name, seq) PGL_EMPTY_STATEMENT
#endif


/**
 * \brief	This is a macro the tests for invariants.
 * \define	pgl_invar
 *
 * This mactro is in a certain way an alias of the assert macro.
 * But if it is fails it will not print out ASSERTION, but INVARIANT.
 * This allows to distinguish what caused the error.
 *
 * \note 	This macro has only an effect if debugging is disabled.
 * 		 and if disabled it will expand to the empty statement
 * 		 and doing nothing.
 *
 * \param  ...		List of tests that should be preformed.
 */
#define pgl_invar(...) PGL_INT_MY_ASSERT_CHECK("INVARIANT", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))


/**
 * \brief	This is a macro the tests for preconditions
 * \define	pgl_precond
 *
 * This mactro is in a certain way an alias of the assert macro.
 * But if it is fails it will not print out ASSERTION, but PRECONDITION.
 * This allows to distinguish what caused the error.
 *
 * \note 	This macro has only an effect if debugging is disabled.
 * 		 and if disabled it will expand to the empty statement
 * 		 and doing nothing.
 *
 * \param  ...		List of tests that should be preformed.
 */
#define pgl_precond(...) PGL_INT_MY_ASSERT_CHECK("PRECONDITION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))


/**
 * \brief	This is a macro the tests for postconditions
 * \define	pgl_postcond
 *
 * This mactro is in a certain way an alias of the assert macro.
 * But if it is fails it will not print out ASSERTION, but POSTCONDITION.
 * This allows to distinguish what caused the error.
 *
 * \note 	This macro has only an effect if debugging is disabled.
 * 		 and if disabled it will expand to the empty statement
 * 		 and doing nothing.
 *
 * \param  ...		List of tests that should be preformed.
 */
#define pgl_postcond(...) PGL_INT_MY_ASSERT_CHECK("POSTCONDITION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

//Undefine the pgl_assert token
#ifdef pgl_assert
	#undef pgl_assert
#endif


/**
 * \brief	This is the PGL assert macro.
 * \define	pgl_assert
 *
 * This macro is similar to the assert macro provided by the standard, but this is cooler.
 * It has a vardiac number of arguments. Each argument is interpreted as a single condition.
 * Meaning that it is easy to specify many tests with the same macro, or to group tests.
 *
 * If the macro PGL_DEBUG_ON has the value zero the macro will expand to the empty statement.
 * Thus no checks will be performed. This means that all side effects that the conditions
 * meight have had will not ake effect.
 * For a solution see pgl_ensure and pgl_enforce.
 *
 * \param  ...		Comma seperated list of conditions to test.
 */
#define pgl_assert(...) PGL_INT_MY_ASSERT_CHECK("ASSERTION", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))

/*
 * Forward calls to the classical asserts to pgl_assert.
 */
#undef assert
#define assert(...) pgl_assert(__VA_ARGS__)


/**
 * \brief	This macro is similar to the assert macro, but is not affected by the debug state.
 * \define	pgl_enforce
 *
 * pgl_assert has only an effect if deguging is enabled. But some tests are so important, that that
 * they should take effect also when debugging is disabled. This is is where requiere comes into
 * play. It is basically the same as the assert macro, but is not disabled if deguging is disabled.
 * It is active all the time.
 * It works the same as the assert macro does, meaning it also accepts a list of conditions to check.
 *
 * \param  ... 		The list of checks that have to be done.
 *
 * \note	This macro is not part of the original PGL assert family. However it was previously
 * 		 called pgl_requier, which was one, but was later remain to better match its purpose.
 */
#define pgl_enforce(...) PGL_INT_MY_ASSERT_IMPLEMENTATION_FORCE("ENFORCE", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__) )


/**
 * \brief	This macro is similar to the assert macro, but will ensure that side effects happens.
 * \define	pgl_ensure
 *
 * If debugging is enabled this macro is like an alias of pgl_assert, but with a different name.
 * However if degugging is disabled, this macro will not expand to the empty statement and ignore
 * its argument. It will execute them, as if (void)(cond), was used, but ignore the return value.
 * This allows side effects, that are important to take effect, but the result of the test is
 * ignored.
 *
 * \param  ...		List of stements to check or execute.
 *
 * \note	In earlier versions of PGL this was an alias of pgl_postcond, but it was never used.
 * \note	The Bosst.Assert library has a similar function, called BOOST_VERIFY.
 */
#if PGL_DEBUG_ON != 0
#	define pgl_ensure(...) PGL_INT_MY_ASSERT_IMPLEMENTATION_FORCE("ENSURE", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__) )
#else
#	define pgl_ensure(...) PGL_INT_ENSURE_IMPLEMENTATION("ENSURE", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__) )
#endif


/*
 * =======================
 * Helper constructs that can come in handy when working with asserts
 */


/**
 * \brief	This macro implements a conditional test.
 * \define	pgl_ifteTest
 *
 * This function implements an if then else test. Depending
 * on the \e If condition, either the \e Then condition or
 * the \e Else condition is evaluated. Note that this macro
 * does not call assert on its own.
 *
 * It is implemented by using the terniary operator.
 *
 * \param  If		Determines which teste we should do.
 * \param  Then		Test that should be done, if If is true.
 * \param  Else		Test that should be done, if If is false.
 */
#define pgl_ifteTest(If, Then, Else) ((If) ? (Then) : (Else))



/**
 * \brief	This macro implements the mathematical implication.
 * \define	pgl_implies
 *
 * This macro can be used to create conditional tests.
 * To only enforce condition is something is true.
 * It is not implemented with teritianary operator, but
 * with logical operators.
 *
 * \param  When		When do we test.
 * \param  Then		The conditition that should be true.
 *
 * \note	Then is only evaluated, iff When is true.
 */
#define pgl_implies(When, Then) ((!(When)) || (Then))


/**
 * \brief	This macro can be used to suppress compiler warnings
 * 		 about unused variables.
 *
 * If debugging is enabled, meaning if assert extressions habve an effect,
 * this macro expands to the empty scope. If debugging is disabled,
 * this macro will cast all of its argument to void.
 * The use of this macro is to suppress that are related to compiler
 * warnings about variables that are only used inside assert expressions.
 * And are not used if asserts are disabled.
 *
 * The macro will accept a vardiac list of arrguments.
 */
#if PGL_DEBUG_OFF == 1
#	define pgl_onlyForDebugging(...) PGL_INT_ENSURE_IMPLEMENTATION("onlyForDebugging", BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__) )
#else
#	define pgl_onlyForDebugging(...) {}
#endif


#endif //End includeguard

