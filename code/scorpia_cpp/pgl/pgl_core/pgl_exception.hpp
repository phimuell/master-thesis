#pragma once
#ifndef PGL_CORE__PGL_EXCEPTION_HPP
#define PGL_CORE__PGL_EXCEPTION_HPP
/**
 * \file 	pgl_core/pgl_ecxeption.hpp
 * \brief 	defines the pgl exception
 *
 * The pgl exceptions are similar to the exception provided by the standard, they are named the same, expet with the pgl
 * prefix. They inherent form std::exception.
 * They provide more information, but required more input argument, to ease their useage, use rthe provided macros.
 */

//Include pgl
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>


//Include std
#include <exception>
#include <string>


PGL_NS_START(pgl)

/**
 * \brief 	Base class o pgl exceptions
 *
 * This class is the base of pgl exceptions.
 * It stores some more informations than the normal exception does.
 * Compare its output to an assert.
 */
class pgl_exception : public std::exception
{
	/*
	 * Some typedefs
	 */
public:
	using String_t = std::string; //!< The used string type

	/**
	 * \brief constructiors of the exception
	 *
	 * The default constructor is forbidden, but the others are defaulted.
	 * Destructor is virtual
	 */

	// Default constructor is forbidden
	inline
	pgl_exception() = delete;

	// Defaulted copy and move constructors
	inline
	pgl_exception(
		const pgl_exception&) = default;

	inline
	pgl_exception(
		pgl_exception&&) = default;


	//Also defaulted assignments
	inline
	pgl_exception&
	operator= (
		const pgl_exception&) = default;

	inline
	pgl_exception&
	operator= (
		pgl_exception&&) = default;


	virtual
	~pgl_exception() = default;


	/**
	 * \brief constructor to properly create a pgl_exception
	 *
	 * It takes different argument, the last one is for the type, this one is defaulted
	 */
public:
	inline
	pgl_exception(
		const String_t&  message,
		const String_t&  func,
		const String_t&  file,
		const int 	 line,
		const String_t&  type = String_t()):
	  std::exception(),
	  m_message(message),
	  m_func(func),
	  m_file(file),
	  m_type(type),
	  m_line(line),
	  m_outputStorage()
	{
		this->prot_composeMessage();

	};

	/**
	 * \brief 	This function formats the message and stors it in the m_outputStorage
	 */
protected:
	void
	prot_composeMessage();


	/**
	 * This is the what function.
	 * it allows to print out some usefull informations
	 */
public:
	virtual
	const char* what() const noexcept override;

protected:
	String_t 	m_message;	//!< Message to store some usefull error notice
	String_t 	m_func;		//!< Name of the function that trows the exception
	String_t 	m_file;		//!< File where the exception was generated
	String_t 	m_type;		//!< A type of the exeption can be empty
	int 		m_line;		//!< The linenumber where the exception was thrown
	String_t 	m_outputStorage;	//!< A string to store the output, Why not return a string
};

/*
 * This macro defines honor the rule of 5
 */
#define PGL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE(Klasse) 		\
	Klasse ( const Klasse &) = default; 			\
	Klasse ( Klasse && ) = default;				\
	Klasse () = default;					\
	Klasse & operator= (const Klasse &) = default;		\
	Klasse & operator= ( Klasse && ) = default;		\
	virtual ~ Klasse () = default;



/**
 * \breif	Runtime Exeption Class
 *
 * This class is the base for all runtime exception.
 * Runtime exceptions are exception that occured during runtime, and that are (probably) not related to programing error.
 * The perfect program should throw them. They are for situation where memory allocation fails, a file is not there or something like them.
 */
class pgl_runtime_error : public pgl::pgl_exception
{
public:
	PGL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE( pgl_runtime_error)

public:
	inline
	pgl_runtime_error(
		const String_t&  message,
		const String_t&  func,
		const String_t&  file,
		const int 	 line,
		const String_t&  type = String_t("Runtime")):
	  pgl_exception(message, func, file, line, type)
	{};
};


/**
 * \breif	Logic Exeption Class
 *
 * This class is the base for all logical errors that can arire.
 * A logical error is an error that has to do with errors that are not runtime induced.
 * Like passing to few argument to a function ore an out of range error.
 */
class pgl_logic_error : public pgl::pgl_exception
{
public:
	PGL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE( pgl_logic_error)

public:
	inline
	pgl_logic_error(
		const String_t&  message,
		const String_t&  func,
		const String_t&  file,
		const int 	 line,
		const String_t&  type = String_t("Logic")):
	  pgl_exception(message, func, file, line, type)
	{};
};





// ==============================================
// ==============================================
//
// MACRO MAGIC

/*
 * This macro writes an exception class, that inherents from a given base.
 */
#define PGL_CORE_EXCEPT_CREATE_EXCEPTION(ClassName, BaseClass, TYPE) class ClassName : public BaseClass { 	\
	public:												  	\
	PGL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE( ClassName )							\
	public:													\
	inline													\
	ClassName (												\
		const String_t&  message,									\
		const String_t&  func,										\
		const String_t&  file,										\
		const int 	 line,										\
		const String_t&  type = String_t( TYPE )):							\
	  BaseClass (message, func, file, line, type)								\
	{};													\
}


// Make some runtime based errors
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_range_error, pgl_runtime_error, "Range");
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_underflow_error, pgl_runtime_error, "Underflow");
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_overflow_error, pgl_runtime_error, "Overflow");
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_alloc_error, pgl_runtime_error, "Allocation");
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_nullptr_error, pgl_runtime_error, "NullPtr Access");
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_time_error, pgl_runtime_error, "Error in Time Functions");


// Make some logic errors
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_invalid_argument, pgl_logic_error, "Invalid Argument");
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_length_error, pgl_logic_error, "Length");
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_out_of_range_error, pgl_logic_error, "Out of Bound");
PGL_CORE_EXCEPT_CREATE_EXCEPTION(pgl_illegal_method_call_error, pgl_logic_error, "Not Implemented Method");


#undef PGL_CORE_EXCEPT_CREATE_EXCEPTION


/*
 * Make some macros that are used to generate clean exceptions, without much hassel
 */

#define PGL_EXCEPT_RUNTIME(MSG) 	::pgl::pgl_runtime_error( MSG, __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define PGL_EXCEPT_ALLOC(MSG)   	::pgl::pgl_alloc_error( MSG, __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define PGL_EXCEPT_NULL(MSG)   		::pgl::pgl_nullptr_error( MSG, __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define PGL_EXCEPT_TIME(MSG)   		::pgl::pgl_time_error( MSG, __PRETTY_FUNCTION__, __FILE__, __LINE__)

#define PGL_EXCEPT_LOGIC(MSG)		::pgl::pgl_logic_error( MSG,  __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define PGL_EXCEPT_InvArg(MSG)		::pgl::pgl_invalid_argument( MSG,  __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define PGL_EXCEPT_LENGTH(MSG)		::pgl::pgl_length_error(MSG,  __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define PGL_EXCEPT_OutOfBound(MSG)	::pgl::pgl_out_of_range_error(MSG,   __PRETTY_FUNCTION__, __FILE__, __LINE__)
#define PGL_EXCEPT_illMethod(MSG)	::pgl::pgl_illegal_method_call_error(MSG,   __PRETTY_FUNCTION__, __FILE__, __LINE__)















PGL_NS_END(pgl)





#undef PGL_CORE_EXCEPT_INTERNAL_RULE_OF_FIVE

#endif //End include guards
