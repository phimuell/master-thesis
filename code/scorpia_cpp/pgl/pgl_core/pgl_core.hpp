#pragma once
#ifndef PGL_CORE__PGL_CORE_HPP
#define PGL_CORE__PGL_CORE_HPP
/**
 * \file pgl_core/pgl_core.hpp
 *
 * \brief this is the core and configuration file of pgl
 *
 * This file is the configuaration file of pgl.
 * It is the central place, each pgl source and headerfile is obligated it to include as first include.
 * Its intended use is to provide global configurations.
 *
 * No other header file shall by included by this file.
 * And all PGL files and derived libraries shall include this file first.
 */


/*
 * This is a constraint that has to be enforced in all files of PGL.
 * Thus it is not checked in the source file, but directly in this header.
 * To be feature complete we only requiere that a C not a C++ compiler is
 * used, if that is desired use __GNUG__
 */
#if !defined(__GNUC__)
#	error "PGL targets only GNU compilers."
#endif


/**
 * \brief	This macro is defined if a C++ compiler is used.
 * \define 	PGL_CXX_IS_USED
 *
 * If a C++ compiler, meang g++, is used this macro is defined to
 * have a non zero value. If no C++ compuliler, but a C compiler
 * is used this macro is not defined or, has the special value
 * zero.
 */
#if defined(__GNUG__)
#	undef   PGL_CXX_IS_USED
#	define  PGL_CXX_IS_USED 1
#else
#	undef   PGL_CXX_IS_USED
#endif


/*
 * \brief	This is a macro that allows expansion.
 * \define 	PGL_INT_EXPAND
 *
 * This macro expands to its argument and thus allows
 * to triger a second expand iteration.
 *
 * \param  X 	The value of the macro
 */
#define PGL_INT_EXPAND(X) X




/**
 * \def 	PGL_NS_START
 * \brief 	This macro declares a namespace with the given name
 *
 * There is also the macro PGL_NS_BEGIN which is an alias of this macro.
 *
 * \param  NAME 	The name of the namespace.
 */
#define PGL_NS_START(NAME) namespace PGL_INT_EXPAND(NAME) {
#define PGL_NS_BEGIN(NAME) PGL_NS_START(PGL_INT_EXPAND(NAME))


/**
 * \def		PGL_NS_END
 * \brief	Macro to close the namespace.
 *
 * Theis macro expands to the closing curly bracket '}',
 * its argument is ignored. It is used to indicate which
 * namespace is closing.
 *
 * \param  NAME		Name of the namespace; is ignored.
 *
 */
#define PGL_NS_END(NAME) }


/**
 * \def 	PGL_IMPORT_TYPE
 * \brief	This macro can inport a type from a class.
 *
 * This macro imports a type that is defined in a class, or also
 * namespace. Technically it expands to
 * 	using TYPE = CLASS :: TYPE
 * so semicolon at the end.
 *
 * \param  TYPE		The type that should be included.
 * \param  CLASS	The class or the namespace where it is defined.
 */
#define PGL_IMPORT_TYPE(TYPE, CLASS) using PGL_INT_EXPAND(TYPE) = PGL_INT_EXPAND(CLASS) :: PGL_INT_EXPAND(TYPE)



/**
 * \def PGL_LIKELY
 * \def PGL_UNLIKELY
 * \brief give some hints to the branch prediction
 *
 * This macros emit some instruction to the branch prediction.
 * Use with care, since it can cost a lot if they are placed wrong.
 *
 * Needs to !! to convert it to an int.
 * Is needed, C quirk.
 * See also: "http://blog.man7.org/2012/10/how-much-do-builtinexpect-likely-and.html"
 */
#define PGL_LIKELY(x)       __builtin_expect(!!(PGL_INT_EXPAND(x)), 1)
#define PGL_UNLIKELY(x)     __builtin_expect(!!(PGL_INT_EXPAND(x)), 0)


/**
 * \def	PGL_NOEXCEPT_IF( expr )
 *
 * \brief 	This macro is for the case, if one whjant to test, if an expresion throws.
 * 		It is intended for the declaration directly.
 */
#define PGL_NOEXCEPT_IF(cond) noexcept(noexcept(PGL_INT_EXPAND(cond)))


/**
 * \brief	This is the debug token.
 * \define 	PGL_NDEBUG
 *
 * This macro has the same meaning as the DEBUG macro from the C standard.
 * If defined it indicates that that no debug operations should be performed.
 * Althought not requiered by the Cstandard nor PGL it is recommended to set
 * the macro to a value different from 0. It is also recomended to consider
 * a value of zero as equal as not defined. Note that for compability, empyt
 * should also count as non zero.
 *
 * The NDEBUG macro is synchronized to match the value of PGL_NDEBUG.
 * So PGL_NDEBUG takes precedence and will overwrite NDEBUG.
 *
 * \note 	If the macro is not defined, the effect is the same as if it
 * 		 was set to zero. This is teh same as undefining it.
 *
 * \note	This header will undefine the PGL_NDEBUG macro if its value
 * 		 equals zo zero.
 */
#if !defined(PGL_NDEBUG)
#	define PGL_NDEBUG 0
#endif


/*
 * Here we sync the PGL_NDEBUG and the UNDEBUG.
 * Since this header shall be the very first included header, this will work.
 * If PGL_NDEBUG is set it will set NDEBUG to the samle value.
 *
 * See also the config source file
 */
#if defined(PGL_NDEBUG) && ((PGL_NDEBUG) != 0)
	//undefine the NDEBUG macro
#	undef NDEBUG
	//redifine it with the value of the PGL_NDEBUG MACRO
#	define NDEBUG PGL_INT_EXPAND(PGL_NDEBUG)
#else
	//If PGL_NDEBUG is not defined or zero, then undefine the NDEBUG macro as well.
#	undef NDEBUG

	//If the PGL_NDEBUG macro is zero, it will be undefined
#	if defined(PGL_NDEBUG) && ((PGL_NDEBUG) == 0)
#		undef PGL_NDEBUG
# 	endif

#endif


/**
 * \brief	This macro constant expans to 1 if debuging is enabled and zero if disabled.
 * \define	PGL_DEBUG_ON
 *
 * This macro constant can be used to bypass tests and make the debuging experience more
 * uniform inside PGL. NOte that this macro constant is quite new so not all function
 * use it.
 * It applies the rule for debuging.
 *
 * \note	That this macro is ALWAYS defined.
 */
#if !defined(PGL_NDEBUG)	/* will not be equal to zero, because of above command */
#	define PGL_DEBUG_ON 1
#else
#	define PGL_DEBUG_ON 0
#endif


/**
 * \brief	This macro constant can be used to check if debuging is disabled.
 * \define 	PGL_DEBUG_OFF
 *
 * Its value is zero if debuiging is enabled and 1 if debugging is disabled.
 * Note that this constant is the invert value of PGL_NEBUG_ON
 *
 * \note	That this macro is ALWAYS defined.
 */
#if PGL_DEBUG_ON == 0
#	define PGL_DEBUG_OFF 1
#else
#	define PGL_DEBUG_OFF 0
#endif



/*
 * Undefining some helper macros
 */

#endif //End include guard
