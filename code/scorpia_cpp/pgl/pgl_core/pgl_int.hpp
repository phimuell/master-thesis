#pragma once
#ifndef PGL_CORE__PGL_INT_HPP
/**
 * \brief This file declares integer that are used through out the library
 * \file pgl_core/pgl_int.hpp
 *
 * The declarations are similar to the one in the cstdint header.
 * The header is similar to cstdint, but it also declares some typedefs for floating points.
 */

//Include PGL
#include <pgl_core.hpp>

//Include STD
#include <cstdint>

namespace pgl
{

	/*
	 * Integer declarations
	 */

	using Int_t 		= int;
	using uInt_t		= unsigned int;

	using Int8_t 		= std::int8_t;
	using uInt8_t 		= std::uint8_t;

	using Int16_t		= std::uint16_t;
	using uInt16_t 		= std::uint16_t;

	using Int32_t		= std::int32_t;
	using uInt32_t		= std::uint32_t;

	using Int64_t 		= std::int64_t;
	using uInt64_t		= std::uint64_t;

	using Size_t		= std::uintmax_t;
	using sSize_t		= std::intmax_t;


	using Numeric_t 	= double;
	using Real_t		= long double;
	using Float_t		= float;


}; //End namespace(pgl)





#endif //End include guards
