/**
 * \brief	This is the associated file for the assert header.
 *
 * This file contains the output statements that output the status.
 * This will have the effect that the output is generated just once.
 *
 * NOTE
 * THIS FILE IS NOT LINKED INTO PGL.
 */

#include "./pgl_assert.hpp"

#ifndef PGL_NDEBUG

	#pragma message "PGL_NDEBUG not set, assertions are enabled"

#else
	#pragma message "PGL_NDEBUG set, assertions are disabled"
#endif

