#pragma once
#ifndef PGL_CORE__PGL_PREPROCESOR
#define PGL_CORE__PGL_PREPROCESOR
/*
 * \file	pgl_core/pgl_preprocessor.hpp
 *
 * \brief	This file defines some preprocessor functions that are usefull.
 */

//Include PGL
#include <pgl_core.hpp>


//Boost for the preprocessor
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/stringize.hpp>


//Is needed for a new level of indirection
#define PGL_IMPL_PP_CONCATENATE(First, Second) First ## Second

/**
 * \brief concatenates the two tokens together.
 * \define 	PGL_CONCATENATE
 *
 * Note that this nacro uses a level of indirection
 * to guarantee that the arguments are expanded, before
 * they are concatenated.
 *
 * \param  first 	The first token
 * \param  second	The second token
 *
 * \note	For compatibility and brivity PGL_PP_CONCATENATE
 * 		 and PGL_CAT are provided as alias.
 */
#define PGL_CONCATENATE(First, Second) PGL_IMPL_PP_CONCATENATE(First, Second)

//For compability
#define PGL_PP_CONCATENATE(x, y) PGL_CONCATENATE(x, y)
#define PGL_CAT(x, y) 	         PGL_CONCATENATE(x, y)


/**
 * \brief	This macro allows to execute a pragma in a macro expression.
 * \define 	PGL_DO_PRAGMA
 *
 * \param  x 	A string literal
 *
 * Rememeber that x iself must be a string, so " must be escaped by \".
 * See also "https://gcc.gnu.org/onlinedocs/cpp/Pragmas.html"
 *
 * PGL_DO_PRAGMA(x) is equivalent to '#pragma x '.
 */
#define PGL_DO_PRAGMA(x) _Pragma (#x)


/**
 * \brief	This macro expand to the empty statement.
 * \define	PGL_EMPTY_STATEMENT
 *
 * Note that it will expand to a single statement, an empty
 * do-while(0) loop. Note that a trailing semicolon is needed.
 * See "http://www.bruceblinn.com/linuxinfo/DoWhile.html"
 */
#define PGL_EMPTY_STATEMENT  do {} while (0)


/**
 * \brief	This macro is able to force an expansion.
 * \param  	PGL_EXPAND
 *
 * This function is basically the identety operation, if will
 * expand to its argument unmodified. It can be used to create
 * a second level scanning.
 *
 * \param  ... 	Vardiac list
 */
#define PGL_EXPAND(...) __VA_ARGS__


/**
 * \brief	This macro is the identty operation.
 * \define 	PGL_IDENTITY
 *
 * This macro will expand to a its argument. Note that
 * this function only accepts one argument in contrast
 * to PGL_EXPAND
 *
 * \param  a	The value to which this mactro expands
 */
#define PGL_IDENTITY(a) a



/**
 * \brief	This macro allows you to stringify an expression.
 * \define 	PGL_STRINGIFY
 *
 * This mnacro uses two levels of indirection to force the the expansion
 * of a macro. So if you pass a macro definition it gets expanded first,
 * before it is stringified.
 *
 * \param  x	What to stringify.
 */
#define PGL_STRINGIFY(x) PGL_IMPL_STRINGIFY(x)


/**
 * \brief	This macro will output a message when the line is compiled.
 * \define 	PGL_TODO
 *
 * This macro will cause the compiler to print a TODO note to the
 * screen when compiled.
 */
#define PGL_TODO(x) PGL_DO_PRAGMA(message ("TODO - " PGL_STRINGIFY(x)))


/**
 * \brief	This macro casts all its argument to void.
 * \define	PGL_UNUSED
 *
 * This macro allows to marc some variables as unused. Note
 * that there is a similar macro in the assert header, that
 * does this, but only if debugging is disabled.
 *
 * \note	This macro also exepts expressions, they will be
 * 		 executed, but it designed for indicating that
 * 		 arguments of functions are not used, or local
 * 		 variables.
 *
 * \param ... 		All variables that should be casted to void.
 */
#define PGL_UNUSED(...) PGL_UNUSED_IMPL(BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))




/*
 * =======================0
 * Internal implementations
 *
 * These are needed for a second pass of the scan
 */

//This is needed to force the expansion
#define PGL_IMPL_STRINGIFY(x) #x


/**
 * \brief	This is the implementation for the
 * 		 unused macro, for a single element.
 * \define 	PGL_UNUSED_IMPL_SINGLE
 *
 * This is designed to be used with Boost macro magic.
 *
 *
 * \param  r		Boost internal variable, not used.
 * \param  name		Constant data, not used.
 * \param  elem		Iteration element, condition.
 */
#define PGL_UNUSED_IMPL_SINGLE(r, name, elem) ((void)(elem));


/**
 * \brief	This is the internal implementation for the unused.
 * \define 	PGL_UNUSED_IMPL
 *
 * This function expects a boost preprocessor sequence as its second argument.
 * The passed sequence will be executed and casted to void.
 * This macro will expand to a single statement.
 *
 * \param  name 	is ignored, provided for compaility.
 * \param  seq		Sequences of code to execute.
 */
#define PGL_UNUSED_IMPL(seq) 						\
do { 									\
	BOOST_PP_SEQ_FOR_EACH(PGL_UNUSED_IMPL_SINGLE, "UNUSED", seq); 	\
} while(false)





#endif //End include guard
