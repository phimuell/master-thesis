#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_VERTEX_PAYLOAD_HPP
#define PGL_GRAPH__PGL_GRAPH_VERTEX_PAYLOAD_HPP
/**
 * \brief 	This file defines the payload for the vertex
 *
 */

//PGL-Stuff
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>


//STD-Stuff
#include <memory>


PGL_NS_START(pgl)
PGL_NS_START(graph)



class pgl_vertex_payload_t
{
	/*
	 * ======================
	 * constructors
	 *
	 * The are defaulted and protected
	 */
protected:

	pgl_vertex_payload_t();

	pgl_vertex_payload_t(
		const pgl_vertex_payload_t&);

	pgl_vertex_payload_t(
		pgl_vertex_payload_t&&);

	pgl_vertex_payload_t&
	operator= (
		const pgl_vertex_payload_t&);

	pgl_vertex_payload_t&
	operator= (
		pgl_vertex_payload_t&&);

public:
	virtual
	~pgl_vertex_payload_t();



	/*
	 * ===============================
	 * BEGIN Distance
	 */
public:


	/**
	 * \brief 	returns true if the distance property is implemented
	 */
	virtual
	bool
	_implements_DISTANCE() const;


	/**
	 * \brief	This returns the current distance of the vertex.
	 */
	virtual
	Distance_t
	getCurrDistance() const;


	/**
	 * \brief	Set the current distance to the given value
	 * \param nd	New Distance
	 *
	 */
	virtual
	void
	setCurrentDistanceTo(
		const Distance_t& nd);

	/*
	 * END DISTANCE
	 * =========================
	 */


	/*
	 * ==========================
	 * BEGIN Visiting
	 */

	virtual
	bool
	_implements_VISITING() const noexcept;


	/**
	 * \brief 	Returns true if this was visited before
	 */
	virtual
	bool
	isVisited() const;


	/**
	 * \brief	Set the vistied status of *this to true
	 */
	virtual
	void
	makeVisited();


	/**
	 * \brief	Make *this unvisited
	 */
	virtual
	void
	makeUnvisited();



	/*
	 * END Visiting
	 * =========================
	 */


	/*
	 * ==============================================
	 * BEGIN ORDERING
	 */

	/**
	 * \brief	Returns true, if *this implements the ordering proprerty
	 */
	virtual
	bool
	_implements_ORDERING() const noexcept;


	/**
	 * \brief	Makes the predecessor ID of *this invalid
	 */
	virtual
	void
	clearPredecessor();


	/**
	 * \brief 	Sets the predecessor to the given ID
	 */
	virtual
	void
	setPredecessorTo(
		pgl_vertexID_t 		newP);


	/**
	 * \brief 	returns the predecessor id of *this
	 */
	virtual
	pgl_vertexID_t
	getPredecessor() const;

	/**
	 * \brief	Returns true, if the ID is valid
	 */
	virtual
	bool
	hasPredecessor() const;

	/*
	 * END ORDERING
	 * =================================
	 */


	/*
	 * ================================
	 * BEGINS Kleinberg Model Core
	 *
	 * To fill the distance to all other nodes, the constructor must be used
	 */


	/**
	 * \brief KleinbergCore:	This function returns the distance to the current target node.
	 * 				This distance is also called the routing distance.
	 */
	virtual
	Distance_t
	getRoutingDistance() const;


	/**
	 * \brief KleinbergCore:	This function returns the ID of the current target node
	 */
	virtual
	pgl_vertexID_t
	getCurrentTargetNodeID() const;


	/**
	 * \brief KleinbergCore:	Retruns true, if this has a valid target node
	 */
	virtual
	bool
	hasValidTargetNode() const;


	/**
	 * \brief KleinbergCore 	This function cheges the state of *this such that now the new Target is considered as target node
	 * 					A call to this fucntion shall invalidate the routingdistance that *this currently has.
	 *
	 * \param newTargetID		The ID of the node that will be the new target node
	 */
	virtual
	void
	setCurrentTargetNodeID(
		const pgl_vertexID_t& newTargetID);

	/**
	 * \brief KleinbergCore		This fuinction will set the current ruting distance to the target.
	 * 					It is the reponsibility of the user, to do it correctly.
	 */
	virtual
	void
	setCurrentRoutingDistanceTo(
		const Distance_t 	newDistance);

	/**
	 * \brief KleinbergCore		This function returns true if the node was vidited in the past.
	 * 					A change to now target node shall unset this value.
	 */
	virtual
	bool
	wasVisitedByKleinberg() const;

	/**
	 * \brief KleinbergCore		This function sets the visited state of this to true
	 */
	virtual
	void
	makeVisitedByKleinberg();


	/*
	 * ENDS Kleinberg Model Core
	 * =================================
	 */
};



PGL_NS_END(graph)
PGL_NS_END(pgl)


#endif //end include guard
