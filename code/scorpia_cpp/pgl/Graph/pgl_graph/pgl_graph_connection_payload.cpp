/**
 * \brief this file implements the default payload for the connection. in a reduced form
 */

#include <pgl_core.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_connection_payload.hpp>


// Include STD
#include <string>

PGL_NS_START(pgl)
PGL_NS_START(graph)



#define PGL_THROW_CONNPAY_NOT_IMPLENTED_YET throw PGL_EXCEPT_illMethod("Tried to call a not implemented method.")
/*
 * =================================================================
 * Kleinberg
 *
 * Here is the part that is used for the Kleinberg part.
 */

bool
pgl_connection_payload_t::_implements_KLEINBERG() const
{
	return false;
};


pgl_connection_payload_t::~pgl_connection_payload_t() = default;


///Defaulted default constructor
pgl_connection_payload_t::pgl_connection_payload_t() = default;

///Defaulted copy constructor
pgl_connection_payload_t::pgl_connection_payload_t(
	const pgl_connection_payload_t&) = default;

///Defaulted moveconstructor
pgl_connection_payload_t::pgl_connection_payload_t(
	pgl_connection_payload_t&&) = default;

///Defaulted copy assignment
pgl_connection_payload_t&
pgl_connection_payload_t::operator= (
	const pgl_connection_payload_t&) = default;

//Defaulted move assignment
pgl_connection_payload_t&
pgl_connection_payload_t::operator= (
	pgl_connection_payload_t&&) = default;

/*
 * END KLEINBERG
 * ================================================================
 */



/*
 * ================================================================
 * Matching
 *
 * This is a boolean property
 */
bool
pgl_connection_payload_t::_implements_MATCHING() const
{
	return false;
};


bool
pgl_connection_payload_t::isMatched() const
{
	PGL_THROW_CONNPAY_NOT_IMPLENTED_YET;
	return false;
};


bool
pgl_connection_payload_t::makeMatched()
{
	PGL_THROW_CONNPAY_NOT_IMPLENTED_YET;
	return false;
};


bool
pgl_connection_payload_t::makeUnmatched()
{
	PGL_THROW_CONNPAY_NOT_IMPLENTED_YET;
	return false;
};

void
pgl_connection_payload_t::setMatchedTo(
        const bool newState)
{
	(void)newState;
	PGL_THROW_CONNPAY_NOT_IMPLENTED_YET;
	return;
};


/*
 * END MATCHED
 * ==========================================
 */





/*
 * ===============================================================
 * Weight
 *
 * This property is for the weight.
 */
bool
pgl_connection_payload_t::_implements_WEIGHT() const
{
	return false;
};


void
pgl_connection_payload_t::setWeightTo(
        const Weight_t& newWeight)
{
	(void)newWeight;
	PGL_THROW_CONNPAY_NOT_IMPLENTED_YET;
	return;
};


Weight_t
pgl_connection_payload_t::getWeight() const
{
	PGL_THROW_CONNPAY_NOT_IMPLENTED_YET;

	return Weight_t(-1);
};

/*
 * END WEIGHT
 * =============================================================
 */


/*
 * ====================================
 * Begin Kleinberg Core
 */


/*
 * \brief	This function returns the the delta between the owning node and the other node to the target distance.
 *
 * This means that negative values means that the other node, are nearer towards the target node, so this is good.
 * If the ceooection is unpassable, like an incoming directed connection at the owning node an exception is generated.
 *
 * \throw	If the connection is not craoosable
 *
 * \param	ConsideredEdge
 */
Distance_t
pgl_connection_payload_t::getRoutingDistanceDelta(
		const pgl_edge_t& 	ConsideredEdge) const
{
	(void)ConsideredEdge;
	PGL_THROW_CONNPAY_NOT_IMPLENTED_YET;
	return Constants::DistanceInfinity;
};

/*
 * END Kleinberg Core
 * ===========================
 */



PGL_NS_END(graph)
PGL_NS_END(pgl)




