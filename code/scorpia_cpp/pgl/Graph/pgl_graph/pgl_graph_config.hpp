#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_CONFIG_HPP
#define PGL_GRAPH__PGL_GRAPH_CONFIG_HPP

/**
 * \file pgl_graph/pgl_graph_config.hpp
 * \brief Contains some definition that are usefull for the graph and controlling its compilation.
 *
 * This files defines some asspect of the graphs compilation.
 * It also defines some types, that belonges to the graph.
 * Like what type does a flow have?
 *
 * Note that the output is in the associated cpp file.
 *
 * \todo 	Make that the switches can be set from the outside.
 *
 */

#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
//#include <pgl_graph_colour.hpp>



//Include STD
#include <limits>


/**
 * \brief	This macro enables, that the graph can have multiple connections, with the same ID.
 * 		This essentialy makes a multigraph out of this
 * \define	PGL_GRAPH_ALLOW_MULTICONNECTION
 *
 * This macro is currently enabled unconditionaly
 */
#define PGL_GRAPH_ALLOW_MULTICONNECTIONS


/**
 * \brief	This macros enables, the the integrity checks are reduced.
 * 		This essentialy reduces the testst, that are perfromed by the test functions.
 * 		This function can also have an effect on the valid check
 *
 * This macro shall be used with caution
 *
 * \define 	PGL_GRAPH_REDUCED_INTERITY
 */
#undef PGL_GRAPH_REDUCED_INTEGRITY



/**
 * \brief	This macro allwos to disable the Payload of the graph.
 * \define	PGL_GRAPH_DISABLE_PAYLOADS
 *
 * There is only one macro for this.
 * In the future, they may be more macros to deactivate the payload of only one entity.
 * But we surrently don't need them.
 *
 * Defining this macro, will deactivate all payload function in the graph.
 * The functions, that are related to them are still present, but generate an exception if called.
 *
 * Also if if a payload is feeded to the builder, the graph will call delete on it.
 */
#undef PGL_GRAPH_DISABLE_PAYLOADS





PGL_NS_START(pgl)
PGL_NS_START(graph)


/**
 * \brief this type defines a flow
 *
 * A flow can also be negative, in some sense (mainly a theoretical construct, which ease formulation).
 * Therefor the flow is signed.
 */
using Flow_t = Int32_t;


/**
 * \brief This type define a weight
 *
 * This also can be used as cost.
 * It is signed because this way one can also express the different of two weights
 */
using Weight_t = Int32_t;


/**
 * \brief This types provides a distance
 *
 * A distance is always positive, so it is unsigned,
 * but if we whant to express a differences of distances, we need a sign, so it is still signed.
 */
using Distance_t = Int32_t;


/**
 * \brief this is a capacity
 *
 * The capacity is the maximal flow that can travel throuch a edge.
 * It is also a signed integer, to be in consistent with the flow.
 */
using Capacity_t = Int32_t;


/**
 * \brief This defines a match
 *
 * A match is some boolean quantity.
 * Here we alo defines the Matched and Unmatched variable.
 */
using Match_t = bool;


/**
 * \brief A fractional Matching
 *
 * This is a matching, where there is not Matched ore Unmatched.
 * Instead one can have, a fraction.
 */
using FractMatch_t = Float_t;


/**
 * \brief The colour class
 *
 * This is the colour.
 * An englich and an american typedef is provided, but the library uses the english one.
 */
using Colour_t = pgl_colour_t;
using Color_t  = Colour_t;

/**
 * \brief 	This namespace is used to express constants inside the graph
 */
namespace Constants
{
	/**
	 * \brief	Constant to express matiching
	 */
	static constexpr Match_t Matched = true;
	static constexpr Match_t Unmatched = false;

	/**
	 * \brief 	Constant that express a distance of infinity
	 */
	static constexpr Distance_t DistanceInfinity = std::numeric_limits<Distance_t>::max();
}; //End namespace(Constants)


PGL_NS_END(graph)
PGL_NS_END(pgl)






#endif //End include guads
