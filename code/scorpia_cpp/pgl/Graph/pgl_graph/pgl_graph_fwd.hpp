#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_FWD_HPP
#define PGL_GRAPH__PGL_GRAPH_FWD_HPP

#include <pgl_core.hpp>
#include <pgl_int.hpp>

/**
 * \file pgl_graph/pgl_graph_fwd.hpp
 * \brief Forward delclarations of the most important class in the graph
 *
 * This files provides forward delcarations of the most important parts of the graph part of pgl.
 */


PGL_NS_START(pgl)	//Start pgl namepace
PGL_NS_START(graph)


//Declaration of the vertex
class pgl_vertex_t;

//Declaration of the edge
class pgl_edge_t;

//Decalration of the vertex payload
class pgl_vertex_payload_t;

//Decalres the vertex ID
class pgl_vertexID_t;

//Declares the EdgeID
class pgl_edgeID_t;

// Declares the colour
class pgl_colour_t;

//Declaration of the connection payload
class pgl_connection_payload_t;

//Declaration of the connection
class pgl_connection_t;

//Decales the pgl Graphtag enum
enum class GraphTag : uInt16_t;

//Declaration of the graph
class pgl_graph_t;


PGL_NS_END(graph)
PGL_NS_END(pgl)





#endif //endincludeguards
