/**
 * \brief	Implements internal the functions that builds the vertex.
 * \file	pgl_graph/pgl_graph_graph_building.cpp
 *
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_utility.hpp>

#include <pgl_container/pgl_vector.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_graphtag.hpp>


//Include STL
#include <functional>
#include <memory>
#include <bitset>
#include <iterator>
#include <algorithm>


PGL_NS_START(pgl)
PGL_NS_START(graph)


pgl_vertex_t::pgl_vertex_t() = default;


pgl_vertex_t::~pgl_vertex_t() = default;


/*
 * \brief 	builder constructor
 *
 * This constructor is the meaningfull constructor.
 * It constructs a valid vertex.
 * The implementation is such, that the ownership of th epointer is ownly taken, if no throw happened before.
 *
 *
 * \param ID	The ID *this should have
 * \param kindOfNode	This is a GraphTag member that describes *this.
 *
 * \throw 	If the ID is not valid and the tag has problem, invalid argument exceptions are created.
 */
pgl_vertex_t::pgl_vertex_t(
	const pgl_vertexID_t& 	ID,
	Payload_ptr 		payload,
	const GraphTag&		kindOfNode) noexcept(false) :
  pgl_vertex_t()
{
	if(ID.isInvalid())
	{
		throw PGL_EXCEPT_InvArg("The supplied ID is not valid.");
	}
	else
	{
		this->m_id = ID;
		this->m_state.reset();
	};

	//Test if Tag describes a vertex
	if(GraphTag_isVertex(kindOfNode) == false)
	{
		throw PGL_EXCEPT_InvArg("The GraphTag does not describe a vertex.");
	};

	//Test if *this is random or original
	if(GraphTag_isRandom(kindOfNode) == true)
	{
		this->m_state.set(ce_isOriginal, false);
	}
	else
	{
		this->m_state.set(ce_isOriginal, true);
	};



#ifdef PGL_GRAPH_DISABLE_PAYLOADS
	/*
	 * If defined, then we delete the payload
	 */
	delete payload;
	payload = nullptr;
#else
	//The payload must be the nullpointer
	if(m_payload != nullptr)
	{
		throw PGL_EXCEPT_RUNTIME("The payload pointer of the vertex was not null!");
	};

	// We check if the payload is null and if not we aquire ownership of it
	if(payload != nullptr)
	{
		m_payload.reset(payload);
	};
#endif

	//Set this to the proper build
	m_state.set(ce_isPropBuild, true);
};


/*
 * \brief 	Adds a new edge to *this
 *
 * This functions takes a connection and add it to *this.
 * An edge is constructed automaticaly.
 * This must be uncomplete.
 *
 * \param newConnection		This is a pointer to a connection, that should be added to this.
 * \throw			If inconsistency
 * \return			true if success. If false you fly with warp 10 int undefined behaviour
 */
bool
pgl_vertex_t::add_connection(
	Connection_ptr newConnection) noexcept(false)
{
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_RUNTIME("Node must first made uncomplete, before one can add new edges.");
	};

	if(this->isValid() == false)
	{
		PGL_EXCEPT_RUNTIME("The node is not valid.");
	};


	if(newConnection == nullptr)
	{
		throw PGL_EXCEPT_InvArg("The supplied Pointer is a nullptr.");
	};

	if(newConnection->isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("The supplied connection is not valid.");
	};

	if(newConnection->isProperConnection() == false || newConnection->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_InvArg("The supplied connection is not proper, nor valid.");
	};


	//test if *this is realy linked with the connection
	if(newConnection->hasID(m_id) == false)
	{
		throw PGL_EXCEPT_InvArg("The supplied Connection, does not has contains *this.");
	};

	//get Some properties from the connection
	const bool isDirectedConnection = newConnection->isDirected(); 	//Test if the connection is directed
	const bool isLoopOne 		= newConnection->isLoopOne();	//Test if the connection is a loop one connection
	const bool ThisIsSRC		= newConnection->isSrc(m_id);	//test if *this is the src of this.
	const bool ThisIsDEST 		= newConnection->isDest(m_id);
	const bool EdgeIsRandom 	= newConnection->isRandom();	//Test if the connection is random


	//Test if the connection is allready inserted into *this
	//NOTE: This only checks if the pointer is there, since we can have may edge, that connects two vertices.
	for(Edge_cref e : m_neigbourhood)
	{
		if(e.getConnectionPtr() == newConnection)
		{
			throw PGL_EXCEPT_RUNTIME("Tried to insert a connection, that was allready inserted.");
		};
	}; //End for(e): Check the neigbourhood


	//Now we create the Tag we need to suply to the edge constructor
	GraphTag EdgeTag = GraphTag::Edge;

	if(isDirectedConnection == true)
	{
		EdgeTag = EdgeTag | GraphTag::Directed;
	}
	else
	{
		pgl_assert(newConnection->isUndirected() );
		EdgeTag = EdgeTag | GraphTag::Undirected;
	};

	if(isLoopOne == true)
	{
		pgl_assert(ThisIsSRC && ThisIsDEST);
		EdgeTag = EdgeTag | GraphTag::LoopOne;
	};

	if(ThisIsSRC == true)
	{
		EdgeTag = EdgeTag | GraphTag::EdgeAssocWithSrc;
	}
	else
	{
		pgl_assert(ThisIsDEST == true);
		EdgeTag = EdgeTag | GraphTag::EdgeAssocWithDest;
	};

	if(EdgeIsRandom == true)
	{
		EdgeTag = EdgeTag | GraphTag::Random;
	};

	/*
	 * Insert the edge into the container of the edges.
	 *
	 * This is not so good. It could be better if we set
	 * construct them at the correct place
	 */
	const auto SizeBefore = m_neigbourhood.size();
	m_neigbourhood.emplace_back(newConnection, this, EdgeTag);
	pgl_assert(SizeBefore + 1 == m_neigbourhood.size());

	Edge_cref newlyEdge = m_neigbourhood.back();
	pgl_assert(newlyEdge.isProperEdge());

	pgl_assert(newlyEdge.getOwningNode() == *this);


	return this->int_testEdge(newlyEdge);
}; //End adding a connection




/**
 * \brief 	This function deletes the Edges from this, that is pointed to by deleteConnection
 *
 * \param deleteConnection	This is an iterator to the connection, that should be deleted
 * 				   The iterator is modified such that it will be an enditerator.
 *
 * This function deletes the connection.
 * True if retunrnneed on success. If false is returned it was not possible to do it.
 * The egde must exists and be valid, if not, an exception is thrown.
 *
 * To work properly the edge, connectiona and the vertex must be uncomplete.
 */
bool
pgl_vertex_t::remove_conection(
	EIterator_t& 	deleteConnection) noexcept(false)
{
	if(this->m_neigbourhood.size() == 0)
	{
		throw PGL_EXCEPT_RUNTIME("*this does not store any vertices.");
	};

	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_RUNTIME("*this is complete, you can't delete an edge from a still complete vertex.");
	};

	//Test if this is realy part of *this
	if( !(
		(this->EBegin().base() <= deleteConnection.base()) &&
		(deleteConnection.base() < this->EEnd().base())
	   ))
	{
		//This is not inside the range
		throw PGL_EXCEPT_OutOfBound("The provided vertex is not in the range that *this manages.");
	};

	//Now we can be sure, that it is valid to access the edge content of the iterator.

	if(deleteConnection->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_InvArg("The provided edge is not valid");
	};

	//Test if the edge is uncomplete
	if(deleteConnection->isComplete() == true)
	{
		throw PGL_EXCEPT_RUNTIME("The ende to remove is complete, this is an error.");
	};

	if(deleteConnection->getConnectionRef().isComplete() == true)
	{
		throw PGL_EXCEPT_RUNTIME("The coresponding connection is not uncomplete.");
	};

	//Check the ID
	if(deleteConnection->getOwningNodeID() != m_id)
	{
		throw PGL_EXCEPT_RUNTIME("The edge has not the same id as *this.");
	};

	//Theck the stored pointer
	if(&(deleteConnection->getOwningNode()) != this)
	{
		throw PGL_EXCEPT_RUNTIME("The stored node, is not indentical with the this-pointer.");
	};


	//Now we can actualy delete it
	const auto sizeBefore = m_neigbourhood.size();
	this->m_neigbourhood.erase(deleteConnection.base());

	//now make ethe iterator to the end iterator
	deleteConnection = this->EEnd();


	return (sizeBefore - m_neigbourhood.size()) == 1;
}; //End remove_connection



Size_t
pgl_vertex_t::reserve(
	const Size_t 	inSize,
	const Size_t 	outSize)
{
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to reserve space for a vertex that is complete.");
	};

	return this->reserve(inSize + outSize);
};



Size_t
pgl_vertex_t::reserve(
	const Size_t  	reqSpace)
{
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to reserve space for a vertex that is complete.");
	};

	this->m_neigbourhood.reserve(reqSpace);

	return this->getCapacity();
};



Size_t
pgl_vertex_t::extentSpace(
	const Size_t 		additionalEdges)
{
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to reserve space for a vertex that is complete.");
	};

	//Do nothing if the extend is zero
	if(additionalEdges == 0)
	{
		return this->getCapacity();
	};

	return this->reserve(this->degree() + additionalEdges);
};



Size_t
pgl_vertex_t::getCapacity() const noexcept
{
	return this->m_neigbourhood.capacity();
};


PGL_NS_END(graph)
PGL_NS_END(pgl)


