#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_EDGE_HPP
#define PGL_GRAPH__PGL_GRAPH_EDGE_HPP
/**
 * \file 	pgl_graph/pgl_graph_edge.hpp
 * \brief	This file declares the edge type.
 */

//PGL-Stuff
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex_saf.hpp>

#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_connection.hpp>
#include <pgl_graph/pgl_graph_connection_base.hpp>



//STD-Stuff
//#include <memory>


PGL_NS_START(pgl)
PGL_NS_START(graph)

/**
 * \brief 	The edge type
 * \class pgl_edge_t
 *
 * In this file we made the declaration of the edge.
 * The edge is basicly a connection which knows at which end it belongs to.
 * There is always a conflict.
 * Storing some information at edge directly, and increase the size of the object or not store it there and
 * read it from the connection.
 * The reading from the connection needs a load from memeory which can be expensive, to do for trivial stuff.
 *
 * Currently we store three things at the edge:
 * 	- Pointer to the connection
 * 	- Pointer to the payload
 * 	- ID of the edge
 * Together with connection_base we inherent from.
 * This base has a size of one byte, so hopes the compiler can do a good padding.
 * So in the worst case we use up to 4 words.
 *
 * Notice, an edge stores only a raw pointer, the payload is managed by a uniquie_ptr.
 * if we whant to do it correctly, we have to use a shred_ptr, but this is nuneccessary.
 * The reason is simple, an edge should not outlive the connection, this is a funcamental assumtion.
 * So we are safe.
 *
 * The edge supports roughly the same functions as the connection does.
 * They also have the same meaning.
 * The edge also defines some more functions, that the connection can't have.
 * The edge also supports the functions to interact with the payload (But they are not yet implemented),
 * this functions are not implemented in the connection.
 *
 * The edge intorduces also implements a new concept for the first time.
 * As stated numerous times, an edge is owned by a node, this node is the node that created it.
 * This node is called "OwningNode".
 * But a connection is made of two nodes.
 * If we talk about this other node we label him as "OtherNode".
 * This other node als owns an edge to the underling connection.
 *
 * Notice that an edge can be copied, this is safe as long als the user ensure, that the edge don't outlive the
 * connection.
 * Also notice that the state of the edge is also copied but not shared.
 * So this can lead to wired situation, do it only when you know what you are doing.
 * Or pgl offewrs help in doing so.
 */
class pgl_edge_t : public pgl_connection_base_t<7>
{
	/*
	 * Some usefull typedefs
	 */
private:
	using Super_t 		= pgl_connection_base_t<7>;		//!< The base class

public:
	//Payload
	using Payload_ptr 		= pgl_connection_t::Payload_ptr;			//!< This si the const payload pointer for the user
	using Payload_cptr 		= pgl_connection_t::Payload_cptr;			//!< Pointer to a constant payload

	//Vertex
	using Vertex_ptr 		= pgl_connection_t::Payload_cptr;			//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= pgl_connection_t::Vertex_cptr;				//!< This is the pointer to a constant vertex
	using Vertex_ref		= pgl_connection_t::Vertex_ref;				//!< This is the type of a reference to a node
	using Vertex_cref		= pgl_connection_t::Vertex_cref;			//!< This is the constant reference to a vertex

	//Connection
	using Connection_ptr		= pgl_connection_t*;					//!< Pointer to a connection
	using Connection_cptr		= const pgl_connection_t*;				//!< Pointer to a constant connection
	using Connection_ref 		= pgl_connection_t&;					//!< Reference to connection
	using Connection_cref		= const pgl_connection_t&;				//!< Const reference to connection


	/*
	 * Constructor, the folowing defines the consrtructors
	 */

	/**
	 * \brief 	Default constructor
	 *
	 * The default constructor is the defaulted one.
	 * It will result in an invalid edge.
	 */
	inline
	pgl_edge_t() noexcept = default;


	/**
	 * \brief 	Copy-Constructor
	 *
	 * The copyconstructor is also the default one, since we want to compy the addrersses.
	 */
	inline
	pgl_edge_t(
		const pgl_edge_t&) noexcept = default;


#if 1
	/**
	 * \brief 	Move-Constructor
	 *
	 * The move assignment calls internaly the copy assignment.
	 */
	inline
	pgl_edge_t(
		pgl_edge_t&& o) noexcept :
	  pgl_edge_t(o) 	//Call the copy constructor
	{
		pgl_assert(o.m_id == this->m_id);
		pgl_assert(o.m_connPtr == this->m_connPtr);
	};
#endif


	/**
	 * \brief 	Copy-Assignment
	 *
	 * Is also defaulted
	 */
	inline
	pgl_edge_t&
	operator= (
		const pgl_edge_t&) noexcept = default;

#if 1

	/**
	 * \brief 	Move-Assignemnt
	 *
	 * The Move assignment calls internaly the copy assignment operator
	 */
	inline
	pgl_edge_t&
	operator= (
		pgl_edge_t&& o) noexcept
	{
		this->operator=(o);	//o is a rvalue reference but here it is an lvalue reference

		return *this;
	};
#endif
	/**
	 * \brief 	Destructor
	 *
	 * Is also defaulted
	 */
	inline
	~pgl_edge_t() noexcept = default;



	/**
	 * \brief 	Build-Consdtructor
	 *
	 * This constructor is used to create an useable edge.
	 * It takes the parameter:
	 *
	 * \param baseConn	A pointer to the underlying connection
	 * \param owningNode	A Pointer to the vertex, that owns this connection
	 * \param kindOfEdge	A value of type GraphTag, that describes the connection
	 * 			This description must be complete.
	 *
	 * If the construictor is called with the wrong arguments, then an exception of type invalid argument is trown.
	 */
	pgl_edge_t(
		Connection_ptr 	baseConn,
		Vertex_cptr 	boundTo,
		const GraphTag& kindOfEdge) noexcept(false);


	/*
	 * =======================================
	 * Managin Functions
	 */
	/**
	 * \brief 	Managing functions
	 *
	 * The folowing functions are fucntions that are used for the managment.
	 * They are not inlined, sicne they are not called often (or I don't expect that)
	 */


	/**
	 * \brief 	Make this complete.
	 *
	 * \param Tag 	Tag must be GraphTag::Complete, if Tag dont compare equal an error occured
	 *
	 * This function makes *this complete, before its completinon it calls the completion functions of the underlying connection.
	 * If this function called twice, there shall be no effect.
	 * The Tag is only for safty.
	 *
	 * If it is not possible to complete, this then a runtime_exception is thrown,
	 */
	void
	makeComplete(
		const GraphTag& Tag) noexcept(false);


	/**
	 * \brief 	Make *this uncoplete
	 *
	 * \param Tag	Tag must be equal GraphTag::Uncomplete, if not an error occured
	 *
	 *
	 * This fucntion makes *this uncompleted.
	 * After the edge uncompleted itself it shall uncomplete the underling edge.
	 * The edge shall not assume that the connection is complete, when it begin its uncopletion.
	 * If this function is called twice, there shall be no effect.
	 */
	void
	makeUncomplete(
		const GraphTag& Tag);


	/*
	 * ==========================================================
	 * Accesserofunctions
	 *
	 * They are mainly identical to the functions that are offered by the connection
	 */


	/**
	 * \brief 	Returns the ID of the node where *this originates
	 * 		If *this is a undirected connection then one of the edgepoints is returned.
	 * 		This function does NOT return the id to which *this is bound.
	 */
	inline
	pgl_vertexID_t
	getSrcID() const noexcept
	{
		/*
		 * Since we made the ID inteligend this is everything we have to do, the edgeID makes the right thing.
		 * Isn't this nice?
		 */
		return m_id.getSrcID();
	};


	/**
	 * \brief 	Get The ID of the node wehere *this ends
	 * 		If *this is a undirected connection then one of the edgepoints is returned.
	 * 		This function does NOT return the id to which *this is bound.
	 */
	inline
	pgl_vertexID_t
	getDestID() const noexcept
	{
		return m_id.getDestID();
	};


	/**
	 * \brief 	Retrun the edgeID of *this
	 */
	inline
	pgl_edgeID_t
	getEdgeID() const noexcept
	{
		return m_id;
	};


	/**
	 * \brief 	Decompose the edgeID
	 *
	 * This returns rthe decomposed ID. This is a pair<vertexID, vertexID>.
	 * If the connection is a directed connection, then first is the srcID and second the destID.
	 * If *this is not directed the ordering is arbitrary (you can generally expect that first is the smaller ID),
	 */
	inline
	std::pair<pgl_vertexID_t, pgl_vertexID_t>
	decomposeID() const
	{
		return m_id.decomposeID();
	};


	/**
	 * \brief 	Returns true if node is part of the connection.
	 * \param nodeID The ID of the node one want to test.
	 * \pre 	nodeID is valid
	 *
	 * In the end this function tests if either src or dest is equal the presented id
	 */
	inline
	bool
	hasID(
		const pgl_vertexID_t& nodeID) const
	{
		pgl_assert(nodeID.isValid() == true);

		const auto IDs = m_id.decomposeID();

		if(IDs.first == nodeID || IDs.second == nodeID)
		{
			return true;
		}
		else
		{
			return false;
		};
	};


	/**
	 * \brief 	thest if ID is the src of *this
	 * \param ID	An ID
	 */
	inline
	bool
	isSrc(
		const pgl_vertexID_t& ID) const
	{
		return ID == m_id.getSrcID();
	};


	/**
	 * \brief 	Tests if ID is the dest ID of *this
	 *\param ID 	An ID
	 */
	inline
	bool
	isDest(
		const pgl_vertexID_t& ID) const
	{
		return ID == m_id.getDestID();
	};




	/**
	 * \brief 	This function test if *this is valid.
	 * 		The test is not very intense.
	 *
	 * If you search for a deep and complete test use the checkIntegrity function.
	 * It is important to note, that this function does not tests if the vertices are valid.
	 */
	inline
	bool
	isValid() const
	{
		return this->isProperBuild() 				//Test if proper build
			&& this->m_id.isValid() 			//Test if iD is Valid
			&& this->m_connPtr != nullptr 			//Test the connection pointer
			&& this->m_connPtr->isValid();			//Test if the connection is valid
	};


	/**
	 * \brief 	This function performs a deep and potentialy costly check of the integrity of *this.
	 *
	 * There was once a the function 'isValidEdge' this function is depricated and deleted, this
	 * because to reduce confusion.
	 * This fucntion is intendet to be use for a deep test, if you whant to make sure, that all is okay.
	 */
	bool
	checkIntegrity() const;

	bool
	isValidEdge() const = delete;


	/**
	 * \brief 	This function tests if the underlining connection is propper builded.
	 */
	inline
	bool
	isProperEdge() const
	{
		return this->isProperBuild() && m_id.isValid();
	};



	/**
	 * \brief	This function retruns a (non-const) reference to the src vertex
	 */
	inline
	Vertex_ref
	getSrcNode()
	{
		return this->m_connPtr->getSrcNode();
	};


	/**
	 * \brief 	This function returns a const reference to the src node
	 */
	inline
	Vertex_cref
	getSrcNode() const
	{
		return this->m_connPtr->getSrcNode();
	};


	/**
	 * \brief	Get a const reference to the src node (explicit)
	 */
	inline
	Vertex_cref
	getCSrcNode() const
	{
		return this->m_connPtr->getCSrcNode();
	};


	/**
	 * \breif 	Get a (non-const) reference ot the dest node
	 */
	inline
	Vertex_ref
	getDestNode()
	{
		return this->m_connPtr->getDestNode();
	};


	/**
	 * \brief 	Get a const reference to the dest node
	 */
	inline
	Vertex_cref
	getDestNode() const
	{
		return this->getCDestNode();
	};


	/**
	 * \brief 	Get a const reference to the dest node (explicit)
	 */
	inline
	Vertex_cref
	getCDestNode() const
	{
		return this->m_connPtr->getCDestNode();
	};


	/*
	 * EDGE ONLY Functions
	 *
	 * The folowing functions are for the edge.
	 * They take into account that that the edge is bound/owned by a node
	 */

	/**
	 * \brief	Return the ID of the owning node
	 *
	 * This returns the ID of the owning node.
	 * This is the node on which *this was created.
	 */
	inline
	pgl_vertexID_t
	getOwningNodeID() const noexcept
	{
		if(this->isOrininAtSrc() == true)
		{
			return this->getSrcID();
		}
		else
		{
			pgl_assert(this->isOrininAtDest() == true);
			return this->getDestID();
		};

		pgl_assert(false);
		return pgl_vertexID_t();
	};


	/**
	 * \breif 	Returns a reference to the owning node
	 */
	inline
	Vertex_ref
	getOwningNode() noexcept
	{
		if(this->isOrininAtSrc() == true)
		{
			return this->getSrcNode();
		}
		else
		{
			pgl_assert(this->isOrininAtDest() == true);
			return this->getDestNode();
		};
		pgl_assert(false);
	};


	/**
	 * \breif 	Returns a const reference to the owning node
	 */
	inline
	Vertex_cref
	getOwningNode() const noexcept
	{
		if(this->isOrininAtSrc() == true)
		{
			return this->getSrcNode();
		}
		else
		{
			pgl_assert(this->isOrininAtDest() == true);
			return this->getDestNode();
		};
		pgl_assert(false);
	};

	inline
	Vertex_cref
	getCOwningNode() const noexcept
	{
		return this->getOwningNode();
	};



	/**
	 * \brief 	returns the ID of the node, the other node (not owning node)
	 *
	 * The other node is the other node that is involveed in this connection.
	 * This is not necessaraly the node, to which this connection leads.
	 * It is simply the bnode of the connection that is not *this and also involved in this connection.
	 *
	 * If *this is a loop one connection, then ofcorse it is *this.
	 */
	inline
	pgl_vertexID_t
	getOtherNodeID() const noexcept
	{
		if(this->isOrininAtSrc() == true)
		{
			return this->getDestID();
		}
		else
		{
			pgl_assert(this->isOrininAtDest() == true);
			return this->getSrcID();
		};

		pgl_assert(false);
		return pgl_vertexID_t();
	};


	/**
	 * \brief 	Retruns a reference to the other node of *this
	 */
	inline
	Vertex_ref
	getOtherNode() noexcept
	{
		if(this->isOrininAtSrc() == true)
		{
			return this->getDestNode();
		}
		else
		{
			pgl_assert(this->isOrininAtDest() == true);
			return this->getSrcNode();
		};
	};


	/**
	 * \brief 	Retruns a const reference to the other node of *this
	 */
	inline
	Vertex_cref
	getOtherNode() const noexcept
	{
		if(this->isOrininAtSrc() == true)
		{
			return this->getDestNode();
		}
		else
		{
			pgl_assert(this->isOrininAtDest() == true);
			return this->getSrcNode();
		};
	};


	inline
	Vertex_cref
	getCOtherNode() const noexcept
	{
		return this->getOtherNode();
	};



	/**
	 * \brief 	This function returns true, if *this represents an incoming connection
	 */
	inline
	bool
	isIncoming() const noexcept
	{
		return this->Super_t::isIncomingEdge();
	};

	/**
	 * \brief 	This function returns true if *this represents an outgoing connection
	 */
	inline
	bool
	isLeaving() const noexcept
	{
		return this->isLeavingEdge();
	};


	/**
	 * \brief 	Return a reference to the underling connection
	 */
	inline
	Connection_ref
	getConnectionRef() noexcept
	{
		pgl_assert(this->isProperEdge());

		return *m_connPtr;
	};

	inline
	Connection_cref
	getConnectionRef() const noexcept
	{
		pgl_assert(this->isProperEdge());

		return *m_connPtr;
	};

	inline
	Connection_cref
	getConnectionCRef() const noexcept
	{
		pgl_assert(this->isProperEdge());

		return *m_connPtr;
	};

	/**
	 * \brief returns a pointer to the underling connection
	 */
	inline
	Connection_ptr
	getConnectionPtr() noexcept
	{
		pgl_assert(this->isProperEdge());

		return m_connPtr;
	};

	inline
	Connection_cptr
	getConnectionPtr() const noexcept
	{
		pgl_assert(this->isProperEdge());

		return m_connPtr;
	};

	inline
	Connection_cptr
	getConnectionCPtr() const noexcept
	{
		pgl_assert(this->isProperEdge());

		return m_connPtr;
	};


	/*
	 * ==============================
	 * Payload access Fucntions
	 */

	inline
	bool
	hasPayload() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return false;
#else
		return m_payloadPtr != nullptr;
#endif
	};


	inline
	Payload_ptr
	getPayload()
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return m_payloadPtr;
#endif
	};

	inline
	Payload_cptr
	getPayload() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return m_payloadPtr;
#endif
	};


	inline
	Payload_cptr
	getCPayload() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return m_payloadPtr;
#endif
	};



	/**
	 * \brief	This function returns the routing distance of this
	 *
	 * This function calls the payload function that is used to calculate the distance difference.
	 * rememeber that the distance is negative.
	 * Also if the fucntion returns a negative distance this means that other node is nearer at the target tan the owning node
	 */
	inline
	Distance_t
	getRoutingDistanceDelta() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("The Payload is decativated in the edge.");
#else
		pgl_assert(m_payloadPtr);
		pgl_assert(this->isValid() );
		return this->m_payloadPtr->getRoutingDistanceDelta(*this);
#endif
	};


	/*
	 * ====================================
	 * Printing finctions
	 */

	/**
	 * \brief 	Prints topological informations.
	 *
	 * This function is meant to be an extension to print_topo function from the vertex.
	 */
	std::string
	print_topo() const;

	/*
	 * ===============================================
	 * Comparision operators
	 *
	 * They use the id to make the comparisions.
	 */

	/**
	 * \brief 	Returns true if the ID of *this is considered less than other's ID
	 */
	inline
	bool
	operator< (
		const pgl_edge_t& rhs) const noexcept
	{
		return this->m_id < rhs.m_id;
	};

	/**
	 * \brief 	returns true if the ID of *this and the ID of rhs are equal
	 */
	inline
	bool
	operator== (
		const pgl_edge_t& rhs) const noexcept
	{
		return this->m_id == rhs.m_id;
	};

	/**
	 * \brief 	returns true if unequal
	 */
	inline
	bool
	operator!= (
		const pgl_edge_t& rhs) const noexcept
	{
		return !(this->operator==(rhs));
	};

	/**
	 * \brief 	Less than with id type as rhs
	 */
	inline
	bool
	operator< (
		const pgl_edgeID_t& rhs) const noexcept
	{
		return this->m_id < rhs;
	};

	friend
	inline
	bool
	operator<(
		const pgl_edgeID_t& lhs,
		const pgl_edge_t& rhs) noexcept;


	/**
	 * \brief	Equal with id type as rhs
	 */
	inline
	bool
	operator== (
		const pgl_edgeID_t& rhs) const noexcept
	{
		return this->m_id == rhs;
	};

	/**
	 * \brief 	Unequal with id type as rhs
	 */
	inline
	bool
	operator!= (
		const pgl_edgeID_t& rhs) const noexcept
	{
		return !(this->operator==(rhs));
	};



	/*
	 * Private memeber
	 */
private:
	Connection_ptr 	m_connPtr;		//!< Pointer to the underling connection
#ifndef PGL_GRAPH_DISABLE_PAYLOADS
	Payload_ptr 	m_payloadPtr;		//!< Pointer to the payload
#endif
	pgl_edgeID_t 	m_id;			//!< The ID of the edge
};


inline
bool
operator<(
	const pgl_edgeID_t& lhs,
	const pgl_edge_t& rhs) noexcept
{
	return lhs < rhs.m_id;
};

inline
bool
operator== (
	const pgl_edgeID_t& lhs,
	const pgl_edge_t& rhs) noexcept
{
	return rhs == lhs;
};

inline
bool
operator!= (
	const pgl_edgeID_t& lhs,
	const pgl_edge_t& rhs) noexcept
{
	return rhs != lhs;
};


PGL_NS_END(graph)
PGL_NS_END(pgl)


#endif //end include guard
