/**
 * \file	pgl_graph/pgl_graph_graph_constructor.cpp
 * \brief 	Defines some functions of the graph
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_utility.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>

#include <pgl_graph/pgl_graph_graphtag.hpp>

#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>

//#include <pgl_container/pgl_small_vector.hpp>
#include <pgl_container/pgl_vector.hpp>

//Include STL
#include <functional>
#include <memory>
#include <bitset>
#include <iterator>
#include <algorithm>


PGL_NS_START(pgl)
PGL_NS_START(graph)



pgl_graph_t::pgl_graph_t():
  m_vertices(),
  m_connections(),
  m_nRandVertices(0),
  m_nRandConnections(0),
  m_isComplete(false),
  m_revIndex()
{};


pgl_graph_t::pgl_graph_t(
	pgl_abstract_gBuilder_t&	Builder) noexcept(false) :
  pgl_graph_t()		//Calling the default constructor
{
	this->build_graph_from_builder(Builder);
};


/*
 * \brief 	This function uses a builder to build a graph
 *
 * For that the graph must be empty, meaning that it was default constructed,
 * or the clean function was called.
 * The graph must have no vertices nor anyrthing else.
 * For the sake of conformace, the graph must be uncomplete.
 *
 * \param Builder	A builder used to create the graph.
 *
 * \note	Is defined in the constructor file
 */
void
pgl_graph_t::build_graph_from_builder(
	pgl_abstract_gBuilder_t&	Builder) noexcept(false)
{
	if(this->isEmpty() == false)
	{
		throw PGL_EXCEPT_LOGIC("The graph was not empty.");
	};

	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("The graph was complete, when tring to build it.");
	};

	//Call the run function of the builder
	Builder.createBuilder();




	const Size_t n = Builder.nVertices();
	if(pgl_vertexID_t::CAN_BE_AN_ID(n) == false)
	{
		throw PGL_EXCEPT_InvArg("To many Vertices to create.");
	};


	//Allocate space
	m_vertices.reserve(n + 3);



	//Creating all the nodes that are neccessary
	for(Size_t node_it = 0; node_it != n; ++node_it)
	{
		pgl_abstract_gBuilder_t::NodeDescriptor_t nodeToBuild = Builder.getNode(node_it);
		pgl_assert(nodeToBuild.isValid() );

		//Use the internal function to create the node
		auto newlyCreatedVertex = this->prot_createNewNode(nodeToBuild.node_id, nodeToBuild.kindOfNode, nodeToBuild.payload_ptr);

		/*
		 * The construction came this far, tis means that the vertex has aquiered ownership over the payload.
		 * This means that we must inform the builder now, and not after the validy check, because then it could result in a double free error.
		 *
		 * So inform the builder about the new ownership.
		 */
		Builder.completedNode(node_it);


		//Check if the node is valid
		if(newlyCreatedVertex->isValid() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The newly created vertex is not valid.");
		};

		//Make reserve space if possible, for the proximity of the vertex
		Size_t 	inDeg = 0,
			outDeg = 0;
		std::tie(inDeg, outDeg) = Builder.getNodeDegree(node_it);
		if((inDeg + outDeg) != 0)	// TEst if space is required
		{
			//We have something to allocate
			newlyCreatedVertex->reserve(inDeg, outDeg);
		};
	}; //End for(node_it): Creating all the nodes


	//Test if Vertices are still sorted
	if(!std::is_sorted(m_vertices.cbegin(), m_vertices.cend()))
	{
		throw PGL_EXCEPT_LOGIC("There was an error in the inerting process.");
	};


	//Now creating the connections
	const Size_t m = Builder.nConnections();

	for(Size_t conn_it = 0; conn_it != m; ++conn_it)
	{
		pgl_abstract_gBuilder_t::ConnDescribtor_t connToBuild = Builder.getConnection(conn_it);
		pgl_assert(connToBuild.isValid() );

		auto newlyCreatedConnection = this->createNewConnection(
				connToBuild.src_id,
				connToBuild.dest_id,
				connToBuild.kindOfConn,
				connToBuild.payload_ptr).base();

		/*
		 * Same reason as for the node above.
		 * We have toinform the builder now that we have taken ownership of the payload.
		 */
		Builder.completedConnection(conn_it);

		//Test if the connection is valid
		if(newlyCreatedConnection->second.isValid() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The newly created connection is invalid.");
		};


		/*
		 * Make some furter tests
		 */
		const pgl_connection_t* newConnectionPointer = &(newlyCreatedConnection->second);

		const bool inSrcInserted = this->getNode(connToBuild.src_id)->count_if(
			[newConnectionPointer](Edge_cref e) -> bool { return e.getConnectionCPtr() == newConnectionPointer; }) == 1;
		if(inSrcInserted == false)
		{
			throw PGL_EXCEPT_RUNTIME("The connection was not inserted into src.");
		};

		const bool isDestInserted = this->getNode(connToBuild.dest_id)->count_if(
			[newConnectionPointer](Edge_cref e) -> bool { return e.getConnectionCPtr() == newConnectionPointer; }) == 1;

		if(isDestInserted == false)
		{
			throw PGL_EXCEPT_RUNTIME("The connection was not inserted inot dest.");
		};
	}; //End for(conn_it): Creating all the connections


	/*
	 * Test the integrity of the graph
	 */
#ifndef PGL_NDEBUG
	if(this->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The Graph didn't pass the integrity check.");
	};
#endif


	/*
	 * Now the graph pass itsepf to the postprocessing
	 */
	Builder.postProcessing(*this);


#ifndef PGL_NDEBUG
	if(this->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_RUNTIME("The Graph didn't pass the integrity check.");
	};
#endif


	/*
	 * Now the graph is finisched building and call the clear function of the builder
	 */
	Builder.clearBuilder();

	return;
};




void
pgl_graph_t::add_additional_connections(
	const ConnDescrContainer_t& 	newConnections) noexcept(false)
{
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to extend a complete graph.");
	};

	//Make an array, that stores how many new edges each node has
	pgl::pgl_vector_t<Size_t> newConnectionCount(this->nVertices(), 0);

	//Iterate through all the descriotions to get get the numbers of new edges for eac vertex
	for(const ConnDescription_t& it : newConnections)
	{
		newConnectionCount.at(it.src_id.getID()) 	+= 1;
		newConnectionCount.at(it.dest_id.getID()) 	+= 1;
	}; //End for(it)

	//Now allocate new space
	const Size_t nNodes = this->nVertices();
	for(Size_t it = 0; it != nNodes; ++it)
	{
		m_vertices.at(it).reserve(newConnectionCount.at(it));
	}; //End for(if)


	//Now we can use it to insert the new connections
	for(const ConnDescription_t& it : newConnections)
	{
		//Add a new connection and ignoring the return type
		const auto newConnection = this->createNewConnection(it.src_id, it.dest_id, it.kindOfConn, it.payload_ptr);

		//Test if new connection is valid
		if(newConnection->isValid() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The newly created connection is not valid.");
		};
	}; //End for(it)

	return;
};












PGL_NS_END(graph)
PGL_NS_END(pgl)


