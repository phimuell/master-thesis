/**
 * \file	pgl_graph/pgl_graph_edgeID.cpp
 * \brief 	This file defines some memberfunctions  of the pgl::graph::pgl_edgeID_t
 *
 */

#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_graphtag.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>


PGL_NS_START(pgl)	//Start pgl namepace
PGL_NS_START(graph)


pgl_edgeID_t::StateFiled
pgl_edgeID_t::convert_from_GraphTag(const GraphTag& gt)
{

	if(GraphTag_isEdge(gt) || GraphTag_isConnection(gt))
	{
		if(GraphTag_isDirectedEdge(gt) == true)
		{
			return StateFiled::DirectedState;
		}
		else if(GraphTag_isUndirectedEdge(gt) == true)
		{
			return StateFiled::UndirectedState;
		}
		else
		{
			pgl_assert(false);
			return StateFiled::IllegalState;
		};

	}
	else
	{
		pgl_assert(false && "Not an edge/connection");
		return StateFiled::IllegalState;
	};
};




pgl_edgeID_t::StateFiled
convert_from_GraphTag(const GraphTag& tag)
{
	return pgl_edgeID_t::convert_from_GraphTag(tag);
};



PGL_NS_END(graph)
PGL_NS_END(pgl)



