/**
 * \file	pgl_graph/pgl_graph_edge.cpp
 * \brief 	Contains the implementation for the edge.
 */




//PGL-Stuff
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graphtag.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex_saf.hpp>

#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_graph/pgl_graph_connection.hpp>
#include <pgl_graph/pgl_graph_connection_base.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>



//STD-Stuff
//#include <memory>


PGL_NS_START(pgl)
PGL_NS_START(graph)





pgl_edge_t::pgl_edge_t(
	Connection_ptr 	baseConn,
	Vertex_cptr 	owningNode,
	const GraphTag& kindOfEdge) noexcept(false) :
  Super_t(true),	//We construct a proper connection without knowing if this is true
  m_connPtr(nullptr),
#ifndef PGL_GRAPH_DISABLE_PAYLOADS
  m_payloadPtr(nullptr),
#endif
  m_id()
{
	//Get the pointer to the underling connection
	//And if it is valid also the pointer to its payload (can be nullprt)
	if(baseConn == nullptr)
	{
		throw PGL_EXCEPT_InvArg("Supplied a nullptr as connection to the edge constructor.");
	}
	else
	{
		/*
		 * Here we must not delete the pointer.
		 * Becase, the edge only has something that one could describe as weak pointer.
		 */
		m_connPtr = baseConn;
#ifndef PGL_GRAPH_DISABLE_PAYLOADS
		m_payloadPtr = m_connPtr->getPayload();		//Remember: Can be null
#endif
	};

	if( !(GraphTag_isEdge(kindOfEdge) || GraphTag_isConnection(kindOfEdge)) )
	{
		throw PGL_EXCEPT_InvArg("The provided tag, doesn't describe an Edge.");
	};

	if(m_connPtr->isProperConnection() == false)
	{
		throw PGL_EXCEPT_InvArg("The supplied connection is not proper.");
	};

	if(m_connPtr->getEdgeID().isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("The connection has an invalid ID.");
	}

	//Compy the edge ID
	m_id = m_connPtr->getEdgeID();

	//Process the owning node
	if(owningNode == nullptr)
	{
		throw PGL_EXCEPT_InvArg("Supplied a nullptr as owning node to the edge constructor.");
	}

	pgl_assert(this->isProperBuild());

	if((kindOfEdge & GraphTag::Edge) != GraphTag::Edge)
	{
		throw PGL_EXCEPT_InvArg("The supplied GraphTag is not a vertex.");
	};

	// Now setting the state
	if(GraphTag_isDirectedEdge(kindOfEdge) == true)
	{
		pgl_assert(m_connPtr->isDirected());

		//It is a directed connection
		this->Super_t::prot_setConnectionState(Super_t::ConnectionState::Directed);


		// The edge is owned by the src
		if((kindOfEdge & GraphTag::EdgeAssocWithSrc) == GraphTag::EdgeAssocWithSrc)
		{
			if(pgl_vertexSAF_getID(owningNode) != baseConn->getSrcID())
			{
				throw PGL_EXCEPT_InvArg("The supplied GraphTag has wrong informations about the owning state. It claims that *this is owned by the src, but the ID of the supplied node, diesent agree with the connections ID");
			}


			this->Super_t::prot_setOriState(Super_t::ConnectionState::EdgeOriginSRC);
			this->Super_t::prot_setPointingState(Super_t::ConnectionState::isLeavingEdge);
		}
		else if((kindOfEdge & GraphTag::EdgeAssocWithDest) == GraphTag::EdgeAssocWithDest)
		{
			//THe edge is owned by the dest
			if(pgl_vertexSAF_getID(owningNode) != baseConn->getDestID())
			{
				throw PGL_EXCEPT_InvArg("The supplied Graphtag has wrong information about the owning state."
							"It claims that *this is owned by the srcm but the ID of the supplied node"
							" didn't agree with the id of the edge.");
			};

			this->Super_t::prot_setOriState(Super_t::ConnectionState::EdgeOriginDEST);
			this->Super_t::prot_setPointingState(Super_t::ConnectionState::isIncomingEdge);
		}
		else
		{
			pgl_assert(false);
		};
	}
	else if(GraphTag_isUndirectedEdge(kindOfEdge) == true)
	{
		pgl_assert(this->isUndirected() );

		this->Super_t::prot_setConnectionState(Super_t::ConnectionState::Undirected);
		this->Super_t::prot_setPointingState(Super_t::ConnectionState::isIncomingEdge);
		this->Super_t::prot_setPointingState(Super_t::ConnectionState::isLeavingEdge);

		/*
		 * We must set, the state to which we are thight.
		 */
		if((kindOfEdge & GraphTag::EdgeAssocWithSrc) == GraphTag::EdgeAssocWithSrc)
		{
			this->Super_t::prot_setOriState(Super_t::ConnectionState::EdgeOriginSRC);
		}
		else if((kindOfEdge & GraphTag::EdgeAssocWithDest) == GraphTag::EdgeAssocWithDest)
		{
			this->Super_t::prot_setOriState(Super_t::ConnectionState::EdgeOriginDEST);
		}
		else
		{
			pgl_assert(false && "No bounding informations.");
		};
	}
	else
	{
		throw PGL_EXCEPT_InvArg("GraphTag didn't contain information avbout the direction of *this");
	};


	//Now the loop one
	if(GraphTag_isLoopOne(kindOfEdge) == true)
	{
		this->prot_setLoopOne(Super_t::ConnectionState::isLoopOne);
	};

	//Process the Random state
	if(GraphTag_isRandom(kindOfEdge) == true)
	{
		this->prot_setOriginalState(Super_t::ConnectionState::RandomConn);
	}
	else
	{
		this->prot_setOriginalState(Super_t::ConnectionState::OriginalConn);
	};


	//Make this uncomplete
	this->prot_setCompleteState(Super_t::ConnectionState::NotComplete);

	//Test if the seting with the owning piinter succeeded
	pgl_assert(&(this->getOwningNode()) == owningNode);


	if(this->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_InvArg("Completion of the edge was unsuccessfull. isValidEdge fails.");
	};
};



void
pgl_edge_t::makeComplete(
	const GraphTag& Tag) noexcept(false)
{

	if(this->isValid() == false)
	{
		throw PGL_EXCEPT_RUNTIME("Tried to complete an invalid edge.");
	};

	if(Tag == GraphTag::Complete)
	{
		if(m_id.isInvalid() || (!m_id.isLegal()))
		{
			throw PGL_EXCEPT_RUNTIME("Tried to complete an edge with invalid ID.");
		};

		/*
		 * Make the underling connection complete
		 */
		this->m_connPtr->makeComplete(GraphTag::Complete);

		if(this->m_connPtr->isComplete() == false)
		{
			throw PGL_EXCEPT_RUNTIME("Called the complete function of the underling connection, but connection was not complete afterwards.");
		};



		/*
		 * MAKE *this complete
		 */
		this->Super_t::prot_setCompleteState(Super_t::ConnectionState::Complete);

		return;
	}
	else
	{
		throw PGL_EXCEPT_InvArg("Called the makeComplete function of an connection with an argument not equal to GrapgTag::Complete");
	};

	return;
};


void
pgl_edge_t::makeUncomplete(
	const GraphTag& Tag)
{
	if(this->isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to incomplete an invalid edge. How does this happens in the first place?");
	};


	//Test if this is allready uncomplete
	if(this->isComplete() == false)
	{
		return;
	};


	if(Tag == GraphTag::Uncomplete)
	{
		/*
		 * Make the underling connection uncomplete
		 */
		this->m_connPtr->makeUncomplete(GraphTag::Uncomplete);

		// We don't perform a test to see if the connection was uncompleted, successfully since it is not important

		/*
		 * Make *this uncompletete
		 */
		this->Super_t::prot_setCompleteState(Super_t::ConnectionState::NotComplete);
		return;
	}
	else
	{
		throw PGL_EXCEPT_InvArg("Called the makeUncomplete function of an edge with an argument not equal to GrapgTag::Uncomplete");
	};
};



bool
pgl_edge_t::checkIntegrity() const
{
	bool result = this->isValid();

#ifndef PGL_GRAPH_REDUCED_INTEGRITY
	if(result == false)
	{
		throw PGL_EXCEPT_LOGIC("Edge didn,t pass the isValid check in its integrity check.");
	}

	//Make the interity check of the underlying connection
	result &= this->m_connPtr->checkIntegrity();
	pgl_assert(result);

	//Check the id
	result = result && this->m_id.isLegal() && this->m_id.isValid();
	result = result && this->m_id == this->m_connPtr->getEdgeID();
	pgl_assert(result);

	//check the sync state
	result = result && this->isDirected() == this->m_connPtr->isDirected();
	pgl_assert(result);

	result = result && this->isOriginalConnection() == this->m_connPtr->isOriginalConnection();
	pgl_assert(result);

	result = result && this->isLoopOne() == this->m_connPtr->isLoopOne();
	pgl_assert(result);


	// We don't check the complete state

	result = result && this->Super_t::isProperBuild();
	pgl_assert(result);

	if(this->isDirected() == true)
	{
		result = result && m_id.isDirected() == true;
		pgl_assert(result);

		if(this->isOrininAtSrc() == true)
		{
			result = result && (m_id.getSrcID() == this->m_connPtr->getSrcID());
			pgl_assert(result);
		}
		else if(this->isOrininAtDest() == true)
		{
			result = result && (m_id.getDestID() == this->m_connPtr->getDestID());
			pgl_assert(result);
		}
		else
		{
			throw PGL_EXCEPT_LOGIC("The connection is directed but didn't save at which ID it started.");
			//result &= false;
		};
	}
	else
	{
		result = result && m_id.isUndirected() == true;
		pgl_assert(result);
	};

#endif
	return result;
};






PGL_NS_END(grpah)
PGL_NS_END(pgl)




