/**
 * \file	pgl_graph/pgl_graph_graph_prionting.cpp
 *
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_utility.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_graphtag.hpp>

#include <pgl_container/pgl_vector.hpp>

//Include STL
#include <functional>
#include <memory>
#include <bitset>
#include <iterator>
#include <algorithm>
#include <string>
#include <sstream>

PGL_NS_START(pgl)
PGL_NS_START(graph)


std::string
pgl_graph_t::print_topo() const
{
	std::stringstream s;

	for(auto v = this->NodeBegin(); v != NodeEnd(); ++v)
	{
		s << v->print_topo() << "\n\n";
	};


	return s.str();
};





PGL_NS_END(graph)
PGL_NS_END(pgl)


