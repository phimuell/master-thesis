#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_GRAPHTAG_HPP
#define PGL_GRAPH__PGL_GRAPH_GRAPHTAG_HPP
/**
 * \brief 	This file declares a scoped enum that names graph components
 * \file 	pgl_graph/pgl_graph_graphtag.hpp
 *
 * This files declares a scoped enum GraphTag, that allows to name some concepts
 * There are also operator overloads for them (most commen one).
 */

#include <pgl_core.hpp>
#include <pgl_int.hpp>

#include <pgl_graph/pgl_graph_config.hpp>



PGL_NS_START(pgl)
PGL_NS_START(graph)





/**
 * \brief 	The enum GraphTags defines some tags, that are usefull in the graph.
 * \enum 	GraphTag
 *
 * This scoped enum is used for varius things inside the Graph.
 * It NAMES some concept of the graph, like edge.
 * Previously, some classes defines their own such tags.
 * This is fine as lng as this is internal.
 *
 * If other enums are used in the library, aka. existing code, then there must be conversions operators.
 * One can use the them internaly without restrictions.
 * But If user code needs them, there must be means to use these ones.
 * The signature of the conversion is:
 *
 *
 * //Converts from GraphTag to OtherEnum
 * constexpr
 * OtherEnum
 * convert_from_GraphTag(const GraphTag&)
 *
 * //Converts from OtherEnum to GraphTag
 * constexpr
 * GraphTag
 * convert_to_GraphTag(const OtherEnum&)
 *
 *
 * An object is considered as original, if it is NOT Random.
 *
 */
enum class GraphTag : uInt16_t
{
	/// Basic types
	Unknown 	= 0,			//!< Unknown, indicates an error, manly for completness
	Edge 		= 1 << 0,		//!< An edge
	NonEdge 	= static_cast<uInt16_t>(~Edge), 		//!< Not an edge, but everything else
	Vertex 		= 1 << 1,		//!< A vertex
	NonVertex 	= static_cast<uInt16_t>(~Vertex), 		//!< Everything except a vertex
	Node 		= Vertex,		//!< An alias of Vertex
	Connection 	= 1 << 3, 		//!< A connection
	Colour 		= 1 << 4, 		//!< A colour
	Color 		= Colour,		//!< An alias of Colour
	Graph 		= 1 << 5, 		//!< A graph

	/// Modifier
	StartAtDest 	= 1 << 6, 		//!< Indicating that a connection starts at the node which is considered or better stored, as destination
	StartAtSrc 	= 1 << 7,		//!< Indicating that a connection starts at the node which is considered or better stored, as src
	Uncomplete 	= 1 << 8,		//!< Indicating uncompleting of something, is only of importance if explicily if asked. (only safty)
	Complete 	= 1 << 9, 		//!< Indicating that the object is complete
	Random 		= 1 << 10, 		//!< Indicated that the object is created randomly
						//!    There is no 'original' modifier, everything that is not random is automatically original
	LoopOne 	= 1 << 11, 		//!< Indicating that the conection is sefpointing
	Payload 	= 1 << 12,		//!< A payload (can be both vertex or edge)
	Undirected 	= 1 << 13, 		//!< A connection/edge is undirected
	Directed 	= 1 << 14,		//!< A connection/edge is directed
	Const 		= 1 << 15, 		//!< The state of being const

	/// Composed types
	ConstEdge 	= Const | Edge, 	//!< A constant edge
	ConstConnection = Const | Connection, 	//!< A constant connection
	ConstVertex 	= Const | Vertex,	//!< A constant vertex
	ConstNode	= ConstVertex, 		//!< An alias of constVertex

	EdgePayload 	= Edge | Payload, 	//!< An edge payload
	VertexPayload 	= Vertex | Payload, 	//!< A vertex payload

	DirectedEdge 	= Edge | Directed, 	//!< A directed edge
	UndirectedEdge 	= Edge | Undirected, 	//!< An undirected edge
	DirectedConnection = Connection | Directed, //!< A directed connection
	UndirectedConnection = Connection | Undirected, //!< An undirected connection

	OriginalGenConn = (Edge | Connection), 	//!< This is an original connection in the generall sense (aplicable to both)

	/*
	 * These two are only for the edge, they can be in theory aslo be aplicibal to edge but are useless.
	 * They are used to indicate to which member an edge is bwond, since there are two possiblititys.
	 */
	EdgeAssocWithSrc = Edge | StartAtSrc, //!< Indicating that an edge is associated with the src part of the connection
	EdgeAssocWithDest = Edge | StartAtDest, //!< Indicating that an edge is associated with the dest part of the connection

	// In teh random setting we give up on the destinction between edge and connection
	RandomEdge 	= (Connection | Edge) | Random, 	//!< The edge is a random edge, This also includes the connection because they are thesame in the end
	RandomConnection = RandomEdge, 		//!< An alias
	RandomEdgeDir  	= RandomEdge | Directed, //!< A directed random edge
	RandomConnDir 	= RandomEdgeDir,
	RandomEdgeUndir = RandomEdge | Undirected,	//!< An undirected random edge
	RandomConnUndir = RandomEdgeUndir,
	RandomVertex 	= Vertex | Random, 	//!< The vertex is a ranmo vertex

	CompleteEdge    = Connection | Edge | Complete, //!< The edge (also the connection is complete)
	CompleteVertex 	= Vertex | Complete, 	//!< The vertex is complete

	LoopOneEdge 	= Edge | LoopOne, 	//!< Indicating that the edge is a loop one (src and dest are the same)
	LoopOneEdgeDir  = DirectedEdge | LoopOne, //!< The edge is a directed loop one edge
	LoopOneEdgeUndir = UndirectedEdge | LoopOne, //!< The edge is an undirected loop one edge
	LoopOneVertex   = Vertex | LoopOne 	//!< Meaning that the vertex has an edge, that is a loop one.
};

/*
 * The folowing macro defines where boundary is, between basic type and Modyfier
 * It A tag that has exactly one one in its binary representation and is bigger ore equal
 * 1 << PGL_GRAPH__GRAPHTAG_BIGGEST_BASIC
 * Is considered as a Modifier
 */
#define PGL_GRAPH__GRAPGTAG_BIGGEST_BASIC 6



/*
 * Some of the most commen operators overloded for GraphTag
 */

constexpr
inline
GraphTag
operator| (
	const GraphTag& lhs,
	const GraphTag& rhs)
{
	return static_cast<GraphTag>(static_cast<uInt16_t>(lhs) | static_cast<uInt16_t>(rhs));
};


constexpr
inline
GraphTag
operator& (
	const GraphTag& lhs,
	const GraphTag& rhs)
{
	return static_cast<GraphTag>(static_cast<uInt16_t>(lhs) & static_cast<uInt16_t>(rhs));
};

/*
constexpr
inline
bool
operator==(
	const GraphTag& lhs,
	const GraphTag& rhs)
{
	return (static_cast<uInt16_t>(lhs) == static_cast<uInt16_t>(rhs));
};


constexpr
inline
bool
operator==(
	GraphTag lhs,
	const GraphTag& rhs)
{
	return (static_cast<uInt16_t>(lhs) == static_cast<uInt16_t>(rhs));
};


constexpr
inline
bool
operator==(
	const GraphTag& lhs,
	GraphTag rhs)
{
	return (static_cast<uInt16_t>(lhs) == static_cast<uInt16_t>(rhs));
};


constexpr
inline
bool
operator!=(
	const GraphTag& lhs,
	const GraphTag& rhs)
{
	return (static_cast<uInt16_t>(lhs) != static_cast<uInt16_t>(rhs));
};

*/

/*
 * =======================================
 * Some usefull function
 */

/**
 * \brief 	Returns true if gt is soly a modyfier
 */
constexpr
inline
bool
GraphTag_isModyfier(
	const GraphTag& gt)
{
	return ((static_cast<uInt16_t>(gt) >= (1 << PGL_GRAPH__GRAPGTAG_BIGGEST_BASIC )) && (__builtin_popcount(static_cast<uInt16_t>(gt)) == 1)) ? true : false;
};



/**
 * \brief 	Returns true if gt is a basic type
 */
constexpr
inline
bool
GraphTag_isBasicType(
	const GraphTag& gt)
{
	return ((static_cast<uInt16_t>(gt) < (1 << PGL_GRAPH__GRAPGTAG_BIGGEST_BASIC )) && (__builtin_popcount(static_cast<uInt16_t>(gt)) == 1)) ? true : false;
};

/**
 * \brief 	Returns true if gt is a composed type
 *
 * A type is composed if it is not one type but more than two like Edge | Connection, which is in many case usefull
 */
constexpr
inline
bool
GraphTag_isComposedType(
	const GraphTag& gt)
{
	return ((static_cast<uInt16_t>(gt) < (1 << PGL_GRAPH__GRAPGTAG_BIGGEST_BASIC )) && (__builtin_popcount(static_cast<uInt16_t>(gt)) > 1)) ? true : false;
};

/**
 * \brief 	returns true of gt is not non zero (unkown)
 *
 * \param gt	To test tag
 */
constexpr
inline
bool
GraphTag_isKnown(
	const GraphTag& gt)
{
	return static_cast<uInt16_t>(gt) > 0;
};


/**
 * \brief	Returns true if gt has the modifier mf
 *
 * \param mf	The modyfier one whants to check
 * \param gt	The tag one wants to analyze
 *
 * If mf is not a modifier, then false is returned
 */
constexpr
inline
bool
GraphTag_hasModyfier(
	const GraphTag& gt,
	const GraphTag& mf)
{
	return (GraphTag_isModyfier(mf)) ? (GraphTag_isKnown( gt & mf )) : false;
};


/**
 * \brief 	returns true if gt is the asked type
 *
 * \param gt 	The tag
 * \param type 	The type one wants to infere
 *
 * If type is not a type, the false is returned
 */
constexpr
inline
bool
GraphTag_isOfType(
	const GraphTag& gt,
	const GraphTag& type)
{
	return GraphTag_isBasicType(type) ? GraphTag_isKnown( gt & type ) : false;
};


/**
 * \brief 	tests if gt is a vertex
 */
constexpr
inline
bool
GraphTag_isVertex(
	const GraphTag& gt)
{
	return GraphTag_isOfType(gt, GraphTag::Vertex);
};

/**
 * \brief 	tests if it is an edge
 */
constexpr
inline
bool
GraphTag_isEdge(
	const GraphTag& gt)
{
	return GraphTag_isOfType(gt, GraphTag::Edge);
};


/**
 * \brief 	Returns true if gt is a connection
 */
constexpr
inline
bool
GraphTag_isConnection(
	const GraphTag& gt)
{
	return GraphTag_isOfType(gt, GraphTag::Connection);
};


/**
 * \brief 	tests if it is a random object
 */
constexpr
inline
bool
GraphTag_isRandom(
	const GraphTag& gt)
{
	return GraphTag_hasModyfier(gt, GraphTag::Random);
};


/**
 * \brief 	returns true if gt is a original generall connection
 *
 * This requires that gt is an edge or connection, with any modyfier except the random modifier
 */
constexpr
inline
bool
GraphTag_isOriginalGenConnection(
	const GraphTag& gt)
{
	return ( !(GraphTag_isRandom(gt))) && GraphTag_isKnown(gt & GraphTag::OriginalGenConn);
}



/**
 * \brief 	retrurns true, if gt is an original object
 */
constexpr
inline
bool
GraphTag_isOriginal(
	const GraphTag& gt)
{
	return !(GraphTag_isRandom(gt));
};


/**
 * \brief 	tests if the tag is a loopone
 */
constexpr
inline
bool
GraphTag_isLoopOne(
	const GraphTag& gt)
{
	return GraphTag_hasModyfier(gt, GraphTag::LoopOne);
};


/**
 * \brief 	returns true if it is an edge and a directed (also applies to connection
 */
constexpr
inline
bool
GraphTag_isDirectedEdge(
	const GraphTag& gt)
{
	return (GraphTag_isEdge(gt) || GraphTag_isConnection(gt)) && GraphTag_hasModyfier(gt, GraphTag::Directed);
};


/**
 * \brief 	returns true if it is an edge and a unbdirected (also applies to connection
 */
constexpr
inline
bool
GraphTag_isUndirectedEdge(
	const GraphTag& gt)
{
	return (GraphTag_isEdge(gt) || GraphTag_isConnection(gt)) && GraphTag_hasModyfier(gt, GraphTag::Undirected);
};


PGL_NS_END(graph)
PGL_NS_END(pgl)




#endif // end include guard
