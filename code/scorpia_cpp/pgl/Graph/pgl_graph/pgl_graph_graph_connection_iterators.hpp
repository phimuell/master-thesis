#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_CONNECTION_ITERATOR
#define PGL_GRAPH__PGL_GRAPH_CONNECTION_ITERATOR
/**
 * \file 	Graph/pgl_graph/pgl_graph_graph_connection_iterator.hpp
 * \brief	This files implements the external connection iterators for the graph.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>

//Include STD
#include <type_traits>
#include <iterator>

//Include BOOST
#include <boost/iterator/iterator_facade.hpp>



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)
PGL_NS_BEGIN(internal)


template<bool isConst, typename T>
struct pgl_conn_pub_helper
{
	using type = std::conditional_t<isConst, const T, T>;
};
template<bool isConst, typename T>
using pgl_conn_pub_helper_t = typename pgl_conn_pub_helper<isConst, T>::type;



/**
 * \class	pgl_graph_pub_connection_iterator_t
 * \broef	This class serves as an iterator for the connection in the graph, as public
 * 		So we can vaoid that annoying second
 */
template<
	bool 	isConst
      , class 	connection_type
      , class 	wraped_iterator
	>
class pgl_graph_pub_connection_iterator_t : public boost::iterator_facade<
	  pgl_graph_pub_connection_iterator_t<isConst, connection_type, wraped_iterator>
	, pgl_conn_pub_helper_t<isConst, connection_type>
	, std::bidirectional_iterator_tag
	>
{
public:
	/*
	 * Some typedefs
	 */
	using Value_t 		= pgl_conn_pub_helper_t<isConst, connection_type>;
	using Reference_t 	= Value_t&;
	using This_t 		= pgl_graph_pub_connection_iterator_t<isConst, connection_type, wraped_iterator>;


	/*
	 * ============================
	 * Constructors
	 */
	pgl_graph_pub_connection_iterator_t() = default;

	pgl_graph_pub_connection_iterator_t(
		const pgl_graph_pub_connection_iterator_t&) = default;

	pgl_graph_pub_connection_iterator_t(
		pgl_graph_pub_connection_iterator_t&&) = default;

	pgl_graph_pub_connection_iterator_t&
	operator= (
		const pgl_graph_pub_connection_iterator_t&) = default;

	pgl_graph_pub_connection_iterator_t&
	operator= (
		pgl_graph_pub_connection_iterator_t&&) = default;

	~pgl_graph_pub_connection_iterator_t() = default;

	//explicit
	pgl_graph_pub_connection_iterator_t(
		wraped_iterator it) :
	  m_currIt(std::move(it))
	{};



	/*
	 * =====================
	 * Some other fnctions
	 */

	auto
	getEdgeID() const
	{
		return m_currIt->first;
	};


	wraped_iterator
	base() const
	{
		return m_currIt;
	};



	/*
	 * =====================
	 * Facade functions
	 */
public:

	Reference_t
	dereference() const
	{
		return m_currIt->second;
	};

	template<bool constTag>
	bool
	equal(
		const pgl_graph_pub_connection_iterator_t<constTag, connection_type, wraped_iterator>& other) const
	{
		return this->m_currIt == other.m_currIt;
	};

	void
	increment()
	{
		++m_currIt;
		return;
	};

	void
	decrement()
	{
		--m_currIt;
	};


	/*
	 * =====================
	 * PRivate memeber
	 */
private:
	wraped_iterator 		m_currIt;	//!< Current Iterator position

}; //end class(pgl_graph_pub_connection_iterator_t)




PGL_NS_END(internal)
PGL_NS_END(graph)
PGL_NS_END(pgl)



#endif //End include guratd
