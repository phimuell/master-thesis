#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_EDGEID_HPP
#define PGL_GRAPH__PGL_GRAPH_EDGEID_HPP

//Include PGL-Headers
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_int.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>


//Include C++ Headers
#include <utility>
#include <iostream>



/**
 * \breif This file provides a class that acts as a wrapper for the ID of an edge.
 * \file pgl_graph/pgl_graph_edgeID.hpp
 *
 */


namespace pgl
{

namespace graph
{


/**
 * \brief This class implements an ID.
 * This ID is for the edge.
 *
 * This class is only a base.
 * It can't be instantietetd, there is another class that will inherit from it, and this class will be abel to instantate.
 *
 */
class pgl_edgeID_t
{
	/*
	 * Public Typedefs
	 */
public:

	/// The underlying type we use for the representation
	using Base_t = pgl::uInt64_t;

	/// This enum is used as syntactic sugar
	enum StateFiled
	{
		DirectedState 		= 0b01,
		UndirectedState 	= 0b10,
		InvalidState 		= 0b11,
		IllegalState		= 0b0
	};


	/*
	 * This conversion is needed to statisfy the requirements of the GraphTag
	 */
	static
 	StateFiled
 	convert_from_GraphTag(const GraphTag&);


	/*
	 * Private static variable
	 */
private:
	 static constexpr uInt_t ce_ID_BitLength 	= 31; 		//!< Numbers of bits, that represents the actual ID of the vertex
	 static constexpr uInt_t ce_ID_totBitLength	= 62;		//!< The total numbers of bits occupied by the ID
	 static constexpr Base_t ce_ZeroStatusMask 	= 0x7fffffff;	//!< Mask to zero out the status field




	/*
	 * Constructors
	 *
	 * The constructors and assignemnts doesn't enforce some rules.
	 * But if one moves from an ID this ID gets invalid
	 */
public:

	// Defaulted compy assignment
	inline
	pgl_edgeID_t&
	operator= (
		const pgl_edgeID_t& other) noexcept = default;


	//move assignment
	inline
	pgl_edgeID_t&
	operator= (
		pgl_edgeID_t&& other) noexcept
	{
		m_id = other.m_id;
		//other.m_id = priv_composeInvalidID();

		return *this;
	};


	//Defaulted compy constructor
	inline
	pgl_edgeID_t(
		const pgl_edgeID_t&) noexcept = default;


	//move constructor
	inline
	pgl_edgeID_t(
		pgl_edgeID_t&& other) noexcept
	{
		m_id = other.m_id;
		//other.m_id = priv_composeInvalidID();
	};

	//Defaulted delete constructor
	inline
	~pgl_edgeID_t() noexcept = default;


	/*
	 * Special constructors
	 */

	/**
	 * \brief construct a connection
	 *
	 * This constructor constructs a connection given two vertexID and a state
	 * If the state is Invalid the provided IDs are ignored.
	 * Also both ID must be valid.
	 *
	 * \param srcID 	The vertexID where the connection shall start
	 * \param destID 	The vertexID where the connection shall end
	 * \param State 	The state that the connection shall have
	 */
	inline

	pgl_edgeID_t(
		const pgl_vertexID_t& srcID,
		const pgl_vertexID_t& destID,
		const StateFiled State) noexcept : m_id(0)
	{
		assert(pgl_implies(State != StateFiled::InvalidState, srcID.isValid() && destID.isValid()));

		switch(State)
		{
		  case InvalidState:
			m_id = priv_composeInvalidID();
			break;

		  case DirectedState:
			m_id = priv_composeDirectedID(srcID, destID);
			break;

		  case UndirectedState:
			m_id = priv_composeUndirectedID(srcID, destID);
			break;

		  case IllegalState: default:
			pgl_assert(false && "pgl_edgeID_t: Illegal State argument");
		  	break;
		}; //End switch(State)

		//If the state was not invalid, then, then we must check.
		pgl_assert(pgl_implies( State != StateFiled::InvalidState, this->isValid()) );
	};


	/**
	 * \brief construct an edge ID but using the GraphTag enum, instead of the internal state
	 *
	 * This constructor constructs a connection given two vertexID and a state
	 * If the state is Invalid the provided IDs are ignored.
	 * Also both ID must be valid.
	 *
	 * \param srcID 	The vertexID where the connection shall start
	 * \param destID 	The vertexID where the connection shall end
	 * \param State 	The state that the connection shall have
	 */
	inline
	pgl_edgeID_t(
		const pgl_vertexID_t& srcID,
		const pgl_vertexID_t& destID,
		const GraphTag& State) noexcept :
	  pgl_edgeID_t(srcID, destID, convert_from_GraphTag(State))
	{
	};

	/**
	 * \brief the default constructor constructs an invalid element
	 */
	inline
	pgl_edgeID_t() noexcept :
	  pgl_edgeID_t(pgl_vertexID_t::INVALID(), pgl_vertexID_t::INVALID(), StateFiled::InvalidState)
	{};




	/*
	 * Public member functions
	 */
public:

	/**
	 * \brief This function determines the state of *this
	 */
	inline

	StateFiled
	getState() const noexcept
	{
		switch((this->m_id >> ce_ID_totBitLength))
		{
		  case 0b01:
		  	return StateFiled::DirectedState;
		  	break;

		  case 0b10:
		  	return StateFiled::UndirectedState;
		  	break;

		  case 0b11:
		  	return StateFiled::InvalidState;
		  	break;

		  case 0b00: default:
			pgl_assert(false && "pgl_edgeID_t: ID is in an illegal state.");
			return StateFiled::IllegalState;
		  	break;
		};
	};



	/**
	 * \breif return true if *this is in a valid state
	 */
	inline

	bool
	isValid() const noexcept
	{
		switch(getState())
		{
		  case StateFiled::DirectedState: case StateFiled::UndirectedState:
			return true;
			break;

		  default:
		  	return false;
		  	break;
		};
	};


	/**
	 * \brief	Returns true, if this is a loopOne connection
	 */
	inline

	bool
	isLoopOne() const
	{
		return (this->getSrcID() == this->getDestID()) ? true : false;
	};


	/**
	 * \breif Returns true if *this is a directed connection
	 */
	inline

	bool
	isDirected() const noexcept
	{
		return (this->getState() == StateFiled::DirectedState);
	};


	/*
	 * \brief Return true if this is an undirected connection
	 */
	inline

	bool
	isUndirected() const noexcept
	{
		return (this->getState() == StateFiled::UndirectedState);
	};



	/**
	 * \brief returns true if *this is in an invalid state
	 */
	inline

	bool
	isInvalid() const noexcept
	{
		return !(this->isValid());
	};


	/**
	 * \breif return true if *this this is in an legal State
	 */
	inline

	bool
	isLegal() const noexcept
	{
		switch(getState())
		{
			case StateFiled::DirectedState: case StateFiled::UndirectedState: case StateFiled::InvalidState:
			return true;
			break;

		  default:
		  	return false;
		  	break;
		};
	};


	/*
	 * Decomposing functions
	 *
	 * This functions are used to decode the the internal state of *this
	 */


	/**
	 * \brief returns the ID of the srcID
	 *
	 * If this is not a directed edge, then the ID which is returned is arbitraral.
	 * It is garanteed to be the same everytime and distinct form the value that is returned by getDestID()
	 *
	 * This function returns a value of the underling type
	 *
	 * \pre *this must be valid
	 */
	inline

	Base_t
	getSrcID_triv() const noexcept
	{
		pgl_assert(this->isValid());
		return Base_t( Base_t(m_id >> ce_ID_BitLength) & ce_ZeroStatusMask);
	};


	/**
	 * \brief This function returns a vertexID object that compares equal to the one that has the src vertex
	 * It works the same as the _triv version
	 */
	inline
	pgl_vertexID_t
	getSrcID() const noexcept
	{
		return pgl_vertexID_t(this->getSrcID_triv(), true);
	};


	/**
	 * \brief returns the ID of the srcID
	 *
	 * If this is not a directed edge, then the ID which is returned is arbitraral.
	 * It is garanteed to be the same everytime and distinct form the value that is returned by getSrcID()
	 *
	 * this function returns the value as type of the underlying data type
	 *
	 * \pre *this must be valid
	 */
	inline

	Base_t
	getDestID_triv() const noexcept
	{
		assert(this->isValid() );
		return Base_t( Base_t(m_id) & ce_ZeroStatusMask);
	};


	/**
	 * \brief This function does the same as getDestID_triv, but it returns a vertexID object
	 */
	inline
	pgl_vertexID_t
	getDestID() const noexcept
	{
		return pgl_vertexID_t(this->getDestID_triv(), true);
	};



	/**
	 * \brief this function returns the whole ID of *this
	 *
	 * \pre This must be valid
	 */
	inline

	Base_t
	getID() const noexcept
	{
		pgl_assert(this->isLegal());
		return m_id;
	};


	/**
	 * \breif return a pair of That has both.
	 *
	 * first: 	is the ID of the starting point of the connection
	 * second:	is the end point of the connection
	 */
	inline
	std::pair<pgl_vertexID_t, pgl_vertexID_t>
	decomposeID() const noexcept
	{
		return std::make_pair(this->getSrcID(), this->getDestID());
	};


	/**
	 * \brief decompose the ID but returns the underlying datatype.
	 */
	inline

	std::pair<Base_t, Base_t>
	decomposeID_triv() const noexcept
	{
		return std::make_pair(this->getSrcID_triv(), this->getDestID_triv());
	};





	/*
	 * Comparision Operator
	 */
public:


	/**
	 * \brief Returns true if *this is less than rhs
	 *
	 * The ruls are described in the manual.
	 * In short this function compares the underling data.
	 *
	 * \pre this must be legal
	 */
	inline

	bool
	operator< (
		const pgl_edgeID_t& rhs) const noexcept
	{
		pgl_assert(this->isLegal(), rhs.isLegal());
		return this->m_id < rhs.m_id;
	};


	/**
	 * \brief Compares for equality
	 *
	 * If exactly one of the ids is invalid, then they are not equal.
	 * if both are invalid then they are equal
	 *
	 * \pre Both must be legal
	 */
	inline

	bool
	operator== (
		const pgl_edgeID_t& rhs) const noexcept
	{
		pgl_assert(this->isLegal(), rhs.isLegal() );

		const bool thisInvalid = this->isInvalid();
		const bool rhsInvalid  = rhs.isInvalid();

		if(thisInvalid || rhsInvalid)
		{
			return thisInvalid && rhsInvalid;
		};

		return (this->m_id == rhs.m_id);
	};


	inline

	bool
	operator!= (
		const pgl_edgeID_t& rhs) const noexcept
	{
		return !(this->operator==(rhs));
	};




	/*
	 * Private member functions
	 */
private:


	/**
	 * \brief This function compose an m_id value that is invalid
	 */
	inline

	static
	Base_t
	priv_composeInvalidID() noexcept
	{
		return ~(Base_t(0));
	};


	/**
	 * \brief This function composes an m_id that is directed
	 *
	 * \pre Both srcID and destID are valid
	 */
	inline
	static
	Base_t
	priv_composeDirectedID(
		const pgl_vertexID_t& srcID,
		const pgl_vertexID_t& destID) noexcept
	{
		const Base_t srcID_Base = srcID.getID();
		const Base_t destID_Base = destID.getID();
		const Base_t bitState = priv_getBitState(StateFiled::DirectedState);

		return Base_t(Base_t(bitState << ce_ID_totBitLength) | Base_t(srcID_Base << ce_ID_BitLength) | destID_Base );
	};



	/**
	 * \brief This function composes an m_id that is undirected
	 *
	 * \pre Both srcID and destID are valid
	 */
	inline
	static
	Base_t
	priv_composeUndirectedID(
		const pgl_vertexID_t& srcID,
		const pgl_vertexID_t& destID) noexcept
	{
		const Base_t srcID_Base = std::min(srcID.getID(), destID.getID() );
		const Base_t destID_Base = std::max(srcID.getID(), destID.getID());
		const Base_t bitState = priv_getBitState(StateFiled::UndirectedState);

		return Base_t(Base_t(bitState << ce_ID_totBitLength) | Base_t(srcID_Base << ce_ID_BitLength) | destID_Base );
	};



	/**
	 * \brief this function returns the bit patern of the presented state field
	 */
	inline

	static
	Base_t
	priv_getBitState(
		const StateFiled& State) noexcept
	{
		switch(State)
		{
		  case InvalidState:
			return 0b11;
			break;

		  case DirectedState:
			return 0b01;
			break;

		  case UndirectedState:
			return 0b10;
			break;

		  case IllegalState: default:
		  	assert(false && "pgl_edgeID_t: Illegal State argument");
		  	return 0;
		  	break;
		}; //End switch(State)

	}; //End priv_getBitState




	/*
	 * Private member variabel
	 */
private:

	/**
	 * The wordening is such, that the edge is a directed one.
	 * The encoding is such that, starting at the lsb,:
	 * 	- The first 31bit represent the ID of the Node where the connection end.
	 * 	- The second 31bit represent the ID of the Node where the connection starts.
	 * 	- The last 2bits are used as a state filed
	 * If the connection is undirected, then the ordering is, such that the smaller ID is placed in the destID part.
	 *
	 * The status field is:
	 * 	01	Connection is directed
	 * 	10 	Connection is undirected
	 * 	11 	*this is invalid
	 * 	00	Illegal state
	 */
	Base_t 		m_id;		//!< The member that represent the id

}; //End class(pgl_edgeID_t)



/**
 * \breif overload the out stream operator for the edge ID
 */
inline
std::ostream&
operator<<(
	std::ostream& out,
	const pgl::graph::pgl_edgeID_t& ID)
{
	switch(ID.getState() )
	{
	  case pgl_edgeID_t::DirectedState:
	  	out << "(" << ID.getSrcID() << " -> " << ID.getDestID() << ")";
	  	break;

	  case pgl_edgeID_t::UndirectedState:
	  	out << "(" << ID.getSrcID() << " <-> " << ID.getDestID() << ")";
	  	break;

	  case pgl_edgeID_t::InvalidState:
	  	out << "(INVALIDE)";
	  	break;

	  case pgl_edgeID_t::IllegalState: default:
	  	out << "(ILLEGALE_STATE)";
	  	break;
	};

	return out;
}; //End operator<< (ostream)



pgl_edgeID_t::StateFiled
convert_from_GraphTag(const GraphTag&);


}; //End namespace (graph)
}; //End namespace(pgl)




/*
 * Overload some standard predicates
 */
namespace std
{


	/**
	 * \brief std::hash<pgl_edgeID_t>
	 *
	 * This is a specialization of the std:hash struct for edgeID
	 * This way they become hashable
	 */
	template <>
	struct hash<::pgl::graph::pgl_edgeID_t>
	{
		size_t operator()(
			const ::pgl::graph::pgl_edgeID_t& x) const
		{
			return m_privHash(x.getID() );
		}; //End operator

	private:
		std::hash<::pgl::graph::pgl_edgeID_t::Base_t>	m_privHash;
	}; //End struct(hash)

}; //End namespace(std)







#endif //End include guard
