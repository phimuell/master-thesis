/**
 * \brief	Implements the printing functions for the vertex.
 * \file	pgl_graph/pgl_graph_vertex_printing.cpp
 *
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_utility.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_graphtag.hpp>

#include <pgl_container/pgl_vector.hpp>

//Include STL
#include <functional>
#include <memory>
#include <bitset>
#include <iterator>
#include <algorithm>
#include <sstream>


PGL_NS_START(pgl)
PGL_NS_START(graph)


std::string
pgl_vertex_t::print_topo() const
{
	std::stringstream s;

	s << "Vertex: " << this->m_id << "\n";
	s << "degree = " << this->degree() << ", indegree = " << this->inDegree() << ", outdegree = " << this->outDegree() << "\n";
#ifndef PGL_GRAPH_DISABLE_PAYLOADS
	s << "Payload: " << m_payload.get() << "\n";
#endif

	for(auto it = this->EBegin(); it != this->EEnd(); ++it)
	{
		s << it->print_topo() << "\n\n";
	}; //End for(it): iterating through the vertices

	return s.str();
};










PGL_NS_END(graph)
PGL_NS_END(pgl)


