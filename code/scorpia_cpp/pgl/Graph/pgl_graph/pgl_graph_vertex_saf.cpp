/**
 * \file pgl_graph/pgl_graph_vertex_saf.cpp
 */

// Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex_saf.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>


PGL_NS_START(pgl)
PGL_NS_START(graph)


/**
 * \brief 	Retruns the ID of the vertex
 * \param This	A pointer to the vertex
 */
pgl_vertexID_t
pgl_vertexSAF_getID(
	const pgl_vertex_t* This)
{
	pgl_assert(This != nullptr);

	return This->getID();
};


/**
 * \brief 	Performs the valid test of the vertex
 * \param This 	A pointr to the vertex
 */
bool
pgl_vertexSAF_isValid(
	const pgl_vertex_t* 	This)
{
	pgl_assert(This != nullptr);

	return This->isValid();
};




PGL_NS_END(graph)
PGL_NS_END(pgl)




