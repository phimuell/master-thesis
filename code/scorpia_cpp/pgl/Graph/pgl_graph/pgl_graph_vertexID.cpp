/**
 * \brief	This file implements some functions of the vertex ID class of PGL.
 */


//Include PGL-Headers
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_int.hpp>

#include <pgl_graph/pgl_graph_vertexID.hpp>


//Include C++ Headers
#include <iostream>
#include <string>


namespace std
{

	::std::string
	to_string(
		::pgl::graph::pgl_vertexID_t id)
	{
		return id.print();
	};


}; //End ns(std)



