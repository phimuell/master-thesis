#pragma once
#ifndef PGL_GRAPH__PGL_VERTEX_ITERATOR_HPP
#define PGL_GRAPH__PGL_VERTEX_ITERATOR_HPP
/**
 * \file	pgl_graph/pgl_graph_vertex_iterator.hpp
 * \brief	Implements an iterator for the an doe a lot of things
 *
 * This is an iterator class.
 */


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>


//Include STD
#include <type_traits>


//Include Boost
#include <boost/iterator/iterator_facade.hpp>



PGL_NS_START(pgl)
PGL_NS_START(graph)
PGL_NS_START(internal)



/**
 * \enum vIteratorTag
 *
 * This Tags are used to controle the behaviour of the iterator.
 */
namespace vIteratorTag
{
enum vIteratorTag
{
	Const 			= 1 << 0,
	nonConst 		= 1 << 1,
	h 			= Const | nonConst,
	Edge			= 1 << 2,
	Vertex			= 1 << 3,
	Incoming 		= 1 << 4,
	Outgoing		= 1 << 5,
	Connection 		= 1 << 6 	//Used for the case, if we want, all connections
};
}; //end namespace

constexpr
vIteratorTag::vIteratorTag
_OR(
	const vIteratorTag::vIteratorTag rhs,
	const vIteratorTag::vIteratorTag lhs)
{
	return static_cast<vIteratorTag::vIteratorTag>(rhs | lhs);
};

constexpr
vIteratorTag::vIteratorTag
_OR(
	const vIteratorTag::vIteratorTag rhs,
	const vIteratorTag::vIteratorTag mhs,
	const vIteratorTag::vIteratorTag lhs)
{
	return static_cast<vIteratorTag::vIteratorTag>(rhs | lhs | mhs);
};

constexpr
bool
_AND_LOG(
	const vIteratorTag::vIteratorTag rhs,
	const vIteratorTag::vIteratorTag lhs)
{
	return static_cast<int>(rhs) & static_cast<int>(lhs);
};







template<vIteratorTag::vIteratorTag Tag>
struct pgl_vertex_iterator_mode_traits
{
	static constexpr bool Vertex = std::integral_constant<bool, _AND_LOG(Tag, vIteratorTag::Vertex)>::value;
	static constexpr bool Edge   = std::integral_constant<bool, _AND_LOG(Tag, vIteratorTag::Edge)>::value;
	static constexpr bool Incoming = std::integral_constant<bool, _AND_LOG(Tag, vIteratorTag::Incoming)>::value;
	static constexpr bool Outgoing = std::integral_constant<bool, _AND_LOG(Tag, vIteratorTag::Outgoing)>::value;
	static constexpr bool BothDirections = std::integral_constant<bool, _AND_LOG(Tag, vIteratorTag::Connection)>::value;


	//static_assert( pgl_implies(BothDirections == false, (Incoming && Outgoing) == false), "Selected two directions");
	//static_assert( pgl_implies(BothDirections == true, (Incoming || Outgoing) == false), "Selected BothDirections and an additional Directions.");
	//static_assert((Vertex && Edge) == false, "Two types are selected.");
	//static_assert((Vertex || Edge) == true, "No Type is selected.");


	template<vIteratorTag::vIteratorTag Tag2>
	struct isCompativleWith
	{
		static constexpr bool Value = std::integral_constant<bool, (Tag | vIteratorTag::h) == (Tag2 | vIteratorTag::h)>::value;
	};
};


template<vIteratorTag::vIteratorTag Tag, class Vertex_Type, class Edge_Type>
struct
pgl_vertex_iterator_mode_types
{
	static constexpr bool isConst = _AND_LOG(Tag, vIteratorTag::Const);
	using Vertex_t 		= std::remove_cv_t<Vertex_Type>;
	using Edge_t 		= std::remove_cv_t<Edge_Type>;

	using baseValueType = std::conditional_t<_AND_LOG(Tag, vIteratorTag::Vertex), std::remove_cv_t<Vertex_Type>, std::remove_cv_t<Edge_Type> >;
	using value_type = std::conditional_t<isConst, const baseValueType, baseValueType>;
	using reference  = value_type&;
	using const_reference = const baseValueType&;
	using pointer    = value_type*;
	using ptrdiff_t  = std::ptrdiff_t;


	using Vertex_ref 	= std::conditional_t<isConst, const Vertex_t&, Vertex_t&>;
	using Vertex_cref	= const Vertex_t&;

	using Edge_ref 		= std::conditional_t<isConst, const Edge_t&, Edge_t&>;
	using Edge_cref 	= const Edge_t&;
};




/**
 * \brief 	This class contains the implementation
 * \class	pgl_vertex_iterator_faceadeImpl
 *
 *
 * If the user want to modify the behaviour of the iterator, then this class must be modified
 *
 *
 */
//Zum erweitern, hier noch ein zusätzlicher template parameter mitgeben, von dem man dan erbt
template<vIteratorTag::vIteratorTag Tag, class Vertex_Type, class Edge_Type, class WrappedIterator_Type>
class pgl_vertex_iterator_faceadeImpl
	: public boost::iterator_facade<
	  	pgl_vertex_iterator_faceadeImpl<Tag, Vertex_Type, Edge_Type, WrappedIterator_Type> ,
	  	typename pgl_vertex_iterator_mode_types<Tag, Vertex_Type, Edge_Type>::value_type ,
	  	std::bidirectional_iterator_tag
	  >
{
public:

	template<vIteratorTag::vIteratorTag TagInternal>
	using Mode = pgl_vertex_iterator_mode_traits<TagInternal>;

	using This_t = pgl_vertex_iterator_faceadeImpl;
	using ThisMode = Mode<Tag>;
	using ThisModeType = pgl_vertex_iterator_mode_types<Tag, Vertex_Type, Edge_Type>;


protected:



	/**
	 * \brief this function des the dereferencing in the case of the node mode, and incoming nodes
	 */
	template<vIteratorTag::vIteratorTag TagI>
	typename ThisModeType::reference
	dereference_impl(
		std::enable_if_t<(Mode<TagI>::Vertex && Mode<TagI>::Incoming), int>) const
	{
		static_assert(TagI == Tag, "dereference_impl: Incoming, Vertex; is wrong.");
		pgl_assert(m_currentIt->isIncoming());

		return m_currentIt->getOtherNode();
	};


	/**
	 * \brief this function des the dereferencing in the case of the node mode and outgoing
	 */
	template<vIteratorTag::vIteratorTag TagI = Tag>
	typename ThisModeType::reference
	dereference_impl(
		std::enable_if_t<Mode<TagI>::Vertex && Mode<TagI>::Outgoing, int>) const
	{
		static_assert(TagI == Tag, "dereference_impl: Incoming, Vertex; is wrong.");
		pgl_assert(m_currentIt->isLeaving());

		return m_currentIt->getOtherNode();
	};


	/**
	 * \brief this function des the dereferencing in the case of the edge and incoming
	 */
	template<vIteratorTag::vIteratorTag TagI = Tag>
	typename ThisModeType::reference
	dereference_impl(
		std::enable_if_t<Mode<TagI>::Edge && Mode<TagI>::Incoming, int>) const
	{
		static_assert(TagI == Tag, "dereference_impl: Incoming, Vertex; is wrong.");
		pgl_assert(m_currentIt->isIncoming() );

		return *m_currentIt;
	};


	/**
	 * \brief this function des the dereferencing in the case of the edge and outgoing
	 */
	template<vIteratorTag::vIteratorTag TagI = Tag>
	typename ThisModeType::reference
	dereference_impl(
		std::enable_if_t<Mode<TagI>::Edge && Mode<TagI>::Outgoing, int>) const
	{
		static_assert(TagI == Tag, "dereference_impl: Incoming, Vertex; is wrong.");
		pgl_assert(m_currentIt->isLeaving() );

		return *(m_currentIt);
	};


	/**
	 * \brief this function des the dereferencing in the case of the edge and both
	 */
	template<vIteratorTag::vIteratorTag TagI = Tag>
	typename ThisModeType::reference
	dereference_impl(
		std::enable_if_t<Mode<TagI>::Edge && Mode<TagI>::BothDirections, int>) const
	{
		static_assert(TagI == Tag, "dereference_impl: Incoming, Vertex; is wrong.");
		//pgl_assert(m_currentIt.isLeaving() );

		return *(m_currentIt);
	};


	/**
	 * \brief this function des the dereferencing in the case of the node mode and both
	 */
	template<vIteratorTag::vIteratorTag TagI = Tag>
	typename ThisModeType::reference
	dereference_impl(
		std::enable_if_t<Mode<TagI>::Vertex && Mode<TagI>::BothDirections, int>) const
	{
		static_assert(TagI == Tag, "dereference_impl: Incoming, Vertex; is wrong.");
		//pgl_assert(m_currentIt.isLeaving());

		return m_currentIt->getOtherNode();
	};

	typename ThisModeType::reference
	dereference() const
	{
		//return dereference_impl<static_cast<vIteratorTag::vIteratorTag>(vIteratorTag::Vertex | vIteratorTag::Incoming)>();
		return dereference_impl<Tag>(1);
	};



	/**
	 * \brief 	Test of equality
	 */
	template<vIteratorTag::vIteratorTag Tag_O, class Vertex_Type_O, class Edge_Type_O, class WrappedIterator_Type_O>
	bool
	equal(
		const pgl_vertex_iterator_faceadeImpl<Tag_O, Vertex_Type_O, Edge_Type_O, WrappedIterator_Type_O>& other) const
	{
		return (m_currentIt == other.m_currentIt) && (m_endRange == other.m_endRange) && (m_beginRange == other.m_beginRange);
	};


	/**
	 * \brief 	Increment *this, in the incoming mode
	 */
	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::Incoming, void>
	increment() noexcept(false)
	{
		static_assert(Tag == tag, "Increment, Incoming: Wrong Tag");

		if(m_currentIt == m_endRange)
		{
			throw PGL_EXCEPT_OutOfBound("TRied to increment an End-Iterator. (Mode Incoming)");
		};

		while(m_currentIt != m_endRange)
		{
			++m_currentIt;

			if(m_currentIt->isIncoming() == true)
			{
				break;
			};
		}; //End while

		return;
	};


	/**
	 * \brief 	Increment *this, in the outgoing mode
	 */
	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::Outgoing, void>
	increment() noexcept(false)
	{
		static_assert(Tag == tag, "Increment, Outgoing: Wrong Tag");

		if(m_currentIt == m_endRange)
		{
			throw PGL_EXCEPT_OutOfBound("TRied to increment an End-Iterator. (Mode Outgoing)");
		};

		while(m_currentIt != m_endRange)
		{
			++m_currentIt;

			if(m_currentIt->isLeaving() == true)
			{
				break;
			};
		}; //End while

		return;
	};

	/**
	 * \brief 	decrement *this, in the incoming mode
	 */
	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::Incoming, void>
	decrement() noexcept(false)
	{
		static_assert(Tag == tag, "decrement, Incoming: Wrong Tag");

		if(m_currentIt == m_beginRange)
		{
			throw PGL_EXCEPT_OutOfBound("TRied to decrement an End-Iterator. (Mode Incoming)");
		};

		while(m_currentIt != m_beginRange)
		{
			--m_currentIt;

			if(m_currentIt->isIncoming() == true)
			{
				break;
			};
		}; //End while

		return;
	};


	/**
	 * \brief 	Increment *this, in the bothdirection
	 */
	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::BothDirections, void>
	increment() noexcept(false)
	{
		static_assert(Tag == tag, "Increment, Outgoing: Wrong Tag");

		if(m_currentIt == m_endRange)
		{
			throw PGL_EXCEPT_OutOfBound("TRied to increment an End-Iterator. (Mode Outgoing)");
		};

		++m_currentIt;

		return;
	};

	/**
	 * \brief 	decrement *this, in the both direction mode
	 */
	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::BothDirections, void>
	decrement() noexcept(false)
	{
		static_assert(Tag == tag, "decrement, Incoming: Wrong Tag");

		if(m_currentIt == m_beginRange)
		{
			throw PGL_EXCEPT_OutOfBound("TRied to decrement an End-Iterator. (Mode Incoming)");
		};

		++m_currentIt;

		return;
	};

	/**
	 * \brief 	decrement *this, in the outgoing mode
	 */
	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::Outgoing, void>
	decrement() noexcept(false)
	{
		static_assert(Tag == tag, "decrement, Outgoing: Wrong Tag");

		if(m_currentIt == m_beginRange)
		{
			throw PGL_EXCEPT_OutOfBound("TRied to decrement an End-Iterator. (Mode Outgoing)");
		};

		while(m_currentIt != m_beginRange)
		{
			--m_currentIt;

			if(m_currentIt->isLeaving() == true)
			{
				break;
			};
		}; //End while

		return;
	};


public:

	/*
	 * Here are some base functions
	 */

	/**
	 * \brief 	Return the wrapped iterator
	 */
	WrappedIterator_Type
	base() const
	{
		return m_currentIt;
	};


	/**
	 * \brief	Returns true if this is the end iterator
	 */
	bool
	isEnd() const noexcept
	{
		return m_currentIt == m_endRange;
	};

	/**
	 * \brief 	Returns true if *this is the beginning of the iterator
	 */
	bool
	isBegin() const noexcept
	{
		return m_currentIt == m_beginRange;
	};


	/*
	 * ======================
	 * Other functions that are usefull from time to time
	 */
public:

	/**
	 * \brief	Returns a reference to the owning node
	 */
	typename ThisModeType::Vertex_ref
	getOwningNode()
	{
		pgl_assert(isEnd() == false);
		return m_currentIt->getOwningNode();
	};

	typename ThisModeType::Vertex_cref
	getOwningNode() const
	{
		pgl_assert(isEnd() == false);
		return m_currentIt->getOwningNode();
	};

	typename ThisModeType::Vertex_cref
	getCOwningNode() const
	{
		pgl_assert(isEnd() == false);
		return m_currentIt->getOwningNode();
	};

	pgl_vertexID_t
	getOwningID() const
	{
		pgl_assert(isEnd() == false);
		return m_currentIt->getOwningID();
	};


	/**
	 * \brief	Returns a reference to the other node
	 */
	typename ThisModeType::Vertex_ref
	getOtherNode()
	{
		pgl_assert(isEnd() == false);
		return m_currentIt->getOtherNode();
	};

	typename ThisModeType::Vertex_cref
	getOtherNode() const
	{
		pgl_assert(isEnd() == false);
		return m_currentIt->getOtherNode();
	};

	typename ThisModeType::Vertex_cref
	getCOtherNode() const
	{
		pgl_assert(isEnd() == false);
		return m_currentIt->getOtherNode();
	};

	pgl_vertexID_t
	getOtherID() const
	{
		pgl_assert(isEnd() == false);
		return m_currentIt->getOtherID();
	};


	/**
	 * \brief 	Returns a reference to the underlying edge
	 */
	typename ThisModeType::Edge_ref
	getEdge()
	{
		pgl_assert(isEnd() == false);
		return *m_currentIt;
	};

	typename ThisModeType::Edge_cref
	getEdge() const
	{
		pgl_assert(isEnd() == false);
		return *m_currentIt;
	};

	typename ThisModeType::Edge_cref
	getCEdge() const
	{
		pgl_assert(isEnd() == false);
		return *m_currentIt;
	};

	/*
	 * ==================================
	 * Constructor
	 */
public:
	pgl_vertex_iterator_faceadeImpl() = default;
	pgl_vertex_iterator_faceadeImpl(const pgl_vertex_iterator_faceadeImpl&) = default;
	pgl_vertex_iterator_faceadeImpl(pgl_vertex_iterator_faceadeImpl&&) = default;
	~pgl_vertex_iterator_faceadeImpl() = default;

	pgl_vertex_iterator_faceadeImpl&
	operator= (
		const pgl_vertex_iterator_faceadeImpl&) = default;

	pgl_vertex_iterator_faceadeImpl&
	operator= (
		pgl_vertex_iterator_faceadeImpl&&) = default;

	pgl_vertex_iterator_faceadeImpl(
		const WrappedIterator_Type& currentIt,
		const WrappedIterator_Type& beginRange,
		const WrappedIterator_Type& endRange) :
	  m_currentIt(currentIt),
	  m_endRange(endRange),
	  m_beginRange(beginRange)
	{
		this->findFirst();
	};


private:
	//This functions are used to find the first suitable iterators
	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::BothDirections, void>
	findFirst()
	{
		return;
	};


	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::Incoming, void>
	findFirst()
	{
		if(m_currentIt == m_endRange)
		{
			return;
		};

		while(m_currentIt != m_endRange)
		{
			if(m_currentIt->isIncoming() == true)
			{
				break;
			};

			++m_currentIt;
		};

		return;
	};


	template<vIteratorTag::vIteratorTag tag = Tag>
	std::enable_if_t<Mode<tag>::Outgoing, void>
	findFirst()
	{
		if(m_currentIt == m_endRange)
		{
			return;
		};

		while(m_currentIt != m_endRange)
		{
			if(m_currentIt->isLeaving() == true)
			{
				break;
			};

			++m_currentIt;
		};

		return;
	};

private:
	WrappedIterator_Type 	m_currentIt;	//!< This is the current iterator
	WrappedIterator_Type 	m_endRange;	//!< This is the iterator that points to the end of the container
	WrappedIterator_Type 	m_beginRange;	//!< This is the begining of the range

	//For the iterator
friend
class boost::iterator_core_access;


}; //End pgl_vertex_iterator_faceadeImpl








template<vIteratorTag::vIteratorTag Tag, class Vertex_Type, class Edge_Type, class WrappedIterator_Type>
using pgl_vertex_iterator_base_t = pgl_vertex_iterator_faceadeImpl<Tag, Vertex_Type, Edge_Type, WrappedIterator_Type>;


PGL_NS_END(internal)
PGL_NS_END(graph)
PGL_NS_END(pgl)


#endif //End include guard
