/**
 * \file 	pgl_graph/pgl_graph_graph_creating.cpp
 * \brief 	Implements the creating functions
 *
 * This file implements some functions for the graph.
 * They are manly used to add and build something.
 *
 * NOTE: The constructors are not defined here.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl/pgl_aglorithm.hpp>

#include <pgl_stable_vector.hpp>
#include <pgl_multimap.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>

#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>

PGL_NS_START(pgl)
PGL_NS_START(graph)

/*
 * \brief	Create a vertex
 *
 * This function creates a new vertex, with the given arguiments
 * an Iterator to the newly construct vertex is returned.
 *
 * Remener that the vertex aquire exclusive ownership of the pointer to the payload.
 * So no managment by the client is required.
 *
 * Other vertex iterators are not invalidated.
 *
 * \param ID		The ID of vertex (must be unique) inside the graph
 * \param KindOfNode	A GraphTag, taht describes the node to be
 * \param payload	A Pointer of type vertex pointer, nullptr is also accepted
 *
 * \return 		An iterator to the vertex.
 */
pgl_graph_t::NodeIterator_t
pgl_graph_t::createNewNode(
	const pgl_vertexID_t&		ID,
	const GraphTag			KindOfNode,
	VertexPayLoad_ptr 		payload) noexcept(false)
{

	//First testing if an insertion can be done
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Graph must be uncomplete to create a vertex.");
	};

	//Test if the inputsAre valid
	if(
	      (ID.isInvalid()) 	//Test if the ID is valid
	   || (GraphTag_isVertex(KindOfNode) == false))
	{
		throw PGL_EXCEPT_InvArg("Kind of node is not an Vertex or ID is invalid.");
	};


	//Search for the position where we have to insert it
	const_intNodeIterator_t toInsertPos = std::lower_bound(m_vertices.begin(), m_vertices.end(), ID, std::less<void>());

	if((toInsertPos != m_vertices.end()) && (toInsertPos->getID() != ID) )
	{
		throw PGL_EXCEPT_LOGIC("There is allready a vertex with the given ID.");
	};

	//Creating the vertex
	//this->m_vertices.emplace(toInsertPos, ID, KindOfNode, payload);
	this->m_vertices.emplace(toInsertPos, ID, payload, KindOfNode);

	//Test if still is sorted
	if(!std::is_sorted(m_vertices.cbegin(), m_vertices.cend()))
	{
		throw PGL_EXCEPT_LOGIC("There was an error in the inerting process.");
	};

	//Sreach for it again to veryfy if it is a valid vertex.
	NodeIterator_t newlyCreatedVertex = this->getNode(ID);

	if(newlyCreatedVertex == m_vertices.end())
	{
		throw PGL_EXCEPT_LOGIC("The vertex was created, but was not found.");
	};

	if(newlyCreatedVertex->isValid() == false && newlyCreatedVertex->getID() != ID)
	{
		throw PGL_EXCEPT_LOGIC("The Vertex, that was created was not valid.");
	};



	//Make accounting
	if(newlyCreatedVertex->isRandom() )
	{
		m_nRandVertices += 1;
		pgl_assert(m_nRandVertices <= m_vertices.size());
	};

	//Return the iterator, to the newly created vertex
	return newlyCreatedVertex;
};


/*
 * \brief 	Create a connection
 *
 * This functions crates a connection (meaing will allocate memeory for the connection object).
 * It will also create the endges in the respective vertieces.
 * Remember that this vill invalidates, the iterators that are provided by the vertex class.
 * The function will return an iterator to the newly created connection.
 *
 * The graph must be uncomplete for this function to work.
 * No connection iterators are invalidaded.
 *
 * \param Src		The ID of the vertex of the source of the connection
 * \param Dest		The ID of the vertex that is the destination of the connection
 * \param KindOfConn	A GraphTag, that describes the connection
 * \param payload	The payload of the connection, nullptr is also accepted
 *
 * \return 		An iterator to the newly created connection
 */
pgl_graph_t::ConnIterator_t
pgl_graph_t::createNewConnection(
		const pgl_vertexID_t&		Src,
		const pgl_vertexID_t&		Dest,
		const GraphTag 			KindOfConnection,
		ConnPayLoad_ptr 		payload) noexcept(false)
{
	//First testing if an insertion can be done
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Graph must be uncomplete to create a vertex.");
	};

	//Test if the ID are valid
	if(Src.isInvalid() || Dest.isInvalid())
	{
		throw PGL_EXCEPT_InvArg("The Src or the Dest are inavlid IDs");
	};

	//Test if the Tag is sane
	if(!(GraphTag_isEdge(KindOfConnection) || GraphTag_isConnection(KindOfConnection)))
	{
		throw PGL_EXCEPT_InvArg("The provided tag, does not describes a connection.");
	};

	//Test if the vertices already exists
	if(this->containVertexID(Src) == false)
	{
		throw PGL_EXCEPT_LOGIC("The Src Vertex, does not exist yet.");
	};
	if(this->containVertexID(Dest) == false)
	{
		throw PGL_EXCEPT_LOGIC("The Dest vertex, does not exist yet.");
	};

#if 1
	//For now
	if(GraphTag_isLoopOne(KindOfConnection) == true || Src == Dest)
	{
		pgl_assert(false && "LoopOne not supported yet.");
		throw PGL_EXCEPT_InvArg("LoopOne not supported yet.");
	};
#endif



	//Create an edge ID
	const pgl_edgeID_t idOfTheNewConnection(Src, Dest, GraphTag::Connection | (GraphTag_isDirectedEdge(KindOfConnection) ? GraphTag::Directed : GraphTag::Undirected) );

	auto srcIt 	= this->getNode(Src);
	pgl_assert(srcIt->isValid());

	auto destIt 	= this->getNode(Dest);
	pgl_assert(destIt->isValid());


#ifndef PGL_GRAPH_ALLOW_MULTICONNECTIONS
	/*
	 * No multiconnection allowed
	 */
	if(m_connections.count(idOfTheNewConnection) != 0)
	{
		throw PGL_EXCEPT_InvArg("Eine verbindung darf nur einmal da sein.");
	};
#endif



	//Now we insert everything into the container.
	intConnIterator_t newlyCreatedConn = this->m_connections.emplace(
			std::piecewise_construct,
			std::forward_as_tuple(idOfTheNewConnection), 	// This is the key
			std::forward_as_tuple(&(*srcIt), &(*destIt), KindOfConnection, payload)
		);


	//Test if valid
	if(newlyCreatedConn->second.checkIntegrity() == false)
	{
		throw PGL_EXCEPT_LOGIC("The connection is not valid.");
	};


	//Adding the connections
	srcIt->add_connection(&(newlyCreatedConn->second));
	pgl_assert(1 == srcIt->count_if([&newlyCreatedConn](Edge_cref e) -> bool { return (&(newlyCreatedConn->second)) == e.getConnectionCPtr();}));

	//We must only add it, if it is not an loop one connection
	if(GraphTag_isLoopOne(KindOfConnection) == false)
	{
		destIt->add_connection(&(newlyCreatedConn->second));
		pgl_assert(1 == destIt->count_if([&newlyCreatedConn](Edge_cref e) -> bool { return (&(newlyCreatedConn->second)) == e.getConnectionCPtr();}));
	}


	// Counting
	if(newlyCreatedConn->second.isRandom() == true)
	{
		m_nRandConnections += 1;
		pgl_assert(m_nRandConnections <= m_connections.size());
	};


	return ConnIterator_t(newlyCreatedConn);	//Remember this is an internal iterator
};



/**
 * \brief	Removes a Connection
 *
 * This function removes a connection.
 * The connection is removed and the two edges, that referes to it, are also deleted.
 * In order to work, the graph must be in an incomplete state. Failing to meet this requirement is consireder an error.
 *
 * The connection is dealocated, since the connection is not observed, the user must ensure that all edge, outside the graph,
 * something that is discurated from, are deleted or not used anylonger.
 *
 * A bool is returned to indicate the success of the operation.
 * This operation can throw.
 * The other connection iterators are not invalidated.
 *
 * To identify the connection, a connection iterator must be presented.
 *
 * \param connectionToDispose		A connection iterator, that we dispose of.
 *
 * \return 				A type of ConnRemoveRet_t
 * \throw 				May throw.
 */
pgl_graph_t::ConnRemoveRet_t
pgl_graph_t::removeConnection(
	ConnIterator_t 			connectionToDispose_) noexcept(false)
{
	return this->prot_removeConnection(std::move(connectionToDispose_));
};


pgl_graph_t::ConnRemoveRet_t
pgl_graph_t::prot_removeConnection(
	ConnIterator_t 			connectionToDispose_) noexcept(false)
{
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("In order to remove a connection, the graph must be uncomplete.");
	};

	//get an internal connection iterator
	intConnIterator_t ConnectionToDelete = prot_convertToInternal_Conn(connectionToDispose_);

	//test if all is uncomplete to delete it
	if(ConnectionToDelete->second.isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("The connection is complete and can be deleted.");
	};
	if(ConnectionToDelete->second.getSrcNode().isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("The src node of the connection is complete and can be deleted.");
	};
	if(ConnectionToDelete->second.getDestNode().isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("The dest node of the connection is complete and can be deleted.");
	};

	//Remove the connections from the edges
	const bool removeStatSrc = this->prot_removeEdgeFromNode(ConnectionToDelete->second.getSrcID(), ConnectionToDelete);
	if(removeStatSrc == false)
	{
		throw PGL_EXCEPT_LOGIC("Could not remove the connection from the src.");
	};

	if(ConnectionToDelete->second.isLoopOne() == false)
	{
		//Only do it, if the connection is not a loop one

		const bool removeStatDest = this->prot_removeEdgeFromNode(ConnectionToDelete->second.getDestID(), ConnectionToDelete);
		if(removeStatDest == false)
		{
			throw PGL_EXCEPT_LOGIC("Could not remove the connection from destination.");
		};
	};

	if(ConnectionToDelete->second.isRandom())
	{
		m_nRandConnections -= 1;
	};

	/*
	 * Remove the connection form this
	 */
	ConnectionToDelete = this->m_connections.erase(ConnectionToDelete); //ConnectionToDelete points now to the next one.

	return std::make_pair(true, ConnIterator_t(ConnectionToDelete));
};



/*
 * \brief 	Removes a vertex
 *
 * This function removes a vertex.
 * The vertex must have degree zero, i.e. no incident edges, in order to work properly.
 *
 * The space of the vertex is deallocated.
 * The other nodeIterators are not affected.
 *
 * If one wants to delete more than one elements, the elements should be processed in decreasing ID.
 * Since the vector must reorder itself.
 * So deleiting the last element is constant, but deleting the element p is linear in the distanc between p and last.
 *
 * \param vertexToDispose		The vertex that sould be removed.
 *
 * \return				Bool to indicate success.
 *
 * \throw				You can bet your cookie.
 */
pgl_graph_t::NodeRemoveRet_t
pgl_graph_t::removeVertex(
	NodeIterator_t 		vertexToDispose_) noexcept(false)
{
	return this->prot_removeVertex(std::move(vertexToDispose_));
};


pgl_graph_t::NodeRemoveRet_t
pgl_graph_t::prot_removeVertex(
	NodeIterator_t 		vertexToDispose_) noexcept(false)
{
	//Convert the vertex into an internal iterator
	intNodeIterator_t vertexToDispose = prot_convertToInternal_Node(vertexToDispose_);

	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Zhe graph is complete, so the vertex can't be deleted.");
	};

	if(vertexToDispose->isValid() == false || vertexToDispose->isComplete())
	{
		throw PGL_EXCEPT_LOGIC("The vertex that should be removed is not uncomplete or invalid.");
	};

	if(vertexToDispose->degree() != 0)
	{
		throw PGL_EXCEPT_LOGIC("The vertex that should be deleted has not degree zero.");
	};

	//make a compy of the ID to use it afterwards
	const pgl_vertexID_t idOfDisposed = vertexToDispose->getID();

	//Bookkeeping
	if(vertexToDispose->isRandom() == true)
	{
		m_nRandVertices -= 1;
	};

	//Now it is time to remove the vertex
	vertexToDispose = this->m_vertices.erase(vertexToDispose); 	//Now points to the one after deleted one

	//Return the result, check if the vertex is inside
	return std::make_pair(this->containVertexID(idOfDisposed) == false ? true : false, vertexToDispose);
};

/**
  * \brief	Removes the "last" vertex of *this.
  *
  * In general, the last vertex is the vertex with the highest ID that is stored inside *this.
  * So this function removes this vertex.
  * The requieremts are the same as the the one of the normal removfunction.
  * But this function is faster.
  *
  * If no vertex exists, then an exception is thrown.
  */
bool
pgl_graph_t::removeLastVertex() noexcept(false)
{
	if(m_vertices.size() != 0)
	{
		throw PGL_EXCEPT_OutOfBound("There are no vertices in the graph, so no last element.");
	};

	return this->prot_removeVertex( this->m_vertices.begin() + m_vertices.size() - 1 ).first;
};



/*
 * \brief 		Remove Random
 *
 * This functions removes all the random stuff.
 * It iterates through all the connections, if one of them is random, it is removed.
 * Then the vertices are proccessed the same.
 *
 * Notice, that an original vertex, with degree Zero is not removed.
 *
 * If a random vertex is found, that does not have degree zero, an exeption is generated.
 *
 * The graph must be uncomplete to work.
 */
void
pgl_graph_t::removeRandomComponents() noexcept(false)
{
	//Test if this is the graph is uncomplete
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Graph is not uncomplete.");
	};

	/*
	 * First we go through all the connections and delete them.
	 */

	//Test if we have random connections
	if(this->nRandConnections() > 0)
	{
		// We have random connections
		const ConnIterator_t conn_end = this->m_connections.end();	//This is the end iterator
		ConnIterator_t conn_it 	    = this->m_connections.begin();	//This is the current iterator
		while(conn_it != conn_end)
		{
			//Test if the connection is a random connection
			if(conn_it->isRandom() == true)
			{
				bool success = false;

				//The connection is random so we must delete it
				std::tie(success, conn_it) = this->prot_removeConnection(conn_it);

				//Test if there is a success
				if(success == true)
				{
					//Tere was success so we can continue
					//The remove function allredy incremented the iterator
					continue;
				}
				else
				{
					//There was no success. PANIC
					throw PGL_EXCEPT_LOGIC("It was not possible to remove the random connection");
				};

			}; //End if(random)


			//Since we are here, the connection was not random, so we must increment the iterator by hand.
			++conn_it;
		}; //End while
	}; //End of test if



	//Test if all is well
	if(std::any_of(m_connections.cbegin(), m_connections.cend(),
			[](const auto& c) -> bool { return c.second.isRandom(); }) )
	{
		throw PGL_EXCEPT_LOGIC("There where supposed to be no random connections anymore, but we found some");
	};


	/*
	 * Now we have cleaned the connections, so now we clean the vertex.
	 *
	 * If this code fails, please check this.
	 */


	//Test if we have random vertices
	if(this->nRandVertices() > 0)
	{
		// First test if the end of the vertices are random, and clence them first
		while(m_vertices.back().isRandom() == true)
		{
			if(m_vertices.back().degree() != 0)
			{
				throw PGL_EXCEPT_LOGIC("After we have disposed all ranom connections there are random graphs, that still have some connections.");
			};

			this->removeLastVertex();	//Remove last one
		};


		//Now we delete the random vertices
		const intNodeIterator_t node_end = this->m_vertices.end();
		intNodeIterator_t 	node_it  = this->m_vertices.begin();
		while(node_it != node_end)
		{
			if(node_it->isRandom() == true)
			{
				//The vertex is random, so we must delete it

				bool success = false;
				std::tie(success, node_it) = this->prot_removeVertex(node_it); //Increment also

				if(success == false)
				{
					throw PGL_EXCEPT_LOGIC("The vertex could not be deleted");
				};

				//So we can continue, we don't need to increment, sicne this is also handled in the delete fucntion
				continue;
			}; //End if(ranom)

			//If we are here then we must increment the iterator
			++node_it;
		}; //end while

		if(std::any_of(m_vertices.cbegin(), m_vertices.cend(), [](const pgl_vertex_t& v) -> bool
			{ return v.isRandom();}))
		{
			throw PGL_EXCEPT_LOGIC("Not all random vertices where removed");
		};

	}; //End test if

	return;
};


/*
 * \brief	This functions clears the graph
 *
 * This function is used to clear the graph.
 * The graph must be uncomplete to work.
 * This function does not call the remove function for every single edge.
 *
 * Instead it calls the clear function of the underlying container.
 * Accoeinng to the standard this does not free memory, the container is still in possesion of the meory
 * Since pgl uses boost container, it is not fully clear to my, how they work.
 * There is at least no way yet, to completly free the memory.
 */
void
pgl_graph_t::clearTheGraph_DeleteEverything()
{
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Tried to clear an graph that was not uncomplete.");
	};

	m_vertices.clear();
	if(m_vertices.size() > 0)
	{
		throw PGL_EXCEPT_RUNTIME("Error in clearing the vertices.");
	};
	m_nRandVertices = 0;

	m_connections.clear();
	if(m_connections.size() > 0)
	{
		throw PGL_EXCEPT_RUNTIME("Error in clearing the connections.");
	};
	m_nRandConnections = 0;

	return;
};




PGL_NS_END(graph)
PGL_NS_END(pgl)


