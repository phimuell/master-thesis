/**
 * \file	pgl_graph/pgl_graph_graph.cpp
 *
 * \brief 	Defines some functions of the graph
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_utility.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>

#include <pgl_graph/pgl_graph_graphtag.hpp>


//#include <pgl_container/pgl_small_vector.hpp>
#include <pgl_container/pgl_vector.hpp>

//Include STL
#include <functional>
#include <memory>
#include <bitset>
#include <iterator>
#include <algorithm>


PGL_NS_START(pgl)
PGL_NS_START(graph)

/*
 * \brief		This function makes the grph complete
 *
 * This function iterates through all the components and makes it complete.
 * It also checkes if thes are valid.
 * If an unvalid object is found an exception is generated.
 *
 * A bool is returned that indicated success.
 *
 * \param Tag		Must be complete.
 */
bool
pgl_graph_t::makeComplete(
	const GraphTag 		Tag) noexcept(false)
{
	if(Tag != GraphTag::Complete)
	{
		throw PGL_EXCEPT_InvArg("To complete the graph the Tag must be complete.");
	};

#ifndef PGL_GRAPH_REDUCED_INTEGRITY
	//Before that we must test the integrity
	if(this->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to complete a graph that does not pass the integritycheck.");
	};
#endif

	//Test if the vertex are still sorted
	if(std::is_sorted(m_vertices.cbegin(), m_vertices.cend()) == false)
	{
		//THe vertices are not sorted
		throw PGL_EXCEPT_RUNTIME("The internal state of the graph is corrupted. The vertices are not sorted anymore.");
	};


	/*
	 * There is this problem that the graph permits arbitrary ids of nodes, as long as they are unique.
	 * But some parts of pgl does assume that the range is consequtive and starts with id 0.
	 * The problem needs more analysis, but for the time being, we enforce that the numbering is consecutive and starts at 0.
	 *
	 * In oder to enforce that we modify the complete function, since each graph operation assumes and checks if the graph is complete.
	 * So it is apropriate to enforce a condition here.
	 *
	 * We rely on the fact that in that case the higest ID is one less the number of vertices.
	 */
	if(this->nVertices() > 1)	//Only usefull if we have more than one vertices
	{
		//Test if it starts at zero
		if(m_vertices.front().getID() != 0)
		{
			throw PGL_EXCEPT_RUNTIME("Vertexnumbering does not start at zero.");
		};

		//Test if the numbering is consecutive
		if((m_vertices.size() - 1) != m_vertices.back().getID())
		{
			throw PGL_EXCEPT_RUNTIME("Non consecutive numberings of the verices was found.");
		};
	}; //End if: check if we have more than one veritces for the test
	/*
	 * END endforce consecutive numbering
	 */
	 
	 
	 /*
	  * Here we test if the graph is allready complete.
	  * If so we return with success
	  */
	 if(this->isComplete() == true)
	 {
		 //The graph is complete, so we can exited here
		 
		 // Check if this is realy is true
		 // Do never trust the user
		 pgl_assert(this->verifyCompletness() == true);
		 
		 
		 return true;
	 }; //End if: the grqaph is allready complete
	 
	 //Increment the revision index
	 m_revIndex.increment();


	//First complete the connections
	for(auto& conn : m_connections)
	{
		conn.second.makeComplete(Tag);

#ifndef PGL_GRAPH_REDUCED_INTEGRITY
		if(conn.second.isComplete() == false)
		{
			throw PGL_EXCEPT_LOGIC("Tried to complete a connection but it was not possible to complete it.");
		};
#endif
	}; //End for(conn): make connections complete


	//Second we complete the vertices
	for(Vertex_ref v : m_vertices)
	{
		v.makeComplete(Tag);

#ifndef PGL_GRAPH_REDUCED_INTEGRITY
		if(v.isComplete() == false)
		{
			throw PGL_EXCEPT_LOGIC("Tried to complete a vertex, but it didn't work.");
		};
#endif
	}; //End for(v): complete the vertices



	/*
	 * Save taht everything is complete
	 */
	m_isComplete = true;

	return true;
};



/*
 * \brief		This functions makes the graph uncomplete
 *
 * This function iterates through all the components and call the make uncomplete functions on them.
 * A bool is returned to indicate success.
 * If an invalid function is found an exception is generated.
 *
 * \param Tag		Must be uncomplete.
 */
bool
pgl_graph_t::makeUncomplete(
	const GraphTag&		Tag) noexcept(false)
{
	if(Tag != GraphTag::Uncomplete)
	{
		throw PGL_EXCEPT_InvArg("Tag passed to uncomplete is not the uncomplete tag");
	};
	
	
	//So we now check if the graph is allready uncomplete and if so we exited
	if(this->isComplete() == false)
	{
		//The graph is incomplete, as we want it
		
		// Check if this is realy is true
		// Do never trust the user
		pgl_assert(this->verifyCompletness() == false);
		
		return true;
	}; //End if: Graph was allready in the uncomplete state
	
	//Increment the revision index
	m_revIndex.increment();
		

	//First iterate throug the vertex and uncompletes them, also validate them
	for(Vertex_ref v : m_vertices)
	{
		if(v.isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("A vertex that is scheduled to be uncompleted, is invalid.");
		};

		//Uncomplete the vertex
		v.makeUncomplete(Tag);

		//Verify the uncomplete state
		if(v.isComplete() == true)
		{
			throw PGL_EXCEPT_LOGIC("A vertex was tried to be uncompleted but it didnt work.");
		};
	}; //End for(v): uncomplete the vertices


	//Now we iterate through the connections
	for(auto& c : m_connections)
	{
		if(c.second.isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("A Connection that was scheduled for uncompletion was found invalid.");
		};

		/*
		 * Since an vertex is required to incomplete the edge whhich must incomplete the vertex,
		 * it is an error if we found an vertex that is still complete
		 */

		if(c.second.isComplete() == true)
		{
			throw PGL_EXCEPT_LOGIC("An connection was found that was supposed to be uncompled (by the vertex).");
		};
	}; //End for(c): Uncomplete the connections

	/*
	 * Update the completion status
	 */
	m_isComplete = false;

	return true;
};



/*
 * \brief 		This function makes an integroty check of the graph
 *
 * This is an integrity check, iit ferst checks the integrity of any connection.
 * Then it checks the integrity of any vertex (which will in turn check the validity of the connectoion again).
 */
bool
pgl_graph_t::checkIntegrity() const
{
	Size_t vertexCounter = 0;
	const bool VertexState = std::all_of(m_vertices.cbegin(), m_vertices.cend(),
		[&vertexCounter](Vertex_cref v) -> bool { vertexCounter += v.isRandom() ? 1 : 0; return v.checkIntegrity(); } );

	pgl_assert(VertexState);
	pgl_assert(vertexCounter == m_nRandVertices);

	if(VertexState == false)
	{
		return false;
	};
	if(vertexCounter != m_nRandVertices)
	{
		return false;
	};

	Size_t connectionCounter = 0;
	const bool ConnectionSatet = std::all_of(m_connections.cbegin(), m_connections.cend(),
		[&connectionCounter](const auto& conn) -> bool { connectionCounter += conn.second.isRandom() ? 1 : 0; return conn.second.checkIntegrity(); } );

	pgl_assert(ConnectionSatet);
	pgl_assert(connectionCounter == m_nRandConnections);

	if(connectionCounter != m_nRandConnections)
	{
		return false;
	};

	return ConnectionSatet;
};




bool
pgl_graph_t::verifyCompletness() noexcept
{

#warning "Maybe checnge this to verifyCompleteness of the vertices."
	const bool VertexState = std::all_of(m_vertices.cbegin(), m_vertices.cend(),
		[](Vertex_cref v) -> bool { return v.isComplete(); } );

	if(VertexState == false)
	{
		//Not all vertex are complete, so we don't need to check the connections.
		m_isComplete = false;
		return false;
	};

	//So the vertices are complete, so we must check the connections.
	const bool ConnectionState = std::all_of(m_connections.cbegin(), m_connections.cend(),
		[](const auto& conn) -> bool { return conn.second.isComplete(); } );

	//We can simply assing it
	m_isComplete = ConnectionState;


	return this->isComplete();
};

pgl_graph_t::ConnRefContainer_t
pgl_graph_t::getConnReferences()
{
	throw PGL_EXCEPT_illMethod("This method is currently not supported.");

	ConnRefContainer_t referencesToConn;
	referencesToConn.reserve(m_connections.size());

	for(auto& conns : m_connections)
	{
		referencesToConn.push_back(std::ref(conns.second));
	};

	return referencesToConn;
};



void
pgl_graph_t::setTargetNodeTo(
	const pgl_vertexID_t& 	newTarget)
{
	pgl_assert(newTarget.isValid() );
	for(auto& v : m_vertices)
	{
		pgl_assert(v.isValid());
		v.getPayload()->setCurrentTargetNodeID(newTarget);
	}; //End for(v): iterating over all vertices

	return;
}; //End functions setTargetNodeTo







PGL_NS_END(graph)
PGL_NS_END(pgl)


