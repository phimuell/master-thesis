/**
 * \file 	pgl_graph/pgl_graph_graph_internal.cpp
 * \brief 	Implements internal functions for the graph
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl/pgl_aglorithm.hpp>
#include <pgl_stable_vector.hpp>
#include <pgl_multimap.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>

#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>



PGL_NS_START(pgl)
PGL_NS_START(graph)

/*
 * \brief 	This function removes the presed connection from a vertex.
 *
 * \param ID	The ID of the node from which we want delete.
 * \param Conn	The connection we want delete (intConnIterator)
 *
 * This function is meant to be used as internal fuinction.
 * It assumes that all the requirements are meet tho dispose the connection.
 * It does not remove the connection_from the collection of connections that the graph stores.
 *
 * \return 	It returns true on success
 */
bool
pgl_graph_t::prot_removeEdgeFromNode(
		const pgl_vertexID_t&		ID,
		const_intConnIterator_t 	ConnIt)
{
	pgl_assert(ID.isValid() == true);
	pgl_assert(ConnIt->second.hasID(ID) == true);
	pgl_assert(this->containVertexID(ID) == true);

	//Get the relevant node
	Vertex_ref v = *(this->getNode(ID));

	//This is the predicate that is used to find the connection
	auto findPred = [&ConnIt](Edge_cref e) -> bool { return (&(ConnIt->second)) == e.getConnectionCPtr(); };

	//Find the edge that corresponds to the connection
	Vertex_t::EIterator_t CorrectEdge = v.find_first_of(findPred);
	pgl_assert(CorrectEdge != v.EEnd());

	/*
	 * Remove the erge from the vertex, but don't delete it
	 */
	const bool removeSuccess = v.remove_conection(CorrectEdge);


	//Test if the connection was removed completely
	pgl_assert(removeSuccess && v.EEnd() == v.find_first_of(findPred));

	//Now we remove it
	return removeSuccess;
};


/**
 * \brief	Create a vertex (internal)
 *
 * This function creates a new vertex, with the given arguiments
 * an Iterator to the newly construct vertex is returned.
 *
 * Remener that the vertex aquire exclusive ownership of the pointer to the payload.
 * So no managment by the client is required.
 *
 * Other vertex iterators are not invalidated.
 *
 * \param ID		The ID of vertex (must be unique) inside the graph
 * \param KindOfNode	A GraphTag, taht describes the node to be
 * \param payload	A Pointer of type vertex pointer, nullptr is also accepted
 *
 * \return 		An iterator to the vertex.
 *
 * This function does the same as the public one, but does not make concistency checks.
 * This function is intended to be used, if many nodes are inserted right after another.
 */
pgl_graph_t::NodeIterator_t
pgl_graph_t::prot_createNewNode(
	const pgl_vertexID_t&		ID,
	const GraphTag			KindOfNode,
	VertexPayLoad_ptr 		payload) noexcept(false)
{

	//First testing if an insertion can be done
	if(this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("Graph must be uncomplete to create a vertex.");
	};

	//Test if the inputsAre valid
	if(
	      (ID.isInvalid()) 	//Test if the ID is valid
	   || (GraphTag_isVertex(KindOfNode) == false))
	{
		throw PGL_EXCEPT_InvArg("Kind of node is not an Vertex or ID is invalid.");
	};

	/*
	 * Find the location, where to insert
	 */
	//Initialize it with the end iterator, the naural place
	const_intNodeIterator_t toInsertPos = m_vertices.cend();

	//We must search only if there is more than one node
	if(m_vertices.size() != 0)
	{
		//There are nodes, so we must search for them

		//Test if it fist at the end
		if(m_vertices.back().getID() < ID)
		{
			//The node at the end has a lower ID than the new node, so we can the new one at the end
			toInsertPos = m_vertices.cend();
		}
		else
		{
			//The end is not the correct one, so we must search for it
			toInsertPos = std::lower_bound(m_vertices.begin(), m_vertices.end(), ID, std::less<void>());
		}; //End if
	}; //End if, there are nodes to insert

	//Test if the node is there
	if((toInsertPos != m_vertices.end()) && (toInsertPos->getID() != ID) )
	{
		throw PGL_EXCEPT_LOGIC("There is allready a vertex with the given ID.");
	};

	//Creating the vertex
	this->m_vertices.emplace(toInsertPos, ID, payload, KindOfNode);

	//Sreach for it again to veryfy if it is a valid vertex.
	NodeIterator_t newlyCreatedVertex = this->getNode(ID);

	if(newlyCreatedVertex == m_vertices.end())
	{
		throw PGL_EXCEPT_LOGIC("The vertex was created, but was not found.");
	};

	if(newlyCreatedVertex->isValid() == false && newlyCreatedVertex->getID() != ID)
	{
		throw PGL_EXCEPT_LOGIC("The Vertex, that was created was not valid.");
	};

	//Make accounting
	if(newlyCreatedVertex->isRandom() )
	{
		m_nRandVertices += 1;
		pgl_assert(m_nRandVertices <= m_vertices.size());
	};

	//Return the iterator, to the newly created vertex
	return newlyCreatedVertex;
};



PGL_NS_END(graph)
PGL_NS_END(pgl)


