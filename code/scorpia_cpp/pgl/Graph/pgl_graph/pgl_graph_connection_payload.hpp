#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_CONNECTION_PAYLOAD_HPP
#define PGL_GRAPH__PGL_GRAPH_CONNECTION_PAYLOAD_HPP

/**
 * \file pgl_graph/pgl_graph_colour.hpp
 * \brief This is the declaration of the colour class (NOT YET IMPLEMTED BUT DUMMY PROVIDED)
 *
 * This class represents a colour.
 * One should not use this class directly, instead use the its typedef in the config header.
 *
 * This class is not yet implemented.
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_graph/pgl_graph_config.hpp>


//Include STD
#include <string>

PGL_NS_START(pgl)
PGL_NS_START(graph)

/**
 * \brief this class represents a payload.
 *
 * This payload is stored inside the connection, but is used by the edge.
 *
 * Its interface is splitted into severel Properties (parts) that represents varius fields of graph theory.
 * Read the manual\footnote{Comming Soon^{TM}} for more information about the usage of them.
 * This interface can be instancied constructors are protected, and it belongs exclusively to an connection.
 *
 * The Propertys are:
 * 	\item[Flow]
 * 	Usie it when you do graph stuff that deals with flow.
 * 	This implicitly implements a capacity on its own.
 *
 * 	\item[Weight]
 * 	Provides a weight, which is a (signed) int quantity.
 *
 * 	\item[Matching]
 * 	This \emph{can} be used with matching.
 * 	It provides simply a boolean flag.
 *
 * 	\item[FractialMatching]
 * 	This is a fractional matching, in the end it provides simply variable of type Float_t
 * 	(Don't expect that it gets implemented before HURD 1.0)
 *
 * 	\item[Color]
 * 	Gives a colour.
 *
 * 	\item[Kleinberg]
 * 	Everything that has to do with Kleinberg model.
 */
class pgl_connection_payload_t
{

	/*
	 * Constructors
	 * All are declared private and defaulted, since this class is merly an interface.
	 */
protected:
//public:

	///Defaulted default constructor
	pgl_connection_payload_t();

	///Defaulted copy constructor
	pgl_connection_payload_t(
		const pgl_connection_payload_t&);

	///Defaulted moveconstructor
	pgl_connection_payload_t(
		pgl_connection_payload_t&&);

	///Defaulted copy assignment
	pgl_connection_payload_t&
	operator= (
		const pgl_connection_payload_t&);

	//Defaulted move assignment
	pgl_connection_payload_t&
	operator= (
		pgl_connection_payload_t&&);

public:
	///The destructor is declared virtual
	virtual ~pgl_connection_payload_t() = 0;


	/*==============================================================
	 * FLOW:
	 *
	 * Here is the flow part.
	 * All this function has the implicit preconditionm, that *this implements the flow
	 * Contract.
	 */
public:

#if 0
	/**
	 * \brief FLOW: Test if has flow property
	 *
	 * This function returns true if *this implemented the flow contract.
	 */
	virtual
	bool
	_implements_FLOW() const;


	/**
	 * \brief FLOW: Used for initialization
	 *
	 * This is a way of initializing the flow property.
	 * It takes a string for universialy
	 *
	 * \param initState A string that is parsed to initializing the flow property
	 */
	virtual
	void
	init_FLOW(
		const std::string& initState);


	/**
	 * \brief FLOW: This function sets the flow the the provided value
	 *
	 * \param newFlow, the new flow value of *this
	 */
	virtual
	void
	setFlowTo(
		const Flow_t&);


	/**
	 * \brief FLOW: This function returns the flow through the connection.
	 *
	 * The edges passes itself as pointer to this function.
	 * This is done to recover the sign of the flow.
	 * If this is called by a connection the nullpointer is passed.
	 *
	 * \param this_edge The edge to which the payload belongs to. (cann be NULL)
	 */
	virtual
	Flow_t
	getFlow(
		const pgl_edge_t* const this_edge) const;


	/**
	 * \brief FLOW: Returns the capacity of the edge
	 *
	 * This returns the natural capacity of this, not the residual capacity
	 */
	virtual
	Capacity_t
	getFlowCapacity() const;


	/**
	 * \brief FLOW: sets the capacity of this
	 *
	 * Sets the flow capacity opf this to the given value
	 *
	 * \param newFlowCapacity The new value of the capacity
	 */
	virtual
	void
	setFlowCapacity(
		const Capacity_t& newFlowCapacity);



	/**
	 * \brief FLOW: Retruns the residual capacity
	 *
	 * Returns the residual capaxcity of *this
	 *
	 * \param this_edge passes the calling edge (can be null)
	 */
	virtual
	Flow_t
	getResidualCapacity(
		const pgl_edge_t* const this_edge) const;



	/*
	 * END FLOW
	 * ===========================================================
	 */
#endif

	/*
	 * =================================================================
	 * Kleinberg
	 *
	 * Here is the part that is used for the Kleinberg part.
	 */
public:
	/**
	 * \brief KLEINBERG: Test if implemnted the kleinberg part
	 */
	virtual
	bool
	_implements_KLEINBERG() const;


	/**
	 * \brief KLEINBERG: This function sets the new
	 */



	/*
	 * END KLEINBERG
	 * ================================================================
	 */



#if 1
	/*
	 * ================================================================
	 * Matching
	 *
	 * This is a boolean property
	 */
public:
	/**
	 * \brief MATCHING:	Test if *this implements matching
	 */
	virtual
	bool
	_implements_MATCHING() const;


	/**
	 * \brief MATCHING:	Returns true is *this is matched
	 */
	virtual
	bool
	isMatched() const;


	/**
	 * \brief MATCHING:	Match *this, no effect if allready matched
	 *
	 * \return Returns the matched state
	 */
	virtual
	bool
	makeMatched();


	/**
	 * \brief MATCHED:	Unmatch *this, no effect if not matched
	 *
	 * \return Returns the matched state before the call
	 */
	virtual
	bool
	makeUnmatched();

	/**
	 * \brief MATCHED:	Set the matched state to the probided state
	 *
	 * \param newState	The state *this should have
	 */
	virtual
	void
	setMatchedTo(
		const bool newState);


	/*
	 * END MATCHED
	 * ==========================================
	 */
#endif





	/*
	 * ===============================================================
	 * Weight
	 *
	 * This property is for the weight.
	 */
public:
	/**
	 * \brief WEIGHT:	Test if whight is implemented
	 */
	virtual
	bool
	_implements_WEIGHT() const;


	/**
	 * \brief WEIGHT:	Sets the wehiht of *this to the given newWeight
	 *
	 * \param newWeight	The new weight of *this
	 */
	virtual
	void
	setWeightTo(
		const Weight_t& newWeight);


	/**
	 * \brief WEIGHT:	Returns the weight of *this
	 */
	virtual
	Weight_t
	getWeight() const;

	/*
	 * END WEIGHT
	 * =============================================================
	 */

#if 0
	/*
	 * ============================================================
	 * BEGIN COLOUR
	 */
public:
	/**
	 * \brief COLOUR:	Tests if *this implements colour
	 */
	virtual
	bool
	_implements_COLOUR() const;


	/**
	 * \brief COULOR:	Returns the colour of *this
	 */
	virtual
	pgl_colour_t
	getColour() const;


	/**
	 * \brief COLOUR:	Set the colour of *this to newCol
	 *
	 * \param  newCol	The new colour
	 */
	virtual
	void
	setColourTo(
		const pgl_colour_t& newColour);



	/**
	 * \brief COLOUR:	Test if the colour of *this matches MayBe
	 *
	 * \param MayBe		The colour the user whant to test
	 */
	virtual
	void
	hasColour(
		const pgl_colour_t& MayBe) const;


	/**
	 * \brief COLOUR:	Test if this is withe
	 */
	virtual
	bool
	isWithe() const;


	/*
	 * \brief COLOUR:	Test if *this is Black
	 */
	virtual
	bool
	isBlack() const;

	/**
	 * \brief Colour:	Test id *this is Blue
	 */
	virtual
	bool
	isBlue() const;


	/**
	 * \brief COLOUR:	Test if this is red
	 */
	virtual
	bool
	isRed() const;

	/*
	 * END COLOUR
	 * ===========================================================
	 */
#endif


	/*
	 * ====================================
	 * Begin Kleinberg Core
	 */


	/**
	 * \brief	This function returns the the delta between the owning node and the other node to the target distance.
	 *
	 * This means that negative values means that the other node, are nearer towards the target node, so this is good.
	 * If the ceooection is unpassable, like an incoming directed connection at the owning node an exception is generated.
	 *
	 * \throw	If the connection is not craoosable
	 *
	 * \param	ConsideredEdge
	 */
	virtual
	Distance_t
	getRoutingDistanceDelta(
		const pgl_edge_t& 	ConsideredEdge) const;

	/*
	 * END Kleinberg Core
	 * ===========================
	 */




}; //End class(pgl_connection_payload_t)







PGL_NS_END(graph)
PGL_NS_END(pgl)




#endif //End include guard
