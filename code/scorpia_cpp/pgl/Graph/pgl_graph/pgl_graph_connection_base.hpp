#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_CONNECTION_BASE_HPP
#define PGL_GRAPH__PGL_GRAPH_CONNECTION_BASE_HPP
/**
 * \file pgl_graph/pgl_graph_connection_base.hpp
 * \brief This file defines a copmmen baseclass for base and connection.
 *
 * Both edge and connection has some boolean state.
 * To shrink the requirement of them, a bitset is used.
 * To avoid confiusion about the numbering, named constans are used.
 * Since both of them are descrabed simillar things, we define them in a comman baseclass.
 */

//PGL-Stuff
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>


//STD-Stuff
#include <bitset>
#include <type_traits>


PGL_NS_START(pgl)
PGL_NS_START(graph)




/**
 * \brief commen basclass of connection and edge
 *
 * This class stores the common things of an edge and a connection.
 * they are mainly static data, so it makes sens to put them togeter in a base class.
 * This base class declares only protected members, and shall not be instanzieted.
 */
template<Size_t N>
class pgl_connection_base_t
{

	/*
	 * Typedef of the used bitfileds
	 *
	 * Position 0 is explicitly defined for the case where we whant to indicate that the base was done by a proper build.
	 *
	 * The idea bhind this aproace is that we program by implication. So if we use a proper method to buiild some a connection ar an edge, then we
	 * should propagate this fact.
	 */
protected:
	using State_t = std::bitset<N + 1>;


	/*
	 * Now we defines the diferent boolean states for the CONNECTION
	 * If a field in the set is true, then this means that the connection/edge fullfiels this property.
	 */
protected:
	//======= Connection State =====//
	/// The connection is a directed connection
	static constexpr uInt8_t ce_isDirectedConnection = 1;

	/// The connection is part of the underlying Graph
	/// This means that *this is not a randome connection
	static constexpr uInt8_t ce_isOriginalConnection = 2;

	/// The connection is complete
	/// Some implementation details, means that the connection is ready
	static constexpr uInt8_t ce_isConnectionComplete = 3;

	/// The connection is a loop of length one, meaning that src and dest are the same
	/// This is a special kind of connection, that is an edge case, we allow it (manual says different)
	static constexpr uInt8_t ce_isLoopOneConnection = 4;

	//======= Edge State ====//
	/// Is the edge an incoming connection at the owning node
	static constexpr uInt8_t ce_EdgeIsIncomming = 5;

	/// The edge is an outgoing edge at the owning node
	static constexpr uInt8_t ce_EdgeIsOutgoing  = 6;

	/// The edge originates at the edgeID::srcID node
	static constexpr uInt8_t ce_EdgeOriginatedAtSrcID = 7;




	/*
	 * ==================================
	 * 	Implementation
	 * ==================================
	 */


	/**
	 * \brief 	Returns true, if *this was a proper build.
	 *
	 * The meaning of "proper build" is that we not constructed it by accident,
	 * insterad it was done intentionaly, with valid parameters.
	 * There is a constructor, that indicates a propper build.
	 */
	inline
	bool
	isProperBuild() const
	{
		return m_state.test(0);
	};



	/**
	* \brief enum that is used to give some syntactig sugar to directed and undirected
	* \enum ConnectionState
	*
	* This enum is declared protected.
	* It is used inside the this class, and its children to name some thing, they are used for everything.
	*/
protected:
	enum class ConnectionState
	{
		Undirected, 		//!< The connection is  an undirected connection
		Directed,   		//!< The connection is a directed connection
		Complete,   		//!< The connection is complete
		NotComplete,		//!< Opposite of complete
		OriginalConn,		//!< The connection is an original connection (belonging to the underling model)
		RandomConn,		//!< The connection is a random connection
		isLoopOne,		//!< The connection is a loop one, src == dest, there is no opposite to it, since this is an exception
		isIncomingEdge,		//!< The edge is incoming at the owning node which implies that the coonnection is undirected or in
					//!	case of a directed connection, the edge is bound to srcNode
		isLeavingEdge,		//!< The edge is a leaving connection at the owning onde, whcih implies that the the connection
					//!	is undirected, or the edge is bound to the destNode
		EdgeOriginSRC,		//!< The Edges belongs to the node which is associated with the srcID of the edgeID
		EdgeOriginDEST 		//!< The edges belongs to the node which is associated with the destOD of the edgeID
	}; //End enum(ConnectionState)


	/**
	 * Now we declare some function to access the state.
	 * first the setter, which are protected.
	 */

	/**
	 * \brief Set the status of directed ore undirected.
	 *
	 * \param state Inidcates the direction, is of type ConnectionState
	 *
	 * If the state is undirected, then then the state inoming and outgoing state are set automaticaly to true, if this is an edge
	 */
protected:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_isDirectedConnection, void>::type
	prot_setConnectionState(
		const ConnectionState& state)
	{
		pgl_assert(N == nn);


		if(this->isComplete() == true)
		{
			throw PGL_EXCEPT_LOGIC("Tried to change a property in connection base, during its complition state.");
		};


		switch(state)
		{
		  case ConnectionState::Directed:
		  	m_state.set(ce_isDirectedConnection, true);
		  	break;

		  case ConnectionState::Undirected:
		  	m_state.set(ce_isDirectedConnection, false);
		  	this->prot_setPointingState(ConnectionState::isIncomingEdge);
		  	this->prot_setPointingState(ConnectionState::isLeavingEdge);
		  	break;

		  default:
		  	pgl_assert(false && "ConnectionBase::SetConnectionState(): Unknown state");
		  	m_state.test(-1); // Will fail
		  	break;
		};

		return;
	}; //end


	/**
	 * \brief Returns true if the connection is directed
	 */
public:
	bool
	isDirected() const
	{
		return (m_state.test(ce_isDirectedConnection) == true) ? true : false;
	};


	/**
	 * \brief Returns true if the connection is undirected
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_isDirectedConnection, bool>::type
	isUndirected() const
	{
		assert(N == nn);
		return (m_state.test(ce_isDirectedConnection) == false) ? true : false;
	};




	/**
	 * \brief Set if it is original
	 *
	 * \param state sets to the provided state
	 */
protected:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_isOriginalConnection, void>::type
	prot_setOriginalState(
		const ConnectionState& state)
	{
		pgl_assert(N == nn);


		if(this->isComplete() == true)
		{
			throw PGL_EXCEPT_LOGIC("Tried to change a property in connection base, during its complition state.");
		};


		switch(state)
		{
		  case ConnectionState::OriginalConn:
		  	m_state.set(ce_isOriginalConnection, true);
		  	break;

		  case ConnectionState::RandomConn:
		  	m_state.set(ce_isOriginalConnection, false);
		  	break;

		  default:
		  	pgl_assert(false && "ConnectionBase::SetOriginalState(): Unknown state");
		  	m_state.set(-1, true); //Will fail
		  	break;
		}; //End switch(state)

		return;
	};


	/**
	 * \brief Returns true if this is a original connection
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if< nn >= ce_isOriginalConnection, bool>::type
	isOriginalConnection() const
	{
		pgl_assert(N == nn);
		return (m_state.test(ce_isOriginalConnection) == true) ? true : false;
	};

	template<Size_t nn = N>
	typename std::enable_if< nn >= ce_isOriginalConnection, bool>::type
	isOriginal() const
	{
		pgl_assert(N == nn);
		return (m_state.test(ce_isOriginalConnection) == true) ? true : false;
	};


	/**
	 * \brief Returns true if this is a random connection
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if< nn >= ce_isOriginalConnection, bool>::type
	isRandomConnection() const
	{
		pgl_assert(N == nn);
		return (m_state.test(ce_isOriginalConnection) == false) ? true : false;
	};

	template<Size_t nn = N>
	typename std::enable_if< nn >= ce_isOriginalConnection, bool>::type
	isRandom() const
	{
		pgl_assert(N == nn);
		return (m_state.test(ce_isOriginalConnection) == false) ? true : false;
	};




	/**
	 * \brief Make this complete
	 *
	 * \param state This is the new state *this should have
	 */
protected:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_isConnectionComplete, void>::type
	prot_setCompleteState(
		const ConnectionState& state)
	{
		pgl_assert(N == nn);

		switch(state)
		{
		  case ConnectionState::Complete:
			m_state.set(ce_isConnectionComplete, true);
			break;

		  case ConnectionState::NotComplete:
		  	m_state.set(ce_isConnectionComplete, false);
		  	break;
		  default:
		  	pgl_assert(false && "ConnectionBase::SetConnectionState(): Unknown state");
		  	m_state.test(-1); //Will fail
		  	break;
		};

		return;
	};


	/**
	 * \brief Returns true if this is complete
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_isConnectionComplete, bool>::type
	isComplete() const
	{
		pgl_assert(nn == N);
		return (m_state.test(ce_isConnectionComplete) == true) ? true : false;
	};


	/**
	 * \brief Make this an loop one connection
	 *
	 * Meaning that the src and dest are the same.
	 * This is a special case, so this might not work properly.
	 * There is alo no opposite of the isLoopOne state
	 */
protected:
	template<Size_t nn = N >
	typename std::enable_if<nn >= ce_isLoopOneConnection, void>::type
	prot_setLoopOne(
		const ConnectionState& state)
	{
		pgl_assert(nn == N);

		if(this->isComplete() == true)
		{
			pgl_assert(false);
			throw PGL_EXCEPT_LOGIC("Tried to change a property in connection base, during its complition state.");
		};

		switch(state)
		{
		  case ConnectionState::isLoopOne:
		  	m_state.set(ce_isLoopOneConnection, true);
		  	break;

		  default:
		  	pgl_assert(false && "ConnectionBase::SetLoopOneState(): Unknown state");
		  	m_state.test(-1); //Will fail
		  	break;
		};

		return;
	};


	/**
	 * \brief Returns true if this is a loop one connection
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_isLoopOneConnection, bool>::type
	isLoopOne() const
	{
		pgl_assert(N == nn);
		return (m_state.test(ce_isLoopOneConnection) == true) ? true : false;
	};








	/**
	 * \brief Set the originate status of the edge.
	 *
	 * As described in the manual, an edge, but not associated connection, belonges to an node.
	 * With this function, one can set this relöation ship.
	 *
	 * \param state the new connection state
	 */
protected:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_EdgeOriginatedAtSrcID, void>::type
	prot_setOriState(
		const ConnectionState& state)
	{
		pgl_assert(N == nn);

		if(this->isComplete() == true)
		{
			throw PGL_EXCEPT_LOGIC("Tried to change a property in connection base, during its complition state.");
		};


		switch(state)
		{
		  /*
		   * This is the case, where the EDGE, belonges to the node that is identified by the */

		  case ConnectionState::EdgeOriginSRC:
		  	m_state.set(ce_EdgeOriginatedAtSrcID, true);
		  	break;

		  case ConnectionState::EdgeOriginDEST:
		  	m_state.set(ce_EdgeOriginatedAtSrcID, false);
		  	break;

		  default:
		  	pgl_assert(false && "ConnectionBase::SetOriginState(): Unknown state");
		  	m_state.test(-1); //Will fail
		  	break;
		};

		return;
	};


	/**
	 * \brief returns true if this connection originates at the srcID
	 *
	 * The case of a loop one connection was once handled seperatly but I give up on that idea.
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_EdgeOriginatedAtSrcID, bool>::type
	isOrininAtSrc() const
	{
		static_assert(N == nn, "N != nn");
		return (m_state.test(ce_EdgeOriginatedAtSrcID) == true);
	};




	/**
	 * \brief returns true if this connection originates at the destID
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_EdgeOriginatedAtSrcID, bool>::type
	isOrininAtDest() const
	{
		return (m_state.test(ce_EdgeOriginatedAtSrcID) == false) ? true : false;
	};



	/**
	 * \brief 	With this function the user can set the leaving and incoming state of the
	 * 		connection. thows two propertys are the same.
	 *
	 * The user can expect that if the connection is undirected both of the queries return true.
	 */
protected:
	template< Size_t nn = N>
	typename std::enable_if<nn >= ce_EdgeIsIncomming, void>::type
	prot_setPointingState(
		const ConnectionState& State)
	{
		static_assert(nn == N, "Not correct parameter");

		switch(State)
		{

		  case ConnectionState::isIncomingEdge:
			m_state.set(ce_EdgeIsIncomming, true);
			break;

		  case ConnectionState::isLeavingEdge:
		  	m_state.set(ce_EdgeIsOutgoing, true);
		  	break;

		  default:
		  	pgl_assert(false && "ConnectionBase : setPointingState unknown state.");
		  	break;
		};

		return;
	};


	/**
	 * \brief 	This function is for providing the possibility to set the state even for connections
	 * 		  meaniong the function exists and does nothing
	 */
protected:
	template<Size_t nn = N>
	typename std::enable_if< nn < ce_EdgeIsIncomming, void>::type
	prot_setPointingState(
		const ConnectionState& State)
	{
		static_assert(N == nn, "Not correct trmplate parameter.");

		(void)State;
		return;
	};




	/**
	 * \brief 	returns true is the connection is outgoing
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_EdgeIsIncomming, bool>::type
	isIncomingEdge() const
	{
		return m_state.test(ce_EdgeIsIncomming);
	};


	/**
	 * \brief 	returns true if this is a leaving connecection
	 */
public:
	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_EdgeIsIncomming, bool>::type
	isLeavingEdge() const
	{
		return m_state.test(ce_EdgeIsOutgoing);
	};

	template<Size_t nn = N>
	typename std::enable_if<nn >= ce_EdgeIsIncomming, bool>::type
	isOutgoingEdge()
	{
		return m_state.test(ce_EdgeIsOutgoing);
	};



	/*
	 * ===========================================================
	 * Constructors all are defaulted, because they do the right thing.
	 * Since this class is not intended to be use, even as base pointer, there
	 * is no virtual deconstructor.
	 */
protected:

	/// Default constructor. The bitset is initialized with all zero
	pgl_connection_base_t() noexcept = default;

	/// Copy Consrtructor
	pgl_connection_base_t(
		const pgl_connection_base_t&) noexcept = default;

	/// Move constructor
	inline
	pgl_connection_base_t(
		pgl_connection_base_t&& other) noexcept :
	  m_state(other.m_state)
	{
		m_state = other.m_state;
		pgl_assert(other.m_state == this->m_state);
	};

	/// Copy assignment
	pgl_connection_base_t&
	operator= (
		const pgl_connection_base_t&) noexcept = default;

	/// Move assignment
	inline
	pgl_connection_base_t&
	operator= (
		pgl_connection_base_t&& other) noexcept
	{
		m_state = other.m_state;
		pgl_assert(other.m_state == this->m_state);

		return *this;
	};

	/// Deconstructor
	~pgl_connection_base_t() = default;


	/**
	 * \brief	This constructor is used to indicate that *this is a proper build
	 *
	 * \param isProper If true, *this is aproper build
	 *
	 * A proper build is a build, that results in an object that is valid.
	 */
	pgl_connection_base_t(
		const bool isPorperBuild):
	  m_state()
	{
		m_state.reset();
		m_state.set(0, isPorperBuild);
	};

#if 0
	/**
	 * \brief 	This constructor is a special copy constructor
	 * \param conBase	The connection base of the connection
	 *
	 * Its main use is to construct the base of a edge form the base of a connection.
	 * since they differ in length, the normal copy constructor is not suitable.
	 *
	 * This is only possible if the base of the connection is proper builded.
	 */
	template<Size_t N2>
	pgl_connection_base_t(
		const pgl_connection_base_t<N2>& conBase) noexcept :
	  m_state(conBase.to_ullong())
	{
		pgl_assert(conBase.isProperBuild());
	};
#endif

	/*
	 * =============================================
	 * Private Data Members
	 */
private:
	State_t 		m_state;	//The boolean state

}; //End class(pgl_connection_t)



PGL_NS_END(graph)
PGL_NS_END(pgl)


#endif //end include guard
