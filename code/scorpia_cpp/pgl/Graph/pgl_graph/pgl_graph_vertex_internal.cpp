/**
 * \brief	Implements internal (private) vector functions
 * \file	pgl_graph/pgl_graph_vertex_internal.cpp
 *
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_utility.hpp>

#include <pgl/pgl_aglorithm.hpp>

#include <pgl_graph/pgl_graph_graphtag.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>

#include <pgl_container/pgl_vector.hpp>

//Include STL
#include <functional>
#include <memory>
#include <bitset>
#include <iterator>


PGL_NS_START(pgl)
PGL_NS_START(graph)



bool
pgl_vertex_t::int_make_complete()
{


#ifndef PGL_GRAPH_REDUCED_INTEGRITY
	if(this->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_RUNTIME("Tried to complete an vertex, which fails the integfritycheck.");
		//return false;
	};
#endif


#if 1
	// Test if we should resize the vector that stores everything
	if(m_neigbourhood.capacity() >= 10)
	{
		//We may do something
		if(m_neigbourhood.capacity() > 2 * m_neigbourhood.size())
		{
			//We do something
			m_neigbourhood.shrink_to_fit();
		};
	};
#endif

	//Now we sort them according to the others neighbour
	auto sortPred = [] (Edge_cref lhs, Edge_cref rhs) -> bool { return lhs.getOtherNodeID() < rhs.getOtherNodeID();};
	//std::stable_sort(m_neigbourhood.begin(), m_neigbourhood.end(), sortPred);
	if(std::is_sorted(m_neigbourhood.cbegin(), m_neigbourhood.cend(), sortPred) == false)
	{
		//The neigbourhood is not sorted, so we must sort it
		//We do not use the stable sort anymore
		std::sort(m_neigbourhood.begin(), m_neigbourhood.end(), sortPred);
	};

	for(Edge_ref e : m_neigbourhood)
	{
		e.makeComplete(GraphTag::Complete);

#ifndef PGL_GRAPH_REDUCED_INTEGRITY
		//HIER NOCH EINEN VALID TEST
		if(e.isComplete() == false)
		{
			throw PGL_EXCEPT_LOGIC("Could not complete the edges incident on the vertex.");
		};
#endif
	}; //End for(e): make the edges complete


	/*
	 * Set the internal bool to true to save that this is complete
	 */
	m_state.set(ce_isComplete, true);

#if defined(PGL_NDEBUG)
	return true;
#else
	const bool sortState = std::is_sorted(m_neigbourhood.cbegin(), m_neigbourhood.cend(), sortPred);
	m_state.set(ce_isComplete, sortState);
	return sortState;
#endif
};


bool
pgl_vertex_t::int_make_uncomplete()
{
	//Is allready incomplete
	if(this->isComplete() == false)
	{
		return true;
	};

	if(this->isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to incomplet an invalid ege. But the real problem is, thet the edge got complete in the first place.");
	};

	for(Edge_ref e : m_neigbourhood)
	{
		e.makeUncomplete(GraphTag::Uncomplete);
		if(e.isComplete() == true)
		{
			throw PGL_EXCEPT_LOGIC("Could not uncomplete the edges incident on the vertex.");
		};
	}; //End for(e): make the edges complete


	m_state.set(ce_isComplete, false);

	return this->isComplete() == false;
};






std::pair<bool, pgl_vertex_t::NHIterator_t>
pgl_vertex_t::int_searchEdge(
	const pgl_vertexID_t& 	ID,
	const Size_t& 		n)
{
	//This is because in the graph counting starts with zero, but the new function nees to return the first one
	auto found = pgl::find_if_n(m_neigbourhood.begin(), m_neigbourhood.end(), [ID](Edge_cref x) -> bool { return ID == x.getOtherNodeID();}, n + 1);
	return std::make_pair(found != m_neigbourhood.end(), std::move(found) );
};


std::pair<bool, pgl_vertex_t::const_NHIterator_t>
pgl_vertex_t::int_searchEdge(
	const pgl_vertexID_t& 	ID,
	const Size_t& 		n) const
{
	//Also new counting rule in the find if structure
	auto found = pgl::find_if_n(m_neigbourhood.cbegin(), m_neigbourhood.cend(), [ID](Edge_cref x) -> bool { return ID == x.getOtherNodeID();}, n + 1);
	return std::make_pair(found != m_neigbourhood.end(), std::move(found) );
};

std::pair<bool, pgl_vertex_t::const_NHIterator_t>
pgl_vertex_t::int_searchEdgeC(
	const pgl_vertexID_t& 	ID,
	const Size_t& 		n) const
{
	return this->int_searchEdge(ID, n);
};


bool
pgl_vertex_t::int_testEdge(
	Edge_cref e) const
{
	bool result = true;

	result &= e.getOwningNode() == (*this);
	pgl_assert(result);

	result &= e.isProperEdge();
	pgl_assert(result);

	result &= e.checkIntegrity();
	pgl_assert(result);


	pgl_assert(result);

	if(e.isLoopOne() == true)
	{
		result &= e.getOtherNode() == *this;
		pgl_assert(result);
	};

	if(e.isDirected() == true)
	{
		if(e.isOrininAtSrc() == true)
		{
			result &= e.getSrcNode() == *this;
			pgl_assert(result);
		};

		if(e.isOrininAtDest() == true)
		{
			result &= e.getDestNode() == *this;
			pgl_assert(result);
		};
	};

	return result;
};



PGL_NS_END(graph)
PGL_NS_END(pgl)


