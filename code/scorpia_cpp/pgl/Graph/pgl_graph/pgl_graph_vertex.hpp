#pragma once
#ifndef PGL_GRAPH___PGL_VERTEX_HPP
#define PGL_GRAPH___PGL_VERTEX_HPP

/**
 * \brief	Declares the vector
 * \file	pgl_graph/pgl_graph_vertex.hpp
 *
 * This file declares the class that is used to represent a vector.
 * unlike the connection and the edge, it tries to avoid inline function,
 * since it supports more highlevel oerations, that are a bit heavy.
 *
 * It provides an interface to query the most needed informations.
 * It is also responsible for creating edges.
 *
 * The Vertex is unmovable and uncopable, because the whole structure depends on the fact that we have pointers to them.
 *
 * There is another very subtile issue here.
 * There can be more than one connection between two nodes.
 * Since we add random nodes, to be generall, we do not restrict such things.
 * Instead we allow this.
 * So the vertex storred its incident edges like a 'multiset', but we don't use one.
 * We stick with the vector.
 * The edge are sorted in ascending otherID.
 * The user can't expect any particular ordering, but it is very likely that the relativ ordering of insertion is preserved.
 * The getter are defaulted to return a particular connection (started by 0).
 *
 * In the furture a reference set will be introduced.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_preprocessor.hpp>

#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>

#include <pgl_container/pgl_vector.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_graph/pgl_graph_edge.hpp>

#include <pgl_graph/pgl_graph_vertex_iterator.hpp>
#include <pgl_graph/pgl_graph_vertex_payload.hpp>


//Include STL
#include <functional>
#include <memory>
#include <bitset>
#include <tuple>


PGL_NS_START(pgl)
PGL_NS_START(graph)



class pgl_vertex_t
{
	/*
	 * Some typedefs
	 */
protected:
	using iPayload_t		= std::unique_ptr<pgl_vertex_payload_t>; 	//!< The internal Type of the payload
	using State_t			= std::bitset<8>;				//!< This is the type that represents the internal state of this
											//!<  The positions are encoded by using statics

	//Payload
public:
	using Payload_ptr 		= pgl_vertex_payload_t*;			//!< This si the const payload pointer for the user
	using Payload_cptr 		= const pgl_vertex_payload_t*;			//!< Pointer to a constant payload
	using PayLoad_cptr 		= Payload_cptr;

	//Vertex
	using Vertex_ptr 		= ::pgl::graph::pgl_vertex_t*;			//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= const ::pgl::graph::pgl_vertex_t*;		//!< This is the pointer to a constant vertex
	using Vertex_ref		= ::pgl::graph::pgl_vertex_t&;			//!< This is the type of a reference to a node
	using Vertex_cref		= const ::pgl::graph::pgl_vertex_t&;		//!< This is the constant reference to a vertex

	using VertexID_t 		= ::pgl::graph::pgl_vertexID_t;			//!< The type for the vertex id


	//Connection
	using Connection_ptr		= pgl_connection_t*;				//!< Pointer to a connection
	using Connection_cptr		= const pgl_connection_t*;			//!< Pointer to a constant connection
	using Connection_ref 		= pgl_connection_t&;				//!< Reference to connection
	using Connection_cref		= const pgl_connection_t&;			//!< Const reference to connection

	//Edge
	using Edge_ptr			= pgl_edge_t*;					//!< Pointer to a edge
	using Edge_cptr			= const pgl_edge_t*;				//!< Pointer to a constant edge
	using Edge_ref 			= pgl_edge_t&;					//!< Reference to edge
	using Edge_cref			= const pgl_edge_t&;				//!< Const reference to edge



	/*
	 * Internal typedefs
	 */
protected:
	using NeighbourhoodContainer_t 		= ::pgl::template Vector_t< ::pgl::graph::pgl_edge_t>; 	//!< This vector stores all the edges that forms our neighbourhood
	using NHIterator_t 		= NeighbourhoodContainer_t::iterator;				//!< This iterator is for the neigbourhood, It is only for internal purpose
	using const_NHIterator_t 	= NeighbourhoodContainer_t::const_iterator;			//!< This is a const iterator for the neigbourhood, it is only for internal purpose

	/*
	 * ==============================
	 * Vertex Iterators
	 *
	 * All of those iterators are bidirectional iterators.
	 *
	 * They also mostly used for conviniance.
	 * Generally speeking they iterate over the connection. But we know that connections are not
	 */
public:
	/**
	 * \brief 	This iterator is for incoming vertices
	 *
	 * If you dereference it, then, it returns the other vertices of that connection.
	 * Also if you increment this iterator, then you get a correct iterator back.
	 */
	using inVIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Vertex, internal::vIteratorTag::Incoming, internal::vIteratorTag::nonConst)
		, pgl_vertex_t, pgl_edge_t, NHIterator_t>;


	/**
	 * \brief 	This is the const version of inVIterator_t
	 *
	 * The difference is, that the reference that a dereference operation returns is constant.
	 */
	using const_inVIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Vertex, internal::vIteratorTag::Incoming, internal::vIteratorTag::Const)
		, pgl_vertex_t, pgl_edge_t, const_NHIterator_t>;



	/**
	 * \brief 	This iterator is for outgoing vertices
	 *
	 * If you dereference it, then, it returns the other vertices of that connection.
	 * Also if you increment this iterator, then you get a correct iterator back.
	 */
	using outVIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Vertex, internal::vIteratorTag::Outgoing, internal::vIteratorTag::nonConst)
		, pgl_vertex_t, pgl_edge_t, NHIterator_t>;

	/**
	 * \brief	This is iterator is the const version of outVIterator_t
	 *
	 * The difference is, that this iterator retuns constant objects
	 */
	using const_outVIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Vertex, internal::vIteratorTag::Outgoing, internal::vIteratorTag::Const)
		, pgl_vertex_t, pgl_edge_t, const_NHIterator_t>;


	/**
	 * \brief 	This iterator gives access to the vertices.
	 *
	 * There is no distinction between incoming and outgoing vertices.
	 * The order of iteration is also arbitrary.
	 */
	using VIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Vertex, internal::vIteratorTag::Connection, internal::vIteratorTag::nonConst)
		, pgl_vertex_t, pgl_edge_t, NHIterator_t>;


	/**
	 * \brief 	This is the constant version of VIterator_t
	 *
	 * The difference is, that this iterator is contant.
	 */
	using const_VIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Vertex, internal::vIteratorTag::Connection, internal::vIteratorTag::Const)
		, pgl_vertex_t, pgl_edge_t, const_NHIterator_t>;


	/**
	 * \breif 	This is the in Edge Iterator
	 *
	 * It is similary to the vertex iterator, with the difference, that the dereferencing retunrns the corresponding edge
	 */
	using EIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Edge, internal::vIteratorTag::Connection, internal::vIteratorTag::nonConst)
		, pgl_vertex_t, pgl_edge_t, NHIterator_t>;

	/**
	 * \breif 	This is the constant edge Iterator
	 *
	 * The difference it that this is the constant version.
	 */
	using const_EIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Edge, internal::vIteratorTag::Connection, internal::vIteratorTag::Const)
		, pgl_vertex_t, pgl_edge_t, const_NHIterator_t>;


	/**
	 * \brief 	This is the outgoing edge iterator
	 *
	 * This isterator is like outVIterator_t but it returns an edge if accessed
	 */
	using outEIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Edge, internal::vIteratorTag::Outgoing, internal::vIteratorTag::nonConst)
		, pgl_vertex_t, pgl_edge_t, NHIterator_t>;


	/**
	 * \brief 	This is the const outgoing edge iterator
	 *
	 * This isterator is like outVIterator_t but it returns an edge if accessed.
	 * The difference is, that the returned edge is const.
	 */
	using const_outEIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Edge, internal::vIteratorTag::Outgoing, internal::vIteratorTag::Const)
		, pgl_vertex_t, pgl_edge_t, const_NHIterator_t>;


	/**
	 * \brief	This is the incoming edge iterator
	 *
	 * This iterator is like inVIterator_t, but if accessed it returns an edge instead of a vertex.
	 */
	using inEIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Edge, internal::vIteratorTag::Incoming, internal::vIteratorTag::nonConst)
		, pgl_vertex_t, pgl_edge_t, NHIterator_t>;


	/**
	 * \brief	This is the const incoming edge iterator
	 *
	 * This iterator is like inVIterator_t, but if accessed it returns an edge instead of a vertex.
	 * The difference between inEIterator_t is, that the returned reference is constant.
	 */
	using const_inEIterator_t	= internal::pgl_vertex_iterator_base_t<
		  internal::_OR(internal::vIteratorTag::Edge, internal::vIteratorTag::Incoming, internal::vIteratorTag::Const)
		, pgl_vertex_t, pgl_edge_t, const_NHIterator_t>;



	/*
	 * =====================================
	 * Range Based loop iterators
	 *
	 * Here we defines some iterators that where used to defione range based loops
	 */
public:

	/**
	 * \brief	This is the iterator for all incoming vertieces.
	 */
	using inVRIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<inVIterator_t, false>;

	/**
	 * \brief	Const version for RBL for incomming vertices
	 */
	using const_inVRIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<const_inVIterator_t, true>;


	/**
	 * \brief	Iterator for RBL for outgoing vertices
	 */
	using outVRIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<outVIterator_t, false>;


	/**
	 * \brief	Const version for RBL outgoing vertices
	 */
	using const_outVRIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<const_outVIterator_t, true>;


	/**
	 * \brief	Iterator to iterate over all vertices
	 */
	using VRIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<VIterator_t, false>;

	/**
	 * \biref	Constant iterator to iterate over all incident vertieces
	 */
	using const_VRIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<const_VIterator_t, true>;


	using inERIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<inEIterator_t, false>;
	using const_inERIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<const_inEIterator_t, true>;

	using outERIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<outEIterator_t, false>;
	using const_outERIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<const_outEIterator_t, true>;

	using ERIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<EIterator_t, false>;
	using const_ERIterator_t = ::pgl::pgl_rangeBasedLoppAdaptor_t<const_EIterator_t, true>;



	/*
	 * =============================
	 * State encodes
	 *
	 * Here we declare the static values to address the bitset
	 */
	static constexpr ::pgl::Int_t 		ce_isComplete 		= 0;	//!< *this is complete
	static constexpr ::pgl::Int_t 		ce_isOriginal 		= 1;	//!< *this is an original vertex
	static constexpr ::pgl::Int_t 		ce_isFlagged		= 2; 	//!< Provided an boolean flag
	static constexpr ::pgl::Int_t 		ce_isPropBuild		= 3;	//!< Indicating a proper build.



	/*
	 * ==============================
	 * Construction a Vertex
	 *
	 * Here are the declarations of the constructor.
	 * The are normaly defaulted, but are not implemented here, they are impleemented in seperate file.
	 */

public:
	/**
	 * \brief 	Default Constructor
	 *
	 * This constructs an invalid vertex.
	 * We can use the defaulted version of it.
	 */
	pgl_vertex_t();


	/**
	 * \brief	Destructor
	 *
	 * We can also use the defaulted one.
	 */
	~pgl_vertex_t();


	/**
	 * \brief	Copy Constructor
	 *
	 * This is Deleted, because we consider them as unmovable and uncompable object.
	 */
	pgl_vertex_t(
		const pgl_vertex_t&) = delete;


	/**
	 * \brief	Copy Assignment
	 *
	 * This is also deleted.
	 */
	pgl_vertex_t&
	operator= (
		const pgl_vertex_t&) = delete;



	/**
	 * \brief	Move Constructor
	 *
	 * Is also deleted.
	 */
	pgl_vertex_t(
		pgl_vertex_t&&) = delete;


	/**
	 * \brief	Move Assignment
	 *
	 * Also deleted
	 */
	pgl_vertex_t&
	operator= (
		pgl_vertex_t&&) = delete;



	/**
	 * \brief 	builder constructor
	 *
	 * This constructor is the meaningfull constructor.
	 * It constructs a valid vertex.
	 * The user has to make sure, that ID is unique inside the graph.
	 *
	 * \param ID	The ID *this should have
	 * \param kindOfNode	This is a GraphTag member that describes *this.
	 *
	 * \throw 	If the ID is not valid and the tag has problem, invalid argument exceptions are created.
	 */
	pgl_vertex_t(
		const pgl_vertexID_t& 	ID,
		Payload_ptr 		payload,
		const GraphTag&		kindOfNode) noexcept(false);


	/**
	 * \brief 	Adds a new edge to *this
	 *
	 * This functions takes a connection and add it to *this.
	 * An edge is constructed automaticaly.
	 * *this must be uncomplete.
	 * And shall not be inserted allready.
	 *
	 * If the connection is a LoopOne connection, then they are handled correctly.
	 * The user must only insert them one.
	 *
	 * \param newConnection		This is a pointer to a connection, that should be added to this.
	 * \throw			If inconsistency
	 * \return			true if success. If false you fly with warp 10 int undefined behaviour
	 */
	bool
	add_connection(
		Connection_ptr newConnection) noexcept(false);


	/**
	 * \brief 	This function deletes the Edges from this, that is pointed to by deleteConnection
	 *
	 * \param deleteConnection	This is an iterator to the connection, that should be deleted
	 * 				   The iterator is modified such that it will be an enditerator.
	 *
	 * This function deletes the connection.
	 * True if retunrnneed on success. If false is returned it was not possible to do it.
	 * The egde must exists and be valid, if not, an exception is thrown.
	 *
	 * To work properly the edge, connectiona and the vertex must be uncomplete.
	 */
	bool
	remove_conection(
		EIterator_t& 	deleteConnection) noexcept(false);




	/**
	 * \brief	This function reserves space for the edges.
	 *
	 * This function is like the reveserve function if the vector.
	 * It allocates space, such that further insertions, doews not need an expanding of the underling array.
	 *
	 * \param inSize 	The number of incoming edges
	 * \param outSize 	The number of outgoing edges
	 *
	 * \return 	The capacity of the vertex is retunred.
	 */
	Size_t
	reserve(
		const Size_t 	inSize,
		const Size_t 	outSize);


	/**
	 * \brief	This function reserves space for the edges.
	 *
	 * This function is like the reveserve function if the vector.
	 * It allocates space, such that further insertions, doews not need an expanding of the underling array.
	 * The provided argunment is interpreted, as the total number of edges, that will be inserted.
	 *
	 * \param reqSpace	The number of edges, that are expected.
	 *
	 * \return 	The capacity of the vertex is retunred.
	 */
	Size_t
	reserve(
		const Size_t  	reqSpace);

	/**
	 * \brief	This function expands the allocated memory to store additionalEdges many edges.
	 *
	 * This function is equivalent in calling 'this->reserve(this->degree() + additionalEdges)'
	 * *this shall not be complete.
	 *
	 * \param additionalEdges 	The numbers of additional edges, that are requested.
	 *
	 * \return 	The numbers of allocated elements
	 */
	Size_t
	extentSpace(
		const Size_t 		additionalEdges);


	/**
	 * \brief 	Retruns the number of eedges, the vertex has allocated for.
	 *
	 * This is not the size of the allocated sapce.
	 * This is the number of edges, the underlying array can store.
	 * It is not the number of free slots!
	 */
	Size_t
	getCapacity() const noexcept;



	/*
	 * ============================
	 * State functions
	 *
	 * With them the user can set and get the state
	 */
public:
	/**
	 * \brief 	test if *this is complete
	 */
	bool
	isComplete() const noexcept;


	/**
	 * \brief	Set the complete status to Complete
	 *
	 * This function also completes the edges that are incident to this.
	 *
	 * \param Tag	Of Type GraphTag, must be either Complete
	 *
	 * \return	Returnvalue is equal to the value a call to isComplete would produce right after this fucntion
	 */
	bool
	makeComplete(
		const GraphTag& Tag);

	/**
	 * \brief 	Set the complete status of *this to uncomplete.
	 *
	 * This function will also uncomplete all of its edges, the will also uncomplete their underling connetions.
	 * So be aware of this.
	 *
	 * \param Tag	Of type GraphTag must be Uncomplete
	 *
	 * \return	Indicates success
	 */
	bool
	makeUncomplete(
		const GraphTag& Tag);


	/**
	 * \brief	Returns true if this is an original vertex
	 */
	bool
	isOriginal() const;


	/**
	 * \brief 	Retruns true if this is a random vertex
	 */
	bool
	isRandom() const;


	/**
	 * \brief	Returns true if this is valid
	 *
	 * Valid is defined as the property, that *this is a proper build an the vertex has a valid ID.
	 * This function does not check if the containing edges are valid.
	 * TThis is done in the checkIntegrity function.
	 */
	bool
	isValid() const noexcept;


	/**
	 * \brief 	Performs an integrity check
	 *
	 * This function performs an integrity check.
	 * It is quite constly.
	 */
	bool
	checkIntegrity() const;


	/**
	 * \brief 	Retrurns true if *this is flagged
	 *
	 * This is a bolean Flag that is NOT provided to the user.
	 * Its use is reserved for pgl:algorithms
	 */
	bool
	isFlagged() const noexcept;


	/**
	 * \brief 	Set the flag status to the provided value
	 *
	 * This function is not for the user.
	 */
	void
	setFalgTo(
		const bool& newFlagStat) noexcept;



	/*
	 * =========================
	 * Topological Functions
	 *
	 * These Functions are functions that allows the user to query the topological structure of the vertex.
	 * Like who is neighbour of what.
	 *
	 * Like Allways we distinguish between connections and edges.
	 * 	- Connections: Recoreded the fact of the excistance of a lionk between two vertices, and ignoring its direction or other obstacle.
	 * 	- Edges: Takes into account the direction of a link.
	 */

public:
	/**
	 * \brief 	returns true if there is a connection between *this and ID
	 *
	 * \param ID	The ID of the vertex.
	 * \pre		ID must be valid
	 *
	 * This function operates on connection level, so directions are ignored.
	 */
	bool
	hasConnectionWith(
		const pgl_vertexID_t& ID) const;

	/**
	 * \brief 	returns the numbers of connection *this has with ID
	 *
	 * \param ID	The ID of the vertex.
	 * \pre		ID must be valid
	 *
	 * This function operates on connection level, so directions are ignored.
	 */
	Size_t
	countConnectionWith(
		const pgl_vertexID_t& ID) const;

	/**
	 * \bool	This function tests if there is an outgoing connection to ID.
	 *
	 * This function essecialy tests if you can reach the node with ID, from this.
	 * If ID is not in the neigbourhood, then also true is returned.
	 *
	 * \param ID	The ID of the vertex.
	 * \pre		ID must be valid
	 */
	bool
	pointsTo(
		const pgl_vertexID_t& ID) const;

	/**
	 * \brief 	This function returns the number of directed connections *this has with ID
	 *
	 * This function obperates on edge level.
	 * It counts how many edges *this has that are leaving and otherNodeID returns ID.
	 *
	 * \param ID	The ID of the other vertex
	 * \pre 	ID must be valid
	 */
	Size_t
	countPointingTo(
		const pgl_vertexID_t& ID) const;



	/**
	 * \brief 	This function thest if ID con reach *this
	 *
	 * This function essecialy tests if *this has an incoming connection from ID.
	 * If ID does not exists in the neighbourhood of *this, then false is returned.
	 *
	 * \param ID	The ID of the vertex
	 * \pre 	ID must be valid
	 */
	bool
	pointsBy(
		const pgl_vertexID_t& ID) const;


	/**
	 * \brief 	This functions returns the numbers of connection that can reach *this from ID
	 *
	 * This fucntion operates on edge level, so directions are honored.
	 * This counts how many edges are incoming, and otherNodeID returns ID.
	 *
	 * \param ID	The ID of the other vertex
	 * \pree	ID must be valid
	 */
	Size_t
	countPointBy(
		const pgl_vertexID_t& ID) const;




	/**
	 * \brief 	This function returns the poiinter to the payload of the vertex
	 *
	 * This is a raw pointer.
	 * The client shall not delete it, it will result in undefined behaviour
	 */
	inline
	Payload_ptr
	getPayLoadPointer() noexcept
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return this->m_payload.get();
#endif
	};

	inline
	Payload_ptr
	getPayload() noexcept
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return this->m_payload.get();
#endif
	};

	inline
	PayLoad_cptr
	getCPayLoadPointer() const noexcept
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return this->m_payload.get();
#endif
	};

	inline
	PayLoad_cptr
	getCPayload() const noexcept
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return m_payload.get();
#endif
	};


	inline
	PayLoad_cptr
	getPayLoadPointer() const noexcept
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return this->m_payload.get();
#endif
	};

	inline
	PayLoad_cptr
	getPayload() const noexcept
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return m_payload.get();
#endif
	};


	/*
	 * Kleinberg Shortcats for the Payload
	 *
	 * The flowing function provides a shorthand for the payload functions
	 */

	inline
	bool
	hasValidTargetNode() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("Payloads are deactivated.");
#else
		return m_payload->hasValidTargetNode();
#endif
	};

	inline
	pgl_vertexID_t
	getCurrentTargetNodeID() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("Payloads are deactivated.");
#else
		return m_payload->getCurrentTargetNodeID();
#endif
	};



	inline
	void
	setCurrentRoutingDistanceTo(
		const Distance_t& dist)
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("Payloads are deactivated.");
#else
		m_payload->setCurrentRoutingDistanceTo(dist);
#endif
		return;
	};


	inline
	bool
	wasVisitedByKleinberg() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("Payloads are deactivated.");
#else
		return m_payload->wasVisitedByKleinberg();
#endif
	};

	inline
	void
	makeVisitedByKleinberg()
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("Payloads are deactivated.");
#else
		return m_payload->makeVisitedByKleinberg();
#endif
	};


	inline
	Distance_t
	getRoutingDistance() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("Payloads are deactivated.");
#else
		return m_payload->getRoutingDistance();
#endif
	};

	inline
	void
	setCurrentTargetNodeID(
		const pgl_vertexID_t& t) const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("Payloads are deactivated.");
#else
		m_payload->setCurrentTargetNodeID(t);
#endif
		return;
	};



	/*
	 * =============================
	 * Enety getter
	 *
	 * This functions allows the user to get the entety, such as edges and vertieces,
	 * that are involved with *this.
	 * If the requested edges does not exists an out of bound exception is generated.
	 *
	 * Rember that the set of incident edges is (unfortunately) a multiset.
	 * So accessing them is a bit of a pain.
	 * The last parameter of the getter functions are used to access a specific edge.
	 * This parameter is defaulted to 0, which means that the first edge found is returned.
	 *
	 * Also an edge is considered non existing if the pair of ID and n is invalid.
	 * Suppose a vertex had one connection with ID 3, and you request the second of thoese edges,
	 * So this edge does not exist and an out of bound is thrown.
	 */

	/**
	 * \brief 	Returns a reference to the incident vertex
	 *
	 * \param ID	The ID of the requested vertices
	 * \param n	Which one is requested.
	 *
	 * \throw 	out_of_bounds if ID is not existing.
	 *
	 * Returns a refernece to the vertex, that has the provided ID
	 */
	Vertex_ref
	getNode(
		const pgl_vertexID_t& 	ID,
		const Size_t& 		n);

	Vertex_cref
	getNode(
		const pgl_vertexID_t& 	ID,
		const Size_t&		n = 0) const;

	Vertex_cref
	getCNode(
		const pgl_vertexID_t& 	ID,
		const Size_t&		n = 0) const;


	/**
	 * \brief 	Returns a reference to the edge, that connects *this with ID
	 *
	 * \param ID	The id of the other node
	 * \param n	The n-th edge is requested
	 *
	 * \throw	out_of_bounds if ID is not incident with *this.
	 */
	Edge_ref
	getEdge(
		const pgl_vertexID_t& 	ID,
		const Size_t&		n = 0);

	Edge_cref
	getEdge(
		const pgl_vertexID_t& 	ID,
		const Size_t&		n = 0) const;

	Edge_cref
	getCEdge(
		const pgl_vertexID_t& 	ID,
		const Size_t&		n = 0) const;



	/*
	 * =========================
	 * Iterator Functions
	 *
	 * Here are the functions that  gives the vertex its usefullness
	 */

#define PGL_VECTOR_HELPER_BEG m_neigbourhood.begin(), m_neigbourhood.begin(), m_neigbourhood.end()
#define PGL_VECTOR_HELPER_END m_neigbourhood.end(), m_neigbourhood.begin(), m_neigbourhood.end()
//#define PGL_VECTOR_HELPER_THR  if(PGL_UNLIKELY(this->isValid() == false)) { throw PGL_EXCEPT_RUNTIME("An invalid vertex, cannot create an iterator."); }
#define PGL_VECTOR_HELPER_THR  pgl_assert(this->isValid() == true)

	/*
	 * Vertex Iterator functions
	 */

	inline
	VIterator_t
	VBegin() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return VIterator_t(PGL_VECTOR_HELPER_BEG);
	};

	inline
	const_VIterator_t
	VBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_VIterator_t( PGL_VECTOR_HELPER_BEG );
	};

	inline
	const_VIterator_t
	cVBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_VIterator_t( PGL_VECTOR_HELPER_BEG );
	};


	inline
	VIterator_t
	VEnd() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return VIterator_t(PGL_VECTOR_HELPER_END);
	};

	inline
	const_VIterator_t
	VEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_VIterator_t( PGL_VECTOR_HELPER_END );
	};

	inline
	const_VIterator_t
	cVEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_VIterator_t( PGL_VECTOR_HELPER_END );
	};


	/*
	 * Edge Iterator functions
	 */
	inline
	EIterator_t
	EBegin() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return EIterator_t( PGL_VECTOR_HELPER_BEG );
	};

	inline
	const_EIterator_t
	EBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_EIterator_t( PGL_VECTOR_HELPER_BEG );
	};

	inline
	const_EIterator_t
	cEBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_EIterator_t( PGL_VECTOR_HELPER_BEG );
	};

	inline
	EIterator_t
	EEnd() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return EIterator_t( PGL_VECTOR_HELPER_END );
	};

	inline
	const_EIterator_t
	EEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_EIterator_t( PGL_VECTOR_HELPER_END );
	};

	inline
	const_EIterator_t
	cEEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_EIterator_t( PGL_VECTOR_HELPER_END );
	};


	/*
	 * incoming Vertex
	 */
	inline
	inVIterator_t
	inVBegin() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return inVIterator_t(PGL_VECTOR_HELPER_BEG);
	};

	inline
	const_inVIterator_t
	inVBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inVIterator_t( PGL_VECTOR_HELPER_BEG );
	};

	inline
	const_inVIterator_t
	cInVBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inVIterator_t( PGL_VECTOR_HELPER_BEG );
	};


	inline
	inVIterator_t
	inVEnd() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return inVIterator_t(PGL_VECTOR_HELPER_END);
	};

	inline
	const_inVIterator_t
	inVEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inVIterator_t( PGL_VECTOR_HELPER_END );
	};

	inline
	const_inVIterator_t
	cInVEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inVIterator_t( PGL_VECTOR_HELPER_END );
	};

	/*
	 * Out Vertex Iterator
	 */
	inline
	outVIterator_t
	outVBegin() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return outVIterator_t(PGL_VECTOR_HELPER_BEG);
	};

	inline
	const_outVIterator_t
	outVBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outVIterator_t( PGL_VECTOR_HELPER_BEG );
	};

	inline
	const_outVIterator_t
	cOutVBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outVIterator_t( PGL_VECTOR_HELPER_BEG );
	};


	inline
	outVIterator_t
	outVEnd() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return outVIterator_t(PGL_VECTOR_HELPER_END);
	};

	inline
	const_outVIterator_t
	outVEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outVIterator_t( PGL_VECTOR_HELPER_END );
	};

	inline
	const_outVIterator_t
	cOutVEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outVIterator_t( PGL_VECTOR_HELPER_END );
	};


	/*
	 * In Edge Iterator
	 */
	inline
	inEIterator_t
	inEBegin() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return inEIterator_t(PGL_VECTOR_HELPER_BEG);
	};

	inline
	const_inEIterator_t
	inEBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inEIterator_t( PGL_VECTOR_HELPER_BEG );
	};

	inline
	const_inEIterator_t
	cInEBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inEIterator_t( PGL_VECTOR_HELPER_BEG );
	};


	inline
	inEIterator_t
	inEEnd() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return inEIterator_t(PGL_VECTOR_HELPER_END);
	};

	inline
	const_inEIterator_t
	inEEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inEIterator_t( PGL_VECTOR_HELPER_END );
	};

	inline
	const_inEIterator_t
	cInEEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inEIterator_t( PGL_VECTOR_HELPER_END );
	};


	/*
	 * Out Edge Iterators
	 */
	inline
	outEIterator_t
	outEBegin() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return outEIterator_t(PGL_VECTOR_HELPER_BEG);
	};

	inline
	const_outEIterator_t
	outEBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outEIterator_t( PGL_VECTOR_HELPER_BEG );
	};

	inline
	const_outEIterator_t
	cOutEBegin() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outEIterator_t( PGL_VECTOR_HELPER_BEG );
	};


	inline
	outEIterator_t
	outEEnd() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return outEIterator_t(PGL_VECTOR_HELPER_END);
	};

	inline
	const_outEIterator_t
	outEEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outEIterator_t( PGL_VECTOR_HELPER_END );
	};

	inline
	const_outEIterator_t
	cOutEEnd() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outEIterator_t( PGL_VECTOR_HELPER_END );
	};
#undef PGL_VECTOR_HELPER_END
#undef PGL_VECTOR_HELPER_BEG



	/*
	 * RANGE BASED LOOP ITERATORS
	 */

	inline
	inVRIterator_t
	inVRIterator() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return inVRIterator_t(this->inVBegin(), this->inVEnd() );
	};


	inline
	const_inVRIterator_t
	inVRIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inVRIterator_t(this->inVBegin(), this->inVEnd() );
	};

	inline
	const_inVRIterator_t
	cInVRIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inVRIterator_t(this->inVBegin(), this->inVEnd() );
	};




	inline
	outVRIterator_t
	outVRIterator() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return outVRIterator_t(this->outVBegin(), this->outVEnd() );
	};


	inline
	const_outVRIterator_t
	outVRIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outVRIterator_t(this->cOutVBegin(), this->cOutVEnd() );
	};


	inline
	const_outVRIterator_t
	cOutVRIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outVRIterator_t(this->cOutVBegin(), this->cOutVEnd() );
	};


	inline
	VRIterator_t
	VRIterator() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return VRIterator_t(this->VBegin(), this->VEnd() );
	};


	inline
	const_VRIterator_t
	VRIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_VRIterator_t(this->cVBegin(), this->cVEnd() );
	};


	inline
	const_VRIterator_t
	cVRIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_VRIterator_t(this->cVBegin(), this->cVEnd() );
	};

	////////////////////////////////////

	inline
	inERIterator_t
	inERIterator() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return inERIterator_t(this->inEBegin(), this->inEEnd() );
	};


	inline
	const_inERIterator_t
	inERIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inERIterator_t(this->inEBegin(), this->inEEnd() );
	};

	inline
	const_inERIterator_t
	cInERIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_inERIterator_t(this->inEBegin(), this->inEEnd() );
	};




	inline
	outERIterator_t
	outERIterator() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return outERIterator_t(this->outEBegin(), this->outEEnd() );
	};


	inline
	const_outERIterator_t
	outERIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outERIterator_t(this->cOutEBegin(), this->cOutEEnd() );
	};


	inline
	const_outERIterator_t
	cOutERIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_outERIterator_t(this->cOutEBegin(), this->cOutEEnd() );
	};


	inline
	ERIterator_t
	ERIterator() noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return ERIterator_t(this->EBegin(), this->EEnd() );
	};


	inline
	const_ERIterator_t
	ERIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_ERIterator_t(this->cEBegin(), this->cEEnd() );
	};


	inline
	const_ERIterator_t
	cERIterator() const noexcept
	{
		PGL_VECTOR_HELPER_THR ;
		return const_ERIterator_t(this->cEBegin(), this->cEEnd() );
	};

#undef PGL_VECTOR_HELPER_THR




	/*
	 * ============================
	 * Printer
	 *
	 * This functions are used to print out some informations
	 */


	/**
	 * \brief	This function prints an approximation of the topology
	 *
	 * Topology means that the neighourhood is printed.
	 */
	std::string
	print_topo() const;




	/*
	 * ==========================
	 * Accessorfunctions
	 *
	 * These functions provide access to the state of *this
	 */
public:
	/**
	 * \brief 	Retrurns the ID of *this
	 */
	inline
	pgl_vertexID_t
	getID() const noexcept
	{
		return m_id;
	};


	/**
	 * \brief	Returns the allocated capacity of *this.
	 *
	 * The allocates capacity is the size currently allocated by the container that stores the edges.
	 */
	Size_t
	edgeCapacity() const noexcept;


	/**
	 * \brief 	Return the Degree
	 *
	 * The degree includes _all_ connection the incoming and the outgoing
	 * connections.
	 */
	Size_t
	degree() const noexcept;


	/**
	 * \brief 	Returns the numbers of incoming connections
	 *
	 * We consider a connection as incoming if edge::isIncoming() returns true.
	 * This also includes the random connections.
	 */
	Size_t
	inDegree() const noexcept;


	/**
	 * \brief 	Returns the numbers of outgoing connections
	 *
	 * We consiger a conection as outgoing if edge::isOutgoing() returns true.
	 * This naturaly also includes random connections.
	 */
	Size_t
	outDegree() const noexcept;


	/**
	 * \brief	Returns true if *this is an leafe.
	 *
	 * *this is a leave if this->outDegree returns zero.
	 * But this fucntion is a bit faster.
	 */
	bool
	isLeafe() const noexcept;


	/**
	 * \brief	Returns the number of random connections
	 *
	 * This functions returns the numbers of random connections,
	 * so direction is ignored.
	 */
	Size_t
	randDegree() const noexcept;


	/**
	 * \brief 	Returns the numbers of original connections
	 *
	 * This fucntions operates on the connection level, so directions are ignored.
	 */
	Size_t
	orgDegree() const noexcept;


	/**
	 * \brief	This function calculates all four degrees of a vertex.
	 *
	 * Since it is potentialy costly to query all degree seperatly, this function offers a way to do it in one sweep.
	 * It returns a tuple, with all the following degrees, in that order:
	 * 	- orgInDegree		The number of incoming non random connections.
	 * 	- orgOutDegree		The number of outgoing non random connections.
	 * 	- randInDegree		The number of incoming random connections.
	 * 	- randOutDegree 	The number of outgoiung random connections
	 *
	 * \note Undirected connections are counted twice, once as incoming and once as leaving connections.
	 */
	std::tuple<Size_t, Size_t, Size_t, Size_t>
	orgRandInOutDegree() const noexcept;


	/**
	 * \brief 	Returns true if *this is a leafe, if the random connections are ignored.
	 *
	 * First all random connections are ignored and then the numbers of the edges that returns true if
	 * edge::idOutgoing() is called.
	 * If this is number is zero, true is returned.
	 */
	bool
	isRealLeafe() const noexcept;


	/*
	 * ===============================
	 * User Supplied functions
	 *
	 * These functions provide a way such that the user can provvide some functions that he wants.
	 * Some functions also offers a version that takes a reference_wrapper, this way, a statefull iteration
	 * can be implemented.
	 *
	 * All this fucntions requires that *this is complete.
	 */

	/**
	 * \brief 	Like the std::count_if on all edges, that builds the neighbourhood.
	 *
	 * \param Pred	The predicate of type std::fucntion<bool(const Edge&)>
	 *
	 * This count how many times the prediccate pred returns true.
	 */
	Size_t
	count_if(
		std::function<bool(const pgl_edge_t&)> Pred) const;


	/**
	 * \brief	Retruns true if the edge to the vertex ID is applicalble
	 * \throw 	If *this is not connectied (by connection sense) to ID
	 * \param ID	The ID of the other vertex
	 * \param Pred	The predicate that is used
	 * \param n 	The number of the edge that has to be tested.
	 *
	 * This function works internaly by first finding the edge {*this, ID} and then feed
	 * A constant reference to this edge is passed to the predicate, and its return value is returned.
	 * If no suce edge excist, a exception is throwm
	 */
	bool
	test_edge(
		std::reference_wrapper<bool(Edge_cref)> 	Pred,
		const pgl_vertexID_t&				ID,
		const Size_t& 					n = 0) const;


	/**
	 * \brief	Test if any of the vertex fullfiles the predicate.
	 *
	 * This function iterates throght all the vertex and applyies the predicate.
	 * If the return value is true, then the iteration is stoped.
	 *
	 * \param Pred	The test predicate that should be applied
	 */
	bool
	any_of(
		std::function<bool(Edge_cref)> Pred) const;


	/**
	 * \brief 	Applies the predicate to any edges.
	 *
	 * This function is allowed to modyfy the edge, meaning it is apped as a non-const reference
	 */
	void
	apply_to_all(
		std::function<void(Edge_ref)>	Pred);

	/**
	 * \brief 	This function Traverses all the edges and applies the provided function
	 *
	 * The difference to apply:to_all is that a reference is passed to *this and the edges
	 * are passed as const reference
	 */
	void
	check_all(
		std::reference_wrapper<void(Edge_cref)>  Pred) const;


	/**
	 * \brief	Find the first edge, that fullfiels the predicate
	 *
	 * \param Pred	The predicate that the edge has to be fullfile
	 *
	 * All edges are checked, the first one that satissfies the predicate is returned.
	 * All edges are checked, an EIterator is returned.
	 */
	EIterator_t
	find_first_of(
		std::function<bool(Edge_cref)> Pred);




	/*
	 * ==============================================
	 * Complerator functions
	 */

#if 0
	bool
	operator== (
		const pgl_vertex_t& rhs) const noexcept
	{
		return this->m_id == rhs.m_id;
	};

	bool
	operator!= (
		const pgl_vertex_t& rhs) const noexcept
	{
		return this->m_id != rhs.m_id;
	};


#endif
	bool
	operator< (
		const pgl_vertex_t& rhs) const noexcept
	{
		return this->m_id < rhs.m_id;
	};

	/*
	 * =========================================
	 * Private Functions
	 *
	 * This functions are private, they are here for conviniance.
	 * They are internal and the user should never use them.
	 */
private:

	/**
	 * \brief 	This function sets the state of *this into a complete state.
	 *
	 * This function is internal, and shall not be called by the user.
	 * It returns true on success.
	 * It assumes that all requirements are meet.
	 * It also completes all the edges of *this, they in turn completes their underlying edges.
	 */
	bool
	int_make_complete();


	/**
	 * \brief 	This function sets the complete status of *this to uncomplete.
	 *
	 * This function is internal, and shall not be called by the user.
	 * It returns true on success.
	 * It assumes that all requirements are meet.
	 * It also completes all the edges of *this, they in turn completes their underlying edges.
	 */
	bool
	int_make_uncomplete();

	/**
	 * \brief 	Search for the requested edge
	 *
	 * \param ID	The ID of the involved vertex
	 * \param n	The count of the involved vertex
	 *
	 * This is an internal function.
	 * It returns an iterator and a bool.
	 *
	 * \return	.first Boolean if in neighbourhood; .second an iterator
	 */
	std::pair<bool, NHIterator_t>
	int_searchEdge(
		const pgl_vertexID_t& 	ID,
		const Size_t&		n);


	std::pair<bool, const_NHIterator_t>
	int_searchEdge(
		const pgl_vertexID_t& 	ID,
		const Size_t& 		n) const;

	std::pair<bool, const_NHIterator_t>
	int_searchEdgeC(
		const pgl_vertexID_t& 	ID,
		const Size_t& 		n) const;

	/**
	 * \brief	Test an edge
	 *
	 * This function tests the provided edge. if it is propper and stuff.
	 *
	 * \param e	The edge to test
	 */
	bool
	int_testEdge(
		Edge_cref e) const;


	/*
	 * ============================
	 * Private Members
	 */
private:
	::pgl::graph::pgl_vertexID_t 	m_id;				//!< The ID of *this
#ifndef PGL_GRAPH_DISABLE_PAYLOADS
	iPayload_t 			m_payload;			//!< Pointer to the payload
#endif
	State_t 			m_state;			//!< The state of *this
	NeighbourhoodContainer_t 	m_neigbourhood;			//!< The neighbourhood of *this



}; //End class(pgl_vertex_t)



/*
 * Some other comparations operations
 */
inline
bool
operator< (
	const pgl_vertex_t& lhs,
	const pgl_vertexID_t& rhs) noexcept
{
	return lhs.getID() < rhs;
};

inline
bool
operator< (
	const pgl_vertexID_t& lhs,
	const pgl_vertex_t& rhs) noexcept
{
	return lhs < rhs.getID();
};



inline
bool
operator==(
	const pgl_vertex_t& rhs,
	const pgl_vertex_t& lhs) noexcept
{
	return lhs.getID() == rhs.getID();
};


inline
bool
operator== (
	const pgl_vertex_t& lhs,
	const pgl_vertexID_t& rhs) noexcept
{
	return lhs.getID() == rhs;
};

inline
bool
operator== (
	const pgl_vertexID_t& lhs,
	const pgl_vertex_t& rhs) noexcept
{
	return lhs == rhs.getID();
};


inline
bool
operator!= (
	const pgl_vertex_t& lhs,
	const pgl_vertexID_t& rhs) noexcept
{
	return lhs.getID() != rhs;
};

inline
bool
operator!= (
	const pgl_vertexID_t& lhs,
	const pgl_vertex_t& rhs) noexcept
{
	return lhs != rhs.getID();
};


PGL_NS_END(graph)
PGL_NS_END(pgl)


#endif //End include guard
