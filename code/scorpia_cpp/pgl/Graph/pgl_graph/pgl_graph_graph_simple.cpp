/**
 * \file 	pgl_graph/pgl_graph_graph_simple.cpp
 * \brief 	Implements the "simple" functions of the graph.
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl_stable_vector.hpp>
#include <pgl_multimap.hpp>

#include <pgl/pgl_aglorithm.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>

#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>




PGL_NS_START(pgl)
PGL_NS_START(graph)



/*
 * \brief 	Returns the numbers of nodes
 */
Size_t
pgl_graph_t::nVertices() const noexcept
{
	return this->m_vertices.size();
};


Size_t
pgl_graph_t::nRandVertices() const noexcept
{
	return this->m_nRandVertices;
};

Size_t
pgl_graph_t::nOrgVertices() const noexcept
{
	pgl_assert(m_nRandVertices <= m_vertices.size());
	return this->nVertices() - this->nRandVertices();
};


Size_t
pgl_graph_t::edgeCapacity() const noexcept
{
	Size_t sum = 0;
	for(const auto& v : m_vertices)
	{
		sum += v.edgeCapacity();
	};

	return sum;
};



/*
 * \brief 	Returns the numbers of edges
 */
Size_t
pgl_graph_t::nConnections() const noexcept
{
	return this->m_connections.size();
};


Size_t
pgl_graph_t::nRandConnections() const noexcept
{
	return this->m_nRandConnections;
};


Size_t
pgl_graph_t::nOrgConnections() const noexcept
{
	pgl_assert(m_nRandConnections <= m_connections.size());
	return this->nConnections() - this->nRandConnections();
};


pgl_graph_t::RevisionState_t
pgl_graph_t::getCurrentRevision() const noexcept
{
	return m_revIndex.getRevision();
};



/*
 * \brief 	returns true if *this is empty
 *
 * A graph is empty if it hasn't any vertices nor connections.
 */
bool
pgl_graph_t::isEmpty() const noexcept
{
	return (this->m_vertices.empty() && this->m_connections.empty());
};

/*
 * \brief 	Returns true if *this is complete
 */
bool
pgl_graph_t::isComplete() const noexcept
{
	return this->m_isComplete;
};

pgl_graph_t::NodeIterator_t
pgl_graph_t::getNode(
	const pgl_vertexID_t& ID) noexcept(false)
{
	//Test if the ID is valid
	if(ID.isValid() == false)
	{
		throw PGL_EXCEPT_OutOfBound("Tryed to query a node with ilegal ID");
	};

	if(m_vertices.size() == 0)
	{
		return m_vertices.end();
	};

	//Extract the raw ID
	const Size_t rawID = ID.getID();	//The Size_T takes care of negative values

	//Test if the ID exceeded the higest range, we assume that
	if(m_vertices.back().getID().getID() < rawID)
	{
		//The vertex with the higest id can't it be, so we knwo that the vertex can't be in the graph
		return m_vertices.end();
	};

	//Test if we are lucky and the node is exactly where its ID tells us
	if((rawID < m_vertices.size()) && (m_vertices[rawID].getID() == ID))
	{
		pgl_assert(m_vertices.at(rawID).getID() == ID);
		return (m_vertices.begin() + rawID);
	};

	auto LocatedIterator = std::lower_bound(m_vertices.begin(), m_vertices.end(), ID, std::less<void>());
	if(LocatedIterator == this->m_vertices.end())
	{
		//We found the end iterator, so we can return it saftly
		return this->m_vertices.end();
	}
	else
	{
		//We didn't found the end iterator, so we must check if we realy found it
		return (LocatedIterator->getID() == ID) ? LocatedIterator : m_vertices.end();
	};
};

pgl_graph_t::const_NodeIterator_t
pgl_graph_t::getNode(
	const pgl_vertexID_t& ID) const noexcept(false)
{
	//Test if the ID is valid
	if(ID.isValid() == false)
	{
		throw PGL_EXCEPT_OutOfBound("Tryed to query a node with ilegal ID");
	};

	if(m_vertices.size() == 0)
	{
		return m_vertices.end();
	};

	//Get the graw ID
	const Size_t rawID = ID.getID();	//Size_t automaticaly handles negative values

	//Test if the ID exceeded the higest range, we assume that
	if(m_vertices.back().getID().getID() < rawID)
	{
		//The vertex with the higest id can't it be, so we knwo that the vertex can't be in the graph
		return m_vertices.end();
	};

	//Test if we are lucky and the node is exactly where its ID tells us
	if((rawID < m_vertices.size()) && (m_vertices.operator[](rawID).getID() == ID))
	{
		pgl_assert(m_vertices.at(rawID).getID() == ID);
		return (m_vertices.begin() + rawID);
	};

	auto LocatedIterator = std::lower_bound(m_vertices.cbegin(), m_vertices.cend(), ID, std::less<void>());
	if(LocatedIterator == this->m_vertices.cend())
	{
		//We found the end iterator, so we can return it saftly
		return this->m_vertices.cend();
	}
	else
	{
		//We didn't found the end iterator, so we must check if we realy found it
		return (LocatedIterator->getID() == ID) ? LocatedIterator : m_vertices.cend();
	};
};


/*
 * \brief 	Returns true if *this has an vertex with the ID.
 */
bool
pgl_graph_t::containVertexID(
	const pgl_vertexID_t& ID) const noexcept
{
	//Check if the ID is invalid, so we can return false
	if(ID.isValid() == false)
	{
		return false;
	};


	//We now gambel and hope that the ID maps exactly to the position in the array
	const Size_t rawID = ID.getID();
	try
	{
		if((rawID < m_vertices.size()) && (m_vertices.at(rawID).getID() == ID))
		{
			//It is exactly at the supposed location
			return true;
		};
	}
	catch(...)
	{
		//IF we generate an exception, we catch it here, to comply with noexcept, and abort the program
		std::cerr << "Exception in the containVertexID function generated. Abort." << std::endl;
		std::abort();
	};


	//Make a binary search for to find the id
	return std::binary_search(m_vertices.cbegin(), m_vertices.cend(), ID, std::less<void>());
};


/*
 * \brief 	Get a range of iterators to the given connection
 *
 * \param ID	The Connection ID to search for.
 *
 * This function returns a range [first, last), which have the same ID as the provided one.
 * If no such ID could be found, both are equal to the end iterator.
 */
pgl_graph_t::ConnRange_t
pgl_graph_t::getConnections(
	const pgl_edgeID_t& 	ID) noexcept
{
	intConnRange_t intRange = this->m_connections.equal_range(ID);
	return std::make_pair(ConnIterator_t(intRange.first), ConnIterator_t(intRange.second));
};

pgl_graph_t::const_ConnRange_t
pgl_graph_t::getConnections(
	const pgl_edgeID_t& 	ID) const noexcept
{
	const_intConnRange_t intRange = this->m_connections.equal_range(ID);
	return std::make_pair(const_ConnIterator_t(intRange.first), const_ConnIterator_t(intRange.second));
}


/*
 * \brief 	Returns how many connections have the same ID as the provided one
 */
Size_t
pgl_graph_t::countConnectionID(
	const pgl_edgeID_t& 	ID) const noexcept
{
	return this->m_connections.count(ID);
};



PGL_NS_END(graph)
PGL_NS_END(pgl)


