#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_CONNECTION_HPP
#define PGL_GRAPH__PGL_GRAPH_CONNECTION_HPP
/**
 * \file pgl_graph/pgl_graph_connection.hpp
 * \brief This file defines the connection of pgl
 *
 * This file declares the connection.
 * A description of the connection can be found in the manual.
 */

//PGL-Stuff
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>

#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_vertex_saf.hpp>

#include <pgl_graph/pgl_graph_connection_payload.hpp>
#include <pgl_graph/pgl_graph_connection_base.hpp>



//STD-Stuff
#include <bitset>
#include <memory>


PGL_NS_START(pgl)
PGL_NS_START(graph)



/**
 * \brief a connection
 *
 * This file models a connection.
 * As stated in the manual this connection has an exclusive ownership of the connectionpayload.
 * But it can't use it.
 *
 * Because of the usage of the unique_ptr, this calss is not copable, but this is exactly what we want.
 *
 * This class is heavy inlined, this is done to speed up some accession.
 * There is allways this discussion that inlining polutes the instruction cash.
 * I consider this not a problem, because the inline keyword is only a suggestion\footnote{More begging.} to the compiler.
 * Inline force the compiler to only one thing, removing the symbol (function) from the symboltable.
 * This allows us to circuvrate the "one definition rule".
 * I must come up with something for debugging.
 *
 * As stated, may times, the payload is overseed by the connection, but some of the function depends
 * on a particular direction. So some functions make no sense if one does not have an edge.
 * So the connection doen't support the quierry functions of the payload at all, for a consistent interface.
 * Buit the connection can give access to the payload, and also provide some cast operation for it.
 */
class pgl_connection_t : public pgl_connection_base_t<4>
{
	/*
	 * =======================================
	 * Some usefull typedefs
	 */
private:
	using Super_t 			= pgl_connection_base_t<4>;				//!< This is the base type, only for completnes
	using iPayload_ptr 		= std::unique_ptr<pgl_connection_payload_t>; 		//!< This is the internal pointer we have for the payload

public:
	using Payload_ptr 		= pgl_connection_payload_t*;				//!< This is the pointertype of the payload we present the user
	using Payload_cptr 		= const pgl_connection_payload_t*;			//!< This si the const payload pointer for the user
	using Vertex_ptr 		= pgl_vertex_t*;					//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= const pgl_vertex_t*;					//!< This is the pointer to a constant vertex
	using Vertex_ref		= pgl_vertex_t&;					//!< This is the type of a reference to a node
	using Vertex_cref		= const pgl_vertex_t&;					//!< This is the constant reference to a vertex




	/*
	 * ==================================================
	 * Constructors
	 */
	/**
	 * \brief Now we declare the constructors
	 *
	 * They are also the building functions.
	 * The default constructor does build a non proper Connection, which is useless.
	 *
	 * Through the use of unique_prt for the payload, this is an move only type.
	 * Copy-operators are deleted.
	 *
	 * The constructors are defined in the source file.
	 */

	/// Default constructor
	pgl_connection_t();

	/// Mopve Constructor (defaulted)
	pgl_connection_t(
		pgl_connection_t&&) noexcept;

	/// Move assigne (defaulted)
	pgl_connection_t&
	operator= (
		pgl_connection_t&&) noexcept;

	/// Copy constructor deleted
	pgl_connection_t(
		const pgl_connection_t&) = delete;

	/// Copy assignment deleted
	pgl_connection_t&
	operator= (
		const pgl_connection_t&) = delete;

	/// Destructor is also the default one
	~pgl_connection_t();


	/**
	 * \brief 	Valid builder constructor
	 *
	 * \param srcNode 	Pointer to the node, which is the beginning (in case of directed connection)
	 * \param destNode 	Pointer to the node, which is the end of the connection.
	 * \param kindOfConnection 	The connection of the connection that sould be created
	 * \param PayLoad	Payload that sould be used.
	 *
	 * The pointers to the two nodes shall not be null, the must be valid and the object they point to too.
	 * They if the connection is a LoopOne connection, then this has to be specified by the kindOfConnection parameter.
	 * If it is not an LoopOne and src == dest an error is produced.
	 *
	 * The payload can be the nullpointer.
	 *
	 */
	pgl_connection_t(
		  pgl_vertex_t* 		srcNode
		, pgl_vertex_t* 		destNode
		, const GraphTag&  		kindOfConnection
		, Payload_ptr 			PayLoad);



	/*
	 * =======================================
	 * Managin Functions
	 */
	/**
	 * \brief 	Managing functions
	 *
	 * The folowing functions are fucntions that are used for the managment.
	 * They are not inlined, sicne they are not called often (or I don't expect that)
	 */


	/**
	 * \brief 	Make this complete.
	 *
	 * \param Tag 	Tag must be GraphTag::Complete, if Tag dont compare equal an error occured
	 *
	 * This function shall be called from the edge if it the edge is completed.
	 * If this function called twice, there shall be no effect.
	 * The Tag is only for safty.
	 *
	 * If it is not possible to complete, this then a runtime_exception is thrown,
	 */
	void
	makeComplete(
		const GraphTag& Tag) noexcept(false);


	/**
	 * \brief 	Make *this uncoplete
	 *
	 * \param Tag	Tag must be equal GraphTag::Uncomplete, if not an error occured
	 *
	 * This function shall also be called by the respective edges if they are uncompleted.
	 * If this function is called twice, there shall be no effect.
	 *
	 * As of now, if the connection is uncompleted directly, then the connection must not inform its edges of this change
	 * (Observer Pattern). But this is an extension of the near future.
	 */
	void
	makeUncomplete(
		const GraphTag& Tag);


	/*
	 * ========================================
	 * Accessorfunction
	 */
	/**
	 * \breif Accessorfunction
	 *
	 * The folowing functions are used for the day-to-day work with the connection.
	 * They don't include any work with the payload nor construction.
	 * They are mainly const.
	 * The user is encuraged to work with them.
	 *
	 * The user is also remebered, that the the connection base also provides some functions that are interessting to use.
	 *
	 * All the folowing functions have the implicit precondition, that the edge is valid.
	 * The edge must nopt be complete, but the edge must be valid
	 */
public:

	/**
	 * \brief 	Returns the ID of the node where *this originates
	 * 		If *this is a undirected connection then one of the edgepoints is returned.
	 */
	inline
	pgl_vertexID_t
	getSrcID() const noexcept
	{
		/*
		 * Since we made the ID inteligend this is everything we have to do, the edgeID makes the right thing.
		 * Isn't this nice?
		 */
		return m_id.getSrcID();
	};


	/**
	 * \brief 	Get The ID of the node wehere *this ends
	 * 		If *this is a undirected connection then one of the edgepoints is returned.
	 */
	inline
	pgl_vertexID_t
	getDestID() const noexcept
	{
		return m_id.getDestID();
	};


	/**
	 * \brief 	Retrun the edgeID of *this
	 */
	inline
	pgl_edgeID_t
	getEdgeID() const noexcept
	{
		return m_id;
	};


	/**
	 * \brief 	Decompose the edgeID
	 *
	 * This returns rthe decomposed ID. This is a pair<vertexID, vertexID>.
	 * If the connection is a directed connection, then first is the srcID and second the destID.
	 * If *this is not directed the ordering is arbitrary (you can generally expect that first is the smaller ID),
	 */
	inline
	std::pair<pgl_vertexID_t, pgl_vertexID_t>
	decomposeID() const
	{
		return m_id.decomposeID();
	};


	/**
	 * \brief 	Returns true if node is part of the connection.
	 * \param nodeID The ID of the node one want to test.
	 * \pre 	nodeID is valid
	 *
	 * In the end this function tests if either src or dest is equal the presented id
	 */
	inline
	bool
	hasID(
		const pgl_vertexID_t& nodeID) const
	{
		pgl_assert(nodeID.isValid() == true);

		const auto IDs = m_id.decomposeID();

		if(IDs.first == nodeID || IDs.second == nodeID)
		{
			return true;
		}
		else
		{
			return false;
		};
	};


	/**
	 * \brief 	thest if ID is the src of *this
	 * \param ID	An ID
	 */
	inline
	bool
	isSrc(
		const pgl_vertexID_t& ID) const
	{
		return ID == m_id.getSrcID();
	};


	/**
	 * \brief 	Tests if ID is the dest ID of *this
	 *\param ID 	An ID
	 */
	inline
	bool
	isDest(
		const pgl_vertexID_t& ID) const
	{
		return ID == m_id.getDestID();
	};




	/**
	 * \brief 	This function test if *this is valid.
	 * 		It is a lightweight version.
	 *
	 * If one wants to have a real sanity check use the checkIntegrity function.
	 * It does not check, if the stored vertex are valid.
	 * this is done in the check integrity function
	 */
	inline
	bool
	isValid() const noexcept(false)
	{
#ifdef PGL_NDEBUG
		return this->isProperBuild() 				//Test if proper build
			&& this->m_id.isValid() 			//Test if iD is Valid
			&& this->m_vertexSrc != nullptr 		// Test if pointer is there
			//&& pgl_vertexSAF_isValid(this->m_vertexSrc)	//To dangerous to use
			&& this->m_vertexDest != nullptr
			//&& pgl_vertexSAF_isValid(this->m_vertexDest)
			&& this->getSrcID() == pgl_vertexSAF_getID(m_vertexSrc)
			&& this->getDestID() == pgl_vertexSAF_getID(m_vertexDest);
#else
		bool result = this->isProperBuild();
		pgl_assert(result);

		result = result && this->m_id.isValid();
		pgl_assert(result);

		result = result && m_vertexSrc != nullptr;
		pgl_assert(result);

		result = result && m_vertexDest != nullptr;
		pgl_assert(result);

		result = result && (getSrcID() == pgl_vertexSAF_getID(m_vertexSrc));
		pgl_assert(result);

		result = result && (getDestID() == pgl_vertexSAF_getID(m_vertexDest));
		pgl_assert(result);

		return result;

#endif
	};



	/**
	 * \brief	Check integrity of *this
	 *
	 * This function checks the integrity of this.
	 * This is a quite coslty function, that does a bit more than the isValid  function.
	 */
	bool
	checkIntegrity() const;


	bool
	isValidConnection() const = delete;


	/**
	 * \brief 	This function tests if the underlining connection is propper builded.
	 */
	inline
	bool
	isProperConnection() const
	{
		return this->isProperBuild();
	};



	/**
	 * \brief	This function retruns a (non-const) reference to the src vertex
	 */
	inline
	Vertex_ref
	getSrcNode()
	{
		return *(this->m_vertexSrc);
	};


	/**
	 * \brief 	This function returns a const reference to the src node
	 */
	inline
	Vertex_cref
	getSrcNode() const
	{
		return *(this->m_vertexSrc);
	};


	/**
	 * \brief	Get a const reference to the src node (explicit)
	 */
	inline
	Vertex_cref
	getCSrcNode() const
	{
		return *(this->m_vertexSrc);
	};


	/**
	 * \breif 	Get a (non-const) reference ot the dest node
	 */
	inline
	Vertex_ref
	getDestNode()
	{
		return *(this->m_vertexDest);
	};


	/**
	 * \brief 	Get a const reference to the dest node
	 */
	inline
	Vertex_cref
	getDestNode() const
	{
		return *(this->m_vertexDest);
	};


	/**
	 * \brief 	Get a const reference to the dest node (explicit)
	 */
	inline
	Vertex_cref
	getCDestNode() const
	{
		return *(this->m_vertexDest);
	};




	/*
	 * ============================================================
	 * Payload Funtion
	 * \brief 	The folowing functions are used for interacting with the payload
	 *
	 * These function mainly depends on the payload.
	 * Calling a function requires that the underling payload implemnt the property.
	 */


	/**
	 * \brief 	This function checks if *this has a valid payload
	 */
	inline
	bool
	hasPayload() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return false;
#else
		return (m_payload == nullptr);
#endif
	};


	/**
	 * \brief 	This function returns the pointer to the payload
	 */
	inline
	Payload_ptr
	getPayload()
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return this->m_payload.get();
#endif
	};

	/**
	 * \brief	Get a pointer to the const payload
	 */
	inline
	Payload_cptr
	getPayload() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		return nullptr;
#else
		return this->m_payload.get();
#endif
	};



	/**
	 * \brief	Allow the user to casts the payload to anytype he want
	 * 		There is no check performed.
	 * \tparam userRequestedType The type the user requested, shall not be a pointer.
	 *
	 * If you whant a check then use the polymorfic version
	 */
	template<typename userRequestedType>
	std::decay_t<userRequestedType>*
	CastPayload_To() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("The Payload is disabled");
#else
		using ret = std::decay_t<userRequestedType>*;
		return static_cast<ret>(this->m_payload.get());
#endif
	};


	/**
	 * \brief 	Perfoms a polymorphic cast of the payload tot the provided type.
	 * \tparam userRequestedType The type the user requested, shall not be a pointer.
	 */
	template<typename userRequestedType>
	std::decay_t<userRequestedType>*
	PolyCastPayload_To() const
	{
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
		throw PGL_EXCEPT_illMethod("The Payload is disabled");
#else
		using ret = std::decay_t<userRequestedType>*;
		return dynamic_cast<ret>(this->m_payload.get());
#endif
	};


	/*
	 * ===============================
	 * Comperators
	 */

	/**
	 * \brief	Returns thue if the id of *this is smaller than the id of rhs
	 */
	inline
	bool
	operator< (
		const pgl_connection_t& rhs) const
	{
		return this->m_id < rhs.m_id;
	};





	/*
	 * ========================================
	 * Private Data-Member
	 */
private:

	/// This is a pointer to the vertex vehere the connection starts, if it is a directed.
	/// If not then the pointer points to the vertex which has the src ID.
	pgl_vertex_t* 		m_vertexSrc;

	/// This is the pointer that points to the dest vertex. Same story as the src vector.
	pgl_vertex_t* 		m_vertexDest;

	/// This is the ID of the connection
	pgl_edgeID_t 		m_id;

#ifndef PGL_GRAPH_DISABLE_PAYLOADS
	/// This is the payload, it is a unique ownership.
	iPayload_ptr 		m_payload;
#endif
}; //End class(pgl_connection_t)



PGL_NS_END(graph)
PGL_NS_END(pgl)


#endif //end include guard
