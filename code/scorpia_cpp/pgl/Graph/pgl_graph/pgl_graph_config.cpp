/**
 * \brief	This file contains code that is related to the config file.
 *
 * This file currently contains message pragmas. Locating them in this file will
 * make that it is outputted only once per compilation.
 *
 * NOTE
 * THIS FILE IS NOT LINKED INTO PGL
 */

#include "./pgl_graph_config.hpp"


//Give a message, if multiconnections are allowed.
#ifdef PGL_GRAPH_ALLOW_MULTICONNECTIONS
	#pragma message "Multiconnections are enabled in the graph."
#endif



//Message if reduced integrity checks are in place
#ifdef PGL_GRAPH_REDUCED_INTEGRITY
	#pragma message "Reduced integrity check is in place."
#endif





#ifdef PGL_GRAPH_DISABLE_PAYLOADS
	#pragma message "Disable Payloads in the graph."
#endif



