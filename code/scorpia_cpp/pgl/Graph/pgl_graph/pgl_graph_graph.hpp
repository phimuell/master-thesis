#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_GRAPH_HPP
#define PGL_GRAPH__PGL_GRAPH_GRAPH_HPP
/**
 * \file 	pgl_graph/pgl_graph_graph.hpp
 * \brief 	Defines the graph
 *
 * This file defines the pgl_graph.
 * It is a container, that supports some functions, that one can use.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl_util/pgl_rangeBasedLoopAdaptor.hpp>
#include <pgl_util/pgl_revision_index.hpp>

#include <pgl_stable_vector.hpp>
#include <pgl_multimap.hpp>
#include <pgl_reference_container.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>

#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>
#include <pgl_graph/pgl_graph_graph_connection_iterators.hpp>


//Inclide STD
#include <string>


PGL_NS_START(pgl)
PGL_NS_START(graph)

/**
 * \brief 	This is the graph
 * \class 	pgl_graph_t
 *
 * This class is the graph.
 * It is mainly a collection for the vertices and the edges.
 * It allows allows iteration over the vertices and connection.
 * It allows O(1) access of a vertex.
 * To access a specific edge is not so fast, since they can be in theory multisets.
 * So pgl::MultiMap_t is used for stoing them.
 * This implies that the consts are O(log(n)) to access a specific element.
 * As key we use the ID. We remember that C++14 suppotrs this trnsparent comperators.
 *
 * The graph is build by using a builder class, see the file there.
 *
 * The graph has a feature that is very notable, its iterators are stable.
 * This means that as long as the assciated element is not removed they are valid.
 * This holds for the connections and the vertices, and also for the end() iterator.
 *
 *
 * The graph also provides a simple mechanism, to detect modifications.
 * This is a simpleminded supstitute for an observer pattern.
 * A graph has a revision index, when the graph is constructed (regardless how)
 * this counter is initialized to zero.
 * The counter is increased each time the graph is completed and uncompleted.
 *
 */
class pgl_graph_t
{
	/*
	 * ===============================
	 * Some usefull typedefs
	 */

	//Internal typedefs
private:

	/// This class stores the vertices
	using VertexContainer_t		= ::pgl::StableVector_t<pgl_vertex_t>;

	/// INternal vertex iterators
	using intNodeIterator_t		= VertexContainer_t::iterator;
	using const_intNodeIterator_t 	= VertexContainer_t::const_iterator;

	/// This class stores the connections
	/// The multimap uses the transparent less
	using ConnectionContainer_t	= ::pgl::MultiMap_t<pgl_edgeID_t, pgl_connection_t, std::less<> >;

	/// Internal connection iterator
	using intConnIterator_t		= ConnectionContainer_t::iterator;
	using const_intConnIterator_t	= ConnectionContainer_t::const_iterator;

	using intConnRange_t 		= std::pair<intConnIterator_t, intConnIterator_t>;
	using const_intConnRange_t 	= std::pair<const_intConnIterator_t, const_intConnIterator_t>;


public:
	//Vertex
	using Vertex_t 			= ::pgl::graph::pgl_vertex_t;			//!< This is the type of the vertex
	using Vertex_ptr 		= ::pgl::graph::pgl_vertex_t*;			//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= const ::pgl::graph::pgl_vertex_t*;		//!< This is the pointer to a constant vertex
	using Vertex_ref		= ::pgl::graph::pgl_vertex_t&;			//!< This is the type of a reference to a node
	using Vertex_cref		= const ::pgl::graph::pgl_vertex_t&;		//!< This is the constant reference to a vertex

	using VertexPayLoad_ptr		= ::pgl::graph::pgl_vertex_payload_t*;		//!< This is the payload of the vertex
	using VertexPayLoad_cptr  	= const ::pgl::graph::pgl_vertex_payload_t*;	//!< This is the payload of the vertices

	using VertexID_t 		= ::pgl::graph::pgl_vertexID_t;			//!< This is the type of the vertex id


	//Connection
	using Connection_t 		= ::pgl::graph::pgl_connection_t;		//!< Type of the connection
	using Connection_ptr		= pgl_connection_t*;				//!< Pointer to a connection
	using Connection_cptr		= const pgl_connection_t*;			//!< Pointer to a constant connection
	using Connection_ref 		= pgl_connection_t&;				//!< Reference to connection
	using Connection_cref		= const pgl_connection_t&;			//!< Const reference to connection
	using ConnPayLoad_ptr		= ::pgl::graph::pgl_connection_payload_t*;	//!< Pointer to a vertex payload
	using ConnPayLoad_cptr		= const ::pgl::graph::pgl_connection_payload_t*;	//!< Pointer to a vertex payload

	using ConnectionID_t		= ::pgl::graph::pgl_edgeID_t;			//!< This is the type of the connection id


	//Edge
	using Edge_t 			= ::pgl::graph::pgl_edge_t; 			//!< Type of the edge
	using Edge_ptr			= pgl_edge_t*;					//!< Pointer to a edge
	using Edge_cptr			= const pgl_edge_t*;				//!< Pointer to a constant edge
	using Edge_ref 			= pgl_edge_t&;					//!< Reference to edge
	using Edge_cref			= const pgl_edge_t&;				//!< Const reference to edge

	using EdgeID_t			= ::pgl::graph::pgl_edgeID_t;			//!< Type of the edge ID

	/*
	 * This are typedefs that are allowing the use of complexer builder.
	 */
	using ConnDescription_t 	= ::pgl::graph::pgl_abstract_gBuilder_t::ConnDescribtor_t;	//!< This type describes a connection
	using ConnDescrContainer_t 	= ::pgl::pgl_vector_t<ConnDescription_t>;			//!< This is a container of connection descriptiers.


	/*
	 * The public iterators
	 * The special implementation is something that could change.
	 */
public:

	/// The public node iterator
	using NodeIterator_t 		= intNodeIterator_t;

	//This is the range version of the Node iterator
	using NodeRange_t 		= ::pgl::pgl_rangeBasedLoppAdaptor_t<NodeIterator_t, false>;

	/// The public constant node iterator
	using const_NodeIterator_t 	= const_intNodeIterator_t;

	// This is the constant version of the node iterator
	using const_NodeRange_t		= ::pgl::pgl_rangeBasedLoppAdaptor_t<const_NodeIterator_t, true>;

	/// The public connection iterator
	using ConnIterator_t 		= internal::pgl_graph_pub_connection_iterator_t<false, pgl_connection_t, intConnIterator_t>;

	/// The public connection range
	using ConnRange_t 		= ::pgl::pgl_rangeBasedLoppAdaptor_t<ConnIterator_t, false>;

	/// The public const connection iterator
	using const_ConnIterator_t 	= internal::pgl_graph_pub_connection_iterator_t<true, pgl_connection_t, const_intConnIterator_t>;

	/// The public const connection range
	using const_ConnRange_t 	= ::pgl::pgl_rangeBasedLoppAdaptor_t<const_ConnIterator_t, true>;

	/*
	 * Some other usefull typedefs
	 */

	/**
	 * \brief 	Return type of the remove connection function.
	 *
	 * This type is returned by the remove connection function.
	 * It is a pair<bool, ConnIterator_t>.
	 * first indicated the exitstatus of the function, true is success.
	 * second is an iterator that points to the one after the removed one.
	 */
	using ConnRemoveRet_t 		= std::pair<bool, ConnIterator_t>;

	/**
	 * \brief 	Return type of the remove node function.
	 *
	 * This type is returned by the remove node function.
	 * It is a pair<bool, NodeIterator_t>.
	 * first indicated the exitstatus of the function, true is success.
	 * second is an iterator that points to the one after the removed one.
	 */
	using NodeRemoveRet_t 		= std::pair<bool, NodeIterator_t>;


	/**
	 * \brief 	Connection Reference container
	 *
	 * This si a reference container, that contains reference to the connections
	 */
	using ConnRefContainer_t 	= ::pgl::pgl_refVec_t<::pgl::graph::pgl_connection_t>;
	using const_ConnRefContainer_t 	= ::pgl::pgl_refVec_t<const ::pgl::graph::pgl_connection_t>;


	/**
	 * \brief	RevisionIndex
	 *
	 * This is the type of the revion index.
	 * As said before, the index is increamented each time the graph is compületed and uncompleted.
	 * This allows algorihm to detect if something could have been going on.
	 * This is not perfect, but is a cheap and fairly safe solution (in my opinion)
	 */
	 using RevisionIndex_t 		= ::pgl::pgl_revision_index_t;
	 using RevisionState_t		= ::pgl::pgl_revision_state_t;	//!< This is the inmutable state of the reviosion index.



	/*
	 * ============================================
	 * The Constructors.
	 *
	 * Some of them are defaulted or forbidden.
	 */


	/**
	 * \brief 	Default Constructor
	 *
	 * The default constructor is not defaulted, but it does some things.
	 */
	pgl_graph_t();


	/**
	 * \brief 	The copyconstructor
	 *
	 * This constructor is deleted, since copying is delicate, and we don't need it right now.
	 */
	pgl_graph_t(
		const pgl_graph_t&) = delete;


	/**
	 * \brief	Copy Assignment
	 *
	 * We also delete this assignment.
	 */
	pgl_graph_t&
	operator= (
		const pgl_graph_t&) = delete;


	/**
	 * \brief 	Move Constructor
	 *
	 * We delete it.
	 */
	pgl_graph_t(
		pgl_graph_t&&) = delete;


	/**
	 * \brief	Move Assignment
	 *
	 * We delete it.
	 */
	pgl_graph_t&
	operator= (
		pgl_graph_t&&) = delete;


	/**
	 * \brief 	Builder constructor
	 *
	 * This constructor is used to build a graph.
	 * It takes a builder and uses it to build itself.
	 *
	 * \param Builder	The used builder.
	 */
	pgl_graph_t(
		pgl_abstract_gBuilder_t& 	Builder) noexcept(false);



	/*
	 * ====================================
	 * Inserting and managment functions
	 *
	 * This functions are used to add and remove components form the graph.
	 */


	/**
	 * \brief	This functions clears the graph
	 *
	 * This function is used to clear the graph.
	 * The graph must be uncomplete to work.
	 * This function does not call the remove function for every single edge.
	 *
	 * Instead it calls the clear function of the underlying container.
	 * Accoeinng to the standard this does not free memory, the container is still in possesion of the meory
	 * Since pgl uses boost container, it is not fully clear to my, how they work.
	 * There is at least no way yet, to completly free the memory.
	 */
	void
	clearTheGraph_DeleteEverything();


	/**
	 * \brief 	This function uses a builder to build a graph
	 *
	 * For that the graph must be empty, meaning that it was default constructed,
	 * or the clean function was called.
	 * The graph must have no vertices nor anyrthing else.
	 * For the sake of conformace, the graph must be uncomplete.
	 *
	 * \param Builder	A builder used to create the graph.
	 *
	 * \note	Is defined in the constructor file
	 */
	void
	build_graph_from_builder(
		pgl_abstract_gBuilder_t&	Builder) noexcept(false);


	/**
	 * \brief	This function uses a ConnDescrContainer_t to \emph{extend} this
	 *
	 * The container stores connection, that we whant to ADD to *this.
	 * This is efficient, because, first the container is scanned and then the space for the edges is extended.
	 *
	 * Notice, that the connection are added and the old ones are NOT removed.
	 * The same restrictions, as for the sequentiel insertion holds.
	 *
	 * \param newConnections 	The connections we whant to add.
	 */
	void
	add_additional_connections(
		const ConnDescrContainer_t& 	newConnections) noexcept(false);




	/**
	 * \brief	Create a vertex
	 *
	 * This function creates a new vertex, with the given arguiments
	 * an Iterator to the newly construct vertex is returned.
	 *
	 * Remener that the vertex aquire exclusive ownership of the pointer to the payload.
	 * So no managment by the client is required.
	 *
	 * Other vertex iterators are not invalidated.
	 *
	 * \param ID		The ID of vertex (must be unique) inside the graph
	 * \param KindOfNode	A GraphTag, taht describes the node to be
	 * \param payload	A Pointer of type vertex pointer, nullptr is also accepted
	 *
	 * \return 		An iterator to the vertex.
	 *
	 * There is another internal function that does the same thing.
	 * But that function makes no concistency checks.
	 * It should be called by the function that uses the builder.
	 */
	NodeIterator_t
	createNewNode(
		const pgl_vertexID_t&		ID,
		const GraphTag			KindOfNode,
		VertexPayLoad_ptr 		payload) noexcept(false);


	/**
	 * \brief 	Create a connection
	 *
	 * This functions crates a connection (meaing will allocate memeory for the connection object).
	 * It will also create the endges in the respective vertieces.
	 * Remember that this vill invalidates, the iterators that are provided by the vertex class.
	 * The function will return an iterator to the newly created connection.
	 *
	 * The graph must be uncomplete for this function to work.
	 * No connection iterators are invalidaded.
	 *
	 * \param Src		The ID of the vertex of the source of the connection
	 * \param Dest		The ID of the vertex that is the destination of the connection
	 * \param KindOfConn	A GraphTag, that describes the connection
	 * \param payload	The payload of the connection, nullptr is also accepted
	 *
	 * \return 		An iterator to the newly created connection
	 */
	ConnIterator_t
	createNewConnection(
		const pgl_vertexID_t&		Src,
		const pgl_vertexID_t&		Dest,
		const GraphTag 			KindOfConnection,
		ConnPayLoad_ptr 		payload) noexcept(false);



	/**
	 * \brief	Removes a Connection
	 *
	 * This function removes a connection.
	 * The connection is removed and the two edges, that referes to it, are also deleted.
	 * In order to work, the graph must be in an incomplete state. Failing to meet this requirement is consireder an error.
	 *
	 * The connection is dealocated, since the connection is not observed, the user must ensure that all edge, outside the graph,
	 * something that is discurated from, are deleted or not used anylonger.
	 *
	 * A bool is returned to indicate the success of the operation.
	 * This operation can throw.
	 * The other connection iterators are not invalidated.
	 *
	 * To identify the connection, a connection iterator must be presented.
	 *
	 * \param connectionToDispose		A connection iterator, that we dispose of.
	 *
	 * \return 				An entety of type ConnRemoveRet_t
	 * \throw 				May throw.
	 */
	ConnRemoveRet_t
	removeConnection(
		ConnIterator_t 			connectionToDispose) noexcept(false);

	/**
	 * \brief 	Removes a vertex
	 *
	 * This function removes a vertex.
	 * The vertex must have degree zero, i.e. no incident edges, in order to work properly.
	 *
	 * The space of the vertex is deallocated.
	 * The other nodeIterators are not affected.
	 *
	 * If one wants to delete more than one elements, the elements should be processed in decreasing ID.
	 * Since the vector must reorder itself.
	 * So deleiting the last element is constant, but deleting the element p is linear in the distanc between p and last.
	 *
	 * \param vertexToDispose		The vertex that sould be removed.
	 *
	 * \return				A NodeRemoveRet object is returned to indicate the result
	 *
	 * \throw				You can bet your cookie.
	 */
	NodeRemoveRet_t
	removeVertex(
		NodeIterator_t 		vertexToDispose) noexcept(false);


	/**
	 * \brief	Removes the "last" vertex of *this.
	 *
	 * In general, the last vertex is the vertex with the highest ID that is stored inside *this.
	 * So this function removes this vertex.
	 * The requieremts are the same as the the one of the normal removfunction.
	 * But this function is faster.
	 *
	 * If no vertex exists, then an exception is thrown.
	 *
	 * \return 	A bool is returned by this function
	 */
	bool
	removeLastVertex() noexcept(false);



	/**
	 * \brief 		Remove Random
	 *
	 * This functions removes all the random stuff.
	 * It iterates through all the connections, if one of them is random, it is removed.
	 * Then the vertices are proccessed the same.
	 *
	 * Notice, that an original vertex, with degree Zero is not removed.
	 *
	 * If a random vertex is found, that does not have degree zero, an exeption is generated.
	 *
	 * The graph must be uncomplete to work.
	 *
	 *
	 * This function is most efficient if the funtion if all random vertices are palced at the end
	 * and consecutive
	 */
	void
	removeRandomComponents() noexcept(false);


	/**
	 * \brief		This function makes the grph complete
	 *
	 * This function iterates through all the components and makes it complete.
	 * It also checkes if thes are valid.
	 * If an unvalid object is found an exception is generated.
	 *
	 * This function may invalidate iterators, reference and pointers involved with thevertecies.
	 * The iterator provided by the graph are not affected.
	 * Ask Philip, if you want to know if something is save.
	 *
	 * A bool is returned that indicated success.
	 *
	 * If this fucntion is called and action are performed, then the revioion index of *this is incremented by one.
	 * This means that clling make complete on an allready complete graph does not change the revision index.
	 *
	 * \param Tag		Must be complete.
	 */
	bool
	makeComplete(
		const GraphTag 		Tag) noexcept(false);




	/**
	 * \brief		This functions makes the graph uncomplete
	 *
	 * This function iterates through all the components and call the make uncomplete functions on them.
	 * A bool is returned to indicate success.
	 * If an invalid function is found an exception is generated.
	 *
	 * This function makes sure that the state is uncomplete.
	 * If this is not possible an exception is generated.
	 *
	 * Calling this fucntion on an _complete_ graph, will change the reviosion index.
	 * The reviosion index is not modified, if the graph is allready uncomplete.
	 *
	 * \param Tag		Must be uncomplete.
	 */
	bool
	makeUncomplete(
		const GraphTag&		Tag) noexcept(false);



	/**
	 * \brief 		This function makes an integroty check of the graph
	 *
	 * This is an integrity check, iit ferst checks the integrity of any connection.
	 * Then it checks the integrity of any vertex (which will in turn check the validity of the connectoion again).
	 */
	bool
	checkIntegrity() const;



	/**
	 * \brief	Thsi function returns the current state of the reviosion.
	 *
	 * This is a imutable object, that refelects the current state of this.
	 */
	RevisionState_t
	getCurrentRevision() const noexcept;


	/**
	 * \brief	This calls the setTargetfunction of the payload off all nodes and set it to newTarget
	 *
	 * \param newTarget	The ID of the new target
	 */
	void
	setTargetNodeTo(
		const pgl_vertexID_t& 		newTarget);

	/*
	 * =================================
	 * Simple queries
	 *
	 * This functions implements some easy queries, that are inherent properties of the graph
	 */
public:
	/**
	 * \brief 	Returns the numbers of nodes
	 */
	Size_t
	nVertices() const noexcept;


	/**
	 * \brief	Returns the number of original nodes
	 */
	Size_t
	nOrgVertices() const noexcept;


	/**
	 * \brief 	Retruns the number of random vertices
	 */
	Size_t
	nRandVertices() const noexcept;


	/**
	 * \brief 	Returns the numbers of edges
	 */
	Size_t
	nConnections() const noexcept;


	/**
	 * \brief	Returns the number of original connections
	 */
	Size_t
	nOrgConnections() const noexcept;


	/**
	 * \brief	Returns the number of random connections
	 */
	Size_t
	nRandConnections() const noexcept;


	/**
	 * \brief	Returns the edge capacity
	 *
	 * The edge capacity is the summ of the edge capacity of all nodes, that are associated with the graph
	 */
	Size_t
	edgeCapacity() const noexcept;


	/**
	 * \brief 	returns true if *this is empty
	 *
	 * A graph is empty if it hasn't any vertices nor connections.
	 */
	bool
	isEmpty() const noexcept;


	/**
	 * \brief 	Reutns the complete status of this
	 *
	 * NOTE: unlike the other graph entities, the graph is a bit special.
	 * It holds a variable which stores that it was completed.
	 * This variable remains set untill the uncomplet function of the graph is called.
	 * But there is no garantee, that the graph will be complete then.
	 * If you wahnt to verify this use the verifyCompletness function wich will update the state an return it.
	 */
	bool
	isComplete() const noexcept;


	/**
	 * \brief 	This function verifies the completstatus of *this
	 *
	 * This fucntions checks all the compnents of the graph and updates them the stored status.
	 * As return value the isComplete right after the state is returned.
	 */
	bool
	verifyCompletness() noexcept;


	/*
	 * =====================
	 * Simple access functions
	 *
	 * Here are some simple access functions
	 */

	/**
	 * \brief 	Get an iterator to the vertex with the given id.
	 *
	 * \param ID	The ID to search for
	 *
	 * If the element could not be found the enditerator is returned.
	 * If the ID is invalid, then an out of bound exception is generated.
	 */
	NodeIterator_t
	getNode(
		const pgl_vertexID_t& ID) noexcept(false);

	const_NodeIterator_t
	getNode(
		const pgl_vertexID_t& ID) const noexcept(false);

	inline
	const_NodeIterator_t
	getCNode(
		const pgl_vertexID_t& ID) const noexcept(false)
	{
		return this->getNode(ID);
	};

	/**
	 * \brief 	Returns true if *this has an vertex with the ID.
	 */
	bool
	containVertexID(
		const pgl_vertexID_t& ID) const noexcept;


	/**
	 * \brief 	Get a range of iterators to the given connection
	 *
	 * \param ID	The Connection ID to search for.
	 *
	 * This function returns a range [first, last), which have the same ID as the provided one.
	 * If no such ID could be found, both are equal to the end iterator.
	 */
	ConnRange_t
	getConnections(
		const pgl_edgeID_t& 	ID) noexcept;

	const_ConnRange_t
	getConnections(
		const pgl_edgeID_t& 	ID) const noexcept;

	inline
	const_ConnRange_t
	getCConnections(
		const pgl_edgeID_t& 	ID) const noexcept
	{
		return this->getConnections(ID);
	};

	/**
	 * \brief 	Returns how many connections have the same ID as the provided one
	 */
	Size_t
	countConnectionID(
		const pgl_edgeID_t& 	ID) const noexcept;


	/**
	 * \brief 	this function returns a reference container that contains all the connections
	 *
	 * \return 	Retunrs a reference container of the connections
	 */
	ConnRefContainer_t
	getConnReferences();


	/*
	 * =================================
	 * Printers
	 */
	std::string
	print_topo() const;



	/*
	 * =============================
	 * Iterators
	 */
public:
#if 0
	ConnIterator_t
	begin() noexcept
	{
		return this->m_connections.begin();
	};

	const_ConnIterator_t
	cbegin() const noexcept
	{
		return this->m_connections.cbegin();
	};

	const_ConnIterator_t
	begin() const noexcept
	{
		return this->m_connections.cbegin();
	};

	ConnIterator_t
	end() noexcept
	{
		return this->m_connections.end();
	};

	const_ConnIterator_t
	end() const noexcept
	{
		return this->m_connections.cend();
	};


	const_ConnIterator_t
	cend() const noexcept
	{
		return this->m_connections.cend();
	};
#endif


	/*
	 * Give access to the connections
	 */
	inline
	ConnIterator_t
	ConnBegin() noexcept
	{
		return m_connections.begin();
	};

	inline
	const_ConnIterator_t
	ConnBegin() const noexcept
	{
		return m_connections.cbegin();
	};

	inline
	const_ConnIterator_t
	CConnBegin() const noexcept
	{
		return m_connections.cbegin();
	};

	inline
	ConnIterator_t
	ConnEnd() noexcept
	{
		return m_connections.end();
	};

	inline
	const_ConnIterator_t
	ConnEnd() const noexcept
	{
		return m_connections.cend();
	};

	inline
	const_ConnIterator_t
	CConnEnd() const noexcept
	{
		return m_connections.cend();
	};

	inline
	ConnRange_t
	Connections() noexcept
	{
		return ConnRange_t(this->ConnBegin(), this->ConnEnd());
	};

	inline
	const_ConnRange_t
	Connections() const noexcept
	{
		return const_ConnRange_t(this->CConnBegin(), this->CConnEnd());
	};

	inline
	const_ConnRange_t
	CConnections() const noexcept
	{
		return const_ConnRange_t(this->CConnBegin(), this->CConnEnd());
	};


	/*
	 * Gibing access to the vertex
	 */
	inline
	NodeIterator_t
	NodeBegin() noexcept
	{
		return m_vertices.begin();
	};

	inline
	const_NodeIterator_t
	NodeBegin() const noexcept
	{
		return m_vertices.cbegin();
	};

	inline
	const_NodeIterator_t
	CNodeBegin() const noexcept
	{
		return m_vertices.cbegin();
	};

	inline
	NodeIterator_t
	NodeEnd() noexcept
	{
		return m_vertices.end();
	};

	inline
	const_NodeIterator_t
	NodeEnd() const noexcept
	{
		return m_vertices.cend();
	};

	inline
	const_NodeIterator_t
	CNodeEnd() const noexcept
	{
		return m_vertices.cend();
	};

	inline
	NodeRange_t
	Nodes() noexcept
	{
		return NodeRange_t(this->NodeBegin(), this->NodeEnd());
	};

	inline
	const_NodeRange_t
	Nodes() const noexcept
	{
		return const_NodeRange_t(this->NodeBegin(), this->NodeEnd());
	};

	inline
	const_NodeRange_t
	CNodes() const noexcept
	{
		return const_NodeRange_t(this->NodeBegin(), this->NodeEnd());
	};




	/*
	 * ========================
	 * Private/Protected function
	 *
	 * These are internal functions that are not menat to be used by the user.
	 */
protected:
	/**
	 * \brief	This function transforms a public edge iterator to an internal iterator
	 */
	static
	inline
	intConnIterator_t
	prot_convertToInternal_Conn(
		const ConnIterator_t& it) noexcept(noexcept(intConnIterator_t(it.base() )))
	{
		return it.base(); 	//This is a temproary, tis can change.
	};


	/**
	 * \brief 	This function transforms a public node iterator to an internal
	 */
	static
	inline
	intNodeIterator_t
	prot_convertToInternal_Node(
		const NodeIterator_t& it) noexcept(noexcept(intNodeIterator_t(it)))
	{
		return it;
	};


	/**
	 * \brief 	This function removes the presed connection from a vertex.
	 *
	 * \param ID	The ID of the node from which we want delete.
	 * \param Conn	The connection we want delete (intConnIterator)
	 *
	 * This function is meant to be used as internal fuinction.
	 * It assumes that all the requirements are meet tho dispose the connection.
	 * It does not remove the connection_from the collection of connections that the graph stores.
	 *
	 * \return 	It returns true on success
	 */
	bool
	prot_removeEdgeFromNode(
		const pgl_vertexID_t&		ID,
		const_intConnIterator_t 	ConnIt);


	/**
	 * \brief 	This function works does the dirty work of the the connection deleting.
	 *
	 * It works like tho other one but works on internal iterators
	 */
	ConnRemoveRet_t
	prot_removeConnection(
		ConnIterator_t 			connectionToDispose) noexcept(false);


	/**
	 * \brief	This function does the same as the not protected one, but works on internal connections
	 */
	NodeRemoveRet_t
	prot_removeVertex(
		NodeIterator_t 		vertexToDispose) noexcept(false);


#if 0
	/*
	 * It seams that thoes functions are not used anywhere.
	 * The uncompleting happens directly
	 */

	/**
	 * \brief 	This is the interal function that makes this uncomplete
	 *
	 * It assumes that all requirements for for that are meet.
	 * If this function have no affect, if the graph is allready uncomplete.
	 */
	bool
	prot_make_uncomplete();

	/**
	 * \brief 	this is the internal function that makes the graph complete.
	 *
	 * It assumes that all requirements are meet.
	 * Completing an allready completeted graph has no effect.
	 */
	bool
	prot_make_complete();
#endif


	/**
	 * \brief	Create a vertex (internal)
	 *
	 * This function creates a new vertex, with the given arguiments
	 * an Iterator to the newly construct vertex is returned.
	 *
	 * Remener that the vertex aquire exclusive ownership of the pointer to the payload.
	 * So no managment by the client is required.
	 *
	 * Other vertex iterators are not invalidated.
	 *
	 * \param ID		The ID of vertex (must be unique) inside the graph
	 * \param KindOfNode	A GraphTag, taht describes the node to be
	 * \param payload	A Pointer of type vertex pointer, nullptr is also accepted
	 *
	 * \return 		An iterator to the vertex.
	 *
	 * This function does the same as the public one, but does not make concistency checks.
	 * This function is intended to be used, if many nodes are inserted right after another.
	 */
	NodeIterator_t
	prot_createNewNode(
		const pgl_vertexID_t&		ID,
		const GraphTag			KindOfNode,
		VertexPayLoad_ptr 		payload) noexcept(false);



	/*
	 * ========================
	 * Private Memebrs
	 */
public:
	VertexContainer_t 		m_vertices;		//!< Variable holding the vertices.
	ConnectionContainer_t 		m_connections;		//!< Variable holding the vertices.

	Size_t 				m_nRandVertices;	//!< Number of random vertices
	Size_t 				m_nRandConnections;	//!< Number of random connections.

	bool 				m_isComplete;		//!< If true, *this is complete.
	RevisionIndex_t			m_revIndex;		//!< This si teh reviosion index
}; //End class(pgl_graph_t)



PGL_NS_END(graph)
PGL_NS_END(pgl)






#endif	//End of include guard
