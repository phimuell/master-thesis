#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_VERTEXID_HPP
#define PGL_GRAPH__PGL_GRAPH_VERTEXID_HPP
/**
 * \brief	This file implements a class that models a vertex ID.
 *
 * A vertex ID is basically a number that names the Vertex.
 */

//Include PGL-Headers
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_int.hpp>


//Include C++ Headers
#include <iostream>
#include <string>



PGL_NS_START(pgl)
PGL_NS_START(graph)


/**
 * \brief	This is the vertex ID class
 * \class 	pgl_vertexID_t
 *
 * This class models the ID of the vertex.
 * Inside a graph the ID must be unique, meaning that no other vertex is allowed to have the same ID.
 * But in other graphs the same ID can exists.
 * An ID is aware of its state, meaning that it knwos if it is a valid ID or not.
 *
 * The underlying type is a unsigned 32 bit integer.
 * The first 31 bits are used for the ID and the last one bit is used to store the state.
 *
 */
class pgl_vertexID_t
{
	/*
	 * Public Typedefs
	 */
public:

	//The underlying type we use for the representation
	using Base_t = pgl::uInt32_t;


	/*
	 * Private static variable
	 */
private:
	constexpr static uInt_t ce_ID_BitLength 	= 31; 		//!< Numbers of bits, that represents the actual ID of the vertex
	constexpr static uInt_t ce_ZeroStatusMask 	= 0x7fffffff;	//!< Mask to zero out the status field


	/*
	 * Constructors
	 */
public:

	///We allow copy assignment
	pgl_vertexID_t&
	operator= (
		const pgl_vertexID_t&) noexcept = default;


	///We allow move assignment
	pgl_vertexID_t&
	operator= (
		pgl_vertexID_t&& other) noexcept
	{
		m_id = other.m_id;
		other.m_id = INVALID().m_id;

		return *this;
	};


	///We allow copy construction
	pgl_vertexID_t(
		const pgl_vertexID_t&) noexcept = default;


	///We allow move construction
	///It can't be the default because we have to take care of the other vertex
	pgl_vertexID_t(
		pgl_vertexID_t&& other) noexcept
	{
		//Fore some unkowon reasons this don't work
		//this->m_id = ::std::exchange(other.m_id, std::move(INVALID()) );

		this->m_id = other.m_id;
		other.m_id = INVALID().m_id;
	};



	///Defaulted destructor
	~pgl_vertexID_t() noexcept = default;


	/**
	 * \brief this constructer is the only way to construct a vertexID.
	 * The bool parameter is not used, but this way thewre is only one way to xonstruct an ID
	 */
	explicit
	inline
	pgl_vertexID_t(
		const Base_t supposedID,
		const bool) noexcept :
	  m_id(supposedID)
	{
		//it is forbidden to construct an invalid ID deliberate
		if(this->isInvalid() )
		{
			pgl_assert(this->isValid() );
		};
	}; //End constructor


	/**
	 * \brief the default constructor creats an invalid ID
	 */
	inline
	pgl_vertexID_t() noexcept
	{
		m_id = ~(Base_t(0));	//All one
	};


	/**
	 * \brief this constructor is private and it is intended to be used by the INVALID function
	 */
private:
	inline
	constexpr
	pgl_vertexID_t(
		const double) noexcept : m_id(~Base_t(0))
	{
	};






	/*
	 * Public member functions
	 */
public:

	/**
	 * \brief Returns true if the ID is a valid ID
	 */
	constexpr
	inline
	bool
	isValid() const noexcept
	{
		return (m_id >> ce_ID_BitLength) == 0;
	};


	/**
	 * \brief treturns true is the ID is invalid
	 */
	constexpr
	inline
	bool
	isInvalid() const noexcept
	{
		return !(this->isValid());
	};


	/**
	 * \brief Retrurns the ID as an Base type
	 * \pre *this has to be a valid ID
	 */
	inline
	Base_t
	getID() const noexcept
	{
		assert(this->isValid());
		return Base_t(m_id & ce_ZeroStatusMask);
	};


	/**
	 * \brief	This function prints the ID as a string
	 *
	 * If *this is invalid "INVALID" is printed instead of an ID
	 */
	inline
	std::string
	print() const
	{
		return (isValid() == true ? std::to_string(m_id) : std::string("INVALID"));
	};




#if 0
	/**
	 * \brief does the same as get ID only as operator
	 * \pre *this also has to be a valid ID
	 */
	inline
	operator Base_t() const
	{
		return this->getID();
	};
#endif

	/**
	 * \brief this function allows to explicitly create an invalid ID
	 */
	inline
	static
	pgl_vertexID_t
	INVALID()
	{
		return pgl_vertexID_t(1.0);
	};



	/**
	 * \brief 	This function returns true, if the provided number could serve as valid ID
	 */
	inline
	constexpr
	static
	bool
	CAN_BE_AN_ID(
		const Base_t&	number)
	{
		return (number & (~ce_ZeroStatusMask)) == 0 ? true : false;
	};





	/*
	 * Comparision Operator
	 */
public:

	/**
	 * \breif less compare
	 *
	 * This function compares *this with the argument, used as rhs.
	 * The comparision is such that invalid IDs are allways greater than valid IDs
	 */
	inline
	bool
	operator< (
		const pgl_vertexID_t& rhs) const noexcept
	{
		return this->m_id < rhs.m_id;
	};

	inline
	bool
	operator< (
		const Base_t& rhs) const noexcept
	{
		return this->m_id < rhs;
	};



	/**
	 * \brief Compares of equality
	 *
	 * The comaprision is suc, that invalid ID are always equal.
	 */
	inline
	bool
	operator==(
		const pgl_vertexID_t& rhs) const noexcept
	{
		const bool thisStatus = this->isInvalid();
		const bool rhsStatus  = rhs.isInvalid();

		//test if at least one is invalide
		if(thisStatus || rhsStatus)
		{
			return (thisStatus && rhsStatus); //If both are invalid this is true, and is exactly what we whant
		};

		//Both are valid
		return this->m_id == rhs.m_id;
	}; //End operator==

	inline
	bool
	operator== (
		const Base_t& rhs) const noexcept
	{
		return this->operator==(pgl_vertexID_t(rhs, true));
	};



	/**
	 * \brief Test for inequality
	 */
	inline
	bool
	operator !=(
		const pgl_vertexID_t& rhs) const noexcept
	{
		return !(this->operator==(rhs));
	};

	inline
	bool
	operator !=(
		const Base_t& rhs) const noexcept
	{
		return this->operator!=(pgl_vertexID_t(rhs, true));
	};



	/*
	 * Private member variabel
	 */
private:
	Base_t 		m_id;		//!< The member that represent the id
					//!  A Zero in the higest bit indicates a valid id


	friend
	inline
	bool
	operator< (
		const pgl_vertexID_t::Base_t lhs,
		const pgl_vertexID_t& rhs) noexcept;

}; //End class(pgl_vertexID_t)




inline
bool
operator== (
	const pgl_vertexID_t::Base_t& lhs,
	const pgl_vertexID_t& rhs) noexcept
{
	return rhs == lhs;
};

inline
bool
operator!= (
	const pgl_vertexID_t::Base_t& lhs,
	const pgl_vertexID_t& rhs) noexcept
{
	return rhs != lhs;
};

inline
bool
operator< (
	const pgl_vertexID_t::Base_t lhs,
	const pgl_vertexID_t& rhs) noexcept
{
	return lhs < rhs.m_id;
};







/**
 * \breif overload the out stream operator for the vertex ID
 */
inline
std::ostream&
operator<<(
	std::ostream& out,
	const ::pgl::graph::pgl_vertexID_t& ID)
{
	if(ID.isValid())
	{
		//ID is valid
		return (out << ID.getID());
	}
	else
	{
		//INVALID
		return (out << "INVALID");
	};

}; //End operator<< (ostream)


PGL_NS_END(grpah)
PGL_NS_END(pgl)





/*
 * Overload some standard predicates
 */
namespace std
{


	/**
	 * \brief std::hash<pgl_vertexID_t>
	 *
	 * This is a specialization of the std:hash struct for vertexID
	 * This way they become hashable
	 */
	template <>
	struct hash<::pgl::graph::pgl_vertexID_t>
	{
		size_t operator()(
			const ::pgl::graph::pgl_vertexID_t& x) const
		{
			return m_privHash(x.getID() );
		}; //End operator

	private:
		std::hash<::pgl::graph::pgl_vertexID_t::Base_t>	m_privHash;
	}; //End struct(hash)


#if 1
	/**
	 * \brief	This is a specialization of the to_string function
	 *
	 * This function is a overloading of the std::to_string function.
	 * It is provided for conveniance
	 *
	 * It returns a string with the number of the ID or INVALID
	 *
	 * \note defined in the cpp file of the vertexID
	 */
	::std::string
	to_string(
		::pgl::graph::pgl_vertexID_t id);
#endif


}; //End namespace(std)







#endif //End include guard
