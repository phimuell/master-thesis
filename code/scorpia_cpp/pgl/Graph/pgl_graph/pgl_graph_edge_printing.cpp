/**
 * \file	pgl_graph/pgl_graph_edge_printing.cpp
 * \brief 	Contains the implementation for the printing functions of the edge
 */




//PGL-Stuff
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex_saf.hpp>
#include <pgl_graph/pgl_graph_graphtag.hpp>

#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_graph/pgl_graph_connection.hpp>
#include <pgl_graph/pgl_graph_connection_base.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>


//STD-Stuff
#include <sstream>

PGL_NS_START(pgl)
PGL_NS_START(graph)


std::string
pgl_edge_t::print_topo() const
{
	std::stringstream s;
	s << "Edge " << this->m_id << "\n";
	s << "Belonges to = " << this->getOwningNodeID() << " (" << &(this->getCOwningNode()) << ")"
		<< ", points to = " << this->getOtherNodeID() << " (" << &(this->getOtherNode()) << ")\n";

	s << "Is Valid: " << (this->isValid() ? "Yes" : "No") << "\n";
	s << "Is Directed: " << (this->isDirected() ? "Yes" : "No") << "\n";
	s << "Is Random: " << (this->isRandom() ? "Yes" : "No") << "\n";
	s << "Is incoming: " << (this->isIncoming() ? "Yes" : "No") << "\n";
	s << "Is Leaving: " << (this->isLeaving() ? "Yes" : "No") << "\n";
	s << "Underlying connection :" << m_connPtr;

#ifndef PGL_GRAPH_DISABLE_PAYLOADS
	s << "\n";
	s << "Payload: " << m_payloadPtr;
#endif
	return s.str();
};





PGL_NS_END(grpah)
PGL_NS_END(pgl)




