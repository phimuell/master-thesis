/**
 * \brief	Implements some vector functions
 * \file	pgl_graph/pgl_graph_vertex.cpp
 *
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_utility.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_edge.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>
#include <pgl_graph/pgl_graph_graphtag.hpp>

#include <pgl_container/pgl_vector.hpp>

//Include STL
#include <functional>
#include <memory>
#include <bitset>
#include <iterator>
#include <algorithm>


PGL_NS_START(pgl)
PGL_NS_START(graph)



bool
pgl_vertex_t::isComplete() const noexcept
{
	return this->m_state.test(ce_isComplete);
};


bool
pgl_vertex_t::makeComplete(
	const GraphTag& Tag)
{
	/*
	 * TESTS MISSING
	 */
	bool success = false;
	switch(Tag)
	{
	  case GraphTag::Complete:
		success = this->int_make_complete();
	  	break;

	  default:
	  	throw PGL_EXCEPT_InvArg("Tag was not equal to complete.");
	  	break;
	}; //End switch(Tag)

	if(success == false || this->isComplete() == false)
	{
		throw PGL_EXCEPT_LOGIC("It was not possible to complete the vertex.");
	};

	return success;
}; //End



bool
pgl_vertex_t::makeUncomplete(
	const GraphTag& Tag)
{
	/*
	 * TESTS MISSING
	 */
	bool success = false;
	switch(Tag)
	{
	  case GraphTag::Uncomplete:
		success = this->int_make_uncomplete();
	  	break;

	  default:
	  	throw PGL_EXCEPT_InvArg("Tag was not equal to uncomplete.");
	  	break;
	}; //End switch(Tag)

	if(success == false || this->isComplete() == true)
	{
		throw PGL_EXCEPT_LOGIC("It was not possible to uncomplete the vertex.");
	};

	return success;
}; //End




bool
pgl_vertex_t::isOriginal() const
{
	pgl_assert(this->isValid());
	return m_state.test(ce_isOriginal);
};


bool
pgl_vertex_t::isRandom() const
{
	pgl_assert(this->isValid());
	return (m_state.test(ce_isOriginal)) ? false : true;
};

bool
pgl_vertex_t::isValid() const noexcept
{
	return (m_state.test(ce_isPropBuild)) && (this->m_id.isValid());
};


bool
pgl_vertex_t::checkIntegrity() const
{
	bool res = this->isValid();

	if(res == false)
	{
		throw PGL_EXCEPT_LOGIC("The Vertex is invalid.");
	};


#ifndef  PGL_GRAPH_REDUCED_INTEGRITY
	for(Edge_cref e : m_neigbourhood)
	{
		//This function does internaly call the checkInterity Function of the edge
		res &= this->int_testEdge(e);

		if(res == false)
		{
			return false;
		};

	};
#else
	//Check if there is only the reduced test
	for(Edge_cref e : m_neigbourhood)
	{
		res = res && e.isValid();
	};
#endif

#ifndef PGL_GRAPH_REDUCED_INTEGRITY
	//Check the incoming state
	for(auto incoming = this->inEBegin(); incoming != this->inEEnd(); ++incoming)
	{
		res = res && incoming->isIncoming();
	}; //End for(incoming)

	//check for outcoming
	for(auto outcgoing = this->outEBegin(); outcgoing != this->outEEnd(); ++outcgoing)
	{
		res = res && outcgoing->isLeaving();
	};
#endif

	return res;
};

bool
pgl_vertex_t::isFlagged() const noexcept
{
	pgl_assert(this->isValid());

	return m_state.test(ce_isFlagged);
};


void
pgl_vertex_t::setFalgTo(
	const bool& newFlagStat) noexcept
{
	pgl_assert(this->isValid());

	m_state.set(ce_isFlagged, newFlagStat);
	return;
};


bool
pgl_vertex_t::hasConnectionWith(
	const pgl_vertexID_t& ID) const
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	//We use 0 here as requested vertex, because, we only whant to know if it exists at least one
	return this->int_searchEdgeC(ID, 0).first;
};


Size_t
pgl_vertex_t::countConnectionWith(
	const pgl_vertexID_t& ID) const
{
	pgl_assert(this->isValid() );
	pgl_assert(ID.isValid());

	return std::count_if(m_neigbourhood.cbegin(), m_neigbourhood.cend(),
		[&ID](Edge_cref x) -> bool { return x.getOtherNodeID() == ID;} );
};


bool
pgl_vertex_t::pointsTo(
	const pgl_vertexID_t& ID) const
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	return std::any_of(m_neigbourhood.cbegin(), m_neigbourhood.cend(),
		[&ID](Edge_cref x) -> bool { return ( (x.isLeaving()) && (x.getOtherNodeID() == ID) ); } );
};


Size_t
pgl_vertex_t::countPointingTo(
	const pgl_vertexID_t& ID) const
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	return std::count_if(m_neigbourhood.cbegin(), m_neigbourhood.cend(),
		[&ID](Edge_cref x) -> bool { return ( (x.isLeaving()) && (x.getOtherNodeID() == ID) ); } );

};



bool
pgl_vertex_t::pointsBy(
	const pgl_vertexID_t& ID) const
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	return std::any_of(m_neigbourhood.cbegin(), m_neigbourhood.cend(),
		[&ID](Edge_cref x) -> bool { return ( (x.isIncoming()) && (x.getOtherNodeID() == ID) ); } );
};


Size_t
pgl_vertex_t::countPointBy(
	const pgl_vertexID_t& ID) const
{

	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	return std::count_if(m_neigbourhood.cbegin(), m_neigbourhood.cend(),
		[&ID](Edge_cref x) -> bool { return ( (x.isIncoming()) && (x.getOtherNodeID() == ID) ); } );
};


/*
 * \brief	This function calculates all four degrees of a vertex.
 *
 * Since it is potentialy costly to query all degree seperatly, this function offers a way to do it in one sweep.
 * It returns a tuple, with all the following degrees, in that order:
 * 	- orgInDegree		The number of incoming non random connections.
 * 	- orgOutDegree		The number of outgoing non random connections.
 * 	- randInDegree		The number of incoming random connections.
 * 	- randOutDegree 	The number of outgoiung random connections
 *
 * \note Undirected connections are counted twice, once as incoming and once as leaving connections.
 */
std::tuple<Size_t, Size_t, Size_t, Size_t>
pgl_vertex_t::orgRandInOutDegree() const noexcept
{
	//Define the return values
	Size_t orgInDegree 	= 0;
	Size_t orgOutDegree 	= 0;
	Size_t randInDegree 	= 0;
	Size_t randOutDegree 	= 0;

	for(const pgl_edge_t& e : m_neigbourhood)
	{
		//Test if random or not
		if(e.isRandom() == true)
		{
			if(e.isIncoming() == true)
			{
				randInDegree += 1;
			}; //End incoming

			if(e.isLeaving() == true)
			{
				randOutDegree += 1;
			}; //End leaving
		}
		else
		{
			pgl_assert(e.isOriginal() == true);

			if(e.isIncoming() == true)
			{
				orgInDegree += 1;
			}; //End incoming

			if(e.isLeaving() == true)
			{
				orgOutDegree += 1;
			};
		};
	}; //End for(e): finding the degrees



	//Retrun it
	return std::make_tuple(orgInDegree, orgOutDegree, randInDegree, randOutDegree);
};




Size_t
pgl_vertex_t::degree() const noexcept
{
	pgl_assert(this->isValid());

	return m_neigbourhood.size();
};


Size_t
pgl_vertex_t::edgeCapacity() const noexcept
{
	return m_neigbourhood.capacity();
};


Size_t
pgl_vertex_t::inDegree() const noexcept
{
	pgl_assert(this->isValid());

	return std::count_if(
		m_neigbourhood.cbegin(),
		m_neigbourhood.cend(),
		[](Edge_cref x) -> bool { return x.isIncoming();});
};


Size_t
pgl_vertex_t::randDegree() const noexcept
{
	return std::count_if(m_neigbourhood.cbegin(), m_neigbourhood.cend(),
		[](Edge_cref e) -> bool { return e.isRandom(); } );
};


Size_t
pgl_vertex_t::orgDegree() const noexcept
{
	return std::count_if(m_neigbourhood.cbegin(), m_neigbourhood.cend(),
		[](Edge_cref e) -> bool { return e.isOriginal(); } );
};


Size_t
pgl_vertex_t::outDegree() const noexcept
{
	pgl_assert(this->isValid());

	return std::count_if(
		m_neigbourhood.cbegin(),
		m_neigbourhood.cend(),
		[](Edge_cref x) -> bool { return x.isLeaving(); });
};


bool
pgl_vertex_t::isLeafe() const noexcept
{
	pgl_assert(isValid());

	Int_t leavingCount = 0;
	for(const pgl_edge_t& e : m_neigbourhood)
	{
		leavingCount += e.isLeaving() ? 1 : 0;
		if(leavingCount >= 2)
		{
			return false;
		};
	}; //End for(e)

	return leavingCount == 1 ? true : false;
};


bool
pgl_vertex_t::isRealLeafe() const noexcept
{
	pgl_assert(this->isValid());

	Int_t leavingCount = 0;
	for(const pgl_edge_t& e : m_neigbourhood)
	{
		if(e.isRandom())
		{
			continue;
		};

		leavingCount += e.isLeaving() ? 1 : 0;
		if(leavingCount >= 2)
		{
			return false;
		};
	}; //End for(e)

	return leavingCount == 1 ? true : false;
};

Size_t
pgl_vertex_t::count_if(
	std::function<bool(const pgl_edge_t&)> Pred) const
{
	pgl_assert(this->isValid());

	Size_t c = 0;
	for(Edge_cref e : m_neigbourhood)
	{
		if(Pred(e) == true)
		{
			c += 1;
		};
	}; //End for(e)

	return c;
};


bool
pgl_vertex_t::test_edge(
	std::reference_wrapper<bool(Edge_cref)> 	Pred,
	const pgl_vertexID_t&				ID,
	const Size_t&					n) const
{
	pgl_assert(this->isValid());

	const auto e = this->int_searchEdgeC(ID, n);

	if(e.first == false)
	{
		throw PGL_EXCEPT_OutOfBound("The Requested ID is not part of the neighbourhood of *this.");
	};

	return Pred(*(e.second));
};


bool
pgl_vertex_t::any_of(
	std::function<bool(Edge_cref)> Pred) const
{
	pgl_assert(this->isValid());

	return std::any_of(m_neigbourhood.cbegin(), m_neigbourhood.cend(), Pred);
};


void
pgl_vertex_t::apply_to_all(
	std::function<void(Edge_ref)>	Pred)
{
	pgl_assert(this->isValid());

	for(Edge_ref e : m_neigbourhood)
	{
		Pred(e);
	};

	return;
};


void
pgl_vertex_t::check_all(
	std::reference_wrapper<void(Edge_cref)>  Pred) const
{
	pgl_assert(this->isValid());

	for(Edge_cref e : m_neigbourhood)
	{
		Pred(e);
	};

	return;
};

pgl_vertex_t::EIterator_t
pgl_vertex_t::find_first_of(
	std::function<bool(Edge_cref)> Pred)
{
	pgl_assert(this->isValid() );

	return std::find_if(this->EBegin(), this->EEnd(), Pred);
};




pgl_vertex_t::Vertex_ref
pgl_vertex_t::getNode(
	const pgl_vertexID_t& 	ID,
	const Size_t&		n)
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	auto e = this->int_searchEdge(ID, n);

	if(e.first == false)
	{
		throw PGL_EXCEPT_OutOfBound("Tried to acces a non incident node.");
	};

	return e.second->getOtherNode();
};

pgl_vertex_t::Vertex_cref
pgl_vertex_t::getNode(
	const pgl_vertexID_t& 	ID,
	const Size_t& 		n) const
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	auto e = this->int_searchEdge(ID, n);

	if(e.first == false)
	{
		throw PGL_EXCEPT_OutOfBound("Tried to acces a non incident node.");
	};

	return e.second->getOtherNode();
}

pgl_vertex_t::Vertex_cref
pgl_vertex_t::getCNode(
	const pgl_vertexID_t& 	ID,
	const Size_t& 		n) const
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	auto e = this->int_searchEdge(ID, n);

	if(e.first == false)
	{
		throw PGL_EXCEPT_OutOfBound("Tried to acces a non incident node.");
	};

	return e.second->getOtherNode();
};

pgl_vertex_t::Edge_ref
pgl_vertex_t::getEdge(
	const pgl_vertexID_t& 	ID,
	const Size_t& 		n)
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	auto e = this->int_searchEdge(ID, n);

	if(e.first == false)
	{
		throw PGL_EXCEPT_OutOfBound("Tried to acces a non incident edge.");
	};

	return *(e.second);
};

pgl_vertex_t::Edge_cref
pgl_vertex_t::getEdge(
	const pgl_vertexID_t& 	ID,
	const Size_t&		n) const
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	auto e = this->int_searchEdge(ID, n);

	if(e.first == false)
	{
		throw PGL_EXCEPT_OutOfBound("Tried to acces a non incident edge.");
	};

	return *(e.second);
};

pgl_vertex_t::Edge_cref
pgl_vertex_t::getCEdge(
	const pgl_vertexID_t& 	ID,
	const Size_t& 		n) const
{
	pgl_assert(this->isValid());
	pgl_assert(ID.isValid());

	auto e = this->int_searchEdge(ID, n);

	if(e.first == false)
	{
		throw PGL_EXCEPT_OutOfBound("Tried to acces a non incident edge.");
	};

	return *(e.second);
};







PGL_NS_END(graph)
PGL_NS_END(pgl)


