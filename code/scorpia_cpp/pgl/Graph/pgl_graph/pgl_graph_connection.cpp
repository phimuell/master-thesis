/*
 * This files contains the implementations of the function that belongs to the connection.
 */


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graphtag.hpp>

#include <pgl_graph/pgl_graph_vertex_saf.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>

#include <pgl_graph/pgl_graph_connection.hpp>
#include <pgl_graph/pgl_graph_connection_base.hpp>


//Include STL
#include <utility>

PGL_NS_START(pgl)
PGL_NS_START(graph)


PGL_NS_START(internal)

/*
 * This function is only meant for testing, it does some checks that will be done again,
 * but to be sure we must do it that way.
 * The function only does basic sanity checks.
 */
static
bool
pgl_connection_checkIfPropperBuild_internal(
	pgl_vertex_t* 		srcNode,
	pgl_vertex_t* 		destNode,
	const GraphTag&  	kindOfConnection)
{
	//Check if nullpointer
	if(srcNode == nullptr)
	{
		throw PGL_EXCEPT_InvArg("Pointer to \"srcNode\" is NULL");
		return false;
	};

	if(destNode == nullptr)
	{
		throw PGL_EXCEPT_InvArg("Pointer to \"destNode\" is NULL");
		return false;
	};

	//check if loop one is violated
	//notice the !
	if(!((pgl_vertexSAF_getID(srcNode) == pgl_vertexSAF_getID(destNode)) ?
		GraphTag_isLoopOne(kindOfConnection) :	//If both are the same ID, loop one must be given
		(GraphTag_isEdge(kindOfConnection) || GraphTag_isConnection(kindOfConnection))	//If not the same there must be a connection or edge type
	)) //End of if
	{
		throw PGL_EXCEPT_InvArg("Violation of the LoopOne constriant");
		return false;
	};



	return true;
};

PGL_NS_END(internal)

/// Default constructor
pgl_connection_t::pgl_connection_t() = default;

/// Mopve Constructor (defaulted)
pgl_connection_t::pgl_connection_t(
	pgl_connection_t&&) noexcept = default;

/// Move assigne (defaulted)
pgl_connection_t&
pgl_connection_t::operator= (
	pgl_connection_t&&) noexcept = default;


/// Destructor is also the default one
pgl_connection_t::~pgl_connection_t() = default;


/*
 * \brief 	Valid builder constructor
 *
 * \param srcNode 	Pointer to the node, which is the beginning (in case of directed connection)
 * \param destNode 	Pointer to the node, which is the end of the connection.
 * \param kindOfConnection 	The connection of the connection that sould be created
 * \param PayLoad	Payload that sould be used.
 *
 * The pointers to the two nodes shall not be null, the must be valid and the object they point to too.
 * They if the connection is a LoopOne connection, then this has to be specified by the kindOfConnection parameter.
 * If it is not an LoopOne and src == dest an error is produced.
 *
 * The payload can be the nullpointer.
 *
 */
pgl_connection_t::pgl_connection_t(
	pgl_vertex_t* 		srcNode,
	pgl_vertex_t* 		destNode,
	const GraphTag&  	kindOfConnection,
	Payload_ptr 		PayLoad) :
  pgl_connection_base_t(internal::pgl_connection_checkIfPropperBuild_internal(srcNode, destNode, kindOfConnection)), 	//This indicate that this is a proper build, this is not the case since we test this a bit later.
  m_vertexSrc(nullptr),
  m_vertexDest(nullptr),
  m_id()
#ifndef PGL_GRAPH_DISABLE_PAYLOADS
  , m_payload(std::move(PayLoad))
#endif
{
	if(this->isProperBuild() == false)
	{
		//Error something is wrong
		throw PGL_EXCEPT_InvArg("ConnectionBase was not build properly.");
	};

	/*
	 * If the payload is disabled, we delete the pointer
	 *
	 * Remember it is save to delete the nullpointer
	 */
#ifdef PGL_GRAPH_DISABLE_PAYLOADS
	delete PayLoad;
#endif


	/*
	 * Now we create the ID
	 */
	m_id = pgl_edgeID_t(
			pgl_vertexSAF_getID(srcNode),
			pgl_vertexSAF_getID(destNode),
			kindOfConnection
			);

	//Save the pointers
	m_vertexSrc = srcNode;
	m_vertexDest = destNode;


	pgl_assert(m_id.isLegal());
	pgl_assert(m_id.isValid());

	(void)m_id.getState();
	(void)m_id.getSrcID();
	(void)m_id.getDestID();

	/*
	 * Set directed state
	 */
	if(GraphTag_isConnection(kindOfConnection) == false)
	{
		//Error not a connection
		throw PGL_EXCEPT_InvArg("kindOfConnection is not of connection type.");
	}
	else
	{
		//Now we doe the directed stuff
		if(GraphTag_isDirectedEdge(kindOfConnection) == true)
		{
			//It is a dirrected connection
			this->Super_t::prot_setConnectionState(Super_t::ConnectionState::Directed);
			pgl_assert(m_id.isDirected() == true);
		}
		else if(GraphTag_isUndirectedEdge(kindOfConnection) == true)
		{
			//It is a undirected
			this->Super_t::prot_setConnectionState(Super_t::ConnectionState::Undirected);
			pgl_assert(m_id.isUndirected() == true);

			if(m_id.getSrcID() == pgl_vertexSAF_getID(srcNode) )
			{
				m_vertexSrc = srcNode;
				m_vertexDest = destNode;
			}
			else if(m_id.getSrcID() == pgl_vertexSAF_getID(destNode)) //THIS IS CORRECT; becaue we must sync the id with the ID of the connection
			{
				m_vertexSrc = destNode;
				m_vertexDest = srcNode;
			}
			else
			{
				pgl_assert(false);
			};

		}
		else
		{
			//An error nothing was given
			throw PGL_EXCEPT_InvArg("kindOfConnection did not contain a direction attribute.");
		};

		pgl_assert(m_id.isValid());
	};



	/*
	 * Set the original state
	 */
	if(GraphTag_isRandom(kindOfConnection) == true)
	{
		this->Super_t::prot_setOriginalState(Super_t::ConnectionState::RandomConn);
	}
	else
	{
		this->Super_t::prot_setOriginalState(Super_t::ConnectionState::OriginalConn);
	};



	/*
	 * A newly created connection is allways uncomplete
	 */
	this->Super_t::prot_setCompleteState(Super_t::ConnectionState::NotComplete);


	/*
	 * Set the loop one state
	 */
	if(GraphTag_isLoopOne(kindOfConnection) == true)
	{
		this->Super_t::prot_setLoopOne(Super_t::ConnectionState::isLoopOne);
		pgl_assert(m_id.isLoopOne() == true);
	}
	else
	{
		pgl_assert(m_id.isLoopOne() == false);
	};

	const auto tt = m_id.getSrcID();
	const auto rr = m_id.getSrcID();
	this->isValid();
	const auto ttt = m_id.getSrcID();

	pgl_assert(this->isValid() );

	pgl_assert(this->checkIntegrity() );

}; //End constructor



/*
 * This function makes some great sanity check
 */
bool
pgl_connection_t::checkIntegrity() const
{
	bool result = this->isValid();
	if(result == false)
	{
		throw PGL_EXCEPT_LOGIC("The connection is not even valid.");
	};

	//Test if the id is correct
	result = result && (pgl_vertexSAF_getID(m_vertexSrc) == m_id.getSrcID());
	result = result && (pgl_vertexSAF_getID(m_vertexDest) == m_id.getDestID());

	pgl_assert(result && "IDWrong");

	result = result && m_id.isLegal() && m_id.isValid();
	pgl_assert(result);

	result = result && m_id.isLoopOne() == this->isLoopOne();
	pgl_assert(result);

	if(this->isDirected() == true)
	{
		result = result && m_id.isDirected() == true;
		pgl_assert(result);

	}
	else
	{
		result = result && m_id.isUndirected() == true;
		pgl_assert(result);
	};

	//Check if the two vertecies are valid
	result = result && this->getSrcNode().isValid();
	result = result && this->getDestNode().isValid();
	pgl_assert(result);

	return result;
};



void
pgl_connection_t::makeComplete(
	const GraphTag& Tag) noexcept(false)
{
	if(this->isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to complete an invalid connection.");
	};

#ifndef PGL_GRAPH_REDUCED_INTEGRITY
	if(this->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to complete an not integer (but valid) connection.");
	};
#endif

	if(this->isComplete() == true)
	{
		return;
	};


	if(Tag == GraphTag::Complete)
	{
		if(m_id.isInvalid() || (!m_id.isLegal()))
		{
			throw PGL_EXCEPT_RUNTIME("Tried to complete a connection with invalid ID.");
		};

		//Make complete
		this->Super_t::prot_setCompleteState(Super_t::ConnectionState::Complete);
		return;
	}
	else
	{
		throw PGL_EXCEPT_InvArg("Called the makeComplete function of connection with an argument not equal to GrapgTag::Complete");
	};
};//End make Complete


void
pgl_connection_t::makeUncomplete(
	const GraphTag& Tag)
{
	if(this->isValid() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to uncomplete an invalid connection. How dioes it reach that state");
	};

	if(this->checkIntegrity() == false)
	{
		throw PGL_EXCEPT_LOGIC("Tried to uncomplete an not integer (but valid) connection. How does it reach that state.");
	};

	if(this->isComplete() == false)
	{
		return;
	};
	if(Tag == GraphTag::Uncomplete)
	{
		//Make uncomplete
		this->Super_t::prot_setCompleteState(Super_t::ConnectionState::NotComplete);
		return;
	}
	else
	{
		throw PGL_EXCEPT_InvArg("Called the makeUncomplete function of connection with an argument not equal to GrapgTag::Uncomplete");
	};
};//End makeUncomplete






PGL_NS_END(graph)
PGL_NS_END(pgl)




