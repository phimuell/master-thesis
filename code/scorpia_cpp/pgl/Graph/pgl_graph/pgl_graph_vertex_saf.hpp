#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_VERTEX_SAF_HPP
#define PGL_GRAPH__PGL_GRAPH_VERTEX_SAF_HPP
/**
 * \file pgl_graph/pgl_graph_vertex_saf.hpp
 * \brief Define some stand alone functions for the vertex
 *
 * SAF mans Stand-Alone-Funtions.
 * This files uses a trick that is similar to the pimpl-Idiom.
 * It exploits the fact that all pointers are the same, and the compiler only want to
 * see the declaration of a dfunction when using it.
 * [NOTE: In the GOD (good old days) this was not necessary and one could use
 * any funtion.]
 * They take as an argument a pointer to a vertex and returns the return value.
 *
 * This function can not be inlined so use them with care
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>

#include <pgl_graph/pgl_graph_vertexID.hpp>



PGL_NS_START(pgl)
PGL_NS_START(graph)


/**
 * \brief 	Retruns the ID of the vertex
 * \param This	A pointer to the vertex
 */
pgl_vertexID_t
pgl_vertexSAF_getID(
	const pgl_vertex_t* This);


/**
 * \brief 	Performs the valid test of the vertex
 * \param This 	A pointr to the vertex
 */
bool
pgl_vertexSAF_isValid(
	const pgl_vertex_t* 	This);




PGL_NS_END(graph)
PGL_NS_END(pgl)




#endif //end include guards
