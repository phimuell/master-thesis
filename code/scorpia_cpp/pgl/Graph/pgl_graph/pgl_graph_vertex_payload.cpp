/**
 * \file pgl_graph/pgl_graph_vertex_payload.cpp
 * \brief 	Provides default implementations for the vertex payload
 *
 */

//PGL-Stuff
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex_saf.hpp>

#include <pgl_graph/pgl_graph_edgeID.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_vertex_payload.hpp>


//STD-Stuff
#include <memory>


PGL_NS_START(pgl)
PGL_NS_START(graph)

#define PGL_THROW_NODEPAY_NOT_IMPLENTED_YET throw PGL_EXCEPT_illMethod("Tried to call a not implemented method.")

pgl_vertex_payload_t::pgl_vertex_payload_t() = default;

pgl_vertex_payload_t::pgl_vertex_payload_t(
	const pgl_vertex_payload_t&) = default;

pgl_vertex_payload_t::pgl_vertex_payload_t(
	pgl_vertex_payload_t&&) = default;

pgl_vertex_payload_t&
pgl_vertex_payload_t::operator= (
	const pgl_vertex_payload_t&) = default;

pgl_vertex_payload_t&
pgl_vertex_payload_t::operator= (
	pgl_vertex_payload_t&&) = default;

pgl_vertex_payload_t::~pgl_vertex_payload_t() = default;


/*
 * ===============================
 * BEGIN Distance
 */
bool
pgl_vertex_payload_t::_implements_DISTANCE() const
{
	return false;
};


Distance_t
pgl_vertex_payload_t::getCurrDistance() const
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;

	return Constants::DistanceInfinity;
};



void
pgl_vertex_payload_t::setCurrentDistanceTo(
    const Distance_t& nd)
{
	(void)nd;
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;

	return;
};


bool
pgl_vertex_payload_t::_implements_VISITING() const noexcept
{
	return false;
};



bool
pgl_vertex_payload_t::isVisited() const
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;

	return false;
};


void
pgl_vertex_payload_t::makeVisited()
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;

	return;
};


void
pgl_vertex_payload_t::makeUnvisited()
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return;
};

/*
 * END DISTANCE
 * =========================
 */


/*
 * ==============================================
 * BEGIN ORDERING
 */

bool
pgl_vertex_payload_t::_implements_ORDERING() const noexcept
{
	return false;
};


void
pgl_vertex_payload_t::clearPredecessor()
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return;
};


void
pgl_vertex_payload_t::setPredecessorTo(
	pgl_vertexID_t 		newP)
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	(void)newP;

	return;
};


pgl_vertexID_t
pgl_vertex_payload_t::getPredecessor() const
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return pgl_vertexID_t::INVALID();
};

bool
pgl_vertex_payload_t::hasPredecessor() const
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return false;
};

/*
 * END ORDERING
 * =================================
 */





/*
 * ================================
 * BEGINS Kleinberg Model Core
 *
 * To fill the distance to all other nodes, the constructor must be used
 */


/*
 * \brief KleinbergCore:	This function returns the distance to the current target node.
 * 				This distance is also called the routing distance.
 */
Distance_t
pgl_vertex_payload_t::getRoutingDistance() const
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return Constants::DistanceInfinity;
};



/*
 * \brief KleinbergCore:	This function returns the ID of the current target node
 */
pgl_vertexID_t
pgl_vertex_payload_t::getCurrentTargetNodeID() const
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return pgl_vertexID_t::INVALID();
};


bool
pgl_vertex_payload_t::hasValidTargetNode() const
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return false;
};


/*
 * \brief KleinbergCore 	This function cheges the state of *this such that now the new Target is considered as target node
 *
 * \param newTargetID		The ID of the node that will be the new target node
 */
void
pgl_vertex_payload_t::setCurrentTargetNodeID(
		const pgl_vertexID_t& newTargetID)
{
	(void)newTargetID;
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return;
};

/*
 * \brief KleinbergCore		This fuinction will set the current ruting distance to the target.
 * 					It is the reponsibility of the user, to do it correctly.
 */
void
pgl_vertex_payload_t::setCurrentRoutingDistanceTo(
	const Distance_t 	newDistance)
{
	(void) newDistance;
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return;
};

/*
 * \brief KleinbergCore		This function returns true if the node was vidited in the past.
 * 					A change to now target node shall unset this value.
 */
bool
pgl_vertex_payload_t::wasVisitedByKleinberg() const
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return false;
};


void
pgl_vertex_payload_t::makeVisitedByKleinberg()
{
	PGL_THROW_NODEPAY_NOT_IMPLENTED_YET;
	return;
};



/*
 * ENDS Kleinberg Model Core
 * =================================
 */









PGL_NS_END(graph)
PGL_NS_END(pgl)


