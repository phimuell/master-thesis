#pragma once
/**
 * \brief	This file implements a topology info class for the graph.
 *
 * This class can be used to generate a summary about the graph.
 * Like the degree distribution or so.
 * The class is vertex oriented,
 * this means that information about the edges, are not stored ecplicitly.
 * At most implicity.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_int.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph/pgl_graph_vertex.hpp>

#include <pgl_graph_utility/pgl_graph_vertex_map.hpp>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)



PGL_NS_BEGIN(internal)
/**
 * \brief	This class is used to represent the datas about one vertex
 * \class 	pgl_nodeTopoInfo_t
 *
 * This is a class that represents the information about a node.
 * Notice that the "owner" of this information is not stored inside the class.
 * The reason for that is, that it is excpected, that this class is always inside a pgl_vertexMap_t, where the "owner" is stored.
 * This is also a helper, that should not be used by the user directly.
 */
class pgl_nodeTopoInfo_t
{
	/*
	 * =====================
	 * Typedef
	 */
public:
	using 	Degree_t = 	::pgl::Size_t;		//This is the type to store a degree


	/*
	 * ==========================
	 * Constructors
	 *
	 * All standard constructors are defaulted
	 */
public:

	/**
	 * \brief	This constructor is initialized by the node
	 *
	 * This is the official way to create such an object
	 *
	 * \param 	node 	The node we want to read from
	 */
	pgl_nodeTopoInfo_t(
		const pgl_vertex_t& 	node) noexcept
	{
		//Read the full degree
		m_randDegree = node.randDegree();
		m_orgDegree  = node.orgDegree();

		// Now the partial degrees
		std::tie(m_orgInDegree, m_orgOutDegree, m_randInDegree, m_randOutDegree) = node.orgRandInOutDegree();

		//Now if random
		if(node.isRandom() == true)
		{
			m_isRandNode = true;
		}
		else
		{
			m_isRandNode = false;
		};
	}; //End main constructor


	/**
	 * \brief	Defaulted default constructor
	 */
	pgl_nodeTopoInfo_t() noexcept = default;


	/**
	 * \brief	Defaulted copy constructor
	 */
	pgl_nodeTopoInfo_t(
		const pgl_nodeTopoInfo_t&) noexcept = default;


	/**
	 * \brief 	Defaulted move assignment
	 */
	pgl_nodeTopoInfo_t(
		pgl_nodeTopoInfo_t&&) noexcept = default;


	/**
	 * \brief	Defaulted copy assignement
	 */
	pgl_nodeTopoInfo_t&
	operator= (
		const pgl_nodeTopoInfo_t&) noexcept = default;

	/**
	 * \brief	Defaulted move assignment
	 */
	pgl_nodeTopoInfo_t&
	operator= (
		pgl_nodeTopoInfo_t&&) noexcept = default;

	/**
	 * \brief	Defaulted destructor
	 */
	~pgl_nodeTopoInfo_t() noexcept = default;


	/*
	 * ==================================
	 * Getter this are the getter functions
	 */
public:
	/**
	 * \brief	Returns the original degree of the node
	 */
	inline
	Degree_t
	orgDegree() const noexcept
	{
		return m_orgDegree;
	};


	/**
	 * \brief	Retruns the random degree
	 */
	inline
	Degree_t
	randDegree() const noexcept
	{
		return m_randDegree;
	};


	/**
	 * \brief	Returns the total degree.
	 *
	 * This ignors direction, and randomness
	 */
	inline
	Degree_t
	degree() const noexcept
	{
		return Degree_t(this->orgDegree() + this->randDegree());
	};


	/**
	 * \breif	Returns the random incoming degree
	 */
	inline
	Degree_t
	randInDegree() const noexcept
	{
		return m_randInDegree;
	};


	/**
	 * \brief	Returns the radom outgoing degree
	 */
	inline
	Degree_t
	randOutDegree() const noexcept
	{
		return m_randOutDegree;
	};


	/**
	 * \brief	Returns the in degree of not random
	 */
	inline
	Degree_t
	orgInDegree() const noexcept
	{
		return m_orgInDegree;
	};


	/**
	 * \brief	Return the not random out degree
	 */
	inline
	Degree_t
	orgOutDegree() const noexcept
	{
		return m_orgOutDegree;
	};


	/**
	 * \brief	Returns the total in degree
	 *
	 * The total indegree is composed of random and not random
	 */
	inline
	Degree_t
	inDegree() const noexcept
	{
		return Degree_t(this->orgInDegree() + this->randInDegree());
	};


	/**
	 * \brief	Returns the total out degree
	 *
	 * The total out degree is composed of random and origianl connections
	 */
	inline
	Degree_t
	outDegree() const noexcept
	{
		return Degree_t(this->orgOutDegree() + this->randOutDegree());
	};


	/**
	 * \brief	Returns true if it is this is a original connection
	 */
	inline
	bool
	isOriginal() const noexcept
	{
		return m_isRandNode == false ? true : false;
	};


	/*
	 * ==============================
	 * Private memebr variable
	 */
private:
	Degree_t 		m_randDegree;	//!< The total random degree of the node
	Degree_t 		m_orgDegree;	//!< The total not normal degree of the node
	Degree_t 		m_randInDegree;	//!< The random incoming degree of the node
	Degree_t 		m_randOutDegree;//!< The random outDegree of the node
	Degree_t 		m_orgInDegree;	//!< The original (not random) in degree of the node
	Degree_t 		m_orgOutDegree;	//!< The original (not random) out degree of the node
	bool 			m_isRandNode;	//!< Indicate if the node is random or not
}; //End class(pgl_nodeTopoInfo_t)


PGL_NS_END(internal)




/**
 * \brief	This class is the graph topo class. It stores information about the topology on the graph
 * \class 	pgl_graphTopo_t
 *
 * This class is node oriented, so we primary store information about the node
 * It act mainly of a container of the pgl_nodeTopoIndo_t class.
 * With some additional informations
 */
class pgl_graphTopo_t
{
	/*
	 * ===================
	 * Typedefs
	 */
public:
	using NodeInfo_t 	= internal::pgl_nodeTopoInfo_t;		//!< This is the class that stores the information about a node
	using Degree_t 		= NodeInfo_t::Degree_t;			//!< This is the type for representing the degree
	using NodeContainer_t 	= pgl_vertexMap_t<NodeInfo_t>;		//!< This is the class that stores the all the node informations
	using iterator 		= NodeContainer_t::const_iterator;	//!< This iss the iterator, we only allow constant iterators
	using const_iterator 	= NodeContainer_t::const_iterator;
	using reference 	= const internal::pgl_nodeTopoInfo_t&;
	using const_reference 	= const internal::pgl_nodeTopoInfo_t&;
	using VertexID_t 	= ::pgl::graph::pgl_vertexID_t;


	/*
	 * ============================
	 * Constructors
	 *
	 * The 5 standard constructors are defaulted.
	 * There is one constructor that can be used to build a usefull instance.
	 *
	 * \param 	Graph	This is the graph we load the infos from.
	 */
public:
	pgl_graphTopo_t(
		const pgl_graph_t& 	Graph) :
	  m_nodes(Graph.nVertices()),
	  m_orgEdges(Graph.nOrgConnections()),
	  m_randEdges(Graph.nRandConnections()),
	  m_nodeTopo(Graph, Graph.CNodeBegin(), Graph.CNodeEnd())
	{
	}; //End usefull constructor


	/**
	 * \brief	Defaulted default constructor
	 */
	pgl_graphTopo_t() noexcept = default;


	/**
	 * \brief	Defaulted compy constructor
	 */
	pgl_graphTopo_t(
		const pgl_graphTopo_t&) noexcept(false) = default;


	/**
	 * \brief	Defaulted move constructor
	 */
	pgl_graphTopo_t(
		pgl_graphTopo_t&&) noexcept = default;


	/**
	 * \brief	Defaulted copy assigmnet
	 */
	pgl_graphTopo_t&
	operator= (
		const pgl_graphTopo_t&) noexcept(false) = default;


	/**
	 * \brief	Defaulted mode assigment
	 */
	pgl_graphTopo_t&
	operator= (
		pgl_graphTopo_t&&) = default;


	/**
	 * \brief	Defaulted destructor
	 */
	~pgl_graphTopo_t() = default;


	/*
	 * =========================
	 * Iterator
	 *
	 * We only provide constant iterators.
	 */
public:

	inline
	const_iterator
	begin() const noexcept
	{
		return m_nodeTopo.cbegin();
	};

	inline
	const_iterator
	cbegin() const noexcept
	{
		return m_nodeTopo.cbegin();
	};

	inline
	const_iterator
	end() const noexcept
	{
		return m_nodeTopo.cend();
	};

	inline
	const_iterator
	cend() const noexcept
	{
		return m_nodeTopo.cend();
	};



	/*
	 * =================================
	 * Queries
	 *
	 * This fucntions allow to read specific parts of the topology
	 */
public:

	/**
	 * \brief	Get a reference to the vertex with ID id
	 */
	inline
	const_reference
	getTopoOf(
		const VertexID_t& 	id) const
	{
		return m_nodeTopo.at(id);
	};


	/**
	 * \brief	Get a reference to the element that matched numrical ID i
	 */
	inline
	const_reference
	getTopoOf(
		const Size_t& 		i) const
	{
		return m_nodeTopo.at(i);
	};


	/**
	 * \brief	Returns the size of this
	 */
	inline
	Size_t
	size() const noexcept
	{
		return m_nodeTopo.size();
	};


	/**
	 * \brief	Returns the numbers of stored nodes
	 */
	inline
	Size_t
	nVertices() const noexcept
	{
		return m_nodes;
	};


	/**
	 * \brief	Returns the numbers of random connections
	 */
	inline
	Size_t
	nRandConnections() const noexcept
	{
		return m_randEdges;
	};


	/**
	 * \brief	Returns the number of original (not random) connections
	 */
	inline
	Size_t
	nOrgConnections() const noexcept
	{
		return m_orgEdges;
	};


	/**
	 * \brief	The total number of connections is returned
	 */
	inline
	Size_t
	nConnections() const noexcept
	{
		return Size_t(this->nOrgConnections() + this->nRandConnections());
	};


	/**
	 * \brief	This returns the random incoming degree of the nodes.
	 *
	 * For that the function iterates through the internal representation and sumes up
	 */
	inline
	Degree_t
	randInDegree() const
	{
		Degree_t inRandDeg_sum = 0;
		for(const NodeInfo_t& i : m_nodeTopo)
		{
			inRandDeg_sum += i.randInDegree();
		}; //End for(i)

		return inRandDeg_sum;
	};


	/**
	 * \brief	Returns the out random degree of the outgoing nodes
	 *
	 * For that the function iterates through the internal sttructure
	 */
	inline
	Degree_t
	randOutDegree() const
	{
		Degree_t outRandDeg_sum = 0;
		for(const NodeInfo_t& i : m_nodeTopo)
		{
			outRandDeg_sum += i.randOutDegree();
		};

		return outRandDeg_sum;
	};


	/**
	 * \brief	This returns the degree of the incoming original degree
	 */
	inline
	Degree_t
	orgInDegree() const
	{
		Degree_t inOrgDeg_sum = 0;
		for(const NodeInfo_t& i : m_nodeTopo)
		{
			inOrgDeg_sum += i.orgInDegree();
		};

		return inOrgDeg_sum;
	};


	/**
	 * \brief 	returns the degree of the outcomming random
	 */
	inline
	Degree_t
	orgOutDegree() const
	{
		Degree_t outOrgDeg_sum = 0;
		for(const NodeInfo_t& i : m_nodeTopo)
		{
			outOrgDeg_sum += i.orgOutDegree();
		};

		return outOrgDeg_sum;
	};


	/**
	 * \brief	Returns true if this is empty (not associated with a graph)
	 */
	inline
	bool
	isEmpty() const noexcept
	{
		return (m_nodeTopo.size() == 0) ? true : false;
	};


	/**
	 * \brief	Returns true, if this is associated with a graph
	 */
	inline
	bool
	isAssociated() const noexcept
	{
		return (m_nodeTopo.size() != 0) ? true : false;
	};




	/*
	 * ======================================
	 * Private Member
	 */
private:
	Degree_t 		m_nodes;		//!< Number of nodes in the graph
	Degree_t 		m_orgEdges;		//!< Number of the original connection in the graph
	Degree_t 		m_randEdges;		//!< Number of the random connections in the graph
	NodeContainer_t 	m_nodeTopo;		//!< The topological information about the nodes
}; //End class(pgl_graphTopo_t)
















PGL_NS_END(graph)
PGL_NS_END(pgl)






