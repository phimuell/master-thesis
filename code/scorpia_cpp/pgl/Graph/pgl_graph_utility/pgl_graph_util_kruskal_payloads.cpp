/**
 * \file	Graph/pgl_graph_utility/pgl_graph_util_kruskal_payload.cpp
 * \brief	This file implements the payload that is needed for the kruskal MST algo
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_connection_payload.hpp>
#include <pgl_graph_utility/pgl_graph_util_kruskal_payloads.hpp>



//Include STD


PGL_NS_START(pgl)
PGL_NS_START(graph)



pgl_kruskal_connPL_t::~pgl_kruskal_connPL_t() = default;

pgl_kruskal_connPL_t::pgl_kruskal_connPL_t() : pgl_connection_payload_t() {};
pgl_kruskal_connPL_t::pgl_kruskal_connPL_t(const pgl_kruskal_connPL_t&) = default;
pgl_kruskal_connPL_t::pgl_kruskal_connPL_t(pgl_kruskal_connPL_t&&) = default;

pgl_kruskal_connPL_t&
pgl_kruskal_connPL_t::operator= (pgl_kruskal_connPL_t&&) = default;

pgl_kruskal_connPL_t&
pgl_kruskal_connPL_t::operator= (const pgl_kruskal_connPL_t&) = default;


pgl_kruskal_connPL_t::pgl_kruskal_connPL_t(
	const Weight_t&	weitht) :
    pgl_connection_payload_t::pgl_connection_payload_t()
  , m_connWeight(weitht)
  , m_isMST(false)
{
	pgl_assert(m_connWeight >= 0);
};


/**
 * \brief 	Retruns true if *this is inside the MST
 */
bool
pgl_kruskal_connPL_t::isMatched() const
{
	return m_isMST;
};


/**
 * \brief 	Make *this to a part in the MST
 *
 * The returnvalue has no meaning
 */
bool
pgl_kruskal_connPL_t::makeMatched()
{
	m_isMST = true;
	return false;
};

bool
pgl_kruskal_connPL_t::makeUnmatched()
{
	m_isMST = false;
	return false;
};



/**
 * \brief	Returns the weight of *this
 */
Weight_t
pgl_kruskal_connPL_t::getWeight() const
{
	return m_connWeight;
};





PGL_NS_END(graph)
PGL_NS_END(pgl)


