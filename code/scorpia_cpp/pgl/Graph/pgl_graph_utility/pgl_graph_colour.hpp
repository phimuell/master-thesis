#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_COULOUR_HPP
#define PGL_GRAPH__PGL_GRAPH_COULOUR_HPP

/**
 * \file 	pgl_graph_utility/pgl_graph_colour.hpp
 * \brief 	This class represents a colour for the graph.
 *
 * This class implements the functionalities of a colour.
 * It can be used to represent a general colour, but it is defined in the context of a graph.
 * It uses RGB to represent the colour. it occupies 32bits of memeory.
 * The 8 remaining bits are reserved for further extension.
 *
 * It is possible to convert the colour into an int this enables switchuing over it.
 *
 * A colour can also be invalid.
 * A default constructed colour object is always withe.
 *
 * The colours are based on the following definition:
 * https://de.wikipedia.org/wiki/Hexadezimale_Farbdefinition
 *
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <tuple>
#include <string>
#include <iostream>




PGL_NS_START(pgl)
PGL_NS_START(graph)

/**
 * \brief	This is a definition of compile time variables for colours
 */
constexpr static ::pgl::Int32_t COLOUR_WITHE 	= 0xffffff;
constexpr static ::pgl::Int32_t COLOUR_BLACK 	= 0x000000;
constexpr static ::pgl::Int32_t COLOUR_GREY 	= 0x888888;
constexpr static ::pgl::Int32_t COLOUR_RED 	= 0xff0000;
constexpr static ::pgl::Int32_t COLOUR_GREEN 	= 0x00ff00;
constexpr static ::pgl::Int32_t COLOUR_BLUE 	= 0x0000ff;
constexpr static ::pgl::Int32_t COLOUR_CYAN 	= 0x00ffff;
constexpr static ::pgl::Int32_t COLOUR_MAGENTA 	= 0xff00ff;
constexpr static ::pgl::Int32_t COLOUR_YELLOW 	= 0xffff00;
constexpr static ::pgl::Int32_t COLOUR_VIOLETT 	= 0x8800ff;
constexpr static ::pgl::Int32_t COLOUR_ORANGE 	= 0xff8800;





class pgl_colour_t
{
	/*
	 * ===================================
	 * Typedefs
	 */
public:
	using colourBase_t 		= ::pgl::Int32_t;	//!< Type for representing the colour.
	using colourComponent_t 	= ::pgl::uInt8_t;		//!< Type to represent a component of the colour.
	using colourFraction_t 		= ::pgl::Float_t;		//!< This is the fractional colour
	/**
	 * \typedef colourTriplet_t
	 * Is used to represent the tree component of a colour.
	 * The ordering inside is:
	 * 	- 0 	Red
	 * 	- 1  	Green
	 * 	- 2 	Blue
	 */
	using colourTriplet_t 		= ::std::tuple<colourComponent_t, colourComponent_t, colourComponent_t>;



	/*
	 * ===================================
	 * Internal definitions
	 *
	 * This are internal definitions for the colour class.
	 * They are connected with the bit represenatation of the colour
	 */
private:
	static constexpr ::pgl::Int32_t redOffset 	= 0; 			//!< Shift to get the red component
	static constexpr ::pgl::Int32_t greenOffset  	= 8; 			//!< Shift to get the green component
	static constexpr ::pgl::Int32_t blueOffset 	= 16;			//!< Shift to get the blue offset
	static constexpr colourBase_t compnentMask	= 0xff;			//!< Mask for the last component
	static constexpr colourBase_t colourPartMask 	= 0xffffff;		//!< Mask for the colour part
	static constexpr colourComponent_t MAX_COL_COMP = 0xff;			//!< Maximal value of the colour component

	/*
	 * ============================================
	 * Internal functions, for manipulating the internal structure
	 */
private:
	/**
	 * \brief	This function transform a component into a base color type, withch is only red
	 */
	inline
	constexpr
	colourBase_t
	inter_makeRedCol(
		const colourComponent_t 	redComp) const noexcept
	{
		return static_cast<colourBase_t>(redComp);
	};


	/**
	 * \brief	This function generates a blue colour
	 *
	 * The presented component is interpreted as blue comonent and the returned colourhas only blue component
	 */
	inline
	constexpr
	colourBase_t
	inter_makeBlueCol(
		const colourComponent_t 	blueComp) const noexcept
	{
		return static_cast<colourBase_t>( (static_cast<colourBase_t>(blueComp)) << blueOffset);
	};


	/**
	 * \brief	This function generates a green colour
	 */
	inline
	constexpr
	colourBase_t
	inter_makeGreenCol(
		const colourComponent_t 	greenComp) const noexcept
	{
		return static_cast<colourBase_t>( (static_cast<colourBase_t>(greenComp)) << greenOffset);
	};


	/**
	 * \brief 	This function extracts the blue component from the colour
	 */
	inline
	constexpr
	colourComponent_t
	inter_getBlueComp(
		const colourBase_t 		colour) const noexcept
	{
		return static_cast<colourComponent_t>( (colour >> blueOffset) & compnentMask );
	};


	/**
	 * \brief	This function extracts the green component from the colour
	 */
	inline
	constexpr
	colourComponent_t
	inter_getGreenComp(
		const colourBase_t 		colour) const noexcept
	{
		return static_cast<colourComponent_t>( (colour >> greenOffset) & compnentMask );
	};



	/**
	 * \brief	This function extract the red part from the colour
	 */
	inline
	constexpr
	colourComponent_t
	inter_getRedComp(
		const colourBase_t 		colour) const noexcept
	{
		return static_cast<colourComponent_t>(compnentMask & colour);
	};




	/*
	 * =================================
	 * Constructor
	 */
public:
	/**
	 * \brief	Component constructor
	 *
	 * This constructor takes three 8bit ints and constructs a colour object.
	 * The order is the RGB order
	 *
	 * \param  redC 	The red component of the colour
	 * \param  greenC 	The green component of the colour
	 * \param  blueC 	The blue component of the colour
	 */
	inline
	constexpr
	pgl_colour_t(
		const colourComponent_t 	redC,
		const colourComponent_t 	greenC,
		const colourComponent_t 	blueC) noexcept :
	  m_colour(inter_makeRedCol(redC) | inter_makeGreenCol(greenC) | inter_makeBlueCol(blueC) )
	{
	};


	/**
	 * \brief	Default constructor
	 *
	 * This constructor constructs a withe colour object.
	 */
	inline
	constexpr
	pgl_colour_t() noexcept :
	  pgl_colour_t(COLOUR_WITHE)
	{
	};


	/**
	 * \brief	Copy constructor
	 * This constructor is defaulted
	 */
	inline
	constexpr
	pgl_colour_t(
		const pgl_colour_t&) noexcept = default;

	/**
	 * \bief	Copy assignment
	 * The copy assignemnt is defaulted
	 */
	inline
	pgl_colour_t&
	operator= (
		const pgl_colour_t&) noexcept = default;

	/**
	 * \brief	Move constructor
	 * This move constructor is defaulted
	 */
	inline
	constexpr
	pgl_colour_t(
		pgl_colour_t&&) noexcept = default;

	/**
	 * \brief	Move assignment
	 * The move assignemt is defaulted
	 */
	inline
	pgl_colour_t&
	operator= (
		pgl_colour_t&&) noexcept = default;


	/**
	 * \brief	The destructor is defaulted
	 */
	inline
	~pgl_colour_t() noexcept = default;


	// ===== Private Constructors
private:
	/**
	 * \brief	This is the full colour constructor
	 *
	 * This constructor is private, and shall be only used by the class itself.
	 * If the colur is claped such that the non color part is ignored
	 */
	inline
	constexpr
	explicit
	pgl_colour_t(
		colourBase_t 		colour) noexcept:
	  m_colour(colour & colourPartMask)
	{
	};



	/*
	 * ===================================
	 * Getter function
	 *
	 * This functions implementing getters.
	 * They can be used to querry the components.
	 *
	 * The fractional colour is a float (single) between [0, 1], to express the component.
	 */
public:

	/**
	 * \brief 	Return the red component of *this
	 */
	inline
	constexpr
	colourComponent_t
	getRedComp() const noexcept
	{
		return inter_getRedComp(this->m_colour);
	};

	/**
	 * \brief	Retrurn the blue component of *this
	 */
	inline
	constexpr
	colourComponent_t
	getBlueComp() const noexcept
	{
		return inter_getBlueComp(this->m_colour);
	};

	/**
	 * \brief	Return the green component of this
	 */
	inline
	constexpr
	colourComponent_t
	getGreenComp() const noexcept
	{
		return inter_getGreenComp(this->m_colour);
	};


	/**
	 * \brief	Retrun a number [0, 1] that express the red component
	 */
	inline
	colourFraction_t
	getRedFraction() const noexcept
	{
		return this->getRedComp() / static_cast<colourFraction_t>(MAX_COL_COMP);
	};


	/**
	 * \brief	Retruns the blue colour fraction of *this
	 */
	inline
	colourFraction_t
	getBlueFraction() const noexcept
	{
		return this->getBlueComp() / static_cast<colourFraction_t>(MAX_COL_COMP);
	};


	/**
	 * \brief	Retruns the green colour fraction of *this
	 */
	inline
	colourFraction_t
	getGreenFraction() const noexcept
	{
		return this->getGreenComp() / static_cast<colourFraction_t>(MAX_COL_COMP);
	};


	/**
	 * \brief	This function returns the colour part of this.
	 *
	 * This is the colour part of *this.
	 * The structure is 0x[RED][GREEN][BLUE]
	 *
	 * All other internal status bits are striped.
	 */
	inline
	constexpr
	colourBase_t
	getColourPart() const noexcept
	{
		return (this->m_colour & colourPartMask);
	};



	/**
	 * \brief	This function returns a triplet of the components.
	 * The first one is red, second is green and third is blue.
	 */
	inline
	constexpr
	colourTriplet_t
	getColourTriplet() const noexcept
	{
		return ::std::make_tuple(this->getRedComp(), this->getGreenComp(), this->getBlueComp() );
	};



	/*
	 * =======================================
	 * Creator and isA functions
	 *
	 * For each colour above there is a set of function.
	 * 	> isCOLOUR: Returns true if *this is of this colour.
	 * 	> COLOUR:  Generates an object with that colour.
	 */
public:
#define COLOUR_GENRATION_FUNCTION(ColourName, COLOURCONST)  inline constexpr static pgl_colour_t ColourName () noexcept { return pgl_colour_t( COLOURCONST ); }; \
	inline constexpr bool is ## ColourName () const noexcept { return this->getColourPart() == COLOURCONST ; };

	COLOUR_GENRATION_FUNCTION(White, COLOUR_WITHE);
	COLOUR_GENRATION_FUNCTION(Black, COLOUR_BLACK);
	COLOUR_GENRATION_FUNCTION(Red, COLOUR_RED);
	COLOUR_GENRATION_FUNCTION(Grey, COLOUR_GREY);
	COLOUR_GENRATION_FUNCTION(Green, COLOUR_GREEN);
	COLOUR_GENRATION_FUNCTION(Blue, COLOUR_BLUE);
	COLOUR_GENRATION_FUNCTION(Cyan, COLOUR_CYAN);
	COLOUR_GENRATION_FUNCTION(Magenta, COLOUR_MAGENTA);
	COLOUR_GENRATION_FUNCTION(Yellow, COLOUR_YELLOW);
	COLOUR_GENRATION_FUNCTION(Violett, COLOUR_VIOLETT);
	COLOUR_GENRATION_FUNCTION(Orange, COLOUR_ORANGE);

#undef COLOUR_GENRATION_FUNCTION


	/*
	 * ========================================
	 * Comperators
	 */
public:
	/**
	 * \brief	Returns true if *this and rhs are equal.
	 *
	 * Only the colpour part is compared, the status is not
	 */
	inline
	constexpr
	bool
	isEqual(
		const pgl_colour_t& rhs) const noexcept
	{
		return this->getColourPart() == rhs.getColourPart();
	};

	/**
	 * \brief	REtruns if *this is less than rhs.
	 *
	 * Establisched a lexicographic order.
	 * First red is condisered, then green and finally blue.
	 */
	inline
	constexpr
	bool
	isLess(
		const pgl_colour_t& rhs) const noexcept
	{
		return this->getColourTriplet() < rhs.getColourTriplet();
	};


	/**
	 * \brief	 Retruns a string with the colour code
	 *
	 * The format is (RR, GG, BB), where as the lets are the components in hex
	 */
	inline
	std::string
	print() const
	{
#define STEFANO_SIZE 512
		char Stefano[STEFANO_SIZE];

		if(snprintf(Stefano, STEFANO_SIZE, "(%x, %x, %x)", this->getRedComp(), this->getGreenComp(), this->getBlueComp() ) >= STEFANO_SIZE)
		{
			//Tried to write more than we have
			throw PGL_EXCEPT_LENGTH("Exceeded Stefanos capabilities.");
		};
#undef STEFANO_SIZE

		return std::string(Stefano);
	};


	/*
	 * ====================================
	 * Private variable
	 */
private:
	colourBase_t 		m_colour;
}; //End class(pgl_connection_payload_t)


/*
 * =======================================
 * Operator overloading
 */

inline
constexpr
bool
operator== (
	const pgl_colour_t& 	rhs,
	const pgl_colour_t& 	lhs)
{
	return rhs.isEqual(lhs);
};

inline
constexpr
bool
operator!= (
	const pgl_colour_t& 	rhs,
	const pgl_colour_t& 	lhs)
{
	return !rhs.isEqual(lhs);
};


inline
constexpr
bool
operator< (
	const pgl_colour_t& 	rhs,
	const pgl_colour_t& 	lhs)
{
	return rhs.isLess(lhs);
};



PGL_NS_END(graph)
PGL_NS_END(pgl)



inline
std::ostream&
operator<< (
	std::ostream& 		o,
	const pgl::graph::pgl_colour_t& 	c)
{
	o << c.print();

	return o;
};




/*
 * Overload some standard predicates
 */
namespace std
{
        template <>
        struct hash<::pgl::graph::pgl_colour_t>
        {
                size_t operator()(
                        const ::pgl::graph::pgl_colour_t& x) const
                {
                        return m_privHash(x.getColourPart() );
                }; //End operator

        private:
                std::hash<::pgl::graph::pgl_colour_t::colourBase_t> m_privHash;
        }; //End struct(hash)

}; //End namespace(std)







#endif //End include guard
