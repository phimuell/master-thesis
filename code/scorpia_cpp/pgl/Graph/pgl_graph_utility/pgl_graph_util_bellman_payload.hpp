#pragma once
/**
 * \file	Graph/pgl_graph_utility/pgl_graph_util_bellman_payload.hpp
 * \brief	Contains the payload for the bellman and ford algorithm
 *
 * The payload that is used for the edge is the same one as the one in kruskal, but the vertex is a bit different
 */


//Include PGL
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex_payload.hpp>

#include <pgl_graph_utility/pgl_graph_util_kruskal_payloads.hpp>


//Include STD

PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


/**
 * \class 	pgl_bf_edgePayload
 * \brief	Payload for the connection in the bellman and ford algorithm.
 *
 * This is essentialy a typedef for the one used in the kruskall algorithm
 */
class pgl_bf_edgePayload_t : public pgl_kruskal_connPL_t
{
public:
	virtual
	~pgl_bf_edgePayload_t();

	pgl_bf_edgePayload_t(
		Distance_t 	d);

}; //End class(pgl_bf_edgePayload_t)



/**
 * \class	pgl_bf_nodePayload_t
 * \brief	Payload for the node in the bf algorithm
 */
class pgl_bf_nodePayload_t : public pgl_vertex_payload_t
{
public:

	/*
	 * ===================
	 * Constructors
	 */
	pgl_bf_nodePayload_t();


	virtual
	~pgl_bf_nodePayload_t();

	pgl_bf_nodePayload_t(
		Distance_t 	d);



	/**
	 * \brief	This returns the current distance of the vertex.
	 */
	virtual
	Distance_t
	getCurrDistance() const override;


	/**
	 * \brief	Set the current distance to the given value
	 * \param nd	New Distance
	 *
	 */
	virtual
	void
	setCurrentDistanceTo(
		const Distance_t& nd) override;



	/**
	 * \brief	Makes the predecessor ID of *this invalid
	 */
	virtual
	void
	clearPredecessor() override;


	/**
	 * \brief 	Sets the predecessor to the given ID
	 */
	virtual
	void
	setPredecessorTo(
		pgl_vertexID_t 		newP) override;


	/**
	 * \brief 	returns the predecessor id of *this
	 */
	virtual
	pgl_vertexID_t
	getPredecessor() const override;

	/**
	 * \brief	Returns true, if the ID is valid
	 */
	virtual
	bool
	hasPredecessor() const override;




	/*
	 * ==============================
	 * Private member variables
	 */
private:

	pgl_vertexID_t 	m_predecessor;
	Distance_t 	m_currDistance; //!< The distance to this
}; //End class(pgl_bf_nodePayload_t)











PGL_NS_END(graph)
PGL_NS_END(pgl)












