# This CMake file will add all the files in the current folder
# to the PGL library.
 
target_sources(
	pgl
  PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_util_bellman_payload.cpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_util_kruskal_payloads.cpp"
  PUBLIC
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_colour.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_edgePath.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_graph_topo_info.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_tree_description.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_tree_path_map.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_util_bellman_payload.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_util_kruskal_payloads.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_vertex_map.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_graph_vertex_map_iterator.hpp"
)
