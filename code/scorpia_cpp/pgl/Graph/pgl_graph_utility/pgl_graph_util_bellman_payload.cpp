//Include PGL
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertex_payload.hpp>

#include <pgl_graph_utility/pgl_graph_util_kruskal_payloads.hpp>
#include <pgl_graph_utility/pgl_graph_util_bellman_payload.hpp>


//Include STD

PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


pgl_bf_edgePayload_t::~pgl_bf_edgePayload_t() = default;

pgl_bf_edgePayload_t::pgl_bf_edgePayload_t(
		Distance_t 	d) :
  pgl_kruskal_connPL_t(0)
{
	m_connWeight = d;	//This is a hack in kruskal the weights are positive and this is enforced, so we bypass it.
};


pgl_bf_nodePayload_t::pgl_bf_nodePayload_t() = default;


pgl_bf_nodePayload_t::~pgl_bf_nodePayload_t() = default;

pgl_bf_nodePayload_t::pgl_bf_nodePayload_t(
		Distance_t 	d) :
	  pgl_vertex_payload_t()
	, m_predecessor()
	, m_currDistance(0)
{
	(void)d;
};



Distance_t
pgl_bf_nodePayload_t::getCurrDistance() const
{
	return m_currDistance;
};


void
pgl_bf_nodePayload_t::setCurrentDistanceTo(
		const Distance_t& nd)
{
	m_currDistance = nd;
	return;
};


void
pgl_bf_nodePayload_t::clearPredecessor()
{
	m_predecessor = pgl_vertexID_t::INVALID();

	return;
};


void
pgl_bf_nodePayload_t::setPredecessorTo(
		pgl_vertexID_t 		newP)
{
	m_predecessor = std::move(newP);
	return;
};

pgl_vertexID_t
pgl_bf_nodePayload_t::getPredecessor() const
{
	return m_predecessor;
};

bool
pgl_bf_nodePayload_t::hasPredecessor() const
{
	return m_predecessor.isValid();
};





PGL_NS_END(graph)
PGL_NS_END(pgl)












