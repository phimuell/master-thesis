#pragma once
/**
 * \brief	This file implements an iterator that is used for iterating over the the map.
 *
 * Its main purpoose is to hide the internal structure of th map
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_allocator.hpp>

#include <pgl_util/pgl_iterator.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>


//Include BOOST
#include <boost/iterator/iterator_facade.hpp>


PGL_NS_START(pgl)
PGL_NS_START(graph)

//Forward declaration
template<typename MappedType, typename Allocator>
class pgl_vertexMap_t;

PGL_NS_START(internal)



/**
 * \brief	Class to hide the internal structure of the vertex map.
 *
 * It assumes that the iterator is pointing to a pair.
 * first is the mapped type ad second is the vertex id
 *
 * \class 	pgl_graph_vertexMapIterator_t
 */
template<
	bool 	isConst, 	//!< Is this a constant iterator
	class 	wrappedIterator, 	//!< This is the underlying iterator
	class 	wrappedType 	//!< This is the type we wrap
>
class pgl_graph_vertexMapIterator_t : public
		boost::iterator_facade<
			pgl_graph_vertexMapIterator_t<isConst, wrappedIterator, wrappedType>,
			wrappedType,
			typename std::iterator_traits<wrappedIterator>::iterator_category
		>
{
	/*
	 * ======================
	 * Typedefs
	 */
public:
	using Value_t 		= ::std::conditional_t<isConst, const ::std::remove_cv_t<wrappedType>, wrappedType>;		//!< The type we wrapp
	using Reference_t 	= ::std::conditional_t<isConst, const Value_t&, Value_t&>;		//!< This is the reference type we use
	using wIterator_t 	= wrappedIterator;	//!< Type of the iterator we wrap
	using VertexID_t 	= ::pgl::graph::pgl_vertexID_t;

	/*
	 * ============================
	 * Constructors
	 *
	 * All constructors are public and defaulted.
	 * also the meaningfull constructor is private, but the map is declared a fried.
	 */
public:
	inline
	pgl_graph_vertexMapIterator_t() noexcept(::std::is_nothrow_default_constructible<wIterator_t>::value) = default;

	inline
	pgl_graph_vertexMapIterator_t(
		const pgl_graph_vertexMapIterator_t&) noexcept(::std::is_nothrow_copy_constructible<wIterator_t>::value) = default;

	inline
	pgl_graph_vertexMapIterator_t(
		pgl_graph_vertexMapIterator_t&&) noexcept(::std::is_nothrow_move_constructible<wIterator_t>::value) = default;

	inline
	pgl_graph_vertexMapIterator_t&
	operator= (
		const pgl_graph_vertexMapIterator_t&) noexcept(::std::is_nothrow_copy_assignable<wIterator_t>::value) = default;

	inline
	pgl_graph_vertexMapIterator_t&
	operator= (
		pgl_graph_vertexMapIterator_t&&) noexcept(::std::is_nothrow_move_assignable<wIterator_t>::value) = default;


	/**
	 * \brief	This is the meaningfull constructor.
	 *
	 * It takes a reference to the real iterator and constructs itself.
	 * It is priovate
	 */
private:
	inline
	pgl_graph_vertexMapIterator_t(
		const wIterator_t& w) noexcept(std::is_nothrow_copy_assignable<wIterator_t>::value) :
	  m_it(w)
	{};

	inline
	pgl_graph_vertexMapIterator_t(
		wIterator_t&& w) noexcept(std::is_nothrow_move_constructible<wIterator_t>::value) :
	  m_it(std::move(w))
	{};


	/*
	 * ============================
	 * This functions are requered by the fascade
	 *
	 * They can throw, if the iterator is not initialized properly
	 */
public:

	/**
	 * \brief	Dereferences this
	 */
	Reference_t
	dereference() const
	{
		return this->m_it->second;
	};


	/**
	 * \briefe test if two iterators are same
	 */
	bool
	equal(
		const pgl_graph_vertexMapIterator_t& y) const noexcept
	{
		return m_it == y.m_it;
	};


	/**
	 * \brief increments this
	 */
	template<class wIT = wIterator_t>
	std::enable_if_t<::pgl::pgl_is_at_least_category<::std::forward_iterator_tag, wIT>, void>
	increment()
	{
		static_assert(std::is_same<wIT, wIterator_t>::value, "Must be the same iterator type");

		++m_it;
		return;
	};


	/**
	 * \brief decrement
	 */
	template<class wIT = wIterator_t>
	std::enable_if_t<::pgl::pgl_is_at_least_category<::std::bidirectional_iterator_tag, wIT>, void>
	decrement()
	{
		static_assert(std::is_same<wIT, wIterator_t>::value, "Must be the same iterator type");

		--m_it;
		return;
	};


	/**
	 * \brief	Addvance by
	 */
	template<class wIT = wIterator_t>
	std::enable_if_t<::pgl::pgl_is_at_least_category<::std::random_access_iterator_tag, wIT>, void>
	advance(
		::pgl::sSize_t n) noexcept(false)
	{
		static_assert(std::is_same<wIT, wIterator_t>::value, "Must be the same iterator type");

		m_it += n;

		return;
	};

	/**
	 * \brief	get the distance to
	 */
	template<class wIT = wIterator_t>
	std::enable_if_t<::pgl::pgl_is_at_least_category<::std::random_access_iterator_tag, wIT>, ::pgl::sSize_t>
	distance_to(
		const pgl_graph_vertexMapIterator_t& i) const noexcept(false)
	{
		static_assert(std::is_same<wIT, wIterator_t>::value, "Must be the same iterator type");

		return ::pgl::sSize_t(i.m_it - m_it);
	};


	/*
	 * =========================
	 * This are functions that are additional
	 */
public:

	/**
	 * \brief	This function returns the associated vertex ID
	 */
	VertexID_t
	key() const noexcept(false)
	{
		return m_it->first;
	};


	/**
	 * \brief	This fucntion returns true if the ID is valid
	 */
	bool
	isValidID() const noexcept(false)
	{
		return this->key().isValid();
	};

	/**
	 * \brief	Returns the value
	 */
	inline
	Reference_t
	value() const
	{
		return m_it->second;
	};



	/*
	 * ==================================
	 * Friend
	 */
	template<typename T1, typename T2>
	friend class ::pgl::graph::pgl_vertexMap_t;


	/*
	 * ========================0
	 * Private member
	 */
private:
	wIterator_t 			m_it;		//!< This is the wrapped iterator
};

PGL_NS_END(internal)

PGL_NS_END(graph)
PGL_NS_END(pgl)







