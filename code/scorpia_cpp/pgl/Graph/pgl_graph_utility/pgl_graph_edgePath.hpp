#pragma once
/**
 * \file	pgl/Graph/pgl_graph_utility/pgl_graph_edgePath.hpp
 * \brief	This class represents a path in the graph.
 *
 * A path is a sequence of edges, that are consecutive, and the end of an element is the start of the other element.
 * This is essentialy a stack, that provides some tests, when adding elements to it.
 *
 * The class stores pointer to edges, so, when the completion state of the graph is changed, the path in it may become invalid
 * in terms of memeory addresses.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>
#include <pgl_stack.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>


PGL_NS_START(pgl)
PGL_NS_START(graph)


template<bool isConst = false >
class pgl_edgePath_t
{
	/*
	 * =========================
	 * Some typedefs
	 */
public:
	using container_type		= Container;
	using value_type 		= typename Container::value_type;
	using size_type			= typename Container::size_type;
	using reference			= typename Container::reference;
	using const_reference		= typename Container::const_reference;
	using iterator			= typename Container::const_reverse_iterator;
	using const_iterator 		= typename Container::const_reverse_iterator;



	/*
	 * ====================
	 * Constructor
	 */
public:
	/**
	 * \brief	Use perfect forwarding to make any constructor apear
	 */
	template<typename... ArgsT>
	pgl_vertexMap_t(ArgsT&&... args) :
	  container_type(std::forward<ArgsT>(args)...)
	{
	};


	pgl_vertexMap_t() = default;

	pgl_vertexMap_t(
		const pgl_vertexMap_t&) = default;

	pgl_vertexMap_t(
		pgl_vertexMap_t&&) = default;

	pgl_vertexMap_t&
	operator= (
		const pgl_vertexMap_t&) = default;

	pgl_vertexMap_t&
	operator= (
		pgl_vertexMap_t&&) = default;

	~pgl_vertexMap_t() = default;






	/*
	 * ============================
	 * Accesser function
	 */
public:
	reference
	operator[](
		const pgl_vertexID_t& n) noexcept
	{
		pgl_assert(n.getID() < this->size());
		pgl_assert(n.isValid());

		return this->container_type::operator[](n.getID());
	};

	const_reference
	operator[](
		const pgl_vertexID_t& n) const noexcept
	{
		pgl_assert(n.getID() < this->size());
		pgl_assert(n.isValid());

		return this->container_type::operator[](n.getID());
	};


	reference
	at(
		const pgl_vertexID_t& n)
	{

		pgl_assert(n.isValid());

		return this->container_type::at(n.getID());
	};


	const_reference
	at(
		const pgl_vertexID_t& n) const
	{

		pgl_assert(n.isValid());

		return this->container_type::at(n.getID());
	};



	void
	swap(
		container_type& other)
	{
		static_cast<container_type*>(this)->swap(other);

		return;
	};



}; //End class(pgl_vertexMap_t)







PGL_NS_END(graph)
PGL_NS_END(pgl)




