#pragma once
/**
 * \file	pgl/Graph/pgl_graph_utility/pgl_graph_vertex_map.hpp
 * \brief	A conveniant vector replacement that works with vertexIDs
 *
 * This class provides the same functionaliy as the deque, vector and array.
 * But their Subscript and at operator also takes pgl_vertexID_t as arguments.
 *
 * For the time being this class assumes that the numbering of the nodes is consecutive and starts with zero.
 * There are plans to lift this requirements, since the implementation of the graph, did not impose this restriction.
 *
 * The current "solution" is the the usage of a simple array that stores pair<VertexID, type>, then a binary search can be performed to search for it.
 * This leads to a time complexity of O[log(n)], in contrast to O[1], that is offered by an array.
 * One could also have used a Hashmap to store the type, the hashmap offers O[1], with a larger consatnt, but the implementation offered by stl imposes a big space penalty.
 * This penalty is so big, that I consider it as not worth.
 * A further extension is the boundedMap.
 * There the graph stores a hashmap that is able to make the mapping from vertexID to consecutive indexis.
 * The boundedMap stores a reference to that map and first retrives the index before accessing a normal array.
 * But it is much work to implement so this type is postponed.
 *
 * The space pennalty by the choosen approace is not so high, it is 32Bit per entry, if one ignores padding that is happening.
 *
 * There is a special iterator that emulates the vector iterators.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_algorithm.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_graph_utility/pgl_graph_vertex_map_iterator.hpp>


//Include STD
#include <type_traits>
#include <utility>
#include <iterator>



PGL_NS_START(pgl)
PGL_NS_START(graph)



/**
 * \brief	This class is used as a tag to indicate to the vertexMap that the numbering is consecutive
 * \class 	pgl_graph_vertexMap_consecutive_numbering_tag
 */
struct pgl_graph_vertexMap_consecutive_numbering_tag
{
};



/**
 * \class 	pgl_vertexMap_t
 * \brief	This class is a convienient expansion to the vector and array.
 *
 * This is a special container.
 * It is able to map vertex IDs to objects.
 *
 * One can think of it as a vector that provides overloads for the operator[] and at() for vertexID types.
 * The map is also able to handel cases where the numbering of the noes is not consecutive and starts at 0.
 * It garantees O[log(n)] access time complexity, but for the case where the IDs are consecutive numbers starting at 0 it offers O[1] complexities.
 *
 * There are iterators that offers normal iterating as if it was a plain old vector.
 * The iterator is guaranteed to be at least a forward iterator, but in the default case, it is a random access iterator.
 *
 * Since this class also covers the case where the IDs are not consecutive numbers starting at 0, this class is thigthly linked to a graph.
 * Furter one needs a graph to change the size, this is needed since the graph is needed to get the correct mapping.
 * There are possiblities for the case when the user can garantee that the IDs are consecutive starting at 0.
 *
 * \tparam MappedType		This is the mapped type, is ignored, if container is present
 * \tapram Allocator 		This is the used allocator
 */
template<typename MappedType, typename Allocator = void >
class pgl_vertexMap_t
{
	/*
	 * =========================
	 * Some typedefs
	 */
public:
	using VertexID_t 	= ::pgl::graph::pgl_vertexID_t;			//!< The vertex ID
	using VIDBase_t 	= VertexID_t::Base_t;				//!< This is the base type of the comparision
	using Node_t 		= ::std::pair<VertexID_t, MappedType>;		//!< This is the type we effectively store inside the container
	using container_type 	= ::pgl::pgl_vector_t<Node_t>;			//!< This is the container we use

	using size_type 	= ::pgl::Size_t;				//!< The size type
	using value_type	= MappedType;					//!< This is the value type that is stored
	using reference 	= MappedType&;					//!< The reference type of the container
	using const_reference	= const MappedType&; 				//!< Const reference

	using iterator 		= internal::pgl_graph_vertexMapIterator_t<false, typename container_type::iterator, MappedType>;
	using const_iterator 	= internal::pgl_graph_vertexMapIterator_t<true, typename container_type::const_iterator, const MappedType>;


	/*
	 * ====================
	 * Constructor
	 */
public:

	/**
	 * \brief	This is teh constructor that will construct a map from the graph.
	 *
	 * All values are default initialized
	 */
	pgl_vertexMap_t(
		const pgl_graph_t& Graph) :
	  m_container()
	{
		this->resize(Graph);
	};


	/**
	 * \brief	Initialize from an range with a graph
	 *
	 */
	template<class InputIt>
	pgl_vertexMap_t(
		const pgl_graph_t& Graph,
		InputIt start,
		InputIt ende) :
	  m_container()
	{
		this->resize(Graph, start, ende);
	};


	/**
	 * \brief	Initialize from an range
	 *
	 * Is only possible, if one assumes consecutive numbering
	 */
	template<class InputIt>
	pgl_vertexMap_t(
		const pgl_graph_vertexMap_consecutive_numbering_tag,
		InputIt start,
		InputIt ende) :
	  m_container()
	{
		const Size_t n = std::distance(start, ende);
		m_container.reserve(n);

		for(VIDBase_t it = 0; it != (VIDBase_t)n; ++it)
		{
			m_container.emplace_back(std::make_pair(VertexID_t(it, true), *start));
			++start;
		}; //End for(it)
	};

	/**
	 * \brief	Default constructor
	 *
	 * Is allowed
	 */
	pgl_vertexMap_t() = default;


	/**
	 * \brief	Defaulted copy constructor
	 */
	pgl_vertexMap_t(
		const pgl_vertexMap_t&) = default;


	/**
	 * \brief	Defaulted move constructor
	 *
	 * It is noexcept
	 */
	pgl_vertexMap_t(
		pgl_vertexMap_t&&) noexcept = default;

	/**
	 * \brief	Defaulted copy assignment
	 */
	pgl_vertexMap_t&
	operator= (
		const pgl_vertexMap_t&) = default;

	/**
	 * \brief	Defaulted move assignemnet
	 */
	pgl_vertexMap_t&
	operator= (
		pgl_vertexMap_t&&) = default;

	/**
	 * \brief	Defaulted destructor
	 */
	~pgl_vertexMap_t() = default;

public:
	/*
	 * ===============================
	 * Iterator functions
	 */

	inline
	iterator
	begin() noexcept
	{
		return m_container.begin();
	};

	const_iterator
	begin() const noexcept
	{
		return m_container.cbegin();
	};

	inline
	const_iterator
	cbegin() const noexcept
	{
		return m_container.cbegin();
	};

	inline
	iterator
	end() noexcept
	{
		return m_container.end();
	};

	inline
	const_iterator
	end() const noexcept
	{
		return m_container.cend();
	};

	inline
	const_iterator
	cend() const noexcept
	{
		return m_container.cend();
	};



	/*
	 * ============================
	 * Accesser function
	 *
	 * The class provides at and operator[].
	 * Both are overloaded for size_t and vertexID_t.
	 *
	 * As allready mentioned, the access is generaly O[log(n)], but if the numbering is consecutive and starts at zero, the access is O[1]
	 *
	 * Please notice that unlike the operator[] of the vector, the operator[] does throw.
	 */
public:

	inline
	reference
	operator[](
		const size_type i) noexcept(false)
	{

#define DO_ACCESS_MAP if(i < m_container.size() && m_container[i].first.getID() == i) \
		{									\
			return m_container[i].second;					\
		}									\
		else									\
		{									\
			const auto it = std::lower_bound(m_container.begin(), m_container.end(), (VIDBase_t)i, [](const Node_t& lhs, const VIDBase_t& rhs){ return lhs.first < rhs;});	\
			if(it == m_container.end())					\
			{								\
				throw PGL_EXCEPT_OutOfBound("The requested ID does not exists"); \
			}								\
			else if(it->first == (VIDBase_t)i)				\
			{								\
				return it->second;					\
			}								\
			else								\
			{								\
				throw PGL_EXCEPT_OutOfBound("The requested ID does not exists");	\
			};								\
		}; //End if: test if shortcut is possible

		DO_ACCESS_MAP
	};


	inline
	const_reference
	operator[](
		const size_type i) const noexcept(false)
	{
		DO_ACCESS_MAP
	};

	inline
	reference
	at(
		const Size_t i) noexcept(false)
	{
		DO_ACCESS_MAP
	};


	inline
	const_reference
	at(
		const Size_t i) const noexcept(false)
	{
		DO_ACCESS_MAP
	};


	inline
	reference
	operator[](
		const pgl_vertexID_t& n) noexcept(false)
	{
		pgl_assert(n.isValid());
		const auto i = n.getID();
		DO_ACCESS_MAP
	};

	inline
	const_reference
	operator[](
		const pgl_vertexID_t& n) const noexcept(false)
	{
		pgl_assert(n.isValid());
		const auto i = n.getID();
		DO_ACCESS_MAP
	};


	inline
	reference
	at(
		const pgl_vertexID_t& n) noexcept(false)
	{
		if(n.isInvalid() == true)
		{
			throw PGL_EXCEPT_LOGIC("Tried to access an invalid ID");
		};
		const auto i = n.getID();
		DO_ACCESS_MAP
	};


	inline
	const_reference
	at(
		const pgl_vertexID_t& n) const noexcept(false)
	{
		if(n.isInvalid() == true)
		{
			throw PGL_EXCEPT_OutOfBound("Tried to access with an invalid vertex ID");
		};

		const Size_t i = n.getID();
		DO_ACCESS_MAP
	};

#undef DO_ACCESS_MAP


	/**
	 * \brief	The size function
	 *
	 * This is the function for quering the size
	 */
	inline
	size_type
	size() const noexcept
	{
		return m_container.size();
	};


	/**
	 * \brief	Retruns true if *this is associated with a graph.
	 *
	 * A map is associated to a graph, if its size is not zero.
	 * We here assume, that there is no graph with 0 vertices.
	 *
	 * \note This function currently not distinguish between the empty graph and not associated.
	 *
	 */
	inline
	bool
	isAssociated() const noexcept
	{
		return (m_container.size() != 0) ? true : false;
	};


	/**
	 * \brief	This function returns true if the map is empty.
	 *
	 * An empty map is generally a not associated map.
	 * But this is not true, because a map can be associated with the mapty graph.
	 *
	 * \note This function currently not distinguish between the empty graph and not associated.
	 */
	inline
	bool
	isEmpty() const noexcept
	{
		return m_container.empty();
	};

	/*
	 * ===================
	 * Managing function
	 */
public:
	/**
	 * \brief	This is an improved assign function.
	 *
	 * It assignes each member that is stored inside the graph the provided value
	 * This function is comparable to the fill function.
	 *
	 * The ordering of the graph is not changed
	 */
	inline
	void
	assign(
		const value_type& newValue)
	{
		for(auto& i : m_container)
		{
			i.second = newValue;
		};

		return;
	};


	/**
	 * \brief	Assigne values form a range to this.
	 *
	 * This function assumes, that the range is ordered wrt to '<' and the IDs of the nodes id
	 */
	template<class forwardIterator>
	inline
	void
	assign(
		forwardIterator 	start,
		const forwardIterator 	ende)
	{
		pgl_assert(std::distance(start, ende) == this->size());
		const Size_t n = m_container.size();
		for(Size_t it = 0; it != n; ++it)
		{
			if(start == ende)
			{
				throw PGL_EXCEPT_OutOfBound("Range was shorter than the number of vertices.");
			};

			//Assigne value
			m_container[it].second = *start;

			//Increment range iterator
			++start;
		};

		pgl_assert(start == ende);

		return;
	};


	/**
	 * \brief	Resizes this on the basis of the graph
	 *
	 * If the graph is associated with an graph, the map is cleared first.
	 * Then a new map is allocated.
	 *
	 * The mapped type is set to the presented value.
	 */
	inline
	void
	resize(
		const pgl_graph_t& 	Graph,
		const MappedType& 	newValue)
	{
		const Size_t n = Graph.nVertices();

		//Clear map if we have one
		if(m_container.size() != 0)
		{
			m_container.clear();
		};

		//Allocate space
		m_container.reserve(n + 1);

		auto Node = Graph.CNodeBegin();
		for(Size_t it = 0; it != n; ++it)
		{
			//Construct the node type
			m_container.emplace_back(Node->getID(), newValue);

			++Node;
		}; //End get the Node ID

		pgl_assert(Node == Graph.CNodeEnd());
		pgl_assert(n == m_container.size());

		//If a sorting was neccessary, then we have a problem
		if(this->int_sortMap() == true)
		{
			throw PGL_EXCEPT_LOGIC("The Nodes, the graph provided were not sorted.");
		};

		return;
	};


	/**
	 * \brief	This resizes the map, associated it with the graph Graph and also sets the content.
	 *
	 * The value of mapped type, is taken from the presented range.
	 * The range is to be assumed ordered with respect to the vertex id.
	 * More precisly. it is assumed that the order of the range is the smae as the order of iteration of the graph.nodes
	 */
	template<class ForwardIt>
	inline
	void
	resize(
		const pgl_graph_t& 	Graph,
		ForwardIt 		start,
		ForwardIt 		ende)
	{
		const Size_t n  = Graph.nVertices();
		const Size_t nR = std::distance(start, ende);

		if(n != nR)
		{
			throw PGL_EXCEPT_OutOfBound("The range has not the same length as the vertices.");
		};

		//Clear map if we have one
		if(m_container.size() != 0)
		{
			m_container.clear();
		};

		//Allocate space
		m_container.reserve(n + 1);

		auto Node = Graph.CNodeBegin();
		for(Size_t it = 0; it != n; ++it)
		{
			//Construct the node type
			m_container.emplace_back(Node->getID(), *start);

			++Node;
			++start;
		}; //End get the Node ID

		pgl_assert(Node == Graph.CNodeEnd());
		pgl_assert(n == m_container.size());

		//If a sorting was neccessary, then we have a problem
		if(this->int_sortMap() == true)
		{
			throw PGL_EXCEPT_LOGIC("The Nodes, the graph provided were not sorted.");
		};

		return;
	};

	/**
	 * \brief	Associated the map with the presented graph.
	 *
	 * This will first delete the content of this.
	 * and then reconstruct it.
	 * The values of the mapped types are default
	 */
	inline
	void
	resize(
		const pgl_graph_t& Graph)
	{
		this->resize(Graph, MappedType());

		return;
	};



	/**
	 * \brief	Resize the container
	 *
	 * This function should not be used anymore, since the numbering of the nodes could become an issue.
	 * The new value is also set
	 *
	 * \param newSize 	The new size of this
	 */
	inline
	void
	resize(
		const pgl_graph_vertexMap_consecutive_numbering_tag tag,
		const size_type newSize,
		const MappedType& 	newValue)
	{
		//Clear map if we have one
		if(m_container.size() != 0)
		{
			m_container.clear();
		};

		//Allocate space
		m_container.reserve(newSize + 1);

		for(VIDBase_t d = 0; d != newSize; ++d)
		{
			m_container.emplace_back(VertexID_t(d, true), newValue);
		};

		return;
		(void)tag;
	};


	inline
	void
	resize(
		const pgl_graph_vertexMap_consecutive_numbering_tag tag,
		const size_type newSize)
	{
		this->resize(tag, newSize, MappedType());

		return;
	};


	/**
	 * \brief	This function allows to associate *this with the same graph as otherMap.
	 *
	 * This function is basically a hack.
	 * It solves a very special provblem.
	 * Say you want to create a map, but you do not have the graph, so you can not associate *this with the graph.
	 * But you have a map, that is already associated with the graph you want, but this map has another type.
	 * So this function will load only the numbering from otherMap.
	 *
	 * This function is only possible if *this has size zero, if this is not fullfilled, an exception is generated.
	 *
	 * \param 	otherMap 	The other map we load the numbering from
	 */
	template<typename T1, typename T2>
	void
	load_from_other_map(
		const pgl_vertexMap_t<T1, T2>& otherMap) noexcept(false)
	{
		using OtherMap_t = pgl_vertexMap_t<T1, T2>;
		using OIT_t = typename OtherMap_t::const_iterator;

		if(m_container.size() != 0)
		{
			throw PGL_EXCEPT_LOGIC("This function can only be called on empty maps");
		};

		//Get the size of the other container
		const Size_t n = otherMap.size();

		//Reserve space
		m_container.reserve(n + 1);


		//Now load the mapping
		const  OIT_t ende = otherMap.cend();
		for(OIT_t it = otherMap.cbegin(); it != ende; ++it)
		{
			m_container.emplace_back(it.key(), MappedType());
		}; //End for(it): load the mapping


		//Test if sorted if not there is a problem
		if(this->int_sortMap() == true)
		{
			throw PGL_EXCEPT_LOGIC("The map we have loaded from was not sorted.");
		};


		return;
	};


	/**
	 * \brief	Clear the content of this and sets the size to zero.
	 *
	 * In addition to this the capacity is also reduced to zero
	 */
	inline
	void
	clear()
	{
		container_type(0).swap(m_container);
		return;
	};


	/**
	 * \brief	Reserve space.
	 *
	 * This function is present for compatibility reasons and does nothing
	 */
	inline
	void
	reserve(
		const size_type 	spaceToReserve)
	{
		return;
		(void)spaceToReserve;
	};


	/**
	 * \brief	Return the capacity of this
	 */
	inline
	void
	capacity() const noexcept
	{
		return m_container.capacity();
	};




	/*
	 * ===================================
	 * Special functions
	 */
public:

	/**
	 * \brief	Allow normal swaping
	 */
	void
	swap(
		pgl_vertexMap_t& other)
	{
		other.m_container.swap(this->m_container);

		return;
	};


	/**
	 * \brief	This function returns true, if the map stores an value of ID id.
	 *
	 * If the id is invalid, then this is not considered as an error, but as not present.
	 */
	inline
	bool
	containID(
		const VertexID_t& id) const noexcept
	{
		if(id.isInvalid() == true)
		{
			return false;
		};

		const auto i = id.getID();
		if(m_container.size() < id.getID() && m_container[i].first == id)
		{
			return true;
		}
		else
		{
			const auto it = std::lower_bound(m_container.begin(), m_container.end(), (VIDBase_t)i, [](const Node_t& lhs, const VIDBase_t& rhs){ return lhs.first < rhs;});

			if(id == m_container.end())
			{
				return false;
			}
			else
			{
				return it->first == id ? true : false;
			};
		}; //End else: not shortcut
	}; //End function: contains ID



	/*
	 * ================
	 * Private functions
	 */
private:
	/**
	 * \brief	Tis function sorts the internal representation of the map.
	 *
	 * This function first checks if a sorting is needed.
	 * It returns true if a sorting was performed and false otherwise
	 */
	inline
	bool
	int_sortMap()
	{
		const auto Less = [](const Node_t& lhs, const Node_t& rhs){ return lhs.first < rhs.first ? true : false;};

		const ::pgl::retValueUniqueSorted_e s = pgl::is_sorted_unique(m_container.cbegin(), m_container.cend(), Less);

		switch(s)
		{
		  case ::pgl::retValueUniqueSorted_e::nonUnique:
			throw PGL_EXCEPT_LOGIC("The range was not unique");
			break;

		  case ::pgl::retValueUniqueSorted_e::notSorted:
		  	//The range is not sorted, so we break and sort it.
		  	break;

		  case ::pgl::retValueUniqueSorted_e::sortedAndUnique:
		  	//Sorted and unique, so we can exit, now
		  	return false;

		  default:
		  	throw PGL_EXCEPT_RUNTIME("What the hell is going on.");
		  	break;
		}; //End switch

		//The range is not sorted, so we must sort it
		std::sort(m_container.begin(), m_container.end(), Less);

		//Test if sorted and unique.
		//Only in the assert
		pgl_assert(pgl::is_sorted_unique(m_container.cbegin(), m_container.cend(), Less) == ::pgl::retValueUniqueSorted_e::sortedAndUnique);

		//A sorting was performed, so we return true
		return true;
	};





	/*
	 * ================
	 * Private Variables
	 */
private:
	container_type 		m_container;	//!< This is the container used to store things
}; //End class(pgl_vertexMap_t)



PGL_NS_END(graph)
PGL_NS_END(pgl)




