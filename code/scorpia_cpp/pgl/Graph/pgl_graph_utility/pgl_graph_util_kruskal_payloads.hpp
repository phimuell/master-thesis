#pragma once
#ifndef PGL_GRAPH__PGL_GRAPH_UTILITY__KRUSKAL_PAYLOAD_HPP
#define PGL_GRAPH__PGL_GRAPH_UTILITY__KRUSKAL_PAYLOAD_HPP
/**
 * \file	Graph/pgl_graph_utility/pgl_graph_util_kruskal_payload.hpp
 * \brief	This file implements the payload that is needed for the kruskal MST algo
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_connection_payload.hpp>



//Include STD


PGL_NS_START(pgl)
PGL_NS_START(graph)




/**
 * \brief 	This class implements the payload that kruskal needs
 * \class	pgl_kruskal_connPL_t
 */
class pgl_kruskal_connPL_t : public pgl_connection_payload_t
{
	/*
	 * ====================
	 * Constructors
	 */
public:
	///Delete default constructor
	pgl_kruskal_connPL_t();

	///Defaulted copy constructor
	pgl_kruskal_connPL_t(
		const pgl_kruskal_connPL_t&);

	///Defaulted moveconstructor
	pgl_kruskal_connPL_t(
		pgl_kruskal_connPL_t&&);

	///Defaulted copy assignment
	pgl_kruskal_connPL_t&
	operator= (
		const pgl_kruskal_connPL_t&);

	//Defaulted move assignment
	pgl_kruskal_connPL_t&
	operator= (
		pgl_kruskal_connPL_t&&);

	///The destructor is declared virtual
	virtual ~pgl_kruskal_connPL_t();

	/**
	 * \brief 	This is the build constructor
	 *
	 * \param weight	The weight of this
	 */
	pgl_kruskal_connPL_t(
		const Weight_t&	weitht);

	/*
	 * ==================================
	 * Kruskal functions
	 *
	 * This are the funtions that ruskal needs
	 */


	/**
	 * \brief 	Retruns true if *this is inside the MST
	 */
	virtual
	bool
	isMatched() const override;


	/**
	 * \brief 	Make *this to a part in the MST
	 *
	 * The returnvalue has no meaning
	 */
	virtual
	bool
	makeMatched() override;

	/**
	 * \brief 	Make *this not part in the MST
	 */
	virtual
	bool
	makeUnmatched() override;



	/**
	 * \brief	Returns the weight of *this
	 */
	virtual
	Weight_t
	getWeight() const override;



	/*
	 * =======================
	 * Private memebrs
	 */
protected:

	Weight_t 		m_connWeight;		//!< This is the Weight of the connection
	bool 			m_isMST;		//!< Indicates if inside the MST
}; //End class(pgl_kruskal_connPL_t)




PGL_NS_END(graph)
PGL_NS_END(pgl)


#endif 	//End include guard
