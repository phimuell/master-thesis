#pragma once
/**
 * \file	pgl/Graph/pgl_graph_utility/pgl_graph_tree_path_map.hpp
 * \brief	This is a map for the structure in a trouted tree.
 *
 * It is used by the ballmann-ford algorithm returning the found path.
 * It is possible to quesy which is the next from the current node.
 *
 * The datastructure is read only.
 */

#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph_utility/pgl_graph_vertex_map.hpp>


PGL_NS_START(pgl)
PGL_NS_START(graph)



/**
 * \class 	pgl_treePathMap_t
 * \brief	Express the struicture of a tree
 *
 * This class can express a tree path from all node to a single node, called root.
 * This class is an imutable, in a sense
 */
class pgl_treePathMap_t
{
	/*
	 * ===============
	 * Some typedefs
	 */
public:
	using VertexID_t 		= ::pgl::graph::pgl_vertexID_t;		//!< Type for using the vertxID
	using Container_t 		= ::pgl::graph::pgl_vertexMap_t<VertexID_t>;	//!< The internal container
	using value_type 		= VertexID_t;
	using reference 		= const VertexID_t&;			//!< Reference type
	using size_type 		= ::pgl::Size_t; 			//!< Size type


	/*
	 * ================
	 * Constructors
	 */
public:

	/**
	 * \brief
	 */
	inline
	pgl_treePathMap_t() noexcept = default;

	/**
	 * \brief 	Copy constructor is defaulted
	 */
	inline
	pgl_treePathMap_t(
		const pgl_treePathMap_t&) noexcept(false) = default;

	/**
	 * \brief	Copy assignment is defauulted too
	 */
	inline
	pgl_treePathMap_t&
	operator= (
		const pgl_treePathMap_t&) noexcept(false) = default;


	/**
	 * \brief	Move constructor is defaulted
	 */
	inline
	pgl_treePathMap_t(
		pgl_treePathMap_t&&) noexcept = default;


	/**
	 * \brief	Move assignment is defaulted
	 */
	inline
	pgl_treePathMap_t&
	operator= (
		pgl_treePathMap_t&&) noexcept = default;


	/**
	 * \brief	Defaulted destructor
	 */
	inline
	~pgl_treePathMap_t() noexcept = default;


	/**
	 * \brief	This constructor builds themap from the map
	 */
	inline
	pgl_treePathMap_t(
		Container_t mapp) noexcept(false) :
	  m_map(std::move(mapp))
	{
		/*
		 * Test if the the range was valid
		 */
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The supplied range was not valid");
		};
	};


	/**
	 * \brief	Buildfs the path from a range, must be a forward iterator
	 */
	template<class ForvardIterator_t>
	pgl_treePathMap_t(
		const pgl_graph_t& 	Graph,
		ForvardIterator_t 	start,
		ForvardIterator_t 	ende) noexcept(false) :
	  m_map(Graph, start, ende)
	{
		/*
		 * Test if the the range was valid
		 */
		if(this->isValid() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The supplied range was not valid");
		};
	};


	/*
	 * ====================================
	 * Accesser function
	 */
public:


	/**
	 * \brief	This function returns the next node that leads to the root
	 *
	 * The underling container throws if an out of bound exception occured
	 *
	 * \param[in] 	currentNode 	The current node
	 */
	inline
	VertexID_t
	nextNode(
		const VertexID_t& currentNode) const noexcept(false)
	{
		return m_map.at(currentNode);
	};


	/**
	 * \brief	This function returns the next node that leads to the node.
	 *
	 * This function tests with an assert.
	 *
	 * \param[in] 	currentNode
	 */
	inline
	VertexID_t
	operator[] (
		const VertexID_t& currentNode) const noexcept(false)
	{
		pgl_assert(currentNode.getID() < m_map.size());
		return m_map[currentNode];
	};

	inline
	VertexID_t
	at(
		const VertexID_t& currentNode) const noexcept(false)
	{
		return m_map.at(currentNode);
	};


	/**
	 * \brief	This function returns true, if current node is the root.
	 *
	 * The route is defined as, the node that points to itself
	 */
	inline
	bool
	isRoot(
		const VertexID_t& currentNode) const noexcept(false)
	{
		return (m_map.at(currentNode) == currentNode) ? true : false;
	};


	/**
	 * \brief	This function searches the node.
	 *
	 * This function is costly.
	 * If the route was not found the the invalid ID is returned.
	 * It is the responsibility of the user to ensure proper handling.
	 */
	inline
	VertexID_t
	searchRoot() const noexcept(false)
	{
		const Size_t n = m_map.size();
		for(Size_t it = 0; it != n; ++it)
		{
			if(m_map.at(it).getID() == it)
			{
				return m_map[it];
			};
		};

		return VertexID_t::INVALID();
	};


	/*
	 * ======================================
	 * Utility function
	 */
public:

	/**
	 * \brief	This function tests if the structure is valid and returns true
	 */
	inline
	bool
	isValid() const
	{
		bool foundRoot = false;	//Indicating if a route was found
		const Size_t n = m_map.size();

		for(Size_t it = 0; it != n; ++it)
		{
			//Test if the current is valid
			if(m_map.at(it).isValid() == false)
			{
				//The ID is not valid, so we return false
				return false;
			}; //End if: test if valid

			// Test if it is the root
			if(m_map.at(it).getID() == it)
			{
				//It is the root, test if we have found one before
				if(foundRoot == true)
				{
					//We have two roots, so we return false
					pgl_assert(foundRoot == false);
					return false;
				}
				else
				{
					//The roote was not known, so we found one
					foundRoot = true;
				};
			}; //End if: test if one root
		}; //End for(it): iterate through all the members

		//We must have found a root
		pgl_assert(foundRoot == true);

		return foundRoot;
	};




	/*
	 * =============
	 * Private member variables
	 */
private:
	Container_t 			m_map;		//!< The map
}; //End class(pgl_treePathMap_t)




PGL_NS_END(graph)
PGL_NS_END(pgl)








