#pragma once
/**
 * \file	pgl/Graph/pgl_graph_utility/pgl_graph_tree_description.hpp
 * \brief	This is a description of a routed tree.
 *
 * It is able to provide naviagation iside the tree.
 * It is a mixture of a 'pgl_treePathMap_t' and a vertex map.
 * In addition to the parent relation it also stores the distance to the root.
 *
 * This container is a read only container.
 * It must be created and then it can only be readed.
 * Althought it supports assignemnts.
 *
 * There is a version of the pair that is used to store everything.
 *
 */

#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_config.hpp>

#include <pgl_graph_utility/pgl_graph_vertex_map.hpp>


PGL_NS_START(pgl)
PGL_NS_START(graph)

class pgl_treeDescription_t;

PGL_NS_START(internal)
/**
 * \class 	pgl_treeDescriptionNode_t
 * \brief	This is a helper class for the treeDescription.
 *
 * It is intendend to be used like a pair.
 */
struct pgl_treeDescriptionNode_t
{
	/*
	 * =======================
	 * Typedef
	 */
public:
	using Predecessor_t 	= ::pgl::graph::pgl_vertexID_t;		//!< Type that is used to descripes the father of the node
	using Distance_t 	= ::pgl::graph::Distance_t;			//!< This is the type to describes the distance to the ROOT
	static constexpr Distance_t InfDistance = ::pgl::graph::Constants::DistanceInfinity; //This is infinity and invalid
	using Pair_t 		= ::std::pair<Predecessor_t, Distance_t>;	//!< This is the layout if it would be a pair

	/*
	 * ========================
	 * Constructors
	 */
	inline
	pgl_treeDescriptionNode_t(
		const Predecessor_t pred,
		const Distance_t    dist) noexcept :
	  predecessor(pred),
	  rDistance(dist)
	{};

	inline
	pgl_treeDescriptionNode_t() noexcept :
	  predecessor(Predecessor_t::INVALID()),
	  rDistance(InfDistance)
	{};

	inline
	pgl_treeDescriptionNode_t(
		const pgl_treeDescriptionNode_t&) noexcept = default;

	inline
	pgl_treeDescriptionNode_t(
		pgl_treeDescriptionNode_t&&) noexcept = default;

	inline
	pgl_treeDescriptionNode_t&
	operator= (
		const pgl_treeDescriptionNode_t&) noexcept = default;

	inline
	pgl_treeDescriptionNode_t&
	operator= (
		pgl_treeDescriptionNode_t&&) noexcept = default;

	inline
	~pgl_treeDescriptionNode_t() noexcept = default;


	inline
	operator Pair_t() const noexcept
	{
		return std::make_pair(predecessor, rDistance);
	};

	/**
	 * \brief	Retunrs the distance to the root form this node.
	 */
	inline
	Distance_t
	getDistance() const noexcept
	{
		return rDistance;
	};

	/**
	 * \brief	Returns the predecessor of the node
	 */
	inline
	Predecessor_t
	getPredecessor() const noexcept
	{
		return predecessor;
	};

	/*
	 * ===================
	 * Test functions
	 */
	inline
	bool
	isValid() const noexcept
	{
		return (rDistance != InfDistance) && (predecessor.isValid());
	};

	friend
	class ::pgl::graph::pgl_treeDescription_t;

	/*
	 * ======================
	 * Variables
	 * They are public
	 */
private:
	Predecessor_t 		predecessor; 	//!< The predecessor of this
	Distance_t 		rDistance;	//!< The disance to the root


}; //End pgl_treeDescriptionNode_t


PGL_NS_END(internal)




/**
 * \class 	pgl_treeDescription_t
 * \brief	Express the struicture of a tree
 *
 * This class can express a tree path from all node to a single node, called root.
 * This class is an imutable, in a sense
 */
class pgl_treeDescription_t
{
	/*
	 * ===============
	 * Some typedefs
	 */
public:
	using VertexID_t 		= ::pgl::graph::pgl_vertexID_t;		//!< Type for using the vertxID
	using node_type 		= ::pgl::graph::internal::pgl_treeDescriptionNode_t;	//!< This is the type that is used to map enything
	using Predecessor_t 		= node_type::Predecessor_t;		//!< The type of the predecessor
	using Distance_t 		= node_type::Distance_t;		//!< Type for the distance
	using Container_t 		= ::pgl::graph::pgl_vertexMap_t<node_type>;	//!< The internal container
	using predecessorMap_t 		= ::pgl::graph::pgl_vertexMap_t<VertexID_t>;	//!< A map that stores the predecessor relationship
	using distanceMap_t 		= ::pgl::graph::pgl_vertexMap_t<Distance_t>;	//!< The map that stores the distance
	using value_type 		= node_type;				//!< This is the type that is used to map enything
	using reference 		= const value_type&; 			//!< Reference
	using const_reference 		= const value_type&;
	using size_type 		= ::pgl::Size_t; 			//!< Size type
	using iterator 			= Container_t::const_iterator;		//!< The iterator
	using const_iterator 		= Container_t::const_iterator;


	/*
	 * ================
	 * Constructors
	 */
public:

	/**
	 * \brief
	 */
	inline
	pgl_treeDescription_t() noexcept = default;

	/**
	 * \brief 	Copy constructor is defaulted
	 */
	inline
	pgl_treeDescription_t(
		const pgl_treeDescription_t&) noexcept(false) = default;

	/**
	 * \brief	Copy assignment is defauulted too
	 */
	inline
	pgl_treeDescription_t&
	operator= (
		const pgl_treeDescription_t&) noexcept(false) = default;


	/**
	 * \brief	Move constructor is defaulted
	 */
	inline
	pgl_treeDescription_t(
		pgl_treeDescription_t&&) noexcept = default;


	/**
	 * \brief	Move assignment is defaulted
	 */
	inline
	pgl_treeDescription_t&
	operator= (
		pgl_treeDescription_t&&) noexcept = default;


	/**
	 * \brief	Defaulted destructor
	 */
	inline
	~pgl_treeDescription_t() noexcept = default;


	/**
	 * \brief	This constructor takes a graph and two vertexMap and construct this.
	 *
	 * \param 	Graph 		The graph we want to associate with
	 * \param 	predessecorMap 	The map that stores the predecessor relationship
	 * \param 	distanceMap 	The map that storews the distances
	 */
	pgl_treeDescription_t(
		const pgl_graph_t& 		Graph,
		const predecessorMap_t& 	predeMap,
		const distanceMap_t& 		distanceMap) :
	  m_map(Graph)
	{
		const auto nG = Graph.nVertices();
		const auto nP = predeMap.size();
		const auto nD = distanceMap.size();

		if(!((nG == nP) && (nP == nD)))
		{
			throw PGL_EXCEPT_LOGIC("The ranges are not equal.");
		};

		for(const pgl_vertex_t& v : Graph.CNodes())
		{
			const VertexID_t currID = v.getID();

			//allocate everything
			m_map.at(currID) = node_type(predeMap.at(currID), distanceMap.at(currID));
		}; //End for(v): iterate over the nodes

		if(this->Verify() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The map is not valid.");
		};
	};


	/**
	 * \brief	This constructor construct the desctiption soly from the maps
	 *
	 * \param 	predessecorMap 	The map that stores the predecessor relationship
	 * \param 	distanceMap 	The map that storews the distances
	 */
	pgl_treeDescription_t(
		const predecessorMap_t& 	predeMap,
		const distanceMap_t& 		distanceMap) :
	  m_map()
	{
		const auto nP = predeMap.size();
		const auto nD = distanceMap.size();

		if(nP != nD)
		{
			throw PGL_EXCEPT_LOGIC("The ranges are not equal.");
		};

		//Construct the mapping
		m_map.load_from_other_map(predeMap);

		const auto ende = m_map.end();
		for(auto it = m_map.begin(); it != ende; ++it)
		{
			const VertexID_t currID = it.key();

			//allocate everything
			m_map.at(currID) = node_type(predeMap.at(currID), distanceMap.at(currID));
		}; //End for(v): iterate over the nodes

		if(this->Verify() == false)
		{
			throw PGL_EXCEPT_RUNTIME("The map is not valid.");
		};
	};





	/*
	 * ====================================
	 * Accesser function
	 */
public:

	/**
	 * \brief	Get the number of elements stored in here
	 */
	inline
	size_type
	size() const noexcept
	{
		return m_map.size();
	};


	/**
	 * \brief	Returns true is, the description is not empty.
	 *
	 * A description is not empty, if its size is larger than zero.
	 */
	inline
	bool
	isAssociated() const noexcept
	{
		return m_map.size() != 0 ? true : false;
	};


	/**
	 * \brief	Retruns true, if this is empty
	 *
	 * A container is empty if its size is zero.
	 * Also note, that an empty description is not associated.
	 */
	inline
	bool
	isEmpty() const noexcept
	{
		return m_map.size() == 0 ? true : false;
	};


	/**
	 * \brief	Get access to the node witch belongs to the vertex with id
	 *
	 * This function throws if it is out of range or invalid id.
	 *
	 * \param id 		The Id of the node we want to query
	 */
	inline
	const_reference
	at(
		const VertexID_t& 	id) const
	{
		return m_map.at(id);
	};


	/**
	 * \brief	This functin retirns the node that is associated with ID zero
	 *
	 * If PGL_NDEBUG is not set, there are checkes iwth asserts
	 */
	inline
	const_reference
	operator[](
		const VertexID_t& id) const
	{
		pgl_assert(id.isValid());
		return m_map[id];
	};



	/**
	 * \brief	Return the distance of id to the root
	 *
	 * If PGL_NDEBUG is not defined then some checkes are performed.
	 */
	inline
	Distance_t
	getDistanceOf(
		const VertexID_t& id) const
	{
		pgl_assert(id.isValid());
		return m_map.at(id).rDistance;
	};


	/**
	 * \brief	Get predecessor of id.
	 *
	 * If PGL_NDEBUG is not defined some checks are performed
	 */
	inline
	Predecessor_t
	getPredecessorOf(
		const VertexID_t& id) const
	{
		pgl_assert(id.isValid());
		return m_map.at(id).predecessor;
	};


	inline
	const_iterator
	begin() const noexcept
	{
		return m_map.cbegin();
	};

	inline
	const_iterator
	cbegin() const noexcept
	{
		return m_map.cbegin();
	};

	inline
	const_iterator
	end() const noexcept
	{
		return m_map.cend();
	};

	inline
	const_iterator
	cend() const noexcept
	{
		return m_map.cend();
	};



	/*
	 * ======================
	 * Higer functions
	 */
public:

	/**
	 * \brief	This is a verify function.
	 *
	 * It returns true if everything is considered good otherwhise false
	 */
	inline
	bool
	Verify() const
	{
		/*
		 * Das problem mit dieser Funktion ist, dass der algorithmus nicht fertig sein muss.
		 * Man kann also nodes haben ohne distanz und sonst was."
		 *
		 * Man muss diese Funktion nochmals anschauen.
		 */
		return true;
#if 0
		int howManyRoots = 0;
		const auto ende = m_map.cend();
		for(const_iterator it = m_map.cbegin(); it != ende; ++it)
		{
			const node_type& Node = *it;

			if(Node.isValid() == false)
			{
				return false;
			};

			//TEst if predecessor is present
			if(m_map.containID(Node.predecessor) == false)
			{
				return false;
			};

			//Is node the root
			if(Node.predecessor == it.key())
			{
				if(howManyRoots != 0)
				{
					//We have more than one root
					return false;
				};

				howManyRoots += 1;
			};

		}; //End for: it

		return true;
#endif
	};


	/**
	 * \brief	Retruns true if id is the root
	 *
	 * There are no restrictions inposed on id
	 */
	inline
	bool
	isRoot(
		const VertexID_t id) const noexcept
	{
		if(id.isInvalid() )
		{
			return false;
		};

		if(id.getID() >= m_map.size())
		{
			return false;
		};

		return (m_map[id].predecessor == id) ? true : false;
	};



	/*
	 * ======================
	 * Private member
	 */
private:
	Container_t 		m_map;		//!< This is the variable that stores the map
}; //End class(pgl_treeDescription_t)




PGL_NS_END(graph)
PGL_NS_END(pgl)








