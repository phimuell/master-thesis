
//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>
#include <pgl_int.hpp>

#include <pgl/pgl_aglorithm.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_graphtag.hpp>
#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>
#include <pgl_graph_builder/pgl_biTreeBuilder.hpp>

#include <pgl_vector.hpp>


// Inculde std
#include <utility>



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


//This is an internal namespace, that contains some usefuill functions for this builder
namespace
{

	/**
	 * \brief	This fucntion returns the ID of the children of the argument
	 */
	inline
	std::pair<Size_t, Size_t>
	get_children_of(
		Size_t parent)
	{
		parent += 1;	//We add one because the description is easier, if we start counting at 1 instead of zero

		//We have made the transformation, for staring with one, so we must substract one from it again
		return ::std::make_pair((2 * parent) - 1, (2 * parent + 1) - 1);
	};


	/**
	 * \brief	This function finds the way up, to the root.
	 *
	 * The root is allways at the top (get it by calling back()) of the vector
	 */
	pgl_vector_t<Size_t>
	internal_findWayToRoot(
		Size_t 		currentVertex)
	{
		//First we must add one again
		currentVertex += 1;

		//This is the way we take upwards
		pgl_vector_t<Size_t> wayUpwards;

		//Here we make use of the fact of integer division,
		//if we would be fully perfect, we must subtract one, from the ID if it is odd
		//But this will be taken care of by the integer division
		while(currentVertex != 1)	//1 is the root
		{
			//The vertex is not the rout, so we must add it to the way up
			wayUpwards.push_back(currentVertex - 1);	//We subtract one for the consistence with the other world

			//Go upwards
			currentVertex /= 2;

			if(currentVertex == 0)
			{
				throw PGL_EXCEPT_RUNTIME("Error, we reached zero");
			};
		}; //End while


		//Add the root
		wayUpwards.emplace_back(0);	//Here we use 0, for the root, like our outerwold convention

		pgl_assert(::pgl::is_unique(wayUpwards.cbegin(), wayUpwards.cend() ));


		//Return the vector
		return wayUpwards;
	}; //End find way to root




}; //End annonymous namespace


//This function calculates the distance in a fully binary tree
Distance_t
pgl_getFullBiTreeDistance(
	const pgl_vertexID_t& 	rhs,
	const pgl_vertexID_t&	lhs)
{
	//Handle the case, bith are equal
	if(rhs == lhs)
	{
		return Distance_t(0);
	};

	//Handle the case one is the root
	if(rhs.getID() == 0 || lhs.getID() == 0)
	{
		//Then we must only calculate one way
		//The returned vectors contains all nodes, between the node and the non root, but this is not equivalent to the distance.
		//Because we need the number of connections, and this number is one less.
		return Distance_t(internal_findWayToRoot(rhs.getID() == 0 ? lhs.getID() : rhs.getID()).size() - 1);
	};


	//First we determine the way upwards of the two nodes
	auto wayOne = internal_findWayToRoot(rhs.getID());		//Both contains at least two ids
	auto wayTwo = internal_findWayToRoot(lhs.getID());


	/*
	 * Now we go and search the place, were they different, the end of the vector are equal until that place
	 *
	 * Don't forget to add one, because we removed the last comon node.
	 */
	while(wayOne.back() == wayTwo.back())
	{
		//Remove the end
		wayOne.pop_back();
		wayTwo.pop_back();

		//Test if one is empty
		if(wayOne.empty() || wayTwo.empty())
		{
			break;
		};
	};

	//Both must not be empty at the same time, this would mean they are the same
	if(wayOne.empty() && wayTwo.empty())
	{
		throw PGL_EXCEPT_RUNTIME("Both are empty.");
	};

	/*
	 * now we have removed all nodes, that are the same in the two ways.
	 * That means that the distance between them is the number of connections in each way, for a description see above.
	 * Plus 2, This bluss two is neccessary, because, we need to go the the common root, and then go to the other way.
	 */
	//return Distance_t(Distance_t(wayOne.size() - 1) + Distance_t(wayTwo.size() - 1) + 2);
	return Distance_t(wayOne.size() + wayTwo.size());	// Is equivalent, and will not suffer from type conversion rules
};




/**
 * \brief	The building constructor.
 *
 * This constructor takes a hight and makes a tree.
 * The number of nodes $N$ are $N = 2^n - 1$.
 */
pgl_biTreeBuilder_t::pgl_biTreeBuilder_t(
	const Size_t 		hight) :
  m_hight(hight),
  m_nodes(),
  m_conns()
{

};

/**
 * \brief	The destructor
 *
 * This is defaulted but defined in the cpp file.
 */
pgl_biTreeBuilder_t::~pgl_biTreeBuilder_t() = default;




void
pgl_biTreeBuilder_t::createBuilder()
{
	if(!m_nodes.empty())
	{
		//there is allready a description, so we do nothing
		return;
	};


	//Now we start in building
	const size_type nNodes = (2 << (m_hight - 1)) - 1;	//Total number of nodes

	//Reserve storage for the description
	m_nodes.reserve(nNodes);
	m_conns.reserve(nNodes);


	for(size_type currentID = 0; currentID != nNodes; ++currentID)
	{
		//Create the node
		m_nodes.emplace_back(pgl_vertexID_t(currentID, true), GraphTag::Vertex, nullptr);

		//Create the children, if needed
		size_type left, right;
		std::tie(left, right) = get_children_of(currentID);

		//Only add if the childeren are not outside the range
		//The left is allways the smaller one
		if(left < nNodes)
		{
			//The node is inside the range, so we must add them.
			const GraphTag TAG = GraphTag::Undirected | GraphTag::Edge | GraphTag::Connection;

			//Add left node
			m_conns.emplace_back(pgl_vertexID_t(currentID, true), pgl_vertexID_t(left, true), TAG, nullptr);

			//Add right one
			m_conns.emplace_back(pgl_vertexID_t(currentID, true), pgl_vertexID_t(right, true), TAG, nullptr);
		};
	}; //End for(currentID): filling the description.


	//Make a test.
	if(m_nodes.size() - 1 != m_conns.size())
	{
		throw PGL_EXCEPT_RUNTIME("No tree.");
	};


	return;
}; //End createBuilder


/**
 * \brief	This function clears the builder
 *
 * This will clear the description, by calling the clear fucntion of the underlying containers.
 *
 */
void
pgl_biTreeBuilder_t::clearBuilder()
{
	return;

	this->m_nodes.clear();
	this->m_nodes.shrink_to_fit();

	this->m_conns.clear();
	this->m_conns.shrink_to_fit();

	return;
};



pgl_biTreeBuilder_t::size_type
pgl_biTreeBuilder_t::nVertices() const noexcept
{
	return m_nodes.size();
};



pgl_biTreeBuilder_t::size_type
pgl_biTreeBuilder_t::nConnections() const noexcept
{
	return m_conns.size();
};


pgl_biTreeBuilder_t::NodeDescriptor_t
pgl_biTreeBuilder_t::getNode(
	const size_type 	i) noexcept(false)
{
	return m_nodes.at(i);
};


pgl_biTreeBuilder_t::ConnDescribtor_t
pgl_biTreeBuilder_t::getConnection(
	const size_type 	i) noexcept(false)
{
	return m_conns.at(i);
};



PGL_NS_END(graph)
PGL_NS_END(pgl)



