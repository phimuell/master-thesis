#pragma once
#ifndef GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_DNA_BF_HPP
#define GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_DNA_BF_HPP
/**
 * \file 	Graph/pgl_graph_builder/pgl_graph_builder_dna_bf.hpp
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>


#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_vector.hpp>

#include <pgl_graph_builder/pgl_graph_builder_node_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_edge_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_dna_base.hpp>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)




class pgl_gBuilder_dna_bf_t : public pgl_gBuilder_dna_base_t
{
	/*
	 * =======================
	 * Typedefs
	 */
public:
	using size_type			= ::pgl::Size_t;						//!< The size type used
	using NodeDescriptor_t 		= ::pgl::graph::internal::pgl_abstract_nodeDescriber_t;		//!< The class that describes a node
	using ConnDescribtor_t		= ::pgl::graph::internal::pgl_abstract_connDescriber_t;		//!< The class that describes a connection

private:
	using NodeContainer_t 		= Vector_t<NodeDescriptor_t>;					//!< Type for storing the nodes
	using ConnContainer_t		= Vector_t<ConnDescribtor_t>;					//!< Type for storing the connections


	/*
	 * ==========================
	 * Constructor
	 */
public:

	/**
	 * \brief 	Construct a builder
	 *
	 * The format of the string is a description of the graph G = (V, E) in the format:
	 * 	n, m, v_1, u_2, c_1, ..., v_m, u_m, c_m.
	 * Where n = |V|, m = |E|. and {v_i, u_i} is an edge with weight c_i.
	 * But there are no commatas.
	 */
	pgl_gBuilder_dna_bf_t(
		const std::string& Description);


	virtual
	~pgl_gBuilder_dna_bf_t();




	/*
	 * ===========================================================
	 * Hooks use them or die
	 */
protected:

	/**
	 * \brief	This function returns a edge Payload.
	 *
	 * \param c 	It takes an int as parameter
	 */
	virtual
	pgl_connection_payload_t*
	prot_buildEdgePayload(
		const Int_t c) const override;

	/**
	 * \brief	This function returns a vertex payload
	 *
	 * \param c	An integer is provided
	 */
	virtual
	pgl_vertex_payload_t*
	prot_buildVertexPayload(
		const Int_t c) const override;


	/**
	 * \brief	This function reurns the status of an edge
	 */
	virtual
	GraphTag
	prot_getEdgeTag() const override;

}; //End class(pgl_abstarct_gBuilder)

PGL_NS_END(graph)
PGL_NS_END(pgl)



#endif // End include guard
