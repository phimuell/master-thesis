/**
 * \file 	Graph/pgl_graph_builder/pgl_graph_builder_dna_base.cpp
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_int.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_graph_builder/pgl_graph_builder_node_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_edge_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_dna_base.hpp>


//Include STD
#include <sstream>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)



pgl_gBuilder_dna_base_t::~pgl_gBuilder_dna_base_t() = default;




/**
 * \brief 	Construct a builder
 *
 * The format of the string is a description of the graph G = (V, E) in the format:
 * 	n, m, v_1, u_2, c_1, ..., v_m, u_m, c_m.
 * Where n = |V|, m = |E|. and {v_i, u_i} is an edge with weight c_i.
 * Without the cmmas
 */
pgl_gBuilder_dna_base_t::pgl_gBuilder_dna_base_t(
    const std::string& Description) :
  pgl_abstract_gBuilder_t()
, m_description(Description)
, m_wasRun(false)
{};



void
pgl_gBuilder_dna_base_t::clearBuilder()
{
	m_wasRun = true;
	m_nodes.clear();
	m_conn.clear();
	return;
};



void
pgl_gBuilder_dna_base_t::createBuilder()
{
	//If the function was allready run, return imediatly.
	if(m_wasRun == true)
	{
		throw PGL_EXCEPT_LOGIC("This builder shall only be used once.");
	};



	std::stringstream s(m_description);

	Int_t nNodes_;	//Numer of nodes
	Int_t nConn_;	//Numner of connections

	s >> nNodes_;
	s >> nConn_;
		pgl_assert(nNodes_ > 0);
		pgl_assert(nConn_ > 0);

	const uInt_t nNodes = nNodes_;
	const uInt_t nConn  = nConn_;

	m_conn.reserve(nConn);
	m_nodes.reserve(nNodes);


	//Reating all the connections that are needed
	for (uInt_t nodeID = 0; nodeID != nNodes; ++nodeID)
	{
		//Adding all the nodes
		this->m_nodes.emplace_back(pgl_vertexID_t(nodeID, true), GraphTag::Vertex, this->prot_buildVertexPayload(0) );
		pgl_assert(this->m_nodes.back().isValid());
	};

	pgl_assert(m_nodes.size() == nNodes);


	uInt_t Inserted_conns = 0;
	uInt_t ignored_conns = 0;
	for (uInt_t it = 0; it != nConn; ++it)
	{
		Int_t startNode = -1;	//Where the connection starts
		Int_t endNode   = -1;  //Where the connection ends
		Weight_t connWei   = -1;  //Weight of the connection

		//reding them
		s >> startNode >> endNode >> connWei;

		//IF loop one is detected, ignore it
		if(startNode == endNode)
		{
			++ignored_conns;
			continue;
		};

		Inserted_conns++;

		//Now we must reduce the id by one, to get the coorect one
		startNode -= 1; pgl_assert(startNode >= 0);
		endNode -= 1; pgl_assert(endNode >= 0);

		//pgl_connection_payload_t* builder = nullptr;
		//builder = new pgl_kruskal_connPL_t();

		this->m_conn.emplace_back(this->m_nodes.at(startNode).node_id, this->m_nodes.at(endNode).node_id,
				this->prot_getEdgeTag(),
				this->prot_buildEdgePayload(connWei));
	}; //end for(it): reading all the stuff


	pgl_assert((ignored_conns + Inserted_conns) == nConn);
	pgl_assert(m_conn.size() == (Size_t)Inserted_conns);
}; //End constructor




/**
 * \brief	Retruns the numbers of nodes
 *
 * This fucntion returns the number of nodes, that the graph has.
 */
pgl_abstract_gBuilder_t::size_type
pgl_gBuilder_dna_base_t::nVertices() const noexcept
{
	return m_nodes.size();
};



/**
 * \brief 	Returns the numbers of edges in the graph
 *
 * This function returns the numbers of edges in the graph.
 */
pgl_abstract_gBuilder_t::size_type
pgl_gBuilder_dna_base_t::nConnections() const noexcept
{
	return this->m_conn.size();
};




/**
 * \brief 	returns the i-th edge descriptor
 *
 * The descriptor is returned by value, this makes it possible to create them on demand.
 * Also an exception is thrown, if a vertex is requested, which does not exist.
 *
 * The counting starts at 0 and goes up to nVertices -1.
 * i and node_id must not be the same, they can be different.
 */
pgl_gBuilder_dna_base_t::NodeDescriptor_t
pgl_gBuilder_dna_base_t::getNode(
    const size_type 	i) noexcept(false)
{
	return this->m_nodes.at(i);
};




/**
 * \brief	Returns the i-th edge descriptor
 *
 *
 * The descriptor is returned by value.
 * An exception is thrown, if the edge number is not existing.
 *
 * The counting starts at 0 and goes up to nConnection -1.
 * Xou don't query the builder with the real edge ID, you use a consequitive key for that.
 */
pgl_gBuilder_dna_base_t::ConnDescribtor_t
pgl_gBuilder_dna_base_t::getConnection(
    const size_type 	i) noexcept(false)
{
	return this->m_conn.at(i);
};


pgl_vertex_payload_t*
pgl_gBuilder_dna_base_t::prot_buildVertexPayload(
	const Int_t c) const
{
	(void)c;
	return nullptr;
};


pgl_connection_payload_t*
pgl_gBuilder_dna_base_t::prot_buildEdgePayload(
	const Int_t c) const
{
	(void)c;
	return nullptr;
};




GraphTag
pgl_gBuilder_dna_base_t::prot_getEdgeTag() const
{
	return GraphTag::Connection | GraphTag::Edge | GraphTag::Undirected;
};




PGL_NS_END(graph)
PGL_NS_END(pgl)



