/**
 * \file 	Graph/pgl_graph_builder/pgl_graph_builder_dna_undirected.cpp
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>


#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_vector.hpp>

#include <pgl_graph_builder/pgl_graph_builder_node_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_edge_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>
#include <pgl_graph_builder/pgl_graph_builder_dna_undirected.hpp>
#include <pgl_graph_utility/pgl_graph_util_kruskal_payloads.hpp>


//Include STD
#include <sstream>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)



pgl_gBuilder_kruskal_t::~pgl_gBuilder_kruskal_t() = default;


pgl_gBuilder_kruskal_t::pgl_gBuilder_kruskal_t(
    const std::string& Description) :
  pgl_gBuilder_dna_base_t(Description)
{
}; //End constructor



pgl_connection_payload_t*
pgl_gBuilder_kruskal_t::prot_buildEdgePayload(
	const Int_t c) const
{
	return new pgl_kruskal_connPL_t(c);
};



PGL_NS_END(graph)
PGL_NS_END(pgl)



