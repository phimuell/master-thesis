//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>


#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_vector.hpp>

#include <pgl_graph_builder/pgl_graph_builder_node_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_edge_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_dna_base.hpp>
#include <pgl_graph_builder/pgl_graph_builder_dna_bf.hpp>

#include <pgl_graph_utility/pgl_graph_util_bellman_payload.hpp>

PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)

pgl_gBuilder_dna_bf_t::pgl_gBuilder_dna_bf_t(
		const std::string& Description) :
 	pgl_gBuilder_dna_base_t(Description)
{};


pgl_gBuilder_dna_bf_t::~pgl_gBuilder_dna_bf_t() = default;


pgl_connection_payload_t*
pgl_gBuilder_dna_bf_t::prot_buildEdgePayload(
		const Int_t c) const
{
	return new pgl_bf_edgePayload_t(c);
};

pgl_vertex_payload_t*
pgl_gBuilder_dna_bf_t::prot_buildVertexPayload(
		const Int_t c) const
{
	return new pgl_bf_nodePayload_t(c);
};

GraphTag
pgl_gBuilder_dna_bf_t::prot_getEdgeTag() const
{
	return GraphTag::Edge | GraphTag::Connection | GraphTag::Directed;
};



PGL_NS_END(graph)
PGL_NS_END(pgl)



