#pragma once
#ifndef GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_CONNECTION_DESCRIPTOR_HPP
#define GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_CONNECTION_DESCRIPTOR_HPP
/**
 * \file 	Graph/pgl_graph_builder/pgl_graph_builder_edge_descriptor.hpp
 * \brief	A class that describes a connection
 *
 * This file describes a calss that gives a very simple description of a connection.
 * This is used by the abstarct builder.
 *
 * This class is implemented inside te internal namespace of graph.
 * The user is expected to use the typedef provided by the builder interface.
 *
 * It declares all public.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>

#include <pgl_graph/pgl_graph_graphtag.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph/pgl_graph_edgeID.hpp>



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)



PGL_NS_BEGIN(internal)

/**
 * \class 	pgl_abstarct_connDescriber_t
 * \brief	This class describes one connection
 *
 * The description of a vertex is done via two node ids and a graph tag.
 * Additionaly the this also conatinas a raw-pointer to the connection payload.
 * No memeory managment is needed.
 */
struct pgl_abstract_connDescriber_t
{
	pgl_vertexID_t		src_id;
	pgl_vertexID_t 		dest_id;
	GraphTag		kindOfConn;
	pgl_connection_payload_t*	payload_ptr;



	/*
	 * =====================
	 * Some functions
	 */


	/**
	 * \brief 	Returns true, if the id is valid
	 */
	inline
	bool
	isValid() const noexcept
	{
		return this->src_id.isValid() && this->dest_id.isValid() && GraphTag_isEdge(kindOfConn);
	};

	/**
	 * \brief 	This functions builds an edgeID, based on the informations this has
	 */
	inline
	pgl_edgeID_t
	make_edge_id() const noexcept
	{
		return pgl_edgeID_t(src_id, dest_id,
			GraphTag::Edge | GraphTag::Connection | (GraphTag_isDirectedEdge(kindOfConn) == true ? GraphTag::Directed : GraphTag::Undirected)
			);
	};



	/**
	 * \brief	Default constructor
	 *
	 * Is the not the defaulted one
	 */
	inline
	pgl_abstract_connDescriber_t():
	    src_id()
	  , dest_id()
	  , kindOfConn(static_cast<GraphTag>(0))
	  , payload_ptr(nullptr)
	{};


	/**
	 * \brief	CopyConstructor
	 *
	 * Is the default one
	 */
	inline
	pgl_abstract_connDescriber_t(
		const pgl_abstract_connDescriber_t&) = default;

	/**
	 * \brief 	MoveConstructor
	 *
	 */
	inline
	pgl_abstract_connDescriber_t(
		pgl_abstract_connDescriber_t&&) = default;


	/**
	 * \brief	CopyAssignment
	 *
	 * Is the default one
	 */
	inline
	pgl_abstract_connDescriber_t&
	operator= (
		const pgl_abstract_connDescriber_t&) = default;


	/**
	 * \brief	Move Assignment
	 *
	 * Is also deleted
	 */
	inline
	pgl_abstract_connDescriber_t&
	operator= (
		pgl_abstract_connDescriber_t&&) = default;


	/**
	 * \brief 	Destructor
	 *
	 * Is also the delete one, because we don't need any memeory managment capabilities.
	 */
	inline
	~pgl_abstract_connDescriber_t() = default;


	/**
	 * \brief a builder constructor
	 *
	 * \param src		The ID that should represent the src of a directed connection.
	 * \param dest 		The ID of the node, that should represent the dest
	 * \param kindOfConn	The GraphTag that describes the edge
	 * \param payload	The payload of the graph defaulted to null
	 */
	inline
	pgl_abstract_connDescriber_t(
		const pgl_vertexID_t& 	src,
		const pgl_vertexID_t&	dest,
		const GraphTag 		Tag,
		pgl_connection_payload_t* 	payload = nullptr) :
	    src_id(src)
	  , dest_id(dest)
	  , kindOfConn(Tag)
	  , payload_ptr(payload)
	{
		pgl_assert(this->isValid() );
	};
}; //End class(pgl_abstract_connDescriber_t)



PGL_NS_END(internal)




PGL_NS_END(graph)
PGL_NS_END(pgl)



#endif // End include guard
