#pragma once
#ifndef GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_INTERFACE_HPP
#define GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_INTERFACE_HPP
/**
 * \file 	Graph/pgl_graph_builder/pgl_graph_builder_interface.hpp
 * \brief	The interfaace to the graph builder
 *
 * This file defines an abstract (virtual) interface for a graph builder.
 * The user can build concrete classes for them, to adapt, for different, situations.
 *
 * In fact they are relativ generall, thought, they don't offer iterators, but an at function.
 *
 *
 *
 * The process of using this function is, that a builder is passed to the graph constructor.
 * The graph will then do the folowing things:
 * 	> Call the "create" function (was also called run function), this shall build a description of the graph.
 * 	> Afterward the graph will use the Builder to build itself. F
 * 		First by iterating through the nodes and then through the edges.
 * 	> After that it will pass itself to the "postprcessing" function.
 * 	> After that it will call the "cleanBuilder" function of the builder.
 *
 * The call to clean builder is neccessary, since the enteties of the graph aquired exclusive ownership of the payloads.
 * So the payload that the builder stores, ar not valid anymore.
 * If tey are no payloads involved then, the clear function is allowed to do nothing.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>

#include <pgl_graph_builder/pgl_graph_builder_node_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_edge_descriptor.hpp>


//Include STD
#include <utility>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)




/**
 * \class 	pgl_abstarct_gBuilder_t
 * \brief	The abstract  graph builder
 */
class pgl_abstract_gBuilder_t
{
	/*
	 * =======================
	 * Typedefs
	 */
public:
	using size_type			= ::pgl::Size_t;						//!< The size type used
	using NodeDescriptor_t 		= ::pgl::graph::internal::pgl_abstract_nodeDescriber_t;		//!< The class that describes a node
	using ConnDescribtor_t		= ::pgl::graph::internal::pgl_abstract_connDescriber_t;		//!< The class that describes a connection
	using NodeDeg_t 		= ::std::pair<Size_t, Size_t>;					//!< This is used to describe the degree status of a vertex


	/*
	 * ==============================
	 * Konstructors
	 */
public:
	virtual
	~pgl_abstract_gBuilder_t() = 0;


protected:

	pgl_abstract_gBuilder_t();

	pgl_abstract_gBuilder_t(
		const pgl_abstract_gBuilder_t&) noexcept;

	pgl_abstract_gBuilder_t(
		pgl_abstract_gBuilder_t&&) noexcept;

	pgl_abstract_gBuilder_t&
	operator= (
		const pgl_abstract_gBuilder_t&) noexcept;

	pgl_abstract_gBuilder_t&
	operator= (
		pgl_abstract_gBuilder_t&&) noexcept;


	/*
	 * ===========================
	 * BUILDING
	 */
public:
	/**
	 * \brief	This function create function, that sets up the actual builder the actual builder proccess.
	 *
	 * This function is neccessary, because in the constructor, the vTable is not enabled yet.
	 * It has to be called after the constructor has run.
	 *
	 * It is also used to recreate the payloads after a graph was constructed.
	 *
	 * If this function is called twice, tis shall have no effect.
	 */
	virtual
	void
	createBuilder() = 0;


	/**
	 * \brief	This function is an alias of createBuilder.
	 *
	 * This function is provided for backward copability, and can not be overwritten
	 * it calls the creatBuilder function
	 */
	virtual
	void
	run() final;


	/**
	 * \brief 	This function is for post processing.
	 *
	 * This function performs a post processing of the graph.
	 * This must be called by the user.
	 *
	 * this funtion has a default implementation, which does nothing.
	 */
	virtual
	void
	postProcessing(
		pgl_graph_t& 	graph);


	/**
	 * \brief	This function clears the builder
	 *
	 * It is used to inform the builder, that the graph has constructed itself.
	 * This is neccessary, since the graph has aquired uniqque ownership of the payloads.
	 * So they can't be used to build a new graph, in generall.
	 *
	 * The graph calls this function at the very end of the construction phase.
	 *
	 * This function is allowed to do nothing, if the payloads are not used
	 */
	virtual
	void
	clearBuilder() = 0;



	/*
	 * ============================
	 * Queries
	 */

	/**
	 * \brief	Retruns the numbers of nodes
	 *
	 * This fucntion returns the number of nodes, that the graph has.
	 */
	virtual
	size_type
	nVertices() const noexcept = 0;



	/**
	 * \brief 	Returns the numbers of edges in the graph
	 *
	 * This function returns the numbers of edges in the graph.
	 */
	virtual
	size_type
	nConnections() const noexcept = 0;


	/**
	 * \brief 	Returns true if the graph is explicit
	 *
	 * This function is only here for completeness.
	 * It has a default, true.
	 * It is used to say that the graph is explicitly stored.
	 */
	virtual
	bool
	isExplicit() const noexcept;



	/*
	 * ===============================
	 * Get access to the descriptors
	 */

	/**
	 * \brief 	returns the i-th edge descriptor
	 *
	 * The descriptor is returned by value, this makes it possible to create them on demand.
	 * Also an exception is thrown, if a vertex is requested, which does not exist.
	 *
	 * The counting starts at 0 and goes up to nVertices -1.
	 * i and node_id must not be the same, they can be different.
	 */
	virtual
	NodeDescriptor_t
	getNode(
		const size_type 	i) noexcept(false) = 0;


	/**
	 * \brief	Informas th builder that the i-th node was builded successfully.
	 *
	 * After the graph created the i-th node successfully, the node has aquired ownership of the pointer of the payload.
	 * To aviod memeory leak, it is recomended that the destructure of a concrete builder cleas up.
	 * To aviod corruption and deleting stuff that is still needed, this function can be used.
	 * It informs the buider, that it now the node is respronsible for managing the payload.
	 *
	 * There is no default implementation for that function.
	 * These is to reminde to user to made one.
	 *
	 * \param i 	Which node is now free
	 */
	virtual
	void
	completedNode(
		const size_type 	i) noexcept(false) = 0;


	// ==== CONNECTION


	/**
	 * \brief	This function returns the number of edges, that are involved with the edge.
	 *
	 * This function is mainly implemented that the graph can make use of the reserve feature of the vertex.
	 * It returns a pair with the following meaning:
	 * 	- first: 	Number of incoming edges
	 * 	- second:	Numebr of outgoing edges
	 *
	 * Per default this function returns zero, for both, this will be interpreted by the
	 * graph as not known and it will do nothing with it.
	 */
	virtual
	NodeDeg_t
	getNodeDegree(
		const size_type 	i) noexcept(false);




	/**
	 * \brief	Returns the i-th edge descriptor
	 *
	 *
	 * The descriptor is returned by value.
	 * An exception is thrown, if the edge number is not existing.
	 *
	 * The counting starts at 0 and goes up to nConnection -1.
	 * Xou don't query the builder with the real edge ID, you use a consequitive key for that.
	 */
	virtual
	ConnDescribtor_t
	getConnection(
		const size_type 	i) noexcept(false) = 0;


	/**
	 * \brief	Informs the builder that the connection has aquired ownership of the payload.
	 *
	 * This function does the same as the 'completedNode' but for the connections.
	 * It informas the builder that it is no longer responsible for managing the memory of the i-th payload.
	 *
	 * There is no default implementation.
	 * This is a reminder to the user to implement one.
	 */
	virtual
	void
	completedConnection(
		const size_type 	i) noexcept(false) = 0;



}; //End class(pgl_abstarct_gBuilder)

PGL_NS_END(graph)
PGL_NS_END(pgl)



#endif // End include guard
