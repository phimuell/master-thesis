#pragma once
#ifndef GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_NODE_DESCRIPTOR_HPP
#define GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_NODE_DESCRIPTOR_HPP
/**
 * \file 	Graph/pgl_graph_builder/pgl_graph_builder_node_descriptor.hpp
 * \brief	A class that describes a node
 *
 * This file describes a calss that gives a very simple description of the node.
 * This is used by the abstarct builder.
 *
 * This class is implemented inside te internal namespace of graph.
 * The user is expected to use the typedef provided by the builder interface.
 *
 * It declares all public.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>

#include <pgl_graph/pgl_graph_graphtag.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)



PGL_NS_BEGIN(internal)

/**
 * \class 	pgl_abstract_nodeDescriber_t
 * \brief	This class describes one node
 *
 * This class is used to describe one single node.
 * The graph itself will use it to determine the property of one single node.
 * The payload is a raw pointer.
 * There is no memeory managment needed, because if it does not goes inside the graph, there are serious trubles.
 */
struct pgl_abstract_nodeDescriber_t
{
	pgl_vertexID_t		node_id;
	GraphTag		kindOfNode;
	pgl_vertex_payload_t*	payload_ptr;




	/*
	 * =====================
	 * Some functions
	 */


	/**
	 * \brief 	Returns true, if the id is valid
	 */
	inline
	bool
	isValid() const noexcept
	{
		return this->node_id.isValid() && GraphTag_isVertex(kindOfNode);
	};



	/**
	 * \brief	Default constructor
	 *
	 * Is the not the defaulted one
	 */
	inline
	pgl_abstract_nodeDescriber_t():
	    node_id()
	  , kindOfNode(static_cast<GraphTag>(0))
	  , payload_ptr(nullptr)
	{};


	/**
	 * \brief	CopyConstructor
	 *
	 * Is the default one
	 */
	inline
	pgl_abstract_nodeDescriber_t(
		const pgl_abstract_nodeDescriber_t&) = default;

	/**
	 * \brief 	MoveConstructor
	 *
	 */
	inline
	pgl_abstract_nodeDescriber_t(
		pgl_abstract_nodeDescriber_t&&) = default;


	/**
	 * \brief	CopyAssignment
	 *
	 * Is the default one
	 */
	inline
	pgl_abstract_nodeDescriber_t&
	operator= (
		const pgl_abstract_nodeDescriber_t&) = default;


	/**
	 * \brief	Move Assignment
	 *
	 * Is also deleted
	 */
	inline
	pgl_abstract_nodeDescriber_t&
	operator= (
		pgl_abstract_nodeDescriber_t&&) = default;


	/**
	 * \brief 	Destructor
	 *
	 * Is also the delete one, because we don't need any memeory managment capabilities.
	 */
	inline
	~pgl_abstract_nodeDescriber_t() = default;


	/**
	 * \brief a builder constructor
	 *
	 * \param ID		The ID that the node should have
	 * \param kindOfNode	The GraphTag that describes the node
	 * \param payload	The payload of the graph defaulted to null
	 */
	inline
	pgl_abstract_nodeDescriber_t(
		const pgl_vertexID_t& 	id,
		const GraphTag 		Tag,
		pgl_vertex_payload_t* 	payload):
	    node_id(id)
	  , kindOfNode(Tag)
	  , payload_ptr(payload)
	{
		pgl_assert(this->isValid() );
	};
}; //End class(pgl_abstract_nodeDescriber_t)



PGL_NS_END(internal)




PGL_NS_END(graph)
PGL_NS_END(pgl)



#endif // End include guard
