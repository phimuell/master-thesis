#pragma once
/**
 * \file 	Graph/pgl_graph_builder/pgl_biTreeBuilder.hpp
 * \brief	This is a builder for a fully balanced binary tree.
 *
 * This builder, is able to build a full binary tree.
 * This means that each node has two ancesstors, except the leafs.
 *
 * In a tree of hight n, there are $2^n - 1$ nodes.
 *
 * The connections are undirected.
 * The root has ID 0.
 * The parrent is the ID with the higher ID than the node.
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>

#include <pgl_vector.hpp>



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


/**
 * \class 	pgl_biTreeBuilder_t
 * \brief	This class builds a fully binary tree.
 */
class pgl_biTreeBuilder_t : public pgl_abstract_gBuilder_t
{
	/*
	 * =======================
	 * Typedefs
	 */
public:
	using size_type			= ::pgl::Size_t;						//!< The size type used
	using Super_t 			= pgl_abstract_gBuilder_t;					//!< The baseclass of this
	using NodeDescriptor_t 		= Super_t::NodeDescriptor_t;					//!< The class that describes a node
	using ConnDescribtor_t		= Super_t::ConnDescribtor_t;					//!< The class that describes a connection
	using NodeContainer_t 		= ::pgl::pgl_vector_t<NodeDescriptor_t>;			//!< This is the container for storing all the nodes
	using ConnContainer_t 		= ::pgl::pgl_vector_t<ConnDescribtor_t>;			//!< This is the conatiner for storing the connections


	/*
	 * ============================
	 * Constructors
	 *
	 * The only constructor that is accessable is the constructor that uses a hight
	 * and the destructor
	 */
public:

	/**
	 * \brief	The building constructor.
	 *
	 * This constructor takes a hight and makes a tree.
	 * The number of nodes $N$ are $N = 2^n - 1$.
	 */
	pgl_biTreeBuilder_t(
		const Size_t 		hight);

	/**
	 * \brief	The destructor
	 *
	 * This is defaulted but defined in the cpp file.
	 */
	~pgl_biTreeBuilder_t();


	/*
	 * The other constructors are deleted.
	 */
	pgl_biTreeBuilder_t(
		const pgl_biTreeBuilder_t&) = delete;

	pgl_biTreeBuilder_t(
		pgl_biTreeBuilder_t&&) = delete;

	pgl_biTreeBuilder_t&
	operator= (
		const pgl_biTreeBuilder_t&) = delete;

	pgl_biTreeBuilder_t&
	operator= (
		pgl_biTreeBuilder_t&&) = delete;



	/*
	 * ===========================
	 * BUILDING
	 */
public:
	/**
	 * \brief	This function create function, that sets up the actual builder the actual builder proccess.
	 *
	 * This function is neccessary, because in the constructor, the vTable is not enabled yet.
	 * It has to be called after the constructor has run.
	 *
	 * It is also used to recreate the payloads after a graph was constructed.
	 *
	 * If this function is called twice, tis shall have no effect.
	 */
	virtual
	void
	createBuilder() override;



#if 0
	// We use the default
	/**
	 * \brief 	This function is for post processing.
	 *
	 * This function performs a post processing of the graph.
	 * This must be called by the user.
	 *
	 * this funtion has a default implementation, which does nothing.
	 */
	virtual
	void
	postProcessing(
		pgl_graph_t& 	graph);
#endif


	/**
	 * \brief	This function clears the builder
	 *
	 * This will clear the description, by calling the clear fucntion of the underlying containers.
	 *
	 */
	virtual
	void
	clearBuilder() override;



	/*
	 * ============================
	 * Queries
	 */

	/**
	 * \brief	Retruns the numbers of nodes
	 *
	 * This fucntion returns the number of nodes, that the graph has.
	 */
	virtual
	size_type
	nVertices() const noexcept override;



	/**
	 * \brief 	Returns the numbers of edges in the graph
	 *
	 * This function returns the numbers of edges in the graph.
	 */
	virtual
	size_type
	nConnections() const noexcept override;


#if 0
	/**
	 * \brief 	Returns true if the graph is explicit
	 *
	 * This function is only here for completeness.
	 * It has a default, true.
	 * It is used to say that the graph is explicitly stored.
	 */
	virtual
	bool
	isExplicit() const noexcept;
#endif



	/*
	 * ===============================
	 * Get access to the descriptors
	 */

	/**
	 * \brief 	returns the i-th edge descriptor
	 *
	 * The descriptor is returned by value, this makes it possible to create them on demand.
	 * Also an exception is thrown, if a vertex is requested, which does not exist.
	 *
	 * The counting starts at 0 and goes up to nVertices -1.
	 * i and node_id must not be the same, they can be different.
	 */
	virtual
	NodeDescriptor_t
	getNode(
		const size_type 	i) noexcept(false) override;




	/**
	 * \brief	Returns the i-th edge descriptor
	 *
	 *
	 * The descriptor is returned by value.
	 * An exception is thrown, if the edge number is not existing.
	 *
	 * The counting starts at 0 and goes up to nConnection -1.
	 * Xou don't query the builder with the real edge ID, you use a consequitive key for that.
	 */
	virtual
	ConnDescribtor_t
	getConnection(
		const size_type 	i) noexcept(false) override;


	/*
	 * ====================================
	 * Private Member variable
	 */
private:

	size_type 				m_hight;		//!< The hight of the tree
	NodeContainer_t 			m_nodes;		//!< The conatiner that stores the node
	ConnContainer_t 			m_conns;		//!< The container for storing the connections
}; //End class(pgl_biTreeBuilder_t)


/**
 * \brief	This fucntion returns the distances between two nodes in a full binary tree.
 *
 * This only works in a binary tree, this is done, by determining the parrent of each of them, until a common node is found.
 */
Distance_t
pgl_getFullBiTreeDistance(
	const pgl_vertexID_t& 	rhs,
	const pgl_vertexID_t&	lhs);






PGL_NS_END(graph)
PGL_NS_END(pgl)



