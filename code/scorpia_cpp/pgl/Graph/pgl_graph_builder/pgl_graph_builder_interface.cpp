//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)

bool
pgl_abstract_gBuilder_t::isExplicit() const noexcept
{
	return true;
};



void
pgl_abstract_gBuilder_t::run()
{
	this->createBuilder();
	return;
};


void
pgl_abstract_gBuilder_t::postProcessing(
	pgl_graph_t& 	graph)
{
	(void)graph;

	return;
};


pgl_abstract_gBuilder_t::NodeDeg_t
pgl_abstract_gBuilder_t::getNodeDegree(
	const size_type 	i) noexcept(false)
{
	if(!(i < this->nVertices()))
	{
		throw PGL_EXCEPT_OutOfBound("Tried to get degree information about a vertex, that does not exoist.");
	};

	return NodeDeg_t(0, 0);
};


pgl_abstract_gBuilder_t::pgl_abstract_gBuilder_t() = default;


pgl_abstract_gBuilder_t::~pgl_abstract_gBuilder_t() = default;

pgl_abstract_gBuilder_t::pgl_abstract_gBuilder_t(
	const pgl_abstract_gBuilder_t&) noexcept = default;

pgl_abstract_gBuilder_t::pgl_abstract_gBuilder_t(
	pgl_abstract_gBuilder_t&&) noexcept = default;

pgl_abstract_gBuilder_t&
pgl_abstract_gBuilder_t::operator= (
	const pgl_abstract_gBuilder_t&) noexcept = default;

pgl_abstract_gBuilder_t&
pgl_abstract_gBuilder_t::operator= (
	pgl_abstract_gBuilder_t&&) noexcept = default;




PGL_NS_END(graph)
PGL_NS_END(pgl)

