#pragma once
#ifndef GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_DNA_BASE_HPP
#define GRAPH__PGL_GRAPH_BUILDER__PGL_GRAPH_BUILDER_DNA_BASE_HPP
/**
 * \file 	Graph/pgl_graph_builder/pgl_graph_builder_dna_base.hpp
 * \brief	This implements a builder, that can read one line in the dna format
 * 		for undirected weighted graphs, i.e (Kruskal)
 *
 * This file implements a base class for the dna builder
 *
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>

#include <pgl_vector.hpp>

#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_graph_builder/pgl_graph_builder_node_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_edge_descriptor.hpp>
#include <pgl_graph_builder/pgl_graph_builder_interface.hpp>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)




/**
 * \class 	pgl_gBuilder_dna_base_t
 * \brief	Builds graphs from krskal
 */
class pgl_gBuilder_dna_base_t : public pgl_abstract_gBuilder_t
{
	/*
	 * =======================
	 * Typedefs
	 */
public:
	using size_type			= ::pgl::Size_t;						//!< The size type used
	using NodeDescriptor_t 		= ::pgl::graph::internal::pgl_abstract_nodeDescriber_t;		//!< The class that describes a node
	using ConnDescribtor_t		= ::pgl::graph::internal::pgl_abstract_connDescriber_t;		//!< The class that describes a connection

private:
	using NodeContainer_t 		= Vector_t<NodeDescriptor_t>;					//!< Type for storing the nodes
	using ConnContainer_t		= Vector_t<ConnDescribtor_t>;					//!< Type for storing the connections


	/*
	 * ==========================
	 * Constructor
	 */
public:

	/**
	 * \brief 	Construct a builder
	 *
	 * The format of the string is a description of the graph G = (V, E) in the format:
	 * 	n, m, v_1, u_2, c_1, ..., v_m, u_m, c_m.
	 * Where n = |V|, m = |E|. and {v_i, u_i} is an edge with weight c_i.
	 * But there are no commatas.
	 */
	pgl_gBuilder_dna_base_t(
		const std::string& Description);


	~pgl_gBuilder_dna_base_t();



	/**
	 * \brief	This function runns the actual builder proccess.
	 *
	 * This function is neccessary, because in the constructor, the vTable is not enabled yet.
	 */
	virtual
	void
	createBuilder() final;


	/**
	 * \brief	This function throws an exception. But it is not neccesary.
	 */
	virtual
	void
	clearBuilder() final;



	/*
	 * ============================
	 * Queries
	 */
public:
	/**
	 * \brief	Retruns the numbers of nodes
	 *
	 * This fucntion returns the number of nodes, that the graph has.
	 */
	virtual
	size_type
	nVertices() const noexcept final;



	/**
	 * \brief 	Returns the numbers of edges in the graph
	 *
	 * This function returns the numbers of edges in the graph.
	 */
	size_type
	nConnections() const noexcept final;



	/*
	 * ===============================
	 * Get access to the descriptors
	 */

	/**
	 * \brief 	returns the i-th edge descriptor
	 *
	 * The descriptor is returned by value, this makes it possible to create them on demand.
	 * Also an exception is thrown, if a vertex is requested, which does not exist.
	 *
	 * The counting starts at 0 and goes up to nVertices -1.
	 * i and node_id must not be the same, they can be different.
	 */
	NodeDescriptor_t
	getNode(
		const size_type 	i) noexcept(false) final;




	/**
	 * \brief	Returns the i-th edge descriptor
	 *
	 *
	 * The descriptor is returned by value.
	 * An exception is thrown, if the edge number is not existing.
	 *
	 * The counting starts at 0 and goes up to nConnection -1.
	 * Xou don't query the builder with the real edge ID, you use a consequitive key for that.
	 */
	ConnDescribtor_t
	getConnection(
		const size_type 	i) noexcept(false) final;




	/*
	 * ===========================================================
	 * Hooks use them or die
	 */
protected:

	/**
	 * \brief	This function returns a edge Payload.
	 *
	 * \param c 	It takes an int as parameter
	 */
	virtual
	pgl_connection_payload_t*
	prot_buildEdgePayload(
		const Int_t c) const;

	/**
	 * \brief	This function returns a vertex payload
	 *
	 * \param c	An integer is provided
	 */
	virtual
	pgl_vertex_payload_t*
	prot_buildVertexPayload(
		const Int_t c) const;


	/**
	 * \brief	This function reurns the status of an edge
	 */
	virtual
	GraphTag
	prot_getEdgeTag() const;



	/*
	 * ====================================
	 * Private Memers
	 */
public:
	std::string 			m_description;
	NodeContainer_t 		m_nodes;	//!< Storing the nodes
	ConnContainer_t 		m_conn;		//!< Storing the connections
	bool 				m_wasRun;	//!< Stores the information, if the run function was allready called
}; //End class(pgl_abstarct_gBuilder)

PGL_NS_END(graph)
PGL_NS_END(pgl)



#endif // End include guard
