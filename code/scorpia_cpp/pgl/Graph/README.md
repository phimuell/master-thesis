# Intention of this folder

This folder is introduced to seperate some parts of the graph.
The graph has its core components (Graph, Vertex, Connection and Edges).
Then there are also other components like the algorithms, that are used.
And as a last part the utilities, that we need.

I don't like to put them all in the same folder, so I decided to split up things.
The organization of the folder is as folows:
	- pgl_graph:
	Contains the core components of the graph

	- pgl_graph_build:
	Classes and functions that are intended to be used as builder.

	- pgl_graph_algo:
	The graph related algorithms.

	- pgl_graph_util:
	Utility functions and classes for the graph, that could be usefull.
	Here we also stores the implementations for concrete payloads.
