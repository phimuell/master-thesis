#pragma once
#ifndef GRAPH__PGL_GRAPH_ALGO__PGL_GRAPH_ALGO_BASE_HPP
#define GRAPH__PGL_GRAPH_ALGO__PGL_GRAPH_ALGO_BASE_HPP
/**
 * \file 	pgl_graph_algo/pgl_graph_algo_base.hpp
 *
 * \brief	This file implements a base class for all algorithm.
 *
 * It uses the CRTP It requires that the algorithm implements three function:
 * All of thoese functions are run by the Base::run() function:
 *
 * - algo_pree_impl:
 *   This function is run befor thee algorithm, the intended use is to set up some things.
 *   The return value, if any is ignored.
 *
 * - algo_impl:
 *   This is the actual algorithm, that is run.
 *   The return value, if any is ignored.
 *
 * - algo_post_impl:
 *   This function performs some postprocessing.
 *   It has to return a bool.
 *   This bool shall indicate, if the computation was successfull.
 *
 * The base also provides a function that indicates if the algoritm was success.
 */


//Include PGL
#include <pgl_core/pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>

#include <pgl_graph_algo/pgl_graph_pglAlgoCommand_e.hpp>
#include <pgl_core/pgl_exception.hpp>


//Include STD



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)




/**
 * \class 	pgl_graphAlgoBase
 * \brief 	A base for all algorithms.
 *
 * This is a base for all algorithm.
 * It uses CRTP.
 *
 * It also checks if the algorithm was run, when destroyed.
 * If this is not the case. an assertion is called.
 *
 * This can de disabled by compiling with PGL_NDEBUG
 *
 * \tparam concreteAlgorithmImpl	The class that implements a concrete algorithm
 */
template<class concreteAlgorithmImpl>
class pgl_graphAlgoBase
{
	/*
	 * ========================
	 */
public:
	/**
	 * \brief	Defaultconstructor sufices.
	 */
	pgl_graphAlgoBase() noexcept :
	  _ALGOBASE_wasSuccess(false),
	  _ALGOBASE_wasRun(false)
	{};

	/**
	 * \brief	The destructor assertes the condition that the algorithm was run
	 */
	~pgl_graphAlgoBase()
	{
		pgl_assert(_ALGOBASE_wasRun == true);
	};


	/**
	 * \brief	Copy constructor.
	 *
	 * Defaulted.
	 */
	pgl_graphAlgoBase(
		const pgl_graphAlgoBase&)
	 noexcept
	 = default;


	/**
	 * \brief	Move constructor.
	 *
	 * Defaulted.
	 */
	pgl_graphAlgoBase(
		pgl_graphAlgoBase&&)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignemnt
	 */
	pgl_graphAlgoBase&
	operator= (
		const pgl_graphAlgoBase&)
	 noexcept
	 = default;


	/**
	 * \brief	Move assignemnt
	 */
	pgl_graphAlgoBase&
	operator= (
		pgl_graphAlgoBase&&)
	 noexcept
	 = default;



	/*
	 * ============================================
	 * Typedefs
	 */
public:
	//Vertex
	using Vertex_t 			= ::pgl::graph::pgl_vertex_t;			//!< This is the type of the vertex
	using Vertex_ptr 		= ::pgl::graph::pgl_vertex_t*;			//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= const ::pgl::graph::pgl_vertex_t*;		//!< This is the pointer to a constant vertex
	using Vertex_ref		= ::pgl::graph::pgl_vertex_t&;			//!< This is the type of a reference to a node
	using Vertex_cref		= const ::pgl::graph::pgl_vertex_t&;		//!< This is the constant reference to a vertex
	using VertexPayLoad_ptr		= ::pgl::graph::pgl_vertex_payload_t*;		//!< This is the payload of the vertex
	using VertexID_t 		= ::pgl::graph::pgl_vertexID_t;			//!< This is the type of the vertex ID

	//Connection
	using Connection_t 		= ::pgl::graph::pgl_connection_t;		//!< Type of the connection
	using Connection_ptr		= pgl_connection_t*;				//!< Pointer to a connection
	using Connection_cptr		= const pgl_connection_t*;			//!< Pointer to a constant connection
	using Connection_ref 		= pgl_connection_t&;				//!< Reference to connection
	using Connection_cref		= const pgl_connection_t&;			//!< Const reference to connection
	using ConnPayLoad_ptr		= ::pgl::graph::pgl_connection_payload_t*;	//!< Pointer to a vertex payload
	using ConnectionID_t 		= ::pgl::graph::pgl_edgeID_t;			//!< This is the ID that connections and edges uses

	//Edge
	using Edge_t 			= ::pgl::graph::pgl_edge_t; 			//!< Type of the edge
	using Edge_ptr			= pgl_edge_t*;					//!< Pointer to a edge
	using Edge_cptr			= const pgl_edge_t*;				//!< Pointer to a constant edge
	using Edge_ref 			= pgl_edge_t&;					//!< Reference to edge
	using Edge_cref			= const pgl_edge_t&;				//!< Const reference to edge
	using EdgeID_t 		= ::pgl::graph::pgl_edgeID_t;			//!< This is the ID that connections and edges uses

	//Graph
	using Graph_t			= ::pgl::graph::pgl_graph_t;			//!< The used graph type
	using Graph_ref 		= ::pgl::graph::pgl_graph_t&;			//!< Reference to the graph
	using Graph_cref		= const ::pgl::graph::pgl_graph_t&; 		//!< Const referecne to the graph
	using Graph_ptr 		= ::pgl::graph::pgl_graph_t*;			//!< pointer to the graph
	using Graph_cptr		= const ::pgl::graph::pgl_graph_t*; 		//!< Const pointer to the graph

	//Algo typedefs
	using Super_t 			= pgl_graphAlgoBase<concreteAlgorithmImpl>;	//!< Type of the base
	using Algo_t 			= concreteAlgorithmImpl;			//!< Type fo the concrete algorithm


	/**
	 * \brief 	This function performs the actuall run of the algorithm
	 *
	 * How this is done is explained in the comment above.
	 *
	 * The algorithm returns a reference of the concrete implementation
	 */
	Algo_t&
	run()
	{
		//This is more logical
		_ALGOBASE_wasRun = false;

		static_cast<Algo_t*>(this)->algo_pre_impl();

		static_cast<Algo_t*>(this)->algo_impl();

		_ALGOBASE_wasSuccess = (static_cast<Algo_t*>(this)->algo_post_impl());

		//Save that the algorithm was run
		_ALGOBASE_wasRun = true;

		return *(static_cast<Algo_t*>(this));
	};


	/**
	 * \brief	Returns true if the computation was done WITHOUT errors
	 */
	bool
	wasSuccessfull() const noexcept
	{
		return _ALGOBASE_wasSuccess;
	};


	/**
	 * \brief	Returns true if *this was run
	 */
	bool
	hasRun() const noexcept
	{
		return _ALGOBASE_wasRun;
	};


	/**
	 * \brief	Makes it possible to disarm the check in the destructor
	 *
	 * The destructor c hecks if the algorithm has run, by performing pgl_assert.
	 * This is for debuging, but it could be that the algorithm does not have to run.
	 * So it should be possible to bypass this check.
	 * And this function is exactly for that.
	 *
	 * The user has to base pglAlgoCommand_e::IGNORE_NOT_RUNNED to this function.
	 * Then the algorithm will change its internal state, to run.
	 * Any other value form that enum, will result in an exception.
	 *
	 * \return 	The old runstatus is returned
	 */
	bool
	ignoreNotRunned(
		const pglAlgoCommand_e c) noexcept(false)
	{
		if(c == pglAlgoCommand_e::INGNORE_NOT_RUNNED)
		{
			const bool oldRunStatus = _ALGOBASE_wasRun;
			_ALGOBASE_wasRun = true;
			return oldRunStatus;
		}
		else
		{
			throw PGL_EXCEPT_InvArg("Not supported command");
		};
	};





	/*
	 * ==============================
	 * Private variables
	 */
private:
	bool 			_ALGOBASE_wasSuccess;	//!< Indicates the success of the algorithm
	bool 			_ALGOBASE_wasRun;	//!< This variable indicates, if the algorithm was run
}; //End class(pgl_graphAlgoBase)








PGL_NS_END(graph)
PGL_NS_END(pgl)

#endif 	//End include guard
