/**
 * \file 	pgl_graph_algo/pgl_graph_algo_kruskal.cpp
 * \brief	This file implemennts the algorithm of kruskal which computes a minimum spanning tree
 */


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_exception.hpp>

#include <pgl/pgl_union_find.hpp>
#include <pgl/pgl_aglorithm.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph_utility/pgl_graph_util_kruskal_payloads.hpp>
#include <pgl_graph_algo/pgl_graph_algo_kruskal.hpp>

#include <pgl_reference_container.hpp>


//Include STD
#include <functional>



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)

pgl_kruskal_t::pgl_kruskal_t(
		pgl_graph_t& 	graph) noexcept(false) :
  m_mstWeight(0)
{
	if(graph.isComplete() == false)
	{
		throw PGL_EXCEPT_InvArg("The graph is not complete.");
	};

	/*
	 * Make the graph clean, make all the edges unset
	 */
	const auto endConnectionIT = graph.ConnEnd();
	for(auto it = graph.ConnEnd(); it != endConnectionIT; ++it)
	{
		it->getPayload()->makeUnmatched();
	};//End for(it): Make all unmatched


	//now get all the connections as reference and sort them
	pgl_graph_t::ConnRefContainer_t Connections = graph.getConnReferences();

	//Now we sort them, such that the light connections are at the beginning
	std::sort(Connections.begin(), Connections.end(),
		[](const pgl_connection_t& lhs, const pgl_connection_t& rhs) -> bool
			{ return lhs.getPayload()->getWeight() < rhs.getPayload()->getWeight() ;}
		); //End of sorting

	//Make an union find structire
	pgl_union_find_t NodeSet(graph.nVertices() );

	/*
	 * We could stop earlier, but this is not done yet, we sto if we have tested all the edges
	 */
	Weight_t lastSeenWeight = Connections.front().getPayload()->getWeight();

	for(pgl_connection_t& Conn : Connections)
	{
		const Weight_t currentWeight = Conn.getPayload()->getWeight();	//get the weight of the current edge we consider

		//test if the weight increses
		pgl_assert(currentWeight >= lastSeenWeight && (lastSeenWeight = currentWeight) );
		pgl_assert(Conn.getPayload()->isMatched() == false);

		//Test if the tow endpoints connect
		const uInt_t KeySrc  = Conn.getSrcID().getID();
		const uInt_t KeyDest = Conn.getDestID().getID();

		if(NodeSet.testSameSet(KeySrc, KeyDest) == false)
		{
			//They are not in the same set, so we can merge them together
			const bool succedMerge = NodeSet.mergeSets(KeySrc, KeyDest);
			pgl_assert(succedMerge);

			//Add the weight to the connection
			m_mstWeight += currentWeight;

			//Make the connection matched
			Conn.getPayload()->makeMatched();


			//Test if we have only one set, i.e. we can stop
			if(NodeSet.onlyOneSet() == true)
			{
				break;
			};

		};
	}; //End for(conn): Iterate through the connections and test them

#ifndef PGL_NDEBUG
	const uInt_t nNodes = graph.nVertices();
	for(uInt_t it = 0; it != nNodes; ++it)
	{
		pgl_assert(NodeSet.testSameSet(0, it));
	};
#endif
}; //End kruskal






Weight_t
pgl_kruskal_t::getWeight() const
{
	return m_mstWeight;
};


pgl_kruskal_t::operator Weight_t() const
{
	return m_mstWeight;
};





PGL_NS_END(graph)
PGL_NS_END(pgl)

