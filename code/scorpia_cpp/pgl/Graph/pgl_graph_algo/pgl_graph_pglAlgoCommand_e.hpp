#pragma once
/**
 * \brief	This file defines an enum that is used to controll certain aspects of the algorithm offered by pgl.
 *
 */

#include <pgl_core.hpp>
#include <pgl_int.hpp>

PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)

/**
 * \brief	This is for the algorithm to process the flow
 * \enum 	pglAlgoCommand_e
 *
 * This is teh behaviour:
 * - acceptEntity: 	This means that the node that is at the other end of the connection is accepted and will be added to the queue.
 *   			  Notice that this type has only an effect, on newly discovered nodes.
 *   			  Notice that if this value is encountered for a non white node, nothing happens, and then it is equivalent to ignoreEntitiy.
 * - ignoreEntity:	This means that a white node is not added to the queue, however the node could be found again, and could then
 *   			  enter the queue. In the case of a non white node, this value indicate "I DO NOT CARE".
 *   			  Notice that the algorithm treats such an edge as non existing.
 *   			  This means that a tree that is not a tree could be considered as a tree, if the right edges are ignored.
 * - stopAlgo:		This means that the algorithm can stop executing, this is NOT considered as an error.
 *   			  This value can be used when searching for some node.
 *   			  It also results that the algorithm is successfull
 * - abortAlgo:		This value indicates that the algorithm should abort. Unlike the 'stopAlgo' value this is an error and will trigger an exception.
 */
enum class pglAlgoCommand_e : ::pgl::uInt64_t
{

	INVALID 		= 0,

	INGNORE_NOT_RUNNED 	= 1 << 0, 	//Is used to disable the theck in the destructor

	acceptEntity 		= 1 << 1,
	ignoreEntity 		= 1 << 2,
	stopAlgo 		= 1 << 3,
	abortAlgo 		= 1 << 4,

};




PGL_NS_END(graph)
PGL_NS_END(pgl)



