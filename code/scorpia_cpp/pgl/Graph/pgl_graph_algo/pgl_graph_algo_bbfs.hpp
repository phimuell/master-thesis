#pragma once
#ifndef _PGL_GRAPH_IG_ALGORITHM_BBFS_HPP
#define _PGL_GRAPH_IG_ALGORITHM_BBFS_HPP
/**
 * \brief	This is the implementation of a backwards breath first search (BBFS) that operates on pgl graphs.
 *
 * The class should not be used directly, instead it is recomended to use the provided builder.
 * The BBFS works similary to the BFS algorithm provided by the boost library.
 * See "http://www.boost.org/doc/libs/1_54_0/libs/graph/doc/breadth_first_search.html" for more informations.
 *
 * Unlike a normal BFS, this algorithm workes in reverse order.
 * After it is run, each node knows how to reach the root, but in the case of a directed graph, the root does not know how to reach the other nodes.
 *
 * The BBFS stores two important quantities:
 * - The distance from any node to the root, node where the algorithm has started.
 * - From any node it is known, which is the next node in the path to reach the root (it predessecor is known).
 * As an extension it is planed that this quantities could also be stored in the payload, but this feature is not implemented yet.
 *
 * During the computation the algorithm each node is implicitly colored, there are 3 possible colors:
 * - WHITE: The node is not wisited yet, this is the initial status.
 * - GRAY:  The node is currently in the queue, the node that is currently processed is GRAY but not part of the queue.
 * - BLACK: The node was fully processed, he was dequeue from the queue.
 *
 * The user can use vistitors that are executing during the algorithm.
 * As already mentioned, they are taken from boost, with sliht  modifications.
 * Since a pgl edge represents more than an edge in boost, we do not need all visitors, boost has.
 * We manly focuses on the edges.
 *
 * > initialize_vertex_visitor:
 * Is executing at the begining of the algorithm.
 * It is called on each vertex.
 * If this function returns true, the preprocessing fails
 *
 * > examine_vertex_visitor:
 * This function is called after the node is removed from the queue, and before it is processed.
 * Its colour is per definition gray.
 * The return type of this function is pglAlgoCommand_e, its value indicate the course of action.
 *
 * > discover_edge_visitor:
 * This function is called on ANY admissible, tis means incomming edge, that is encountered.
 * This function can be called up to two times for any edge.
 * Its intention is to provide a preprocessing of the edge.
 *
 * Then we start to process all incomming edges, since this is a BBFS, of that node.
 * Depending on the colour of the otherNode, this is the node that could this node.
 * A different function is called.
 * The returnType of this function is pglAlgoCommand_e and its value is able to inflict the current processing.
 * For all three function the return value is interpreted simillary:
 * - acceptEntity: 	This means that the node that is at the other end of the connection is accepted and will be added to the queue.
 *   			  Notice that this type has only an effect, on newly discovered nodes.
 *   			  Notice that if this value is encountered for a non white node, nothing happens, and then it is equivalent to ignoreEntitiy.
 * - ignoreEntity:	This means that a white node is not added to the queue, however the node could be found again, and could then
 *   			  enter the queue. In the case of a non white node, this value indicate "I DO NOT CARE".
 *   			  Notice that the algorithm treats such an edge as non existing.
 *   			  This means that a tree that is not a tree could be considered as a tree, if the right edges are ignored.
 * - stopAlgo:		This means that the algorithm can stop executing, this is NOT considered as an error.
 *   			  This value can be used when searching for some node.
 *   			  It also results that the algorithm is successfull
 * - abortAlgo:		This value indicates that the algorithm should abort. Unlike the 'stopAlgo' value this is an error and will trigger an exception.
 *
 * > tree_edge_visitor
 * This function is called when a new not yet visited node, that has the colour white, is found.
 * This means that the colour is of otherNode is WHITE.
 * It also means thet owningNode is the predecessor of otherNode.
 *
 * > gray_edge_visitor
 * This visitor is called when otherNode of the currently processed edge is of colour GRAY.
 * This means that the node is known and allready in state of processing.
 * As a reminder the value 'acceptEntity' and 'ignoreEntity' are equivanent here.
 *
 * > black_edge_visitor
 * This visitor is called if otherNode is a node with colour BLACK.
 * This is the node was allready processed.
 * Please remember that this function has a second argument of type bool.
 * It indicates if the other edge, was the predecessor of the currently proceessed edge.
 * As a reminder the value 'acceptEntity' and 'ignoreEntity' are equivanent here.
 *
 *
 *
 *
 * > finish_vertex
 * This visitor is called on the node when all of its node are processed.
 * Its current colour is GRAY, but imdiatly after this function it is set to BLACK.
 *
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph_algo/pgl_graph_algo_interface.hpp>
#include <pgl_graph_utility/pgl_graph_vertex_map.hpp>
#include <pgl_graph_utility/pgl_graph_colour.hpp>
#include <pgl_graph_utility/pgl_graph_tree_description.hpp>

//The builder is inclooded at the end of this file


//Include STD
#include <functional>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


PGL_NS_BEGIN(bbfs_internal)

//Forward declaration of the implementation of the bbfs
class pgl_graphAlgo_bbfs_impl;


PGL_NS_END(bbfs_internal)


//Forward delcaration of the builder
class pgl_graphAlgo_bbfs_builder_t;



/**
 * \brief	This is pgl's version of the bbfs
 * \class 	pgl_graphAlgo_bbfs_t
 *
 * It behaves the same way as described above.
 * It uses the pimpl idiom.
 * It also offers a builder class that provides named arguments.
 *
 * The algorithm needs a graph, we say that the algorithm (oject) has an associated graph, he operates on.
 * Further, this algorithm uses the standard run implementation.
 *
 * To check if the algorithm was a success, the algorithm tests all the nodes and if all have color Withe or black, then anything was good.
 *
 * Notice that the algorithm associates the internal maps only during the FIRST run.
 * So if you create an algorithm and then modify the graph.
 * Then the graph in the internal maps are not in sync anymore.
 *
 * This error is not detected.
 *
 * Note: The algorithm also have problems with graphes that have multible connections between two nodes.
 * In that case the detechtion if the graph is a tree does not work, since all these connections are considered the same.
 */
class pgl_graphAlgo_bbfs_t : public pgl_graphAlgoBase_i
{
	/*
	 * ======================================
	 * Typedefs
	 */
public:
	/*
	 * Typedefs of the visitors
	 */
	/// This is the type of the init_vertex_visitor
	using initVertexVisitor_t 	= ::std::function<bool(Vertex_ref)>;

	/// This is the type for the examine vertex visitor
	using examineVertexVisitor_t 	= ::std::function<pglAlgoCommand_e(Vertex_ref)>;

	/// This is the type for the edge_discovery_visitor
	using edgeDiscoveryVisitor_t 	= ::std::function<pglAlgoCommand_e(Edge_ref)>;

	/// This is the tree_edge_visitor type
	using treeEdgeVisitor_t  	= ::std::function<pglAlgoCommand_e(Edge_ref)>;

	/// This is the gray_edge_visitor_type
	using grayEdgeVisitor_t 	= ::std::function<pglAlgoCommand_e(Edge_ref)>;

	/// This is the black_edge_visitor_type
	using blackEdgeVisitor_t 	= ::std::function<pglAlgoCommand_e(Edge_ref, bool)>;

	/// This is the finish_node_visitor type
	using finishingNodeVisitor_t 	= ::std::function<pglAlgoCommand_e(Vertex_ref)>;

	/*
	 * Normal Typedefs
	 */
	using Distance_t 		= ::pgl::graph::Distance_t;			//!< This is the type to describe distances
	using Colour_t 			= ::pgl::graph::pgl_colour_t;			//!< This is the colour type
	using PredecessorMap_t 		= pgl_vertexMap_t<VertexID_t>;	//!< This is the map used to describes the predecessor relation ship
	using DistanceMap_t 		= pgl_vertexMap_t<Distance_t>;			//!< This is the type that is the distacne map
	using ColourMap_t 		= pgl_vertexMap_t<Colour_t>;			//!< This is the colourmap
	using TreeDescription_t 	= ::pgl::graph::pgl_treeDescription_t;		//!< This is the tree description

	//This is the builder to build the algorithm
	using Builder_t 		= pgl_graphAlgo_bbfs_builder_t;


private:
	using Implementation_t 		= ::pgl::graph::bbfs_internal::pgl_graphAlgo_bbfs_impl;	//!< Implementation of the bbfs
	using Implementation_ptr 	= std::unique_ptr<Implementation_t>;			//!< This is the pointer to the implementation



	/*
	 * ====================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the constructor that allows to construct an algorithm that is associated with a graph.
	 *
	 * The passed graph is the graph that the algorithm is working on.
	 * It uses an builder to build the algorithm.
	 *
	 */
	pgl_graphAlgo_bbfs_t(
		const Builder_t& 	builder);


	/**
	 * \brief	Default constructor
	 */
	pgl_graphAlgo_bbfs_t() noexcept = delete;


	/**
	 * \brief	Copy constructor
	 */
	pgl_graphAlgo_bbfs_t(
		const pgl_graphAlgo_bbfs_t&);

	/**
	 * \brief	Move constructor
	 */
	pgl_graphAlgo_bbfs_t(
		pgl_graphAlgo_bbfs_t&&) noexcept;

	/**
	 * \brief	Default copy assignment
	 */
	pgl_graphAlgo_bbfs_t&
	operator= (
		const pgl_graphAlgo_bbfs_t&);


	/**
	 * \brief	Default move assignment
	 */
	pgl_graphAlgo_bbfs_t&
	operator= (
		pgl_graphAlgo_bbfs_t&&);

	/**
	 * \brief	Firtual destructor
	 */
	~pgl_graphAlgo_bbfs_t();


	/*
	 * =========================
	 * Quering functions
	 */


	/**
	 * \brief	This function returns true, if the algorithm has a source, where the bfs starts
	 */
	bool
	hasSource() const noexcept;


	/**
	 * \brief	Set source to new Source
	 *
	 * Unlike the visitors and the graph it is possible to chenage the source of the bbfs, without the need to reconstruct the algorithm.
	 * Please notice that this function throws, if the source is not set before.
	 * Please clear the source before calling this fucntion.
	 * This is node as a safe gurard
	 *
	 * Upon changing the source, the algorithm is set to not run and is set to default.
	 * Also all maps are invalidaded.
	 *
	 * \param 	newSource	New source, start point of the bbfs
	 */
	pgl_graphAlgo_bbfs_t&
	setSourceTo(
		const VertexID_t& newSource) noexcept(false);


	/**
	 * \brief	Returns the current source or invalid if no source was defined
	 */
	VertexID_t
	getSource() const noexcept;


	/**
	 * \brief	This function copies the found distances into the provided map.
	 *
	 * The map is reassociated
	 */
	const pgl_graphAlgo_bbfs_t&
	getFoundDistances(
		DistanceMap_t& 		exportDistances) const;


	/**
	 * \brief	This function returns a description of the found bfs tree
	 */
	TreeDescription_t
	getTreeDescription() const;


	/**
	 * \brief	This function tests if all nodes are visited.
	 *
	 * This is done, by checking that all nodes have color black
	 */
	bool
	allNodesWereVisited() const noexcept;


	/**
	 * \brief	This function returns if the graph is a tree.
	 *
	 * A graph is a tree, if only edges where descovered, that where NOT rejected by the examine edge function.
	 * taht leads to unkown vertices.
	 *
	 * Please notice that if a node is rejected (ignoreEntiy) by the examine function, then this can make a tree a non tree.
	 * also if the algorithm has halted earlier can inpact the result.
	 */
	bool
	isGraphATree() const noexcept;


	/*
	 * =======================
	 * Protected internal functions
	 */
protected:
	/**
	 * \brief	This is the preprocessing step of the algorithm
	 *
	 * This function resets everything such that the algorithm can rerun.
	 * This overwrites any data saved.
	 *
	 * Notice, that the association with the gfraph is only done in the first run.
	 * So, do not use the algorithjm after modifing the graph.
	 *
	 * If the source is not set an exception is generated
	 */
	virtual
	bool
	algo_pre_impl() final;


	/**
	 * \brief	This is the actual processing step, think of it as kernel.
	 *
	 * Here the hard work is done.
	 */
	virtual
	void
	algo_impl() final;


	/**
	 * \brief	This is the post processing step.
	 *
	 * Thsi fucntion checks if the color of all nodes is balck or white.
	 * Firter smale tests are performed.
	 */
	virtual
	bool
	algo_post_impl() final;


	/*
	 * The implementation is a friend
	 */
	 friend
	 class ::pgl::graph::bbfs_internal::pgl_graphAlgo_bbfs_impl;


	/*
	 * ====================
	 * Private Variables
	 */
private:
	Implementation_ptr 	m_impl;		//!< The implementation
}; // end class(pgl_graphAlgo_bbfs_t)



PGL_NS_END(graph)
PGL_NS_END(pgl)



//INclude the builder
 #include <pgl_graph_algo/pgl_graph_algo_bbfs_builder.hpp>


#endif
//End include guard




