/**
 * \brief	Implements the interface for the graph base interface
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph_algo/pgl_graph_pglAlgoCommand_e.hpp>
#include <pgl_graph_algo/pgl_graph_algo_interface.hpp>

//Include STD



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)



pgl_graphAlgoBase_i::pgl_graphAlgoBase_i() noexcept
{
	//Bring the status in the good status
	this->hook_resetState();
};


pgl_graphAlgoBase_i::pgl_graphAlgoBase_i(
	const pgl_graphAlgoBase_i&) noexcept = default;


pgl_graphAlgoBase_i::pgl_graphAlgoBase_i(
	pgl_graphAlgoBase_i&&) noexcept = default;


pgl_graphAlgoBase_i&
pgl_graphAlgoBase_i::operator= (
	const pgl_graphAlgoBase_i&) noexcept = default;


pgl_graphAlgoBase_i&
pgl_graphAlgoBase_i::operator= (
	pgl_graphAlgoBase_i&&) noexcept = default;



pgl_graphAlgoBase_i::~pgl_graphAlgoBase_i() noexcept
{
	pgl_assert(_ALGOBASE_wasRun == true);
};


pgl_graphAlgoBase_i&
pgl_graphAlgoBase_i::run()
{
	// Reset
	this->hook_resetState();

	// We have run
	this->hook_setRunStateTo(true);

	try
	{
		//Perform the preprocessing
		const bool resultPrePros = this->algo_pre_impl();

		//Test if preprocessing was successfull
		if(resultPrePros == false)
		{

			//throw an exception
			throw PGL_EXCEPT_RUNTIME("The preprocessing step failed.");
		}; //End if: preprocessing was uncucessfull


		//Perform the actual algorithm - Kernel
		this->algo_impl();


		//Test it
		_ALGOBASE_wasSuccess = this->algo_post_impl();
	}
	catch(...)
	{
		//THere was an error, so we set the aborted flag
		this->hook_setHasAbortedTo(true);
		this->hook_setSuccessSateTo(false);

		//Rethrow the exception
		std::exception_ptr currEx = std::current_exception();
		std::rethrow_exception(currEx);
	};


	return (*this);
};



bool
pgl_graphAlgoBase_i::wasSuccessfull() const noexcept
{
	return _ALGOBASE_wasSuccess;
};


bool
pgl_graphAlgoBase_i::hasRun() const noexcept
{
	return _ALGOBASE_wasRun;
};

bool
pgl_graphAlgoBase_i::hasAborted() const noexcept
{
	return _ALGOBASE_hasAborted;
};

bool
pgl_graphAlgoBase_i::hasHaltedEarlier() const noexcept
{
	return _ALGOBASE_wasHaltedEarlier;
};


bool
pgl_graphAlgoBase_i::ignoreNotRunned(
		const pglAlgoCommand_e c) noexcept(false)
{
	if(c == pglAlgoCommand_e::INGNORE_NOT_RUNNED)
	{
		const bool oldRunStatus = _ALGOBASE_wasRun;
		_ALGOBASE_wasRun = true;
		return oldRunStatus;
	}

	throw PGL_EXCEPT_InvArg("Not supported command");
};


pgl_graphAlgoBase_i&
pgl_graphAlgoBase_i::run(
	const pgl_graphAlgoRunArgument_i& Arg)
{
	throw PGL_EXCEPT_illMethod("TThe argument run is not implemented");
	return *this;
	(void)Arg;
};


pgl_graphAlgoBase_i&
pgl_graphAlgoBase_i::run(
	const pgl_graphAlgoRunArgument_i* const Arg)
{
	return this->run(*Arg);
};


bool
pgl_graphAlgoBase_i::algo_pre_impl()
{
	return false;
};

void
pgl_graphAlgoBase_i::algo_impl()
{
	throw PGL_EXCEPT_illMethod("This method is not implemented");
};


bool
pgl_graphAlgoBase_i::algo_post_impl()
{
	return false;
};



bool
pgl_graphAlgoBase_i::hook_setRunStateTo(
		const bool newState) noexcept
{
	const bool oldState = _ALGOBASE_wasRun;
	_ALGOBASE_wasRun = newState;
	return oldState;
};

bool
pgl_graphAlgoBase_i::hook_setSuccessSateTo(
		const bool newState) noexcept
{
	const bool oldState = _ALGOBASE_wasSuccess;
	_ALGOBASE_wasSuccess = newState;
	return oldState;
};



bool
pgl_graphAlgoBase_i::hook_setHasAbortedTo(
	const bool newState) noexcept
{
	const bool old = _ALGOBASE_hasAborted;
	_ALGOBASE_hasAborted = newState;

	return old;
};

bool
pgl_graphAlgoBase_i::hook_setHaltedEarlierTo(
	const bool newS) noexcept
{
	const bool old = _ALGOBASE_wasHaltedEarlier;
	_ALGOBASE_wasHaltedEarlier = newS;

	return old;
};


void
pgl_graphAlgoBase_i::hook_resetState() noexcept
{
	_ALGOBASE_wasRun = false;
	_ALGOBASE_hasAborted = false;
	_ALGOBASE_wasSuccess = false;
	_ALGOBASE_wasHaltedEarlier = false;

	return;
};







PGL_NS_END(graph)
PGL_NS_END(pgl)

