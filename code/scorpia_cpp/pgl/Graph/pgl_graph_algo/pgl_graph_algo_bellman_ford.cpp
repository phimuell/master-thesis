/**
 * \file 	pgl_graph_algo/pgl_graph_algo_bellman_ford.cpp
 * \brief	This file implemennts the algorithm of bellamn and ford, for finding shortest path rom one to the others.
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>

#include <pgl_graph_algo/pgl_graph_algo_bellman_ford.hpp>

#include <pgl_queue.hpp>


//Include STD
#include <utility>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)



pgl_bf_algo_t::pgl_bf_algo_t(
	const pgl_vertexID_t 	srcID,
	pgl_graph_t&		graph):
    Super_t()
  , m_graph(graph)
  , m_srcID(srcID)
  , m_hasNegativeCycle(false)
{
	//We run the algorithm in the constructor
	this->run();
};

void
pgl_bf_algo_t::algo_impl()
{

	if(m_graph.isComplete() == false)
	{
		throw PGL_EXCEPT_LOGIC("The m_graph is not complete.");
	};

	//This is the node where all originated
	const pgl_graph_t::NodeIterator_t srcNode = m_graph.getNode(m_srcID);
	srcNode->getPayLoadPointer()->setPredecessorTo(m_srcID);
	srcNode->getPayLoadPointer()->setCurrentDistanceTo(0);


	/*
	 * Make the queue that we are using
	 *
	 * The queue conatins iterators to outgoing vertex iterators.
	 * This are the vertieces that should be considered next.
	 */
	using Queue_t = ::pgl::pgl_queue_t<pgl_vertex_t::outVIterator_t>;
	Queue_t vToProcess;

	//Now add all the reacable nodes to the list
	for(auto outNode = srcNode->outVBegin(); outNode != srcNode->outVEnd(); ++outNode)
	{
		const pgl_vertex_t& currNode = *srcNode;

		pgl_assert(outNode->getPayLoadPointer()->hasPredecessor() == false);

		//Changes the data
		outNode->getPayLoadPointer()->setPredecessorTo(currNode.getID() );
		outNode->getPayLoadPointer()->setCurrentDistanceTo( Int_t(outNode.getCEdge().getPayload()->getWeight()) );
		vToProcess.push(outNode);
	};


	/*
	 * The problem is, that the concept of Phase is not adequate.
	 * A phase is processing all verieces found.
	 * So we must have a list, this list is filed inside the for-loop below.
	 * This list is ten inserted as ONE Object into the queue.
	 * And processing a hole list is considered as one phases.
	 */
	//This is the face counter, we use it to detect a negative cycle
	Size_t PhaseCounter = m_graph.nConnections() * m_graph.nVertices();
	//pgl_assert(false);

	while( PhaseCounter != 0)
	{
		//std::cout << "Perform one Step\n" << std::flush;

		if(vToProcess.empty() )
		{
			break;
		};

		//Reduce the phase counter
		PhaseCounter--;

		//The current vertex we whant now to process
		pgl_vertex_t& currentVertex = *(vToProcess.peek());
		const Distance_t currentDistance = currentVertex.getPayload()->getCurrDistance();
		const auto ende = currentVertex.outVEnd();

		int i = 0;
		for(auto indicentNodes = currentVertex.outVBegin(); indicentNodes != ende; ++indicentNodes)
		{
			const Distance_t connectionDistance = indicentNodes.getCEdge().getPayload()->getWeight();

			if(indicentNodes->getPayload()->hasPredecessor() == false)
			{
				//This node was not visited before
				indicentNodes->getPayload()->setPredecessorTo(currentVertex.getID() );
				indicentNodes->getPayload()->setCurrentDistanceTo( currentDistance + connectionDistance );

				i++;
				vToProcess.push(indicentNodes);
			}
			else
			{
				//We where here before so we must check if we have found a nearer path
				const Distance_t potentialDistance = currentDistance + connectionDistance;
				const Distance_t realDistance      = indicentNodes->getPayload()->getCurrDistance();

				if(potentialDistance < realDistance)
				{
					//We have found a shorter way
					indicentNodes->getPayload()->setPredecessorTo(currentVertex.getID() );
					indicentNodes->getPayload()->setCurrentDistanceTo(potentialDistance );

					//Insert it
					vToProcess.push(indicentNodes);
					i++;
				}; //End if: found a nearer path

			}; //End if
		}; //ENd for(indicentNodes): Scanning the new nodes

		//std::cout << "Added nodes: " << i << "\n";

		//Remove from the queue
		vToProcess.pop();
	}; //End while(phase)

	//pgl_assert(false);




	/*
	 * Now determine if a negative cycle was found
	 */
	if(vToProcess.empty() == false)
	{
		//The queue is not empty, so there must be an negative cycle
		m_hasNegativeCycle = true;
	};

};// End constructor






void
pgl_bf_algo_t::algo_pre_impl()
{
	const auto ende = m_graph.NodeEnd();
	for(auto node = m_graph.NodeBegin(); node != ende; ++node)
	{
		node->getPayLoadPointer()->clearPredecessor();
		node->getPayLoadPointer()->setCurrentDistanceTo(Constants::DistanceInfinity);
	}; //for(node)

	return;
};



bool
pgl_bf_algo_t::algo_post_impl()
{
	return true;
};







PGL_NS_END(graph)
PGL_NS_END(pgl)

