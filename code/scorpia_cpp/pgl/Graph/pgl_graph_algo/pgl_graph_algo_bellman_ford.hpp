#pragma once
#ifndef GRAPH__PGL_GRAPH_ALGO__PGL_GRAPH_ALGO__BELLMAN_FORD_HPP
#define GRAPH__PGL_GRAPH_ALGO__PGL_GRAPH_ALGO__BELLMAN_FORD_HPP
/**
 * \file 	pgl_graph_algo/pgl_graph_algo_bellman_ford.hpp
 * \brief	This file implemennts the algorithm of bellamn and ford, for finding shortest path rom one to the others.
 */


//Include PGL
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph_algo/pgl_graph_algo_base.hpp>


//Include STD



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)




/**
 * \class 	pgl_bf_algo_t
 * \brief 	Bellman And Ford Algo
 *
 * This class implements the bellman and ford algorithm
 */
class pgl_bf_algo_t : public pgl_graphAlgoBase<pgl_bf_algo_t>
{
	/*
	 * ========================
	 * The constructor runs the algorithm
	 */
public:
	pgl_bf_algo_t(
		const pgl_vertexID_t 	srcID,
		pgl_graph_t& 		graph);



	/**
	 * \brief	Returns true if a negative cycle was detected
	 */
	inline
	bool
	hasNegativeCycle() const noexcept
	{
		return m_hasNegativeCycle;
	};

	operator bool() noexcept
	{
		return m_hasNegativeCycle;
	};


	/*
	 * =======================
	 * Internal functions
	 */
public:

	/**
	 * \brief	This function prepares the graph, set all to unviited and so one
	 */
	void
	algo_pre_impl();

	/**
	 * \brief	This function does the prost processing of the algoritzhm.
	 * 		Its return value indicates if the algorithm was done witout errors
	 */
	bool
	algo_post_impl();


	/**
	 * \brief	This function runs the actual bellman-ford algorithm
	 */
	void
	algo_impl();


	/*
	 * ========================
	 * Private meber
	 */
private:
	pgl_graph_t&		m_graph;	//!< A reference to the graph
	VertexID_t 		m_srcID;
	bool			m_hasNegativeCycle;	//!< Stores if a negativecycle was detected

}; //End class(pgl_bf_algo_t)








PGL_NS_END(graph)
PGL_NS_END(pgl)

#endif 	//End include guard
