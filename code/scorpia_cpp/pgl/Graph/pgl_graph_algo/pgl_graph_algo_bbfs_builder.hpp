#pragma once
/**
 * \brief	This is a builder class for the bbfs algorithm.
 *
 * It is a nice and the only way to build  such an algorithm
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph_algo/pgl_graph_algo_interface.hpp>
#include <pgl_graph_utility/pgl_graph_vertex_map.hpp>
#include <pgl_graph_utility/pgl_graph_colour.hpp>
#include <pgl_graph_utility/pgl_graph_tree_description.hpp>
#include <pgl_graph_algo/pgl_graph_algo_bbfs.hpp>


//Include STD
#include <functional>

PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


PGL_NS_BEGIN(bbfs_internal)

//Forward declaration of the implementation of the bbfs
class pgl_graphAlgo_bbfs_impl;


PGL_NS_END(bbfs_internal)


/**
 * \brief	This is pgl's version of the bbfs
 * \class 	pgl_graphAlgo_bbfs_builder
 *
 * This is a builder for easy use.
 *
 */
class pgl_graphAlgo_bbfs_builder_t
{
	/*
	 * ============================================
	 * Typedefs
	 */
public:
	//Vertex
	using Vertex_t 			= ::pgl::graph::pgl_vertex_t;			//!< This is the type of the vertex
	using Vertex_ptr 		= ::pgl::graph::pgl_vertex_t*;			//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= const ::pgl::graph::pgl_vertex_t*;		//!< This is the pointer to a constant vertex
	using Vertex_ref		= ::pgl::graph::pgl_vertex_t&;			//!< This is the type of a reference to a node
	using Vertex_cref		= const ::pgl::graph::pgl_vertex_t&;		//!< This is the constant reference to a vertex
	using VertexPayLoad_ptr		= ::pgl::graph::pgl_vertex_payload_t*;		//!< This is the payload of the vertex
	using VertexID_t 		= ::pgl::graph::pgl_vertexID_t;			//!< This is the type of the vertex ID

	//Connection
	using Connection_t 		= ::pgl::graph::pgl_connection_t;		//!< Type of the connection
	using Connection_ptr		= pgl_connection_t*;				//!< Pointer to a connection
	using Connection_cptr		= const pgl_connection_t*;			//!< Pointer to a constant connection
	using Connection_ref 		= pgl_connection_t&;				//!< Reference to connection
	using Connection_cref		= const pgl_connection_t&;			//!< Const reference to connection
	using ConnPayLoad_ptr		= ::pgl::graph::pgl_connection_payload_t*;	//!< Pointer to a vertex payload
	using ConnectionID_t 		= ::pgl::graph::pgl_edgeID_t;			//!< This is the ID that connections and edges uses

	//Edge
	using Edge_t 			= ::pgl::graph::pgl_edge_t; 			//!< Type of the edge
	using Edge_ptr			= pgl_edge_t*;					//!< Pointer to a edge
	using Edge_cptr			= const pgl_edge_t*;				//!< Pointer to a constant edge
	using Edge_ref 			= pgl_edge_t&;					//!< Reference to edge
	using Edge_cref			= const pgl_edge_t&;				//!< Const reference to edge
	using EdgeID_t 		= ::pgl::graph::pgl_edgeID_t;			//!< This is the ID that connections and edges uses

	//Graph
	using Graph_t			= ::pgl::graph::pgl_graph_t;			//!< The used graph type
	using Graph_ref 		= ::pgl::graph::pgl_graph_t&;			//!< Reference to the graph
	using Graph_cref		= const ::pgl::graph::pgl_graph_t&; 		//!< Const referecne to the graph
	using Graph_ptr 		= ::pgl::graph::pgl_graph_t*;			//!< pointer to the graph
	using Graph_cptr		= const ::pgl::graph::pgl_graph_t*; 		//!< Const pointer to the graph


	/*
	 * Typedefs of the visitors
	 */
	/// This is the type of the init_vertex_visitor
	using initVertexVisitor_t 	= pgl_graphAlgo_bbfs_t::initVertexVisitor_t;

	/// This is the type for the examine vertex visitor
	using examineVertexVisitor_t 	= pgl_graphAlgo_bbfs_t::examineVertexVisitor_t;

	/// This is the type for the edge_discovery_visitor
	using edgeDiscoveryVisitor_t 	= pgl_graphAlgo_bbfs_t::edgeDiscoveryVisitor_t;

	/// This is the tree_edge_visitor type
	using treeEdgeVisitor_t  	= pgl_graphAlgo_bbfs_t::treeEdgeVisitor_t;

	/// This is the gray_edge_visitor_type
	using grayEdgeVisitor_t 	= pgl_graphAlgo_bbfs_t::grayEdgeVisitor_t;

	/// This is the black_edge_visitor_type
	using blackEdgeVisitor_t 	= pgl_graphAlgo_bbfs_t::blackEdgeVisitor_t;

	/// This is the finish_node_visitor type
	using finishingNodeVisitor_t 	= pgl_graphAlgo_bbfs_t::finishingNodeVisitor_t;

	/*
	 * Normal Typedefs
	 */

	using GraphReference_t 		= std::reference_wrapper<Graph_t>;		//!< This is the copyable version of the reference, that is needed internaly


	/*
	 * ====================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the constructor that allows to construct an algorithm that is associated with a graph.
	 *
	 * The passed graph is the graph that the algorithm is working on.
	 * It uses an builder to build the algorithm.
	 *
	 * \param 	source 		The vertex where the algorithm starts
	 */
	pgl_graphAlgo_bbfs_builder_t(
		Graph_ref 	Graph) :
	  pgl_graphAlgo_bbfs_builder_t(Graph, VertexID_t::INVALID())
	{};

	pgl_graphAlgo_bbfs_builder_t(
		Graph_ref 	Graph,
		VertexID_t 	source);


	/**
	 * \brief	Default constructor
	 */
	pgl_graphAlgo_bbfs_builder_t() noexcept = delete;


	/**
	 * \brief	Copy constructor
	 */
	pgl_graphAlgo_bbfs_builder_t(
		const pgl_graphAlgo_bbfs_builder_t&) = default;

	/**
	 * \brief	Move constructor
	 */
	pgl_graphAlgo_bbfs_builder_t(
		pgl_graphAlgo_bbfs_builder_t&&) = default;

	/**
	 * \brief	Default copy assignment
	 */
	pgl_graphAlgo_bbfs_builder_t&
	operator= (
		const pgl_graphAlgo_bbfs_builder_t&) = default;


	/**
	 * \brief	Default move assignment
	 */
	pgl_graphAlgo_bbfs_builder_t&
	operator= (
		pgl_graphAlgo_bbfs_builder_t&&) noexcept = default;

	/**
	 * \brief	Firtual destructor
	 */
	~pgl_graphAlgo_bbfs_builder_t() = default;


	/*
	 * =========================
	 * Quering functions
	 */

	pgl_graphAlgo_bbfs_builder_t&
	bindBuilderTo(
		Graph_ref Graph)
	{
		if(Graph.isComplete() == false)
		{
			throw PGL_EXCEPT_LOGIC("Graph is not complete.");
		};

		m_graph = std::ref(Graph);

		return *this;
	};

	pgl_graphAlgo_bbfs_builder_t&
	setInitVertexVisitor(
		initVertexVisitor_t 	visitor)
	{
		if((bool)visitor == false)
		{
			throw PGL_EXCEPT_InvArg("Visitor is not callable");
		};

		m_initVertexVisitor = std::move(visitor);

		return *this;
	};

	pgl_graphAlgo_bbfs_builder_t&
	setExamineVertexVisitor(
		examineVertexVisitor_t 	visitor)
	{
		if((bool)visitor == false)
		{
			throw PGL_EXCEPT_InvArg("Visitor is not callable");
		};

		m_examineVertexVisitor = std::move(visitor);

		return *this;
	};

	pgl_graphAlgo_bbfs_builder_t&
	setEdgeDiscovVisitor(
		edgeDiscoveryVisitor_t 	visitor)
	{
		if((bool)visitor == false)
		{
			throw PGL_EXCEPT_InvArg("Visitor is not callable");
		};

		m_edgeDiscoverVisitor = std::move(visitor);

		return *this;
	};

	pgl_graphAlgo_bbfs_builder_t&
	setTreeEdgeVisitor(
		treeEdgeVisitor_t 	visitor)
	{
		if((bool)visitor == false)
		{
			throw PGL_EXCEPT_InvArg("Visitor is not callable");
		};

		m_treeEdgeVisitor = std::move(visitor);

		return *this;
	};


	pgl_graphAlgo_bbfs_builder_t&
	setGrayEdgeVisitor(
		grayEdgeVisitor_t 	visitor)
	{
		if((bool)visitor == false)
		{
			throw PGL_EXCEPT_InvArg("Visitor is not callabel");
		};

		m_grayEdgeVisitor = std::move(visitor);

		return *this;
	};

	pgl_graphAlgo_bbfs_builder_t&
	setBlackEdgeVisitor(
		blackEdgeVisitor_t 	visitor)
	{
		if((bool)visitor == false)
		{
			throw PGL_EXCEPT_InvArg("Visitor not callable");
		};

		m_balckEdgeVisitor = std::move(visitor);

		return *this;
	};

	pgl_graphAlgo_bbfs_builder_t&
	setFinishingNodeViditor(
		finishingNodeVisitor_t 	visitor)
	{
		if((bool)visitor == false)
		{
			throw PGL_EXCEPT_InvArg("Visitor not callable");
		};

		m_finNodeVisitor = std::move(visitor);

		return *this;
	};

	pgl_graphAlgo_bbfs_builder_t&
	setSourceNode(
		VertexID_t 	newSource)
	{
		if(newSource.isInvalid())
		{
			throw PGL_EXCEPT_InvArg("New Source is invalid.");
		};

		m_source = newSource;

		return *this;
	};

	friend
	class ::pgl::graph::bbfs_internal::pgl_graphAlgo_bbfs_impl;



	/*
	 * ====================
	 * Private Variables
	 */
private:
	//THe visitors
	GraphReference_t 			m_graph;
	VertexID_t 				m_source;
	initVertexVisitor_t 			m_initVertexVisitor;
	examineVertexVisitor_t 			m_examineVertexVisitor;
	edgeDiscoveryVisitor_t 			m_edgeDiscoverVisitor;
	treeEdgeVisitor_t 			m_treeEdgeVisitor;
	grayEdgeVisitor_t 			m_grayEdgeVisitor;
	blackEdgeVisitor_t 			m_balckEdgeVisitor;
	finishingNodeVisitor_t 			m_finNodeVisitor;
}; // end class(pgl_graphAlgo_bbfs_builder_t)



PGL_NS_END(graph)
PGL_NS_END(pgl)









