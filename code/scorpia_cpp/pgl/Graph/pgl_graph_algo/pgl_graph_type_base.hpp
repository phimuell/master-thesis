#pragma once
/**
 * \file	pgl/Graph/graph_algo/pgl_graph_type_base.hpp
 * \brief	A common base class for all graph related class.
 *
 * This is a template class, that takes a graph as it template parametr.
 * It extracts the type oif the enteties that are declared as part of the public interface of the graph.
 * This way no hardcoding of types is neccessary.
 */


//Include PGL
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


/**
 * \class 	pgl_graph_commoin_base_t
 *
 * The second template argument is optional. It is used to create diffrent classes wen using this interface in diffrent ways
 *
 */
template< class graph_type,
	class Distinguisher_t = void>
struct pgl_graph_common_base_t
{

	/*
	 * ===========================================
	 * All typedefs
	 */
public:
	//Graph
	using Graph_t 			= graph_type;					//!< The type of the graph
	using Graph_ref 		= graph_type&;					//!< Refernece to the grpah
	using Graph_cref 		= const graph_type&;				//!< Const reference to the graph
	using Graph_ptr 		= graph_type*;					//!< Pointer to the graph
	using Graph_cptr 		= const graph_type*;				//!< Constant pointer to the graph


	//Vertex
	using Vertex_t 			= typename graph_type::Vertex_t;		//!< This is the type of the vertex
	using Vertex_ptr 		= typename graph_type::Vertex_ptr;		//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= typename graph_type::Vertex_cptr;		//!< This is the pointer to a constant vertex
	using Vertex_ref		= typename graph_type::Vertex_ref;		//!< This is the type of a reference to a node
	using Vertex_cref		= typename graph_type::Vertex_cref;		//!< This is the constant reference to a vertex
	using VertexPayLoad_ptr		= typename graph_type::VertexPayLoad_ptr;	//!< This is the payload of the vertex
	using VertexPayLoad_cptr	= typename graph_type::VertexPayLoad_cptr;	//!< This is the payload of the vertex
	using VertexID_t 		= typename graph_type::VertexID_t;		//!< This is the type of the vertex ID

	//Connection
	using Connection_t 		= typename graph_type::Connection_t;		//!< Type of the connection
	using Connection_ptr		= typename graph_type::Connection_ptr;		//!< Pointer to a connection
	using Connection_cptr		= typename graph_type::Connection_ref;		//!< Pointer to a constant connection
	using Connection_ref 		= typename graph_type::Connection_ref;		//!< Reference to connection
	using Connection_cref		= typename graph_type::Connection_cptr;		//!< Const reference to connection
	using ConnPayLoad_ptr		= typename graph_type::ConnPayLoad_ptr;		//!< Pointer to a vertex payload
	using ConnPayLoad_cptr		= typename graph_type::ConnPayLoad_cptr;	//!< Pointer to a vertex payload
	using ConnectionID_t 		= typename graph_type::ConnectionID_t;		//!< This is the ID that connections and edges uses

	//Edge
	using Edge_t 			= typename graph_type::Edge_t; 			//!< Type of the edge
	using Edge_ptr			= typename graph_type::Edge_ptr;		//!< Pointer to a edge
	using Edge_cptr			= typename graph_type::Edge_cptr;		//!< Pointer to a constant edge
	using Edge_ref 			= typename graph_type::Edge_ref;		//!< Reference to edge
	using Edge_cref			= typename graph_type::Edge_cref;		//!< Const reference to edge
	using EdgeID_t 			= typename graph_type::EdgeID_t;		//!< This is the ID that connections and edges uses



}; //End class(pgl_graph_common_base_t)





PGL_NS_END(graph)
PGL_NS_END(pgl)


