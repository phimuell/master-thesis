#pragma once
/**
 * \file 	pgl_graph_algo/pgl_graph_algo_interface.hpp
 *
 * \brief	This file implements a base class for all algoritms, with polymorphism.
 *
 * This is an generall interface for graph algorithm.
 * There is another baseclass that emploies CRTP to achieve polymorphism.
 * This implementation dies the same, but with virtual functions.
 *
 * The folowing functions needed to be provided by the deriving class:
 * - algo_pree_impl:
 *   This function is run befor thee algorithm, the intended use is to set up some things.
 *   The return value, if any is ignored.
 *
 * - algo_impl:
 *   This is the actual algorithm, that is run.
 *   The return value, if any is ignored.
 *
 * - algo_post_impl:
 *   This function performs some postprocessing.
 *   It has to return a bool.
 *   This bool shall indicate, if the computation was successfull.
 *
 * The base also provides a function that indicates if the algoritm was success.
 *
 *
 *
 * The base manages the state of the algorithm.
 * With state we mean atributes like run (was executed), successfully completed of other such things.
 * This managing can not be done by deriving classes, but there are protected hooks that can be called by them, to change the status.
 * There are severall statuses:
 * 	> wasRun:
 * 	  This means that the run function was called.
 * 	  The function must not be completed or something, it just mean tht the run function was called.
 * 	> successfullyCompleted
 * 	  This indicated that the algorithm had been excecuted successfully.
 * 	  Notice that this property impleies the hasrun.
 * 	> wasHaltedEarlier
 * 	  This properties is used to indicate that the algorithm was ended earlier by the user.
 * 	  This does not need to indicate an error, it is more meant ot indicate that an algorithm had to be halted before it was fully executed.
 * 	  The implementation is requeired to ensure that the state is consistent
 * 	> hasAborted
 * 	  This is similar to the halted earlier propertiy, but this is to indicate an error.
 * 	  If this is set, an error is encountered and no exception is generated.
 * 	  Please notice that the default implementaion of the run function monitores the three impl function. And if one of them throws the zhis state is set.
 * 	  If this happens then this value is set to true.
 * 	  Also if the preprocessing fails this value is set to true.
 */


//Include PGL
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph_algo/pgl_graph_pglAlgoCommand_e.hpp>
#include <pgl_graph_algo/pgl_graph_algo_interface_runArgument.hpp>

//Include STD



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)





/**
 * \class 	pgl_graphAlgoBase
 * \brief 	A base for all algorithms using virtual functions
 *
 *
 * The working of this class is the same as the CRTP but with virtual functions.
 * All constructors are protected and defaulted.
 * The only exception is the destructor witch is defaulted virtual and public.
 *
 * The hook functions that are called by the run function are protected.
 * They are NOT abstarct but they are fool proofe, at least to some extend.
 *
 * The algorithm will not succeed and if the jernel (algo_impl()) is called an exception is raised.
 *
 * The run function is declared as final.
 * But the user can specify its own runfunction.
 * There are methiods to set the internal state of the interface.
 * It is not recomended to do this, the functionality is provided for other codes that needs to extend the algorithm concept.
 *
 */
class pgl_graphAlgoBase_i
{
	/*
	 * ========================
	 */
public:
	/**
	 * \brief	Destructor
	 *
	 * As the CRTP version does the destructor checks if  the algoriuthm has run.
	 * If it has not run, then an assert is trigered, not active if the PGL_NDEBUG is active.
	 *
	 * This behaviour can be deactivated with a call to ignoreNotRunned
	 */
	virtual
	~pgl_graphAlgoBase_i() noexcept;


protected:

	/**
	 * \brief	Defaulted default constructor
	 */
	pgl_graphAlgoBase_i() noexcept;


	/**
	 * \brief	Copy constructor
	 */
	pgl_graphAlgoBase_i(
		const pgl_graphAlgoBase_i&) noexcept;

	/**
	 * \brief	Move constructor
	 */
	pgl_graphAlgoBase_i(
		pgl_graphAlgoBase_i&&) noexcept;

	/**
	 * \brief	Default copy assignment
	 */
	pgl_graphAlgoBase_i&
	operator= (
		const pgl_graphAlgoBase_i&) noexcept;


	/**
	 * \brief	Default move assignment
	 */
	pgl_graphAlgoBase_i&
	operator= (
		pgl_graphAlgoBase_i&&) noexcept;


	/*
	 * ============================================
	 * Typedefs
	 */
public:
	//Vertex
	using Vertex_t 			= ::pgl::graph::pgl_vertex_t;			//!< This is the type of the vertex
	using Vertex_ptr 		= ::pgl::graph::pgl_vertex_t*;			//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= const ::pgl::graph::pgl_vertex_t*;		//!< This is the pointer to a constant vertex
	using Vertex_ref		= ::pgl::graph::pgl_vertex_t&;			//!< This is the type of a reference to a node
	using Vertex_cref		= const ::pgl::graph::pgl_vertex_t&;		//!< This is the constant reference to a vertex
	using VertexPayLoad_ptr		= ::pgl::graph::pgl_vertex_payload_t*;		//!< This is the payload of the vertex
	using VertexID_t 		= ::pgl::graph::pgl_vertexID_t;			//!< This is the type of the vertex ID

	//Connection
	using Connection_t 		= ::pgl::graph::pgl_connection_t;		//!< Type of the connection
	using Connection_ptr		= pgl_connection_t*;				//!< Pointer to a connection
	using Connection_cptr		= const pgl_connection_t*;			//!< Pointer to a constant connection
	using Connection_ref 		= pgl_connection_t&;				//!< Reference to connection
	using Connection_cref		= const pgl_connection_t&;			//!< Const reference to connection
	using ConnPayLoad_ptr		= ::pgl::graph::pgl_connection_payload_t*;	//!< Pointer to a vertex payload
	using ConnectionID_t 		= ::pgl::graph::pgl_edgeID_t;			//!< This is the ID that connections and edges uses

	//Edge
	using Edge_t 			= ::pgl::graph::pgl_edge_t; 			//!< Type of the edge
	using Edge_ptr			= pgl_edge_t*;					//!< Pointer to a edge
	using Edge_cptr			= const pgl_edge_t*;				//!< Pointer to a constant edge
	using Edge_ref 			= pgl_edge_t&;					//!< Reference to edge
	using Edge_cref			= const pgl_edge_t&;				//!< Const reference to edge
	using EdgeID_t 		= ::pgl::graph::pgl_edgeID_t;			//!< This is the ID that connections and edges uses

	//Graph
	using Graph_t			= ::pgl::graph::pgl_graph_t;			//!< The used graph type
	using Graph_ref 		= ::pgl::graph::pgl_graph_t&;			//!< Reference to the graph
	using Graph_cref		= const ::pgl::graph::pgl_graph_t&; 		//!< Const referecne to the graph
	using Graph_ptr 		= ::pgl::graph::pgl_graph_t*;			//!< pointer to the graph
	using Graph_cptr		= const ::pgl::graph::pgl_graph_t*; 		//!< Const pointer to the graph


	/**
	 * \brief 	This function performs the actuall run of the algorithm
	 *
	 * How this is done is explained in the comment above.
	 *
	 * As the equvivalent function from the CTRP version, this also returns a reference to this.
	 * Previously this function was declared final, but now it is not.
	 * This is done to allow the user to offer more freedo,.
	 * But there is a default implementation.
	 *
	 * This function calls the three protected function, see below successfully.
	 *
	 * The function dies not check the run status, so a once runned algorithm can be run again.
	 * The first action performed by this function is, to set the runstatus to false.
	 *
	 * Also notice that this is not thread safe.
	 */
	virtual
	pgl_graphAlgoBase_i&
	run();


	/**
	 * \brief	This function is a run function that takes arguments.
	 *
	 * The user can derive from the argument class, which is only an interface.
	 * This function has a defaault implementation that throws.
	 */
	virtual
	pgl_graphAlgoBase_i&
	run(
		const pgl_graphAlgoRunArgument_i& Arg);


	/**
	 * \brief	This function is the same as the one with the reference.
	 *
	 * It is final, it simly dereferences the argument and passes it to the run function with the reference argument
	 */
	virtual
	pgl_graphAlgoBase_i&
	run(
		const pgl_graphAlgoRunArgument_i* const Arg) final;


	/**
	 * \brief	Returns true if the computation was done WITHOUT errors
	 */
	virtual
	bool
	wasSuccessfull() const noexcept final;


	/**
	 * \brief	Retruns true if the algorithm halted earlier
	 */
	virtual
	bool
	hasHaltedEarlier() const noexcept final;

	/**
	 * \brief	Retruns true if the algorithm has aborted
	 */
	virtual
	bool
	hasAborted() const noexcept final;


	/**
	 * \brief	Returns true if *this was run
	 */
	virtual
	bool
	hasRun() const noexcept final;


	/**
	 * \brief	Makes it possible to disarm the check in the destructor
	 *
	 * The destructor c hecks if the algorithm has run, by performing pgl_assert.
	 * This is for debuging, but it could be that the algorithm does not have to run.
	 * So it should be possible to bypass this check.
	 * And this function is exactly for that.
	 *
	 * The user has to base pglAlgoCommand_e::IGNORE_NOT_RUNNED to this function.
	 * Then the algorithm will change its internal state, to run.
	 * Any other value form that enum, will result in an exception.
	 *
	 * This allows deriving classes to decativate this future.
	 *
	 * \return 	The old runstatus is returned
	 */
	virtual
	bool
	ignoreNotRunned(
		const pglAlgoCommand_e c) noexcept(false);


	/*
	 * ===================================
	 * Protected functions
	 *
	 * This are like the hooks.
	 * We preprocess, excecute and postprocess, see above.
	 *
	 */
protected:

	/**
	 * \brief	This is the preprocessing step of the algorithm
	 *
	 * This function must be implemenrted by the derivied class.
	 * It is called firest.
	 * It is used to set up the internal state, which can not be handled in the constructor.
	 * This is the first function that is called by the run-function.
	 *
	 * if this function returns false, this is interpreted as "The algorithm can not run".
	 * deriving classes can indicating by that that algorithm is not set up properly.
	 * An exception is generated.
	 * The hasRun is set to true regardless of the result of that.
	 *
	 * Default is that this function does nothing.
	 *
	 */
	virtual
	bool
	algo_pre_impl();


	/**
	 * \brief	This is the actual processing step, think of it as kernel.
	 *
	 * Here the hard work is done.
	 * This function is called imediatly after the preprocessing function returned.
	 */
	virtual
	void
	algo_impl();


	/**
	 * \brief	This is the post processing step.
	 *
	 * It is intended to check the results.
	 * The returntype of this function is bool.
	 * It is used to determine if the computation was successfully.
	 *
	 * After this function returns the algoritm goes in the runned state.
	 *
	 * There is a default implementation that returns false
	 */
	virtual
	bool
	algo_post_impl();


	/**
	 * \brief	with this private hook the user can make the algorithm to successfull.
	 *
	 * This function is provided for extensional prupose.
	 * As said before it is not recomended to use this function.
	 *
	 * The old successstate is returned.
	 */
	virtual
	bool
	hook_setSuccessSateTo(
		const bool newState) noexcept final;


	/**
	 * \brief	This hook enables the deriving class to set the runstate.
	 *
	 * This function is provided for extensional prupose only.
	 * use the provided runfuntion.
	 */
	virtual
	bool
	hook_setRunStateTo(
		const bool newState) noexcept final;


	/**
	 * \brief	set the halted earlier value to
	 */
	virtual
	bool
	hook_setHaltedEarlierTo(
		const bool newState) noexcept final;


	/**
	 * \brief	This function resetes the state.
	 *
	 * The state is like a default constructed
	 */
	virtual
	void
	hook_resetState() noexcept final;


	/**
	 * \brief	This function sets the aborted status to
	 */
	virtual
	bool
	hook_setHasAbortedTo(
		const bool newState) noexcept final;



	/*
	 * ==============================
	 * Private variables
	 */
private:
	bool 			_ALGOBASE_wasSuccess;	//!< Indicates the success of the algorithm
	bool 			_ALGOBASE_wasRun;	//!< This variable indicates, if the algorithm was run
	bool 			_ALGOBASE_wasHaltedEarlier; //!< Indicates that the algorithm stoped before the finishing conditions were meet
	bool 			_ALGOBASE_hasAborted;	//!< Indicates if the algorithm has aborted
}; //End class(pgl_graphAlgoBase)








PGL_NS_END(graph)
PGL_NS_END(pgl)

