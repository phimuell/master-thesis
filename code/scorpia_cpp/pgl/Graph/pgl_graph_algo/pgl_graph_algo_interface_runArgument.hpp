#pragma once
/**
 * \brioef	This file implements an abstact base class, for the algorithm interface.
 *
 * It is used as an argument replacement.
 * It is a better void*.
 * It can be used as baseclass, if the user want to pass arguments to the run function.
 */


//Include PGL
#include <pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_fwd.hpp>
#include <pgl_graph/pgl_graph_vertexID.hpp>
#include <pgl_graph_algo/pgl_graph_pglAlgoCommand_e.hpp>

//Include STD



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)




/**
 * \class 	pgl_graphAlgoRunArgument_i
 * \vbrief	Argument type for the runfunction.
 *
 * This is a baseclass the user can inherent form, if he/she wnats to pass arguiments to the run function of the algorithm
 *
 * The destructor is virtual and public.
 * All other constructors are defaulted and noexcept
 */
class pgl_graphAlgoRunArgument_i
{
public:
	virtual
	~pgl_graphAlgoRunArgument_i() = default;

protected:

	pgl_graphAlgoRunArgument_i() noexcept = default;

	pgl_graphAlgoRunArgument_i(
		const pgl_graphAlgoRunArgument_i&) noexcept = default;

	pgl_graphAlgoRunArgument_i(
		pgl_graphAlgoRunArgument_i&&) noexcept = default;

	pgl_graphAlgoRunArgument_i&
	operator= (
		const pgl_graphAlgoRunArgument_i&) noexcept = default;

	pgl_graphAlgoRunArgument_i&
	operator= (
		pgl_graphAlgoRunArgument_i&&) noexcept = default;

}; //End class(pgl_graphAlgoRunArgument_i)





PGL_NS_END(graph)
PGL_NS_END(pgl)
