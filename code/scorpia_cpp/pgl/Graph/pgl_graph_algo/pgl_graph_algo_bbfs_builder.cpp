/**
 * \brief 	cpp file of the bbfs builder
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph_algo/pgl_graph_algo_interface.hpp>
#include <pgl_graph_utility/pgl_graph_vertex_map.hpp>
#include <pgl_graph_utility/pgl_graph_colour.hpp>
#include <pgl_graph_utility/pgl_graph_tree_description.hpp>
#include <pgl_graph_algo/pgl_graph_algo_bbfs.hpp>
#include <pgl_graph_algo/pgl_graph_algo_bbfs_builder.hpp>


//Include STD
#include <functional>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


PGL_NS_BEGIN(bbfs_internal)

//Default for the init function
bool
bbfs_iniVertexVisitor_default(
	pgl_graphAlgo_bbfs_t::Vertex_ref)
{
	return true;
};


//This is the default fopr the examine node
pglAlgoCommand_e
bbfs_examineNode_default(
	pgl_graphAlgo_bbfs_t::Vertex_ref)
{
	return pglAlgoCommand_e::acceptEntity;
};

//Edge descovery
pglAlgoCommand_e
bbfs_edgeDescovery_default(
	pgl_graphAlgo_bbfs_t::Edge_ref)
{
	return pglAlgoCommand_e::acceptEntity;
};

pglAlgoCommand_e
bbfs_teeEdge_default(
	pgl_graphAlgo_bbfs_t::Edge_ref)
{
	return pglAlgoCommand_e::acceptEntity;
};

pglAlgoCommand_e
bbfs_grayEdge_default(
	pgl_graphAlgo_bbfs_t::Edge_ref)
{
	return pglAlgoCommand_e::ignoreEntity;
};

pglAlgoCommand_e
bbfs_blackEdge_default(
	pgl_graphAlgo_bbfs_t::Edge_ref,
	bool)
{
	return pglAlgoCommand_e::ignoreEntity;
};

pglAlgoCommand_e
bbfs_finischingNode_default(
	pgl_graphAlgo_bbfs_t::Vertex_ref)
{
	return pglAlgoCommand_e::acceptEntity;
};



PGL_NS_END(bbfs_internal)


pgl_graphAlgo_bbfs_builder_t::pgl_graphAlgo_bbfs_builder_t(
		pgl_graph_t& 	Graph,
		VertexID_t 	source) :
  m_graph(std::ref(Graph)),
  m_source(source),
  m_initVertexVisitor(bbfs_internal::bbfs_iniVertexVisitor_default),
  m_examineVertexVisitor(bbfs_internal::bbfs_examineNode_default),
  m_edgeDiscoverVisitor(bbfs_internal::bbfs_edgeDescovery_default),
  m_treeEdgeVisitor(bbfs_internal::bbfs_teeEdge_default),
  m_grayEdgeVisitor(bbfs_internal::bbfs_grayEdge_default),
  m_balckEdgeVisitor(bbfs_internal::bbfs_blackEdge_default),
  m_finNodeVisitor(bbfs_internal::bbfs_finischingNode_default)
{

};





PGL_NS_END(graph)
PGL_NS_END(pgl)






