#pragma once
#ifndef GRAPH__PGL_GRAPH_ALGO__PGL_GRAPH_ALGO_KRUSKAL_HPP
#define GRAPH__PGL_GRAPH_ALGO__PGL_GRAPH_ALGO_KRUSKAL_HPP
/**
 * \file 	pgl_graph_algo/pgl_graph_algo_kruskal.hpp
 * \brief	This file implemennts the algorithm of kruskal which computes a minimum spanning tree
 */


//Include PGL
#include <pgl_core/pgl_core.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>


//Include STD



PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)



/**
 * \class 	pgl_kruskal_t
 * \brief	Solves the MST problem by using kruskal
 */
class pgl_kruskal_t
{
	/*
	 * ==========================
	 * The Constructor runs the alogo
	 *
	 * The graph must be complete
	 */
public:
	pgl_kruskal_t(
		pgl_graph_t& 	graph) noexcept(false);



	/*
	 * =======================
	 * Soime Queries
	 */

	/**
	 * \brief	Returns the weight of the mst
	 */
	Weight_t
	getWeight() const;

	/*
	 * Makes thae same
	 */
	operator Weight_t() const;




	/*
	 * =========================
	 * Private Member
	 */
private:

	Weight_t 		m_mstWeight;
}; //End class(pgl_kruskal_t);











PGL_NS_END(graph)
PGL_NS_END(pgl)

#endif 	//End include guard
