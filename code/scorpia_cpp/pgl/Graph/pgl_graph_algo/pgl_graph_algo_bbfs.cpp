/**
 * \brief	This file implements the backwards breath first search algorithm.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph_algo/pgl_graph_algo_interface.hpp>
#include <pgl_graph_utility/pgl_graph_vertex_map.hpp>
#include <pgl_graph_utility/pgl_graph_colour.hpp>
#include <pgl_graph_utility/pgl_graph_tree_description.hpp>
#include <pgl_graph_algo/pgl_graph_algo_bbfs.hpp>
#include <pgl_graph_algo/pgl_graph_algo_bbfs_impl.hpp>


//Include STD
#include <functional>
#include <memory>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


PGL_NS_BEGIN(bbfs_internal)

//Forward declaration of the implementation of the bbfs
class pgl_algo_bbfs_impl;


PGL_NS_END(bbfs_internal)


/**
 * \brief	Construct a algoriothmn, that has not a source
 */
pgl_graphAlgo_bbfs_t::pgl_graphAlgo_bbfs_t(
		const Builder_t& 	builder) :
  pgl_graphAlgoBase_i(),
  m_impl(std::make_unique<Implementation_t>(builder))
{

};

pgl_graphAlgo_bbfs_t::pgl_graphAlgo_bbfs_t(
	pgl_graphAlgo_bbfs_t&&) noexcept = default;

pgl_graphAlgo_bbfs_t&
pgl_graphAlgo_bbfs_t::operator= (
	pgl_graphAlgo_bbfs_t&&) = default;

pgl_graphAlgo_bbfs_t::pgl_graphAlgo_bbfs_t(
	const pgl_graphAlgo_bbfs_t& o) :
  pgl_graphAlgoBase_i(o),
  m_impl(o.m_impl->clone())
{
};

pgl_graphAlgo_bbfs_t&
pgl_graphAlgo_bbfs_t::operator= (
	const pgl_graphAlgo_bbfs_t& o)
{
	pgl_graphAlgo_bbfs_t tmp(o);
	this->operator=(std::move(tmp));

	return *this;
};







/*
 * \brief	Firtual destructor
 */
pgl_graphAlgo_bbfs_t::~pgl_graphAlgo_bbfs_t() = default;


/*
 * =========================
 * Quering functions
 */


/*
 * \brief	This function returns true, if the algorithm has a source, where the bfs starts
 */
bool
pgl_graphAlgo_bbfs_t::hasSource() const noexcept
{
	return this->m_impl->hasSource();
};



pgl_graphAlgo_bbfs_t&
pgl_graphAlgo_bbfs_t::setSourceTo(
		const VertexID_t& newSource) noexcept(false)
{
	//Reset the state
	this->hook_resetState();

	//This function will invalidate the distacnes and so on.
	this->m_impl->setSourceTo(newSource);

	return *this;
};


/*
 * \brief	Returns the current source or invalid if no source was defined
 */
pgl_graphAlgo_bbfs_t::VertexID_t
pgl_graphAlgo_bbfs_t::getSource() const noexcept
{
	return m_impl->getSource();
};


/*
 * \brief	This function copies the found distances into the provided map.
 *
 * The map is reassociated
 */
const pgl_graphAlgo_bbfs_t&
pgl_graphAlgo_bbfs_t::getFoundDistances(
		DistanceMap_t& 		exportDistances) const
{
	this->m_impl->getFoundDistances(exportDistances);

	return *this;
};


/*
 * \brief	This function returns a description of the found bfs tree
 */
pgl_graphAlgo_bbfs_t::TreeDescription_t
pgl_graphAlgo_bbfs_t::getTreeDescription() const
{
	return this->m_impl->getTreeDescription();
};


/*
 * \brief	This function tests if all nodes are visited.
 *
 * This is done, by checking that all nodes have color black
 */
bool
pgl_graphAlgo_bbfs_t::allNodesWereVisited() const noexcept
{
	return this->m_impl->allNodesWereVisited();
};


/*
 * \brief	This function returns if the graph is a tree.
 *
 * A graph is a tree, if only edges where descovered, that where NOT rejected by the examine edge function.
 * taht leads to unkown vertices
 */
bool
pgl_graphAlgo_bbfs_t::isGraphATree() const noexcept
{
	return m_impl->isGraphATree();
};


/*
 * =======================
 * Protected internal functions
 */
/*
 * \brief	This is the preprocessing step of the algorithm
 *
 * This function resets everything such that the algorithm can rerun.
 * This overwrites any data saved.
 *
 * Notice, that the association with the gfraph is only done in the first run.
 * So, do not use the algorithjm after modifing the graph.
 *
 * If the source is not set an exception is generated
 */
bool
pgl_graphAlgo_bbfs_t::algo_pre_impl()
{
	return this->m_impl->algo_pre_impl();
};


/*
 * \brief	This is the actual processing step, think of it as kernel.
 *
 * Here the hard work is done.
 */
void
pgl_graphAlgo_bbfs_t::algo_impl()
{
	this->m_impl->algo_impl();

	//This information can not propagate to the outside
	//So we must set it manualy
	this->hook_setHaltedEarlierTo(m_impl->hasHaltedEarlier());

	return;
};


/*
 * \brief	This is the post processing step.
 *
 * Thsi fucntion checks if the color of all nodes is balck or white.
 * Firter smale tests are performed.
 */
bool
pgl_graphAlgo_bbfs_t::algo_post_impl()
{
	return m_impl->algo_post_impl();
};





PGL_NS_END(graph)
PGL_NS_END(pgl)









