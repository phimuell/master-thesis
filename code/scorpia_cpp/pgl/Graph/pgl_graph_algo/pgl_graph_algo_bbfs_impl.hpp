#pragma once
/**
 * \brief	This file conatins the definition of the implementation of the bbfs.
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>
#include <pgl_queue.hpp>

#include <pgl_graph/pgl_graph_config.hpp>
#include <pgl_graph/pgl_graph_graph.hpp>
#include <pgl_graph_algo/pgl_graph_algo_interface.hpp>
#include <pgl_graph_utility/pgl_graph_vertex_map.hpp>
#include <pgl_graph_utility/pgl_graph_colour.hpp>
#include <pgl_graph_utility/pgl_graph_tree_description.hpp>
#include <pgl_graph_algo/pgl_graph_algo_bbfs.hpp>
#include <pgl_graph_algo/pgl_graph_algo_bbfs_builder.hpp>


//Include STD
#include <functional>


PGL_NS_BEGIN(pgl)
PGL_NS_BEGIN(graph)


PGL_NS_BEGIN(bbfs_internal)



/**
 * \brief	This is pgl's version of the bbfs
 * \class 	pgl_graphAlgo_bbfs_impl
 *
 * It behaves the same way as described above.
 * It uses the pimpl idiom.
 * It also offers a builder class that provides named arguments.
 *
 * The algorithm needs a graph, we say that the algorithm (oject) has an associated graph, he operates on.
 * Further, this algorithm uses the standard run implementation.
 *
 * To check if the algorithm was a success, the algorithm tests all the nodes and if all have color Withe or black, then anything was good.
 *
 * Notice that the algorithm associates the internal maps only during the FIRST run.
 * So if you create an algorithm and then modify the graph.
 * Then the graph in the internal maps are not in sync anymore.
 *
 * This error is not detected.
 *
 * This is the implementation
 */
class pgl_graphAlgo_bbfs_impl
{
	/*
	 * ============================================
	 * Typedefs
	 */
public:
	//Vertex
	using Vertex_t 			= ::pgl::graph::pgl_vertex_t;			//!< This is the type of the vertex
	using Vertex_ptr 		= ::pgl::graph::pgl_vertex_t*;			//!< This is the type of a pointer to a vertex
	using Vertex_cptr 		= const ::pgl::graph::pgl_vertex_t*;		//!< This is the pointer to a constant vertex
	using Vertex_ref		= ::pgl::graph::pgl_vertex_t&;			//!< This is the type of a reference to a node
	using Vertex_cref		= const ::pgl::graph::pgl_vertex_t&;		//!< This is the constant reference to a vertex
	using VertexPayLoad_ptr		= ::pgl::graph::pgl_vertex_payload_t*;		//!< This is the payload of the vertex
	using VertexID_t 		= ::pgl::graph::pgl_vertexID_t;			//!< This is the type of the vertex ID

	//Connection
	using Connection_t 		= ::pgl::graph::pgl_connection_t;		//!< Type of the connection
	using Connection_ptr		= pgl_connection_t*;				//!< Pointer to a connection
	using Connection_cptr		= const pgl_connection_t*;			//!< Pointer to a constant connection
	using Connection_ref 		= pgl_connection_t&;				//!< Reference to connection
	using Connection_cref		= const pgl_connection_t&;			//!< Const reference to connection
	using ConnPayLoad_ptr		= ::pgl::graph::pgl_connection_payload_t*;	//!< Pointer to a vertex payload
	using ConnectionID_t 		= ::pgl::graph::pgl_edgeID_t;			//!< This is the ID that connections and edges uses

	//Edge
	using Edge_t 			= ::pgl::graph::pgl_edge_t; 			//!< Type of the edge
	using Edge_ptr			= pgl_edge_t*;					//!< Pointer to a edge
	using Edge_cptr			= const pgl_edge_t*;				//!< Pointer to a constant edge
	using Edge_ref 			= pgl_edge_t&;					//!< Reference to edge
	using Edge_cref			= const pgl_edge_t&;				//!< Const reference to edge
	using EdgeID_t 		= ::pgl::graph::pgl_edgeID_t;			//!< This is the ID that connections and edges uses

	//Graph
	using Graph_t			= ::pgl::graph::pgl_graph_t;			//!< The used graph type
	using Graph_ref 		= ::pgl::graph::pgl_graph_t&;			//!< Reference to the graph
	using Graph_cref		= const ::pgl::graph::pgl_graph_t&; 		//!< Const referecne to the graph
	using Graph_ptr 		= ::pgl::graph::pgl_graph_t*;			//!< pointer to the graph
	using Graph_cptr		= const ::pgl::graph::pgl_graph_t*; 		//!< Const pointer to the graph


	/*
	 * Typedefs of the visitors
	 */
	/// This is the type of the init_vertex_visitor
	using initVertexVisitor_t 	= pgl_graphAlgo_bbfs_t::initVertexVisitor_t;

	/// This is the type for the examine vertex visitor
	using examineVertexVisitor_t 	= pgl_graphAlgo_bbfs_t::examineVertexVisitor_t;

	/// This is the type for the edge_discovery_visitor
	using edgeDiscoveryVisitor_t 	= pgl_graphAlgo_bbfs_t::edgeDiscoveryVisitor_t;

	/// This is the tree_edge_visitor type
	using treeEdgeVisitor_t  	= pgl_graphAlgo_bbfs_t::treeEdgeVisitor_t;

	/// This is the gray_edge_visitor_type
	using grayEdgeVisitor_t 	= pgl_graphAlgo_bbfs_t::grayEdgeVisitor_t;

	/// This is the black_edge_visitor_type
	using blackEdgeVisitor_t 	= pgl_graphAlgo_bbfs_t::blackEdgeVisitor_t;

	/// This is the finish_node_visitor type
	using finishingNodeVisitor_t 	= pgl_graphAlgo_bbfs_t::finishingNodeVisitor_t;

	/*
	 * Normal Typedefs
	 */
	using Distance_t 		= ::pgl::graph::Distance_t;			//!< This is the type to describe distances
	constexpr static Distance_t DistInfinity 		= ::pgl::graph::Constants::DistanceInfinity;
	using Colour_t 			= ::pgl::graph::pgl_colour_t;			//!< This is the colour type
	using PredecessorMap_t 		= pgl_graphAlgo_bbfs_t::PredecessorMap_t;	//!< This is the map used to describes the predecessor relation ship
	using DistanceMap_t 		= pgl_graphAlgo_bbfs_t::DistanceMap_t;		//!< This is the type that is the distacne map
	using ColourMap_t 		= pgl_graphAlgo_bbfs_t::ColourMap_t;		//!< This is the colourmap
	using TreeDescription_t 	= pgl_graphAlgo_bbfs_t::TreeDescription_t;	//!< This is the tree description

	//This is the builder to build the algorithm
	using Builder_t 		= pgl_graphAlgo_bbfs_t::Builder_t;

	using GraphReference_t 		= std::reference_wrapper<Graph_t>;		//!< This is the copyable version of the reference, that is needed internaly
	using Queue_t 			= ::pgl::pgl_queue_t<VertexID_t>;

	//This is the implementation (this class) that is used by the containing algorithm
	using Implementation_t 		= pgl_graphAlgo_bbfs_t::Implementation_t;	//!< Implementation of the bbfs (this class)
	using Implementation_ptr 	= pgl_graphAlgo_bbfs_t::Implementation_ptr;	//!< This is the pointer to the implementation


	/*
	 * ====================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the constructor that allows to construct an algorithm that is associated with a graph.
	 *
	 * The passed graph is the graph that the algorithm is working on.
	 * It uses an builder to build the algorithm.
	 *
	 */
	pgl_graphAlgo_bbfs_impl(
		const Builder_t& 	builder) :
	  m_graph(builder.m_graph),
	  m_source(builder.m_source),
	  m_distances(),
	  m_colours(),
	  m_vaeter(),
	  m_initVertexVisitor(builder.m_initVertexVisitor),
	  m_examineVertexVisitor(builder.m_examineVertexVisitor),
	  m_edgeDiscoverVisitor(builder.m_edgeDiscoverVisitor),
	  m_treeEdgeVisitor(builder.m_treeEdgeVisitor),
	  m_grayEdgeVisitor(builder.m_grayEdgeVisitor),
	  m_balckEdgeVisitor(builder.m_balckEdgeVisitor),
	  m_finNodeVisitor(builder.m_finNodeVisitor),
	  m_isTree(true),
	  m_hasHaltedEarlier(false)
	{
		//Enforce that the graph is complete
		if(this->m_graph.get().isComplete() == false)
		{
			throw PGL_EXCEPT_LOGIC("The graph is not complete.");
		};
	};


	/**
	 * \brief	Default constructor
	 */
	pgl_graphAlgo_bbfs_impl() noexcept = delete;


	/**
	 * \brief	Copy constructor
	 */
	pgl_graphAlgo_bbfs_impl(
		const pgl_graphAlgo_bbfs_impl&) = default;

	/**
	 * \brief	Move constructor
	 */
	pgl_graphAlgo_bbfs_impl(
		pgl_graphAlgo_bbfs_impl&&) noexcept = delete;

	/**
	 * \brief	Default copy assignment
	 */
	pgl_graphAlgo_bbfs_impl&
	operator= (
		const pgl_graphAlgo_bbfs_impl&) = delete;


	/**
	 * \brief	Default move assignment
	 */
	pgl_graphAlgo_bbfs_impl&
	operator= (
		pgl_graphAlgo_bbfs_impl&&) noexcept = delete;

	/**
	 * \brief	Firtual destructor
	 */
	~pgl_graphAlgo_bbfs_impl() = default;


	/**
	 * \brief	This is a clone function.
	 *
	 * It returns an implementation pointer, that is initialized with the content of this
	 */
	 Implementation_ptr
	 clone() const
	 {
		 return std::make_unique<Implementation_t>(*this);
	 }; //End clone function


	/*
	 * =========================
	 * Quering functions
	 */


	/**
	 * \brief	This function returns true, if the algorithm has a source, where the bfs starts
	 */
	bool
	hasSource() const noexcept
	{
		return m_source.isValid();
	};


	/**
	 * \brief	Set source to new Source
	 *
	 * Unlike the visitors and the graph it is possible to chenage the source of the bbfs, without the need to reconstruct the algorithm.
	 * Please notice that this function throws, if the source is not set before.
	 * Please clear the source before calling this fucntion.
	 * This is node as a safe gurard.
	 *
	 * The containers of the stored quantities are _not_invalidated.
	 * The reason is, that before we can set the new source we have to call the clear function.
	 * And this fucntion invalidates everything allready.
	 *
	 * \param 	newSource	New source, start point of the bbfs
	 */
	pgl_graphAlgo_bbfs_impl&
	setSourceTo(
		const VertexID_t& newSource) noexcept(false)
	{
		if(m_source.isValid() == true)
		{
			throw PGL_EXCEPT_LOGIC("Can not set the source, unless it is still valid");
		};

		if(m_graph.get().getCNode(newSource)->isValid() == false)
		{
			throw PGL_EXCEPT_LOGIC("The source seems not to be in the graph.");
		};

		//Set the new sopurce
		m_source = newSource;

		return *this;
	};




	/**
	 * \brief	This function invalidetes the containers that stores the computed quantiies.
	 *
	 * This function simply calls the assign functions of the container.
	 * The colors are set to white, the distances are set to infinity and the opredecessors to invalid.
	 *
	 */
	void
	invalidateContainers()
	{
		//Invalidate the conatiners
		m_vaeter.assign(VertexID_t::INVALID());
		m_colours.assign(Colour_t::White());
		m_distances.assign(Constants::DistanceInfinity);

		return;
	}; //End invalidate containers

	/**
	 * \brief	This function reassociates the containers with the graph.
	 *
	 * This fucntion calls the resize fuinction, with the graph, stored in m_graph.
	 * The colors are set to white, the distances are set to infinity and the opredecessors to invalid.
	 */
	void
	associateContainers()
	{
		m_vaeter.resize(m_graph.get(), VertexID_t::INVALID());
		m_colours.resize(m_graph.get(), Colour_t::White());
		m_distances.resize(m_graph.get(), Constants::DistanceInfinity);

		return;
	}; //End reassociate containers

	/**
	 * \brief	This fucntion returns true, if the containers are associated with a graph.
	 *
	 * This fucntion calls isAssociated on each container.
	 * And returns true if all have returned true
	 *
	 * Return that this function can not be used to check if only some of the comntainer are associated.
	 */
	bool
	areContainerAssociated() const
	{
		return (m_vaeter.isAssociated() && m_colours.isAssociated() && m_distances.isAssociated()) ? true : false;
	};

	/**
	 * \brief	sets the source of *this to invalid.
	 *
	 * Also invalidate the containers.
	 * This function also resets the is tree value and the hashalted earlier value if set.
	 * This is done, because if we clear the source, we have nothing more.
	 */
	pgl_graphAlgo_bbfs_impl&
	clearSource() noexcept
	{
		m_source = VertexID_t::INVALID();

		//Invalidate the conatiners
		this->invalidateContainers();

		//Set the states to initial values
		m_hasHaltedEarlier = false;
		m_isTree = false;

		return *this;
	};



	/**
	 * \brief	Returns the current source or invalid if no source was defined
	 */
	VertexID_t
	getSource() const noexcept
	{
		return m_source;
	};


	/**
	 * \brief	This function copies the found distances into the provided map.
	 *
	 * The map is reassociated
	 */
	void
	getFoundDistances(
		DistanceMap_t& 		exportDistances) const
	{
		exportDistances = m_distances;

		return;
	};


	/**
	 * \brief	This function returns a description of the found bfs tree
	 */
	TreeDescription_t
	getTreeDescription() const
	{
		return TreeDescription_t(m_graph.get(), m_vaeter, m_distances);
	};


	/**
	 * \brief	This function tests if all nodes are visited.
	 *
	 * This is done, by checking that all nodes have color black
	 */
	bool
	allNodesWereVisited() const noexcept
	{
		return ::std::all_of(m_colours.cbegin(), m_colours.cend(), [](const Colour_t& c) -> bool { return ((c.isBlack() || c.isWhite()) ? true : false);});
	};


	/**
	 * \brief	This function returns if the graph is a tree.
	 *
	 * A graph is a tree, if only edges where descovered, that where NOT rejected by the examine edge function.
	 * taht leads to unkown vertices
	 */
	bool
	isGraphATree() const noexcept
	{
		return m_isTree;
	};


	/**
	 * \brief	Is used to set halted earlier
	 */
	void
	set_HaltedEarlier() noexcept
	{
		m_hasHaltedEarlier = true;
		return;
	};

	bool
	hasHaltedEarlier() noexcept
	{
		return m_hasHaltedEarlier;
	};


	/**
	 * \brief	This is the preprocessing step of the algorithm
	 *
	 * This function resets everything such that the algorithm can rerun.
	 * This overwrites any data saved.
	 *
	 * Notice, that the association with the gfraph is only done in the first run.
	 * So, do not use the algorithjm after modifing the graph.
	 *
	 * If the source is not set an exception is generated
	 */
	bool
	algo_pre_impl()
	{

		if(m_source.isInvalid() || (m_graph.get().getCNode(m_source)->isValid() == false))
		{
			throw PGL_EXCEPT_LOGIC("The source is not correct.");
		};

		if(m_graph.get().isComplete() == false)
		{
			throw PGL_EXCEPT_LOGIC("Graph is not complete.");
		};

		//Since it is not possible, to do iot by inheritance,
		//we set here the has halted hearlier back to false
		m_hasHaltedEarlier = false;
		m_isTree = true;	//Each graph is a tree until we find a conter edge

		//Test if the container are associated with the graph
		if(this->areContainerAssociated() == false)
		{
			//Need to associate with the graph
			this->associateContainers();
		}
		else
		{
			//Needs to invalidate //Set to default value// of the container
			this->invalidateContainers();
		};

		const auto n = m_graph.get().nVertices();
		if(m_vaeter.size() != n)
		{
			return false;
		};

		if(m_distances.size() != n)
		{
			return false;
		};

		if(m_colours.size() != n)
		{
			return false;
		};

		//Apply the init vertex visitor
		for(Vertex_ref v : m_graph.get().Nodes())
		{
			if(m_initVertexVisitor(v) == false)
			{
				return false;
			};
		};

		return true;
	};


	/**
	 * \brief	This is the actual processing step, think of it as kernel.
	 *
	 * Here the hard work is done.
	 */
	void
	algo_impl()
	{
		Queue_t vQueue;	//This is the queue for the vertices
		Graph_ref Graph = m_graph.get();

		//Handle the source
		{
			Vertex_ref Source = *(Graph.getNode(m_source));

			this->set_colourTo(Source, Colour_t::Grey());
			this->set_distanceTo(Source, 0);
			this->set_predecessorTo(Source, Source);

			vQueue.push(m_source);
		}; //ENd handle source

		pgl_assert(vQueue.size() == 1);



		//This is the iteration handler
		while(vQueue.empty() == false)
		{
			//Head of the queu
			const VertexID_t currID = vQueue.peek();
			vQueue.pop();	//Remove from the queue

			Vertex_ref       currV  = *(Graph.getNode(currID));

			bool ignoreThis = false;

			//Call the descover/processing vertex
			switch(m_examineVertexVisitor(currV))
			{
			  case pglAlgoCommand_e::abortAlgo:
			  	throw PGL_EXCEPT_LOGIC("Command to abort was given");
			  	break;

			  case pglAlgoCommand_e::stopAlgo:
			  	//The algorithm should stop
			  	this->set_HaltedEarlier();
			  	return;
			  	break;

			  case pglAlgoCommand_e::ignoreEntity:
			  	ignoreThis = true;
			  	break;

			  case pglAlgoCommand_e::acceptEntity:
			  	//Do nothing
			  	break;

			  case pglAlgoCommand_e::INVALID:
			  	throw PGL_EXCEPT_LOGIC("Invalid was returned as command");
			  	break;

			  default:
			  	throw PGL_EXCEPT_RUNTIME("Command unknown.");
			  	break;
			}; //End switch: examine vertex


			//Test of we can ignore this
			if(ignoreThis == true)
			{
				//Set the colur to black
				this->set_colourTo(currV, Colour_t::Black());
				continue;
			};


			const Distance_t distCurr = this->get_distanceOf(currV);
			const Distance_t nextDist = distCurr + 1;

			//Now start to process the neigbourhoods
			for(Edge_ref eNeigh : currV.inERIterator())
			{
				switch(this->process_neighbourNode(eNeigh))
				{
				  case pglAlgoCommand_e::stopAlgo:
				  	  //Stop the algorithm
				  	  this->set_HaltedEarlier();
				  	  return;

				  case pglAlgoCommand_e::ignoreEntity:
				  	  //Missuse of enum
				  	  break;

				  case pglAlgoCommand_e::acceptEntity:
				  {
				  	Vertex_ref 	newNode = eNeigh.getOtherNode();
				  	const VertexID_t newID  = newNode.getID();

				  	//Set the parameters
				  	this->set_colourTo(newNode, Colour_t::Grey());
				  	this->set_predecessorTo(newNode, currV);
				  	this->set_distanceTo(newNode, nextDist);

				  	//Enqueu
				  	vQueue.push(newID);
				  };
				  break;

				  default:
				  	throw PGL_EXCEPT_RUNTIME("TZTZT");
				  	break;
				}; //End switch
			}; //ENd for(vNeigh): iterating over the neighbourhood




			//Call the finishing node visitor
			switch(m_finNodeVisitor(currV))
			{
			  case pglAlgoCommand_e::abortAlgo:
			  	throw PGL_EXCEPT_LOGIC("Command to abort was given");
			  	break;

			  case pglAlgoCommand_e::stopAlgo:
			  	//The algorithm should stop
			  	this->set_HaltedEarlier();
			  	return;
			  	break;

			  case pglAlgoCommand_e::ignoreEntity:
			  	//Do nothing
			  	break;

			  case pglAlgoCommand_e::acceptEntity:
			  	//Do nothing
			  	break;

			  case pglAlgoCommand_e::INVALID:
			  	throw PGL_EXCEPT_LOGIC("Invalid was returned as command");
			  	break;

			  default:
			  	throw PGL_EXCEPT_RUNTIME("Command unknown.");
			  	break;
			}; //End switch: examine vertex


			//Make the vertex black
			this->set_colourTo(currV, Colour_t::Black());
		}; //End queue wile




		return;
	};


	/**
	 * \brief	This is the post processing step.
	 *
	 * Thsi fucntion checks if the color of all nodes is balck or white.
	 * Firter smale tests are performed.
	 */
	bool
	algo_post_impl()
	{
		auto it_v = m_vaeter.cbegin();
		auto it_d = m_distances.cbegin();
		const auto ende_c = m_colours.cend();
		const bool preExited = this->hasHaltedEarlier();
		for(ColourMap_t::const_iterator it_c = m_colours.cbegin(); it_c != ende_c; ++it_c)
		{
#ifndef PGL_NDEBUG
			const bool isBlack = it_c->isBlack();	(void)isBlack;
			const bool isWhite = it_c->isWhite();	(void)isWhite;
			const bool isGray = it_c->isGrey();	(void)isGray;
			const auto ke = it_c.key();		(void)ke;
#endif

			if(it_c->isWhite())
			{
				//THe node is white, this mean the node was not found
				if(it_v->isValid() || *it_d != DistInfinity)
				{
					//The fater is known or the distance is not infity, both cases an error
					return false;
				};
			}
			else if(it_c->isBlack())
			{
				//The node is balc also kknown
				if((*it_d == DistInfinity) || (it_v->isInvalid()) || (m_colours.at(*it_v).isBlack() == false))
				{
					return false;
				};
			}
			else if(preExited == false)
			{
				//NOthing holds
				return false;
			};


			//Increment
			++it_v;
			++it_d;
		}; //End it_c


		//Anything is good
		return true;
	};


	/*
	 * ============================
	 * Private helper functions
	 */
private:
	/**
	 * \brief	This function sets the color of the node to the specified value
	 */
	void
	set_colourTo(
		Vertex_t& 		v,
		const Colour_t& 	c)
	{
		switch(m_colours.at(v.getID()).getColourPart())
		{
		  case COLOUR_WITHE:
		  	if(c.isGrey() == false)
			{
				throw PGL_EXCEPT_LOGIC("Wrong color order");
			};
			break;

		  case COLOUR_GREY:
		  	if(c.isBlack() == false)
			{
				throw PGL_EXCEPT_LOGIC("Wrong colour order");
			};
			break;

		  default:
		  	throw PGL_EXCEPT_LOGIC("What are you doing.");
		}; //End switch

		m_colours.at(v.getID()) = c;

		return;
	};

	/**
	 * \brief	Return the color of v
	 */
	Colour_t
	get_colourOf(
		const Vertex_t& 	v) const
	{
		return m_colours.at(v.getID());
	};


	/**
	 * \brief	set the distance of we to d
	 */
	void
	set_distanceTo(
		Vertex_t& 		v,
		const Distance_t 	d)
	{
		if(m_distances.at(v.getID()) == DistInfinity)
		{
			m_distances.at(v.getID()) = d;
		}
		else
		{
			throw PGL_EXCEPT_LOGIC("Tried to set an allrready known distance.");
		};
		return;
	};

	/**
	 * \brief	Return distance of
	 */
	Distance_t
	get_distanceOf(
		const Vertex_t& 	v) const
	{
		return m_distances.at(v.getID());
	};


	/**
	 * \brief	Set predecessor of
	 */
	void
	set_predecessorTo(
		Vertex_ref 	v,
		Vertex_cref 	predec)
	{
		if(m_vaeter.at(v.getID()).isValid() == true)
		{
			throw PGL_EXCEPT_LOGIC("The predecessor is allready set.");
		};

		//TEst if have connected
		if(m_graph.get().getCNode(v.getID())->pointsTo(predec.getID()) == false && !(m_source == v.getID() && m_source == predec.getID()))
		{
			throw PGL_EXCEPT_LOGIC("The node does not have a connection with the predecessor.");
		};

		m_vaeter.at(v.getID()) = predec.getID();

		return;
	};

	/**
	 * \brief	Returns the predecessor of
	 */
	VertexID_t
	get_predecessorOf(
		const Vertex_t& v)
	{
		return m_vaeter.at(v.getID());
	};


	/**
	 * \brief	This nodes handles the edge handeling.
	 *
	 * It modiefies the queue if needed and calls all the functions that we need to process
	 */
	pglAlgoCommand_e
	process_neighbourNode(
		Edge_ref 	currEdge)
	{
		//Call the descover/processing
		switch(m_edgeDiscoverVisitor(currEdge))
		{
		  case pglAlgoCommand_e::abortAlgo:
			throw PGL_EXCEPT_LOGIC("Command to abort was given");
			break;

		  case pglAlgoCommand_e::stopAlgo:
			//The algorithm should stop
			this->set_HaltedEarlier();
			return pglAlgoCommand_e::stopAlgo;
			break;

		  case pglAlgoCommand_e::ignoreEntity:
			return pglAlgoCommand_e::ignoreEntity;
			break;

		  case pglAlgoCommand_e::acceptEntity:
			//Do nothing
			break;

		  case pglAlgoCommand_e::INVALID:
			throw PGL_EXCEPT_LOGIC("Invalid was returned as command");
			break;

		  default:
			throw PGL_EXCEPT_RUNTIME("Command unknown.");
			break;
		}; //End switch: examine vertex

		/*
		 * We have decided to accept the entity
		 */


		//Now we will call the approprioate fuinction
		pglAlgoCommand_e command = pglAlgoCommand_e::INVALID;	//This is the return value of the distance

		//This value indicates, if we have found a new node, this means a tree edge or a back edge,
		// Whih is a edge, that leads to, or precice comes from, a node, that is knwon (black/gray).
		//If the user decided to accept this entity, the veertex will be added to the queue
		bool isTreeEdge = false;

		/*
		 * This variables transport the information that we have found a backedge, which is equivalent to having found a cycle.
		 * But since we allow the user to ignore an entity, we could ignore that connection.
		 * So we can not set m_isTree directly to false!
		 */
		 bool hasFoundABackEdge = false;


		switch(get_colourOf(currEdge.getOtherNode()).getColourPart())
		{

		  case COLOUR_WITHE:
		  	command = m_treeEdgeVisitor(currEdge);

		  	//We have found a tree edge
		  	isTreeEdge = true;
		  	break;

		  case COLOUR_GREY:
		  	command = m_grayEdgeVisitor(currEdge);

		  	/*
		  	 * Here we are sure that we have found a cycle,
		  	 * because when we examine the edge, that leads us back to the predecessor, the predecessor must have color black
		  	 */
		  	hasFoundABackEdge = true;

		  	break;

		  case COLOUR_BLACK:
		  {
			const VertexID_t predecessor = this->get_predecessorOf(currEdge.getOwningNode());	//This is the vorgänger, des aktuellen nodes
			const bool newNodeIsPredecessor = (predecessor == currEdge.getOtherNodeID());
		  	command = m_balckEdgeVisitor(currEdge, newNodeIsPredecessor);

		  	/*
		  	 * Here we can not simply say, that we have found a cycle.
		  	 * Because a node could also reach its predeccessor in case of undirected connections.
		  	 * So we test this, in the case we have found a back connection, we test if this will lead to the predecessor.
		  	 *
		  	 * NOTE:
		  	 * This approche has some problems in the case of multiple connection between nodes,
		  	 * but we ignore this
		  	 */
		  	 if(newNodeIsPredecessor == false)
			 {
				 //The edge goes not to the predecessor, so we have found a cycle!
				 hasFoundABackEdge = true;
			 }; // End if: We have found a cycle
		  }
		  	break;

		  default:
		  	throw PGL_EXCEPT_RUNTIME("Wrong collor");
		  	break;
		}; //End switch




		switch(command)
		{
		  case pglAlgoCommand_e::abortAlgo:
			throw PGL_EXCEPT_LOGIC("Command to abort was given");
			break;

		  case pglAlgoCommand_e::stopAlgo:
			//The algorithm should stop
			this->set_HaltedEarlier();
			return pglAlgoCommand_e::stopAlgo;
			break;

		  case pglAlgoCommand_e::ignoreEntity:
			return pglAlgoCommand_e::ignoreEntity;
			/*
			 * Since we ignore this entity, we do not set the m_isTree Variable, since the user has decided to ignore that.
			 */
			break;

		  case pglAlgoCommand_e::acceptEntity:
			/*
			 * Now we can manage the is tree property
			 * The user has decided to accept this edge, so if it is a back edge, also a cycle forming edge, we must det the isTree edge to false
			 */
			 if(hasFoundABackEdge == true)
			 {
				 m_isTree = false;
			 };

			/*
			 * This if is here to decide what we do.
			 * since the isAdmissible controles if a node can enter the queue we must here decide.
			 */

			if(isTreeEdge == true)
			{
				return pglAlgoCommand_e::acceptEntity;
			}
			else
			{
				return pglAlgoCommand_e::ignoreEntity;
			};
			break;

		  case pglAlgoCommand_e::INVALID:
			throw PGL_EXCEPT_LOGIC("Invalid was returned as command");
			break;

		  default:
			throw PGL_EXCEPT_RUNTIME("Command unknown.");
			break;
		}; //End switch: examine vertex


		return pglAlgoCommand_e::INVALID;
	};





	/*
	 * ====================
	 * Private Variables
	 */
private:
	GraphReference_t 			m_graph;	//!< A referenbce to the graph
	VertexID_t 				m_source;	//!< This is the start of thew bbfs
	DistanceMap_t 				m_distances;	//!< All the distances
	ColourMap_t 				m_colours;	//!< Colours of the nodes
	PredecessorMap_t 			m_vaeter;	//!< Predeccessors


	//THe visitors
	initVertexVisitor_t 			m_initVertexVisitor;
	examineVertexVisitor_t 			m_examineVertexVisitor;
	edgeDiscoveryVisitor_t 			m_edgeDiscoverVisitor;
	treeEdgeVisitor_t 			m_treeEdgeVisitor;
	grayEdgeVisitor_t 			m_grayEdgeVisitor;
	blackEdgeVisitor_t 			m_balckEdgeVisitor;
	finishingNodeVisitor_t 			m_finNodeVisitor;

	bool 					m_isTree;	//!< Thsi value indicates if the tree is a link
	bool 					m_hasHaltedEarlier;	//!> This indicates, if the algorithm has halted erlier
							//!< This value must then be called by the container
}; // end class(pgl_graphAlgo_bbfs_impl)


PGL_NS_END(bbfs_internal)

PGL_NS_END(graph)
PGL_NS_END(pgl)









