#pragma once
/**
 * \brief	This file is the core file that connects PGL with Eigen.
 *
 * This file has to be included _before_ any other Eigen file is included.
 * This is done to be able to configure Eigen.
 * This may or may not include Eigen files on its own, thus
 * the user should not depend if this file is included or not.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>





// =================================
//    MAKE YOUR SETTINGS HERE










// =================================
// DO NOT EDIT THE FILE BELOW THIS LINE

//We include the Eigen/Core file, here.
//Thus it is not possible to change something
#include <Eigen/Core>


PGL_NS_START(pgl)

/**
 * \typedef 	Index_t
 * \brief 	The index type.
 *
 * In C++ you use std::size_t for indexing.
 * In PGL Size_t is used.
 * However in Eigen you use a singed integer for indexing.
 * Since we will work with Eigen, we will extend PGL.
 * This extension involves the definition of a new index type,
 * that is compatible with Eigen.
 *
 * See also "https://eigen.tuxfamily.org/dox-devel/namespaceEigen.html#a62e77e0933482dafde8fe197d9a2cfde"
 */
using Index_t = ::Eigen::Index;

PGL_NS_END(pgl)

