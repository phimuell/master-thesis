#pragma once
#ifndef PGL_UTIL_PGL_FORMAT_UTIL_HPP
#define PGL_UTIL_PGL_FORMAT_UTIL_HPP
/**
 * \brief	This file contains functions that are very small, and helps to deal with output operations.
 *
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>


//Include std
#include <type_traits>



PGL_NS_START(pgl)



/**
 * \brief	This function can be used to format a floating point number
 *
 * The user can select how many digit after the point he want to see.
 */
template<typename T>
inline
std::enable_if_t<::std::is_floating_point<T>::value, T>
pgl_cutFloat(
	const T& 	number,
	const Size_t 	Digits = 2) noexcept(false)
{
	//Handling the special case that no thing are neded
	if(Digits == 0)
	{
		return T(::pgl::sSize_t(number));
	};

	T tmp;

	switch(Digits)
	{
	  case 1: tmp =   10.0; break;
	  case 2: tmp =  100.0; break;
	  case 3: tmp = 1000.0; break;

	  default:
		  //Is this necessary???
		tmp = 10.0;
		for(Size_t i = 0; i != Digits; ++i)
		{
			tmp *= T(10.0);
		}; //end for(i)
	}; //End switch

	return (T((::pgl::sSize_t(number * tmp)) / tmp));
};
//ENd function :pgl_cutFloat





PGL_NS_END(pgl)


#endif
