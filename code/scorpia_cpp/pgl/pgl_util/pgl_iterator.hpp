#pragma once
/**
 * \brief	This fule is an extension to the iterator library provided by the std.
 */


//Include PGL
#include <pgl_core.hpp>


//Include STD
#include <iterator>
#include <type_traits>


PGL_NS_START(pgl)


/**
 * \brief	This class is used to detect if something is at least an iterator of a given class
 * \class 	pgl_is_at_least_category_t
 *
 * The if typeTag is the output_iterator, then the value will be always false, but the meber,
 * isOutput is true.
 *
 * \tparam 	minCategory		The minimal category that is allowed
 * \tparam 	type_tag 		The tag of the type
 */
template<
	class minCategory,
	class typeTag>
class pgl_is_at_least_category_t
{
	/*
	 * Typedefs
	 */
	//This tests if the tag is realy a tag or if it is an iterator, in that case, we use the traits class to get the category
	using realTypeTag = ::std::conditional_t<
		::std::is_base_of<std::input_iterator_tag, typeTag>::value ||
		::std::is_base_of<std::output_iterator_tag, typeTag>::value,
		typeTag,
		typename ::std::iterator_traits<typeTag>::iterator_category>;

	/*
	 * ===================
	 * Here are the results
	 */
public:
	constexpr static bool isOutput = ::std::is_same<realTypeTag, ::std::output_iterator_tag>::value;
	constexpr static bool value    = ::std::is_base_of<minCategory, realTypeTag>::value;
	using iterator_category        = realTypeTag;
}; //End class(pgl_is_at_least_category_t)


/**
 * \brief	 This class is a fast access to the result
 */
template<
	class minCategory,
	class typeTag>
static constexpr bool pgl_is_at_least_category = pgl_is_at_least_category_t<minCategory, typeTag>::value;




PGL_NS_END(pgl)






