#pragma once
/**
 * \brief	This files implements some small utility functions
 *
 * This is a collecting header for small functions, that are so small that they do not deserve their own header.
 * On the other hand they are the everyday task, so their functionality should be offered
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>
#include <pgl_int.hpp>

//Include boost
//#include <boost/functional/hash.hpp>


PGL_NS_BEGIN(pgl)




/**
 * \brief	Test three values to equality
 *
 * This function takes three values and test if they are the same.
 *
 * \param 	val1
 * \param 	val2
 * \param 	val3
 *
 * All three types must be integers
 */
template<
	typename T1,
	typename T2,
	typename T3>
constexpr
bool
threeEqual(
	const T1& 	val1,
	const T2& 	val2,
	const T3& 	val3) noexcept
{
	return val1 == val2 ? (val1 == val3 ? true : false) : false;
};


/**
 * \brief	This function performs a check if all four values are equal.
 */
template<
	typename T1,
	typename T2,
	typename T3,
	typename T4>
constexpr
bool
fourEqual(
	const T1& 	val1,
	const T2& 	val2,
	const T3& 	val3,
	const T4& 	val4)
{
	return (val1 == val2 && val3 == val4 && val2 == val4) ? true : false;
};

PGL_NS_END(pgl)


/*
 * ==================
 * Include specific implementations
 */

//Include the hash
#include <pgl_util/pgl_hash.hpp>











