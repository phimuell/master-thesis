/**
 * \brief	This file implements some funnctions of the phased event counter.
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>

#include "./pgl_phasedEventCounter.hpp"


//Include STD
#include <iostream>
#include <iomanip>


PGL_NS_START(pgl)


std::ostream&
pgl_phasedEventCounter_t::printFullReport(
	std::ostream& 		o,
	const Size_t 		digits)
 const
{
	if(this->m_eDist.empty() )
	{
		o << "No iterations were recorded.\n";
		return o;
	};

	const auto startWidth = o.width();	//Get the width of the stream at the beginning
	const auto fillChar   = o.fill();	//Get the current fill character

	//Set the width parameter to zero, no minimum, and the fill character to the space
	o.width(0);
	o.fill(' ');

	//We now compute the fill witdh that is optimal for the iteration rages
	const Size_t optFill  = Size_t(std::floor(std::log10(m_itCount))) + 1;	//This will give us the number of digits

	//Optimal range for the numbers of events
	const Size_t optFillE = Size_t(std::floor(std::log10(m_phaseDur))) + 1;

	//Writting the total
	o
		<< "Total iteration " << this->nIterations()
		<< ",  total events " << this->nEvents()
		<< ",  ~  " << this->getTotalFraction(digits) << "\n";

	const Size_t N = m_eDist.size();
	for(Size_t i = 0; i != N; ++i)
	{
		const Size_t startIt = i * m_phaseDur + 1;
		const Size_t  endeIt = ((i + 1) != N) ? ( (i + 1) * m_phaseDur ) : (m_itCount);	//Are we in the past phase
		o
			<< "\t"
			<< std::right
			<< std::setw(optFill) << startIt
			<< " - "
			<< std::setw(optFill) << endeIt
			<< ": "
			<< "events " << std::setw(optFillE) << m_eDist.at(i)
			<< ", ~ " << this->getFraction(i, digits)
			<< "\n";
	}; //End for(i):

	//Restore states
	o.width(startWidth);
	o.fill(fillChar);

	return o;
}; //End: full report


PGL_NS_END(pgl)

