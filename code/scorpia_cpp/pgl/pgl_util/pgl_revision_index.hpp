#pragma once
/**
 * \brief	This file implements a revision index.
 *
 * This is a simple method of keeping track of something.
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>


//Include STD
#include <iostream>
#include <limits>


PGL_NS_START(pgl)


//Forward declaration of the index
class pgl_revision_index_t;


/**
 * \brief	The reviosion index (below) is not suited for storing a certain pint in time, for that this class is used
 * \class 	pgl_revision_state_t
 *
 * This class can only be constructed form a revion index it is unmodifable and used to store a certain index of the reviosion.
 *
 * It can be default constructed, and is at that point invalid
 */
class pgl_revision_state_t
{
	/*
	 * ==================
	 * Typedef
	 */
public:
	using Base_t 				= ::pgl::uInt64_t;				//!< This is the base type
	static constexpr Base_t INVALID 	= ::std::numeric_limits<Base_t>::max();		//!< This is the maximum

	/*
	 * ====================
	 * Constructors
	 *
	 * All constructor behaves the same, that they copies the index form src into this.
	 * Also the move.
	 * The default constructor sets this to invalid
	 */
	inline
	pgl_revision_state_t() noexcept :
	  m_index(INVALID)
	{};

	inline
	pgl_revision_state_t(
		const pgl_revision_state_t& src) noexcept :
	  m_index(src.m_index)
	{};

	inline
	pgl_revision_state_t(
		pgl_revision_state_t&& src) noexcept :
	  m_index(src.m_index)
	{};

	inline
	pgl_revision_state_t&
	operator= (
		const pgl_revision_state_t& src) noexcept
	{
		m_index = src.m_index;

		return *this;
	};

	inline
	pgl_revision_state_t&
	operator= (
		pgl_revision_state_t&& src) noexcept
	{
		m_index = src.m_index;

		return *this;
	};

	inline
	~pgl_revision_state_t() noexcept = default;

	/**
	 * \brief	This is a private constructor that can construct a state form a base type.
	 *
	 * It is private and the index is a friend
	 */
private:
	inline
	explicit
	pgl_revision_state_t(
		const Base_t i) noexcept :
	  m_index(i)
	{
		pgl_assert(i == INVALID);
	};


	/*
	 * =======================
	 * Functions
	 */
public:
	/**
	 * \brief	Returns true if this is invalid
	 */
	inline
	bool
	isInvalid() const noexcept
	{
		return m_index == INVALID;
	};//End is invalid


	/**
	 * \brief	Retrurns true if *this is valid
	 */
	inline
	bool
	isValid() const noexcept
	{
		return m_index != INVALID;
	}; //End is valid

	/**
	 * \brief	Retruns the stored index as type base
	 */
	inline
	Base_t
	getBase() const noexcept
	{
		return m_index;
	};
	//End function getBase



	/*
	 * =============================
	 * Operators
	 */
public:
	/**
	 * \brief	compares *this with rhs if the reviosions are the same
	 */
	inline
	bool
	operator== (
		const pgl_revision_state_t& rhs) noexcept
	{
		return (this->m_index == rhs.m_index) ? true : false;
	}; //End equal operator


	/**
	 * \brief	This is a templated equal function, that takes every argument and copares it with comthing of type base
	 */
	template<class T>
	bool
	equal(
		const T& t) const noexcept
	{
		return (((const Base_t)(m_index)) == t) ? true : false;
	}; //Enc equal operator


	/**
	 * \brief	Allow explicite convertuion to the Base_t type
	 */
	inline
	explicit
	operator Base_t() const noexcept
	{
		return m_index;
	};


	/**
	 * \brief	This generates an outstream operator
	 */
	friend
	std::ostream& operator<<(
		std::ostream& os,
		const pgl_revision_state_t& i)
	{
		return (os << i.m_index);
	}; //End of outstream operator


	//Make the revision index a friend of this, such that it can use the constructor
	friend
	class ::pgl::pgl_revision_index_t;


	/*
	 * =========================
	 * Private variables
	 */
private:
	Base_t 		m_index;	//!< The index of this
}; //End class pgl_revioion_state_t



/**
 * \brief	A simple reviosion index
 * \class 	pgl_revision_index_t
 *
 * This index can only be incremented.
 * It implements the constructor such that they behave correctly.
 *
 * On Copy:
 * If the index is copied, the value (of the new) index is not copied but set to zero.
 * The index of the one we have ciopied form is increemented by one if it is a constant object and unmodified else.
 *
 * On Moving:
 * This behaves differently than the copy case, since when we move A into B, we expect that B is now just as A.
 * So the reviosion index of B is the one of A plus one.
 * We also set the reviosion index of A to zero, since the content that was once in A has gone, so setting it to zero reflects that state.
 *
 * ThreadSafty:
 * Not that this implementation is not thread safty at all.
 * Making it thread safty, using atomic instruction would increase the overhead of this class.
 *
 * Overflow:
 * The functions are able to handle overflow.
 *
 */
class pgl_revision_index_t
{
	/*
	 * ==================
	 * Typedef
	 */
public:
	using State_t 				= ::pgl::pgl_revision_state_t;	//!< This is the state that is inmputable that is used to store a certen reviosion
	using Base_t 				= State_t::Base_t;		//!< This is the base type
	static constexpr Base_t INVALID 	= State_t::INVALID;		//!< This is invalid



	/*
	 * ==========================
	 * Constructor
	 */
public:
	/**
	 * \brief	Default constructor
	 *
	 * Initializes the index to zero
	 */
	pgl_revision_index_t() :
	  m_index(0)
	{};


	/**
	 * \brief	Copy constructor for constant source
	 *
	 * This is the copy constructor for constant sources.
	 * Here we initialize the index of *this with zero and leave the index of src untouched
	 *
	 * \param	src 	The index we copied form
	 */
	inline
	pgl_revision_index_t(
		const pgl_revision_index_t& src) noexcept :
	  m_index(0)
	{ (void)src; };


	/**
	 * \brief	Copy constructor for the not constant case
	 *
	 * In this case the index of *this si set to zero.
	 * And the index of src is incremented by one
	 *
	 * \param 	src 	The other rev index
	 */
	inline
	pgl_revision_index_t(
		pgl_revision_index_t& src) noexcept :
	  m_index(0)
	{
		src.m_index += 1;

		//Handle overflows
		if(src.m_index == INVALID)
		{
			src.m_index = 1;
		};
	};


	/**
	 * \brief	This is the constant copy assignment
	 *
	 * It works the same way as the copy constructor
	 */
	inline
	pgl_revision_index_t&
	operator= (
		const pgl_revision_index_t& src) noexcept
	{
		this->m_index = 0;

		return *this;
		(void)src;
	};


	/**
	 * \brief	This is the copy assignment for the non constant case
	 *
	 * Wiorks the same way as the constant copy assignment
	 */
	inline
	pgl_revision_index_t&
	operator= (
		pgl_revision_index_t& src) noexcept
	{
		this->m_index = 0;
		src.m_index += 1;

		//Handle overflows
		if(src.m_index == INVALID)
		{
			src.m_index = 1;
		};

		return *this;
	};


	/**
	 * \brief	This is the move constructor.
	 *
	 * It copies the value of src into this and increments it by one.
	 * The index of src is set to zero.
	 */
	inline
	pgl_revision_index_t(
		pgl_revision_index_t&& src) noexcept :
	  m_index(src.m_index + 1)
	{
		src.m_index = 0;
	};


	/**
	 * \brief	This is the move assignment.
	 *
	 * It works the same way as the move constructor
	 */
	 inline
	pgl_revision_index_t&
	operator= (
		pgl_revision_index_t&& src) noexcept
	{
		this->m_index = src.m_index + 1;
		src.m_index = 0;

		//Handle overflows
		if(src.m_index == INVALID)
		{
			src.m_index = 1;
		};

		return *this;
	};


	/**
	 * \brief	The destructor is defaulted
	 */
	inline
	~pgl_revision_index_t() noexcept = default;






	/*
	 * =========================
	 * Public functions
	 */
public:
	/**
	 * \brief	This increments the index of this by one.
	 *
	 * This is done to give the user the possiblility to reflect changes in the revisied object that are not reflected by coping
	 */
	inline
	pgl_revision_index_t&
	increment() noexcept
	{
		++m_index;

		//Handle overflows
		if(m_index == INVALID)
		{
			m_index = 0;
		};

		return *this;
	}; //End increment


	/**
	 * \brief	This is an alias of increment
	 */
	inline
	pgl_revision_index_t&
	newRevision() noexcept
	{
		return this->increment();
	}; //End new revision


	/**
	 * \brief	This fucntion returns the current reviosion index
	 *
	 * It is of type state, also inmutable
	 */
	inline
	State_t
	getIndex() const noexcept
	{
		return pgl_revision_state_t(m_index);
	}; //End of getIndex


	/**
	 * \brief	Thi
	 */
	inline
	State_t
	getRevision() const noexcept
	{
		return pgl_revision_state_t(m_index);
	}; //End of reviosn

	/**
	 * \brief	Retrun the current revision index as type Base_t
	 */
	inline
	Base_t
	getBase() const noexcept
	{
		return m_index;
	};



	/*
	 * =============================
	 * Operators
	 */
public:
	/**
	 * \brief	compares *this with rhs if the reviosions are the same
	 */
	inline
	bool
	operator== (
		const pgl_revision_index_t& rhs) noexcept
	{
		return (this->m_index == rhs.m_index) ? true : false;
	}; //End equal operator


	/**
	 * \brief	This is a templated equal function, that takes every argument and copares it with comthing of type base
	 */
	template<class T>
	bool
	equal(
		const T& t) const noexcept
	{
		return (((const Base_t)(m_index)) == t) ? true : false;
	}; //Enc equal operator


	/**
	 * \brief	This generates an outstream operator
	 */
	friend std::ostream& operator<<(std::ostream& os, const pgl_revision_index_t& i)
	{
		return (os << i.m_index);
	}; //End of outstream operator


	/*
	 * ==================
	 * Private Variable
	 */
private:
	Base_t		m_index;	//!< This is the variable
}; //End class(pgl_revision_index_t)


/*
 * Operators to compare rev state and indexeces
 */
inline
bool
operator== (
	const pgl_revision_state_t& lhs,
	const pgl_revision_index_t&  rhs) noexcept
{
	return lhs.getBase() == rhs.getBase();
};

inline
bool
operator== (
	const pgl_revision_index_t& lhs,
	const pgl_revision_state_t&  rhs) noexcept
{
	return lhs.getBase() == rhs.getBase();
};




PGL_NS_END(pgl)



/*
 * Overload some standard predicates
 */
namespace std
{

	template <>
	struct hash<::pgl::pgl_revision_index_t>
	{
		size_t
		operator()(
			const ::pgl::pgl_revision_index_t& x) const
		{
			return m_privHash(x.getBase() );
		}; //End operator

	private:
		std::hash<::pgl::pgl_revision_index_t::Base_t>	m_privHash;
	}; //End struct(hash)

	template <>
	struct hash<::pgl::pgl_revision_state_t>
	{
		size_t
		operator()(
			const ::pgl::pgl_revision_state_t& x) const
		{
			return m_privHash(x.getBase() );
		}; //End operator

	private:
		std::hash<::pgl::pgl_revision_state_t::Base_t>	m_privHash;
	}; //End struct(hash)
}; //End namespace(std)


