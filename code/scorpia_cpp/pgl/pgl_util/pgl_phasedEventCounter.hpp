#pragma once
/**
 * \brief	This file implements a phased event counter.
 *
 * This is nice to count if an event hapened in a certain iteration.
 */


//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>

#include <pgl_vector.hpp>

#include <pgl/pgl_math.hpp>


//Include STD
#include <iostream>


PGL_NS_START(pgl)


/**
 * \brief	The phased event counter is used to count if something happens in an iteration or not.
 * \class 	pgl_phasedEventCounter_t
 *
 * This is basically a counter that can be used to keep track if certain events in an
 * iteration happened. It is basically two counters, the first counts how many iterations
 * were performed and the second counts how much a certain event happened. For example
 * how many times the time step had to be restricted.
 *
 * However this class also maintains a compressed name of the distributuion when it happens.
 * The iterations are not recorded seperatly, but into chunkes that are called phases.
 * The duration of a phase can be set at construction.
 *
 */
class pgl_phasedEventCounter_t
{
	/*
	 * ==================
	 * Typedef
	 */
public:
	using Base_t 			= ::pgl::uInt64_t;			//!< This is the base type
	using DistCounter_t 		= ::pgl::uInt32_t;			//!< This is the type for counting the number of events inside a phase.
	using EventDistribution_t	= ::pgl::pgl_vector_t<DistCounter_t>;	//!< This is the Type for the distribution.
	using const_iterator 		= EventDistribution_t::const_iterator;	//!< This allows to iterates over the distribution.
	using iterator 			= const_iterator;

	/*
	 * ====================
	 * Constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * As argument it takes the number of steps that are one
	 * phase. This number must be larger than zero.
	 * Note that a value of one is not space efficient.
	 * See also the decritiption to the default constructor.
	 *
	 * \param  phaseDur	The number of iterations comprising one phase.
	 */
	pgl_phasedEventCounter_t(
		const DistCounter_t& 	phaseDur)
	 :
	  pgl_phasedEventCounter_t()	//Default construct
	{
		if(phaseDur == 0)
		{
			throw PGL_EXCEPT_InvArg("Passed zero as the number of iterations per phase.");
		};

		m_phaseDur = phaseDur;	//Set the number of phases
	}; //End building constructor


	/**
	 * \brief	Default constructor.
	 *
	 * Is public and defaulted, will construct a counter with a
	 * phase duration of 100 steps. Note that technically
	 * the counter is in the -1 phase, but this is useless
	 * since not even one iteration is done jet.
	 */
	pgl_phasedEventCounter_t()
	 noexcept
	 :
	  m_itCount(0),
	  m_nEvents(0),
	  m_phaseDur(100),
	  m_eDist()	//Empty
	{};


	/**
	 * \brief	Copy constructor is defaulted.
	 */
	pgl_phasedEventCounter_t(
		const pgl_phasedEventCounter_t&)
	= default;


	/**
	 * \brief	NMove constructo is defaulted.
	 */
	pgl_phasedEventCounter_t(
		pgl_phasedEventCounter_t&& src)
	 noexcept
	 = default;


	/**
	 * \brief	Copy assignment is defaulted.
	 */
	pgl_phasedEventCounter_t&
	operator= (
		const pgl_phasedEventCounter_t& src)
	 = default;


	/**
	 * \brief	Move assignment is defaulted.
	 */
	pgl_phasedEventCounter_t&
	operator= (
		pgl_phasedEventCounter_t&& src)
	 = default;


	/**
	 * \brief	Destructor is defaulted.
	 */
	~pgl_phasedEventCounter_t()
	 = default;



	/*
	 * =======================
	 * Status Functions
	 */
public:
	/**
	 * \brief	Returns the number of recorded iterations.
	 *
	 * This function returns the total number of recorded iterations.
	 * Note that this function does not place a restriction on the phase.
	 * This means it is safe to be called right after construction.
	 */
	Base_t
	nIterations()
	 const
	 noexcept
	{
			pgl_assert(m_nEvents <= m_itCount);
		return m_itCount;
	}; //End: number of iterations


	/**
	 * \brief	This function will return the number of iterations that are
	 * 		 recorded in the current phase.
	 *
	 * Note that this function does not place a restriction on the phase.
	 * This means it is safe to call this function right after construction.
	 * But its value has no real meaning.
	 */
	Base_t
	currIterations()
	 const
	 noexcept
	{
			pgl_assert(m_nEvents <= m_itCount);
			pgl_assert(m_phaseDur > 0);
		return ((m_itCount % m_phaseDur) + 1);	//We have to add + 1, to account for non zero based counting.
	}; //End: current iteration per phase


 	/**
 	 * \brief	This function returns the number of iterations in phase p.
 	 *
 	 * This function is used internaly, and provided to simplify things.
 	 * It will return phaseDuration() for all but the last phase, where
 	 * it will return currIterations(). Note that this function needs
 	 * that at least one interatuion was recorded. Note this function
 	 * will never return 0.
 	 *
 	 * \param  p	The id of the phase that is cheked
 	 */
 	Base_t
 	iterationsInPhase(
 		const Size_t p)
 	 const
 	{
 		if(m_eDist.empty() )
		{
			throw PGL_EXCEPT_OutOfBound("No phase is recorded yet.");
		};
		if(p >= m_eDist.size())
		{
			throw PGL_EXCEPT_OutOfBound("No phase " + std::to_string(p) + ", is known, only " + std::to_string(m_eDist.size()) + " are known.");
		};

 		if(p == (m_eDist.size() - 1))
		{
			//The last one
			return this->currIterations();
		}
		else
		{
			return this->phaseDuration();
		};
	}; //End: get iterations in phases


	/**
	 * \brief	Returns the total number of recorded events
	 */
	Base_t
	nEvents()
	 const
	 noexcept
	{
		pgl_assert(m_nEvents <= m_itCount);
		return m_nEvents;
	}; //End: number of happened events


	/**
	 * \brief	This function returns the number of steps
	 * 		 that consists of one phase.
	 *
	 * This function returns the value that was passed at
	 * construction time.
	 */
	Base_t
	phaseDuration()
	 const
	 noexcept
	{
		pgl_assert(m_phaseDur > 0);
		return m_phaseDur;
	}; //End: phase duration


	/**
	 * \brief	Returns the number of phases we have.
	 *
	 * This is besically the length of the vector that is
	 * used to store the distributuion. Note that this function
	 * checks with an assert if the length is larger than zero.
	 *
	 * If *this was created it is in the -1 phase with is a
	 * useless phase since no iteration has happened yet.
	 * Also note that the counting of phases is zero based
	 *
	 * This function does not return the id or phase we are currently
	 * in. To get that number subtract one from this one. But be
	 * aware that this function returns a unsigned int.
	 * Thus you could get in truble.
	 */
	Size_t
	nPhases()
	 const
	 noexcept
	{
		pgl_assert(m_eDist.empty() == false);
		return m_eDist.size();
	}; //End: Number of phases



	/**
	 * \brief	This function returns the number of
	 * 		 events that are recorded in phase p.
	 *
	 * Note that the counting starts with zero. Note that
	 * this function will generate an error if there is no
	 * phase yet, thus it is not safe to call this function
	 * right after construction.
	 *
	 * \param  p	The phase index.
	 *
	 * \throw 	If an out of bound access hapens.
	 */
	DistCounter_t
	eventsInPhase(
		const Size_t 	p)
 	 const
 	{
 		pgl_assert(p < m_eDist.size());	//Better debugging
 		pgl_assert(m_eDist[p] <= m_phaseDur);
 		return m_eDist.at(p);	//Exception
 	}; //End: get count in phase


 	/**
 	 * \brief	This function returns the number of efents that are recoreded
 	 * 		 in the current phase.
 	 *
 	 * It is technically equivalent in calling in eventsInPhase(nPhases() - 1),
 	 * but more efficient. Note that this function also requieres that at least
 	 * one phase is active, so calling this function right after construction
 	 * will fail.
 	 */
 	DistCounter_t
 	currEvents()
 	 const
 	{
 		if(this->m_eDist.empty())	//Needed because back() does not check
		{
			throw PGL_EXCEPT_OutOfBound("No phase was statered yet.");
		};

		return m_eDist.back();		//Does not check for non empty
	}; //End: get current events


	/**
	 * \brief	This function returns the fraction of events to iterations.
	 *
	 * This is basically eventsInPhase(p) divided by iterationsInPhase(p) casted
	 * to an integer. Note that this function will return a fraction, so it is
	 * a number between 0 and 1. It is possible to specify the number of digits
	 * that the float should be turncutted, zero means no restriction.
	 *
	 * \param  p		Index of the phase.
	 * \param  digits	The number of digits that should be used.
	 */
	Numeric_t
	getFraction(
		const Size_t 	p,
		const Size_t 	digits = 0)
	 const
	{
 		if(m_eDist.empty() )
		{
			throw PGL_EXCEPT_OutOfBound("No phase is recorded yet.");
		};
		if(p >= m_eDist.size())
		{
			throw PGL_EXCEPT_OutOfBound("No phase " + std::to_string(p) + ", is known, only " + std::to_string(m_eDist.size()) + " are known.");
		};

		//get the full fraction
		const Numeric_t fullFraction = this->eventsInPhase(p) / Numeric_t(this->iterationsInPhase(p));
			pgl_assert(pgl::isValidFloat(fullFraction), fullFraction >= 0.0, fullFraction <= 1.0);

		if(digits == 0)		//No post processing of the result needed.
		{
			return fullFraction;
		};

		//We now post porcess the result, this is a bit inefficent, but this function is only used in certain cases
		const Numeric_t b = ::std::pow(10.0, digits);

		return Numeric_t( Size_t(fullFraction * b) / b);	//Mind the cast
	}; //End: getFraction


	/**
	 * \brief	This function is able to generate the total fraction.
	 *
	 * It devides the number of all recorded events divided by the number of recoreded
	 * iterations. The number of digits can be specified.
	 */
	Numeric_t
	getTotalFraction(
		const Size_t digits = 0)
	 const
	{
 		if(m_eDist.empty() )
		{
			throw PGL_EXCEPT_OutOfBound("No phase is recorded yet.");
		};

		//get the full fraction
		const Numeric_t fullFraction = m_nEvents / Numeric_t(m_itCount);
			pgl_assert(pgl::isValidFloat(fullFraction), fullFraction >= 0.0, fullFraction <= 1.0);

		if(digits == 0)		//No post processing of the result needed.
		{
			return fullFraction;
		};

		//We now post porcess the result, this is a bit inefficent, but this function is only used in certain cases
		const Numeric_t b = ::std::pow(10.0, digits);

		return Numeric_t( Size_t(fullFraction * b) / b);	//Mind the cast
	}; //End: total fraction


	/*
	 * =======================
	 * Event recording functions
	 *
	 * These functions allows to increase the count of the events
	 */
public:
	/**
	 * \brief	This function is the function to increase the counter.
	 *
	 * The argument indicates if the event happens (true) or did not happens
	 * in this iteration. This function will increase the number of phases if
	 * needed.
	 *
	 * Note this function has an unpredictable runtime, so you should not use
	 * it in tight loops, where each cycle counts.
	 *
	 * \param  didHappen	Bool to indicate of the event happens
	 */
	pgl_phasedEventCounter_t&
	recordIteration(
		const bool 	didHappen)
	{
		//Tets if we have to go to the next phase, this also works for the first time.
		if((m_itCount % m_phaseDur) == 0)
		{
			m_eDist.push_back(0);	//We have to start a new phase
		};

		//Increae the count, important we have to do it afterwards
		m_itCount       += 1;
		if(didHappen )
		{
			m_eDist.back() += 1;	//Distribution
			m_nEvents      += 1;	//Full event count
		};

		return *this;
	}; //End: record iteration


	/**
	 * \brief	This function is used to record a happening.
	 *
	 * This fuinction is a shorthand for recordIteration(true).
	 */
	pgl_phasedEventCounter_t&
	recordEvent()
	{
		return this->recordIteration(true);
	};


	/**
	 * \brief	This function is used to record an empty iteration, without a happening
	 */
	pgl_phasedEventCounter_t&
	recordNothing()
	{
		return this->recordIteration(false);
	};



 	/*
 	 * =====================
 	 * Vector Compability
 	 *
 	 * These functions allows to maintain a
 	 * compability of *this with the vector.
 	 */
public:
 	/**
 	 * \brief	This function returns the number of
 	 * 		 events in phase p.
 	 *
 	 * This function is provided to mimic a vector.
 	 *
 	 * \param  p 	The phase to access.
 	 *
 	 * \throw	This function throws if an out of bound acces
 	 * 		 is detected.
 	 */
 	DistCounter_t
 	operator[](
 		const Size_t p)
	 const
	{
 		pgl_assert(p < m_eDist.size());	//Better debugging
 		return m_eDist.at(p);	//Exception
	}; //End: operator[]


	/**
	 * \brief	This function is an alias of the nPhase function.
	 *
	 * But it does not test if the distribution is zero.
	 * So this function can return zero. It is provided to maintain
	 * compability with the vector.
	 */
	Size_t
	size()
	 const
	 noexcept
	{
		return m_eDist.size();
	}; //End: size()




	/*
	 * =====================
	 * Printing functions
	 *
	 * Tese functions are used to generate a reprot.
	 * Probably more printing functions will be implemented
	 * in the future. Also see the outstream operator.
	 */
public:
	/**
	 * \brief	This function will generate a full report.
	 *
	 * A full report contains nice list that lists every phase.
	 * The report is written to stream.
	 *
	 * \param  o		The out stream the report is written to.
	 * \param  digits	The number of digits that should be used.
	 */
	std::ostream&
	printFullReport(
		std::ostream& 		o,
		const Size_t 		digits = 3)
	 const;


	/*
	 * ======================
	 * Iterator
	 *
	 * It is only constant iterators that are supported.
	 * The iterator interface allows to iterate over the
	 * event count in the different phases.
	 * The phases are traversed in ascending order.
	 */
public:
	/**
	 * \brief	Returns an iterator to the start of the event count.
	 *
	 * Note this will return an iterator that points to the first phase.
	 */
	const_iterator
	begin()
	 const
	 noexcept
	{
		return m_eDist.cbegin();
	};


	/**
	 * \brief	Explicit constant version of begin().
	 */
	const_iterator
	cbegin()
	 const
	 noexcept
	{
		return m_eDist.cbegin();
	};


	/**
	 * \brief	This function returns the past the end iterator of the event count range.
	 */
	const_iterator
	end()
	 const
	 noexcept
	{
		return m_eDist.cend();
	};


	/**
	 * \brief	Explicit constant version on end().
	 */
	const_iterator
	cend()
	 const
	 noexcept
	{
		return m_eDist.cend();
	};


	/*
	 * =========================
	 * Private variables
	 */
private:
	Base_t 			m_itCount   = 0;	//!< The total numbers of iterations that were performed.
	Base_t 			m_nEvents   = 0;	//!< The total number of events that have occured.
	Base_t 			m_phaseDur  = 100;	//!< The duration of a phase, default 100.
	EventDistribution_t 	m_eDist;		//!< The event distribution.
}; //End class pgl_revioion_state_t


/**
 * \brief	This function will generate a report.
 *
 * The digits parameter will be set to 3.
 *
 * \param  out		The out stream.
 * \param  pec		The phased event counter object.
 */
inline
std::ostream&
operator<< (
	std::ostream& 				out,
	const pgl_phasedEventCounter_t&		pec)
{
	return pec.printFullReport(out, 3);
};




PGL_NS_END(pgl)


