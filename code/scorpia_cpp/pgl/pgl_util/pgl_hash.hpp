#pragma once
/**
 * \brief	This files implements some small utility functions
 *
 * This is a collecting header for small functions, that are so small that they do not deserve their own header.
 * On the other hand they are the everyday task, so their functionality should be offered
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_exception.hpp>
#include <pgl_int.hpp>

//Include STD
#include <functional>

//Include boost


PGL_NS_BEGIN(pgl)


/**
 * \brief	this is the hash function that is used inside pgl
 *
 * All containers that uses a hash function are defaulted to this hash function.
 * Thus specializing it allows to use them without a major change.
 *
 * The implementation is gurarnteed to have the same specializations
 * as requiered by the std.
 * It is not guaranteed to be an alias of std::hash.
 *
 * The current implementation, is implementing by inherenting from
 * std::hash and forwarding all calls to the implementation.
 * This means that any specialization of std::hash is also
 * aviable for pgl_hash_t without any modification.
 *
 * Thus you shall not extend pgl_hash_T but std::hash.
 */
template<typename T>
struct pgl_hash_t : protected ::std::hash<T>
{
	/*
	 * ====================
	 * Typedefs
	 *
	 * Are deprecated in C++17, but we will provide them.
	 */
public:
	using argument_type 	= T;			//!< This is the key type
	using result_type 	= ::pgl::Size_t;	//!< Result type
	using Super_t 		= ::std::hash<T>;	//!< This is the super class.

	/*
	 * =======================
	 * Hash function
	 */
public:
	/**
	 * \brief	This is the hash function that will compute a hash.
	 *
	 * This function basically calls the super function
	 *
	 * \param  k	The key to hash.
	 */
	result_type
	operator() (
		const argument_type& 	k)
	 const
	 noexcept
	{
		return result_type((*(static_cast<const Super_t* const>(this)))(k));
	}; //End: hash fucntion

}; //End: struct(pgl_hash_t)



PGL_NS_END(pgl)











