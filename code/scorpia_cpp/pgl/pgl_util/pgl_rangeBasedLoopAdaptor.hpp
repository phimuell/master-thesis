#pragma once
/**
 * \brief	This file implements a class that is able to provide rangepased loops.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>


//Include STD
#include <type_traits>
#include <utility>






PGL_NS_BEGIN(pgl)


/**
 * \class 	pgl_rangeBaseedIterator_helper_t
 * \brief	This class can provide suports for range based loops
 *
 * \tparam beginIt 	The type of the iterator that has the begin()
 * \tparam endIT 	The type of the iterator that has the end()
 * \tparam isConst 	Bool that controles fi something is const
 */
template<
	class beginIt,
	bool  isConstIt,
	class endIt = beginIt>
class pgl_rangeBasedLoppAdaptor_t
{
	/*
	 * ================
	 * Typedef
	 */
public:
	using FirstIt_t 	= beginIt; 		//!< This is the iterator for the bigin function
	using SecondIt_t 	= endIt;		//!< This is the iterator for the end function
	using Pair_t 		= ::std::pair<FirstIt_t, SecondIt_t>;	//!< This is the pair representation of this
	static constexpr bool ConstIt = isConstIt;		//!< Indicates if it is a constant iterator


	/*
	 * ===============================
	 * Iterator Functions
	 */
public:

	/**
	 * \brief	The begin function
	 */
	inline
	FirstIt_t
	begin() const noexcept
	{
		return m_begin;
	};

	/**
	 * \brief	The cbegin function.
	 *
	 * This fucntion is only present if the iteratior is a constant iterator
	 */
	template<bool isConstT = ConstIt>
	inline
	std::enable_if_t<isConstT == true, FirstIt_t>
	cbegin() const noexcept
	{
		static_assert(isConstIt == isConstT, "Nust be the same");
		return m_begin;
	};


	/**
	 * \brief 	The end function
	 */
	inline
	SecondIt_t
	end() const noexcept
	{
		return m_end;
	};


	/**
	 * \brief	The cend function
	 *
	 * It is only present, if this is a constant iterator
	 */
	template<bool isConstT = ConstIt>
	inline
	std::enable_if_t<isConstT == true, SecondIt_t>
	cend() const noexcept
	{
		return m_end;
	};



	/*
	 * ============================
	 * Constructors are all defaulted
	 */
public:
	#define NOEXCEPT_(NAME) noexcept(NAME <FirstIt_t>::value || NAME <FirstIt_t>::value)

	inline
	pgl_rangeBasedLoppAdaptor_t() NOEXCEPT_(std::is_nothrow_default_constructible) = default;

	inline
	pgl_rangeBasedLoppAdaptor_t(
		const pgl_rangeBasedLoppAdaptor_t&) NOEXCEPT_(std::is_nothrow_copy_constructible) = default;

	inline
	pgl_rangeBasedLoppAdaptor_t(
		pgl_rangeBasedLoppAdaptor_t&&) NOEXCEPT_(std::is_nothrow_move_constructible) = default;

	inline
	pgl_rangeBasedLoppAdaptor_t&
	operator= (
		const pgl_rangeBasedLoppAdaptor_t&) NOEXCEPT_(std::is_nothrow_copy_assignable) = default;

	inline
	pgl_rangeBasedLoppAdaptor_t&
	operator= (
		pgl_rangeBasedLoppAdaptor_t&&) NOEXCEPT_(std::is_nothrow_move_assignable) = default;

	inline
	pgl_rangeBasedLoppAdaptor_t(
		const Pair_t& p) NOEXCEPT_(std::is_nothrow_copy_constructible) :
	  m_begin(p.first),
	  m_end(p.second)
	{};

	inline
	pgl_rangeBasedLoppAdaptor_t(
		Pair_t&& p) NOEXCEPT_(std::is_nothrow_move_constructible) :
	  m_begin(std::move(p.first)),
	  m_end(std::move(p.second))
	{};


	inline
	pgl_rangeBasedLoppAdaptor_t&
	operator= (
		const Pair_t& p) NOEXCEPT_(std::is_nothrow_copy_assignable)
	{
		m_begin = p.first;
		m_end = p.second;

		return *this;
	};

	inline
	pgl_rangeBasedLoppAdaptor_t&
	operator= (
		Pair_t&& p) NOEXCEPT_(std::is_nothrow_move_assignable)
	{
		m_begin = std::move(p.first);
		m_end = std::move(p.second);

		return *this;
	};


	inline
	explicit
	operator Pair_t() NOEXCEPT_(std::is_nothrow_copy_constructible)
	{
		return std::make_pair(m_begin, m_end);
	};


	inline
	pgl_rangeBasedLoppAdaptor_t(
		FirstIt_t&& 	Begin,
		SecondIt_t&&	End) NOEXCEPT_(std::is_nothrow_move_constructible) :
	  m_begin(std::move(Begin)),
	  m_end(std::move(End))
	{};

	inline
	pgl_rangeBasedLoppAdaptor_t(
		const FirstIt_t& 	Begin,
		const SecondIt_t&	End) NOEXCEPT_(std::is_nothrow_copy_constructible) :
	  m_begin(Begin),
	  m_end(End)
	{};

#undef NOEXCEPT_


	/*
	 * =========================
	 * Non Iterator functions
	 */
public:

	/**
	 * \brief	Retruns true if start and end are the same
	 */
	inline
	bool
	isEmpty() const noexcept
	{
		return (m_begin == m_end) ? true : false;
	};


	/**
	 * \brief	Retruns true if the iterators are const iterators
	 */
	inline
	bool
	constexpr
	isConst() noexcept
	{
		return isConstIt;
	};


	/*
	 * ====================================
	 * Private Member
	 */
private:
	FirstIt_t 	m_begin;
	SecondIt_t 	m_end;
}; //End class(pgl_rangeBasedLoopAdptor_t)





PGL_NS_END(pgl)


