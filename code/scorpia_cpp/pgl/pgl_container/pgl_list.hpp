#pragma once
#ifndef PGL_CONTAINER__PGL_LIST_HPP
#define PGL_CONTAINER__PGL_LIST_HPP
/**
 * \brief	This file declares the list prototype for PGL.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl/pgl_allocator.hpp>

//Include the underlying implementation
#if defined(PGL_CONTAINER_USE_STD_LIST) && PGL_CONTAINER_USE_STD_LIST != 0
	//Use the std version
#	include <list>
#else
	//Use the boost version
#	include <boost/container/list.hpp>
#endif

namespace pgl
{

/**
 * \typedef 	pgl_list_t
 * \brief	This class implements a doubly linked list.
 *
 * This class addheres to the specification from the STD.
 * The usage of the std implementation can be enforced.
 *
 * \tparam  T		The type we store in the list.
 * \tparam  Alloc	The allocator that is used.
 */
template<
	typename 	T,
	class 		Alloc = ::pgl::pgl_allocator_t<T>
>
#if defined(PGL_CONTAINER_USE_STD_LIST) && PGL_CONTAINER_USE_STD_LIST != 0
using pgl_list_t = ::std::list<T, Alloc>;
#else
using pgl_list_t = ::boost::container::list<T, Alloc>;
#endif



/**
 * \brief	This is an alias of pgl_list_t for backwards compability.
 * \typedef 	List_t
 *
 * This class ensures that older parts of PGL still works as intended.
 * This is only an alias of pgl_list_t.
 * It should not be used.
 */
template<typename T, class Alloc = ::pgl::pgl_allocator_t<T> >
using List_t = ::pgl::pgl_list_t<T, Alloc>;


}; //End namespace(pgl)

#endif //End include guards


