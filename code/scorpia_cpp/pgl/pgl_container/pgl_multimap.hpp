#pragma once
#ifndef PGL_CONTAINER__PGL_MULTIMAP_HPP
#define PGL_CONTAINER__PGL_MULTIMAP_HPP
/**
 * \brief	This file implements a multi map similar to std::multimap.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl/pgl_allocator.hpp>

//Include the underlying implementation
#if defined(PGL_CONTAINER_USE_STD_MULTIMAP) && PGL_CONTAINER_USE_STD_MULTIMAP != 0
#	include <map>	//Strange, but multimap does not have its own header
#else
#	include <boost/container/map.hpp>
#endif

namespace pgl
{

/**
 * \brief	This class implements the multimap functionality.
 * \typedef 	pgl_multiMap_t
 *
 * A multimap is similar to a regular map, aviable as pgl_map_t, but there is a difference.
 * The reguar map allows that each key is unique inside the map.
 * Inside a multimap a key can apear multiple times.
 * This class conforms with the implementation of std::multimap.
 *
 * \tparam  key_t	The type we use for identifing the objects.
 * \tparam  value_t 	The type of the objects we want to store inside the map.
 * \tparam  comp_fkt 	This function establisches a less than relation between the keys.
 * \tparam  Alloc	This is the allocator that is used.
 */

template<
	typename 	key_t,
	typename 	value_t,
	class 		comp_fkt = std::less<key_t>,
	class 		Alloc = ::pgl::pgl_allocator_t<::std::pair<const key_t, value_t> >
>
#if defined(PGL_CONTAINER_USE_STD_MULTIMAP) && PGL_CONTAINER_USE_STD_MULTIMAP != 0
using pgl_multiMap_t = ::std::multimap<key_t, value_t, comp_fkt, Alloc>;
#else
using pgl_multiMap_t = ::boost::container::multimap<key_t, value_t, comp_fkt, Alloc>;
#endif


/**
 * \typedef	MultiMap_t
 *
 * This class ensures backwards compability inside PGL.
 * This is an alias of pgl_multiMap_t.
 * It should not be used.
 */
template<typename key_t, typename value_t, class comp_fkt = std::less<key_t>, class Alloc = ::pgl::pgl_allocator_t<::std::pair<const key_t, value_t> > >
using MultiMap_t = pgl_multiMap_t<key_t, value_t, comp_fkt, Alloc>;





}; //End namespace(pgl)

#endif //End include guard

