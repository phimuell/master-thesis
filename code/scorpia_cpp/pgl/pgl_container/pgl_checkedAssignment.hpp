#pragma once
#ifndef PGL_CONTAINER__PGL_INDIRECT_ASSIGNMENT_HPP
#define PGL_CONTAINER__PGL_INDIRECT_ASSIGNMENT_HPP
/**
 * \brief 	This file defines an indirect assignment or a cehcked onde.
 */
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>

#include <type_traits>


PGL_NS_START(pgl)

/**
 * \class 	pgl_checkedAssignment_t
 *
 * Brief this class implements a checked assignment.
 * It is intended as a return type of an assignment
 * operator.
 * Before the actuall assignment happens *this class
 * a function which checks if the return type is valid
 * or not. This function is one of the template parameter.
 *
 * The function must have the signatiure
 * 	bool(Lhs_t& lhs, const Rhs_t& rhs)
 * Lhs_t is the type that we wrap and Rhs_t is the
 * type we use as rhs of the assignment.
 * A true indicates that the assignment is okay and
 * false indicate that the assignment is not okay.
 * In this case an exception is generated.
 *
 * Note that this function must also perform the assignment.
 * Thus it is possible to c hange or addapt the assignment
 * value.
 *
 * In order to ease the creation of the class
 * a function is provided. that does the type deductions.
 *
 * \tparam  Lhs_t	The left hand side of the expression.
 * \tapram  checkFkt_t	The function used for checking.
 */
template<
	typename 	Lhs_t,
	typename 	checkFkt_t
>
class pgl_checkedAssignment_t
{
	/*
	 * ================================
	 * Some usefull typedefs
	 */
public:
	/*
	 * Types that are required for the vector
	 */
	using value_type 		= Lhs_t;
	using reference 		= Lhs_t&;
	using const_reference 		= const Lhs_t&;
	using CheckFkt_t 		= checkFkt_t;		//!< The function that is used for the check.

	/*
	 * ================
	 * constructors
	 */
public:
	/**
	 * \brief	This is the building constructor.
	 *
	 * This function takes a reference to a managed object and aquieres it.
	 * The resource is not managed by *this.
	 *
	 * \param  obj		The object to wrap.
	 */
	explicit
	pgl_checkedAssignment_t(
		Lhs_t& 			obj,
		const checkFkt_t&	chkFkt)
	 :
	  m_ref(::std::addressof(obj)),
	  m_fkt(chkFkt)
	{};


	/**
	 * \brief	Default constructor is forbidden.
	 */
	pgl_checkedAssignment_t()
	 = delete;


	/**
	 * \brief	Copy constructor.
	 *
	 * Is deleted.
	 */
	pgl_checkedAssignment_t(
		const pgl_checkedAssignment_t&)
	 = delete;


	/**
	 * \move constructor is allowed.
	 */
	pgl_checkedAssignment_t(
		pgl_checkedAssignment_t&& )
	 noexcept
	 = default;


	/**
	 * \brief	Destructor.
	 *
	 * Sets the pointer of the managed resources to zero.
	 * Does not free it.
	 */
	~pgl_checkedAssignment_t()
	 noexcept
	{
		m_ref = nullptr;
	};


	/**
	 * \brief	Copy assigment
	 *
	 * Is deletd.
	 */
	pgl_checkedAssignment_t&
	operator= (
		const pgl_checkedAssignment_t&)
	 = delete;


	/**
	 * \brief	Move assignment.
	 *
	 * is defaulted.
	 */
	pgl_checkedAssignment_t&
	operator= (
		pgl_checkedAssignment_t&&)
	 noexcept
	 = default;


	/*
	 * ==========================
	 * The overloaded assignments
	 */
public:
	/**
	 * \brief	This is the assignment operator, that is overloaded.
	 *
	 * This function takes a type of RHS_t and calls a different assignment
	 * function.
	 * The assignment is done by the passed function.
	 *
	 */
	template<
		typename 	Rhs_t
	>
	Lhs_t&
	operator=(
		const Rhs_t& 		rhs)
	{
		static_assert(::std::is_assignable<Lhs_t&, const Rhs_t&>::value, "Not assinable.");
		if(m_ref == nullptr)
		{
			throw PGL_EXCEPT_NULL("The referenced type is the nullptr.");
		};


		/*
		 * Perform the assignment.
		 */
		if(m_fkt(static_cast<Lhs_t&>(*m_ref), rhs) == false)
		{
			throw PGL_EXCEPT_InvArg("Can not assigne.");
		};


		/*
		 * Return the reference
		 */
		return *m_ref;
	}; //ENd assignment



	/*
	 * =======================
	 * Conversions Operators
	 */
public:
	/*
	 * =======================
	 * Private Members
	 */
private:
	Lhs_t* 			m_ref;
	CheckFkt_t 		m_fkt;
}; //end class(pgl_checkedAssignment_t)


/**
 * \brief	This function is provided to make
 * 		 the creation of checked assignments easier.
 *
 * This function allows to do the the template deduction
 * by arguments.
 *
 * \param  lhs		The wrapped type.
 * \param  checkFkt	The checker function.
 */
template<
	typename 	Lhs_t,
	typename 	checkFkt_t
>
pgl_checkedAssignment_t<Lhs_t, checkFkt_t>
makeCheckedAssignment(
	Lhs_t& 			lhs,
	const checkFkt_t& 	chkFkt)
{
	return pgl_checkedAssignment_t<Lhs_t, checkFkt_t>(lhs, chkFkt);
};

PGL_NS_END(pgl)

#endif //End include guard
