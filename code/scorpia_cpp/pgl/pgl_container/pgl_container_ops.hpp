#pragma once
/**
 * \brief	Thid files implements container operations
 *
 * This are operations that can be applied to 1, 2, ore 3 containers.
 * They are intendet mainly as a substitute for eigen.
 *
 * They only require that the container defines a value_type and begin(), end() and size()
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>
#include <pgl_int.hpp>
#include <pgl_exception.hpp>

#include <pgl/pgl_math.hpp>

#include <pgl_util/pgl_util.hpp>

//Include STD
#include <functional>
#include <utility>
#include <type_traits>


PGL_NS_BEGIN(pgl)


/**
 * \brief	This is the unary operator.
 *
 * This appys a function to the container and returns one.
 *
 * This function performs:
 * 	y_i = f(x_i), 	for i = 0, ..., size(x)
 *
 *
 * \param 	x		The vector we apply
 * \param 	y 		Where we save the result
 * \param 	f 		The function we apply
 */
template<
	class ContainerY_t,
	class ContainerX_t,
	class Function_t>
	void
unaryCOp(
	ContainerY_t& 		y,
	const ContainerX_t& 	x,
	Function_t 		f)
{
	const auto ende = x.end();
	auto yit = y.begin();

	for(auto it = x.begin(); it != ende; ++it)
	{
		*yit = f(*it);
		++yit;
	}; //End apply
};



/**
 * \brief	This function is also an unatary operation
 *
 * It again calculates $y_i = f(x_i)$.
 * But the vector y is created by the function and returned.
 * For the type the one of x is used
 */
template<
	class Conatiner_t,
	class Function_t>
Conatiner_t
unaryCOp(
	const Conatiner_t&	x,
	Function_t 		f)
{
	const auto N = x.size();
	Conatiner_t y(N);

	unaryCOp<Conatiner_t, Conatiner_t, Function_t>(y, x, f);

	return y;
};

/**
 * \brief	This function is also an unatary operation
 *
 * It again calculates $x_i = f(x_i)$.
 * But the value is written back into the vector.
 */
template<
	class Conatiner_t,
	class Function_t>
Conatiner_t&
unaryCOpM(
	Conatiner_t&	x,
	Function_t 	f)
{
	using Value_t = typename Conatiner_t::value_type;

	const auto N = x.size();

	const auto ende = x.end();

	for(auto it = x.begin(); it != ende; ++it)
	{
		*it = Value_t(f(*it));
	};

	return x;
};



/**
 * \brief	This is the binary operator.
 *
 * This applys an operation to two ceontainers.
 * Mathematicaly it implements
 * 	z_i = f(x_i, y_i), for i = 0, ..., size(x)
 *
 * All vector must have the smae length
 */
template<
	class ContainerZ_t,
	class ContainerX_t,
	class ContainerY_t,
	class Function_t>
void
binaryCOp(
	ContainerZ_t& 		z,
	const ContainerX_t& 	x,
	const ContainerY_t& 	y,
	Function_t 		f)
{
	const Size_t N = x.size();
	if(threeEqual(N, y.size(), z.size()) == false)
	{
		throw PGL_EXCEPT_InvArg("The two ranges differ. The size of x is " + std::to_string(N) + ", but the size of y id " + std::to_string(y.size()));
	};

	auto ix = x.begin();
	auto iy = y.begin();
	auto iz = z.begin();
	for(Size_t it = 0; it != N; ++it)
	{
		*iz = f(*ix, *iy);

		++ix;
		++iy;
		++iz;
	}; //End applying and safe

	return;
};


/**
 * \brief	This is the binary operator.
 *
 * This applys an operation to two ceontainers.
 * Mathematicaly it implements
 * 	z_i = f(x_i, y_i), for i = 0, ..., size(x)
 *
 * The difference to the other version of this function is, that the Container z
 * is generated automatically and returned.
 * As type the type of x is used
 */
template<
	class ContainerX_t,
	class ContainerY_t,
	class Function_t>
ContainerX_t
binaryCOp(
	const ContainerX_t& 	x,
	const ContainerY_t& 	y,
	Function_t 		f)
{
	const auto N = x.size();
	ContainerX_t z(N);

	binaryCOp<ContainerX_t, ContainerX_t, ContainerY_t, Function_t>(z, x, y, f);

	return z;
};




/**
 * \brief	This is a trinary function of continers
 *
 * This applys a function with all three vectors at once.
 * Mathematically the function does
 * 	u_i = f(x_i, y_i, z_i), for i = 0, ..., size(x)
 *
 */
template<
	class ContainerU_t,
	class ContainerX_t,
	class ContainerY_t,
	class ContainerZ_t,
	class Function_t>
void
trinaryCOp(
	ContainerU_t& 		u,
	const ContainerX_t& 	x,
	const ContainerY_t& 	y,
	const ContainerZ_t& 	z,
	Function_t 		f)
{
	const Size_t N = x.size();
	if(fourEqual(N, y.size(), z.size(), u.size()) == false)
	{
		throw PGL_EXCEPT_InvArg("The two ranges differ. The size of x is " + std::to_string(N) + ", but the size of y id " + std::to_string(y.size()));
	};

	auto ix = x.begin();
	auto iy = y.begin();
	auto iz = z.begin();
	auto iu = u.begin();
	for(Size_t it = 0; it != N; ++it)
	{
		*iu = f(*ix, *iy, *iz);

		++ix;
		++iy;
		++iz;
		++iu;
	}; //End applying and safe

	return;
};


/**
 * \brief	This is a trinary function of continers
 *
 * This applys a function with all three vectors at once.
 * Mathematically the function does
 * 	u_i = f(x_i, y_i, z_i), for i = 0, ..., size(x)
 *
 * The function creates the u container itself and returns it.
 * As type for that container the type of container x is taken
 */
template<
	class ContainerX_t,
	class ContainerY_t,
	class ContainerZ_t,
	class Function_t>
ContainerX_t
trinaryCOp(
	const ContainerX_t& 	x,
	const ContainerY_t& 	y,
	const ContainerZ_t& 	z,
	Function_t 		f)
{
	const auto N = x.size();
	ContainerX_t u(N);

	trinaryCOp<ContainerX_t, ContainerX_t, ContainerY_t, ContainerZ_t, Function_t>(u, x, y, z, f);

	return u;
};


/**
 * \brief	In this namespace we collect concrete instatations of the container operations
 * \namespace 	contOps
 */
namespace contOps
{

/**
 * \brief	This function add two vectors togeter.
 *
 * It is an aplication of of the binaryCOps with add as operation.
 * 	computes:	z = x + y
 *
 */
template<
	class Container_t,
	class ContainerY_t>
Container_t
contAdd(
	const Container_t& 	x,
	const ContainerY_t& 	y)
{
	using value_type_x = typename Container_t::value_type;
	using value_type_y = typename ContainerY_t::value_type;

	return binaryCOp(x, y, [](const value_type_x& ax, const value_type_y& ay) -> value_type_x { return ax + ay;});
};

/**
 * \brief	This function emulates the x += y operation
 *
 */
template<
	class ContainerX_t,
	class ContainerY_t>
ContainerX_t&
contAddA(
	ContainerX_t& 		x,
	const ContainerY_t	y)
{
	const auto ende = x.end();
	auto xit = x.begin();
	auto yit = y.begin();

	while(xit != ende)
	{
		*xit += *yit;
	};

	return x;
};



/**
 * \brief	This function substartcts two containers from each other
 *
 * It is an aplication of of the binaryCOps with add as operation.
 * 	computes:	z = x - y
 */
template<
	class Container_t,
	class ContainerY_t>
Container_t
contSub(
	const Container_t& 	x,
	const ContainerY_t& 	y)
{
	using value_type_x = typename Container_t::value_type;
	using value_type_y = typename ContainerY_t::value_type;

	return binaryCOp(x, y, [](const value_type_x& ax, const value_type_y& ay) -> value_type_x { return ax - ay;});
};


/**
 * \brief	This function emulates the x -= y operation
 *
 */
template<
	class ContainerX_t,
	class ContainerY_t>
ContainerX_t&
contSubA(
	ContainerX_t& 		x,
	const ContainerY_t	y)
{
	const auto ende = x.end();
	auto xit = x.begin();
	auto yit = y.begin();

	while(xit != ende)
	{
		*xit -= *yit;
	};

	return x;
};



/**
 * \brief	This function multiplies two containers from each other
 *
 * It is an aplication of of the binaryCOps with add as operation.
 * 	computes:	z = x * y
 */
template<
	class Container_t,
	class ContainerY_t>
Container_t
contMul(
	const Container_t& 	x,
	const ContainerY_t& 	y)
{
	using value_type_x = typename Container_t::value_type;
	using value_type_y = typename ContainerY_t::value_type;

	return binaryCOp(x, y, [](const value_type_x& ax, const value_type_y& ay) -> value_type_x { return ax * ay;});
};


/**
 * \brief	This function divides two containers from each other
 *
 * It is an aplication of of the binaryCOps with add as operation.
 * 	computes:	z = x / y
 */
template<
	class Container_t,
	class ContainerY_t>
Container_t
contDiv(
	const Container_t& 	x,
	const ContainerY_t& 	y)
{
	using value_type_x = typename Container_t::value_type;
	using value_type_y = typename ContainerY_t::value_type;

	return binaryCOp(x, y, [](const value_type_x& ax, const value_type_y& ay) -> value_type_x { return ax / ay;});
};


/**
 * \brief	This fucntion scales all element of x with a
 *
 * mathematicaly this function computes:
 * 	y = a * x
 */
template<
	typename Scalar,
	class Container_t>
std::enable_if_t<std::is_arithmetic<Scalar>::value, Container_t>
contScale(
	const Scalar& 		a,
	const Container_t& 	x)
{
	using value_type_x = typename Container_t::value_type;

	return unaryCOp(x, [a](const value_type_x& ax) -> value_type_x { return a * ax;});
};


/**
 * \brief	This function emulates the x *= a operation,
 *
 * Where a is a scalar.
 * Note that the the non modifing function, here the argumens are swaped.
 * This is done for syntactic reasons.
 *
 * This function returns a reference to the input argument.
 */
template<
	class Container_t,
	typename Scalar>
std::enable_if_t<std::is_arithmetic<Scalar>::value, Container_t&>
contScaleM(
	Container_t& 		x,
	const Scalar& 		a)
{
	const auto ende = x.end();
	for(auto it = x.begin(); it != ende; ++it)
	{
		*it *= a;
	};

	return x;
};



/**
 * \brief	This function adds a a scalar to each element of the vector and returns the result
 *
 * Notice that a copy is generated.
 */
template<
	class Container_t,
	typename Scalar>
std::enable_if_t<std::is_arithmetic<Scalar>::value, Container_t>
contSAdd(
	const Container_t& 	x,
	const Scalar& 		a)
{
	using ValX_t = typename Container_t::value_type;
	auto F = [a](const ValX_t X) -> decltype(X + a) { return X + a;};

	return unaryCOp<Container_t, decltype(F)>(x, F);
};

/**
 * \brief	This function performs x_i += a.
 *
 * This function modifies the argument x And returns it
 */
template<
	class Container_t,
	typename Scalar>
std::enable_if_t<std::is_arithmetic<Scalar>::value, Container_t&>
contSAddM(
	Container_t& 		x,
	const Scalar& 		a)
{
	const auto ende = x.end();
	for(auto it = x.begin(); it != ende; ++it)
	{
		*it += a;
	};

	return x;
};







/**
 * \brief	Tis function performs fused multyply add on the three vectors
 *
 * 	u = x * y + z
 *
 * This function is not implemented by calling the trinary function, but implements it directly to exploit vectorization
 */
template<
	class Container_t,
	class ContainerY_t,
	class ContainerZ_t,
	class Function_t>
Container_t
contFMA(
	const Container_t& 	x,
	const ContainerY_t& 	y,
	const ContainerZ_t& 	z)
{
	const Size_t N = x.size();
	if(threeEqual(N, y.size(), z.size()) == false)
	{
		throw PGL_EXCEPT_InvArg("The two ranges differ. The size of x is " + std::to_string(N) + ", but the size of y id " + std::to_string(y.size()));
	};

	Container_t u(N);

	auto ix = x.begin();
	auto iy = y.begin();
	auto iz = z.begin();
	auto iu = u.begin();

	for(Size_t it = 0; it != N; ++it)
	{
		*iu = std::fma(*ix, *iy, *iz);

		++ix;
		++iy;
		++iz;
		++iu;
	}; //End applying and safe

	return u;
}; //End contFMA

}; //End container operations



/**
 * \brief	This namespace implements some functions that reduces the container
 * \namespace 	contRed
 */
namespace contRed
{

/**
 * \brief	This function sums all elements up that are stored inside this
 *
 * Please notice that the implementation is not so nice, and no protection against cancelation is done
 *
 * \param 	x	This vector will be summed up
 */
template<
	class Conatiner_t>
typename Conatiner_t::value_type
contSum(
	const Conatiner_t& 	x)
{
	using Value_t  = typename Conatiner_t::value_type;
	Value_t accumulat = Value_t(0);

	const auto ende = x.end();
	for(auto it = x.begin(); it != ende; ++it)
	{
		accumulat += *it;
	};


	return accumulat;
};


/**
 * \brief	This function performs a cumulative sum of the input element.
 *
 * This means that the output container a has the property
 * 	a_i = init + sum_{j = 0}^{i} x_j
 *
 * This is function creates a new container and will return it
 */
template<class ContainerX_t>
ContainerX_t
contCumSum(
	const ContainerX_t& 	x)
{
	const Size_t N = x.size();
	ContainerX_t cs(N);

	if(N == 0)
	{
		return cs;
	}

	auto it_x = x.begin();
	auto it_cs = cs.begin();

	*it_cs = *it_x;

	if(N == 1)
	{
		return cs;
	};

	const auto ende = x.end();

	//This is the hintere iterator
	auto it_cs_h = cs.begin();

	//Increment
	++it_x;
	++it_cs;


	while(it_x != ende)
	{
		*it_cs = *it_cs_h + *it_x;

		++it_x;
		++it_cs;
		++it_cs_h;
	};

	return cs;
}; //End cumulative sum


/**
 * \brief	This function performs a cumulative sum of the input element.
 *
 * This means that the output container a has the property
 * 	a_i = init + sum_{j = 0}^{i} x_j
 *
 * The operation happens inplace
 */
template<class ContainerX_t>
ContainerX_t&
contCumSumM(
	ContainerX_t& 		x)
{
	const Size_t N = x.size();

	if(N == 0 | N == 1)
	{
		return x;
	}

	auto it_f = x.begin(); 	//The "Front element
	++it_f;

	auto it_b = x.begin();	// TheBehind element

	const auto ende = x.end();

	while(it_f != ende)
	{
		*it_f += *it_b;

		++it_f;
		++it_b;
	};

	return x;
};




}; //End contRed













PGL_NS_END(pgl)
