#pragma once
#ifndef PGL_CONTAINER__PGL_SMALL_VECTOR_HPP
#define PGL_CONTAINER__PGL_SMALL_VECTOR_HPP
/**
 * \brief	This file provides a small vector type.
 *
 * This vector is not standard an is only provided by boost container.
 */


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl/pgl_allocator.hpp>

//Inlcude BOOST
#include <boost/container/small_vector.hpp>

namespace pgl
{

/**
 * \typedef 	pgl_smallVector_t
 * \brief	This class is a vector that is optimized for small sizes.
 *
 * This class stores the objects directly inside an array, so no dynamic allocation is needed.
 * This way it can store up to N objects.
 * It supports all the functions that are also supported by the normal vector.
 * If the space is exceeded, meaning more than N elements should be stored,
 * the vector switches to a dynamic allocated array.
 * This handling is transparent to the user.
 *
 * This class is not part of the std.
 *
 * \tparam  T 		The type of the objects we want to store.
 * \tparam  N 		The number of objects that should be stored statically.
 * \tparam  Alloc	The allocator that should be used, if the static space is exceeded.
 */
template<
	typename 	T,
	Size_t 		N,
	class 		Alloc = ::pgl::pgl_allocator_t<T>
>
using pgl_smallVector_t = ::boost::container::small_vector<T, N, Alloc>;

}; //End namespace(pgl)

#endif //End include guard

