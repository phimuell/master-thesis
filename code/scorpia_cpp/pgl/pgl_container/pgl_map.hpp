#pragma once
#ifndef PGL_CONTAINER__PGL_MAP_HPP
#define PGL_CONTAINER__PGL_MAP_HPP
/**
 * \brief	This file provides a map type, simillar to std::map.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl/pgl_allocator.hpp>

//Include the underyling implementation
#if defined(PGL_CONTAINER_USE_STD_MAP) && PGL_CONTAINER_USE_STD_MAP != 0
	//Include STD
#	include <map>
#else
	//Include BOOST
#	include <boost/container/map.hpp>
#endif

namespace pgl
{

/**
 * \typedef 	pgl_map_t
 * \brief	This class implements the functionality of std::map.
 *
 * It is possible to force PGL to use the std implementation of the list.
 * This can be selected at compile time.
 *
 * \tparam  key_t	The type we use for identification of teh objects we store.
 * \tparam  value_t	The type of the payload we store, this is the actual object we want to store inside the map.
 * \tparam  comp_fkt 	This is the function that establish an less than relation between keys.
 * \tparam  Alloc 	The allocator that is used.
 */
template<
	typename 	key_t,
	typename 	value_t,
	class 		comp_fkt = std::less<key_t>,
	class 		Alloc = ::pgl::pgl_allocator_t<::std::pair<const key_t, value_t> >
>
#if defined(PGL_CONTAINER_USE_STD_MAP) && PGL_CONTAINER_USE_STD_MAP != 0
using pgl_map_t = ::std::map<key_t, value_t, comp_fkt, Alloc>;
#else
using pgl_map_t = ::boost::container::map<key_t, value_t, comp_fkt, Alloc>;
#endif

/**
 * \typedef 	Map_t
 *
 * This is an alias of pgl_map_t for enabling backwards compability with older parts of pgl.
 * It should not be used.
 */
template<typename key_t, typename value_t, class comp_fkt = std::less<key_t>, class Alloc = ::pgl::pgl_allocator_t<::std::pair<const key_t, value_t> > >
using Map_t = ::pgl::pgl_map_t<key_t, value_t, comp_fkt, Alloc>;

}; //End namespace(pgl)

#endif //End include guard

