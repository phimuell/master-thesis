/**
 * \brief	This file contains print statement that outputs if the std versions of the compiler was used.
 */

#include <pgl_core.hpp>


#if defined(PGL_CONATINER_USE_STD_HASHSET) && PGL_CONATINER_USE_STD_HASHSET != 0
#	pragma message "STD version of the hashset (unordered_set) is used."
#endif

#if defined(PGL_CONTAINER_USE_STD_DEQUE) && (PGL_CONTAINER_USE_STD_DEQUE != 0)
#	pragma message "STD version of the deque is used."
#endif

#if defined(PGL_CONTAINER_USE_STD_HASHMAP) && PGL_CONTAINER_USE_STD_HASHMAP != 0
#	pragma message "STD version of the hashmap (unordered_map) is used."
#endif

#if defined(PGL_CONTAINER_USE_STD_LIST) && PGL_CONTAINER_USE_STD_LIST != 0
#	pragma message "STD version of the list is used."
#endif

#if defined(PGL_CONTAINER_USE_STD_MAP) && PGL_CONTAINER_USE_STD_MAP != 0
#	pragma message "STD version of the map is used."
#endif

#if defined(PGL_CONTAINER_USE_STD_MULTIMAP) && PGL_CONTAINER_USE_STD_MULTIMAP != 0
#	pragma message "STD version of the multimap is used."
#endif

#if defined(PGL_CONTAINER_USE_STD_SET) && PGL_CONTAINER_USE_STD_SET != 0
#	pragma message "STD version of the set is used."
#endif

#if defined(PGL_CONTAINER_USE_STD_VECTOR) && PGL_CONTAINER_USE_STD_VECTOR != 0
#	pragma message "STD version of the vector is used."
#endif



