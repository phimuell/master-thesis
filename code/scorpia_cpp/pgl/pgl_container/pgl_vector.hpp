#pragma once
#ifndef PGL_CONTAINER__PGL_VECTOR_HPP
#define PGL_CONTAINER__PGL_VECTOR_HPP
/**
 * \brief	This file provides a type that complies with the requirement for std::vector.
 */

//Inclue PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl/pgl_allocator.hpp>

//This will include the boost container only if needed.
#if defined(PGL_CONTAINER_USE_STD_VECTOR) && PGL_CONTAINER_USE_STD_VECTOR != 0
//Nothing to do
#else
#	include <boost/container/vector.hpp>
#endif

//Vector is so important, that we provides alias for it.
//So we have to include the std version always.
#include <vector>

namespace pgl
{

/**
 * \typedef	pgl_vector
 * \brief	This is the type used by PGL for vector.
 *
 * This class provides a type that is simillar to std::vector.
 * At compile time it can be selected if the std implementation
 * should be used.
 *
 * \tparam  T		The type we want to store.
 * \tparam  Alloc	The allocator that should be used.
 */
template<
	typename 	T,
	class 		Alloc = ::pgl::pgl_allocator_t<T>
>
#if defined(PGL_CONTAINER_USE_STD_VECTOR) && PGL_CONTAINER_USE_STD_VECTOR != 0
using pgl_vector_t = ::std::vector<T, Alloc>;
#else
using pgl_vector_t = ::boost::container::vector<T, Alloc>;
#endif


/**
 * \typedef 	Vector_t
 * \brief	Backward compability.
 *
 * In the dark age of pgl, ::pgl::Vector_t, was the vector, that was used.
 * But it introduced an inconcistency in the naming scheme.
 * So I changed this, to provide backward compability, this alias is provided.
 */
template<typename T, class Alloc = ::pgl::pgl_allocator_t<T> >
using Vector_t = ::pgl::pgl_vector_t<T, Alloc>;


/**
 * \typedef 	stlVector_t
 * \brief	This class is a typedef of the vector,
 * 		 that is provided by the standard.
 *
 * This is doen, to have a uniform access to it.
 * And the boost vector has problem with reference_wrapper.
 *
 * \tparam  T		The type we want to store.
 * \tparam  Alloc	The allocator that should be used.
 */
template<
	typename 	T,
	class 		Alloc = ::pgl::pgl_allocator_t<T>
>
using stlVector_t = ::std::vector<T, Alloc>;


}; //End namespace

#endif //End include guards
