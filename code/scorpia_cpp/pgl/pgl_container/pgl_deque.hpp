#pragma once
#ifndef PGL_CONTAINER__PGL_DEQUE_HPP
#define PGL_CONTAINER__PGL_DEQUE_HPP


//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl/pgl_allocator.hpp>


//Include the correct underlying container
#if defined(PGL_CONTAINER_USE_STD_DEQUE) && (PGL_CONTAINER_USE_STD_DEQUE != 0)
	//Std
#	include <deque>
#else
	//Boost
#	include <boost/container/deque.hpp>
#endif


namespace pgl
{

/**
 * \brief	This class models a deque.
 * \typedef 	pgl_deque_t
 *
 * This class models a deque, that is simmilar to the std version.
 * The usage of the std version can be selected.
 *
 * \tparam  T		The type we map to.
 * \tparam  Alloc	The allocator that is used.
 */
template<
	typename 	T,
	class 		Alloc = ::pgl::pgl_allocator_t<T>
>
#if defined(PGL_CONTAINER_USE_STD_DEQUE) && (PGL_CONTAINER_USE_STD_DEQUE != 0)
using pgl_deque_t = ::std::deque<T, Alloc>;
#else
using pgl_deque_t = ::boost::container::deque<T, Alloc>;
#endif



/**
 * \brief	This is a backwards compability measure.
 * \typedef 	Deque_t
 *
 * This is only an alias of pgl_deque_t.
 * It is provided for compability reason with older parts of PGL.
 * It should not be used.
 */
template<typename T, class Alloc = ::pgl::pgl_allocator_t<T> >
using Deque_t = ::pgl::pgl_deque_t<T, Alloc>;


}; //End namespace

#endif //End include guards
