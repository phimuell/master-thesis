#pragma once
#ifndef PGL_CONTAINER__PGL_stack_HPP
#define PGL_CONTAINER__PGL_stack_HPP
/**
 * \file	pgl_container/pgl_stack.hpp
 * \brief 	Provides a stack
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_assert.hpp>

#include <pgl_vector.hpp>

//Include STD
#include <utility>

//This allows us to call swap without specifiing std.
//Thus a more suitable verson could be found.
using std::swap;



PGL_NS_BEGIN(pgl)


/**
 * \class	pgl_stack_t
 * \brief 	A container adaptor, that provides the semantic of a stack
 *
 * This is an adaptor, the standard is a dequeu.
 * I made this myself because I didn't like the std version of if.
 *
 * The requieremnts of the container are:
 *  - begin/end	Iterators
 *  - push_back		(cref and &&)
 *  - pop_back
 *  - back
 *  - emplace_back
 *  - swap
 *  - size
 *
 *  The stack gives access to the stored element via an iterator.
 *  This iterator is only const.
 *  The order is the order of consumation.
 *
 *  So the container must support the reverse iterator.
 */
template<
	typename	T,
	class 	Container = ::pgl::Vector_t<T>
>
class pgl_stack_t
{
	/*
	 * =================
	 * Some typedef
	 */
public:
	using container_type		= Container;
	using value_type 		= typename Container::value_type;
	using size_type			= typename Container::size_type;
	using reference			= typename Container::reference;
	using const_reference		= typename Container::const_reference;
	using iterator			= typename Container::const_reverse_iterator;
	using const_iterator 		= typename Container::const_reverse_iterator;


	/*
	 * ===========================
	 * Constructors
	 */

	pgl_stack_t() = default;

	pgl_stack_t(
		const pgl_stack_t&) = default;

	pgl_stack_t(
		pgl_stack_t&&) = default;


	pgl_stack_t&
	operator= (
		const pgl_stack_t&) = default;

	pgl_stack_t&
	operator= (
		pgl_stack_t&&) = default;




	/*
	 * ==============================
	 * stack related functions
	 */

	/**
	 * \brief 	Enstack elem into *this
	 * \param elem	An element of tyoe T, that will be enqued at the end of *this
	 */
	void
	push(
		const_reference 	elem)
	{
		m_stack.push_back(elem);
		return;
	};

	void
	push(
		value_type&& 		elem)
	{
		m_stack.push_back(elem);
		return;
	};


	/**
	 * \brief	Construct an new element inplace.
	 *
	 */
	template<typename... Args>
	decltype(auto)
	emplace(
		Args&&... args)
	{
		return m_stack.emplace_back( std::forward<Args>(args)... );
	};


	/**
	 * \brief 	returns the front element, meaning the next tah will be consumed.
	 *
	 * The element is not removed from *this.
	 * This function also have a const version.
	 *
	 * \pre		There is at least one element in the container
	 */
	reference
	peek()
	{
		pgl_assert(m_stack.empty() == false);
		return m_stack.back();
	};

	const_reference
	peek() const
	{
		pgl_assert(m_stack.empty() == false);
		return m_stack.back();
	};


	/**
	 * \brief	Removes the first element of *this
	 *
	 * The first element is not returned, if this is whished, there are non standard functions to do that.
	 */
	void
	pop()
	{
		pgl_assert(m_stack.empty() == false);
		m_stack.pop_back();
		return;
	};

	/**
	 * \brief	This function returns the first element and also removes it from this.
	 *
	 * The element is forst moved (if supported) to a temportary then the elemnt is removed and then the temporary is returned
	 */
	value_type
	pop_get()
	{
		pgl_assert(m_stack.empty() == false);
		value_type tmp = std::move_if_noexcept(m_stack.back() );
		m_stack.pop_back();
		return tmp;
	};

	/**
	 * \brief	This function removes the element, but is returns it by an refewrence argument.
	 *
	 * This function swaps the content of the ret argument, with the first element of *this, using swap
	 * and then removes the first one.
	 */
	void
	pop_swap(
		reference 	ret)
	{
		pgl_assert(m_stack.empty() == false);
		swap(m_stack.back(), ret);
		m_stack.pop_back();
		return;
	};


	/*
	 * ====================================
	 * Normal querey functions
	 */

	/**
	 * \breif	Return the size of *this
	 */
	size_type
	size() const noexcept
	{
		return m_stack.size();
	};

	/**
	 * \brief	Test if empty
	 */
	bool
	empty() const noexcept
	{
		return m_stack.empty();
	};

	/**
	 * \brief	Returns a reference to the last insterted element
	 */
	const_reference
	last_inserted_element() const
	{
		return m_stack.back();
	};


	/**
	 * \brief	Clears the stack.
	 *
	 * Remove all stored elements.
	 */
	void
	clear()
	{
		m_stack.clear();
		return;
	};


	/*
	 * ========================
	 * Other functions
	 */
	void
	swap(
		pgl_stack_t& other)
	{
		m_stack.swap(other);

		return;
	};


	/*
	 * ===========================
	 * This function gives access to the stored elements
	 * the order is the order of consumation
	 */

	iterator
	begin() noexcept
	{
		return m_stack.crbegin();
	};

	const_iterator
	begin() const noexcept
	{
		return m_stack.crbegin();
	};

	const_iterator
	cbegin() const noexcept
	{
		return m_stack.crbegin();
	};


	iterator
	end() noexcept
	{
		return m_stack.crend();
	};

	const_iterator
	end() const noexcept
	{
		return m_stack.crend();
	}

	const_iterator
	cend() const noexcept
	{
		return m_stack.crend();
	};



	/*
	 * ==========================
	 * Private Members
	 */
private:
	container_type 				m_stack;	//!< This Variable stores the elements
}; //End class(pgl_stack_t)


PGL_NS_END(pgl)


namespace std
{
	template<typename T, class Conatiner>
	void
	swap(
		::pgl::pgl_stack_t<T, Conatiner>& a,
		::pgl::pgl_stack_t<T, Conatiner>& b)	noexcept( std::is_nothrow_move_constructible<::pgl::pgl_stack_t<T, Conatiner> >::value && std::is_nothrow_move_assignable<::pgl::pgl_stack_t<T, Conatiner> >::value )
	{
		a.swap(b);
	};
}; //End namespace(std)


#endif //End includeguard
