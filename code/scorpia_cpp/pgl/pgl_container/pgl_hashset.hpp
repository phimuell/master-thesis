#pragma once
#ifndef PGL_CONTAINER__PGL_HASHSET_HPP
#define PGL_CONTAINER__PGL_HASHSET_HPP
/**
 * \brief	This file proviodes the implementation of a hash set
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_utility.hpp>
#include <pgl/pgl_allocator.hpp>


//Include the correct underlying implementation
#if defined(PGL_CONATINER_USE_STD_HASHSET) && PGL_CONATINER_USE_STD_HASHSET != 0
	//Include the STD version
#	include <unordered_set>
#else
	//Include the boost version
# 	include <boost/unordered_set.hpp>
#endif

namespace pgl
{


/**
 * \brief 	This class defines a hasset
 * \typedef 	pgl_hashSet_t
 *
 * A hash set is similar to a unordered_set from the std library.
 * This one also implements all teh requierements.
 * It can be deselcted if the std implementation or the boost implementation should be used.
 *
 * \tparam  key_t 	This is the type that is used for the key. It can not be changed.
 * \tparam  hash_fkt	This is the hash function, it maps key_t to a std::uintmax value.
 * 			 It has to model the hash concept.
 * \tparam  pred_fkt 	This function is used to determine if two key_t values are the same.
 * \tparam  Alloc	This is the allocator that is used.
 */
template<
	typename 	key_t,
	class 		hash_fkt = ::pgl::pgl_hash_t<key_t>,
	class 		pred_fkt = std::equal_to<key_t>,
	class 		Alloc = ::pgl::pgl_allocator_t<key_t>
>
#if defined(PGL_CONATINER_USE_STD_HASHSET) && PGL_CONATINER_USE_STD_HASHSET != 0
using pgl_hashSet_t = ::std::unordered_set<key_t, hash_fkt, pred_fkt, Alloc>;
#else
using pgl_hashSet_t = ::boost::unordered_set<key_t, hash_fkt, pred_fkt, Alloc>;
#endif


/**
 * \typedef 	HashSet_t
 *
 * This class ensures backwards compability with older versions of PGL.
 * It is an alias of pgl_hashSet_t.
 * It should not be used.
 */
template<typename key_t, class hash_fkt = ::pgl::pgl_hash_t<key_t>, class pred_fkt = std::equal_to<key_t>, class Alloc = ::pgl::pgl_allocator_t<key_t> >
using HashSet_t = pgl_hashSet_t<key_t, hash_fkt, pred_fkt, Alloc>;





}; //End namespace(pgl)




#endif //End include guard
