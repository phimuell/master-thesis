#pragma once
#ifndef PGL_CONTAINER__PGL_REFERENCECONTAINER_HPP
#define PGL_CONTAINER__PGL_REFERENCECONTAINER_HPP
/**
 * \file	pgl_container/pgl_reference_container.hpp
 * \brief 	This file defines a reference container.
 *
 * A reference container is a container, that don't stores actual objects, instead it stors reference to it
 * It is basically a vector, that has a vector.
 * It supports two kinds of iteratore.
 * The normal iterators, are references. The other one return reference_wrapper.
 *
 *
 * This vector is still uncomplete.
 */
#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_exception.hpp>
#include <pgl_core/pgl_assert.hpp>
#include <pgl_core/pgl_int.hpp>
#include <pgl/pgl_allocator.hpp>


#include <pgl_container/pgl_vector.hpp>

#include <functional>
#include <type_traits>

#include <vector>


PGL_NS_START(pgl)

/**
 * \brief	This class is a reference container.
 * \class 	pgl_refVec_t
 *
 * This class is a vector that does not store objects, but references to them.
 * This class can be seen as pgl_vector_t<std::reference_wrapper>.
 * However this class has some syntactic sugar that makes handling with the types easier.
 *
 * Note that this class stores pointer to the objects and that the life time of an object is not
 * extended by adding it to this container.
 * This means that is could happen that pointers inside this class are dangeling.
 * It is the users responsibility to ensure that the objects are not deleted.
 *
 * \tparam  T		The type we want to map.
 * \tparam  Alloc	The allocator that is used.
 */
template<
	typename 	T,
	class 		Alloc = ::pgl::pgl_allocator_t<T>
>
class pgl_refVec_t
{
	/*
	 * ================================
	 * Some usefull typedefs
	 */
public:
	using BaseType_t		= std::remove_cv_t<T>;			//!< This is the type, without const modifier

	using refWrapper_t		= std::reference_wrapper<T>;	//!< The reference type
	using internalContainer_t 	= std::vector<refWrapper_t, Alloc>; 		//!< The internal container, that is used

	static constexpr bool isConstType    = std::is_const<T>::value;		//!< This bool stores if the type is const.

	/*
	 * Types that are required for the vector
	 */
	using value_type 		= T; 	//refWrapper_t;
	using allocator_type		= Alloc;
	using size_type			= typename internalContainer_t::size_type;
	using difference_type 		= typename internalContainer_t::difference_type;
	using reference 		= value_type&;
	using const_reference 		= const BaseType_t&;

	using iterator			= typename internalContainer_t::iterator;
	using const_iterator		= typename internalContainer_t::const_iterator;
	using reverse_iterator		= std::reverse_iterator<iterator>;
	using const_reverse_iterator	= std::reverse_iterator<const_iterator>;


	/*
	 * ================
	 * constructors
	 */
	pgl_refVec_t();

	pgl_refVec_t(
		const pgl_refVec_t&);

	pgl_refVec_t(
		pgl_refVec_t&& );

	~pgl_refVec_t();

	pgl_refVec_t&
	operator= (
		const pgl_refVec_t&);

	pgl_refVec_t&
	operator= (
		pgl_refVec_t&&);


	/*
	 * ==================================
	 * Capacity
	 */

	/**
	 * \brief	Returns true if this is empty
	 */
	inline
	bool
	empty() const noexcept
	{
		return this->m_refs.empty();
	};

	/**
	 * \brief 	Retruns the size of this
	 */
	inline
	size_type
	size() const noexcept
	{
		return this->m_refs.size();
	};

	/**
	 * \brief 	Returns the allocated space
	 */
	inline
	size_type
	capacity() const noexcept
	{
		return m_refs.capacity();
	};



	/**
	 * \brief 	Reserve space
	 */
	inline
	void
	reserve(
		const size_type 	expectedSize)
	{
		//this->m_refs.reserve(expectedSize);
		return;
		(void)expectedSize;
	};


	/**
	 * \brief 	Shrinks to fit
	 *
	 * This function may redduce the allocated storage.
	 * Move will be used
	 */
	inline
	void
	shrink_to_fit()
	{
		this->m_refs.shrink_to_fit();
		return;
	};


	/*
	 * ==============================
	 * Modifiers
	 */

	/**
	 * \brief 	Removes all elements from the container.
	 *
	 * The capacity is unchanged.
	 */
	void
	clear() noexcept
	{
		this->m_refs.clear();
		return;
	};
#if 0
	/**
	 * \brief 	create a newcreate a new reference, at the suplied point.
	 *
	 * \param pos	The index at which we want to insert
	 * \param newElement	The new element we whant a reference to.
	 */
	iterator
	emplace(
		iterator 	pos,
		BaseType_t&	newElement)
	{
		pgl_assert(std::distance(this->m_refs.begin(), pos) < size());

		this->m_refs.insert(pos, refWrapper_t(newElement));

		return (this->begin() + pos);
	};

	iterator
	emplace(
		iterator 	pos,
		refWrapper_t&	newElement)
	{
		pgl_assert(std::distance(this->m_refs.begin(), pos) < size());

		this->m_refs.insert(pos, newElement);

		return (this->begin() + pos);
	};

	/**
	 * \brief 	Delete the element that is provided
	 * \param pos	The index of the element to remove
	 * \return 	Returns the iterator next to the element
	 */
	iterator
	erase(
		iterator 	pos)
	{
		pgl_assert(std::distance(m_refs.begin(), pos) < m_refs.size());

		return this->m_refs.erase(pos);
	};

	const_iterator
	erase(
		const_iterator 	pos)
	{
		pgl_assert(std::distance(m_refs.cbegin(), pos) < m_refs.size());

		return this->m_refs.erase(pos);
	};

	/**
	 * \brief 	This function creates a new reeference at the end
	 * \param newElem	The element we what a reference to
	 */
	void
	push_back(
		BaseType_t& newElem)
	{
		this->m_refs.push_back(refWrapper_t(newElem));
		return;
	};
#endif
	void
	push_back(
		refWrapper_t&& newElem)
	{
		this->m_refs.push_back(newElem);
		return;
	};

	/**
	 * \brief 	Remove the last element form the conllection
	 */
	void
	pop_back()
	{
		this->m_refs.pop_back();
		return;
	};


	/*
	 * ============================
	 * Iterator
	 */
	iterator
	begin() noexcept
	{
		return this->m_refs.begin();
	};

	const_iterator
	begin() const noexcept
	{
		return this->m_refs.cbegin();
	};

	const_iterator
	cbegin() const noexcept
	{
		return this->m_refs.cbegin();
	};


	iterator
	end() noexcept
	{
		return this->m_refs.end();
	};

	const_iterator
	end() const noexcept
	{
		return this->m_refs.cend();
	};

	const_iterator
	cend() const noexcept
	{
		return this->m_refs.cend();
	};




	/*
	 * ========================
	 * Element access
	 */

	/**
	 * \brief gives element acces
	 */
	inline
	reference
	operator[](
		const size_type& n) noexcept
	{
		pgl_assert(n < size());

		return this->m_refs[n];
	};

	inline
	const_reference
	operator[](
		const size_type& n) const noexcept
	{
		pgl_assert(n < size());

		return this->m_refs[n];
	};


	/**
	 * \brief	AT access
	 */
	inline
	reference
	at(
		const size_type& n)
	{
		if(size() <= n)
		{
			throw PGL_EXCEPT_OutOfBound("RefVector, tried to access out of bound.");
		};

		return m_refs[n];
	};


	inline
	const_reference
	at(
		const size_type& n) const
	{
		if(size() <= n)
		{
			throw PGL_EXCEPT_OutOfBound("RefVector, tried to access out of bound. (const)");
		};

		return m_refs[n];
	};


	inline
	reference
	front() noexcept
	{
		pgl_assert(size() != 0);

		return m_refs.front();
	};


	inline
	const_reference
	front() const noexcept
	{
		pgl_assert(size() != 0);

		return m_refs.front();
	};


	inline
	reference
	back() noexcept
	{
		pgl_assert(size() != 0);

		return m_refs.back();
	};

	inline
	const_reference
	back() const noexcept
	{
		pgl_assert(size() != 0);

		return m_refs.back();
	};




private:
	internalContainer_t 		m_refs;	//!< The container that stores the references
}; //end class(pgl_refVec_t)


template<typename T, class Alloc>
pgl_refVec_t<T, Alloc>::pgl_refVec_t() = default;


template<typename T, class Alloc>
pgl_refVec_t<T, Alloc>::pgl_refVec_t(
		const pgl_refVec_t&) = default;

template<typename T, class Alloc>
pgl_refVec_t<T, Alloc>::pgl_refVec_t(
		pgl_refVec_t&& ) = default;

template<typename T, class Alloc>
pgl_refVec_t<T, Alloc>::~pgl_refVec_t() = default;

template<typename T, class Alloc>
pgl_refVec_t<T, Alloc>&
pgl_refVec_t<T, Alloc>::operator= (
	const pgl_refVec_t&) = default;

template<typename T, class Alloc>
pgl_refVec_t<T, Alloc>&
pgl_refVec_t<T, Alloc>::operator= (
	pgl_refVec_t&&) = default;




PGL_NS_END(pgl)








#endif //End include guard
