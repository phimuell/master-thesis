#pragma once
#ifndef PGL_CONTAINER__PGL_HASHMAP_HPP
#define PGL_CONTAINER__PGL_HASHMAP_HPP
/**
 * \brief	This file defines a replacement for the unordered_map container.
 * 		 Inside PGL this container is called a hash map.
 */


#include <pgl_core/pgl_core.hpp>
#include <pgl_core/pgl_utility.hpp>
#include <pgl/pgl_allocator.hpp>


//Include the correct underlying container
#if defined(PGL_CONTAINER_USE_STD_HASHMAP) && PGL_CONTAINER_USE_STD_HASHMAP != 0
	//Include std version
#	include <unordered_map>
#else
	//Include boost version
#	include <boost/unordered_map.hpp>
#endif


namespace pgl
{

/**
 * \brief	This class is a replacement for a unordered_map.
 * \typedef 	pgl_hashMap_t
 *
 * This class defines a type that adheres to the definition of the unoredered_map type of the standard.
 * It can be selected if the boost implementation or the std implementation is used.
 *
 * \tparam  key_t 	This is the type that is used for the key. It can not be changed.
 * \tparam  value_t 	This is the mapped type. It can be modified after the insertion.
 * \tparam  hash_fkt	This is the hash function, it maps key_t to a std::uintmax value.
 * 			 It has to model the hash concept.
 * \tparam  pred_fkt 	This function is used to determine if two key_t values are the same.
 * \tparam  Alloc	This is the allocator that is used.
 */
template<
	typename 	key_t,
	typename 	value_t,
	class 		hash_fkt = ::pgl::pgl_hash_t<key_t>,
	class 		pred_fkt = std::equal_to<key_t>,
	class 		Alloc = ::pgl::pgl_allocator_t<::std::pair<const key_t, value_t> >
>
#if defined(PGL_CONTAINER_USE_STD_HASHMAP) && PGL_CONTAINER_USE_STD_HASHMAP != 0
using pgl_hashMap_t = ::std::unordered_map<key_t, value_t, hash_fkt, pred_fkt, Alloc>;
#else
using pgl_hashMap_t = ::boost::unordered_map<key_t, value_t, hash_fkt, pred_fkt, Alloc>;
#endif


/**
 * \brief	This class is an alias of pgl_hashMap_t.
 *
 * This class ensures backwards compability.
 * It should not be used.
 */
template<typename key_t, typename value_t, class hash_fkt = ::pgl::pgl_hash_t<key_t>, class pred_fkt = std::equal_to<key_t>, class Alloc = ::pgl::pgl_allocator_t<::std::pair<const key_t, value_t> > >
using HashMap_t = pgl_hashMap_t<key_t, value_t, hash_fkt, pred_fkt, Alloc>;



}; //End namespace(pgl)




#endif //End include guard
