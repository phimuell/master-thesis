#pragma once
#ifndef PGL_CONTAINER__PGL_STABLE_VECTOR_HPP
#define PGL_CONTAINER__PGL_STABLE_VECTOR_HPP
/**
 * \brief	This file implements a stable vector type.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl/pgl_allocator.hpp>

//Include BOOST
#include <boost/container/stable_vector.hpp>

namespace pgl
{

/**
 * \brief	This class implements a static vector type.
 * \typedef 	pgl_stableVector_t
 *
 * A stable vector is a vector that guarantees iterator
 * stability when the container is modified. In simple
 * terms it is a std::vector<T*> type, with a wrapper to
 * hide the fact that pointers are stored.
 *
 * It still allows accessing objects (by index) in O[1]
 * time, but it is sligthly slower since no continuus memeory
 * exists.
 *
 * This type is nopt part of the std standard and is
 * provided by boost.
 *
 * \tparam  T 		The type we want to store.
 * \tparam  Alloc	The allocator that is used.
 */
template<
	typename 	T,
	class 		Alloc = ::pgl::pgl_allocator_t<T>
>
using pgl_stableVector_t = ::boost::container::stable_vector<T, Alloc>;


/**
 * \typedef 	StableVector_t
 *
 * This is an alias for pgl_stableVector_t.
 * It is provided to ensure compability with
 * older parts of PGL.
 * It should not be used.
 */
template<typename T, class Alloc = ::pgl::pgl_allocator_t<T> >
using StableVector_t = pgl_stableVector_t<T, Alloc>;



}; //End namespace(pgl)

#endif //End include guard

