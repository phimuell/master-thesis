#pragma once
#ifndef PGL_CONTAINER__PGL_SET_HPP
#define PGL_CONTAINER__PGL_SET_HPP
/**
 * \brief	This file proivides a type equivalent to std::set.
 */

//Include PGL
#include <pgl_core/pgl_core.hpp>
#include <pgl/pgl_allocator.hpp>

//Include the underlying oimplementation
#if defined(PGL_CONTAINER_USE_STD_SET) && PGL_CONTAINER_USE_STD_SET != 0
#	include <set>
#else
#	include <boost/container/set.hpp>
#endif

namespace pgl
{

/**
 * \typedef	pgl_set_t
 * \brief	This class implements the functionality of std::set.
 *
 * This class implements teh full fucntionality that is requiered for std::set.
 * The usage of the std implmenetation can be enbaled at compile time.
 *
 * \tparam  key_t 	The value of the object we want to store in the set and the type we used for identifing them.
 * \tparam  comp_fkt	A predicate that can establish a less than relation between the keys.
 * \tparam  Alloc	The allocator that is used.
 */
template<
	typename 	key_t,
	class 		comp_fkt = std::less<key_t>,
	class 		Alloc = ::pgl::pgl_allocator_t<key_t>
>
#if defined(PGL_CONTAINER_USE_STD_SET) && PGL_CONTAINER_USE_STD_SET != 0
using pgl_set_t = ::std::set<key_t, comp_fkt, Alloc>;
#else
using pgl_set_t = ::boost::container::set<key_t, comp_fkt, Alloc>;
#endif


/**
 * \typedef	Set_t
 *
 * This is an alias of pgl_set_t. It is provided
 * for backward compability with older parts of PGL.
 * It should not be used.
 */
template<typename key_t, class comp_fkt = std::less<key_t>, class Alloc = ::pgl::pgl_allocator_t<key_t> >
using Set_t = pgl_set_t<key_t, comp_fkt, Alloc>;


}; //End namespace(pgl)

#endif //End include guard



