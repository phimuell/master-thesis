# Info
This is the PGL folder.
It contains the code of PGL.

This is the stage of the library, as it was last used.
It will be addapted for the use inside the master thesis.

## machen.sh
This script contains a function, "machen".
This file can create the CMake files that are used to populate a library.


# PGL
PGL stands for $\phi \gamma \lamdba$ which is PCL and means "Philip's C Library".
It was developed for the Bachelor thesis, that was an problem on graph.
Thsi means it also implements a graph, which is not needed/used in this project.

However it contains some usefull helper functions and an infrastructure that can be extended.

## History
The PGL that was used in the bachelor thesis was actually version two.
The first version was written for the courdse "Designe of Parallel and High Performance Computing".
There is also the source of teh name, it was "Philip's Graph Library".

The library was mainly written by Philip Müller.
But Stefano Weidmann and Manuel Rodrigez also contributed parts.


# Folders
Here we list the folders of PGL.
It gives a small summary about the code they contain.
Some of them have their own README inside.

## GRAPH
This folder contains all the code that is related to the graph that PGL implements.
For the matser thesis it is not used.
The graph code is splitted into several folder and all of them are located inside this one.
This folder is added to the include path.

## PGL 
This folder contains general code of the library.

## PGL_CONTAINER
This folder contains the code for the containers that are used inside PGL.
PGL provides all containers that are also offered by the STD.
The implementation uses BOOST.
However it is possible to select the STD implementation, if aviable.
This folder is inside the search path.

## PGL_CORE
This folder contains the core configuration of PGL.
There is the config file that controls PGL.
Code in this folder is only allowed to include files from that folder.
The first PGL header that is included must be "pgl_core.hpp".
The folder is part of the search path.

## PGL_PARALLEL
This folder conatins some helper constructs that helps with parallel programming.

## PGL_RANDOM
This folder implements some random number generators.
it also provides "pgl_rand()" which generates better numbers than "std::rand()".

## PGL_STAT
Contains some functions that deals with statistics.
Like a histogram or quantiles.

## PGL_UTIL
Contains utility code.

## PGL_LINALG
This folder contains code that allows linear algebra.
It is absically used for uniformly include Eigen.
It can be seen similary to parallel folder.


