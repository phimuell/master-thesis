#pragma once
/**
 * \file	code/pgl/pgl_random/pgl_xoroshiro.hpp
 * \brief	This fle implements a xoroshiro128 random number generator.
 *
 * For informations about the original authors see below.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_assert.hpp>


//Include STD
#include <limits>


/**************************************************************************************************
 *                          THE XOROSHIRO 128 + RANDOM NUMBER GENERATOR                           *
 **************************************************************************************************/

/*
 * Shamelessly stolen from http://xoroshiro.di.unimi.it/xoroshiro128plus.c (creative commons C0)
 * All credit is due to Sebastiano Vigna and David Blackman
 *
 */

/* This is the successor to xorshift128+. It is the fastest full-period
   generator passing BigCrush without systematic failures, but due to the
   relatively short period it is acceptable only for applications with a
   mild amount of parallelism; otherwise, use a xorshift1024* generator.

   Beside passing BigCrush, this generator passes the PractRand test suite
   up to (and included) 16TB, with the exception of binary rank tests,
   which fail due to the lowest bit being an LFSR; all other bits pass all
   tests. We suggest to use a sign test to extract a random Boolean value.

   Note that the generator uses a simulated rotate operation, which most C
   compilers will turn into a single instruction. In Java, you can use
   Long.rotateLeft(). In languages that do not make low-level rotation
   instructions accessible xorshift128+ could be faster.

   The state must be seeded so that it is not everywhere zero. If you have
   a 64-bit seed, we suggest to seed a splitmix64 generator and use its
   output to fill s. */


PGL_NS_START(pgl)
PGL_NS_START(random_intern)



typedef struct Xoroshiro_State_Struct {
    uInt64_t lower;
    uInt64_t upper;
} Xoroshiro_State_t;

static inline
uInt64_t
Xoroshiro_RotateLeft(
    const uInt64_t x,
    int k
) {
    return (x << k) | (x >> (64 - k));
}

// Returns a random uInt64_t
static inline
uInt64_t
Xoroshiro_GetNextNumber(
    Xoroshiro_State_t* state
) {
    const uInt64_t s0 = state->lower;
    uInt64_t s1 = state->upper;
    const uInt64_t result = s0 + s1;

    s1 ^= s0;
    state->lower = Xoroshiro_RotateLeft(s0, 55) ^ s1 ^ (s1 << 14); // a, b
    state->upper = Xoroshiro_RotateLeft(s1, 36); // c

    return result;
}

/* This is the jump function for the generator. It is equivalent
   to 2^64 calls to next(); it can be used to generate 2^64
   non-overlapping subsequences for parallel computations. */

static inline
void
Xoroshiro_JumpNumbers(
    Xoroshiro_State_t* state
) {
    static const uInt64_t JUMP[] = { 0x8a5cd789635d2dff, 0x121fd2155c472f96 };

    uInt64_t newLowerState = 0;
    uInt64_t newUpperState = 0;
    for (int i = 0; i < (int)(sizeof(JUMP) / sizeof(*JUMP)); i++)
        for (int b = 0; b < 64; b++) {
            if (JUMP[i] & 1ULL << b) {
                newLowerState ^= state->lower;
                newUpperState ^= state->upper;
            }
            Xoroshiro_GetNextNumber(state);
        }

    state->lower = newLowerState;
    state->upper = newUpperState;
}

static inline
uInt64_t
Xoroshiro_SplitMix64(
    uInt64_t* x
) {
    uInt64_t z = (*x += UINT64_C(0x9E3779B97F4A7C15));
    z = (z ^ (z >> 30)) * UINT64_C(0xBF58476D1CE4E5B9);
    z = (z ^ (z >> 27)) * UINT64_C(0x94D049BB133111EB);
    return z ^ (z >> 31);
}

static inline
Xoroshiro_State_t
Xoroshiro_GetInitialState(
    uInt64_t seed,
    const unsigned threadNo
) {
    Xoroshiro_State_t initialState;
    initialState.lower = seed;
    initialState.upper = Xoroshiro_SplitMix64(&seed);

    // jump to my section; inefficient if threadNo is high.
    // Then we would need to distribute the initial state
    for (unsigned j = 0; j < threadNo; ++j) {
        Xoroshiro_JumpNumbers(&initialState);
    }

    return initialState;
}

static inline
double
Xoroshiro_GenerateRealInClosedInterval(
    const double lowerBound,
    const double upperBound,
    Xoroshiro_State_t* state
) {
    assert(lowerBound <= upperBound);
    const uInt64_t randomInteger = Xoroshiro_GetNextNumber(state);

    const double intervalLength = upperBound - lowerBound;
    const double randomReal = lowerBound + (intervalLength * (double) randomInteger) / (double) UINT64_MAX;

    assert(lowerBound <= randomReal && randomReal <= upperBound);

    return randomReal;
}

PGL_NS_END(random_intern)



/**************************************************************************************************
 *             ADAPTATION OF THE XOROSHIRO RNG AS C++ 11 RANDOM NUMBER ENGINE CONCEPT             *
 **************************************************************************************************/


#include <limits>

// satisfies the concept UniformRandomBitGenerator
/**
 * \class 	pgl_xoroshiro128RNG_t
 * \brief	A random number generator using the xoroshiro128.
 *
 * This class was originaly written by Stefano Weidman (stefanow@etz.ch).
 * It was addapted by Philip Müller for pgl.
 *
 * It satisfied the concept of a Uniform radmon umber generator.
 *
 * This random number generator is made to be used in parallel computations.
 */
class pgl_xoroshiro128RNG_t
{

    public:
        // named with underscore for compatibility with other RNGs in <random>
        using result_type = uint64_t;
        const static result_type default_seed = 42;



        /**
         * \brief	Sefault seed used
         */
        inline
        pgl_xoroshiro128RNG_t()
        {
            this->seed(default_seed);
        }


        /**
         * \brief	Seed the generator with a given seed.
         *
         * One can also use the thread number, wich is defaulted to zero.
         */
        inline
        pgl_xoroshiro128RNG_t(
        	uInt64_t seedToUse,
            	const unsigned threadNo = 0)
        {
            this->seed(seedToUse, threadNo);
        }


        pgl_xoroshiro128RNG_t(
        	const pgl_xoroshiro128RNG_t&) = default;

        pgl_xoroshiro128RNG_t(
        	pgl_xoroshiro128RNG_t&&) = default;

        pgl_xoroshiro128RNG_t&
        operator= (
        	const pgl_xoroshiro128RNG_t&) = default;

        pgl_xoroshiro128RNG_t&
        operator= (
        	pgl_xoroshiro128RNG_t&&) = default;


        /**
         * \brief	Seed the generator anew.
         *
         * This also need a seed and a thread number
         */
        inline
        void
        seed(
        	uint64_t seedToUse,
            	const unsigned threadNo = 0)
        {
            	seedUsed_M = seedToUse;
            	state_M = random_intern::Xoroshiro_GetInitialState(seedToUse, threadNo);
        }

        /**
         * \brief	Returns the seed of this
         */
        inline
        result_type
        getSeed() const
        {
            return seedUsed_M;
        }

        /**
         * \brief 	Return the state of *this.
         */
        inline
        random_intern::Xoroshiro_State_t
        getState() const
        {
            return state_M;
        }


        inline static constexpr
        result_type
        min()
        {
            return std::numeric_limits<result_type>::min();
        }


        inline static constexpr
        result_type
        max()
        {
            return std::numeric_limits<result_type>::max();
        }

        /**
         * \brief	Produce a random number
         */
        inline
        result_type
        operator()()
        {
            return random_intern::Xoroshiro_GetNextNumber(&state_M);
        }

        /*
         * =========================
         * Private memeber
         */

private:
	random_intern::Xoroshiro_State_t 	state_M;		//The state
        result_type 				seedUsed_M;		//The used seed
};



PGL_NS_END(pgl)
