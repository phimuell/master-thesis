#pragma once
/**
 * \file 	code/pgl/pgl_random/pgl_seedRNG.hpp
 * \brief	A thread safe rng that can be used to seed other generators.
 *
 * This is a thread safe RNG, that is used to seed other generators.
 * It is not copyable, nor moveable. It shall not be initialized by the user.
 * It is a global object, that is created by pgl.
 *
 * It uses a default seed. But the user can seed it once.
 * It is protected by a lock, so the user shall not use it directly since it is prety slow.
 *
 * The object that is created is called pgl_seedRNG inside the pgl namespace
 *
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>


//Include STD
#include <memory>


PGL_NS_START(pgl)

/**
 * \class 	pgl_seedRNG_impl
 * \brief	A fwd of the implementation of the rng
 */
class pgl_seedRNG_impl;



/**
 * \class 	pgl_seedRNG_t
 * \brief	This is a implementation of a thread safe rng.
 *
 * Its main purpose is to seed other generators
 */
class pgl_seedRNG_t
{
	/*
	 * ================================
	 * Typedefs
	 */
public:
	using result_type 	= ::pgl::uInt64_t;		//!< The result type of the rng
	using Implt_t 		= pgl_seedRNG_impl;		//!< The underlying implementation
	using Implt_ptr		= ::std::unique_ptr<Implt_t>;	//!< The pointer to the implementation


	/*
	 * ========================================
	 * Constructor
	 *
	 * All constructor except a dumy one are deleted
	 */
public:
	pgl_seedRNG_t() = delete;

	pgl_seedRNG_t(
		const pgl_seedRNG_t&) = delete;

	pgl_seedRNG_t(
		pgl_seedRNG_t&&) = delete;

	pgl_seedRNG_t&
	operator= (
		pgl_seedRNG_t&&) = delete;

	pgl_seedRNG_t&
	operator= (
		const pgl_seedRNG_t&) = delete;


	/**
	 * \brief	This is the actual only way of creating one, but the user cant do it
	 */
	pgl_seedRNG_t(
		const pgl_seedRNG_impl&) noexcept(false);


	~pgl_seedRNG_t() noexcept;

	/*
	 * =======================================
	 * Functions for the random generator
	 *
	 * All the functions below are thread safe
	 */
public:

	/**
	 * \brief	This function returns a random number
	 */
	result_type
	operator()() noexcept;


	/**
	 * \brief	This function returns a random number
	 */
	result_type
	next() noexcept;


	/**
	 * \brief	This function discarges, the next random numer, without using it
	 */
	void
	discard() noexcept;


	/**
	 * \brief	This function dicarges some of the next random numbers
	 */
	void
	discard(
		const Size_t& n) noexcept;


	/**
	 * \brief	Retruns true, if *this was reseded by the user allready.
	 *
	 * May not be true, is only true during the lock
	 */
	bool
	wasSeeded() noexcept;


	/**
	 * \brief	Seeds the underlying generator with the given value.
	 *
	 * If *this was allready seesed, then false is returned
	 */
	bool
	seedByUser(
		const result_type& newSeed) noexcept;

	/**
	 * \brief	This function is used if pgl is used in a ditributed seting.
	 *
	 * It takes a seed and derive a unques seed from this.
	 * This is done by seeding another rng and then performing some steps, the result is then taken as the seed of *this.
	 *
	 * This function also returns true if this was possible$
	 */
	bool
	seedByUserJump(
		const result_type& 	newBasicSeed,
		const Size_t& 		ID) noexcept;


	/**
	 * \brief	does the same as the seedByUser, but it *this was allready seeded, then an exception is generated.
	 */
	bool
	seedByUser_strong(
		const result_type& newSeed) noexcept(false);


	/**
	 * \brief	This doies the same, as the other seedByUserJump-function, but if the generator was allready seeded, it produces an exception.
	 */
	bool
	seedByUserJump_strong(
		const result_type&	newBasicSeed,
		const Size_t&		ID) noexcept(false);

	/**
	 * \brief	Returns the minimum number that returns the generator
	 */
	result_type
	min() const noexcept;


	/**
	 * \brief	Returns the maximum that the generator can return
	 */
	result_type
	max() const noexcept;

	/**
	 * \brief	This function returns the numbers of calls to this
	 */
	Size_t
	callCount() const noexcept;


	/*
	 * =====================
	 * Internal Variables
	 */
private:
	Implt_ptr 		m_impl;		//!< The underlying implementation
}; //End class(pgl_seedRNG_t)




//This is the declaration of the global object
extern
pgl_seedRNG_t pgl_seedRNG;


/**
 * \brief	This is an alias for the global seeding generator.
 */
inline
pgl_seedRNG_t::result_type
rand()
{
	return ::pgl::pgl_seedRNG();
};






PGL_NS_END(pgl)









