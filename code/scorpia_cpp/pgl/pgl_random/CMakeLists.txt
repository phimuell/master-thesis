# This CMake file will add all the files in the current folder
# to the PGL library.
 
target_sources(
	pgl
  PRIVATE
	"${CMAKE_CURRENT_LIST_DIR}/pgl_seedRNG.cpp"
  PUBLIC
	"${CMAKE_CURRENT_LIST_DIR}/pgl_seedRNG.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_splitmix64.hpp"
	"${CMAKE_CURRENT_LIST_DIR}/pgl_xoroshiro128.hpp"
)
