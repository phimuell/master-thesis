#pragma once
/**
 * \file	code/pgl/pgl_random/pgl_splitmix64.hpp
 * \brief	Implements the splitmix algorithm for random number generaters
 *
 */
#include <pgl_core.hpp>
#include <pgl_int.hpp>


PGL_NS_START(pgl)


/*  Written in 2015 by Sebastiano Vigna (vigna@acm.org)

To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

See <http://creativecommons.org/publicdomain/zero/1.0/>. */


/* This is a fixed-increment version of Java 8's SplittableRandom generator
   See http://dx.doi.org/10.1145/2714064.2660195 and
   http://docs.oracle.com/javase/8/docs/api/java/util/SplittableRandom.html

   It is a very fast generator passing BigCrush, and it can be useful if
   for some reason you absolutely want 64 bits of state; otherwise, we
   rather suggest to use a xoroshiro128+ (for moderately parallel
   computations) or xorshift1024* (for massively parallel computations)
   generator. */


class pgl_splitmix64RNG_t
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using State_t 		= ::pgl::uInt64_t;	//!< Type that represent the internal state of the engine
	using result_type 	= ::pgl::uInt64_t;	//!< The type the generator returns

#define PGL_RANDOM_SPLITMIX64_DEFAULT_SEED 42
	const static result_type ce_defaultSeed = 42;	//!< The default seed
	const static result_type t = 42;


	/*
	 * =====================
	 * Constructors
	 */
public:
	/**
	 * \brief	Uses the provided seed to seed this
	 */
	inline
	pgl_splitmix64RNG_t(
		const State_t& Seed) noexcept
	{
		this->seed(Seed);
	};

	/**
	 * \brief	This is the default consructor, it uses the default seed
	 */
	inline
	pgl_splitmix64RNG_t() noexcept
	{
		this->seed(PGL_RANDOM_SPLITMIX64_DEFAULT_SEED);
	};


	/**
	 * \brief	Copyconstructor is defaulted
	 */
	inline
	pgl_splitmix64RNG_t(
		const pgl_splitmix64RNG_t&) noexcept = default;


	/**
	 * \brief	Copyassignment is defaulted
	 */
	inline
	pgl_splitmix64RNG_t&
	operator= (
		const pgl_splitmix64RNG_t&) noexcept = default;


	/**
	 * \brief	Move assignemnt is default
	 */
	inline
	pgl_splitmix64RNG_t(
		pgl_splitmix64RNG_t&&) noexcept = default;


	/**
	 * \brief	Move assignment is defaulted
	 */
	inline
	pgl_splitmix64RNG_t&
	operator= (
		pgl_splitmix64RNG_t&&) noexcept = default;



	/*
	 * ==========================
	 * Normal functions
	 */
public:

	/**
	 * \brief	Get the next number in the sequence
	 */
	inline
	result_type
	operator()() noexcept
	{
		return this->next();
	};


	/**
	 * \brief	This function returns the next number in the sequence
	 */
	inline
	result_type
	next() noexcept
	{
		uInt64_t z = (m_state += 0x9e3779b97f4a7c15);
		z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
		z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
		return z ^ (z >> 31);
	};

	/**
	 * \brief	Moves the generator one steps furter
	 */
	inline
	void
	discard() noexcept
	{
		(void)(this->next());
		return;
	};


	/**
	 * \brief	This function dicharges the generator, by the requested steps
	 */
	inline
	void
	discard(
		const Size_t&	n) noexcept
	{
		for(Size_t it = 0; it != n; ++it)
		{
			(void)(this->next());
			(void)it;
		};

		return;
	};



	/**
	 * \brief	This function sets the state of *this to the presented state
	 */
	inline
	void
	seed(
		const State_t& newState) noexcept
	{
		m_state = newState;
		return;
	};


	/*
	 * ==================================
	 * The interal state of this
	 */
private:
	State_t 		m_state;	//!< The state of this
}; //End class(pgl_splitmix64RNG_t)


PGL_NS_END(pgl)



#undef PGL_RANDOM_SPLITMIX64_DEFAULT_SEED

