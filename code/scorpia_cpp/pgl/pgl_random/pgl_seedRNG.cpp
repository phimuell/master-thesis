/**
 * \file 	code/pgl/pgl_random/pgl_seedRNG.cpp
 * \brief	A thread safe rng that can be used to seed other generators.
 */

//Include PGL
#include <pgl_core.hpp>
#include <pgl_int.hpp>
#include <pgl_exception.hpp>
#include <pgl_assert.hpp>
#include <pgl_random/pgl_splitmix64.hpp>
#include <pgl_random/pgl_seedRNG.hpp>


//Include STD
#include <memory>
#include <atomic>
#include <mutex>
#include <limits>
#include <random>


PGL_NS_START(pgl)

/**
 * \class 	pgl_seedRNG_impl
 * \brief	A fwd of the implementation of the rng
 */
class pgl_seedRNG_impl
{
	/*
	 * =========================
	 * Typedefs
	 */
public:
	using 	BaseRNG_t	= ::pgl::pgl_splitmix64RNG_t;		//!< The underlying rng
	using 	result_type 	= BaseRNG_t::result_type;		//!< The result type
	using 	Lock_t 		= ::std::mutex;				//!< The lock type
	using 	Guard_t 	= ::std::unique_lock<Lock_t>;		//!< The lock guard we use
	using   BoolFlag_t	= ::std::atomic<bool>;			//!< The bool type that stores the generator


	/*
	 * ======================
	 * Construictors
	 *
	 * We need only the default constructor
	 * So so we define it only
	 */

	/// Default constructor
	pgl_seedRNG_impl() noexcept(false) :
	  m_lock(),
	  m_wasSeeded(false),
	  m_geni()
	{

	};

	/*
	 * ==================================
	 * Functions
	 *
	 * These functions are atomar, and protected by the lock of *this.
	 */

	/**
	 * \brief	This fucntion returns the next random number
	 */
	result_type
	next() noexcept
	{
		//A lock guard that will protect us
		Guard_t guard(m_lock);

		return m_geni.next();
	};


	/**
	 * \brief	This function discarges some states
	 */
	void
	discard(
		const Size_t& n)
	{
		Guard_t guard(m_lock);

		m_geni.discard(n);
		return;
	};


	/**
	 * \brief	This function returns true, if *this was seeded by the user.
	 *
	 * Notice, that there is the old problem, taht this is not soe correct.
	 */
	bool
	wasSeededByUser() noexcept
	{
		Guard_t guard(m_lock);
		return m_wasSeeded.load();
	};

	/**
	 * \brief	This function seeds the underlying generator again, if it was not seeded.
	 *
	 * If it was seeded, then it returns false, and noting happens.
	 */
	bool
	seedByUser(
		const result_type& newSeed) noexcept
	{
		Guard_t guard(m_lock);

		return this->seedGeneratorInternal(newSeed, guard);
	};


	/**
	 * \brief	This is the seed by user function. It will seed the generator, if possible.
	 * It is used to derive a unique seed.
	 */
	bool
	seedByUserJump(
		const result_type& 	newSeed,
		const Size_t&		jump) noexcept
	{
		Guard_t guard(m_lock);

		//Check if this was seeded
		if(m_wasSeeded.load() == true)
		{
			return false;
		};

		//Derive a seed
		const result_type derivedSeed = deriveSeed(newSeed, jump);

		return this->seedGeneratorInternal(derivedSeed, guard);
	};



	result_type
	max() const noexcept
	{
		return std::numeric_limits<result_type>::max();
	};

	result_type
	min() const noexcept
	{
		return std::numeric_limits<result_type>::min();
	};


	/*
	 * =================================
	 * Private Functions
	 */
private:


	/**
	 * \brief	This function implements the seeding of *this, this function is internal and does not
	 * 		Aquiere the lock, instead it assumes, that the lock is hold, for that the look must be passed to this
	 *
	 */
	bool
	seedGeneratorInternal(
		const result_type& 	newSeed,
		Guard_t& 		Guard)
	{
		if(Guard.owns_lock() == false)
		{
			pgl_assert(Guard.owns_lock() == true);
			std::exit(1);
		};

		//Test if the generator was seeded before
		if(m_wasSeeded.load() == true)
		{
			return false;
		};

		//Seed the geni with new seed
		m_geni.seed(newSeed);

		m_wasSeeded.store(true);

		return m_wasSeeded.load();
	};


	/**
	 * \brief	This function derives a seed form a seed.
	 *
	 * This function is intended to be used for the jump functions
	 * It is static
	 */
	static
	result_type
	deriveSeed(
		const result_type 	seed,
		const Size_t 		jumps) noexcept
	{
		//Generating a rng
		std::minstd_rand metaSeedGeni(seed);

		const Size_t offset = 500000;
		const Size_t jumpImprove = 10;
		if((jumpImprove * jumps) >= (std::numeric_limits<Size_t>::max() - offset))
		{
			pgl_assert(false && "Too many Threads for the seed generator to handle.");
			std::exit(1);
		};

		//Performing some steep, to make them independend
		for(Size_t it = 0; it != ((jumpImprove * jumps) + offset); ++it)
		{
			volatile result_type e = metaSeedGeni();
			(void)e;
		};

		return metaSeedGeni();
	};


	/*
	 * ================
	 * Private Variables
	 */
private:
	Lock_t 			m_lock;			//!< The lock that protects anything
	BoolFlag_t 		m_wasSeeded;		//!< Stores if the gerntor was seeded
	BaseRNG_t 		m_geni;			//!< The protected RNG
};


//THis is a variable we need to initialize the seed
pgl_seedRNG_impl DummyImpl;


/**
 * \var 	pgl_globalRNG
 * \brief	This is the global variable, that is used as a generator
 */
pgl_seedRNG_t pgl_seedRNG(DummyImpl);




pgl_seedRNG_t::pgl_seedRNG_t(
		const pgl_seedRNG_impl& i) :
  m_impl(::std::make_unique<pgl_seedRNG_impl>())
{
	(void)i;
};

pgl_seedRNG_t::~pgl_seedRNG_t() noexcept = default;


pgl_seedRNG_t::result_type
pgl_seedRNG_t::operator()() noexcept
{
	return m_impl->next();
};


	/**
	 * \brief	This function returns a random number
	 */
pgl_seedRNG_t::result_type
pgl_seedRNG_t::next() noexcept
{
	return m_impl->next();
};


void
pgl_seedRNG_t::discard() noexcept
{
	return m_impl->discard(1);
};


void
pgl_seedRNG_t::discard(
	const Size_t& n) noexcept
{
	return m_impl->discard(n);
};


bool
pgl_seedRNG_t::wasSeeded() noexcept
{
	return m_impl->wasSeededByUser();
};


bool
pgl_seedRNG_t::seedByUser(
	const result_type& newSeed) noexcept
{
	return m_impl->seedByUser(newSeed);
};

pgl_seedRNG_t::result_type
pgl_seedRNG_t::min() const noexcept
{
	return m_impl->min();
};

pgl_seedRNG_t::result_type
pgl_seedRNG_t::max() const noexcept
{
	return m_impl->max();
};


bool
pgl_seedRNG_t::seedByUserJump(
	const result_type& 	newBasicSeed,
	const Size_t& 		ID) noexcept
{
	return this->m_impl->seedByUserJump(newBasicSeed, ID);
};

bool
pgl_seedRNG_t::seedByUser_strong(
	const result_type& newSeed) noexcept(false)
{
	const bool wasSucces = m_impl->seedByUser(newSeed);
	if(wasSucces == false)
	{
		throw PGL_EXCEPT_RUNTIME("The global generator was allready seeded.");
	};

	return wasSucces;
};



bool
pgl_seedRNG_t::seedByUserJump_strong(
	const result_type& 	newBasicSeed,
	const Size_t& 		ID) noexcept(false)
{
	const bool wasSuccess = this->m_impl->seedByUserJump(newBasicSeed, ID);
	if(wasSuccess == false)
	{
		throw PGL_EXCEPT_RUNTIME("The global generator was allready seeded.");
	};

	return wasSuccess;
};


PGL_NS_END(pgl)









